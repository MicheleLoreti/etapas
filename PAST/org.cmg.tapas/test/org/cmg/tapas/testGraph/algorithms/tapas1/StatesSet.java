package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

/**
 * La classe StatesSet rappresenta un insieme di stati.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class StatesSet {

	/**
	 * Rappresenta l'insieme degli stati. 
	 **/	
	private Vector<State> states = null;
	
	/**
	 * Costruisce una nuovo insieme di stati vuoto. 
	 **/
	public StatesSet() {
		states = new Vector<State>();
  	}
  	
	/**
	 * Restituisce il numero di elementi contenuti nell'insieme.
	 **/   	
  	public int getDimension() {
  		return states.size();
  	}

	/**
	 * Aggiunge all'insieme un oggetto di tipo <tt>State</tt>. 
	 * Durante l'inserimento, l'insieme viene
	 * mantenuto ordinato lessicograficamente rispetto ai nomi degli stati. 
	 * Inoltre non � possibile aggiungere stati ripetuti, 
	 * cio� stati il cui nome � gi� presente nell'insieme.
	 **/
	public int addState(State s) {	
		int i = 0;
		int compare = 1;
		for( i=0; i<getDimension(); i++ ) {
			compare = stateAt(i).compareTo(s);
			if( compare >= 0 ) 
				break;
		}
		if( compare != 0 ) 
			states.add(i,s);
			
		return i;
	}
	
	/**
	 * Aggiunge all'insieme un insieme di oggetti.
	 **/
	public boolean addStatesSet(StatesSet ss) {
		int initialDim = getDimension();
		for( int i=0; i<ss.getDimension(); i++ )
			addState(ss.stateAt(i));
		return getDimension() != initialDim;
	}

	/**
	 * Rimuove dall'insieme lo stato il cui nome coincide
	 * con quello passato come parametro.
	 **/
	public boolean removeState(String name) {
		int i = getStateIndex(name);
		if (i != -1) {
			states.removeElementAt(i);
			return true;
		}
		
		return false;
	}

	
	/**
	 * Permette di ricercare uno stato all'interno 
	 * dell'insieme specificandone il nome. 
	 **/		
	public State getState(String name) {
		int compare = 0;
		int low = 0;
		int high = this.getDimension() - 1;
		int mid = 0;
		
		while (low <= high) {
			mid = (low + high) / 2;
			compare = this.stateAt(mid).getName().compareTo(name);
			if (compare < 0) low = mid + 1;
			else if (compare > 0) high = mid -1;
			else return this.stateAt(mid);
		}
		return null;
	}

	/**
	 * Controlla se l'insieme contiene uno stato con etichetta 
	 * uguale al parametro stringa in ingresso. 
	 **/	
	public int getStateIndex(String name) {
		int compare = 0;
		int low = 0;
		int high = this.getDimension() - 1;
		int mid = 0;
		
		while (low <= high) {
			mid = (low + high) / 2;
			compare = this.stateAt(mid).getName().compareTo(name);
			if (compare < 0) low = mid + 1;
			else if (compare > 0) high = mid -1;
			else return mid;
		}
		return -1;
	}

	/**
	 * Ritorna l'oggetto di tipo <tt>State</tt> contenuto nell'insieme 
	 * degli stati alla posizione 
	 * specificata dal parametro intero in ingresso. 
	 **/ 
	public State stateAt(int index) {
		try	{ 
			return states.elementAt(index); 
		}	
		catch(ArrayIndexOutOfBoundsException e)	{ 
			return null; 
		}
	}

	

	/**
 	 * Calcola l'Acceptance set dell'insieme di stati. 
 	 * Implementa la funzione cos� definita:
 	 * <p>
 	 * 		AP(Q) = { CP(s) | s appartiene a Q e DP(s,tau) = vuoto }
 	 * <p>
 	 * dove <tt>Q</tt> rappresenta questo insieme di stati.
 	 **/
	public AcceptanceSet aP() {
		AcceptanceSet accs = new AcceptanceSet();
		for( int i=0; i<this.getDimension(); i++ )
			if( stateAt(i).dP( Action.ACTION_TAU ).isEmpty() )
				accs.addActionsSet( stateAt(i).cP() );	
		
		return accs;		
	}

	/**
 	 * Implementa la funzione DP(Q,a). Tale funzione � cos� definita 
 	 * <p>
 	 * 		DP(Q,a) = Unione( DP(s,a) )	per ogni s in Q
 	 * <p>
 	 **/
	public StatesSet dP(Action alfa) {
		StatesSet statesSet = new StatesSet();
		
		for( int i=0; i<getDimension(); i++ )
			statesSet.addStatesSet( stateAt(i).dP(alfa) );
			
		return statesSet;
	}

	/**
 	 * Effettua la epsilon chiusura dell'insieme di stati. 
 	 * Tale funzione � cos� definita 
 	 * <p>
 	 * 		sEpsilon(Q) = Union( sEspilon(s) )     per ogni s in Q
 	 * <p>
 	 **/
	public StatesSet sEpsilon() {
		StatesSet sepsilon = new StatesSet();
		StatesSet temp = null;

		sepsilon.addStatesSet(this);
		temp = sepsilon.dP( Action.ACTION_TAU );
		if (!(temp.isEmpty()))
			if (sepsilon.addStatesSet(temp))
				return sepsilon.sEpsilon();		

		return sepsilon;
	}

	/**
 	 * Controlla se l'insieme di stati � convergente, 
 	 * cio� se tutti gli stati sono convergenti.
 	 **/
	public boolean statesSetConvergent() {
		for (int i=0; i<this.getDimension(); i++)	
			if ((!this.stateAt(i).stateConvergent())) return false;
		return true;
	}

	/**
 	 * Controlla se l'insieme di stati contiene almeno uno stato finale, 
 	 * senza alcuna transizione uscente.
 	 **/
	public boolean statesSetFinal() {
		for (int i=0; i<this.getDimension(); i++)	
			if (this.stateAt(i).stateFinal()) return true;
		return false;
	}
	
	/**
	 * Controlla se l'insieme degli stati contiene lo stato passato come
	 * parametro di ingresso. 
	 **/
	public boolean containState(State s) {
		if( getState(s.getName())==s ) 
			return true;
		
		return false;
	}

	/**
	 * Verifica se l'insieme correte � uguale all'insieme passato come 
	 * parametro.
	 **/
	public boolean equals(StatesSet ss) {
		if( getDimension()!=ss.getDimension() ) 
			return false;
		
		for( int i=0; i<ss.getDimension(); i++ )
			if( !containState(ss.stateAt(i)) ) 
				return false;
		
		return true;
	}


	/**
	 * Controlla se l'insieme degli stati � vuoto. 
	 **/
	public boolean isEmpty() {
		return getDimension() == 0;
	}

	/**
	 * Calcola la differenza tra l'insieme degli stati corrente e 
	 * quello passato come parametro.
	 **/ 
	public StatesSet difference(StatesSet statesSet) {
		State state = null;
		StatesSet result = new StatesSet();
		for (int i=0; i<this.getDimension(); i++) {
			state = this.stateAt(i);
			if( !statesSet.containState(state) )
				result.addState(state);
		}
		return result;
	}

	/**
	 * Calcola l'intersezione tra l'insieme degli stati corrente e 
	 * quello passato come parametro.
	 **/ 
	public StatesSet intersection(StatesSet statesSet) {
		State state = null;
		StatesSet result = new StatesSet();
		for (int i=0; i<this.getDimension(); i++) {
			state = this.stateAt(i);
			if( statesSet.containState(state) )
				result.addState(state);
		}
		
		return result;
	}

	/**
	 * Stampa l'intero insieme.
	 **/ 
	public void print() {
		int i = 0;
		
		System.out.print("{");
		for (i=0; i<this.getDimension(); i++) {
			if( i>0 ) 
				System.out.print(", ");
			states.elementAt(i).print();
		}
		if( i==0 ) 
			System.out.print("empty");
			
		System.out.print("}");
	}

	/**
	 * Stampa tutte le transizioni uscenti dagli stati dell'insieme
	 **/ 
	public void printTransitions() {
		for( int i=0; i<getDimension(); i++ )
			states.elementAt(i).printTransitions();
	}

}
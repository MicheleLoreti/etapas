package org.cmg.tapas.graph;

import java.util.Comparator;

/**
 * Confrontatore di stati: confronta due stati sulla base del nome.
 *   
 * @author Guzman Tierno
 */
public class StateNameComparator<S extends StateInterface> 
implements Comparator<S> {
	
	/**
	 * Confronta due stati sulla base del nome. <p>
	 * Rende -1 se il primo viene prima. <p>
	 * Rende 1 se il secondo viene prima. <p>
	 * Rende 0 se hanno lo stesso nome. 
	 */
 	public int compare(S state1, S state2) {
 		return state1.getId().compareTo( state2.getId() );
 	}
           	
}

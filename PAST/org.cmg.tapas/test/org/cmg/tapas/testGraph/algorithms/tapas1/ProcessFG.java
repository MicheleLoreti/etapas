package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.HashMap;
import java.util.Vector;

/**
 * La classe ProcessFG rappresenta un Final Graph.
 * A ciascuno stato viene fatto corrispondere un flag booleano 
 * che sta ad indicare se l'insieme contiene uno stato di deadlock. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class ProcessFG extends Lts {

	/**
	 * Insieme dei flag di deadlock degli stati. 
	 */	
	private Vector<Boolean> finalFlags = new Vector<Boolean>();

	/**
	 * Restituisce l'insieme dei flag di deadlock di tutti gli stati.
	 */  	
  	public Vector<Boolean> getFinalFlags() {
  		return finalFlags;	
  	}

	/**
	 * Restituisce il flag di deadlock relativo allo stato specificato.
	 */
	public boolean getFinalFlag(State s) {
		int k = getStatesSet().getStateIndex(s.getName());
		return getFinalFlags().elementAt(k);
	}

	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt> 
	 * inizializzando a <tt>false</tt> il flag associato a tale stato. 
	 */	  	
	@Override 
  	public void addState(State s) {
  		addState(s, false);
  	}  	

	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt>.
	 */  	
  	public void addState(State s, boolean finalFlag) {
  		StatesSet ss = getStatesSet();
		int i = ss.addState(s);
		if( ss.getDimension() != finalFlags.size() )
			finalFlags.add(i, finalFlag);
  	}

  	/**
	 * Aggiunge all'insieme degli stati un nuovo oggetto di tipo <tt>State</tt> 
	 * con flag di deadlock inizializzato a <tt>false</tt>. 
  	 */
	@Override 
  	public State addNewState() {
  		return addNewState(false);	
  	}  	

  	/**
	 * Aggiunge all'insieme degli stati un nuovo oggetto di tipo <tt>State</tt> 
	 * con flag di deadlock inizializzato al valore passato come parametro. 
  	 */
  	public State addNewState(boolean finalFlag) {
  		State state = new State(getNewStateName());
 		int i = getStatesSet().addState( state );
		finalFlags.add(i, finalFlag);

  		return state;
  	}

	/**
	 * Crea un nuovo Final Graph derivato dalla fusione dei due 
	 * Final Graphs. 
	 * <p>
	 * Durante l'operazione di fusione vengono generati stati 
	 * nuovi all'interno del nuovo AG.
	 */
	public static ProcessFG createProcessFusion(
		ProcessFG p1, 
		ProcessFG p2
	) {
		ProcessFG p = new ProcessFG();
		ActionsSet actionsSet = null;
		StatesSet statesSet = null;
		Vector<Boolean> ff = null;
		State newState = null;
		int actualIndex = 0;
		State stateOrigin = null;
		State stateDest = null;
		Action a = null;	
		StatesSet destinations = null;
		TransitionCouple tc = null;
		Vector<TransitionCouple> transitionCouples = null;		
		HashMap<State,State> hmS = new HashMap<State,State>();
		HashMap<Action,Action> hmA = new HashMap<Action,Action>();		

		//Fondo l'insieme delle azioni
		actionsSet = p1.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex));
		}
		actionsSet = p2.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex));
		}

		//Fondo l'insieme degli stati, rinominando gli stati con lo stesso nome
		//e fondo anche i rispettivi finalFlags
		statesSet = p1.getStatesSet();
		ff = p1.getFinalFlags();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = p.addNewState( ff.elementAt(i) );
			hmS.put(statesSet.stateAt(i),newState);
		}
		statesSet = p2.getStatesSet();
		ff = p2.getFinalFlags();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = p.addNewState( ff.elementAt(i) );
			hmS.put(statesSet.stateAt(i),newState);
		}

		//Creo la relazione di transizione del processo fuso
		statesSet = p1.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}
		statesSet = p2.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}

		//Setto i 2 stati iniziali del nuovo processo
		p.setInitState(  hmS.get(p1.getInitState()) );
		p.setSecondInitState( hmS.get(p2.getInitState()) );
		
		return p;
	}
  	
	/**
	 * Costruisce ricorsivamente il Final Graph corrispondente 
	 * al processo passato come parametro il cui
	 * stato iniziale sia associato all'insieme di stati 
	 * del processo originale passato anch'esso come parametro. 
	 * E' un metodo di supporto al metodo <tt>buildFinalGraph</tt> 
	 * per la costruzione del Final Graph associato ad un processo.
	 */
	private static State buildFG(
		Lts p,
		ProcessFG p1,
		StatesSet s,
		// no convergence flag
		HMap h
	) {
		State tnew = h.getElement(s,true);
		if( tnew!=null )
			return tnew;

		boolean f = s.statesSetFinal();
		State t = p1.addNewState(f);
		h.addItem(s, true, t);
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i); 
			StatesSet dp = s.dP(alfa);
			if( !dp.isEmpty() ) {
				tnew = buildFG(p, p1, dp, h);
				t.addTransition(alfa,tnew);
			}
		}

		return t;
	}

	/**
	 * Costruisce ricorsivamente il Weak Final Graph 
	 * corrispondente al processo passato come parametro il cui
	 * stato iniziale sia associato all'insieme di stati del 
	 * processo originale passato anch'esso come parametro. 
	 * E' un metodo di supporto al metodo <tt>buildWFGraph</tt> 
	 * per la costruzione dello Weak Final Graph associato ad un processo.
	 **/
	private static State buildWFG(
		Lts p,
		ProcessFG p1,
		StatesSet s,
		// no convergence flag
		HMap h
	) {
		State tnew = h.getElement(s, true);
		if (tnew != null) 
			return tnew;

		boolean f = s.statesSetFinal();
		State t = p1.addNewState(f);
		h.addItem(s, true, t);
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i); 
			if (!(alfa.isTau())) {
				StatesSet dp = s.dP(alfa);
				if( !dp.isEmpty() ) {
					StatesSet salfa = dp.sEpsilon();
					tnew = buildWFG(p, p1, salfa, h);
					t.addTransition(alfa, tnew);
				}
			}
		}

		return t;
	}

	/**
	 * Costruisce il Final Graph corrispondente 
	 * al processo passato come parametro.
	 */	
	public static ProcessFG buildFinalGraph(Lts p) {
		ProcessFG p1 = new ProcessFG();
		HMap h = new HMap();
		
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i);
			p1.addAction(alfa);
		}

		StatesSet ss = new StatesSet();
		ss.addState( p.getInitState() );
		State initState = buildFG(p, p1, ss, h);
		
		p1.setInitState(initState);
		return p1;
	}
	
	/**
	 * Costruisce il Weak Final Graph corrispondente al 
	 * processo passato come parametro.
	 **/	
	public static ProcessFG buildWFGraph(Lts p) {
		ProcessFG p1 = new ProcessFG();
		HMap h = new HMap();
		
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i);
			if( !alfa.isTau() ) 
				p1.addAction(alfa);
		}
		
		StatesSet sm = p.getInitState().sEpsilon();
		State initState = buildWFG(p, p1, sm, h);

		p1.setInitState(initState);
		return p1;
	}

	/**
	 * Stampa l'intero Final Graph sullo standard output.
	 */
	@Override 
	public void print() {
		System.out.print("  |-- States Set:\n");
		StatesSet ss = getStatesSet();
		
		int i = 0;	
		System.out.print("           {");
		for (i=0; i<ss.getDimension(); i++) {
			if(i>0) 
				System.out.print(",\n            ");
			System.out.print("(");
			ss.stateAt(i).print();
			
			System.out.print(", " + finalFlags.elementAt(i).booleanValue());
			System.out.print(")");
		}
		if( i==0 ) 
			System.out.print("empty");
			
		System.out.print("}");
		System.out.print("\n  |-- Actions Set: ");
		
		getActionsSet().print();
		
		System.out.print("\n  |-- Transition Relation:\n");
		getStatesSet().printTransitions();
		
		System.out.print("  |-- Initial State: ");
		if( getInitState()==null) 
			System.out.print("nessuno");
		else 
			getInitState().print();
			
		if( getSecondInitState()!= null ) {
			System.out.print("\n  |-- Second Initial State: ");
			getSecondInitState().print();
		}
		
		System.out.print("\n\n");
	}

	
}
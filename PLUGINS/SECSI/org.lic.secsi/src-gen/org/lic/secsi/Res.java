/**
 */
package org.lic.secsi;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Res</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Res#getLeftlists <em>Leftlists</em>}</li>
 *   <li>{@link org.lic.secsi.Res#getLeflists <em>Leflists</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getRes()
 * @model
 * @generated
 */
public interface Res extends EObject
{
  /**
   * Returns the value of the '<em><b>Leftlists</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Leftlists</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Leftlists</em>' reference.
   * @see #setLeftlists(Channel)
   * @see org.lic.secsi.SecsiPackage#getRes_Leftlists()
   * @model
   * @generated
   */
  Channel getLeftlists();

  /**
   * Sets the value of the '{@link org.lic.secsi.Res#getLeftlists <em>Leftlists</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Leftlists</em>' reference.
   * @see #getLeftlists()
   * @generated
   */
  void setLeftlists(Channel value);

  /**
   * Returns the value of the '<em><b>Leflists</b></em>' reference list.
   * The list contents are of type {@link org.lic.secsi.Channel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Leflists</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Leflists</em>' reference list.
   * @see org.lic.secsi.SecsiPackage#getRes_Leflists()
   * @model
   * @generated
   */
  EList<Channel> getLeflists();

} // Res

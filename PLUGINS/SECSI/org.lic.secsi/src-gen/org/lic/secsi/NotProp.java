/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Prop</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.lic.secsi.SecsiPackage#getNotProp()
 * @model
 * @generated
 */
public interface NotProp extends Prop
{
} // NotProp

package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.HashMap;
import java.util.Vector;

/**
 * La classe ProcessDSG rappresenta un Divergence Sensitive Graph, 
 * A ciascuno stato del DSGraph viene fatto corrispondere un flag booleano, 
 * che sta ad indicare se l'insieme di stati associato nel processo originale 
 * contiene uno stato di deadlock, ed un altro flag booleano che indica
 * se l'insieme di stati associato nel processo originale contiene uno stato 
 * in grado di divergere. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class ProcessDSG extends Lts {
	
	/**
	 * Rappresenta l'insieme dei flag di deadlock degli stati.
	 */	
	private Vector<Boolean> finalFlags = new Vector<Boolean>();

	/**
	 * Rappresenta l'insieme dei flag di divergenza degli stati. 
	 */	
	private Vector<Boolean> closedFlags = new Vector<Boolean>();

 	/**
	 * Restituisce l'insieme dei flag di deadlock di tutti gli stati.
	 */ 	
  	public Vector<Boolean> getFinalFlags() {
  		return finalFlags;	
  	}
  	
	/**
	 * Restituisce il flag di deadlock dello stato specificato.
	 */
	public boolean getFinalFlag(State s) {
		int k = getStatesSet().getStateIndex(s.getName());
		return getFinalFlags().elementAt(k);
	}

 	/**
	 * Restituisce l'insieme dei flag di divergenza di tutti gli stati.
	 */
  	public Vector<Boolean> getClosedFlags() {
  		return closedFlags;	
  	}

	/**
	 * Restituisce il flag di divergenza relativo dello stato specificato.
	 */
	public boolean getClosedFlag(State s) {
		int k = this.getStatesSet().getStateIndex(s.getName());
		return getClosedFlags().elementAt(k);
	}

	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt>. 
	 */  	
	@Override 
  	public void addState(State s) {
  		addState( s, true, false );
  	}  	

	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt>.
	 */  	  	
  	public void addState(State s, boolean closedFlag, boolean finalFlag) {
  		StatesSet ss = getStatesSet();
		int i = ss.addState(s);
		if( ss.getDimension() != finalFlags.size() ) {
  			closedFlags.add(i, closedFlag );
  			finalFlags.add(i, finalFlag );
  		}
  	}  	  	

  	/**
	 * Aggiunge all'insieme degli stati un nuovo oggetto di tipo <tt>State</tt>.
  	 */
	@Override 
  	public State addNewState() {
  		return addNewState(true, false);
  	}  	

  	/**
	 * Aggiunge all'insieme degli stati un nuovo oggetto di tipo <tt>State</tt>.
  	 */
  	public State addNewState(boolean closedFlag, boolean finalFlag) {
  		State state = null;		
  		StatesSet ss = getStatesSet();
  		String s = getNewStateName();
  		state = new State(s);
 		int i = ss.addState(state);
  		closedFlags.add(i, closedFlag );
  		finalFlags.add(i, finalFlag );
  		
  		return state;
  	}  	
  	
	/**
	 * Crea un nuovo Divergence Sensitive Graph derivato dalla fusione dei 
	 * due Divergence Sensitive Graphs. 
	 */
	public static ProcessDSG createProcessFusion(
		ProcessDSG p1, 
		ProcessDSG p2
	) {
		ProcessDSG p = new ProcessDSG();
		ActionsSet actionsSet = null;
		StatesSet statesSet = null;
		Vector<Boolean> cf = null;
		Vector<Boolean> ff = null;
		State newState = null;
		int actualIndex = 0;
		State stateOrigin = null;
		State stateDest = null;
		Action a = null;	
		StatesSet destinations = null;
		TransitionCouple tc = null;
		Vector<TransitionCouple> transitionCouples = null;		
		HashMap<State,State> hmS = new HashMap<State,State>();
		HashMap<Action,Action> hmA = new HashMap<Action,Action>();

		// Fondo l'insieme delle azioni
		actionsSet = p1.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex));
		}
		actionsSet = p2.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex));
		}

		// Fondo l'insieme degli stati, rinominando gli stati con lo stesso nome
		// e fondo anche i rispettivi closedFlag e finalFlag
		statesSet = p1.getStatesSet();
		ff = p1.getFinalFlags();
		cf = p1.getClosedFlags();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = p.addNewState( cf.elementAt(i), ff.elementAt(i) );
			hmS.put(statesSet.stateAt(i),newState);
		}
		statesSet = p2.getStatesSet();
		ff = p2.getFinalFlags();
		cf = p2.getClosedFlags();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = p.addNewState( cf.elementAt(i), ff.elementAt(i) );
			hmS.put(statesSet.stateAt(i),newState);
		}		

		//Creo la relazione di transizione del processo fuso
		statesSet = p1.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					a = hmA.get( tc.getAction() );
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}
		statesSet = p2.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}

		// Setto i 2 stati iniziali del nuovo processo
		p.setInitState( hmS.get(p1.getInitState()) );
		p.setSecondInitState( hmS.get(p2.getInitState()) );

		return p;
	}

	/**
	 * Costruisce ricorsivamente il Divergence Sensitive Graph 
	 * corrispondente al processo passato come parametro 
	 */
	private static State buildDSG(
		Lts p,
		ProcessDSG p1,
		StatesSet s,
		HMap h
	) {
		State tnew = h.getElement(s, true);
		if( tnew!=null ) 
			return tnew;

		boolean c = s.statesSetConvergent();
		boolean f = s.statesSetFinal();
		State t = p1.addNewState(c,f);
		h.addItem(s, true, t);
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i); 
			if( alfa.isTau() )
				continue;
			StatesSet image = s.dP(alfa);
			if( !image.isEmpty() ) {
				tnew = buildDSG( p, p1, image.sEpsilon(), h );
				t.addTransition( alfa, tnew );
			}
		}

		return t;
	}

	/**
	 * Costruisce il Divergence Sensitive Graph corrispondente al 
	 * processo passato come parametro.
	 */		
	public static ProcessDSG buildDSGraph(Lts p) {
		ProcessDSG p1 = new ProcessDSG();
		
		HMap h = new HMap();
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i);
			if( !alfa.isTau() ) 
				p1.addAction(alfa);
		}
		StatesSet sm = p.getInitState().sEpsilon();
		State initState = buildDSG(p, p1, sm, h);

		p1.setInitState(initState);
		return p1;
	}

	/**
	 * Stampa l'intero Divergence Sensitive Graph sullo standard output. 
	 */
	@Override 
	public void print() {
		System.out.print("  |-- States Set:\n");
		StatesSet ss = getStatesSet();
		
		int i = 0;	
		System.out.print("           {");
		for( i=0; i<ss.getDimension(); i++ ) {
			if( i>0 ) 
				System.out.print(",\n            ");
			System.out.print( "(" );
			ss.stateAt(i).print();
			System.out.print( ", " + closedFlags.elementAt(i) );
			System.out.print( ", " + finalFlags.elementAt(i) );
			System.out.print( ")" );
		}
		if( i==0 ) 
			System.out.print("empty");			
		System.out.print("}");
		
		System.out.print("\n  |-- Actions Set: ");
		getActionsSet().print();
		
		System.out.print("\n  |-- Transition Relation:\n");
		getStatesSet().printTransitions();
		
		System.out.print("  |-- Initial State: ");
		if( getInitState()==null ) 
			System.out.print("nessuno");
		else 
			getInitState().print();
			
		System.out.print("\n\n");
	}

}
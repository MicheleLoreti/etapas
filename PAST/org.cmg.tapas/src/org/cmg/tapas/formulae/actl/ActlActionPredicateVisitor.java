/**
 * 
 */
package org.cmg.tapas.formulae.actl;


import org.cmg.tapas.formulae.actl.action.ActActionTauPredicate;
import org.cmg.tapas.formulae.actl.action.ActlActionPredicateUnion;
import org.cmg.tapas.formulae.actl.action.ActlActionSetPredicate;
import org.cmg.tapas.formulae.actl.action.ActlAllActions;
import org.cmg.tapas.formulae.actl.action.ActlDifAction;
import org.cmg.tapas.formulae.actl.action.ActlNotAction;
import org.cmg.tapas.formulae.actl.action.ActlOrAction;
import org.cmg.tapas.formulae.actl.action.TauPredicate;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface ActlActionPredicateVisitor<S extends APProcess<S,A> , A extends ActionInterface> {
	
	public void visit( ActlActionPredicate<S,A> chi );
	public void accept( TauPredicate<S,A> chi );
	public void accept( ActlAllActions<S,A> chi );
	public void accept( ActlNotAction<S,A> chi );
	public void accept( ActlActionSetPredicate<S,A> chi );
	public void accept( ActlActionPredicateUnion<S,A> chi);
	public void accept( ActActionTauPredicate<S,A> chi);
	public void accept( ActlOrAction<S,A> chi);
	public void accept( ActlDifAction<S,A> chi);

}

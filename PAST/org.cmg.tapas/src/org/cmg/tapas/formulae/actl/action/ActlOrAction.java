/**
 * 
 */
package org.cmg.tapas.formulae.actl.action;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlActionPredicateVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class ActlOrAction<S extends APProcess<S, A> , A extends ActionInterface> implements ActlActionPredicate<S,A> {

	private ActlActionPredicate<S, A> chi1;
	private ActlActionPredicate<S, A> chi2;

	public ActlOrAction(ActlActionPredicate<S,A> chi) {
		this.chi1 = chi1;
		this.chi2 = chi2;
	}

	/* (non-Javadoc)
	 * @see org.cmg.actl.ActlActionPredicate#acceptVisitor(org.cmg.actl.ActlActionPredicateVisitor)
	 */
	public void acceptVisitor(ActlActionPredicateVisitor<S,A> v) {
		v.accept(this);
	}

	/* (non-Javadoc)
	 * @see org.cmg.actl.ActlActionPredicate#getFilter()
	 */
	public Filter<A> getFilter() {
		final Filter<A> f1 = chi1.getFilter();
		final Filter<A> f2 = chi2.getFilter();
		return new Filter<A>() {

			public boolean check(A t) {
				return f1.check(t)&&(!f2.check(t));
			}
			
		};
	}

}

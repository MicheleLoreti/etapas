/**
 */
package org.lic.secsi;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Relop</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.lic.secsi.SecsiPackage#getRelop()
 * @model
 * @generated
 */
public enum Relop implements Enumerator
{
  /**
   * The '<em><b>REL EQ</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #REL_EQ_VALUE
   * @generated
   * @ordered
   */
  REL_EQ(0, "REL_EQ", "=="),

  /**
   * The '<em><b>REL NEQ</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #REL_NEQ_VALUE
   * @generated
   * @ordered
   */
  REL_NEQ(1, "REL_NEQ", "!="),

  /**
   * The '<em><b>REL LEQ</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #REL_LEQ_VALUE
   * @generated
   * @ordered
   */
  REL_LEQ(2, "REL_LEQ", "<="),

  /**
   * The '<em><b>REL LES</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #REL_LES_VALUE
   * @generated
   * @ordered
   */
  REL_LES(3, "REL_LES", "<"),

  /**
   * The '<em><b>REL GEQ</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #REL_GEQ_VALUE
   * @generated
   * @ordered
   */
  REL_GEQ(4, "REL_GEQ", ">="),

  /**
   * The '<em><b>REL GTR</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #REL_GTR_VALUE
   * @generated
   * @ordered
   */
  REL_GTR(5, "REL_GTR", ">");

  /**
   * The '<em><b>REL EQ</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>REL EQ</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #REL_EQ
   * @model literal="=="
   * @generated
   * @ordered
   */
  public static final int REL_EQ_VALUE = 0;

  /**
   * The '<em><b>REL NEQ</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>REL NEQ</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #REL_NEQ
   * @model literal="!="
   * @generated
   * @ordered
   */
  public static final int REL_NEQ_VALUE = 1;

  /**
   * The '<em><b>REL LEQ</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>REL LEQ</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #REL_LEQ
   * @model literal="<="
   * @generated
   * @ordered
   */
  public static final int REL_LEQ_VALUE = 2;

  /**
   * The '<em><b>REL LES</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>REL LES</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #REL_LES
   * @model literal="<"
   * @generated
   * @ordered
   */
  public static final int REL_LES_VALUE = 3;

  /**
   * The '<em><b>REL GEQ</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>REL GEQ</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #REL_GEQ
   * @model literal=">="
   * @generated
   * @ordered
   */
  public static final int REL_GEQ_VALUE = 4;

  /**
   * The '<em><b>REL GTR</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>REL GTR</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #REL_GTR
   * @model literal=">"
   * @generated
   * @ordered
   */
  public static final int REL_GTR_VALUE = 5;

  /**
   * An array of all the '<em><b>Relop</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final Relop[] VALUES_ARRAY =
    new Relop[]
    {
      REL_EQ,
      REL_NEQ,
      REL_LEQ,
      REL_LES,
      REL_GEQ,
      REL_GTR,
    };

  /**
   * A public read-only list of all the '<em><b>Relop</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<Relop> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>Relop</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static Relop get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      Relop result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Relop</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static Relop getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      Relop result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Relop</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static Relop get(int value)
  {
    switch (value)
    {
      case REL_EQ_VALUE: return REL_EQ;
      case REL_NEQ_VALUE: return REL_NEQ;
      case REL_LEQ_VALUE: return REL_LEQ;
      case REL_LES_VALUE: return REL_LES;
      case REL_GEQ_VALUE: return REL_GEQ;
      case REL_GTR_VALUE: return REL_GTR;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private Relop(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //Relop

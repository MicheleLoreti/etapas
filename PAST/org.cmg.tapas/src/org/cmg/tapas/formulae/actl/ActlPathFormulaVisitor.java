/**
 * 
 */
package org.cmg.tapas.formulae.actl;


import org.cmg.tapas.formulae.actl.path.ActlFuture;
import org.cmg.tapas.formulae.actl.path.ActlGlobally;
import org.cmg.tapas.formulae.actl.path.ActlNext;
import org.cmg.tapas.formulae.actl.path.ActlUntil;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface ActlPathFormulaVisitor<S extends APProcess<S,A> , A extends ActionInterface> {

	public void visitNextFormula( ActlNext<S, A> phi );
	public void visitUntilFormula( ActlUntil<S, A> phi );
	public void visitFeaturesFormula(ActlFuture<S, A> phi);
	public void visitGloballyFormula(ActlGlobally<S, A> phi); 
	
}

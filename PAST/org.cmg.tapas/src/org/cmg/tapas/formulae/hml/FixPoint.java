/**
 * 
 */
package org.cmg.tapas.formulae.hml;


import java.util.HashSet;

import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public final class FixPoint<X extends APProcess<X, Y>,Y extends ActionInterface> 
						extends HmlFormula<X,Y> {

	private HmlFormula<X,Y> arg;
	private boolean isMax;
	private boolean cloning = false;
	private FixPoint<X,Y> clone;
	private HashSet<X> tableaux;
	private String var;
	private HashSet<X> cacheSat = new HashSet<X>();
	private HashSet<X> cacheUnsat  = new HashSet<X>();

	public FixPoint( String var ) {
		this(true , var );
	}
	
	public FixPoint( boolean isMax , String var ) {
		this(new True<X, Y>() , isMax , var );
	}
	
	public FixPoint( HmlFormula<X,Y> arg , boolean isMax , String var ) {
		this.arg = arg;
		this.isMax = isMax;
		this.tableaux = new HashSet<X>();
		this.var = var;
	}

	public boolean satisfies( X x ) {
		if (cacheSat.contains(x)) {
			return true;
		}
		if (cacheUnsat.contains(x)) {
			return false;
		}
		if (tableaux.contains(x)) {
			return isMax;
		}
		FixPoint<X, Y> f = (FixPoint<X, Y>) clone();
		f.tableaux.add(x);
		f.cacheSat = cacheSat;
		f.cacheUnsat = cacheUnsat;
		boolean flag = f.arg.satisfies(x);
		if (flag) {
			cacheSat.add(x);
		} else {
			cacheUnsat.add(x);
		}
		return flag;
	}

	public Proof<X,Y> getProof( X x ) {
		if (tableaux.contains(x)) {
			return new Proof<X, Y>( x , this , isMax );
		}
		FixPoint<X, Y> f = (FixPoint<X, Y>) clone();
		f.tableaux.add(x);
		Proof<X, Y> p = f.arg.getProof(x);
		return new Proof<X, Y>(x,this, new Object[] { p } , p.isSuccess() );
	}

	public void setSubformula( HmlFormula<X,Y> f ) {
		this.arg = f ;
	}
	
	@Override
	protected HmlFormula<X, Y> doClone() {
		
		synchronized (this) {
			while (cloning) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}		
			cloning = true;
		}
		clone = new FixPoint<X, Y>(isMax,var);
		this.setCloning(true);
		HmlFormula<X,Y> f = arg.clone();
		clone.isMax = isMax;
		clone.tableaux = (HashSet<X>) tableaux.clone();
		clone.arg = f; 
		FixPoint<X, Y> toReturn = clone;
		toReturn.cacheSat = cacheSat;
		toReturn.cacheUnsat = cacheUnsat;
		synchronized (this) {
			cloning = false;
			clone = null;
			notifyAll();
		}
		return toReturn;
	}
	
	private synchronized FixPoint<X, Y> getClone() {
		return clone;
	}
	
	private synchronized boolean isCloning() {
		return cloning;
	}

	private synchronized void setCloning( boolean cloning ) {
		this.cloning = cloning;
	}
	
	public HmlFormula<X,Y> getReference() {
		return new Reference();
	}

	@Override
	protected boolean _equals(Object obj) {
		if(obj == null)
			return false;
		
		if (obj instanceof FixPoint) {
			FixPoint<X, Y> f = (FixPoint<X, Y>) obj;
			
			if(isMax != f.isMax ||
					var != f.var)
				return false;
			
			return arg.equals(f.arg);
		}
		
		return false;
	}
	
	public class Reference extends HmlFormula<X,Y> {
		
		private Reference() {
			
		}
		
		/* (non-Javadoc)
		 * @see org.cmg.lmc.Formula#satisfies(graph.Graph)
		 */
		public boolean satisfies(X x ) {
			return FixPoint.this.satisfies(x);
		}

		@Override
		protected HmlFormula<X, Y> doClone() {
			if (isCloning()) {
				return getClone().getReference();
			}
			return new Reference();
		}

		@Override
		protected void doVisit(FormulaVisitor<X, Y> visitor) {
			visitor.visitReference(this);
		}

		@Override
		protected String _toString() {
			return var;
		}

		@Override
		public String getUnicode() {
			return var;
		}

		@Override
		public Proof<X, Y> getProof(X s) {
			return FixPoint.this.getProof(s);
		}

		@Override
		public String operator() {
			return (isMax ? "MAX" : "MIN");
		}
		
		@Override
		public boolean _equals(Object obj) {
			if(obj == null)
				return false;
			
			if (obj instanceof FixPoint.Reference) { 
				Reference f = (Reference) obj;
								
				return equals(f);			
			}
			
			return false;
		}
	}

	@Override
	protected void doVisit(FormulaVisitor<X, Y> visitor) {
		visitor.visitFixPoint(this);
	}

	@Override
	protected String _toString() {
		return (isMax?"max ":"min ")+var+". "+arg;
	}

	@Override
	public String getUnicode() {
		return (isMax?"\u03BD ":"\u03BC ")+var+". "+arg.getUnicode();
	}
	
	@Override
	public String operator() {
		return (isMax ? "MAX" : "MIN");
	}
	
	public void setVar(String name){
		this.var = name;
	}
}

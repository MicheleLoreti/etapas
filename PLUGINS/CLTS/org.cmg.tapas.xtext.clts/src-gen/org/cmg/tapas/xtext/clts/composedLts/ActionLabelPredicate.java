/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate#getAct <em>Act</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getActionLabelPredicate()
 * @model
 * @generated
 */
public interface ActionLabelPredicate extends LabelPredicate
{
  /**
   * Returns the value of the '<em><b>Act</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Act</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Act</em>' reference.
   * @see #setAct(Action)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getActionLabelPredicate_Act()
   * @model
   * @generated
   */
  Action getAct();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate#getAct <em>Act</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Act</em>' reference.
   * @see #getAct()
   * @generated
   */
  void setAct(Action value);

} // ActionLabelPredicate

package org.cmg.tapas.formulae.hml;


import java.util.LinkedList;

import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.SingleActionFilter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author calzolai
 *
 */
public class DivergenceFormula<X extends APProcess<X, Y>,Y extends ActionInterface> 
			extends HmlFormula<X,Y> {

	private String toString = "DIV";
	private HmlFormula<X, Y> div;
	private Y tauAction;
	
	public DivergenceFormula(Y tauAction){
		this.tauAction = tauAction; 
		div = divFormula();
		
	}
	
	private HmlFormula<X, Y> divFormula(){
		// max X.(
		// <tau> true & <tau> X
		// )
		String name = "X";
		FixPoint<X, Y> fix = new FixPoint<X, Y>(true, name);

		LinkedList<HmlFormula<X, Y>> subList = new LinkedList<HmlFormula<X, Y>>();

		Filter<Y> f = new SingleActionFilter<Y>(tauAction);
		subList.add(new DiamondFormula<X, Y>(f, new True<X, Y>()));

		HmlFormula<X, Y> formula = new DiamondFormula<X, Y>(tauAction, fix.getReference());
		subList.add(formula);

		AndFormula<X, Y> and = new AndFormula<X, Y>(subList);
		fix.setSubformula(and);
		return fix;
	}
	
	@Override
	protected String _toString() {
		return toString;
	}

	@Override
	protected HmlFormula<X, Y> doClone() {
		return div.doClone();
	}

	@Override
	protected void doVisit(FormulaVisitor<X, Y> visitor) {
		visitor.visitDiv(this);
	}

	@Override
	protected boolean _equals(Object o) {
		if(o == null)
			return false;
		
		if (o instanceof DivergenceFormula){
			DivergenceFormula f = (DivergenceFormula) o;
			return tauAction.equals(f.tauAction);
		}else 
			return false;
	}

	@Override
	public Proof<X, Y> getProof(X s) {
		return div.getProof(s);
	}

	@Override
	public String getUnicode() {
		return toString;
	}

	@Override
	public String operator() {
		return "";
	}

	@Override
	public boolean satisfies(X s) {
		return div.satisfies(s);
	}

}

/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.state.ActlDiamond;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class DiamondMacro< S extends APProcess<S,A>,A extends ActionInterface> extends StateFormulaMacro<S, A>{

	private ActlActionPredicate<S,A> predicate;
	private StateFormulaMacro<S, A> macro;
	
	public DiamondMacro(ActlActionPredicate<S,A> predicate,
			StateFormulaMacro<S, A> macro) {
		super();
		this.predicate = predicate;
		this.macro = macro;
	}
	
	@Override
	public ActlFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		if (f.length != parameters()) {
			throw new IllegalArgumentException();
		}
		ActlFormula<S, A>[] right = new ActlFormula[ macro.parameters() ];
		System.arraycopy(f, 0, right, macro.parameters(), macro.parameters());
		return new ActlDiamond<S, A>(predicate, macro.getFormula(right));
	}

	@Override
	public int parameters() {
		return macro.parameters();
	}

	@Override
	public void acceptVisitor(MacroVisitor<S, A> visitor) {
		visitor.visitDiamondMacro(this);
	}

	public StateFormulaMacro<S, A> getArgument() {
		return macro;
	}
}

package org.cmg.tapas.graph.algorithms.branching.gv;


import java.util.LinkedHashSet;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;

/** 
 * Coda di BBlocks.
 * <p>
 * @param <S> tipo degli stati
 * @param <A> tipo delle azioni
 * 
 * @author Guzman Tierno
 **/

class BQueue<
	S extends StateInterface,
	A extends ActionInterface
> extends LinkedHashSet<BBlock<S,A>> {
	private static final long serialVersionUID = 1;
	
	@Override
    public boolean add(BBlock<S,A> block) {
		block.setQueue(this);
		return super.add(block);
	}
		
}


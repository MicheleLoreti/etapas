package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;


/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBStatesSetBottom {
	
	private Vector<BBState> bbstates = null;
	
	public BBStatesSetBottom() {
		bbstates = new Vector<BBState>();
  	}
  	
  	public int getDimension() {
  		return bbstates.size();
  	}

	public void addBBState(BBState s) {	
		bbstates.add(s);
	}
	
	public BBState getBBState(String name) {
		BBState s = null;
		for( int i=0; i<this.getDimension(); i++ ) {
			s = bbstateAt(i);
			if( s.getState().getName().equals(name) ) 
				return s;
		}
		
		return null;
	}

	public boolean containState(State s) {
		for( int i=0; i<this.getDimension(); i++ )	
			if( bbstateAt(i).getState()==s ) 
				return true;
				
		return false;
	}

	public BBState bbstateAt(int index) {
		try {
			return bbstates.elementAt(index);
		}	
		catch(ArrayIndexOutOfBoundsException unused) {
			return null;
		}
	}
	
	public boolean isEmpty() {
		if( getDimension()==0 ) 
			return true;
		return false;
	}

	public void print() {
		printAll(System.out);
	}

	public void printAll(java.io.PrintStream console) {
		int i = 0;
		
		for( i=0; i<this.getDimension(); i++ ) {
			if(i>0) 
				console.append("\n                ");
			bbstateAt(i).printAll(console);
		}
		if(i==0)
			console.append("empty");
	}	

}
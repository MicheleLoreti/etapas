package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessAG;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;



/**
 * La classe MustChecker implementa realizza l'algoritmo di verifica 
 * per l'equivalenza must. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class MustChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {
    /** 
     * Costruisce una nuova istanza del checker 
     **/
	public MustChecker() {
		super("Must");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 **/	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );

		// strong acceptance graphs
		// console.println("computing SAGraphs ...");
		ProcessAG p1SAG = ProcessAG.buildStrongAcceptanceGraph( p1 );
		ProcessAG p2SAG = ProcessAG.buildStrongAcceptanceGraph(	p2 );		

		// fusion
		// console.println("process fusion ...");
		ProcessAG pSAG = ProcessAG.createProcessFusion( p1SAG, p2SAG );

		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();
		
		// initializing partition 
		// console.println("inizializing partition ...");
		PTPartition initialRo = MustUtil.initPTPartition( pSAG );
		
		// refinement
		// console.println("refining ...");	
		PTState initialPTState1 = initialRo.getPTState(pSAG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pSAG.getSecondInitState());
		
		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pSAG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		boolean result = ro.checkSameClass(
			pSAG.getInitState(),pSAG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			MustUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName(),
				pSAG
			);
		}
		
		return result;
	}
}
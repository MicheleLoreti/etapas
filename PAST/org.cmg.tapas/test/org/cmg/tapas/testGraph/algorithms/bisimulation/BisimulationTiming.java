package org.cmg.tapas.testGraph.algorithms.bisimulation;

// graph
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.bisimulation.ks.KSChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.ksopt.KSOChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.mpt.MPTChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.pt.PTChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.rankBased.RankBasedChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.RandomGraphGenerator;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;


/**
 * BisimulationTiming esegue un confronto tra i vari
 * algoritmi per il calcolo della bisimulazione.
 *
 * @author Guzman Tierno
 **/
public class BisimulationTiming {
	
	public static void main(String[] args) {
		// Options

		// active checkers
		boolean ks = true;		// must be true, it is used for comparisons
		boolean kso = true;	
		boolean pt = true;
		boolean mpt = true;	
		int mpt_degree = 3;
		boolean rank = true;
		boolean tapas1 = true;
		
		int repetitions = 100;
		int actionCount = 4;	
		int statesNumber = 100;
		int printEvery = 1;
		double connectionProbability = 0.2;
		//	int statesNumber = 3600;
		//	double connectionProbability = 0.02;
		//	int statesNumber = 14000;
		//	double connectionProbability = 0.005;
		//	int statesNumber = 500;
		//	double connectionProbability = 0.078;

		// _____________________________________________________________________

		GraphInterface<LtsState,LtsAction> graph;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>(new GraphItemFactory());
		
		LtsAction[] actions = new LtsAction[actionCount];
		for( int i=0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );
		
		boolean ptRes, mptRes, ksRes, tapas1Res, rankRes, ksoRes;
		// make compiler happy
		ptRes = mptRes = ksRes = tapas1Res = rankRes = ksoRes = true; 
		double totalTime=0;
		double genTime=0;
		double ptTime=0, mptTime=0, ksTime=0, tapas1Time=0, rankTime=0, ksoTime=0;
		double start;
		double initTime = System.currentTimeMillis();
		Evaluator<LtsState,?> map;
		for( int i=0; i<repetitions; ++i ) {							
			if(i%printEvery==0)
				System.out.print(i + ": ");
			
			// generate random graph (according to settings)			
			start = System.currentTimeMillis();
			pointedGraph = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph = pointedGraph.getGraph();
			genTime += System.currentTimeMillis() - start;
			
			// KS
			if( ks ) {
				start = System.currentTimeMillis();
				KSChecker<LtsState,LtsAction> ksChecker = 
					new KSChecker<LtsState,LtsAction>(graph);
				map = ksChecker.computeEquivalence(null);
				ksRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				ksTime += System.currentTimeMillis() - start;
			}
				
			// PT	
			if( pt ) {
				start = System.currentTimeMillis();
				PTChecker<LtsState,LtsAction> ptChecker = 
					new PTChecker<LtsState,LtsAction>(graph);
				map = ptChecker.computeEquivalence(null);
				ptRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				ptTime += System.currentTimeMillis() - start;
			}
			
			// MPT	
			if( mpt ) {
				start = System.currentTimeMillis();
				MPTChecker<LtsState,LtsAction> mptChecker = 
					new MPTChecker<LtsState,LtsAction>(mpt_degree, graph);
				map = mptChecker.computeEquivalence(null);
				mptRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				mptTime += System.currentTimeMillis() - start;
			}
			
			// RANK-BASED
			if( rank ) {
				start = System.currentTimeMillis();
				RankBasedChecker<LtsState,LtsAction> rankChecker = 
					new RankBasedChecker<LtsState,LtsAction>(graph);
				map = rankChecker.computeEquivalence(null);
				rankRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				rankTime += System.currentTimeMillis() - start;
			}

			// KSO
			if( kso ) {
				start = System.currentTimeMillis();
				KSOChecker<LtsState,LtsAction> ksoChecker = 
					new KSOChecker<LtsState,LtsAction>(graph);
				map = ksoChecker.computeEquivalence(null);
				ksoRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				ksoTime += System.currentTimeMillis() - start;
			}
			
			//	Tapas1, solo controllo
			//	if( tapas1 ) {
			//		start = System.currentTimeMillis();
			//		TBisimulationChecker<LtsState,LtsAction> checker = 
			//			new TBisimulationChecker<LtsState,LtsAction>();
			//		checker.setParameters(
			//			graph, graph, 
			//			pointedGraph.getState1(), pointedGraph.getState2(), 
			//			false
			//		); 
			//		tapas1Res = checker.checkEquivalence();
			//		tapas1Time += System.currentTimeMillis() - start;
			//	}

			// Tapas1, calcolo della partizione
			if( tapas1 ) {
				start = System.currentTimeMillis();
				Lts lts = Lts.createProcess(
					graph, pointedGraph.getState1(), pointedGraph.getState2()
				);
				PTPartition initialRo = Bisimulation.initPTPartition(lts);
				Partition partition = 
					Bisimulation.computePTPartitionRefinement(lts, initialRo);
				tapas1Res = partition.checkSameClass(
					lts.getInitState(), lts.getSecondInitState()
				);
				tapas1Time += System.currentTimeMillis() - start;
			}

			// check equality of results
			if( kso && ksoRes != ksRes ) {
				System.out.println("Errore: kso");
				System.out.println( graph.toString() );
				break;
			}
			if( pt && ptRes != ksRes ) {
				System.out.println("Errore: pt");
				System.out.println("pt: " + ptRes);
				System.out.println("ks: " + ksRes);
				System.out.println( "point1: " + pointedGraph.getState1() );
				System.out.println( "point2: " + pointedGraph.getState2() );
				System.out.println( graph.toString() );
				break;
			}
			if( mpt && mptRes != ksRes ) {
				System.out.println("Errore: mpt");
				System.out.println("mpt: " + mptRes);
				System.out.println("ks: " + ksRes);
				System.out.println( "point1: " + pointedGraph.getState1() );
				System.out.println( "point2: " + pointedGraph.getState2() );
				System.out.println( graph.toString() );
				break;
			}
			if( rank && rankRes != ksRes ) {
				System.out.println("Errore: rank based");
				System.out.println("rank: " + rankRes);
				System.out.println("ks: " + ksRes);
				System.out.println( graph.toString() );
				System.out.println( "point1: " + pointedGraph.getState1() );
				System.out.println( "point2: " + pointedGraph.getState2() );
				break;
			}
			if( tapas1 &&  tapas1Res != ksRes) {
				System.out.println("Errore: ks");
				System.out.println( graph.toString() );
				break;
			}
			
			if(i%printEvery==0)
				System.out.println( ksRes ? "True" : "False");						
				
			System.gc();
		}
		totalTime = System.currentTimeMillis() - initTime;
				
		// Print results
		System.out.println( );			
		System.out.println( "total: " + totalTime );			
		System.out.println( "generation: " + genTime );			
		System.out.println( );			
		if( tapas1 )
			System.out.println( "tapas1: " + tapas1Time );			
		if( ks )
			System.out.println( "ks: " + ksTime );			
		if( kso )
			System.out.println( "kso: " + ksoTime );			
		if( pt )
			System.out.println( "pt: " + ptTime );			
		if( mpt )
			System.out.println( "mpt: " + mptTime );			
		if( rank )
			System.out.println( "rank: " + rankTime );			
		
	}	
	
}


//	clazz.siblings: [[7, 6], [2]]
//	clazz: []
//	clazzCount: 2
//	Stati: 12
//	Archi: 35
//	0	0 0
//	0	0 10
//	1	1 3
//	1	0 4
//	1	0 6
//	1	0 7
//	1	0 10
//	2	0 5
//	2	0 7
//	2	1 9
//	2	1 10
//	3	1 0
//	3	0 2
//	4	0 2
//	4	1 5
//	4	1 8
//	4	0 9
//	5	1 1
//	5	0 3
//	5	1 5
//	5	0 10
//	6	0 6
//	6	1 11
//	7	1 1
//	7	0 3
//	7	1 5
//	7	1 11
//	9	0 4
//	9	1 6
//	10	0 6
//	10	0 7
//	10	1 8
//	11	0 1
//	11	1 4
//	11	0 10

















/**
 */
package org.lic.secsi;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.lic.secsi.SecsiPackage#getType()
 * @model
 * @generated
 */
public interface Type extends EObject
{
} // Type

package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;
import org.cmg.tapas.testGraph.algorithms.tapas1.ActionsSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.HMLUFormula;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.State;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.TransitionCouple;


/**
 * @author Gori Massimiliano
 **/
public class BBUtil {

	public static void whyNotEquivalent(
		BBLogSplitting log1,
 		BBLogInfo log2,
 		BBState s1, 
 		BBState s2,
 		String nome1,
 		String nome2,
 		Lts p
 	) {
 		java.io.PrintStream console = System.out;

		console.append("because "+nome1+" satisfies the formula:\n    ");
		console.flush();
		HMLUFormula formula = BBUtil.computeCounterExample(
			log1, log2, s1.getState(), s2.getState(), p
		);
		formula.print(console);
		console.append("\nwhereas "+nome2+" does not");			
		console.flush();
	}

	// (tutta la procedura ha uno stile veramente orrendo)
	private static HMLUFormula computeCounterExample(
		BBLogSplitting log1,
		BBLogInfo log2,
		State s1, 
		State s2,
		Lts p
	) { 
		BBLogSplittingEntry e1 = null;
		BBLogInfoEntry e2 = null;
		BBBloc bb1 = null;
		BBBloc bb2 = null;
		Vector<BBLogInfoEntry> info = null;
		boolean isS1 = false;
		StatesSet bloccoB = null;		
		int i,j,k = 0;
		boolean bb1not = false;
		//boolean bb2not = false;
		StatesSet ssl = null;
		StatesSet ssr = null;
		StatesSet ssrEpsilon = null;
		StatesSet SL = null;
		StatesSet SR = null;
		State untilStateSL = null;
		State untilStateSR = null;
		BBStatesSetBottom bbstatesSetBottom = null;
		BBStatesSetNotBottom bbstatesSetNotBottom = null;

		HMLUFormula formula = null;
		HMLUFormula temp = null;
		HMLUFormula head = null;
		Vector<HMLUFormula> queue = new Vector<HMLUFormula>();
		// boolean trovato = false;
		boolean until = false;
		
		
		// Cerco la causa dello splitting in LogSplitting
		e1 = log1.getDeppestCause(s1,s2);
		if( e1==null) 
			return null;
			
		// Recupero i blocchi che contengono s1 e s2, rispettivamente bb1 e bb2
		bb1 = e1.getBBBloc1();
		bb2 = e1.getBBBloc2();
		if( !bb1.containState(s1) ) {
			BBBloc bbblocTemp = bb1;
			bb1 = bb2;
			bb2 = bbblocTemp;
		}

		// Cerco in LogInfo il bloccoB rispetto al 
		// quale sono stati splittati bb1 e bb2
		info = log2.getInfo(bb1,bb2,e1.getCause());
		e2 = info.elementAt(0);
		bb1not = e2.isNot();
		e2 = info.elementAt(1);
		// bb2not = 
        e2.isNot();
		bloccoB = new StatesSet();
		bbstatesSetBottom = e2.getDestination().getBBStatesSetBottom();
		for( i=0; i<bbstatesSetBottom.getDimension(); i++ )
			bloccoB.addState(bbstatesSetBottom.bbstateAt(i).getState());
		bbstatesSetNotBottom = e2.getDestination().getBBStatesSetNotBottom();
		for( i=0; i<bbstatesSetNotBottom.getDimension(); i++ )
			bloccoB.addState(bbstatesSetNotBottom.bbstateAt(i).getState());
		isS1 = !bb1not;
		
		// Calcolo gli insiemi ssl e ssr
		ssl = new StatesSet();
		ssr = new StatesSet();
		bbstatesSetBottom = bb1.getBBStatesSetBottom();
		for (i=0; i<bbstatesSetBottom.getDimension(); i++)
			ssl.addState(bbstatesSetBottom.bbstateAt(i).getState());
		bbstatesSetNotBottom = bb1.getBBStatesSetNotBottom();
		for (i=0; i<bbstatesSetNotBottom.getDimension(); i++)
			ssl.addState(bbstatesSetNotBottom.bbstateAt(i).getState());
		bbstatesSetBottom = bb2.getBBStatesSetBottom();
		for (i=0; i<bbstatesSetBottom.getDimension(); i++)
			ssr.addState(bbstatesSetBottom.bbstateAt(i).getState());
		bbstatesSetNotBottom = bb2.getBBStatesSetNotBottom();
		for (i=0; i<bbstatesSetNotBottom.getDimension(); i++)
			ssr.addState(bbstatesSetNotBottom.bbstateAt(i).getState());				
		if( !isS1 ) {				
			StatesSet statesSetTemp = ssl;
			ssl = ssr;
			ssr = statesSetTemp;
		}
		ssrEpsilon = ssr.sEpsilon();
	
		// Calcolo gli insiemi SL e SR
		SL = bloccoB.intersection(ssl.dP(e1.getCause()));
		SR = ssrEpsilon.dP(e1.getCause());

		//	console.append("\nSL: ");
		//	SL.print(console);
		//	console.append(" SR: ");
		//	SR.print(console);
		
		// Condizione di Ricorsione
		if( SR.getDimension() != 0 ) {			
			// per ogni sl in SL e sr in SR 
			// calcolo delta(sl,sr) e la aggiungo a gamma
			temp = new HMLUFormula(new Action(""),true);
			until = false;
			for (i=0; i<SL.getDimension(); i++) {
				for (j=0; j<SR.getDimension(); j++) {
					if( !temp.containCoupleSatisfied(
						SL.stateAt(i),SR.stateAt(j)
					)) {
						formula = computeCounterExample(							
							log1, log2, SL.stateAt(i), SR.stateAt(j),p 
						);
						if (formula==null) {
							//	console.append("non trovato!!!\n");
							untilStateSL = SL.stateAt(i);
							untilStateSR = SR.stateAt(j);
							until = true;
							queue = new Vector<HMLUFormula>();
							break;
						}

						for (k=0; k<formula.getCouplesSatisfiedDimension(); k++)
							if( !temp.containCoupleSatisfied(
										formula.getCoupleSatisfiedAt(k)
							))
								temp.addCoupleSatisfied(
									formula.getCoupleSatisfiedAt(k)
							);
						queue.addElement(formula);
					}
				}
				
				if(until)
					break;
			}
							
			// Comincio a costruire la formula dell'until
			if(until) {
				// costruisco i nuovi SL e SR
				SL = new StatesSet();
				for(i=0; i<ssl.getDimension(); i++)
					if( ssl.stateAt(i).dP(e1.getCause()).containState(
						untilStateSL
					))
						SL.addState(ssl.stateAt(i));
				SR = new StatesSet();
				for(i=0; i<ssrEpsilon.getDimension(); i++) {
					if(isS1) {
						if( ssrEpsilon.stateAt(i) != s2 ) 
							if( 
								ssrEpsilon.stateAt(i).sEpsilon().
								dP(e1.getCause()).containState(untilStateSR)
							)
								SR.addState(ssrEpsilon.stateAt(i));
					} else {
						if( ssrEpsilon.stateAt(i) != s1 ) 
							if( 
								ssrEpsilon.stateAt(i).sEpsilon().
								dP(e1.getCause()).containState(untilStateSR)
							)
								SR.addState(ssrEpsilon.stateAt(i));
					}
				}
				//	console.append("\nSL-New: ");
				//	SL.print(console);
				//	console.append(" SR-New: ");
				//	SR.print(console);
				until = false;
				for(i=0; i<SL.getDimension(); i++) {
					for(j=0; j<SR.getDimension(); j++) {
						formula = computeCounterExample(
							log1, log2, SL.stateAt(i), SR.stateAt(j), p
						);
						if( formula != null ) {
							head = formula;
							until = true;
							break;
						}
					}
					if(until) break;
				}
			}	//fine costruzione until

			//	else {//riduzione gamma
			//		i = 0;
			//		while (true) {
			//			formula = queue.elementAt(i);
			//			trovato = false;
			//			for( j=0; j<SR.getDimension(); j++ ) {
			//				if( !formula.satisfy(SR.stateAt(j),p) ) {
			//					trovato = true;
			//					for( k=0; k<queue.size(); k++ ) {
			//						if(k!=i) {
			//							temp = queue.elementAt(k);
			//							if( !temp.satisfy(SR.stateAt(j),p) ) {
			//								trovato = false;
			//								break;	
			//							}
			//						}
			//					}
			//				}
			//				if (trovato) break;
			//			}
			//			if(!trovato) 
			//				queue.removeElementAt(i); // posso scartare la formula	
			//			else 
			//				i++;
			//				
			//			if( i>= queue.size() ) 
			//				break;
			//		}
			//	}
		
		} //fine condizione di ricorsione
		
		// costruisco la formula finale
		formula = new HMLUFormula(e1.getCause(),!isS1);
		
		if(until) {
			formula.setHead(head);				
		} else {
			for (i=0; i<queue.size(); i++)
				formula.addQueue( queue.elementAt(i) );
		}
		
		if (isS1) {
			for (i=0; i<ssl.getDimension(); i++)
				for (j=0; j<ssr.getDimension(); j++)
					formula.addCoupleSatisfied(ssl.stateAt(i),ssr.stateAt(j));
		} else {
			for (i=0; i<ssl.getDimension(); i++)
				for (j=0; j<ssr.getDimension(); j++)
					formula.addCoupleSatisfied(ssr.stateAt(j),ssl.stateAt(i));
		}
		return formula;
	}
	

	public static BBPartition initBBPartition(Lts process) {
		State state = null;
		StatesSet destinations = null;
		Vector<TransitionCouple> transitionCouples = null;
		TransitionCouple tc = null;
		BBState bbstate = null;
		BBBloc bbbloc = null;
		NotInertTransition notInertT = null;
		HashMap<State,BBState> hm = new LinkedHashMap<State,BBState>();
		BBPartition p = null;
		Iterator<BBState> iter = null;
		int j,i,k;

		StatesSet statesSet = process.getStatesSet();
	
		// creo gli stati BBState
		for (i=0; i<statesSet.getDimension(); i++) {
			bbstate = new BBState(statesSet.stateAt(i));
			hm.put(bbstate.getState(), bbstate);
		}
		
		// creo le liste inert degli stati
		// e le liste Bottom e NotBottom del nuovo blocco
		bbbloc = new BBBloc();
		iter = hm.values().iterator();
		while (iter.hasNext()) {
			bbstate = iter.next();
			destinations = bbstate.getState().dP( Action.ACTION_TAU );
			j=0;
			while (j<destinations.getDimension()) {
				state = destinations.stateAt(j);
				bbstate.addInert( hm.get(state) );
				j++;
			}
			if(j==0) 
				bbbloc.addBBStateBottom(bbstate);
			else 
				bbbloc.addBBStateNotBottom(bbstate);
		}

		// creo la lista In del Blocco
		for (i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				if( !tc.getAction().isTau() ) {
					destinations = tc.getDestinationsSet();
					for (k=0; k<destinations.getDimension(); k++) {
						notInertT = new NotInertTransition(
							hm.get(statesSet.stateAt(i)), 
							tc.getAction(),
							hm.get(destinations.stateAt(k))
						);
						bbbloc.addNotInertTransition(notInertT);					
					}
				}
			}
		}

		
		// creo la partizione		
		p = new BBPartition(bbbloc);

		return p;
	}	


	public static boolean split(
		final BBBloc b, final BBBloc b1, final BBBloc b2
	) {
		BBState nonultimo = null;
		NotInertTransition t = null;
		boolean inb1 = false;
		boolean changed = false;
		int start = 0;
		int end = 0;
		Inert inert = null;
		
		// Scandisco gli stati ultimi
		BBStatesSetBottom ultimi = b.getBBStatesSetBottom();
		for(int i=0; i<ultimi.getDimension(); i++) {
			if( ultimi.bbstateAt(i).isMark() ) 
				b1.addBBStateBottom(ultimi.bbstateAt(i));
			else 
				b2.addBBStateBottom(ultimi.bbstateAt(i));
		}
			
		// Scandisco gli stati non ultimi
		BBStatesSetNotBottom nonultimi = b.getBBStatesSetNotBottom();
		for (int i=0; i<nonultimi.getDimension(); i++) {
			nonultimo = nonultimi.bbstateAt(i);
			inb1 = false;
			if( !nonultimo.isMark() ) {
				inert = nonultimo.getInert();
				for(int j=0; j<inert.getDimension(); j++) {
					if( inert.inertTransitionAt(j).getOwner() == b1 ) {
						inb1 = true;
						break;	
					}
				}
			} else {
				inb1 = true;
			}
			
			if (inb1) {
				inert = nonultimo.getInert();
				start = 0;
				end = inert.getDimension();
				while (start<end)
					if (inert.inertTransitionAt(start).getOwner() == b2) {
						t = new NotInertTransition(
							nonultimo, 
							Action.ACTION_TAU,
							inert.removeInertTransitionAt(start)
						);	
						b2.addNotInertTransition(t);
						end--;
					} else {
						start++;
					}
					
				if(end == 0) {
					b1.addBBStateBottom(nonultimo);
					changed = true;
				} else {
					b1.addBBStateNotBottom(nonultimo);
				}
			} else {
				b2.addBBStateNotBottom(nonultimo);  
			}
		}
		
		// Scandisco In(B)
		In in = b.getIn();
		for (int i=0; i<in.getDimension(); i++) {
			if( in.notInertTransitionAt(i).getDestination().getOwner() == b1 )
				b1.addNotInertTransition(in.notInertTransitionAt(i));
			else 
				b2.addNotInertTransition(in.notInertTransitionAt(i));
		}
		
		//Ritorno se qualche stato non ultimo � diventato ultimo
		return changed;
	}


	public static boolean canBeSplitted(BBBloc b) {
		BBStatesSetBottom ultimi = b.getBBStatesSetBottom();
		for (int i=0; i<ultimi.getDimension(); i++)
			if( !ultimi.bbstateAt(i).isMark() )
				return true;
				
		return false;		
	}

	public static void removeFromLists(
		BBBloc b, BBPartition working, BBPartition stable
	) {
		int i = working.getBBBlocIndex(b);
		if(i == -1) 
			stable.removeBBBloc(stable.getBBBlocIndex(b));
		else 
			working.removeBBBloc(i);
	}


	public static Partition computeBBPartitionRefinement(
		Lts process, 
		BBPartition p, 
		BBState initBBState1, 
		BBState initBBState2,
		BBLogSplitting log1,
		BBLogInfo log2		
	) {	
		ActionsSet act = process.getActionsSet();
		BBPartition stable = new BBPartition();
		BBPartition working = new BBPartition(p.bbblocAt(0));
		Vector<BBBloc> pre = new Vector<BBBloc>();
		BBBloc sp = null;
		BBBloc b = null;
		BBBloc b1 = null;
		BBBloc b2 = null;
		NotInertTransition t = null;
		Vector<NotInertTransition> notInertTransitions = null;
		In inSp = null;
		boolean splitted = false;
		boolean changed = false;

		while( !working.isEmpty() ) {
			// Scelgo il blocco da processare SP
			sp = working.getFirstBBBloc();
			pre = new Vector<BBBloc>();
			inSp = sp.getIn();
			splitted = false;
			changed = false;
			for (int i=0; i<act.getDimension(); i++) {					
				// Scandisco la lista delle a-transizioni in In(SP)
				// e setto i mark degli stati origine e creo la lista pre
				notInertTransitions = inSp.getNotInertTransitionsWith(
					act.actionAt(i)
				);
				for (int j=0; j<notInertTransitions.size(); j++) {
					t = notInertTransitions.elementAt(j);
					t.getOrigin().setMark(true);
					if( !t.getOrigin().getOwner().isMark() ) {
						t.getOrigin().getOwner().setMark(true);
						pre.add(t.getOrigin().getOwner());
					}
				}

				// Scandisco tutta la lista pre
				while( pre.size()>0 ) {
					b = pre.remove(0);				
					
					// se SP � uno splitter per B appartenente a pre	
					if( canBeSplitted(b) ) {
						if(sp == b) 
							splitted = true;
						b.getListOwner().removeBBBloc(b);
						b1 = new BBBloc();
						b2 = new BBBloc();
						working.addBBBloc(b1);
						working.addBBBloc(b2);
						if (split(b,b1,b2)) {
							changed = true;
						}						
						if(log1!=null) 
							log1.addEntry(
								new BBLogSplittingEntry(b,b1,b2,act.actionAt(i))
							);
						if(log2!=null) {
							log2.addEntry(
								new BBLogInfoEntry(b1,act.actionAt(i),sp,false)
							);
							log2.addEntry(
								new BBLogInfoEntry(b2,act.actionAt(i),sp,true)
							);
						}
					}
				}
				
				// resetto tutti i flag dei blocchi e degli stati
				for (int j=0; j<notInertTransitions.size(); j++) {
					t = notInertTransitions.elementAt(j);
					t.getOrigin().setMark(false);
					t.getOrigin().getOwner().setMark(false);
				}
				
				// Blocco di uscita anticipata
				if (initBBState1.getOwner() != initBBState2.getOwner()) {
					working.takeFrom(stable);
					return working.toPartition();
				}
				
				// se SP stesso � stato splittato non � il caso di continuare	
				if(splitted) 
					break;					
			}

			if( changed ) 
				working.takeFrom(stable);
			else if( !splitted ) 
				stable.addBBBloc(working.removeBBBloc(sp));
		}			
		
		return stable.toPartition();
	}	

	public static Partition computeBBPartitionRefinement(
		Lts process, 
		BBPartition p
	) {	
		ActionsSet act = process.getActionsSet();
		BBPartition stable = new BBPartition();
		BBPartition working = new BBPartition(p.bbblocAt(0));
		Vector<BBBloc> pre = null;
		BBBloc sp = null;
		BBBloc b = null;
		BBBloc b1 = null;
		BBBloc b2 = null;
		NotInertTransition t = null;
		Vector<NotInertTransition> notInertTransitions = null;
		In inSp = null;
		boolean splitted = false;
		boolean changed = false;
		
		while ( !working.isEmpty() ) {
			
			// Scelgo il blocco da processare SP
			sp = working.getFirstBBBloc();
			pre = new Vector<BBBloc>();
			inSp = sp.getIn();
			splitted = false;
			changed = false;
			for (int i=0; i<act.getDimension(); i++) {					
				// Scandisco la lista delle a-transizioni in In(SP)
				// e setto i mark degli stati origine e creo la lista pre
				notInertTransitions = inSp.getNotInertTransitionsWith(
					act.actionAt(i)
				);
				for (int j=0; j<notInertTransitions.size(); j++) {
					t = notInertTransitions.elementAt(j);
					t.getOrigin().setMark(true);
					if( !t.getOrigin().getOwner().isMark() ) {
						t.getOrigin().getOwner().setMark(true);
						pre.add(t.getOrigin().getOwner());
					}
				}
				
				// Scandisco tutta la lista pre
				while (pre.size()>0) {
					b = pre.remove(0);				
					// se SP � uno splitter per B appartenente a pre	
					if (canBeSplitted(b)) {
						if(sp == b) 
							splitted = true;
						b.getListOwner().removeBBBloc(b);
						b1 = new BBBloc();
						b2 = new BBBloc();
						working.addBBBloc(b1);
						working.addBBBloc(b2);
						if (split(b,b1,b2)) {
							changed = true;
						}
					}
				}
				
				// resetto tutti i flag dei blocchi e degli stati
				for (int j=0; j<notInertTransitions.size(); j++) {
					t = notInertTransitions.elementAt(j);
					t.getOrigin().setMark(false);
					t.getOrigin().getOwner().setMark(false);
				}
					
				//se SP stesso � stato splittato non � il caso di continuare	
				if (splitted) 
					break;					
			}
			
			if (changed) 
				working.takeFrom(stable);
			else if (!splitted) 
				stable.addBBBloc(working.removeBBBloc(sp));
		}			
	
		return stable.toPartition();
	}

	public static Lts buildMinimizedProcess(
		Lts process, Partition ro
	) {
		State nuovo = null;
		StatesSet blocco = null;
		StatesSet ss = null;
		StatesSet destinations = null;
		Vector<TransitionCouple> transitionCouples = null;
		TransitionCouple tc = null;
		HashMap<State,State> h = new LinkedHashMap<State,State>();
		Lts result = new Lts();
				
		// costruisco l'insieme degli stati
		for (int i=0; i<ro.getDimension(); i++) {
			nuovo = result.addNewState();
			blocco = ro.statesSetAt(i);
			for (int j=0; j<blocco.getDimension(); j++)
				h.put(blocco.stateAt(j), nuovo);
		}
		

		// costruisco la relazione di transizione e l'insieme delle azioni
		ss = process.getStatesSet();
		for (int i=0; i<ss.getDimension(); i++) {
			transitionCouples = ss.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				result.addAction(tc.getAction());
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++)
					h.get(ss.stateAt(i)).addTransition(
						tc.getAction(), h.get(destinations.stateAt(k))
					);
			}
		}

		// costruisco lo stato iniziale
		result.setInitState( h.get(process.getInitState()) );

		
		return result;
	}
	   
}
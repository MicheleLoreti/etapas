package org.cmg.tapas.clts.extensions;

import com.google.common.collect.Iterables;
import java.util.LinkedList;
import org.cmg.tapas.clts.extensions.CLTSGraphElementContentProvider;
import org.cmg.tapas.clts.extensions.CLTSGraphElementLabelProvider;
import org.cmg.tapas.extensions.TAPAsElementViewProvider;
import org.cmg.tapas.views.TAPAsGraphElementContentProvider;
import org.cmg.tapas.views.TAPAsGraphElementLabelProvider;
import org.cmg.tapas.xtext.clts.composedLts.Element;
import org.cmg.tapas.xtext.clts.composedLts.Lts;
import org.cmg.tapas.xtext.clts.composedLts.Model;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.InputOutput;

@SuppressWarnings("all")
public class CLTSElementViewProvider implements TAPAsElementViewProvider {
  private Model model;
  
  @Override
  public TAPAsGraphElementContentProvider getContentProvider() {
    return new CLTSGraphElementContentProvider();
  }
  
  @Override
  public String[] getElements() {
    LinkedList<String> toReturn = new LinkedList<String>();
    EList<Element> _elements = this.model.getElements();
    Iterable<Lts> _filter = Iterables.<Lts>filter(_elements, Lts.class);
    for (final Lts s : _filter) {
      String _name = s.getName();
      toReturn.add(_name);
    }
    InputOutput.<LinkedList<String>>print(toReturn);
    return ((String[])Conversions.unwrapArray(toReturn, String.class));
  }
  
  @Override
  public Object getInput(final String elementName) {
    throw new UnsupportedOperationException("TODO: auto-generated method stub");
  }
  
  @Override
  public TAPAsGraphElementLabelProvider getLabelProvier() {
    return new CLTSGraphElementLabelProvider();
  }
  
  @Override
  public void setModel(final EObject model) {
    if ((model instanceof Model)) {
      this.model = ((Model)model);
    } else {
      this.model = null;
    }
  }
}

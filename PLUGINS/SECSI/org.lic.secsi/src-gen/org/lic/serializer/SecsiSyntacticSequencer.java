package org.lic.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.lic.services.SecsiGrammarAccess;

@SuppressWarnings("all")
public class SecsiSyntacticSequencer extends AbstractSyntacticSequencer {

	protected SecsiGrammarAccess grammarAccess;
	protected AbstractElementAlias match_BaseExpression_LeftParenthesisKeyword_3_0_a;
	protected AbstractElementAlias match_BaseExpression_LeftParenthesisKeyword_3_0_p;
	protected AbstractElementAlias match_Term_FalseKeyword_3_or_TrueKeyword_2;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (SecsiGrammarAccess) access;
		match_BaseExpression_LeftParenthesisKeyword_3_0_a = new TokenAlias(true, true, grammarAccess.getBaseExpressionAccess().getLeftParenthesisKeyword_3_0());
		match_BaseExpression_LeftParenthesisKeyword_3_0_p = new TokenAlias(true, false, grammarAccess.getBaseExpressionAccess().getLeftParenthesisKeyword_3_0());
		match_Term_FalseKeyword_3_or_TrueKeyword_2 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getTermAccess().getFalseKeyword_3()), new TokenAlias(false, false, grammarAccess.getTermAccess().getTrueKeyword_2()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_BaseExpression_LeftParenthesisKeyword_3_0_a.equals(syntax))
				emit_BaseExpression_LeftParenthesisKeyword_3_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_BaseExpression_LeftParenthesisKeyword_3_0_p.equals(syntax))
				emit_BaseExpression_LeftParenthesisKeyword_3_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_Term_FalseKeyword_3_or_TrueKeyword_2.equals(syntax))
				emit_Term_FalseKeyword_3_or_TrueKeyword_2(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     '('*
	 */
	protected void emit_BaseExpression_LeftParenthesisKeyword_3_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '('+
	 */
	protected void emit_BaseExpression_LeftParenthesisKeyword_3_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'false' | 'true'
	 */
	protected void emit_Term_FalseKeyword_3_or_TrueKeyword_2(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}

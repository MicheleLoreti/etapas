package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

/**
 *
 */
public class Trace {
	private Vector<Action> traccia = null;
	
	
	public Trace() {
		traccia = new Vector<Action>();	
	}
	
	public void append(Action a) {
		traccia.add(a);
	}

	public int getDimension() {
		return traccia.size();	
	}	
	
	public Action actionAt(int index) {
		return traccia.elementAt(index);	
	}

	public void print(java.io.PrintStream console) {
		if( getDimension()==0 ) {
			console.println("epsilon");
		} else {
			for( int i=0; i<this.getDimension(); i++ ) {
				if(i>0) 
					console.println(".");	
				actionAt(i).print(console);
			}				
		}
	}	
}





















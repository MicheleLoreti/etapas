package org.cmg.tapas.testGraph.algorithms.traces;

import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.algorithms.traces.DecoratedTraceChecker;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;



/**
 * Test per gli algoritmi di verifica di equivalenze a tracce decorate.
 * Si tratta di un piccolo test che verifica solo
 * un grafo specifico. Per test pi� intensivi
 * si usi DecoratedTraceTiming che oltre alle misurazioni
 * esegue una verifica dei risultati. 
 *
 * @author Guzman Tierno
 **/
public class DecoratedTraceTest {
	
    private static Graph<LtsState, LtsAction> lts1;
    //private static Graph<LtsState, LtsAction> lts2;
	private static LtsState A0, A1, A2, A3, A4, A5, A6, A7, A8;
	private static LtsState B0, B1, B2, B3, B4, B5, B6, B7, B8;//, s8, s9;
	private static LtsAction a, b, c;
//	private static TransitionTriple<LtsState, LtsAction> 
//		t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14;
//	
    private static void setUp() {  	
    	lts1 = new Graph<LtsState, LtsAction>();

    	A0 = new LtsState("A0");
    	A1 = new LtsState("A1");
    	A2 = new LtsState("A2");
    	A3 = new LtsState("A3");
    	A4 = new LtsState("A4");
    	A5 = new LtsState("A5");
    	A6 = new LtsState("A6");
    	A7 = new LtsState("A7");
    	A8 = new LtsState("A8");
    	
    	B0 = new LtsState("B0");
    	B1 = new LtsState("B1");
    	B2 = new LtsState("B2");
    	B3 = new LtsState("B3");
    	B4 = new LtsState("B4");
    	B5 = new LtsState("B5");
    	B6 = new LtsState("B6");
    	B7 = new LtsState("B7");
    	B8 = new LtsState("B8");
    	
//    	s5 = new LtsState("5");
//    	s6 = new LtsState("6");
//    	s7 = new LtsState("7");
//    	s8 = new LtsState("8");
//    	s9 = new LtsState("9");
//
    	a = new LtsAction("a");
    	b = new LtsAction("b");
    	c = new LtsAction("c");
    	
    	lts1.addState(A0);
    	lts1.addState(A1);
    	lts1.addState(A2);
    	lts1.addState(A3);
    	lts1.addState(A4);
    	lts1.addState(A5);
    	lts1.addState(A6);
    	lts1.addState(A7);
    	lts1.addState(A8);

    	lts1.addState(B0);
    	lts1.addState(B1);
    	lts1.addState(B2);
    	lts1.addState(B3);
    	lts1.addState(B4);
    	lts1.addState(B5);
    	lts1.addState(B6);
    	lts1.addState(B7);
    	lts1.addState(B8);
    	
    	lts1.addEdge(A0, a, A1);
    	lts1.addEdge(A0, a, A2);
    	lts1.addEdge(A2, a, A3);
    	lts1.addEdge(A2, a, A4);
    	lts1.addEdge(A3, b, A5);
    	lts1.addEdge(A3, c, A6);
    	lts1.addEdge(A4, a, A7);    	
    	
    	lts1.addEdge(B0, a, B1);
    	lts1.addEdge(B0, a, B2);
    	lts1.addEdge(B2, a, B3);
    	lts1.addEdge(B2, a, B4);
    	lts1.addEdge(B3, b, B5);
    	lts1.addEdge(B4, c, B6);
    	
    	
//    	LtsAction atau = LtsAction.TAU;
//
    }

    	public static void main(String[] args) {
		setUp();		
		LtsState init1 = A0;
		LtsState init2 = B0;

		boolean tapas2Res;
		
		// Tapas2
		DecoratedTraceChecker<LtsState,LtsAction> tapas2Checker = 
			new DecoratedTraceChecker<LtsState,LtsAction>(
				lts1, DecoratedTraceChecker.MUST_W
			);
		tapas2Res = tapas2Checker.checkEquivalence( init1, init2 );
		
		System.out.println( "RESULT: "+tapas2Res );

	}	
	
}





package org.cmg.tapas.testGraph.algorithms.regression;


/** 
 * Calcola, col metodo dei minimi quadrati,
 * dipendenze quadratiche tra due vettori di valori.
 *
 * @author Guzman Tierno
 **/
public class Regression2 {
	// variabile
	private double[] x;		 
	// valori
	private double[] y; 

	private double A;
	private double B;
	private double C;
	private int regType;
	
	public static final int QUADRATIC = 0; 			// y = A*x^2 + B*x + C
	
	public Regression2(double[] vx, double[] vy,int regType) {
		x = vx;
		y = vy;
		
		this.regType = regType;		
		
		int s0 = x.length;
		double s1 = sum(x,1);							 
		double s2 = sum(x,2);  
		double s3 = sum(x,3);
		double s4 = sum(x,4);
		
		double t1 = sum(y,1);
		double t2 = sum2(x,y,1);
		double t3 = sum2(x,y,2);

		
		double det = det3(	s4, s3, s2,
							s3, s2, s1,
							s2, s1, s0	);
		
		A = det3(	t3, s3, s2,
					t2, s2, s1,
					t1, s1, s0	) 
			/ det;
		
		B = det3(	s4, t3, s2,
					s3, t2, s1,
					s2, t1, s0	)
			/ det;		
					
		C = det3(	s4, s3, t3,
					s3, s2, t2,
					s2, s1, t1	)
			/ det;
					
	}
	
	public double getType() {
		return regType;
	}

	public double getA() {
		return A;
	}

	public double getB() {
		return B;
	}

	public double getC() {
		return C;
	}

	public static double sum(double[] x, int k) {
		double s=0;
		for( int i=0; i<x.length; ++i ) 
			s += Math.pow(x[i],k);
		
		return s;
	}		
		
	public static double sum2(double[] x, double[] y, int k) {
		double s=0;
		for( int i=0; i<x.length; ++i ) 
			s += Math.pow(x[i],k) * y[i];
		
		return s;
	}		

	public static double det3(
		double x1, double x2, double x3,
		double x4, double x5, double x6,
		double x7, double x8, double x9
	) {
		return 
			  x1*x5*x9 + x2*x6*x7 + x4*x8*x3 
			- x3*x5*x7 - x4*x2*x9 - x1*x6*x8;
	}
		
	public static double[] ln(double[] x) {
		double[] r = new double[x.length];
		for( int i=0; i<x.length; ++i ) 
			r[i] = Math.log(x[i]);
		
		return r;
	}
	
	public static double[] div(double[] a,double[] b) {
		double[] r = new double[a.length];
		for( int i=0; i<a.length; ++i ) 
			r[i] = a[i]/b[i];
		
		return r;
	}
	
	@Override 
	public String toString() {
		String s = "";
		double tA = ((int)(A*100))/100.0;
		double tB = ((int)(B*100))/100.0;
		double tC = ((int)(C*100))/100.0;
		
		switch(regType) {
			case QUADRATIC: 
				s = "Dipendenza quadratica2, y = " 
					+ tA + " * x^2 + " + tB + " * x + " + tC;
			break;
		}	
				
		return s;		
	}
	
}	
		
		
		
		
	

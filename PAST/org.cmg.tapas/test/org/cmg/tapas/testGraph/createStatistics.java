package org.cmg.tapas.testGraph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;

public class createStatistics {
	
	private static final String PATH1 = "/home/francesco/workspaces/Eclipse/org.cmg.tapas/";
	private static final String PATH2 = "/home/francesco/workspaces/Eclipse/org.cmg.tapas.gui/";
	private static final String PATH3 = "/home/francesco/workspaces/Eclipse/org.cmg.tapas.ccs/";
	private static final String PATH4 = "/home/francesco/workspaces/Eclipse/org.cmg.tapas.pepa/";
	private static final String SRC = "src";
	private static final String TEST = "test";
	private static final String LIB = "lib";
	protected static final String JAVA = ".java";
	
	public static void main(String[] args) {		
		printProjectStatistic("KERNEL", PATH1);
		printProjectStatistic("GUI", PATH2);
		printProjectStatistic("CCS", PATH3);
		printProjectStatistic("PEPA", PATH4);

	}
	
	private static void printProjectStatistic(String title, String path){
		File f1 = new File(path+SRC);
		statistic s = createStatistic(f1);
		String str = "----"+title+"----\n";			
		str += "---SRC---\n"
				+"PATH: "+f1.getAbsolutePath()+"\n"
				+s+"\n---SRC---\n";		
//		System.out.println(str);
		
		f1 = new File(path+TEST);
		s = createStatistic(f1);
		str += "---TEST---\n"
			+"PATH: "+f1.getAbsolutePath()+"\n"
			+s+"\n---TEST---\n";
		str += "----"+title+"----\n";		
		System.out.println(str); 
	}
		
	private static statistic createStatistic(File dir){
		statistic res = new statistic();
		if(!dir.isDirectory())
			return res;
		
		//TODO Count lib		
		
		//Count files package & .java & code lines
		FileFilter filter = new FileFilter() {
			public boolean accept(File pathname) {
				String str = pathname.getName();
				return str.endsWith(JAVA);
			}
		};
		File[] files = dir.listFiles(filter);
		if(files.length > 0)
			res.packageNum++; 
		
		for (File f : files) {
			res.filesNum++;
			res.add(countMethodsAndLine(f));
		}
		
		//Iterate over subdir
		FileFilter dirFilter = new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		};		
		files = dir.listFiles(dirFilter);
		for (File d : files) {
			statistic tmp = createStatistic(d);
			res.add(tmp);
		}		
		
		return res;
	}
	
	private static statistic countMethodsAndLine(File file){
		statistic res = new statistic();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String str = "";
			while(str != null){
				str = br.readLine();
				if(isMethodDeclaration(str))
					res.methodsNum++;
				
				res.linesNum++;
			}
		} catch (Exception e) {
			return res;
		}
		return res;
	}	

	private static boolean isMethodDeclaration(String str) {
		if(!str.contains("("))
			return false;
		
		if(str.contains("public")
				|| str.contains("protected")
				|| str.contains("private"))
			return true;
		
		return false;
	}
}

class statistic {
	 protected int packageNum = 0;
	 protected int filesNum = 0;
	 protected int methodsNum = 0;
	 protected int linesNum = 0;
	 
	 protected void add(statistic s){
		 packageNum += s.packageNum;
		 filesNum += s.filesNum;
		 methodsNum += s.methodsNum;
		 linesNum += s.linesNum;
	 }
	 
	 public String toString(){
		 String res = "package: "+packageNum
					+"\nfiles: "+filesNum
		 			+"\nmethods: "+methodsNum
		 			+"\nlines: "+linesNum;
		 return res;
	 }
}

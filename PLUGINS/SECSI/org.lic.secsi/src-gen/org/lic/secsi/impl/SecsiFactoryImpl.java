/**
 */
package org.lic.secsi.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.lic.secsi.Action;
import org.lic.secsi.ActionModality;
import org.lic.secsi.Agent;
import org.lic.secsi.AndExpression;
import org.lic.secsi.Assignment;
import org.lic.secsi.Block;
import org.lic.secsi.BoolConstant;
import org.lic.secsi.BoolValue;
import org.lic.secsi.BooleanType;
import org.lic.secsi.Channel;
import org.lic.secsi.ChannelsDeclaration;
import org.lic.secsi.Constant;
import org.lic.secsi.Expression;
import org.lic.secsi.IfThenElse;
import org.lic.secsi.IntConstant;
import org.lic.secsi.IntegerType;
import org.lic.secsi.MulExpression;
import org.lic.secsi.Negation;
import org.lic.secsi.NotProp;
import org.lic.secsi.OrExpression;
import org.lic.secsi.OrProp;
import org.lic.secsi.Par;
import org.lic.secsi.Proc;
import org.lic.secsi.Program;
import org.lic.secsi.Prop;
import org.lic.secsi.PropBody;
import org.lic.secsi.PropList;
import org.lic.secsi.ReferenceInExpression;
import org.lic.secsi.ReferenceableElement;
import org.lic.secsi.Relation;
import org.lic.secsi.Relop;
import org.lic.secsi.Ren;
import org.lic.secsi.Res;
import org.lic.secsi.SecsiFactory;
import org.lic.secsi.SecsiPackage;
import org.lic.secsi.Signal;
import org.lic.secsi.Statement;
import org.lic.secsi.Sum;
import org.lic.secsi.SumExpression;
import org.lic.secsi.Switch_Case;
import org.lic.secsi.Type;
import org.lic.secsi.TypeDef;
import org.lic.secsi.TypeElement;
import org.lic.secsi.TypeExpression;
import org.lic.secsi.TypeReference;
import org.lic.secsi.VarDeclaration;
import org.lic.secsi.Variable;
import org.lic.secsi.While;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SecsiFactoryImpl extends EFactoryImpl implements SecsiFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SecsiFactory init()
  {
    try
    {
      SecsiFactory theSecsiFactory = (SecsiFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.lic.org/Secsi"); 
      if (theSecsiFactory != null)
      {
        return theSecsiFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SecsiFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SecsiFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case SecsiPackage.PROGRAM: return createProgram();
      case SecsiPackage.CONSTANT: return createConstant();
      case SecsiPackage.AGENT: return createAgent();
      case SecsiPackage.CHANNELS_DECLARATION: return createChannelsDeclaration();
      case SecsiPackage.CHANNEL: return createChannel();
      case SecsiPackage.TYPE: return createType();
      case SecsiPackage.TYPE_EXPRESSION: return createTypeExpression();
      case SecsiPackage.INTEGER_TYPE: return createIntegerType();
      case SecsiPackage.BOOLEAN_TYPE: return createBooleanType();
      case SecsiPackage.TYPE_REFERENCE: return createTypeReference();
      case SecsiPackage.BLOCK: return createBlock();
      case SecsiPackage.STATEMENT: return createStatement();
      case SecsiPackage.ACTION: return createAction();
      case SecsiPackage.SWITCH_CASE: return createSwitch_Case();
      case SecsiPackage.IF_THEN_ELSE: return createIfThenElse();
      case SecsiPackage.WHILE: return createWhile();
      case SecsiPackage.SYSTEM: return createSystem();
      case SecsiPackage.PAR: return createPar();
      case SecsiPackage.SUM: return createSum();
      case SecsiPackage.PROC: return createProc();
      case SecsiPackage.REN: return createRen();
      case SecsiPackage.RES: return createRes();
      case SecsiPackage.PROP_LIST: return createPropList();
      case SecsiPackage.PROP: return createProp();
      case SecsiPackage.PROP_BODY: return createPropBody();
      case SecsiPackage.NOT_PROP: return createNotProp();
      case SecsiPackage.EXPRESSION: return createExpression();
      case SecsiPackage.NEGATION: return createNegation();
      case SecsiPackage.REFERENCE_IN_EXPRESSION: return createReferenceInExpression();
      case SecsiPackage.REFERENCEABLE_ELEMENT: return createReferenceableElement();
      case SecsiPackage.TYPE_DEF: return createTypeDef();
      case SecsiPackage.TYPE_ELEMENT: return createTypeElement();
      case SecsiPackage.VARIABLE: return createVariable();
      case SecsiPackage.INT_CONSTANT: return createIntConstant();
      case SecsiPackage.BOOL_CONSTANT: return createBoolConstant();
      case SecsiPackage.VAR_DECLARATION: return createVarDeclaration();
      case SecsiPackage.ASSIGNMENT: return createAssignment();
      case SecsiPackage.SIGNAL: return createSignal();
      case SecsiPackage.OR_PROP: return createOrProp();
      case SecsiPackage.OR_EXPRESSION: return createOrExpression();
      case SecsiPackage.AND_EXPRESSION: return createAndExpression();
      case SecsiPackage.RELATION: return createRelation();
      case SecsiPackage.SUM_EXPRESSION: return createSumExpression();
      case SecsiPackage.MUL_EXPRESSION: return createMulExpression();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case SecsiPackage.ACTION_MODALITY:
        return createActionModalityFromString(eDataType, initialValue);
      case SecsiPackage.BOOL_VALUE:
        return createBoolValueFromString(eDataType, initialValue);
      case SecsiPackage.RELOP:
        return createRelopFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case SecsiPackage.ACTION_MODALITY:
        return convertActionModalityToString(eDataType, instanceValue);
      case SecsiPackage.BOOL_VALUE:
        return convertBoolValueToString(eDataType, instanceValue);
      case SecsiPackage.RELOP:
        return convertRelopToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Program createProgram()
  {
    ProgramImpl program = new ProgramImpl();
    return program;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Constant createConstant()
  {
    ConstantImpl constant = new ConstantImpl();
    return constant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Agent createAgent()
  {
    AgentImpl agent = new AgentImpl();
    return agent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ChannelsDeclaration createChannelsDeclaration()
  {
    ChannelsDeclarationImpl channelsDeclaration = new ChannelsDeclarationImpl();
    return channelsDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Channel createChannel()
  {
    ChannelImpl channel = new ChannelImpl();
    return channel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeExpression createTypeExpression()
  {
    TypeExpressionImpl typeExpression = new TypeExpressionImpl();
    return typeExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerType createIntegerType()
  {
    IntegerTypeImpl integerType = new IntegerTypeImpl();
    return integerType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanType createBooleanType()
  {
    BooleanTypeImpl booleanType = new BooleanTypeImpl();
    return booleanType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeReference createTypeReference()
  {
    TypeReferenceImpl typeReference = new TypeReferenceImpl();
    return typeReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Block createBlock()
  {
    BlockImpl block = new BlockImpl();
    return block;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action createAction()
  {
    ActionImpl action = new ActionImpl();
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Switch_Case createSwitch_Case()
  {
    Switch_CaseImpl switch_Case = new Switch_CaseImpl();
    return switch_Case;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IfThenElse createIfThenElse()
  {
    IfThenElseImpl ifThenElse = new IfThenElseImpl();
    return ifThenElse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public While createWhile()
  {
    WhileImpl while_ = new WhileImpl();
    return while_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.lic.secsi.System createSystem()
  {
    SystemImpl system = new SystemImpl();
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Par createPar()
  {
    ParImpl par = new ParImpl();
    return par;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Sum createSum()
  {
    SumImpl sum = new SumImpl();
    return sum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Proc createProc()
  {
    ProcImpl proc = new ProcImpl();
    return proc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Ren createRen()
  {
    RenImpl ren = new RenImpl();
    return ren;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Res createRes()
  {
    ResImpl res = new ResImpl();
    return res;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropList createPropList()
  {
    PropListImpl propList = new PropListImpl();
    return propList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Prop createProp()
  {
    PropImpl prop = new PropImpl();
    return prop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropBody createPropBody()
  {
    PropBodyImpl propBody = new PropBodyImpl();
    return propBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotProp createNotProp()
  {
    NotPropImpl notProp = new NotPropImpl();
    return notProp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression createExpression()
  {
    ExpressionImpl expression = new ExpressionImpl();
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Negation createNegation()
  {
    NegationImpl negation = new NegationImpl();
    return negation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferenceInExpression createReferenceInExpression()
  {
    ReferenceInExpressionImpl referenceInExpression = new ReferenceInExpressionImpl();
    return referenceInExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferenceableElement createReferenceableElement()
  {
    ReferenceableElementImpl referenceableElement = new ReferenceableElementImpl();
    return referenceableElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeDef createTypeDef()
  {
    TypeDefImpl typeDef = new TypeDefImpl();
    return typeDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TypeElement createTypeElement()
  {
    TypeElementImpl typeElement = new TypeElementImpl();
    return typeElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntConstant createIntConstant()
  {
    IntConstantImpl intConstant = new IntConstantImpl();
    return intConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolConstant createBoolConstant()
  {
    BoolConstantImpl boolConstant = new BoolConstantImpl();
    return boolConstant;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VarDeclaration createVarDeclaration()
  {
    VarDeclarationImpl varDeclaration = new VarDeclarationImpl();
    return varDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Assignment createAssignment()
  {
    AssignmentImpl assignment = new AssignmentImpl();
    return assignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Signal createSignal()
  {
    SignalImpl signal = new SignalImpl();
    return signal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrProp createOrProp()
  {
    OrPropImpl orProp = new OrPropImpl();
    return orProp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OrExpression createOrExpression()
  {
    OrExpressionImpl orExpression = new OrExpressionImpl();
    return orExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AndExpression createAndExpression()
  {
    AndExpressionImpl andExpression = new AndExpressionImpl();
    return andExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Relation createRelation()
  {
    RelationImpl relation = new RelationImpl();
    return relation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SumExpression createSumExpression()
  {
    SumExpressionImpl sumExpression = new SumExpressionImpl();
    return sumExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MulExpression createMulExpression()
  {
    MulExpressionImpl mulExpression = new MulExpressionImpl();
    return mulExpression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActionModality createActionModalityFromString(EDataType eDataType, String initialValue)
  {
    ActionModality result = ActionModality.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertActionModalityToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BoolValue createBoolValueFromString(EDataType eDataType, String initialValue)
  {
    BoolValue result = BoolValue.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertBoolValueToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Relop createRelopFromString(EDataType eDataType, String initialValue)
  {
    Relop result = Relop.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertRelopToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SecsiPackage getSecsiPackage()
  {
    return (SecsiPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SecsiPackage getPackage()
  {
    return SecsiPackage.eINSTANCE;
  }

} //SecsiFactoryImpl

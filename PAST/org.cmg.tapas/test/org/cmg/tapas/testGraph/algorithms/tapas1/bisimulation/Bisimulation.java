package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;


import java.util.HashMap;
import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;
import org.cmg.tapas.testGraph.algorithms.tapas1.ActionsSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.HMLFormula;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.State;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.TransitionCouple;

/**
 * Questa classe � stata ottenuta, come molte altre
 * in questo pacchetto, riadattano analoghe classi in TAPAs_1.
 * Calcola la bisimulazione per un grafo con 
 * un algoritmo che � un incrocio tra l'intuitivo e 
 * l'esageratamente complesso. Ne viene fuori un algoritmo
 * col costo medio m*n*log(n) e costo massimo m*n*n.
 * Ha il pregio di essere stata piuttosto testata e quindi
 * pu� essere usata come raffronto.
 *
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class Bisimulation {
	

	/** initPTPartition */
	public static PTPartition initPTPartition(
		Lts process
	) {
		StatesSet statesSet = process.getStatesSet();
		PTStatesSet ptstatesSet = new PTStatesSet();
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			ptstatesSet.addPTState( new PTState(statesSet.stateAt(i)) );
		} 
		
		Vector<TransitionCouple> transitionCouples;
		TransitionCouple tc;
		StatesSet destinations;
		PTState ptstate;
		PTState ptstatePre;
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			ptstatePre = ptstatesSet.getPTState(statesSet.stateAt(i));
			for( int j=0; j<transitionCouples.size(); j++ ) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for( int k=0; k<destinations.getDimension(); k++ ) {
					ptstate = ptstatesSet.getPTState(destinations.stateAt(k));
					ptstate.addPresetCouple(tc.getAction(), ptstatePre);
				}
			}
		}
		
		return new PTPartition(ptstatesSet);
	}	

	/** initSplittersSet */
	public static SplittersSet initSplittersSet(PTPartition p) {	
		SplittersSet x = new SplittersSet();
		
		for( int i=0; i<p.getDimension(); i++ )
			x.addSplitter( new Splitter(p.ptstatesSetAt(i)) );
		
		return x;
	}

	/** computeTaInvSet */
	public static void computeTaInvSet(
		PTStatesSet spStates, 
		Action a
	) {
		PTStatesSet tainv;
		for( int i=0; i<spStates.getDimension(); i++ ) {
			tainv = spStates.ptstateAt(i).getPTStatesPreset(a);	
			if( tainv!=null )
				for( int j=0; j<tainv.getDimension(); j++ )
					tainv.ptstateAt(j).incrementCount(spStates, a);
		}
	}


	/** computeTaInv */
	public static void computeTaInv(
		Splitter sp, 
		Action a
	) {
		if( sp.isSimple() ) {
			computeTaInvSet(sp.getRoot(), a);
		} else {
			computeTaInvSet(sp.getLeftChild(), a);
			computeTaInvSet(sp.getRightChild(), a);
		}			
	}


	/** split */
	public static PTPartition split(
		PTStatesSet b, 				// set to be splitted
		Action a, 					// splitting action
		Splitter sp, 				// splitter
		PTLogInfo log				// log for recording splitting
	) {
		// partition
		PTPartition p = new PTPartition();

		// splitter
		PTStatesSet spStates = sp.getRoot();

		// splitted parts of b
		PTStatesSet b1 = new PTStatesSet();
		PTStatesSet b2 = new PTStatesSet();
		PTStatesSet b3 = new PTStatesSet();
		
		// state
		PTState s;
		
		// simple splitter
		if( sp.isSimple() ) {
			for( int i=0; i<b.getDimension(); i++ ) {				
				s = b.ptstateAt(i);
				if( s.getCount(spStates,a)>0 )
					b1.addPTState(s);	
				else 
					b2.addPTState(s);
			}
			
			if( 										// no true splitting
				b1.getDimension()==b.getDimension() ||
				b2.getDimension()==b.getDimension()
			) {
				p.addPTStatesSet(b);
			} else {									// true splitting
				p.addPTStatesSet(b1);
				p.addPTStatesSet(b2);
				if( log!=null ) {						// record splitting
					log.addEntry(new PTLogInfoEntry(b1,a,spStates,null,false));
					log.addEntry(new PTLogInfoEntry(b2,a,spStates,null,true));
				}
			}
		}
		
		// compound splitter
		else {
			PTStatesSet sp1States = sp.getMinorChild();
			PTStatesSet sp2States = sp.getMajorChild();

			for( int i=0; i<b.getDimension(); i++) {				
				s = b.ptstateAt(i);
				if( s.getCount(sp1States,a)==s.getCount(spStates,a) ) 
					b1.addPTState(s);			
				else if( s.getCount(sp1States,a)==0 ) 
					b3.addPTState(s);
				else 
					b2.addPTState(s);
					
				s.removeCount(spStates,a);		// PROVA OTTIMIZZAZIONE
			}

			if(									// no true splitting
				b1.getDimension()==b.getDimension() ||
				b2.getDimension()==b.getDimension() ||
				b3.getDimension()==b.getDimension() 
			) {
				p.addPTStatesSet(b);			
			} else {							// true splitting
				if( b1.getDimension()>0 ) {
					p.addPTStatesSet(b1);
					if( log!=null ) 
						log.addEntry(new PTLogInfoEntry(b1,a,sp1States,null,false));
				}
				if( b2.getDimension()>0 ) {
					p.addPTStatesSet(b2);
					if( log!=null ) 
						log.addEntry(new PTLogInfoEntry(b2,a,sp1States,sp2States,false));
				}					
				if( b3.getDimension()>0 ) {
					p.addPTStatesSet(b3);
					if( log!=null ) 
						log.addEntry(new PTLogInfoEntry(b3,a,sp2States,null,false));
				}					
			}
		}
		return p;
	}


	/** updateSplittersSet */
	public static void updateSplittersSet(
		SplittersSet x, 
		PTStatesSet b, 
		PTPartition p1
	) {
		if( p1.getDimension()==1 )
			return;
		
		//Caso 1: 
		// X contiene B come simple splitter
		// sostituisco B con (B1 U B2) o (B1 U B2 U B3)
		Splitter sp;		
		for( int i=0; i<x.getDimension(); i++ ) {
			sp = x.splitterAt(i);
			if( sp.getRoot()==b && sp.isSimple() ) {
				x.removeSplitterAt(i);
				for( int j=0; j<p1.getDimension(); j++ )
					x.addSplitter( new Splitter(p1.ptstatesSetAt(j)) );
				return;   
			}
		}
		
		PTStatesSet b1 = p1.ptstatesSetAt(0);
		PTStatesSet b2 = p1.ptstatesSetAt(1);

		//Caso 2 o 3: 
		// X contiene B come foglia di un compound splitter o non lo contiene
		// Inserisco in X un compound splitter (B,B1,B2) o 
		// due compound splitter (B,B1,B2 U B3) e (B2 U B3,B2,B3)
		// a seconda che B sia stato splittato in (B1,B2) o (B1,B2,B3)
		if( p1.getDimension()==2 ) {
			x.addSplitter(new Splitter(b, b1, b2));
		} else {
			PTStatesSet b3 = p1.ptstatesSetAt(2);
			PTStatesSet b23 = new PTStatesSet();
			b23.addPTStatesSet(b2);
			b23.addPTStatesSet(b3);
			x.addSplitter(new Splitter(b, b1, b23));
			x.addSplitter(new Splitter(b23, b2, b3));
		}
	}
	

	/** computePTPartitionRefinement with log */
	public static Partition computePTPartitionRefinement(
		Lts process, 
		PTPartition p, 
		PTState initPTState1, 
		PTState initPTState2, 
		PTLogSplitting log1,
		PTLogInfo log2
	) {	
		PTPartition p1 = null;
		PTPartition temp = null;
		SplittersSet x = initSplittersSet(p);
		ActionsSet act = process.getActionsSet();
		Splitter sp = null;
		PTStatesSet b = null;

		//Blocco uscita anticipata
		if( !p.checkSameClass(initPTState1,initPTState2) ) {
			return p.toPartition();
		}
		
		//Algoritmo con output in modalit� normale
		while (!(x.isEmpty())) {
			sp = x.removeFirstSplitter();
			for (int i=0; i<act.getDimension(); i++) {
				computeTaInv( sp, act.actionAt(i) );
				temp = new PTPartition();
				while (!(p.isEmpty())) {	
					b = p.removeFirstPTStatesSet();
					if (b.getDimension() == 1) p1 = new PTPartition(b);
					else p1 = split(b, act.actionAt(i), sp, log2);
					if (b == p1.ptstatesSetAt(0)) temp.addPTStatesSet(b);
					else {
						if( log1!=null ) 
							log1.addEntry(new PTLogSplittingEntry(
								b,p1,act.actionAt(i)
						));
						for (int j=0; j<p1.getDimension(); j++)
							temp.addPTStatesSet(p1.ptstatesSetAt(j));
						updateSplittersSet(x, b, p1);
					}
				}
				p = temp;

				//Blocco uscita anticipata
				if( !p.checkSameClass(initPTState1,initPTState2) ) {
					return p.toPartition();
				}
			}
		}

		return p.toPartition();
	}	


	/** computePTPartitionRefinement */
	public static Partition computePTPartitionRefinement(
		Lts process, 
		PTPartition p
	) {	
		PTPartition p1;
		PTPartition temp;
		SplittersSet x = initSplittersSet(p);
		ActionsSet act = process.getActionsSet();
		Splitter sp;
		PTStatesSet b;

		while( !x.isEmpty() ) {
			sp = x.removeFirstSplitter();
			for( int i=0; i<act.getDimension(); i++ ) {
				computeTaInv( sp, act.actionAt(i) );
				temp = new PTPartition();
				while( !p.isEmpty() ) {			
					b = p.removeFirstPTStatesSet();
					if( b.getDimension()==1 ) {
						p1 = new PTPartition(b);
					} else {
						p1 = split(b, act.actionAt(i), sp, null);
					}
						
					if( b == p1.ptstatesSetAt(0) ) {
						temp.addPTStatesSet(b);
					} else {
						for( int j=0; j<p1.getDimension(); j++ )
							temp.addPTStatesSet( p1.ptstatesSetAt(j) );							
						updateSplittersSet(x, b, p1);
					}
				}
				p = temp;
			}
		}
		
		return p.toPartition();
	}	


	/** buildMinimizedProcess */
	public static Lts buildMinimizedProcess(
		Lts process, 
		Partition ro
	) {
		State nuovo = null;
		StatesSet blocco = null;
		StatesSet ss = null;
		StatesSet destinations = null;
		Vector<TransitionCouple> transitionCouples = null;
		TransitionCouple tc = null;
		HashMap<State,State> h = new HashMap<State,State>();
		Lts result = new Lts();
				
		//costruisco l'insieme degli stati
		for (int i=0; i<ro.getDimension(); i++) {
			nuovo = result.addNewState();
			blocco = ro.statesSetAt(i);
			for (int j=0; j<blocco.getDimension(); j++)
				h.put(blocco.stateAt(j), nuovo);
		}
		
		//costruisco la relazione di transizione e l'insieme delle azioni
		ss = process.getStatesSet();
		for (int i=0; i<ss.getDimension(); i++) {
			transitionCouples = ss.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				result.addAction(tc.getAction());
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++)
					h.get(ss.stateAt(i)).addTransition(
						tc.getAction(), 
						h.get(destinations.stateAt(k))
					);
			}
		}
		
		//costruisco lo stato iniziale
		result.setInitState( h.get(process.getInitState()) );

		
		return result;
	}

	/** whyNotEquivalent */
	public static void whyNotEquivalent(
		PTLogSplitting log1,
 		PTLogInfo log2,
 		PTState s1, 
 		PTState s2,
 		String nome1,
 		String nome2,
 		Lts p
 	) {
		// console.append("because "+nome1+" satisfies the formula:\n    ");
		// console.flush();
		// HMLFormula formula = 
        computeCounterExample(
			log1, log2, s1.getState(), s2.getState(), p
		);
		// formula.printWithBox(console);
		// console.append("\nwhereas "+nome2+" does not");				
	}
	
	/** computeCounterExample */
	private static HMLFormula computeCounterExample(
		PTLogSplitting log1,
		PTLogInfo log2,
		State s1, 
		State s2,
		Lts p
	) {
		PTLogSplittingEntry e1 = null;
		PTLogInfoEntry e2 = null;
		PTPartition splittedSet = null;
		PTStatesSet ss1 = null;
		PTStatesSet ss2 = null;		
		PTStatesSet bloccoB = null;		
		int trovati = 0;
		int i,j,k = 0;
		Vector<PTLogInfoEntry> info = null;
		PTStatesSet ss1d1 = null;
		PTStatesSet ss1d2 = null;
		PTStatesSet ss2d1 = null;
		PTStatesSet ss2d2 = null;
		boolean ss1not = false;
		boolean ss2not = false;
		StatesSet ssl = null;
		StatesSet ssr = null;
		StatesSet SL = null;
		StatesSet SR = null;
		boolean isS1 = false;
		HMLFormula formula = null;
		HMLFormula temp = null;
		Vector<HMLFormula> gamma = new Vector<HMLFormula>();
		boolean trovato = false;
		
		
		//Cerco la causa dello splitting in LogSplitting
		e1 = log1.getDeppestCause(s1,s2);
		
		//Recupero i blocchi che contengono s1 e s2, rispettivamente ss1 e ss2
		splittedSet = e1.getSplittedSet();
		trovati = 0;
		for (i=0; i<splittedSet.getDimension(); i++)
		{
			if (splittedSet.ptstatesSetAt(i).containState(s1))
			{
				ss1 = splittedSet.ptstatesSetAt(i);
				trovati++;
			}
			else if (splittedSet.ptstatesSetAt(i).containState(s2))
			{
				ss2 = splittedSet.ptstatesSetAt(i);
				trovati++;
			}
			if (trovati==2) break;
		}
		
		//Cerco in LogInfo il bloccoB rispetto al quale sono stati splittati ss1 e ss2
		info = log2.getInfo(ss1,ss2,e1.getCause());
		e2 = info.elementAt(0);
		ss1d1 = e2.getFirstDestination();
		ss1d2 = e2.getSecondDestination();
		ss1not = e2.isNot();
		e2 = info.elementAt(1);
		ss2d1 = e2.getFirstDestination();
		ss2d2 = e2.getSecondDestination();			
		ss2not = e2.isNot();
		if ((ss1d2==null)&&(ss2d2==null))
		{
			bloccoB = ss1d1;
			//pu� anche darsi che abbia trovato un not
			if (ss1not)	isS1 = false;
			else isS1 = true;
		}
		else if (ss2d2==null)
		{
			if (ss1d1.equals(ss2d1)) bloccoB = ss1d2;
			else bloccoB = ss1d1;
			isS1 = true;
		}
		else
		{
			if (ss2d1.equals(ss1d1)) bloccoB = ss2d2;
			else bloccoB = ss2d1;
			isS1 = false;
		}
		
		
		//Calcolo gli insiemi ssl e ssr
		ssl = new StatesSet();
		ssr = new StatesSet();
		if (isS1)
		{
			for (i=0; i<ss1.getDimension(); i++)
				ssl.addState(ss1.ptstateAt(i).getState());
			for (i=0; i<ss2.getDimension(); i++)
				ssr.addState(ss2.ptstateAt(i).getState());
		}
		else
		{
			for (i=0; i<ss1.getDimension(); i++)
				ssr.addState(ss1.ptstateAt(i).getState());
			for (i=0; i<ss2.getDimension(); i++)
				ssl.addState(ss2.ptstateAt(i).getState());			
		}
		
		//Condizione di Ricorsione
		if (!((ss1not)||(ss2not)))
		{
			
			//Calcolo gli insiemi SL e SR
			SL = bloccoB.intersection(ssl.dP(e1.getCause()));
			SR = ssr.dP(e1.getCause());
			
			//Per ogni sl in SL e sr in SR calcolo delta(sl,sr) e la aggiungo a gamma
			temp = new HMLFormula(new Action(""),true);
			for (i=0; i<SL.getDimension(); i++)
			{
				for (j=0; j<SR.getDimension(); j++)
				{
					if (!(temp.containCoupleSatisfied(SL.stateAt(i),SR.stateAt(j))))
					{
						formula = computeCounterExample(
							log1, log2, SL.stateAt(i), SR.stateAt(j), p 
						);
						for (k=0; k<formula.getCouplesSatisfiedDimension(); k++)
							if (!(temp.containCoupleSatisfied(formula.getCoupleSatisfiedAt(k))))
								temp.addCoupleSatisfied(formula.getCoupleSatisfiedAt(k));
						gamma.addElement(formula);
					}
				}	
			}
			
/*			//Prima riduzione gamma
			i = 0;
			while (true)
			{
				formula = (HMLFormula)gamma.elementAt(i);
				for (j=0; j<formula.getCouplesSatisfiedDimension(); j++)
				{
					trovato = false;
					for (k=0; k<gamma.size(); k++)
						if (k!=i)
						{
							temp = (HMLFormula)gamma.elementAt(k);
							if (temp.containCoupleSatisfied(formula.getCoupleSatisfiedAt(j)))
							{
								trovato = true;
								break;	
							}
						}
					if (!trovato) break;
				}
				if (trovato) gamma.removeElementAt(i);//posso scartare la formula	
				else i++;
				if (i>= gamma.size()) break;
			}
*/			
			//Seconda riduzione gamma
			i = 0;
			while (true) {
				formula = gamma.elementAt(i);
				trovato = false;
				for (j=0; j<SR.getDimension(); j++)
				{
					if (!(formula.satisfy(SR.stateAt(j),p)))
					{
						trovato = true;
						for (k=0; k<gamma.size(); k++)
						{
							if (k!=i)
							{
								temp = gamma.elementAt(k);
								if (!(temp.satisfy(SR.stateAt(j),p)))
								{
									trovato = false;
									break;	
								}
							}
						}
					}
					if (trovato) break;
				}
				if (!trovato) gamma.removeElementAt(i);//posso scartare la formula	
				else i++;
				if (i>= gamma.size()) break;
			}

		}
		
		//Costruisco la formula finale
		formula = new HMLFormula(e1.getCause(),!isS1);
		for (i=0; i<gamma.size(); i++)
			formula.addQueue( gamma.elementAt(i) );
		for (i=0; i<ss1.getDimension(); i++)
			for (j=0; j<ss2.getDimension(); j++)
				formula.addCoupleSatisfied(ss1.ptstateAt(i).getState(),ss2.ptstateAt(j).getState());
		return formula;
	}


																   
}
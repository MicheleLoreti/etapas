package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

/**
 * La classe AcceptanceSet rappresenta l'insieme di accettazione 
 * di un insieme di stati.
 * E' costituito da un insieme di insieme di azioni, 
 * ciascuno dei quali rappresenta l'insieme di accettazione di uno stato.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class AcceptanceSet {

	/**
	 * L'insieme di insiemi di azioni.
	 */	
	private Vector<ActionsSet> actionsSetSet = null;
	
	/**
	 * Costruisce una nuovo insieme vuoto.
	 **/
	public AcceptanceSet() {
		actionsSetSet = new Vector<ActionsSet>();
  	}		

	/**
	 * Aggiunge all'insieme un oggetto di tipo <tt>ActionsSet</tt>. 
	 * Non � possibile aggiungere elementi ripetuti,
	 * cio� insiemi di azioni che sono gi� presenti nell'insieme.
	 **/  	
  	public boolean addActionsSet(ActionsSet as) {
		if( containActionsSet(as) ) 
			return false;

		actionsSetSet.add(as);
		return true;
  	}

	/**
	 * Restituisce il numero di elementi contenuti nell'insieme.
	 **/ 
  	public int getDimension() {
  		return actionsSetSet.size();
  	}

	/**
	 * Ritorna l'oggetto di tipo <tt>ActionsSet</tt> contenuto 
	 * nell'insieme di insiemi di azioni alla 
	 * posizione specificata dal parametro intero in ingresso. 
	 **/	
	public ActionsSet actionsSetAt(int index) {
		try	{ 
			return actionsSetSet.elementAt(index); 
		}	
		catch(ArrayIndexOutOfBoundsException e)	{ 
			return null; 
		}
	}

	/**
	 * Controlla se l'insieme � vuoto. 
	 **/
	public boolean isEmpty() {
		return getDimension() == 0;
	}

	/**
	 * Calcola il minimo Acceptance set relativo all'Acceptance set corrente. 
	 * Implementa la seguente funzione:
	 * 		min Acc = { A | 
	 *			A appartiene a Acc e non esiste un A' in Acc tale 
	 *			che A sia contenuto in A'
	 *		}
	 **/  	
	public AcceptanceSet getMinAcceptanceSet() {		
		AcceptanceSet result = new AcceptanceSet();		
		
		for( int i=0; i<getDimension(); i++ ) {
			ActionsSet actionsSet = actionsSetAt(i);
			boolean minimal = true;
			for( int j=0; j<getDimension(); j++ ) {
				if( j!=i && actionsSet.containActionsSet(actionsSetAt(j)) ) {
					minimal = false;
					break;	
				}
			}
			if( minimal ) 
				result.addActionsSet(actionsSet);
		}
		
		return result;	
	}

	/**
	 * Controlla se tra tutti gli insiemi di azioni contenuti nell'Acceptance 
	 * set ne esiste uno che contiene 
	 * le stesse azioni dell'insieme passato come parametro di ingresso. 
	 */
	public boolean containActionsSet(ActionsSet as) {
		for( int i=0; i<getDimension(); i++)	
			if( actionsSetAt(i).equals(as) ) 
				return true;
				
		return false;
	}

	/**
	 * Verifica se l'insieme corrente � uguale 
	 * all'insieme passato come parametro di ingresso.
	 **/
	public boolean equals(AcceptanceSet acc) {
		if( getDimension() != acc.getDimension() ) 
			return false;

		for (int i=0; i<acc.getDimension(); i++)	
			if( !containActionsSet(acc.actionsSetAt(i)) ) 
				return false;
				
		return true;
	}

	/**
	 * Stampa l'intero insieme di accettazione.
	 **/ 
	public void print() {
		print(System.out);
	}
	
	/**
	 * Stampa l'intero insieme di accettazione.
	 **/ 
	public void print(java.io.PrintStream console) {
		
		console.print("{");
		
		int i = 0;
		for( i=0; i<this.getDimension(); i++ ) {
			if(i>0) 
				console.print(", ");
			actionsSetAt(i).print(console);
		}
		
		if(i==0) 
			console.print("empty");
			
		console.print("}");
	} 
	
	
}
package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBLogInfoEntry {
	
	private BBBloc origin = null;	
	private Action cause = null;	
	private BBBloc destination = null;	
	private boolean negation = false;
	
	public BBLogInfoEntry(
		BBBloc origin, Action cause, BBBloc destination, boolean negation
	) {
		this.origin = origin;
		this.cause = cause;
		this.destination = destination;
		this.negation = negation;
	}
	
	public BBBloc getOrigin() {
		return origin;
	}


	public Action getCause() {
		return cause;	
	}


	public BBBloc getDestination() {
		return destination;
	}
	
	public boolean isNot() {
		return negation;
	}		

	public void print() {
		print(System.out);
	}

	public void print(java.io.PrintStream console) {
		getOrigin().printAll(console);
		
		if( isNot() ) 
			console.append(" NOT -- ");
		else 
			console.append(" -- ");
		
		getCause().print(console);
		console.append(" --> ");		
		getDestination().printAll(console);
	}
}
/**
 * 
 */
package org.cmg.tapas.formulae.actl;


import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.cmg.tapas.formulae.actl.macro.StateFormulaMacro;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author Michele Loreti
 *
 */
public class ActlSuite<X extends APProcess<X ,Y>,Y extends ActionInterface> 
				extends AbstractTableModel 
				implements Iterable<ActlFormula<X, Y>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static int DESCRIPTION_COLUMN = 0;
	private static int FORMULA_COLUMN = 1;
	private static int SELECT_COLUMN = 2;
	private static int COLUMNS = 3;
	
	private Vector<ActlFormula<X, Y>> formulae; 
	private Vector<Boolean> selection;
	private Hashtable<String, StateFormulaMacro<X, Y>> macros;
	
	public ActlSuite() {
		formulae = new Vector<ActlFormula<X,Y>>();
		selection = new Vector<Boolean>();
		macros = new Hashtable<String, StateFormulaMacro<X,Y>>();
	}
	
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
		case 1:
			return String.class;
		case 2:
			return String.class;
		default:
			return null;
		}
	}

	public int getColumnCount() {
		return COLUMNS;
	}

	public String getColumnName(int columnIndex) {
		if (columnIndex == DESCRIPTION_COLUMN) {
			return "Property Name";
		}
		if (columnIndex == FORMULA_COLUMN) {
			return "Formula";
		}
		if (columnIndex == SELECT_COLUMN) {
			return "Check";
		}
		return null;
	}

	public int getRowCount() {
		return formulae.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return formulae.get(rowIndex).getName();
		case 1:
			return formulae.get(rowIndex).getUnicode();
		case 2:
			Boolean b = selection.get(rowIndex);
			if (b == null) {
				return " ";
			}
			return (b?"Yes":"No");
		default:
			return "BO";
		}
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (columnIndex == SELECT_COLUMN) {
			selection.set(rowIndex, (Boolean) aValue);
			fireTableDataChanged();
		}
	}

	public void addFormula( ActlFormula<X, Y> f ) {
		formulae.add(f);
		selection.add(null);
		fireTableRowsInserted(0, formulae.size());
	}
	
//	public void doCheck(GraphData<X, Y> g , X s) {
//		JProgressBar jpb = new JProgressBar(JProgressBar.HORIZONTAL,0,formulae.size());
//		jpb.setVisible(true);
//		for( int i=0 ; i<formulae.size() ; i++ ) {
//			selection.set(i, formulae.get(i).satisfies(g, s));
//			fireTableDataChanged();
//			jpb.setValue(i);
//		}
//		jpb.setVisible(false);
//	}
	
	public void doReset( ) {
		for( int i=0 ; i<formulae.size() ; i++ ) {
			selection.set(i, null);
		}
		fireTableDataChanged();
	}

	public Iterator<ActlFormula<X, Y>> iterator() {
		return formulae.iterator();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String toReturn = "";
		for (ActlFormula<X, Y> f : formulae) {
			toReturn += "property "+f.getName()+":\n";
			toReturn += f+"\n";
		}
		return toReturn;
	}

	public String toUnicode() {
		String toReturn = "";
		for (ActlFormula<X, Y> f : formulae) {
			toReturn += f.getUnicode()+"\n";
		}
		return toReturn;
	}

	public void addAll(ActlSuite<X, Y> suite) {
		for (ActlFormula<X, Y> formula : suite) {
			addFormula(formula);
		}
		
	}

	public void clear() {
		int size = formulae.size();
		formulae.removeAllElements();
		selection.removeAllElements();
		fireTableRowsDeleted(0, size);
	}
	
	public StateFormulaMacro<X, Y> getMacro( String name ) {
		return macros.get(name);
	}
	
	public void addMacro( String name , StateFormulaMacro<X, Y> macro ) {
		macros.put(name, macro);
	}

	public Collection<StateFormulaMacro<X, Y>> getMacros() {
		return macros.values();
	}
}

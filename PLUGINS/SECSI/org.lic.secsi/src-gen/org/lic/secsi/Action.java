/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Action#getChannel <em>Channel</em>}</li>
 *   <li>{@link org.lic.secsi.Action#getModality <em>Modality</em>}</li>
 *   <li>{@link org.lic.secsi.Action#isNotSignal <em>Not Signal</em>}</li>
 *   <li>{@link org.lic.secsi.Action#getE <em>E</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends Statement
{
  /**
   * Returns the value of the '<em><b>Channel</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Channel</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Channel</em>' reference.
   * @see #setChannel(Channel)
   * @see org.lic.secsi.SecsiPackage#getAction_Channel()
   * @model
   * @generated
   */
  Channel getChannel();

  /**
   * Sets the value of the '{@link org.lic.secsi.Action#getChannel <em>Channel</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Channel</em>' reference.
   * @see #getChannel()
   * @generated
   */
  void setChannel(Channel value);

  /**
   * Returns the value of the '<em><b>Modality</b></em>' attribute.
   * The literals are from the enumeration {@link org.lic.secsi.ActionModality}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modality</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modality</em>' attribute.
   * @see org.lic.secsi.ActionModality
   * @see #setModality(ActionModality)
   * @see org.lic.secsi.SecsiPackage#getAction_Modality()
   * @model
   * @generated
   */
  ActionModality getModality();

  /**
   * Sets the value of the '{@link org.lic.secsi.Action#getModality <em>Modality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Modality</em>' attribute.
   * @see org.lic.secsi.ActionModality
   * @see #getModality()
   * @generated
   */
  void setModality(ActionModality value);

  /**
   * Returns the value of the '<em><b>Not Signal</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Not Signal</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Not Signal</em>' attribute.
   * @see #setNotSignal(boolean)
   * @see org.lic.secsi.SecsiPackage#getAction_NotSignal()
   * @model
   * @generated
   */
  boolean isNotSignal();

  /**
   * Sets the value of the '{@link org.lic.secsi.Action#isNotSignal <em>Not Signal</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Not Signal</em>' attribute.
   * @see #isNotSignal()
   * @generated
   */
  void setNotSignal(boolean value);

  /**
   * Returns the value of the '<em><b>E</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>E</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>E</em>' containment reference.
   * @see #setE(Expression)
   * @see org.lic.secsi.SecsiPackage#getAction_E()
   * @model containment="true"
   * @generated
   */
  Expression getE();

  /**
   * Sets the value of the '{@link org.lic.secsi.Action#getE <em>E</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>E</em>' containment reference.
   * @see #getE()
   * @generated
   */
  void setE(Expression value);

} // Action

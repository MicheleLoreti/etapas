package org.cmg.tapas.formulae.actl.state;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlFormulaVisitor;
import org.cmg.tapas.formulae.actl.path.ActlNext;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public class ActlBox<X extends APProcess<X ,Y>,Y extends ActionInterface> 
						extends ActlFormula<X,Y>  {
	
	private ActlActionPredicate<X,Y> actionFilter; 
	private ActlFormula<X, Y> actlFormula;	
	private ActlFormula<X, Y> equivalent;
	
	public ActlBox(ActlActionPredicate<X,Y> actionFilter
					, ActlFormula<X, Y> actlFormula){
		this.actionFilter = actionFilter;
		this.actlFormula = actlFormula;
		
		//not formula
		ActlNot<X, Y> not = new ActlNot<X, Y>(actlFormula);
		
		//X {filter} not formula
		ActlNext<X, Y> next = new ActlNext<X, Y>(actionFilter, not);
		
		//EX {filter} not formula
		ActlQuantification<X, Y> exist = new ActlQuantification<X, Y>(false, next);
		
		//not EX {filter} not formula
		equivalent = new ActlNot<X, Y>(exist); 
	}

	@Override
	protected String _toString() {
		return "["+actionFilter+"] "+actlFormula;
	}

	@Override
	protected ActlFormula<X, Y> doClone() {		
		return new ActlBox<X, Y>(actionFilter, actlFormula.clone());
	}

	@Override
	protected void doVisit(ActlFormulaVisitor<X, Y> visitor) {
		visitor.visitBox(this);		
	}

	@Override
	public String getUnicode() {
		return "["+actionFilter+"] "+actlFormula.getUnicode();
	}

	@Override
	public String operator() {
		return "";
	}

	@Override
	public boolean satisfies(X x) {
		return equivalent.satisfies(x);
	}
	
	public ActlFormula<X,Y> getFormula(){
		return actlFormula; 
	}
	
	public ActlActionPredicate<X, Y> getFilter(){
		return actionFilter;
	}
}

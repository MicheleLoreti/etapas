package org.cmg.tapas.testGraph.algorithms.bisimulation.regression;

import java.util.Random;

/** 
 * Sottopone a test un algoritmo per dedurre
 * la formula delle prestazioni.
 * Le sottoclassi devono implementare il metodo
 * <tt>algorithm(dim,vector)</tt>.
 *
 * @author Guzman Tierno
 **/
public abstract class Simulation {
    protected Random rand = new Random();

	private int execs;				// numero di prove per ciascuna dimensione 
	private int dim;				// dimensione base
	private int gap;				// salto tra una dimensione e un'altra
	private int cases = 10;			// quantit� delle dimensioni da considerare 

	// vettore delle dimensioni
	private double[] dimension;
	
	// vettore delle medie (per ogni dimensione)
	private double[] meanValue;
	
	// vettore delle varianze (per ogni dimensione)
	private double[] variance;
	
	public void setSeed(long seed) {
		rand.setSeed(seed);
	}
	
	public double[] getDimensions() {
		return dimension;
	}
	
	public double[] getMeanValues() {
		return meanValue;
	}
	
	
	public double[] getVariances() {
		return variance;
	}
	
	private int algorithmType;

	// tipi di algoritmo
	public static final int SEARCH =		0;
	public static final int SEARCH_POW2 =	1;
	public static final int SORT =			2;
	public static final int SORT_POW2 =	3;
	
	private int shuffleFlag;

	// shuffle flag
	public static final int UNSHUFFLED =	0;	
	public static final int SHUFFLED =		1;
	public static final int RANDOM_VECTOR =	2;
	public static final int NULL_VECTOR =	3;
	
	// Costruttore
	public Simulation( 
		int execs,	//100
		int dim,	//100
		int gap,	//100
		int cases,	//10
		int algorithmType,
		int shuffleFlag
	){		
		this.gap = gap;
		this.dim = dim;
		this.execs = execs;
		this.cases = cases;
		this.algorithmType = algorithmType;					
		this.shuffleFlag = shuffleFlag;					
		
		dimension = new double[cases];
		meanValue = new double[cases];			// valori sperimentali
		variance = new double[cases];
	}
	
	// mette una permutazione casuale in 'v'
	public void shuffle( int[] v, int flagShuffle ){
		if( v==null )
			return;
			
		int n = v.length;
		int i, j, tmp;
		
		if( flagShuffle == RANDOM_VECTOR ){			// random
			for( i = 0; i < n; i++ ) 
				v[i] = rand.nextInt(30000);
			return;
		}
			
			
		for( i = 0; i < n; i++ )	 			// inizializza vettore
			v[i] = i;

		if( flagShuffle != SHUFFLED )
			return;
	
		for( i = n-1; i >= 1; i-- ) {			// shuffle
			j = rand.nextInt(i+1);
			tmp = v[j];
			v[j] = v[i];
			v[i] = tmp;
		}
	}

	// chiQuadro = Somme( (s_i-t_i)^2 / t_i )
	public double chiSquare() {		
		double X2 = 0;
		double t;								
		
		for( int i = 0; i < cases; i++ ) {
			t = theoricValue( dimension[i] );
			
			if(t==0) {
				System.out.println("Valore teorico non definito");
				return -1;
			}
			
			X2 += (t - meanValue[i]) * (t - meanValue[i]) / t;
		}

		return X2;
	}

	/** 
	 * Esegue l'algoritmo da testare ('cases*execs*reps' volte,
	 * riempie i vettori 'dimension', 'meanValue' e 'variance').
	 * Dopo questa chiamata � possibile calcolare la dipendenza
	 * tra dimensione e variabile.
	 **/
	public void simulate() {
		int[] v = null;		
		double tot, tot2, count;
		
		int i, k, n;
		int reps;
		for( int j = 0; j < cases; j++ ) {
			
			switch(algorithmType) {
				case SORT_POW2 : 
				case SEARCH_POW2 : 
					n = (1<<j) * gap + dim;
					break;
				default :
					n =	j * gap + dim;
					break;
			}

			if( shuffleFlag != NULL_VECTOR ) 
 				v = new int[n];
			reps = (algorithmType==SORT || algorithmType==SORT_POW2)
					? 1 : n/4;
			
			for( tot = tot2 = 0, k = 0; k < execs; k++ ) {		
				shuffle(v, shuffleFlag );																
				for( i = 0; i<reps; i++ ) { 										
					count = algorithm(n, v);
					tot += count;
					tot2 += count*count;			
				} 
			} 
			
			dimension[j] = n;	
			k = reps * execs;
			meanValue[j] = tot / k;		
			variance[j] = tot2 / k - meanValue[j] * meanValue[j];	
		} 					
	} 
  
	public abstract double algorithm(int dim, int[] v);
	
	public double theoricValue(double param) {
		return 0;
	}
	
	/** Rende la dipendenza migliore tra dimensione e valori. **/
	public Regression bestRegression() {
		Regression linearReg = regression(Regression.LINEAR);
		Regression logReg = regression(Regression.LOGARITMIC);
		Regression polyReg = regression(Regression.POLYNOMIAL);
		Regression expReg = regression(Regression.EXPONENTIAL);
		Regression quadReg = regression(Regression.QUADRATIC); 
		Regression linearLogReg = regression(Regression.LINEARITMIC); 
	
		Regression bestReg = linearReg;		
		if( logReg.getCorrelation() > bestReg.getCorrelation() ) 
			bestReg = logReg;			
		if( expReg.getCorrelation() > bestReg.getCorrelation() ) 		
			bestReg = expReg;			
		if( quadReg.getCorrelation() > bestReg.getCorrelation() ) 		
			bestReg = quadReg;
		if( polyReg.getCorrelation() > bestReg.getCorrelation() ) 		
			bestReg = polyReg;
		if( linearLogReg.getCorrelation() > bestReg.getCorrelation() ) 		
			bestReg = linearLogReg;
			
		return bestReg;
	}
	
	
	/** Rende la dipendenza del tipo specificato tra dimensione e valori. **/
	public Regression regression(int type) {
		return new Regression( dimension, meanValue, type);
	}
	
	/** Rende la dipendenza quadratica tra dimensione e valori. **/
	public Regression2 regression2(int type) {
		return new Regression2( dimension, meanValue, type);
	}





}	
	

	
	
	
	
		
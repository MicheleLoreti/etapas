/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Label Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule#getLabel <em>Label</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLabelRule()
 * @model
 * @generated
 */
public interface LabelRule extends EObject
{
  /**
   * Returns the value of the '<em><b>Label</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Label</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Label</em>' reference.
   * @see #setLabel(Label)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLabelRule_Label()
   * @model
   * @generated
   */
  Label getLabel();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule#getLabel <em>Label</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Label</em>' reference.
   * @see #getLabel()
   * @generated
   */
  void setLabel(Label value);

  /**
   * Returns the value of the '<em><b>States</b></em>' reference list.
   * The list contents are of type {@link org.cmg.tapas.xtext.clts.composedLts.LtsState}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>States</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>States</em>' reference list.
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLabelRule_States()
   * @model
   * @generated
   */
  EList<LtsState> getStates();

} // LabelRule

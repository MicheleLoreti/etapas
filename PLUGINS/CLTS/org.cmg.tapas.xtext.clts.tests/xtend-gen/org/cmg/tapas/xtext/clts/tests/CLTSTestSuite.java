package org.cmg.tapas.xtext.clts.tests;

import com.google.inject.Inject;
import org.cmg.tapas.clts.runtime.CLTSUtil;
import org.cmg.tapas.clts.runtime.CltsProcess;
import org.cmg.tapas.formulae.ltl.LtlFormula;
import org.cmg.tapas.xtext.clts.ComposedLtsInjectorProvider;
import org.cmg.tapas.xtext.clts.composedLts.Model;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.util.IAcceptor;
import org.eclipse.xtext.xbase.compiler.CompilationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.util.ReflectExtensions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@InjectWith(ComposedLtsInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class CLTSTestSuite {
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Inject
  @Extension
  private ParseHelper<Model> _parseHelper;
  
  @Inject
  @Extension
  private CompilationTestHelper _compilationTestHelper;
  
  @Inject
  @Extension
  private ReflectExtensions _reflectExtensions;
  
  @Test
  public void testDF() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("model DF;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("action tic;");
      _builder.newLine();
      _builder.append("action get0;");
      _builder.newLine();
      _builder.append("action rel0;");
      _builder.newLine();
      _builder.append("action get1;");
      _builder.newLine();
      _builder.append("action rel1;");
      _builder.newLine();
      _builder.append("action get2;");
      _builder.newLine();
      _builder.append("action rel2;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("label hl0;");
      _builder.newLine();
      _builder.append("label hr0;");
      _builder.newLine();
      _builder.append("label hl1;");
      _builder.newLine();
      _builder.append("label hr1;");
      _builder.newLine();
      _builder.append("label hl2;");
      _builder.newLine();
      _builder.append("label hr2;");
      _builder.newLine();
      _builder.append("label e0;");
      _builder.newLine();
      _builder.append("label e1;");
      _builder.newLine();
      _builder.append("label e2;");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts P0 {\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("states: T, H, HL, HR, E, RL, RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("init: T;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rules:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("T - tic -> H;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("H - get0 -> HL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("H - get1 -> HR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("HL - get1 -> E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("HR - get0 -> E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("E - rel0 -> RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("E - rel1 -> RL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("RR - rel1 -> T;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("RL - rel0 -> T;\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endrules\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("labels:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hl0: HL,E,RL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hr0: HR,E,RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("e0: E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endlabels");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts P1 {\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("states: T, H, HL, HR, E, RL, RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("init: T;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rules:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("T - tic -> H;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("H - get1 -> HL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("H - get2 -> HR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("HL - get2 -> E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("HR - get1 -> E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("E - rel1 -> RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("E - rel2 -> RL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("RR - rel2 -> T;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("RL - rel1 -> T;\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endrules");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("labels:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hl1: HL,E,RL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hr1: HR,E,RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("e1: E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endlabels");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts P2 {\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("states: T, H, HL, HR, E, RL, RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("init: T;");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rules:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("T - tic -> H;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("H - get2 -> HL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("H - get0 -> HR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("HL - get0 -> E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("HR - get2 -> E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("E - rel2 -> RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("E - rel0 -> RL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("RR - rel0 -> T;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("RL - rel2 -> T;\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endrules");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("labels:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hl2: HL,E,RL;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("hr2: HR,E,RR;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("e2: E;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endlabels");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts F0 {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("states: A, O;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("init: A;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rules:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("A - get0 -> O;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("O - rel0 -> A;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("endrules\t");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts F1 = F0{ get0 -> get1 , rel0 -> rel1 };");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts F2 = F0{ get0 -> get2 , rel0 -> rel2 };");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts SysP = P0||P1||P2;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts SysF = F0||F1||F2;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("lts Sys = SysP |get0,rel0,get1,rel1,get2,rel2| SysF;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("ltl formula CorrectUse0 = !\\F (hl0 & hr1); ");
      _builder.newLine();
      _builder.newLine();
      _builder.append("ltl formula EventuallyEat0 = \\G \\F e0;");
      _builder.newLine();
      final IAcceptor<CompilationTestHelper.Result> _function = (CompilationTestHelper.Result it) -> {
        try {
          Class<?> _compiledClass = it.getCompiledClass();
          Object _newInstance = _compiledClass.newInstance();
          final Procedure1<Object> _function_1 = (Object it_1) -> {
            Assert.assertNotNull(it_1);
            CltsProcess sys = this.getReactiveModule(it_1, "Sys");
            LtlFormula<CltsProcess> f = this.getLTLFormula(it_1, "CorrectUse0");
            boolean _check = CLTSUtil.check(sys, f);
            Assert.assertTrue(_check);
          };
          ObjectExtensions.<Object>operator_doubleArrow(_newInstance, _function_1);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      this._compilationTestHelper.compile(_builder, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public CltsProcess getReactiveModule(final Object object, final String name) {
    try {
      CltsProcess _xblockexpression = null;
      {
        Object data = this._reflectExtensions.invoke(object, "getSystem", name);
        Assert.assertNotNull(data);
        Assert.assertTrue((data instanceof CltsProcess));
        _xblockexpression = ((CltsProcess) data);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public LtlFormula<CltsProcess> getLTLFormula(final Object object, final String name) {
    try {
      LtlFormula<CltsProcess> _xblockexpression = null;
      {
        Object data = this._reflectExtensions.invoke(object, "getLtlFormula", name);
        Assert.assertNotNull(data);
        Assert.assertTrue((data instanceof LtlFormula<?>));
        _xblockexpression = ((LtlFormula<CltsProcess>) data);
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}

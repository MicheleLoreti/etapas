/**
 */
package org.lic.secsi.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.lic.secsi.Agent;
import org.lic.secsi.Proc;
import org.lic.secsi.Ren;
import org.lic.secsi.SecsiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Proc</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.lic.secsi.impl.ProcImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProcImpl#getName1 <em>Name1</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProcImpl#getRe <em>Re</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProcImpl extends MinimalEObjectImpl.Container implements Proc
{
  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected Agent name;

  /**
   * The cached value of the '{@link #getName1() <em>Name1</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName1()
   * @generated
   * @ordered
   */
  protected Agent name1;

  /**
   * The cached value of the '{@link #getRe() <em>Re</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRe()
   * @generated
   * @ordered
   */
  protected Ren re;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProcImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.PROC;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Agent getName()
  {
    if (name != null && name.eIsProxy())
    {
      InternalEObject oldName = (InternalEObject)name;
      name = (Agent)eResolveProxy(oldName);
      if (name != oldName)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, SecsiPackage.PROC__NAME, oldName, name));
      }
    }
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Agent basicGetName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(Agent newName)
  {
    Agent oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.PROC__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Agent getName1()
  {
    if (name1 != null && name1.eIsProxy())
    {
      InternalEObject oldName1 = (InternalEObject)name1;
      name1 = (Agent)eResolveProxy(oldName1);
      if (name1 != oldName1)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, SecsiPackage.PROC__NAME1, oldName1, name1));
      }
    }
    return name1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Agent basicGetName1()
  {
    return name1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName1(Agent newName1)
  {
    Agent oldName1 = name1;
    name1 = newName1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.PROC__NAME1, oldName1, name1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Ren getRe()
  {
    return re;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRe(Ren newRe, NotificationChain msgs)
  {
    Ren oldRe = re;
    re = newRe;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.PROC__RE, oldRe, newRe);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRe(Ren newRe)
  {
    if (newRe != re)
    {
      NotificationChain msgs = null;
      if (re != null)
        msgs = ((InternalEObject)re).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.PROC__RE, null, msgs);
      if (newRe != null)
        msgs = ((InternalEObject)newRe).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.PROC__RE, null, msgs);
      msgs = basicSetRe(newRe, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.PROC__RE, newRe, newRe));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SecsiPackage.PROC__RE:
        return basicSetRe(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SecsiPackage.PROC__NAME:
        if (resolve) return getName();
        return basicGetName();
      case SecsiPackage.PROC__NAME1:
        if (resolve) return getName1();
        return basicGetName1();
      case SecsiPackage.PROC__RE:
        return getRe();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SecsiPackage.PROC__NAME:
        setName((Agent)newValue);
        return;
      case SecsiPackage.PROC__NAME1:
        setName1((Agent)newValue);
        return;
      case SecsiPackage.PROC__RE:
        setRe((Ren)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.PROC__NAME:
        setName((Agent)null);
        return;
      case SecsiPackage.PROC__NAME1:
        setName1((Agent)null);
        return;
      case SecsiPackage.PROC__RE:
        setRe((Ren)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.PROC__NAME:
        return name != null;
      case SecsiPackage.PROC__NAME1:
        return name1 != null;
      case SecsiPackage.PROC__RE:
        return re != null;
    }
    return super.eIsSet(featureID);
  }

} //ProcImpl

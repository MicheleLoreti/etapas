/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.formulae.actl.path.ActlFuture;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class FutureMacro< S extends APProcess<S, A> , A extends ActionInterface> extends PathFormulaMacro<S, A> {

	private ActlActionPredicate<S,A> predicate;
	private StateFormulaMacro<S, A> macro;
	
	public FutureMacro(ActlActionPredicate<S,A> predicate,
			StateFormulaMacro<S, A> macro) {
		super();
		this.predicate = predicate;
		this.macro = macro;
	}

	@Override
	public PathFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		if (f.length != parameters()) {
			throw new IllegalArgumentException();
		}
		ActlFormula<S, A>[] right = new ActlFormula[ macro.parameters() ];
		System.arraycopy(f, 0, right, macro.parameters(), macro.parameters());
		return new ActlFuture<S, A>(predicate, macro.getFormula(right));
	}

	@Override
	public int parameters() {
		return macro.parameters();
	}

	@Override
	public void accept(PathMacroVisitor<S, A> visitor) {
		visitor.visitFuture(this);
	}

	public ActlActionPredicate<S,A> getPredicate() {
		return predicate;
	}

	public StateFormulaMacro<S, A> getMacro() {
		return macro;
	}
}

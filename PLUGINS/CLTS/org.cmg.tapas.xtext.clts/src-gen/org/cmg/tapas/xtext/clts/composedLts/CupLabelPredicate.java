/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cup Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getLeft <em>Left</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getRigh <em>Righ</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getCupLabelPredicate()
 * @model
 * @generated
 */
public interface CupLabelPredicate extends LabelPredicate
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(LabelPredicate)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getCupLabelPredicate_Left()
   * @model containment="true"
   * @generated
   */
  LabelPredicate getLeft();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(LabelPredicate value);

  /**
   * Returns the value of the '<em><b>Righ</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Righ</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Righ</em>' containment reference.
   * @see #setRigh(LabelPredicate)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getCupLabelPredicate_Righ()
   * @model containment="true"
   * @generated
   */
  LabelPredicate getRigh();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getRigh <em>Righ</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Righ</em>' containment reference.
   * @see #getRigh()
   * @generated
   */
  void setRigh(LabelPredicate value);

} // CupLabelPredicate

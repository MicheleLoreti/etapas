package org.cmg.tapas.graph.algorithms;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;

/** 
 * EquivalenceChecker rappresenta l'interfaccia delle classi
 * che offrono i metodi per il calcolo di equivalenze.
 * <p>
 * @param <S> tipo degli stati
 * @param <A> tipo delle azioni
 * 
 * @author Guzman Tierno
 **/
public interface EquivalenceChecker <
	S extends StateInterface,
	A extends ActionInterface	
> {
	
	/**
     * Controlla se gli stati specificati sono nella stessa 
     * classe di equivalenza rispetto all'equivalenza, 
     * del tipo fissato dalla sottoclasse
     **/
	boolean checkEquivalence(S state1, S state2);
	
}















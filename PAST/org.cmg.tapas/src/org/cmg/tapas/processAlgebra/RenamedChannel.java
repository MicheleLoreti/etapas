package org.cmg.tapas.processAlgebra;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RenamedChannel {
	
	private Map<Channel, RenamingHistory<Channel>> map;
	
	public RenamedChannel(){
		map = new HashMap<Channel, RenamingHistory<Channel>>();
	}
	
	public Collection<RenamingHistory<Channel>> getRenamedChannels(){
		return map.values();
	}
	
	public void addAll(Collection<RenamingHistory<Channel>> list){
		for (RenamingHistory<Channel> r : list) {
			Channel c = r.getRoot();
			if(map.containsKey(c)){
				RenamingHistory<Channel> newR = map.get(c);
				newR.merge(r);
			}else{
				map.put(c, r);
			}
		}
	}
	
	public void compose(Map<Channel,Channel> f, Collection<RenamingHistory<Channel>> list){
		addAll(list);
		Set<Channel> keys = f.keySet();
		for (Channel oldC : keys) {
			Channel newC = f.get(oldC);
			RenamingHistory<Channel> rh;
			if(map.containsKey(oldC)){
				rh = new RenamingHistory<Channel>(newC);
				rh.addChild(map.remove(oldC));
			}else{
				rh = new RenamingHistory<Channel>(newC, oldC);
			}
			
			if(map.containsKey(newC)){
				map.get(newC).merge(rh);
			}else{
				map.put(newC, rh);
			}
		}		
	}
	
	@Override
	public String toString(){
		String res = "";
		for (RenamingHistory<Channel> re : map.values()) {
			res += re.toString();
		}
		return res;
	}
}

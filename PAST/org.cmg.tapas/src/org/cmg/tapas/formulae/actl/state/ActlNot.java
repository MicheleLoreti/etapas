/**
 * 
 */
package org.cmg.tapas.formulae.actl.state;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlFormulaVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public final class ActlNot<X extends APProcess<X ,Y>,Y extends ActionInterface> extends ActlFormula<X,Y> {

	private ActlFormula<X,Y> arg;
	
	public ActlNot( ActlFormula<X,Y> arg ) {
		this.arg = arg;
	}

	@Override
	protected ActlFormula<X, Y> doClone() {
		return new ActlNot<X, Y>(arg.clone());
	}

	@Override
	protected void doVisit(ActlFormulaVisitor<X, Y> visitor) {
		visitor.visitNotFormula(this);
	}

	@Override
	protected String _toString() {
		return "! "+arg;
	}

	@Override
	public String getUnicode() {
		return "\u00AC "+arg.getUnicode();
	}
	
	@Override
	public String operator() {
		return "NOT";	
	}

	@Override
	public boolean satisfies(X x) {
		return !arg.satisfies(x);
	}

	public ActlFormula<X, Y> getFormula() {
		return arg;
	}

}

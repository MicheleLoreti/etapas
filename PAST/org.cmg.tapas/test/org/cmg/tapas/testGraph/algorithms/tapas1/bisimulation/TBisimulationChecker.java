package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;


/**
 * La classe BisimulationChecker implementa 
 * la classe <tt>EquivalenceChecker</tt> realizzando l'algoritmo di verifica 
 * tra due automi per l'equivalenza bisimulation. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class TBisimulationChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {

    /** 
     * Costruisce una nuova istanza del checker 
     **/
	public TBisimulationChecker() {
		super("Bisimulation");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 **/	
	@Override 
	public boolean checkEquivalence()  {
		Lts p1 = Lts.createProcess( getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess( getSecondAutomaton(), getInitState2(), null );
		Lts p = Lts.createProcessFusion(p1, p2);
		PTPartition initialRo = Bisimulation.initPTPartition(p);

		PTState initialPTState1 = initialRo.getPTState(p.getInitState());
		PTState initialPTState2 = initialRo.getPTState(p.getSecondInitState());

		PTLogSplitting logSplitting = new PTLogSplitting();
		PTLogInfo logInfo = new PTLogInfo();
		Partition ro = null;

		if( getVerboseSolution() )
			ro = Bisimulation.computePTPartitionRefinement(
				p, initialRo, initialPTState1, initialPTState2, 
				logSplitting, logInfo
			);
		else 
			ro = Bisimulation.computePTPartitionRefinement(
				p, initialRo, initialPTState1, initialPTState2, 
				null, null
		);
		
		boolean result = 
			ro.checkSameClass(p.getInitState(),p.getSecondInitState());
		if( !result && getVerboseSolution() ) {
			Bisimulation.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1,initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName(), 
				p
			);
		}
		
		return result;
	}
	
}
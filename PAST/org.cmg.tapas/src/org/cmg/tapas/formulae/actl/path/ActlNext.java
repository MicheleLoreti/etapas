package org.cmg.tapas.formulae.actl.path;


import java.util.LinkedList;
import java.util.Map;

import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlPathFormulaVisitor;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.processAlgebra.APProcess;



public class ActlNext <X extends APProcess<X ,Y>,Y extends ActionInterface> 
				extends PathFormula<X,Y> {

	private ActlActionPredicate<X,Y> actionFilter;
	private ActlFormula<X, Y> actlFormula;	
	
	public ActlNext(ActlActionPredicate<X,Y> filter, ActlFormula<X, Y> actlFormula) {
		super();
		this.actlFormula = actlFormula;
		this.actionFilter = filter;
	}
	
	private boolean checkExistsNext(X x) {
		Map<Y, LinkedList<X>> next = x.getNext();
		for (Y a : next.keySet()) {
			if (actionFilter.getFilter().check( a )) {
				for (X p : next.get(a)) {
					if (actlFormula.satisfies(p)) {
						return true;
					} 
				}
			}
		}
		return false;
	}

	private boolean checkForAllNext(X x) {
		Map<Y, LinkedList<X>> next = x.getNext();
		for (Y a : next.keySet()) {
			LinkedList<X> nextList = next.get(a);
			if(!nextList.isEmpty()){
				if (actionFilter.getFilter().check( a )) {
					for (X p : nextList) {
						if (!actlFormula.satisfies(p)) {
							return false;
						} 
					}
				} else {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	protected String _toString() {		
		return "X {"+actionFilter+"} "+actlFormula.toString();
	}

	@Override
	protected PathFormula<X,Y> doClone() {
		return new ActlNext<X, Y>(actionFilter, actlFormula.clone());
	}

	@Override
	protected void doVisit(ActlPathFormulaVisitor<X, Y> visitor) {
		visitor.visitNextFormula(this);
	}

	public Filter<Y> getFilter(){
		return actionFilter.getFilter();
	}
	
	public ActlFormula<X, Y> getActlFormula(){
		return actlFormula;
	}

	@Override
	public String getUnicode() {
		String toReturn = "X";
		if (actionFilter != null) {
			toReturn += " {"+actionFilter+"} ";
		}
		return toReturn+actlFormula.getUnicode();
	}

	@Override
	public String operator() {
		return "X";
	}

	@Override
	public boolean sat(X x, boolean forall) {
		if (forall) {
			return checkForAllNext( x );
		} else {
			return checkExistsNext( x );
		}
	}

	public ActlActionPredicate<X, Y> getPredicate() {
		return actionFilter;
	}
}

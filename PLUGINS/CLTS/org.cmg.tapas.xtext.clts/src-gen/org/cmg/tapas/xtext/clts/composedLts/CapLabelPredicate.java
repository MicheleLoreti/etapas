/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cap Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getLeft <em>Left</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getCapLabelPredicate()
 * @model
 * @generated
 */
public interface CapLabelPredicate extends LabelPredicate
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(LabelPredicate)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getCapLabelPredicate_Left()
   * @model containment="true"
   * @generated
   */
  LabelPredicate getLeft();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(LabelPredicate value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(LabelPredicate)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getCapLabelPredicate_Right()
   * @model containment="true"
   * @generated
   */
  LabelPredicate getRight();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(LabelPredicate value);

} // CapLabelPredicate

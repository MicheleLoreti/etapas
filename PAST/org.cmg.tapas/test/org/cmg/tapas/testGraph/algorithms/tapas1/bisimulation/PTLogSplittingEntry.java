package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class PTLogSplittingEntry {
	
	private PTStatesSet ptstatesSet = null;	
	private PTPartition splittedSet = null;
	
	private Action cause = null;
	
	public PTLogSplittingEntry(PTStatesSet ss, PTPartition ro, Action a) {
		ptstatesSet = ss;
		splittedSet = ro;
		cause = a;
	}

	public PTStatesSet getPTStatesSet() {
		return ptstatesSet;
	}
	
	public PTPartition getSplittedSet() {
		return splittedSet;
	}
	
	public Action getCause() {
		return cause;	
	}

}
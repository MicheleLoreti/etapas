/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComposedLtsFactoryImpl extends EFactoryImpl implements ComposedLtsFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static ComposedLtsFactory init()
  {
    try
    {
      ComposedLtsFactory theComposedLtsFactory = (ComposedLtsFactory)EPackage.Registry.INSTANCE.getEFactory(ComposedLtsPackage.eNS_URI);
      if (theComposedLtsFactory != null)
      {
        return theComposedLtsFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new ComposedLtsFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComposedLtsFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case ComposedLtsPackage.MODEL: return createModel();
      case ComposedLtsPackage.ELEMENT: return createElement();
      case ComposedLtsPackage.LTS: return createLts();
      case ComposedLtsPackage.LTS_BODY: return createLtsBody();
      case ComposedLtsPackage.LTS_COMPOSITION: return createLtsComposition();
      case ComposedLtsPackage.COMPOSITION_BODY: return createCompositionBody();
      case ComposedLtsPackage.REFERENCE: return createReference();
      case ComposedLtsPackage.MAPPING: return createMapping();
      case ComposedLtsPackage.LTS_DECLARATION_BODY: return createLtsDeclarationBody();
      case ComposedLtsPackage.LTS_RULE: return createLtsRule();
      case ComposedLtsPackage.LTS_STATE: return createLtsState();
      case ComposedLtsPackage.LABEL_RULE: return createLabelRule();
      case ComposedLtsPackage.STATE: return createState();
      case ComposedLtsPackage.RULE: return createRule();
      case ComposedLtsPackage.LABEL: return createLabel();
      case ComposedLtsPackage.ACTION: return createAction();
      case ComposedLtsPackage.FORMULA: return createFormula();
      case ComposedLtsPackage.HML_FORMULA_DECLARATION: return createHmlFormulaDeclaration();
      case ComposedLtsPackage.HML_FORMULA: return createHmlFormula();
      case ComposedLtsPackage.HML_PREDICATE: return createHmlPredicate();
      case ComposedLtsPackage.HML_TRUE: return createHmlTrue();
      case ComposedLtsPackage.HML_FALSE: return createHmlFalse();
      case ComposedLtsPackage.HML_NOT_FORMULA: return createHmlNotFormula();
      case ComposedLtsPackage.HML_DIAMOND_FORMULA: return createHmlDiamondFormula();
      case ComposedLtsPackage.HML_BOX_FORMULA: return createHmlBoxFormula();
      case ComposedLtsPackage.HML_RECURSION_FORMULA: return createHmlRecursionFormula();
      case ComposedLtsPackage.HML_MIN_FIX_POINT: return createHmlMinFixPoint();
      case ComposedLtsPackage.HML_MAX_FIX_POINT: return createHmlMaxFixPoint();
      case ComposedLtsPackage.HML_RECURSION_VARIABLE: return createHmlRecursionVariable();
      case ComposedLtsPackage.LTL_FORMULA_DECLARATION: return createLtlFormulaDeclaration();
      case ComposedLtsPackage.LTL_FORMULA: return createLtlFormula();
      case ComposedLtsPackage.LTL_ATOMIC_PROPOSITION: return createLtlAtomicProposition();
      case ComposedLtsPackage.LTL_NEXT_FORMULA: return createLtlNextFormula();
      case ComposedLtsPackage.LTL_ALWAYS_FORMULA: return createLtlAlwaysFormula();
      case ComposedLtsPackage.LTL_EVENTUALLY_FORMULA: return createLtlEventuallyFormula();
      case ComposedLtsPackage.LTL_NOT_FORMULA: return createLtlNotFormula();
      case ComposedLtsPackage.LTL_TRUE: return createLtlTrue();
      case ComposedLtsPackage.LTL_FALSE: return createLtlFalse();
      case ComposedLtsPackage.LABEL_PREDICATE: return createLabelPredicate();
      case ComposedLtsPackage.NOT_LABEL_PREDICATE: return createNotLabelPredicate();
      case ComposedLtsPackage.ACTION_LABEL_PREDICATE: return createActionLabelPredicate();
      case ComposedLtsPackage.ANY_LABEL_PREDICATE: return createAnyLabelPredicate();
      case ComposedLtsPackage.INTERLEAVING: return createInterleaving();
      case ComposedLtsPackage.SYNCHRONIZATION: return createSynchronization();
      case ComposedLtsPackage.CONTROLLED_INTERACTION: return createControlledInteraction();
      case ComposedLtsPackage.RENAMING: return createRenaming();
      case ComposedLtsPackage.HML_OR_FORMULA: return createHmlOrFormula();
      case ComposedLtsPackage.HML_AND_FORMULA: return createHmlAndFormula();
      case ComposedLtsPackage.LTL_OR_FORMULA: return createLtlOrFormula();
      case ComposedLtsPackage.LTL_AND_FORMULA: return createLtlAndFormula();
      case ComposedLtsPackage.LTL_UNTIL_FORMULA: return createLtlUntilFormula();
      case ComposedLtsPackage.CUP_LABEL_PREDICATE: return createCupLabelPredicate();
      case ComposedLtsPackage.CAP_LABEL_PREDICATE: return createCapLabelPredicate();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Element createElement()
  {
    ElementImpl element = new ElementImpl();
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Lts createLts()
  {
    LtsImpl lts = new LtsImpl();
    return lts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsBody createLtsBody()
  {
    LtsBodyImpl ltsBody = new LtsBodyImpl();
    return ltsBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsComposition createLtsComposition()
  {
    LtsCompositionImpl ltsComposition = new LtsCompositionImpl();
    return ltsComposition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompositionBody createCompositionBody()
  {
    CompositionBodyImpl compositionBody = new CompositionBodyImpl();
    return compositionBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Reference createReference()
  {
    ReferenceImpl reference = new ReferenceImpl();
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Mapping createMapping()
  {
    MappingImpl mapping = new MappingImpl();
    return mapping;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsDeclarationBody createLtsDeclarationBody()
  {
    LtsDeclarationBodyImpl ltsDeclarationBody = new LtsDeclarationBodyImpl();
    return ltsDeclarationBody;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsRule createLtsRule()
  {
    LtsRuleImpl ltsRule = new LtsRuleImpl();
    return ltsRule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsState createLtsState()
  {
    LtsStateImpl ltsState = new LtsStateImpl();
    return ltsState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelRule createLabelRule()
  {
    LabelRuleImpl labelRule = new LabelRuleImpl();
    return labelRule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public State createState()
  {
    StateImpl state = new StateImpl();
    return state;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Rule createRule()
  {
    RuleImpl rule = new RuleImpl();
    return rule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Label createLabel()
  {
    LabelImpl label = new LabelImpl();
    return label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action createAction()
  {
    ActionImpl action = new ActionImpl();
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Formula createFormula()
  {
    FormulaImpl formula = new FormulaImpl();
    return formula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlFormulaDeclaration createHmlFormulaDeclaration()
  {
    HmlFormulaDeclarationImpl hmlFormulaDeclaration = new HmlFormulaDeclarationImpl();
    return hmlFormulaDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlFormula createHmlFormula()
  {
    HmlFormulaImpl hmlFormula = new HmlFormulaImpl();
    return hmlFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlPredicate createHmlPredicate()
  {
    HmlPredicateImpl hmlPredicate = new HmlPredicateImpl();
    return hmlPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlTrue createHmlTrue()
  {
    HmlTrueImpl hmlTrue = new HmlTrueImpl();
    return hmlTrue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlFalse createHmlFalse()
  {
    HmlFalseImpl hmlFalse = new HmlFalseImpl();
    return hmlFalse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlNotFormula createHmlNotFormula()
  {
    HmlNotFormulaImpl hmlNotFormula = new HmlNotFormulaImpl();
    return hmlNotFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlDiamondFormula createHmlDiamondFormula()
  {
    HmlDiamondFormulaImpl hmlDiamondFormula = new HmlDiamondFormulaImpl();
    return hmlDiamondFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlBoxFormula createHmlBoxFormula()
  {
    HmlBoxFormulaImpl hmlBoxFormula = new HmlBoxFormulaImpl();
    return hmlBoxFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlRecursionFormula createHmlRecursionFormula()
  {
    HmlRecursionFormulaImpl hmlRecursionFormula = new HmlRecursionFormulaImpl();
    return hmlRecursionFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlMinFixPoint createHmlMinFixPoint()
  {
    HmlMinFixPointImpl hmlMinFixPoint = new HmlMinFixPointImpl();
    return hmlMinFixPoint;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlMaxFixPoint createHmlMaxFixPoint()
  {
    HmlMaxFixPointImpl hmlMaxFixPoint = new HmlMaxFixPointImpl();
    return hmlMaxFixPoint;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlRecursionVariable createHmlRecursionVariable()
  {
    HmlRecursionVariableImpl hmlRecursionVariable = new HmlRecursionVariableImpl();
    return hmlRecursionVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlFormulaDeclaration createLtlFormulaDeclaration()
  {
    LtlFormulaDeclarationImpl ltlFormulaDeclaration = new LtlFormulaDeclarationImpl();
    return ltlFormulaDeclaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlFormula createLtlFormula()
  {
    LtlFormulaImpl ltlFormula = new LtlFormulaImpl();
    return ltlFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlAtomicProposition createLtlAtomicProposition()
  {
    LtlAtomicPropositionImpl ltlAtomicProposition = new LtlAtomicPropositionImpl();
    return ltlAtomicProposition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlNextFormula createLtlNextFormula()
  {
    LtlNextFormulaImpl ltlNextFormula = new LtlNextFormulaImpl();
    return ltlNextFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlAlwaysFormula createLtlAlwaysFormula()
  {
    LtlAlwaysFormulaImpl ltlAlwaysFormula = new LtlAlwaysFormulaImpl();
    return ltlAlwaysFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlEventuallyFormula createLtlEventuallyFormula()
  {
    LtlEventuallyFormulaImpl ltlEventuallyFormula = new LtlEventuallyFormulaImpl();
    return ltlEventuallyFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlNotFormula createLtlNotFormula()
  {
    LtlNotFormulaImpl ltlNotFormula = new LtlNotFormulaImpl();
    return ltlNotFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlTrue createLtlTrue()
  {
    LtlTrueImpl ltlTrue = new LtlTrueImpl();
    return ltlTrue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlFalse createLtlFalse()
  {
    LtlFalseImpl ltlFalse = new LtlFalseImpl();
    return ltlFalse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelPredicate createLabelPredicate()
  {
    LabelPredicateImpl labelPredicate = new LabelPredicateImpl();
    return labelPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotLabelPredicate createNotLabelPredicate()
  {
    NotLabelPredicateImpl notLabelPredicate = new NotLabelPredicateImpl();
    return notLabelPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActionLabelPredicate createActionLabelPredicate()
  {
    ActionLabelPredicateImpl actionLabelPredicate = new ActionLabelPredicateImpl();
    return actionLabelPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnyLabelPredicate createAnyLabelPredicate()
  {
    AnyLabelPredicateImpl anyLabelPredicate = new AnyLabelPredicateImpl();
    return anyLabelPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Interleaving createInterleaving()
  {
    InterleavingImpl interleaving = new InterleavingImpl();
    return interleaving;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Synchronization createSynchronization()
  {
    SynchronizationImpl synchronization = new SynchronizationImpl();
    return synchronization;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ControlledInteraction createControlledInteraction()
  {
    ControlledInteractionImpl controlledInteraction = new ControlledInteractionImpl();
    return controlledInteraction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Renaming createRenaming()
  {
    RenamingImpl renaming = new RenamingImpl();
    return renaming;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlOrFormula createHmlOrFormula()
  {
    HmlOrFormulaImpl hmlOrFormula = new HmlOrFormulaImpl();
    return hmlOrFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlAndFormula createHmlAndFormula()
  {
    HmlAndFormulaImpl hmlAndFormula = new HmlAndFormulaImpl();
    return hmlAndFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlOrFormula createLtlOrFormula()
  {
    LtlOrFormulaImpl ltlOrFormula = new LtlOrFormulaImpl();
    return ltlOrFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlAndFormula createLtlAndFormula()
  {
    LtlAndFormulaImpl ltlAndFormula = new LtlAndFormulaImpl();
    return ltlAndFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtlUntilFormula createLtlUntilFormula()
  {
    LtlUntilFormulaImpl ltlUntilFormula = new LtlUntilFormulaImpl();
    return ltlUntilFormula;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CupLabelPredicate createCupLabelPredicate()
  {
    CupLabelPredicateImpl cupLabelPredicate = new CupLabelPredicateImpl();
    return cupLabelPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CapLabelPredicate createCapLabelPredicate()
  {
    CapLabelPredicateImpl capLabelPredicate = new CapLabelPredicateImpl();
    return capLabelPredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComposedLtsPackage getComposedLtsPackage()
  {
    return (ComposedLtsPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static ComposedLtsPackage getPackage()
  {
    return ComposedLtsPackage.eINSTANCE;
  }

} //ComposedLtsFactoryImpl

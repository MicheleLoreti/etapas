package org.cmg.tapas.processAlgebra;

import java.util.LinkedList;

public class RenamingHistory<E> {
	
	private E root;
	private LinkedList<RenamingHistory<E>> children;

	public RenamingHistory(E root){
		this.root = root;
		children = new LinkedList<RenamingHistory<E>>();
	}
	
	public RenamingHistory(E newVal, E oldVal){
		this.root = newVal;
		children = new LinkedList<RenamingHistory<E>>();
		
		RenamingHistory<E> old = new RenamingHistory<E>(oldVal);
		children.add(old);
	}
	
	public E getRoot() { 
		return root;
	}

	public LinkedList<RenamingHistory<E>> getSubTree() {
		return children;
	}

	public void addChild(RenamingHistory<E> c) {
		children.add(c);
	}

	public RenamingHistory<E> getTree(E node) {
		if(root.equals(node)){
			return this;
		}else{
			RenamingHistory<E> res = null;
			for (RenamingHistory<E> child : children) {
				res = child.getTree(node);
				if(res != null)
					return res;
			}
		}		
		return null;
	}

	public void addChildren(LinkedList<RenamingHistory<E>> c) {
		children.addAll(c);
	}
	
	public void merge(RenamingHistory<E> rn){
		if(root.equals(rn.getRoot()))
			children.addAll(rn.getSubTree());
	}
	
	private String _toString(int offset){
		String res = "";

		res += root.toString();		
		String tmp = "";
		
		if(children == null || children.size() == 0)
			return res+"\n";
		
		boolean first = true;
		for (RenamingHistory<E> r : children) {
			if(first){
				res += " - ";
				offset+=res.length();
				res += r._toString(offset);
				first = false;
				for (int i = 0; i < offset-3; i++) {
					tmp += " ";
				}
			}
			else {
				res += tmp+" - "+r._toString(offset);
			}
		}
		
		return res;
	}
	
	@Override
	public String toString(){
		return _toString(0);
	}
	                        
}

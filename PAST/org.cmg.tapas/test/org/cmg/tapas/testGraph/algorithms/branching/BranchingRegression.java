package org.cmg.tapas.testGraph.algorithms.branching;

import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.branching.gv.GVChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.TauDagGenerator;
import org.cmg.tapas.testGraph.algorithms.regression.Regression;
import org.cmg.tapas.testGraph.algorithms.regression.Regression2;
import org.cmg.tapas.testGraph.algorithms.regression.Simulation;


/**
 * BranchingRegression calcola la dipendenza (regressione)
 * tra la dimensione del grafo e il tempo per calcolare 
 * la bisimulazione.
 *
 * @author Guzman Tierno
 **/
public class BranchingRegression {
	
	public static void main(String[] args) {
		Simulation sim = new BranchSimulation();
		sim.simulate();

		System.out.println( sim.regression(Regression.POLYNOMIAL) + "\n" );		
		System.out.println( sim.regression(Regression.EXPONENTIAL) + "\n" );		
		System.out.println( sim.regression(Regression.LINEARITMIC) + "\n" );		
		System.out.println( sim.regression(Regression.LOGARITMIC) + "\n" );		
		System.out.println( sim.regression(Regression.QUADRATIC) + "\n" );		
		System.out.println( sim.regression(Regression.LINEAR) + "\n" );		
		System.out.println( sim.regression(Regression.LINEAR_LOG) + "\n" );		
		System.out.println( "Migliore:\n" + sim.bestRegression() );		
		System.out.println( sim.regression2(Regression2.QUADRATIC) + "\n" );
		
		double[] dims = sim.getDimensions();
		double[] means = sim.getMeanValues();		
		System.out.print( "plot( [" );
		for( int i=0; i<dims.length; ++i ) {
			System.out.print( dims[i] );		
			if( i<dims.length-1 )
				System.out.print( ", " );		
		}
		System.out.print( "], [" );
		for( int i=0; i<means.length; ++i ) {
			System.out.print( means[i] );		
			if( i<means.length-1 )
				System.out.print( ", " );		
		}
		System.out.println( "], 'r' );" );
	}	

}



class BranchSimulation extends Simulation {

	public BranchSimulation() {
		super(
			1,	 						// prove	
			400,						// dim base
			200,						// gap 
			16,							// gradi (numero di dimensioni)
			Simulation.SORT,
			Simulation.NULL_VECTOR
		);
	}

	private int actionCount = 4;

	private LtsAction[] actions;
	{
		actions = new LtsAction[actionCount]; 
		actions[0] = LtsAction.TAU;
		for(int i=1; i<actionCount; ++i )
			actions[i] = new LtsAction( i + "" );
	}
	
	private double tauProbability = 0.3;

	//	private RandomGraphGenerator<LtsState,LtsAction> generator = 
	//		new RandomGraphGenerator<LtsState,LtsAction>(new GraphItemFactory());
	private TauDagGenerator<LtsState,LtsAction> generator = 
		new TauDagGenerator<LtsState,LtsAction>(new GraphItemFactory());
	
	@Override 
	public double algorithm(int dim, int[] v) {
		double start;
		double time = 0;
		
		//double connectionProbability = 1.72/(dim * tauProbability);
		double connectionProbability = 0.005;
		
		for( int i=0; i<1; ++i ) {
			//	if( i%2==0 )
			//		System.out.println( dim + ": " + i );
	
			PointedGraph<LtsState,LtsAction> pointedGraph = 
				generator.generateRandomGraph(
					dim, actions, connectionProbability, tauProbability
				);
			GraphInterface<LtsState,LtsAction> graph = pointedGraph.getGraph();
			
		 	// gv
			start = System.currentTimeMillis();
			GVChecker<LtsState,LtsAction> checker = 
				new GVChecker<LtsState,LtsAction>(graph);			
			checker.computeEquivalence(null);
			time += System.currentTimeMillis() - start - GVChecker.collapseTime;

		 	// gvks
//			start = System.currentTimeMillis();
//			GVKSChecker<LtsState,LtsAction> checker = 
//				new GVKSChecker<LtsState,LtsAction>(graph);			
//			checker.computeEquivalence(null);
//			time += System.currentTimeMillis() - start - GVKSChecker.collapseTime;
		
			GraphInterface<?,LtsAction> newGraph = checker.getNewGraph();
			System.out.println( 
				dim + " " +
				newGraph.numStates() + " " +
				newGraph.numEdges() + " " +
				((double)newGraph.getEdgeCount(LtsAction.TAU)/newGraph.numEdges()) +
				" " + time
			);
								
		}
		
		System.gc();
		return time;
	}
	
	@Override 
	public double theoricValue(double n) {
		return n;
	}
}


/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Diamond Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getAction <em>Action</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getArg <em>Arg</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlDiamondFormula()
 * @model
 * @generated
 */
public interface HmlDiamondFormula extends HmlFormula
{
  /**
   * Returns the value of the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Action</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Action</em>' containment reference.
   * @see #setAction(LabelPredicate)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlDiamondFormula_Action()
   * @model containment="true"
   * @generated
   */
  LabelPredicate getAction();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getAction <em>Action</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Action</em>' containment reference.
   * @see #getAction()
   * @generated
   */
  void setAction(LabelPredicate value);

  /**
   * Returns the value of the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' containment reference.
   * @see #setArg(HmlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlDiamondFormula_Arg()
   * @model containment="true"
   * @generated
   */
  HmlFormula getArg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getArg <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' containment reference.
   * @see #getArg()
   * @generated
   */
  void setArg(HmlFormula value);

} // HmlDiamondFormula

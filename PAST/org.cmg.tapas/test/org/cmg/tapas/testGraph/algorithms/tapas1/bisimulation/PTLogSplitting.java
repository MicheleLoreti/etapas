package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;


/**
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class PTLogSplitting {
	
	private Vector<PTLogSplittingEntry> log = null;
	
	public PTLogSplitting() {
		log = new Vector<PTLogSplittingEntry>();
	}
	
	public void addEntry(PTLogSplittingEntry e) {
		if( e!=null ) 
			log.addElement(e);
	}
	
	public int getDimension() {
		return log.size();	
	}
	
	public PTLogSplittingEntry entryAt(int index) {
		try	{ 
			return log.elementAt(index); 
		}
		catch(ArrayIndexOutOfBoundsException e) { 
			return null; 
		}
	}

	public PTLogSplittingEntry getDeppestCause(
		State s1, 
	    State s2
	) {
		PTLogSplittingEntry e = null;
		PTStatesSet ptstatesSet = null;
		
		//Cerco il blocco pi� in profondit� che contiene s1 s2
		for (int i=getDimension()-1; i>=0; i--) {
			e = entryAt(i);	
			ptstatesSet = e.getPTStatesSet();
			if( ptstatesSet!=null )
				if( ptstatesSet.containState(s1) )
					if( ptstatesSet.containState(s2) )
						return e;
		}
		return null;
	}	

	public PTLogSplittingEntry getDeppestCause(
		PTState s1, 
		PTState s2
	) {
		PTLogSplittingEntry e = null;
		PTStatesSet ptstatesSet = null;
		
		//Cerco il blocco pi� in profondit� che contiene s1 s2
		for( int i=getDimension()-1; i>=0; i-- ) {
			e = entryAt(i);	
			ptstatesSet = e.getPTStatesSet();
			if( ptstatesSet!=null )
				if( ptstatesSet.containPTState(s1) )
					if( ptstatesSet.containPTState(s2) )
						return e;
		}
		
		return null;
	}	
	
	public PTLogSplittingEntry getDeppestCause(
		PTStatesSet ss1, 
	    PTStatesSet ss2
	) {
		PTLogSplittingEntry e = null;
		PTStatesSet ptstatesSet = null;
		
		//Cerco il blocco pi� in profondit� che contiene ss1 ss2
		for( int i=getDimension()-1; i>=0; i-- ) {
			e = entryAt(i);	
			ptstatesSet = e.getPTStatesSet();
			if( ptstatesSet!=null )
				if( ptstatesSet.containPTStatesSet(ss1) )
					if( ptstatesSet.containPTStatesSet(ss2) )
						return e;
		}
		
		return null;
	}	



}
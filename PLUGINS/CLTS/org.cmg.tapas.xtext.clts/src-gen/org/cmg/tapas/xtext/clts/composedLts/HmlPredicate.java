/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlPredicate#getLabel <em>Label</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlPredicate()
 * @model
 * @generated
 */
public interface HmlPredicate extends HmlFormula
{
  /**
   * Returns the value of the '<em><b>Label</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Label</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Label</em>' reference.
   * @see #setLabel(Label)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlPredicate_Label()
   * @model
   * @generated
   */
  Label getLabel();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlPredicate#getLabel <em>Label</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Label</em>' reference.
   * @see #getLabel()
   * @generated
   */
  void setLabel(Label value);

} // HmlPredicate

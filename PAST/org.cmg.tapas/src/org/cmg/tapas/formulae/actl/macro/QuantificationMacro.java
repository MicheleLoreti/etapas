/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.state.ActlQuantification;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class QuantificationMacro< S extends APProcess<S,A>,A extends ActionInterface> extends StateFormulaMacro<S, A>{
	
	private PathFormulaMacro<S, A> macro;
	private boolean isForAll;
	
	public QuantificationMacro( PathFormulaMacro<S, A> macro , boolean isForAll ) {
		this.macro = macro;
		this.isForAll = isForAll;
	}
	
	@Override
	public ActlFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		return new ActlQuantification<S, A>(isForAll,macro.getFormula(f));
	}

	@Override
	public int parameters() {
		return macro.parameters();
	}

	@Override
	public void acceptVisitor(MacroVisitor<S, A> visitor) {
		visitor.visitQuantificationMacro(this);
	}

	public boolean isForAll() {
		return isForAll;
	}

	public PathFormulaMacro<S, A> getArgument() {
		return macro;
	}
}

package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.HashMap;
import java.util.Vector;

/**
 * La classe ProcessAG rappresenta un Acceptance Graph.
 * A ciascuno stato viene fatto corrispondere un Acceptance set.
 *
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class ProcessAG extends Lts {

	/**
	 * Insieme degli Acceptance set degli stati. 
	 **/	
	private Vector<AcceptanceSet> acceptanceSetSet = new Vector<AcceptanceSet>();

	/**
	 * Restituisce l'insieme degli Acceptance set.
	 **/	 	
  	public Vector<AcceptanceSet> getAcceptanceSetSet() {
  		return acceptanceSetSet;	
  	}

	/**
	 * Restituisce l'Acceptance set relativo allo stato specificato.
	 **/	 
  	public AcceptanceSet getAcceptanceSet(State s) {
		int k = getStatesSet().getStateIndex(s.getName());
		return getAcceptanceSetSet().elementAt(k);  		
  	}

	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt> 
	 * inizializzando a <tt>null</tt> l'acceptance set associato a tale stato. 
	 **/
	@Override 
  	public void addState(State s) {
  		addState(s, null);
  	}  	

	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt>.
	 **/	   	
  	public void addState(State s, AcceptanceSet acceptanceSet) {
  		StatesSet ss = getStatesSet(); 		
  		int i = ss.addState(s);
  		if( ss.getDimension() != acceptanceSetSet.size() )
  			acceptanceSetSet.add(i,acceptanceSet);
  	} 
  	
  	/**
	 * Aggiunge all'insieme degli stati un nuovo stato
	 * con Acceptance set <tt>null</tt>. 
  	 **/
	@Override 
  	public State addNewState() {
  		return addNewState(null);	
  	}  	

  	/**
	 * Aggiunge all'insieme degli stati un nuovo oggetto di tipo <tt>State</tt> 
	 * con Acceptance set specificato nel parametro. 
  	 **/
  	public State addNewState(AcceptanceSet acceptanceSet) {
  		State state = new State(getNewStateName());
 		int i = getStatesSet().addState( state );
  		acceptanceSetSet.add(i, acceptanceSet);
  		
  		return state;
  	}  	
 	  	
	/**
	 * Crea un nuovo Acceptance Graph derivato dalla fusione di due 
	 * Acceptance Graphs. 
	 * <p>
	 * Durante l'operazione di fusione vengono generati stati 
	 * nuovi all'interno del nuovo AG.
	 **/
	public static ProcessAG createProcessFusion(
		ProcessAG p1, 
		ProcessAG p2
	) {
		ProcessAG p = new ProcessAG();

		// Fondo l'insieme delle azioni
		HashMap<Action,Action> hmA = new HashMap<Action,Action>();
		ActionsSet actionsSet = p1.getActionsSet();
		for( int i=0; i<actionsSet.getDimension(); i++ ) {
			int actualIndex = p.addAction( actionsSet.actionAt(i) );
			hmA.put(
				actionsSet.actionAt(i), p.getActionsSet().actionAt(actualIndex)
			);
		}
		actionsSet = p2.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			int actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(
				actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex)
			);
		}

		// Fondo l'insieme degli stati, rinominando gli stati con lo stesso nome
		// e fondo anche i rispettivi acceptance set
		StatesSet statesSet = p1.getStatesSet();
		Vector<AcceptanceSet> acceptanceSetSet = p1.getAcceptanceSetSet();
		HashMap<State,State> hmS = new HashMap<State,State>();
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			AcceptanceSet newAccSet = null;
			AcceptanceSet accSet = acceptanceSetSet.elementAt(i);
			if( accSet != null ) {
				newAccSet = new AcceptanceSet();
				for( int j=0; j<accSet.getDimension(); j++ ) {
					actionsSet = accSet.actionsSetAt(j);
					ActionsSet newActionsSet = new ActionsSet();
					for( int k=0; k<actionsSet.getDimension(); k++ )
						newActionsSet.addAction(hmA.get(actionsSet.actionAt(k)));
					newAccSet.addActionsSet(newActionsSet);
				}
			}
			hmS.put(statesSet.stateAt(i), p.addNewState(newAccSet));
		}
		statesSet = p2.getStatesSet();
		acceptanceSetSet = p2.getAcceptanceSetSet();
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			AcceptanceSet newAccSet = null;
			AcceptanceSet accSet = acceptanceSetSet.elementAt(i);
			if( accSet != null ) {			
				newAccSet = new AcceptanceSet();
				for( int j=0; j<accSet.getDimension(); j++ ) {
					actionsSet = accSet.actionsSetAt(j);
					ActionsSet newActionsSet = new ActionsSet();
					for (int k=0; k<actionsSet.getDimension(); k++)
						newActionsSet.addAction(hmA.get(actionsSet.actionAt(k)));
					newAccSet.addActionsSet(newActionsSet);
				}
			}
			hmS.put(statesSet.stateAt(i), p.addNewState(newAccSet));
		}

		
		State stateOrigin = null;
		State stateDest = null;
		StatesSet destinations = null;

		// Creo la relazione di transizione del processo fuso
		TransitionCouple tc;
		Vector<TransitionCouple> transitionCouples;		
		statesSet = p1.getStatesSet();
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for( int j=0; j<transitionCouples.size(); j++ ) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for( int k=0; k<destinations.getDimension(); k++ ) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					Action a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}
		statesSet = p2.getStatesSet();
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for( int j=0; j<transitionCouples.size(); j++ ) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for( int k=0; k<destinations.getDimension(); k++ ) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					Action a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}

		//Setto i 2 stati iniziali del nuovo processo
		p.setInitState( hmS.get(p1.getInitState()) );
		p.setSecondInitState( hmS.get(p2.getInitState()) );
		
		return p;
	}
  	
	/**
	 * Costruisce ricorsivamente l'Acceptance Graph 
	 * corrispondente al processo passato come parametro il cui
	 * stato iniziale sia associato all'insieme di stati 
	 * del processo originale passato anch'esso come parametro. 
	 * E' un metodo di supporto ai due metodi <tt>buildAcceptanceGraph</tt> e 
	 * <tt>buildWeakAcceptanceGraph</tt>.
	 **/
	private static State buildAG(
		Lts p,
		ProcessAG p1,
		StatesSet s,
		boolean b,
		HMap h
	) {		
		State tnew = h.getElement(s,b);
		if( tnew!=null ) 
			return tnew;			

		AcceptanceSet accSet = null;
		if( b ) 
			accSet = s.aP();

		State t = p1.addNewState(accSet);
		h.addItem(s, b, t);
		ActionsSet as = p.getActionsSet();
		
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i); 
			if( !alfa.isTau() ) {
				StatesSet image = s.dP(alfa);
				if( !image.isEmpty() ) {
					StatesSet salfa = image.sEpsilon();
					boolean c = false;
					if( b ) 
						c = salfa.statesSetConvergent();
					tnew = buildAG(p, p1, salfa, c, h);
					t.addTransition(alfa, tnew);
				}
			}
		}

		return t;
	}

	/**
	 * Costruisce ricorsivamente lo Strong Acceptance Graph 
	 * corrispondente al processo passato come parametro il cui
	 * stato iniziale sia associato all'insieme di stati del 
	 * processo originale passato anch'esso come parametro. 
	 * E' un metodo di supporto al metodo <tt>buildStrongAcceptanceGraph</tt>
	 * per la costruzione dello Strong Acceptance Graph associato ad un processo.
	 **/
	private static State buildSAG(
		Lts p,
		ProcessAG p1,
		StatesSet s,
		boolean b,
		HMap h
	) {
		State tnew = h.getElement(s, b);
		if( tnew!=null ) 
			return tnew;

		AcceptanceSet accs = null;
		if( b ) 
			accs = s.aP();

		State t = p1.addNewState(accs);
		h.addItem(s, b, t);		
		if( !b ) 
			return t;
			
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i); 
			if( !alfa.isTau() ) {
				StatesSet dp = s.dP(alfa);
				if( !dp.isEmpty() ) {
					StatesSet salfa = dp.sEpsilon();
					boolean c = false;
					if( b ) 
						c = salfa.statesSetConvergent();						
					tnew = buildSAG(p, p1, salfa, c, h);
					t.addTransition(alfa,tnew);
				}
			}
		}
		return t;
	}

	/**
	 * Costruisce l'Acceptance Graph corrispondente 
	 * al processo passato come parametro.
	 **/	
	public static ProcessAG buildAcceptanceGraph(Lts p) {
		ProcessAG p1 = new ProcessAG();
		HMap h = new HMap();

		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i);
			if( !alfa.isTau() ) 
				p1.addAction(alfa);
		}
		
		StatesSet sm = p.getInitState().sEpsilon();
		State initState = buildAG(p,p1,sm,sm.statesSetConvergent(),h);
		
		p1.setInitState(initState);		
		return p1;
	}

	/**
	 * Costruisce il Weak Acceptance Graph corrispondente al 
	 * processo passato come parametro.
	 **/		
	public static ProcessAG buildWeakAcceptanceGraph(Lts p) {
		ProcessAG p1 = new ProcessAG();
		HMap h = new HMap();
		
		ActionsSet as = p.getActionsSet();		
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i);
			if( !alfa.isTau() ) 
				p1.addAction(alfa);
		}
		
		StatesSet sm = p.getInitState().sEpsilon();
		State initState = buildAG(p, p1, sm, false, h);

		p1.setInitState(initState);
		return p1;
	}

	/**
	 * Costruisce lo Strong Acceptance Graph corrispondente al 
	 * processo passato come parametro.
	 **/
	public static ProcessAG buildStrongAcceptanceGraph(Lts p) {
		ProcessAG p1 = new ProcessAG();
		HMap h = new HMap();

		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i);
			if( !alfa.isTau() ) 
				p1.addAction(alfa);
		}
		
		StatesSet sm = p.getInitState().sEpsilon();
		State initState = buildSAG(p, p1, sm, sm.statesSetConvergent(), h);
		p1.setInitState(initState);
		return p1;
	}	

	/**
	 * Stampa l'intero Acceptance Graph sullo standard output. 
	 **/
	@Override 
	public void print() {
		System.out.print("  |-- States Set:\n");
		StatesSet ss = getStatesSet();
		AcceptanceSet accs = null;
		int i = 0;	
		System.out.print("           {");
		for( i=0; i<ss.getDimension(); i++ ) {
			if(i>0) 
				System.out.print(",\n            ");
			System.out.print("(");
			ss.stateAt(i).print();
			accs = acceptanceSetSet.elementAt(i);
			if( accs!=null ) {
				System.out.print(", ");
				accs.print();
			}
			System.out.print(")");
		}
		if( i==0 ) 
			System.out.print("empty");
		
		System.out.print("}");
		System.out.print("\n  |-- Actions Set: ");
		
		getActionsSet().print();
		
		System.out.print("\n  |-- Transition Relation:\n");		
		getStatesSet().printTransitions();
		
		System.out.print("  |-- Initial State: ");
		if( getInitState()==null ) 
			System.out.print("nessuno");
		else 
			getInitState().print();
			
		System.out.print("\n\n");
	}

}
/**
 */
package org.lic.secsi.impl;

import org.eclipse.emf.ecore.EClass;

import org.lic.secsi.SecsiPackage;
import org.lic.secsi.Signal;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SignalImpl extends TypeExpressionImpl implements Signal
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SignalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.SIGNAL;
  }

} //SignalImpl

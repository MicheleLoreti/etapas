/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.formulae.actl.path.ActlUntil;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class UntilMacro< S extends APProcess<S, A> , A extends ActionInterface> extends PathFormulaMacro<S, A> {

	private StateFormulaMacro<S, A> leftMacro;
	private ActlActionPredicate<S,A> leftPredicate;
	private ActlActionPredicate<S,A> rightPredicate;
	private StateFormulaMacro<S, A> rightMacro;
	
	/**
	 * @param leftMacro
	 * @param leftPredicate
	 * @param rightPredicate
	 * @param rightMacro
	 */
	public UntilMacro(StateFormulaMacro<S, A> leftMacro,
			ActlActionPredicate<S,A> leftPredicate,
			ActlActionPredicate<S,A> rightPredicate,
			StateFormulaMacro<S, A> rightMacro) {
		super();
		this.leftMacro = leftMacro;
		this.leftPredicate = leftPredicate;
		this.rightPredicate = rightPredicate;
		this.rightMacro = rightMacro;
	}

	@Override
	public PathFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		if (f.length != parameters()) {
			throw new IllegalArgumentException();
		}
		ActlFormula<S, A>[] left = new ActlFormula[ leftMacro.parameters() ];
		ActlFormula<S, A>[] right = new ActlFormula[ rightMacro.parameters() ];
		System.arraycopy(f, 0, left, 0, leftMacro.parameters());
		System.arraycopy(f, 0, right, leftMacro.parameters(), rightMacro.parameters());
		
		return new ActlUntil<S, A>( leftMacro.getFormula(left) , leftPredicate , rightPredicate , rightMacro.getFormula(right));
		
	}

	@Override
	public int parameters() {
		return leftMacro.parameters()+rightMacro.parameters();
	}

	@Override
	public void accept(PathMacroVisitor<S, A> visitor) {
		visitor.visitUntil(this);
	}

	public ActlActionPredicate<S,A> getLeftPredicate() {
		return leftPredicate;
	}

	public ActlActionPredicate<S,A> getRightPredicate() {
		return leftPredicate;
	}

	public StateFormulaMacro<S, A> getLeftMacro() {
		return leftMacro;
	}

	public StateFormulaMacro<S, A> getRightMacro() {
		return rightMacro;
	}
}

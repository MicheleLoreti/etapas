package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;


/**
 * La classe BranchingChecker realizza l'algoritmo di verifica 
 * per l'equivalenza branching bisimulation. 
 * <p>
 * @author Gori Massimiliano
 */
public class TBranchingChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {

    /** 
     * Costruisce una nuova istanza del checker 
     **/
	public TBranchingChecker() {
		super("Branching bisimulation");
	}
	
	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 **/	
	@Override 
	public boolean checkEquivalence() {
		Lts p1 = Lts.createProcess( getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess( getSecondAutomaton(), getInitState2(), null );

		Lts p1T = Lts.buildNoCicleGraph( p1 );
		Lts p2T = Lts.buildNoCicleGraph( p2 );		

		Lts pT = Lts.createProcessFusion( p1T, p2T );
				
		BBPartition initialRo = BBUtil.initBBPartition( pT );
		
		// Groote VaanDrager 
		BBState initialBBState1 = initialRo.getBBState(
			pT.getInitState().getName()
		);
		BBState initialBBState2 = initialRo.getBBState(
			pT.getSecondInitState().getName()
		);		
		BBLogSplitting logSplitting = null;
		BBLogInfo logInfo = null;		
		if( getVerboseSolution() ) {
			logSplitting = new BBLogSplitting();
			logInfo = new BBLogInfo();		
		}
		
		Partition ro = BBUtil.computeBBPartitionRefinement(
			pT, initialRo, initialBBState1, initialBBState2, 
			logSplitting, logInfo
		);

		boolean result = 
			ro.checkSameClass( pT.getInitState(), pT.getSecondInitState() );
			
		if( !result && getVerboseSolution() ) {
			BBUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialBBState1, initialBBState2,
				initialBBState1.getState().getName(), 
				initialBBState2.getState().getName(), 
				pT
			);
		}
		
		return result;
	}
	
}
package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * La classe PresetCouple rappresenta le transizioni entranti 
 * in uno stato di tipo <tt>PTState</tt>, etichettate con
 * una determinata etichetta. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class PresetCouple {
	
	/**
	 * Rappresenta l'azione con cui sono etichettate 
	 * le transizioni rappresentate da questo oggetto.
	 **/		
	private Action a = null;

	/**
	 * Rappresenta l'insieme di stati origine delle transizioni 
	 * rappresentate da questo oggetto.
	 **/		
	private PTStatesSet ptstatesPreset = null;
	
	/**
	 * Costruisce una nuova coppia con azione specificata 
	 * nel parametro di ingresso ed insieme di stati
	 * di origine vuoto.
	 **/		
	public PresetCouple(Action a) {
		this.a = a;
		ptstatesPreset = new PTStatesSet();	
	}

	/**
	 * Restituisce l'azione che etichetta tutte le transizioni 
	 * rappresentate da questo oggetto.
	 **/		
	public Action getAction() {
		return this.a;	
	}

	/**
	 * Restituisce l'insieme di stati di origine delle transizioni 
	 * rappresentate da questo oggetto.
	 * <p>
	 **/		
	public PTStatesSet getPTStatesPreset() {
		return this.ptstatesPreset;	
	}

	/**
	 * Aggiunge all'insieme degli stati di origine un nuovo oggetto 
	 * di tipo <tt>PTState</tt>. 
	 **/		
	public void addPTState(PTState s) {
		this.ptstatesPreset.addPTState(s);	
	}

	/**
	 * Stampa tutte le transizioni sullo standard output.
	 **/
	public void print() {
		System.out.print("{"+a.getName()+", ");
		ptstatesPreset.print();
		System.out.print("}");
	}
}
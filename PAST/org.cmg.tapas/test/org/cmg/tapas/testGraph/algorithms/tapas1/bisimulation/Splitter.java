package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

/**
 * La classe Splitter rappresenta uno splitter 
 * per gli algoritmi di calcolo della bisimulazione.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class Splitter {
	
	private PTStatesSet root = null;	
	private PTStatesSet rightChild = null;	
	private PTStatesSet leftChild = null;
	
	public Splitter(PTStatesSet root) {
		this.root = root;	
	}

	public Splitter(
		PTStatesSet root, 
		PTStatesSet leftChild, 
		PTStatesSet rightChild
	) {
		this.root = root;
		this.leftChild = leftChild;
		this.rightChild = rightChild;
	}
	
	public boolean isCompound() {
		return rightChild!=null || leftChild!=null;
	}

	public boolean isSimple() {
		return !isCompound();
	}
	
	public PTStatesSet getRoot() {
		return root;		
	}

	public PTStatesSet getLeftChild() {
		return leftChild;		
	}

	public PTStatesSet getRightChild() {
		return rightChild;		
	}
	
	public PTStatesSet getMinorChild() {
		if( isCompound() ) {
			if( leftChild.getDimension() <= rightChild.getDimension() ) 
				return leftChild;
			
			return rightChild;
		}
		
		return null;
	}

	public PTStatesSet getMajorChild() {
		if( isCompound() ) {
			if( leftChild.getDimension() <= rightChild.getDimension() ) 
				return rightChild;
			
			return leftChild;
		}
		
		return null;
	}

	public boolean containBlock(PTStatesSet b) {
		if( isCompound() ) {
			return leftChild == b || rightChild == b; 
		}
		
		return false;
	}

}
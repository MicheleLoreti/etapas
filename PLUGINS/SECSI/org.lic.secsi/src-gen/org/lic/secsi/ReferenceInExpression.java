/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference In Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.ReferenceInExpression#getReference <em>Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getReferenceInExpression()
 * @model
 * @generated
 */
public interface ReferenceInExpression extends Expression
{
  /**
   * Returns the value of the '<em><b>Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reference</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' reference.
   * @see #setReference(ReferenceableElement)
   * @see org.lic.secsi.SecsiPackage#getReferenceInExpression_Reference()
   * @model
   * @generated
   */
  ReferenceableElement getReference();

  /**
   * Sets the value of the '{@link org.lic.secsi.ReferenceInExpression#getReference <em>Reference</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' reference.
   * @see #getReference()
   * @generated
   */
  void setReference(ReferenceableElement value);

} // ReferenceInExpression

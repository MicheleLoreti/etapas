package org.lic.secsi.tests;

import com.google.inject.Inject;
import com.google.inject.Provider;
import java.io.FileInputStream;
import org.eclipse.emf.common.util.URI;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.junit4.InjectWith;
import org.eclipse.xtext.junit4.XtextRunner;
import org.eclipse.xtext.junit4.util.ParseHelper;
import org.eclipse.xtext.junit4.validation.ValidationTestHelper;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lic.SecsiInjectorProvider;
import org.lic.secsi.Program;

@InjectWith(SecsiInjectorProvider.class)
@RunWith(XtextRunner.class)
@SuppressWarnings("all")
public class TestParser {
  @Inject
  private ParseHelper<Program> parser;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Inject
  private Provider<XtextResourceSet> resourceSetProvider;
  
  @Test
  public void testPaerserEmpty() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("process test() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("boolean flag;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("flag = true||flag;");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      Program _parse = this.parser.parse(_builder);
      this._validationTestHelper.assertNoErrors(_parse);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testParseBrp() {
    try {
      FileInputStream _fileInputStream = new FileInputStream("Examples/leaderelection.secsi");
      URI _createFileURI = URI.createFileURI("Examples/leaderelection.secsi");
      XtextResourceSet _get = this.resourceSetProvider.get();
      Program _parse = this.parser.parse(_fileInputStream, _createFileURI, null, _get);
      this._validationTestHelper.assertNoErrors(_parse);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}

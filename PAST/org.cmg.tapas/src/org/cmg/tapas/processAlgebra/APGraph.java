/*********************************************************************
  * FILE:  APGraph.java                                               *
  *                                                                   *
  * Copyright (C) 2006 Francesco Calzolai                             *
  *                                                                   *
  * This is free software; you can redistribute it and/or             *
  * modify it under the terms of the GNU General Public License       *
  * as published by the Free Software Foundation; either version 2    *
  * of the License, or (at your option) any later version.            *
  *                                                                   *
  * This software is distributed in the hope that it will be useful,  *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of    *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *
  * GNU General Public License for more details.                      *
  *                                                                   *
  * You should have received a copy of the GNU General Public License *
  * along with JAgent; if not, write to the Free Software             *
  * Foundation, Inc., 51 Franklin St, Fifth Floor,                    *
  * Boston, MA  02110-1301  USA                                       *
  *                                                                   *
  *********************************************************************/
package org.cmg.tapas.processAlgebra;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.EdgeFilter;
import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphData;
import org.cmg.tapas.graph.StateSet;
import org.cmg.tapas.graph.filter.Filter;

/** 
 * La classe mette a disposizione i metodi per gestire un grafo 
 * generato da un processo <i>p</i> in una generica algebra di 
 * processo. Questa classe &egrave; parametrica rispetto al concetto di
 * processo ed a quello di azione.
 *  
 * @see ccsGraph.CCSGraph
 * 
 * @param <P> la definizione di processo.
 * @param <R> la definizione di azione.
 *  
 * @author Francesco Calzolai
 * @author Michele Loreti
 * @version 
 */
public class APGraph<P extends APProcess<P, R>,R extends ActionInterface> implements GraphData<P, R> {
		
	/*Lista di stati che non sono stati espansi. */
	private LinkedList<P> notExpanded;
	
	/*Lista di stati gi� creati*/
	private HashSet<P> yetExpanded;

	private Graph<P,R> graph;
	
	private Vector<Set<P>> layers;
	
	private boolean stopExpand;
	
	/**Costruisce un <i>APGraph</i> contenente un solo stato chiamato
	 *  <i>name</i> che contiene il processo <i>process</i>.
	 * 
	 * @param process Il processo contenuto nello stato. 
	 */
	public APGraph(P process) {		
		notExpanded = new LinkedList<P>();
		yetExpanded = new HashSet<P>();
		graph = new Graph<P, R>();
		notExpanded.add(process);		
		graph.addState(process);
		stopExpand= false;
		layers = new Vector<Set<P>>();
	}
	
	@SuppressWarnings("unchecked")
	public APGraph( APGraph<P,R> lts) {
		this.notExpanded = (LinkedList<P>) lts.notExpanded.clone();
		this.yetExpanded = (HashSet<P>) lts.yetExpanded.clone();
		this.graph = lts.graph.clone();
		stopExpand= false;
	}
	
	/** Aggiunge uno stato di nome <i>name</i> contenente il 
	 * processo <i>process</i>. Se il grafo non contiene gi� uno
	 * stato di nome <i>name</i> lo stato viene aggiunto e viene
	 * restituito vero; altrimenti lo stato non viene aggiunto e
	 * viene restituito falso.
	 * 
	 * @param process Il processo contenuto nello stato.
	 * @return <i>True</i> se ha aggiunto lo stato; <i>False</i> se
	 *  esisteva gi� uno stato con ID uguale a <i>name</i>.
	 */
	public boolean addProcess( P process) {
		if (graph.stateIsIn(process)) return false;
		
		graph.addState(process);
		notExpanded.add(process);
		return true;
	}
	
	public Set<P> getLayer( int i ) {
		return layers.get(i);
	}
	
	protected Set<P> newLayer() {
		HashSet<P> layer = new HashSet<P>();
		layers.add(layer);
		return layer;
	}

	
	/**Dato uno stato <i>s</i>, restituisce <i>True</i> se lo stato
	 *  <i>s</i> � contenuto nel grafo. Restituisce <i>False</i> altrimenti.
	 *   
	 * @param state Il nome dello stato.
	 * @return <i>True</i> se lo stato � contenuto nel grafo; 
	 * <i>False</i> altrimenti.
	 */
	public boolean containState(String state){
		return graph.stateIsIn(state); 
	}
	
	/**Dati uno stato <i>s</i> e un filtro <i>f</i>, espande lo stato
	 * <i>s</i> se � verificato il filtro <i>f</i>.
	 * @param s Il nome dello stato.
	 * @param f Il filtro che deve essere verificato.
	 */
	public void expand(P s, Filter<R> f){
		//TODO
	};
	
	/**Dato un filtro <i>f</i>, espande tutti gli stati che
	 *  verificano il filtro <i>f</i>.
	 *  
	 * @param f Il filtro che deve essere verificato.
	 */	
	public void expand(Filter<R> f){
		//TODO
	};
	
	
	/** 
	 * Metodo che espande tutti gli stati del grafo. 
	 */
	public void expandAll(){
		//TODO: L'implementazione di questo metodo potrebbe essere cambiata
//		long start = System.currentTimeMillis();
		P s;
		HashSet<P> foo = new HashSet<P>();
		foo.addAll(notExpanded);
		while(!notExpanded.isEmpty() && !stopExpand){
			s = notExpanded.removeFirst();
			foo.remove(s);
			yetExpanded.add(s);
			Map<R, LinkedList<P>> nextMap = s.getNext();
			for (R action : nextMap.keySet()) {
				for (P dest : nextMap.get(action)) {
					graph.addEdge(s , action , dest );
					if ((!yetExpanded.contains(dest))&&(!foo.contains(dest))) {
						notExpanded.add(dest);
						foo.add(dest);
					}
				}
				
			}
		}
//		System.out.println(System.currentTimeMillis() - start);
	}
	
	public void stopExpand(){
		stopExpand = true;
	}


	/**
	 * Rende lo stato il cui nome coincide 
	 * con la stringa passata come parametro 
	 * oppure rende <code>null</code> se un tale stato non 
     * � presente nel grafo.
     * 
     * @param name Nome dello stato da ricercare.
     * @return Lo stato col nome specificato.
	 * @see org.cmg.tapas.graph.GraphInterface#getState(java.lang.String)
	 */
	public P getState(String name){
		return graph.getState(name);
	}


	/**
	 * @param from
	 * @param action
	 * @param to
	 * @return
	 * @see org.cmg.tapas.graph.Graph#edgeIsIn(org.cmg.tapas.graph.StateInterface, org.cmg.tapas.graph.ActionInterface, org.cmg.tapas.graph.StateInterface)
	 */
	public boolean edgeIsIn(P from, R action, P to) {
		return graph.edgeIsIn(from, action, to);
	}
	
	public boolean edgeIsIn(P from , P to ) {
		return graph.edgeIsIn(from, to);
	}


	/**
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getActionSet()
	 */
	public Set<R> getActionSet() {
		return graph.getActionSet();
	}


	/**
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getAllStates()
	 */
	public LinkedList<P> getAllStates() {
		return graph.getAllStates();
	}


	/**
	 * @param action
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getEdgeCount(org.cmg.tapas.graph.ActionInterface)
	 */
	public int getEdgeCount(R action) {
		return graph.getEdgeCount(action);
	}


	/**
	 * @param state
	 * @param post
	 * @param filter
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getImageIterator(org.cmg.tapas.graph.StateInterface, boolean, org.cmg.tapas.graph.EdgeFilter)
	 */
	public Iterator<Entry<R, P>> getImageIterator(P state, boolean post, EdgeFilter<P, R> filter) {
		return graph.getImageIterator(state, post, filter);
	}


	/**
	 * @param src
	 * @param action
	 * @param dest
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getMultiplicity(org.cmg.tapas.graph.StateInterface, org.cmg.tapas.graph.ActionInterface, org.cmg.tapas.graph.StateInterface)
	 */
	public int getMultiplicity(P src, R action, P dest) {
		return graph.getMultiplicity(src, action, dest);
	}


	/**
	 * @param state
	 * @param action
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getPostset(org.cmg.tapas.graph.StateInterface, org.cmg.tapas.graph.ActionInterface)
	 */
	public Set<P> getPostset(P state, R action) {
		return graph.getPostset(state, action);
	}


	/**
	 * @param state
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getPostset(org.cmg.tapas.graph.StateInterface)
	 */
	public Set<P> getPostset(P state) {
		return graph.getPostset(state);
	}


	/**
	 * @param state
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getPostsetMap(org.cmg.tapas.graph.StateInterface)
	 */
	public Map<R, ? extends Set<P>> getPostsetMap(P state) {
		return graph.getPostsetMap(state);
	}


	/**
	 * @param state
	 * @param action
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getPreset(org.cmg.tapas.graph.StateInterface, org.cmg.tapas.graph.ActionInterface)
	 */
	public Set<P> getPreset(P state, R action) {
		return graph.getPreset(state, action);
	}


	/**
	 * @param state
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getPresetMap(org.cmg.tapas.graph.StateInterface)
	 */
	public Map<R, ? extends Set<P>> getPresetMap(P state) {
		return graph.getPresetMap(state);
	}


	/**
	 * @param stateFilter
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getStates(org.cmg.tapas.graph.filter.Filter)
	 */
	public LinkedList<P> getStates(Filter<P> stateFilter) {
		return graph.getStates(stateFilter);
	}


	/**
	 * @return
	 * @see org.cmg.tapas.graph.Graph#getStatesIterator()
	 */
	public Iterator<P> getStatesIterator() {
		return graph.getStatesIterator();
	}


	/**
	 * @param state
	 * @return
	 * @see org.cmg.tapas.graph.Graph#isFinal(org.cmg.tapas.graph.StateInterface)
	 */
	public boolean isFinal(P state) {
		return graph.isFinal(state);
	}


	/**
	 * @return
	 * @see org.cmg.tapas.graph.Graph#numEdges()
	 */
	public int numEdges() {
		return graph.numEdges();
	}


	/**
	 * @param multiplicity
	 * @return
	 * @see org.cmg.tapas.graph.Graph#numEdges(boolean)
	 */
	public int numEdges(boolean multiplicity) {
		return graph.numEdges(multiplicity);
	}


	/**
	 * @return
	 * @see org.cmg.tapas.graph.Graph#numStates()
	 */
	public int numStates() {
		return graph.numStates();
	}


	/**
	 * @param state
	 * @return
	 * @see org.cmg.tapas.graph.Graph#stateIsIn(org.cmg.tapas.graph.StateInterface)
	 */
	public boolean stateIsIn(P state) {
		return graph.stateIsIn(state);
	}


	public Set<P> getPostset(P state, Filter<R> filter) {
		return graph.getPostset(state,filter);
	}
	

	public Graph<P,R> getGraph() {
		return graph;
	}
	

	
	public Graph<StateSet<P>,R> getGraphImage( HashMap<P,? extends Set<P>> map ) {
		Graph<StateSet<P>,R> newGraph = new Graph<StateSet<P> , R>();

		Iterator<P> iter = graph.getStatesIterator();
		P state;
		
		Set<P> start;
		Set<P> end;
		Set<P> poset;
		Set<R> actions = graph.getActionSet();
		iter = graph.getStatesIterator();
		while( iter.hasNext() ) {
			state = iter.next();
			start = map.get(state);
			if (start != null) {
				for( R action: actions ){				
					poset = graph.getPostset(state,action);
					if (poset != null) {
						for( P reached: poset ) {
							end = map.get(reached);
							if (end != null) {
								newGraph.addEdge( new StateSet<P>(start) , action, new StateSet<P>(end) );
							}
						}
					}
				}
			}
		}
		return newGraph;
	}
	
	@Override
	public APGraph<P,R> clone() {
		return new APGraph<P, R>(this);
	}

	public int[][] getAdiacenceMatrix(Map<P, Integer> map) {
		return graph.getAdiacenceMatrix(map);
	}
	
}

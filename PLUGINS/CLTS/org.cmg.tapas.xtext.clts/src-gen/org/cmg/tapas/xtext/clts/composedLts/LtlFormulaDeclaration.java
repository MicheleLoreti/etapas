/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ltl Formula Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration#getFormula <em>Formula</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtlFormulaDeclaration()
 * @model
 * @generated
 */
public interface LtlFormulaDeclaration extends Formula
{
  /**
   * Returns the value of the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formula</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formula</em>' containment reference.
   * @see #setFormula(LtlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtlFormulaDeclaration_Formula()
   * @model containment="true"
   * @generated
   */
  LtlFormula getFormula();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration#getFormula <em>Formula</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formula</em>' containment reference.
   * @see #getFormula()
   * @generated
   */
  void setFormula(LtlFormula value);

} // LtlFormulaDeclaration

package org.cmg.tapas.testGraph.algorithms.tapas1;

/**
 */
 
import java.util.Vector;

public class HMap {

	private Vector<HashPair> dominio = new Vector<HashPair>();	
	private Vector<State> codominio = new Vector<State>();
	
	public int getDimension() {
		return dominio.size();	
	}

	public void addItem(StatesSet sp, boolean b, State s) {
		dominio.add( new HashPair(sp, b) );
		codominio.add(s);
	}

	public void addItem(HashPair hashPair, State s) {
		dominio.add(hashPair);
		codominio.add(s);
	}
	
	public State getElement(StatesSet sp, boolean b) {
		HashPair hashPair;
		
		for( int i=0; i<this.getDimension(); i++ ) {
			hashPair = dominio.elementAt(i);
			if( hashPair.getStatesSet().equals(sp) )
				if( hashPair.getConvergenceFlag() == b ) 
					return codominio.elementAt(i);
		}
		
		return null;
	}
	
	
	public State getElement(HashPair hp) {
		for( int i=0; i<this.getDimension(); i++ ) {
			if( dominio.elementAt(i).equals(hp) )
				return codominio.elementAt(i);
		}
		
		return null;
	}

}


/**
 */
 
class HashPair {
	
	private StatesSet sp = null;	
	private boolean convergenceFlag = true;
	
	public HashPair(StatesSet sp, boolean convergenceFlag) {
		this.sp = sp;
		this.convergenceFlag = convergenceFlag;
	}
	
	public StatesSet getStatesSet() {
		return this.sp;	
	}
	
	public boolean getConvergenceFlag() {
		return convergenceFlag;
	}
	
	public boolean equals(HashPair hp) {
		return 
			sp.equals(hp.getStatesSet()) &&
			convergenceFlag == hp.getConvergenceFlag();
	}
	
}
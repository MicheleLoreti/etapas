/**
 * 
 */
package org.cmg.tapas.graph;

import java.util.LinkedList;

/**
 * @author loreti
 *
 */
public class TimeTester {

	static LinkedList<Long> stat = new LinkedList<Long>();
	static long start;
	static double total=0;
	static long count=0;
	
	public synchronized static void start(long x) {
		start = x;
	}

	public static void end(long end) {
		total += (start-end);
		count++;
	}

	public static void printStatistics() {
		System.out.println("COUNT: "+stat.size());
		System.out.println("AVERAGE: "+(total/count));

	}

}

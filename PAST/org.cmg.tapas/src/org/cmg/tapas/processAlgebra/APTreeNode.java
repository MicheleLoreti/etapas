package org.cmg.tapas.processAlgebra;


import java.util.Collection;

import javax.swing.tree.DefaultMutableTreeNode;

import org.cmg.tapas.graph.ActionInterface;

public class APTreeNode<P extends APProcess<P, A>, A extends ActionInterface> 
		extends DefaultMutableTreeNode{
	
	private static final long serialVersionUID = 1094832576595564742L;
	private String str;
	private P process; 

	public APTreeNode(String str){
		super(str);
		this.str = str;
		this.process = null;
	}
	
	public APTreeNode(P process){
		super(process);
		this.process = process;
		this.str = process.toString();
	}
	
	public APTreeNode(P process, String toString){
		super(process);
		this.process = process;
		this.str = toString;
	}
	
//	public APTreeNode(String str, Collection<P> children){
//		this(str);
//		for (P p : children) {
//			add(new APTreeNode<P, A>(p));			
//		}
//	}		
	public APTreeNode(String str, Collection<? extends P> children){
		this(str);
		for (P p : children) {
			add(new APTreeNode<P, A>(p));			
		}
	}
	
	public boolean isProcess(){
		return process != null;
	}
	
	public P getProcess(){
		return process;
	}
	
	public String getId(){
		if(isProcess())
			return process.getId();
		else 
			return "";
	}
	
	public String toString(){
		return str;
	}
	
	public APTreeNode<P, A> getNode(String str){
		if(toString().equals(str))
			return this;
		else{  
			if(children != null)
				for (Object child : children) {
					if (child instanceof APTreeNode) {
						APTreeNode<P, A> node = (APTreeNode<P, A>) child;
						APTreeNode<P, A> tmp = node.getNode(str);					
						if(tmp != null)
							return tmp;
					}
				}
			return null;				
		}
	}
}

/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lts Declaration Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getStates <em>States</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getInit <em>Init</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getRules <em>Rules</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getLabels <em>Labels</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsDeclarationBody()
 * @model
 * @generated
 */
public interface LtsDeclarationBody extends LtsBody
{
  /**
   * Returns the value of the '<em><b>States</b></em>' containment reference list.
   * The list contents are of type {@link org.cmg.tapas.xtext.clts.composedLts.LtsState}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>States</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>States</em>' containment reference list.
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsDeclarationBody_States()
   * @model containment="true"
   * @generated
   */
  EList<LtsState> getStates();

  /**
   * Returns the value of the '<em><b>Init</b></em>' reference list.
   * The list contents are of type {@link org.cmg.tapas.xtext.clts.composedLts.LtsState}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Init</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Init</em>' reference list.
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsDeclarationBody_Init()
   * @model
   * @generated
   */
  EList<LtsState> getInit();

  /**
   * Returns the value of the '<em><b>Rules</b></em>' containment reference list.
   * The list contents are of type {@link org.cmg.tapas.xtext.clts.composedLts.LtsRule}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rules</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rules</em>' containment reference list.
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsDeclarationBody_Rules()
   * @model containment="true"
   * @generated
   */
  EList<LtsRule> getRules();

  /**
   * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
   * The list contents are of type {@link org.cmg.tapas.xtext.clts.composedLts.LabelRule}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Labels</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Labels</em>' containment reference list.
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsDeclarationBody_Labels()
   * @model containment="true"
   * @generated
   */
  EList<LabelRule> getLabels();

} // LtsDeclarationBody

/**
 * 
 */
package org.cmg.tapas.process.exception;

/**
 * @author loreti
 *
 */
public class UndefinedSystemException extends UnknownNameException {

	public UndefinedSystemException(String name) {
		super( name );
	}

}

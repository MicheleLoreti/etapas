package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;


/**
 * La classe PTStatesSet rappresenta un insieme di stati 
 * di tipo <tt>PTState</tt>.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class PTStatesSet {

	/**
	 * Rappresenta l'insieme degli stati. 
	 */	
	private Vector<PTState> ptstates = null;
	
	/**
	 * Costruisce una nuovo insieme di stati vuoto. 
	 **/
	public PTStatesSet() {
		ptstates = new Vector<PTState>();
  	}

	/**
	 * Restituisce il numero di elementi di tipo <tt>PTState</tt> contenuti 
	 * nell'insieme.
	 **/   	  	
  	public int getDimension() {
  		return ptstates.size();
  	}

	/**
	 * Aggiunge all'insieme un oggetto di tipo <tt>PTState</tt>. 
	 **/
	public void addPTState(PTState s) {
		ptstates.add(s);
	}
	
	/**
	 * Aggiunge all'insieme un insieme di oggetti di tipo <tt>PTState</tt>. 
	 **/
	public void addPTStatesSet(PTStatesSet ss) {
		for (int i=0; i<ss.getDimension(); i++)
			this.addPTState(ss.ptstateAt(i));
	}
	
	/**
	 * Rimuove dall'insieme lo stato passato come parametro.
	 **/
	public boolean removePTState(PTState s) {
		int i = this.getPTStateIndex(s.getState());
		if (i != -1) 
			if (this.ptstateAt(i) == s) {
				this.ptstates.removeElementAt(i);
				return true;
			}
		return false;
	}

	/**
	 * Permette di ricercare uno stato di tipo <tt>PTState</tt>
	 **/		
	public PTState getPTState(State state) {
		PTState s = null;
		for (int i=0; i<this.getDimension(); i++) {
			s = this.ptstateAt(i);
			if (s.getState() == state) return s;
		}
		return null;		
	}
	
	/**
	 * Controlla se l'insieme contiene uno stato di tipo <tt>PTState</tt> 
	 * con associato l'oggetto di tipo <tt>State</tt> passato come parametro.
	 **/
	public int getPTStateIndex(State state) {
		PTState s = null;
		for (int i=0; i<this.getDimension(); i++) {
			s = this.ptstateAt(i);
			if (s.getState() == state) return i;
		}
		return -1;
	}

	/**
	 * Ritorna l'oggetto di tipo <tt>PTState</tt> contenuto nell'insieme 
	 * degli stati alla posizione 
	 * specificata dal parametro intero in ingresso. 
	 **/
	public PTState ptstateAt(int index) {
		try	{ 
			return ptstates.elementAt(index); 
		}	
		catch(ArrayIndexOutOfBoundsException E) { 
			return null; 
		}
	}

	/**
	 * Controlla se l'insieme degli stati contiene un <tt>PTState</tt> 
	 * che rappresenta lo stato di tipo
	 * <tt>State</tt> passato come parametro di ingresso.
	 **/	
	public boolean containState(State s) {
		if( getPTState(s)!=null ) 
			return true;
		
		return false;
	}

	/**
	 * Controlla se l'insieme degli stati contiene lo stato di tipo 
	 * <tt>PTState</tt> passato come parametro di ingresso.
	 **/
	public boolean containPTState(PTState s) {
		if( getPTState(s.getState())==s ) 
			return true;
		
		return false;
	}

	/**
	 * Controlla se l'insieme degli stati contiene tutti gli stati di 
	 * tipo <tt>PTState</tt> 
	 * appartenenti all'insieme passato come parametro di ingresso.
	 **/
	public boolean containPTStatesSet(PTStatesSet ss) {
		for (int i=0; i<ss.getDimension(); i++)	
			if (!(this.containPTState(ss.ptstateAt(i)))) return false;
		return true;
	}

	/**
	 * Verifica se l'insieme correte � uguale all'insieme passato 
	 * come parametro di ingresso.
	 **/
	public boolean equals(PTStatesSet ss) {
		if( getDimension()!=ss.getDimension() ) 
			return false;

		for( int i=0; i<ss.getDimension(); i++ )
			if( !containPTState(ss.ptstateAt(i)) ) 
				return false;
		return true;
	}

	/**
	 * Controlla se l'insieme degli stati � vuoto. 
	 **/
	public boolean isEmpty() {
		return getDimension() == 0;
	}

	/**
	 * Calcola l'intersezione tra l'insieme degli stati corrente e 
	 * quello passato come parametro. 
	 **/ 
	public StatesSet intersection(StatesSet statesSet) {
		State state = null;
		StatesSet result = new StatesSet();		
		for (int i=0; i<this.getDimension(); i++) {
			state = this.ptstateAt(i).getState();
			if (statesSet.containState(state)) result.addState(state);
		}
		return result;
	}

	/**
	 * Stampa l'intero insieme degli stati richiamando 
	 * per ciascun <tt>PTState</tt> il metodo 
	 * <tt>printAll()</tt>. 
	 **/	
	public void printAll() {
		int i = 0;
		System.out.print("{");
		for (i=0; i<this.getDimension(); i++) {
			if (i>0) System.out.print("\n, ");
			ptstates.elementAt(i).printAll();
		}
		if (i==0) System.out.print("empty");
		System.out.print("\n}");
	}	

	/**
	 * Stampa l'intero insieme degli stati visto come insieme di stringhe, 
	 **/ 
	public void print() {
		int i = 0;
		System.out.print("{");
		for (i=0; i<this.getDimension(); i++) {
			if (i>0) System.out.print(", ");
			ptstates.elementAt(i).print();
		}
		if (i==0) System.out.print("empty");
		System.out.print("}");
	}

}
package org.cmg.tapas.clts.extensions;

import org.cmg.tapas.views.TAPAsGraphElementLabelProvider;
import org.cmg.tapas.xtext.clts.composedLts.Action;
import org.cmg.tapas.xtext.clts.composedLts.LtsRule;
import org.cmg.tapas.xtext.clts.composedLts.LtsState;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.graphics.Color;
import org.eclipse.zest.core.widgets.ZestStyles;

@SuppressWarnings("all")
public class CLTSGraphElementLabelProvider extends TAPAsGraphElementLabelProvider {
  @Override
  public Color getColor(final Object rel) {
    return ColorConstants.black;
  }
  
  @Override
  public int getConnectionStyle(final Object rel) {
    return ZestStyles.CONNECTIONS_DIRECTED;
  }
  
  @Override
  public Color getHighlightColor(final Object rel) {
    return ColorConstants.yellow;
  }
  
  @Override
  public int getLineWidth(final Object rel) {
    return 1;
  }
  
  @Override
  public IFigure getTooltip(final Object rel) {
    return null;
  }
  
  @Override
  public boolean fisheyeNode(final Object entity) {
    return false;
  }
  
  @Override
  public Color getBackgroundColour(final Object entity) {
    return null;
  }
  
  @Override
  public Color getBorderColor(final Object entity) {
    return null;
  }
  
  @Override
  public Color getBorderHighlightColor(final Object entity) {
    return null;
  }
  
  @Override
  public int getBorderWidth(final Object entity) {
    return 1;
  }
  
  @Override
  public Color getForegroundColour(final Object entity) {
    return null;
  }
  
  @Override
  public Color getNodeHighlightColor(final Object entity) {
    return null;
  }
  
  @Override
  public String getText(final Object element) {
    String _switchResult = null;
    boolean _matched = false;
    if (element instanceof LtsState) {
      _matched=true;
      _switchResult = ((LtsState)element).getName();
    }
    if (!_matched) {
      if (element instanceof LtsRule) {
        _matched=true;
        Action _act = ((LtsRule)element).getAct();
        _switchResult = _act.getName();
      }
    }
    return _switchResult;
  }
}

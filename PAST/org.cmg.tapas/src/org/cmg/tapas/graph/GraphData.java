package org.cmg.tapas.graph;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.cmg.tapas.graph.filter.Filter;

/** 
 * Interfaccia per le classi che rappresentano grafi di 
 * cui si possono leggere i dati. 
 * 
 * @param <S> Tipo degli stati del grafo. 
 * @param <A> Tipo delle azioni del grafo.
 * 
 * @see StateInterface
 * @see ActionInterface
 *  
 * @author Guzman Tierno
 * @author Michele Loreti
 */
public interface GraphData<
	S extends StateInterface, 
	A extends ActionInterface
>  {
	
// _____________________________________________________________________________
// get States 

	/** 
	 * Restituisce il numero degli stati del grafo.
	 **/
	int numStates();
	
	/** 
	 * Dice se lo stato specificato � presente nel grafo.
	 **/
	boolean stateIsIn(S state);

	/**
	 * Restituisce una <tt>LinkedList</tt> contenente tutti gli 
	 * stati del grafo.
	 * Quando possibile si preferisca una visita.
	 **/
	LinkedList<S> getAllStates();
	
	
	
	/**
	 * Restituisce una <tt>LinkedList</tt> contenente tutti gli 
	 * stati del grafo che verificano il fltro <tt>f</tt>.
	 * Quando possibile si preferisca <tt>getStatesIterator()</tt>.
	 **/
	LinkedList<S> getStates(Filter<S> f);
	
	/**
	 * Rende un iteratore per l'insieme degli stati del grafo.
	 * Il metodo � da preferire a <tt>getAllStates().iterator()</tt>
	 * in quanto evita di dover generate la collezione 
	 * degli stati e scorre invece direttamente la struttura interna
	 * che memorizza gli stati.
	 **/
	Iterator<S> getStatesIterator();
	
// _____________________________________________________________________________
// get Edges
	
	/**
	 * Restituisce il numero di archi nel grafo sensa tener conto delle molteplicit&agrave; degli archi.
	 **/
	int numEdges();

	/**
	 * Restituisce il numero di archi nel grafo, se il flag <tt>multiplicity</tt> &grave; true
	 * ogniarco viene considerato con la sua moltiplicit&agrave;, altrimenti, non viene
	 * tenuto conto della moltiplicit&agrave.
	 **/
	int numEdges(boolean multiplicity);
	
	/** 
	 * Rende il numero di archi etichettati con l'azione
	 * specificata. 
	 **/
	public int getEdgeCount(A action);
	
	/**
	 * Rende la molteplicit� di un arco.
	 **/
	public int getMultiplicity(S src, A action, S dest);
	
	/**
	 * Dice se nel grafo vi � un arco tra gli stati passati
	 * come parametri etichettato con l'azione passata come parametro.
	 **/
	boolean edgeIsIn(S from, A action, S to);	

	/**
	 * Dice se nel grafo vi � un arco tra gli stati passati
	 * come parametri.
	 **/
	boolean edgeIsIn(S from, S to);	
	
	
// _____________________________________________________________________________
// get Actions
	

	/**
     * Rende un insieme con le azioni del grafo. <p>
     * L'insieme restituito NON deve essere modificata
     * perch� esso � direttamente cablata con il grafo in questione.
     **/
    Set<A> getActionSet();

// _____________________________________________________________________________
// get Pre/post-images

	/**
	 * Rende la mappa dei preset di uno stato <tt>state</tt>: cio�
	 * la mappa che associa ad ogni azione <tt>a</tt>
	 * la retroimmagine di <tt>state</tt> sotto l'azione <tt>a</tt>.
	 * Nel caso che <tt>state</tt> non abbia nessun arco entrante
	 * il metodo rende null. <p>
     * La mappa restituita NON deve essere modificata
     * perch� essa � direttamente cablata con il grafo in questione.
	 **/
	Map<A,? extends Set<S>> getPresetMap(S state);

	/**
	 * Rende la mappa dei postset di uno stato <tt>state</tt>: cio�
	 * la mappa che associa ad ogni azione <tt>a</tt>
	 * l'immagine di <tt>state</tt> sotto l'azione <tt>a</tt>.
	 * Nel caso che <tt>state</tt> non abbia nessun arco uscente
	 * il metodo rende null. <p>
     * La mappa restituita NON deve essere modificata
     * perch� essa � direttamente cablata con il grafo in questione.
	 **/
	Map<A,? extends Set<S>> getPostsetMap(S state);
	
	/**
	 * Rende il preset di uno stato sotto una azione.
	 * Se lo stato non ha archi entranti con tali azione
	 * pu� rendere null. <p>
     * L'insieme restituito NON deve essere modificato
     * perch� esso � direttamente cablato con il grafo in questione.
	 **/
	Set<S> getPreset(S state, A action);

	/**
	 * Rende il postset di uno stato sotto una azione.
	 * Se lo stato non ha archi uscenti con tali azione
	 * pu� rendere null. <p>
     * L'insieme restituito NON deve essere modificato
     * perch� esso � direttamente cablato con il grafo in questione.
	 **/
	Set<S> getPostset(S state, A action);

	/**
	 * Rende il postset di uno stato sotto un filtro.
	 * Se lo stato non ha archi uscenti con azioni che
	 * soddisfano il filtro pu&ograve; rendere null. <p>
     * L'insieme restituito NON deve essere modificato
     * perch� esso � direttamente cablato con il grafo in questione.
	 **/
	Set<S> getPostset(S state, Filter<A> filter);

	/**
	 * Restituisce tutti gli stati successivi di <tt>state</tt> 
	 * indipendentemente dall'azione. <p>
     * L'insieme restituito NON deve essere modificato
     * perch� esso � direttamente cablato con il grafo in questione.
	 */
	Set<S> getPostset(S state);
		
	/**
     * Rende un iteratore per le transizioni dello stato specificato.
     * Le transizioni considerate sono quelle uscenti se 'post'
     * � true, quelle entranti altrimenti.
     **/	
    Iterator<Map.Entry<A,S>> getImageIterator(
    	S state, boolean post, EdgeFilter<S,A> filter
    );

	/** Rende true se lo stato specificato non ha archi uscenti. **/
	boolean isFinal(S state);

}

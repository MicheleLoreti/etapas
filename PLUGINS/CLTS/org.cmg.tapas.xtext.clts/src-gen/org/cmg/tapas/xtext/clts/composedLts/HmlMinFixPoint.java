/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Min Fix Point</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlMinFixPoint()
 * @model
 * @generated
 */
public interface HmlMinFixPoint extends HmlRecursionFormula
{
} // HmlMinFixPoint

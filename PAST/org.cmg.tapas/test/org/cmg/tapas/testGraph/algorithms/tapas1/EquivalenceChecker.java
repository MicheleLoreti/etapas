package org.cmg.tapas.testGraph.algorithms.tapas1;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.StateInterface;

/**
 * La classe EquivalenceChecker � una classe astratta 
 * per la verifica di equivalenze. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public abstract class EquivalenceChecker<
	S extends StateInterface,
	A extends ActionInterface
> {  

	/**
	 * Rappresenta il primo dei due grafi da confrontare.
	 * <p>
	 **/
	private GraphInterface<S,A> a1 = null;

	/**
	 * Rappresenta il primo dei due grafi da confrontare.
	 */
	private GraphInterface<S,A> a2 = null;

	/**
	 * Rappresenta il flag booleano che indica se � attiva la modalit� 
	 * verbose per il calcolo e la stampa del 
	 * controesempio qualora i due processi non siano equivalenti.
	 **/
	private boolean verboseSolution = true;

	/**
	 * Nome dell'equivalenza per cui � progettato il checker.
	 **/
    private String name = null;

	private S initState1 = null;
	private S initState2 = null;

	public S getInitState1() {
		return initState1;
	}
	public S getInitState2() {
		return initState2;
	}
	public void setInitState1(S initState1) {
		this.initState1 = initState1;
	}
	public void setInitState2(S initState2) {
		this.initState2 = initState2;
	}
	
    /** 
     * Costruisce una nuova istanza del checker inizializzando 
     * il nome dell'equivalenza con la stringa passata come
     * parametro.
     **/
    public EquivalenceChecker(String equivalenceName) {
        name = equivalenceName;
    }
    
	/**
	 * Retituisce il primo dei due sistemi su cui effettuare la verifica.
	 **/
    public GraphInterface<S,A> getFirstAutomaton() { 
    	return this.a1; 
    }

	/**
	 * Retituisce il secondo dei due automi su cui effettuare la verifica.
	 **/
    public GraphInterface<S,A> getSecondAutomaton() { 
    	return this.a2; 
    }    
    
	/**
	 * Retituisce il valore della modalit� verbose per il calcolo e 
	 * la stampa del controesempio nel caso i due
	 * non siano equivalenti.
	 **/
    public boolean getVerboseSolution() { 
    	return verboseSolution; 
    }      

	/**
	 * Retituisce il nome dell'equivalenza per cui � progettato il checker.
	 **/
    public String getEquivalenceName() { 
    	return name; 
    }

	/**
	 * Permette di settare tutti i campi del checker.
	 **/
    public void setParameters(
    	GraphInterface<S,A> a1, 
		GraphInterface<S,A> a2, 
		S initState1, 
		S initState2, 
		boolean verboseSolution
	) {
		this.a1 = a1;
		this.a2 = a2;
		this.initState1 = initState1;
		this.initState2 = initState2;
		this.verboseSolution = verboseSolution;
    }

	/**
	 * Controlla se vale l'equivalenza tra i due sistemi
	 * considerando i due punti iniziali.
	 **/
    public abstract boolean checkEquivalence();
}

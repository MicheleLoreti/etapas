/**
 * 
 */
package org.cmg.tapas.process.exception;




/**
 * @author loreti
 *
 */
public class MultipleNameDefinition extends ProcessException {
	
	private static final String MSG1 = "The process \"";
	private static final String MSG2 = "\" is already defined.";
	
	private String name;

	public MultipleNameDefinition(String s) {
		this.name = s;
	}

	public String getName() {
		return name;
	}
	
	public String getMessage(){
		return MSG1+name+MSG2;
	}
}

/**
 * 
 */
package org.cmg.tapas.formulae.actl.action;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlActionPredicateVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.OrFilter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class ActlActionPredicateUnion<S extends APProcess<S,A> , A extends ActionInterface> implements ActlActionPredicate<S,A> {

	public ActlActionPredicate<S,A> getChi2() {
		return chi2;
	}

	public void setChi2(ActlActionPredicate<S,A> chi2) {
		this.chi2 = chi2;
	}

	public ActlActionPredicate<S,A> getChi1() {
		return chi1;
	}

	public void setChi1(ActlActionPredicate<S,A> chi1) {
		this.chi1 = chi1;
	}

	private ActlActionPredicate<S,A> chi2;
	private ActlActionPredicate<S,A> chi1;

	public ActlActionPredicateUnion() {
		this( new ActlAllActions<S,A>() , new ActlAllActions<S,A>() );
	}
	
	public ActlActionPredicateUnion(ActlActionPredicate<S,A> chi1, ActlActionPredicate<S,A> chi2) {
		this.chi1 = chi1;
		this.chi2 = chi2;
	}

	public void acceptVisitor(ActlActionPredicateVisitor<S,A> v) {
		v.accept(this);
	}

	public Filter<A> getFilter() {
		return new OrFilter<A>(chi1.getFilter(),chi2.getFilter());
	}
	
}

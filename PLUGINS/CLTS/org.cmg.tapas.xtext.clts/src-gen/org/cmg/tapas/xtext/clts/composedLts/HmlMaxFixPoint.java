/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Max Fix Point</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlMaxFixPoint()
 * @model
 * @generated
 */
public interface HmlMaxFixPoint extends HmlRecursionFormula
{
} // HmlMaxFixPoint

/**
 * 
 */
package org.cmg.tapas.formulae.lmc;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;

/**
 * @author loreti
 *
 */
public interface ActionLoader<S extends StateInterface, A extends ActionInterface> {

	A getAction(String string, String value);

}

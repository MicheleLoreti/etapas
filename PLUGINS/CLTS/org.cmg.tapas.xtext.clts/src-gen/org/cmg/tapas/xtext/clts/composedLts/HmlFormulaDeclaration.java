/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Formula Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration#getFormula <em>Formula</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlFormulaDeclaration()
 * @model
 * @generated
 */
public interface HmlFormulaDeclaration extends Formula
{
  /**
   * Returns the value of the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formula</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formula</em>' containment reference.
   * @see #setFormula(HmlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlFormulaDeclaration_Formula()
   * @model containment="true"
   * @generated
   */
  HmlFormula getFormula();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration#getFormula <em>Formula</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formula</em>' containment reference.
   * @see #getFormula()
   * @generated
   */
  void setFormula(HmlFormula value);

} // HmlFormulaDeclaration

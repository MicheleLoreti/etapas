package org.cmg.tapas.testGraph.algorithms.branching;


import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.branching.gv.GVChecker;
import org.cmg.tapas.graph.algorithms.branching.gvks.GVKSChecker;
import org.cmg.tapas.graph.algorithms.branching.gvs.GVSChecker;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.tapas1.branching.TBranchingChecker;


/**
 * Test per gli algoritmi di bisimulazione branching.
 * Si tratta di un piccolo test che verifica solo
 * un grafo specifico. 
 *
 * @author Guzman Tierno
 **/
public class BranchingTest2 extends TestCase {
	
    private GraphInterface<LtsState,LtsAction> lts1;
    //private GraphInterface<LtsState,LtsAction> lts2;
	private LtsState s0, s1, s2, s3, s4, s5, s6, s7, s8; // s9, s10, s11;
	private LtsState init1, init2;    	
	private LtsAction tau, a, b;
	
	
	public static void main(String[] args) {
		System.out.print("Run BranchingTest.java");
		junit.textui.TestRunner.run(BranchingTest.class);
	}
    
	
    public BranchingTest2(String s){
    	super(s);
    }

	@Override 
    protected void setUp() {  	
    	lts1 = new Graph<LtsState, LtsAction>();

    	// nodi primo lts
    	s0  = new LtsState("0" );
    	s1  = new LtsState("1" );
    	s2  = new LtsState("2" );
    	s3  = new LtsState("3" );
    	s4  = new LtsState("4" );
    	
    	// nodi secondo lts
    	s5  = new LtsState("5" );
    	s6  = new LtsState("6" );
    	s7  = new LtsState("7" );
    	s8  = new LtsState("8" );

		lts1.addState(s0); 
		lts1.addState(s1); 
		lts1.addState(s2); 
		lts1.addState(s3); 
		lts1.addState(s4); 
		lts1.addState(s5); 
		lts1.addState(s6); 
		lts1.addState(s7); 
		lts1.addState(s8); 

		// azioni
    	a = new LtsAction("a");
    	b = new LtsAction("b");
    	tau = LtsAction.TAU;

    	// archi primo lts
		lts1.addEdge(s0, a  , s1); 
		lts1.addEdge(s0, b  , s2); 		
		lts1.addEdge(s1, tau, s3); 		
		lts1.addEdge(s3, b  , s4); 

		// archi secondo lts
		lts1.addEdge(s5, a  , s6); 
		lts1.addEdge(s5, b  , s7); 		
		lts1.addEdge(s6, b  , s8); 

		// stati iniziali
		init1 = s0;
    	init2 = s5;    	
    }
    
    public void testBranching(){
    	Evaluator<LtsState, ?> map;
		boolean res;
		System.out.println();

		// branching checker GVKS
		GVKSChecker<LtsState,LtsAction> gvksChecker = 
			new GVKSChecker<LtsState,LtsAction>(lts1);
//		res = map.eval(init1)==map.eval(init2);
		res = gvksChecker.checkEquivalence(init1, init2);
		map = gvksChecker.computeEquivalence(
			new Evaluator<LtsState,Object>() {
                @Override public Object eval(LtsState s) {
					return this;
				}
			}
		);
		Assert.assertTrue( res );						// assert equivalent		
		
		// branching checker GV
		GVChecker<LtsState,LtsAction> gvChecker = 
			new GVChecker<LtsState,LtsAction>(lts1);
		map = gvChecker.computeEquivalence(
			new Evaluator<LtsState,Object>() {
                @Override public Object eval(LtsState s) {
					return this;
				}
			}
		);
		res = map.eval(init1)==map.eval(init2);
		Assert.assertTrue( res );						// assert equivalent
		
		// branching checker GVSemplificato
		GVSChecker<LtsState,LtsAction> gvsChecker = 
			new GVSChecker<LtsState,LtsAction>(lts1);
		map = gvsChecker.computeEquivalence(
			new Evaluator<LtsState,Object>() {
                @Override public Object eval(LtsState s) {
					return this;
				}
			}
		);
		res = map.eval(init1)==map.eval(init2);
		Assert.assertTrue( res );						// assert equivalent
		
		// branching checker Tapas1
		for( int i=0; i<1; i++ ) {
			TBranchingChecker<LtsState,LtsAction> tapas1Checker = 
				new TBranchingChecker<LtsState,LtsAction>();
			tapas1Checker.setParameters(lts1, lts1, init1, init2, false); 
			res = tapas1Checker.checkEquivalence();
			Assert.assertTrue( res );					// assert equivalent
		}
	}
 
	public static Test suite() {
	    return new TestSuite(BranchingTest.class);
	}
	
	@Override 
    protected void tearDown() {
        //   
    }
    
}

/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.TypeReference#getReference <em>Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getTypeReference()
 * @model
 * @generated
 */
public interface TypeReference extends TypeExpression
{
  /**
   * Returns the value of the '<em><b>Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reference</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' reference.
   * @see #setReference(TypeDef)
   * @see org.lic.secsi.SecsiPackage#getTypeReference_Reference()
   * @model
   * @generated
   */
  TypeDef getReference();

  /**
   * Sets the value of the '{@link org.lic.secsi.TypeReference#getReference <em>Reference</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' reference.
   * @see #getReference()
   * @generated
   */
  void setReference(TypeDef value);

} // TypeReference

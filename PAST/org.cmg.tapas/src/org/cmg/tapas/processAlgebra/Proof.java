package org.cmg.tapas.processAlgebra;


import java.util.Collection;
import java.util.Iterator;
import java.util.Observer;
import java.util.Set;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;

public interface Proof<P extends APProcess<P, A>,A extends ActionInterface> 
		extends Observer {
	
	public Set<A> getActions();
	
	public Set<Channel> getRestrictedChannels();
	
	public Collection<RenamingHistory<Channel>> getRenamedChannels();
	
	public Iterator<P> get(A act);
	
	public Iterator<P> getComponents();
	
	public Iterator<P> get(Filter<A> f);
	
	public boolean updating();
	
	public Iterator<A> getActIterator();
	
	public String getProcess();
	
	public Set<Integer> getEvolvedComponent();
}

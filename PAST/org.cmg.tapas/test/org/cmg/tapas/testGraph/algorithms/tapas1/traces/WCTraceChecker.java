package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessFG;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;


/**
 * La classe WCTraceChecker realizza l'algoritmo di verifica 
 * per l'equivalenza weak completed trace. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class WCTraceChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {

    /** 
     * Costruisce una nuova istanza del checker.
     */
	public WCTraceChecker() {
		super("WCTrace");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 */	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );
		
		// weak final graph
		ProcessFG p1WFG = ProcessFG.buildWFGraph(p1);
		ProcessFG p2WFG = ProcessFG.buildWFGraph(p2);
		
		// fusion
		ProcessFG pWFG = ProcessFG.createProcessFusion(p1WFG, p2WFG);
		
		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();

		// initializing partition 
		PTPartition initialRo = WCTraceUtil.initPTPartition(pWFG);
		
		// refinement
		PTState initialPTState1 = initialRo.getPTState(pWFG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pWFG.getSecondInitState());
		
		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pWFG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		boolean result = ro.checkSameClass(
			pWFG.getInitState(), pWFG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			WCTraceUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName(),
				pWFG				
			);
		}
		
		return result;
	}
}
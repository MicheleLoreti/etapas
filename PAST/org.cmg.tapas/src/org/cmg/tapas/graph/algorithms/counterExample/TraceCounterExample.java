package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.cmg.tapas.formulae.hml.DivergenceFormula;
import org.cmg.tapas.formulae.hml.False;
import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.formulae.hml.True;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.GraphData;
import org.cmg.tapas.graph.algorithms.AcceptanceSet;
import org.cmg.tapas.graph.algorithms.AcceptanceState;
import org.cmg.tapas.graph.algorithms.ActionSet;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.processAlgebra.APProcess;
import org.cmg.tapas.util.DebugMode;


public class TraceCounterExample
	<S extends APProcess<S, A>
	, A extends ActionInterface> 
		extends AbstractCounterExample<S, A>{

	// Bit masks per la definizione delle equivalenze:
	// 1 closure
	// 2 convergenceFlag
	// 3 finalSensitive
	// 4 acceptanceFlag
	// 5 convergenceHistory
	// 6 convergenceCut

	/** Flag per le equivalenze deboli. * */
	public static final int WEAK = 1 << 0;
	/** Flag per le equivalenze sensibili alla convergenza. * */
	public static final int CONV = 1 << 1;
	/** Flag per le equivalenze sensibili agli stati finali. * */
	public static final int FINL = 1 << 2;
	/** Flag per le equivalenze sensibili agli insiemi di accettazione. * */
	public static final int ACPT = 1 << 3;
	/** Flag per le equivalenze con storia della divergenza. * */
	public static final int HIST = 1 << 4;
	/** Flag per le equivalenze che tagliano i rami divergenti. * */
	public static final int CUTC = 1 << 5;

	private static final DebugMode debug = new DebugMode(TraceCounterExample.class);

	// Flag che definiscono il grafo da costruire
	private boolean closureFlag;
	private boolean finalSensitive;
	private boolean convergenceFlag;
	private boolean acceptanceFlag;
	private boolean convergenceHistory;
	private boolean convergenceCut;
	
	private S p1;
	private S p2;
	private AcceptanceState<A> s1;
	private AcceptanceState<A> s2;
	
	private Evaluator<AcceptanceState<A>,? extends Set<AcceptanceState<A>>> evaluator;
	private HashMap<Object, HashSet<AcceptanceState<A>>> partitions;
	private HashMap<Object, Next> nextMap;
	private A tauAction = null;
	private FormulaeFactory<S, A> Ffactory;

	public TraceCounterExample(AcceptanceState<A> s1, AcceptanceState<A> s2
			, S p1, S p2
			, A tauAction, int traceType
			, Evaluator<AcceptanceState<A>,? extends Set<AcceptanceState<A>>> evaluator 
			, GraphData<AcceptanceState<A>, A> graph) {

		this.s1 = s1;
		this.s2 = s2;
		this.p1 = p1;
		this.p2 = p2;
		this.evaluator = evaluator;
		this.tauAction= tauAction;
		
		debug.println("["+s1+"] ------------------------> ["+s2+"]:");
		
		// Compute flags from equivalence
		closureFlag = (traceType & WEAK) > 0;
		finalSensitive = (traceType & FINL) > 0;
		convergenceFlag = (traceType & CONV) > 0;
		acceptanceFlag = (traceType & ACPT) > 0;
		convergenceCut = (traceType & CUTC) > 0;
		convergenceHistory = (traceType & HIST) > 0;

		// Crea la struttura dati per formule
		Ffactory = new FormulaeFactory<S, A>(!closureFlag, tauAction);
		
		init(graph);
	}
	
	private void init(GraphData<AcceptanceState<A>, A> graph){
		partitions = new HashMap<Object, HashSet<AcceptanceState<A>>>();
		nextMap = new HashMap<Object, Next>();
		LinkedList<AcceptanceState<A>> statesList = graph.getAllStates();
		for (AcceptanceState<A> s : statesList) {
			//Inizializza partitions
			Object o = evaluator.eval(s);
			HashSet<AcceptanceState<A>> asSet = partitions.get(o); 
			if(asSet == null){
				asSet = new HashSet<AcceptanceState<A>>();
				partitions.put(o, asSet);
			}
			asSet.add(s);
			
			//Inizializza nextMap
			Next next = nextMap.get(o); 
			if(next == null){
				next = new Next();
				nextMap.put(o, next);
			}			
			Map<A, ? extends Set<AcceptanceState<A>>> psMap = graph.getPostsetMap(s);
			if(psMap != null){
				for (A act : psMap.keySet()) {
					next.add(act, psMap.get(act));	
				}
			}
		}	
		
		//Inizializza la tabella
		curr_formulae = new TableFormulae<S, A>(new HashSet<Object>(partitions.keySet()));
		old_formulae = new TableFormulae<S, A>(new HashSet<Object>(partitions.keySet()));
	}
	
	private boolean finalFormula(Object s1, Object s2){
		HashSet<AcceptanceState<A>> hs1 = partitions.get(s1);
		HashSet<AcceptanceState<A>> hs2 = partitions.get(s2);
		if(hs1.size() > 0 && hs2.size() > 0){
			AcceptanceState<A> as1 = hs1.iterator().next();
			AcceptanceState<A> as2 = hs2.iterator().next();
			HmlFormula<S, A> fin = Ffactory.createBox(new False<S, A>());
			if (as1.isFinal() && !as2.isFinal()) {
				curr_formulae.addFormula(s1, s2, fin);
				return true;
			} else if (as2.isFinal() && !as1.isFinal()) {
				curr_formulae.addFormula(s2, s1, fin);
				return true;
			} 
		}
		return false;
	}
		
	private void actions(int i, Set<A> l1, int j, Set<A> l2){
		TreeSet<A> newL = new TreeSet<A>(l1);
		newL.removeAll(l2);
		if(newL.size() > 0){
			for (A a : newL) {
				HmlFormula<S, A> f = Ffactory.createDiamond(a, new True<S, A>());
				curr_formulae.addFormula(i, j, f);
			}
		}
		//--------------------------------------------------
		newL = new TreeSet<A>(l2);
		newL.removeAll(l1);
		if(newL.size() > 0){
			for (A a : newL) {
				HmlFormula<S, A> f = Ffactory.createDiamond(a, new True<S, A>());
				curr_formulae.addFormula(j, i, f);
			}
		}
	}
	
	private boolean convergenceFormualae(int i, Object o1, int j, Object o2){
		AcceptanceState<A> hs1 = partitions.get(o1).iterator().next();
		AcceptanceState<A> hs2 = partitions.get(o2).iterator().next();
		boolean hs1Conv = hs1.isConvergent();
		boolean hs2Conv = hs2.isConvergent();
		HmlFormula<S, A> div = new DivergenceFormula<S, A>(tauAction);
		if (!hs1Conv && hs2Conv) {
			curr_formulae.addFormula(i, j, div);
			return true;
		} else if (!hs2Conv && hs1Conv) {
			curr_formulae.addFormula(j, i, div);
			return true;
		}
		return false;
	}
		
	private void subSet(Object s1, Set<Object> set1
			, A act, Object s2, Set<Object> set2){
		
		LinkedList<LinkedList<HmlFormula<S, A>>> sfList1 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
		LinkedList<LinkedList<HmlFormula<S, A>>> sfList2 = new LinkedList<LinkedList<HmlFormula<S,A>>>();

		Iterator<Object> iter = set2.iterator();		
		while(iter.hasNext()){
			Object subHS2 = iter.next();
			for (Object subHS1 : set1) {
				if((!subHS1.equals(hs1) || !subHS2.equals(hs2))
						&&
					(!subHS1.equals(hs2) || !subHS2.equals(hs1))){
					sfList1.add(old_formulae.getFormulae(subHS1, subHS2));
					sfList2.add(old_formulae.getFormulae(subHS2, subHS1));
				}
			}			
		}

		if(sfList1.size() > 0){
			LinkedList<HmlFormula<S,A>> boxF = Ffactory.createBox(act, sfList1); 
			curr_formulae.addFormulae(hs2, hs1, boxF);
		}
		
		if(sfList2.size() > 0){
			LinkedList<HmlFormula<S,A>> diamondF = Ffactory.createDiamond(act, sfList2);
			curr_formulae.addFormulae(hs1, hs2, diamondF);
		}
	}	
		
	private void disjoinSet(Object hs1, Object hs2, A act, Set<Object> S1, Set<Object> S2){
		for (Object subHS1 : S1) {
			LinkedList<LinkedList<HmlFormula<S, A>>> fList = new LinkedList<LinkedList<HmlFormula<S,A>>>();
			for(Object subHS2 : S2) {
				if(!subHS1.equals(subHS2)){
					fList.add(old_formulae.getFormulae(subHS1, subHS2));
				}
			}
			
			LinkedList<HmlFormula<S,A>> newFL = Ffactory.createDiamond(act, fList);
			
			if(newFL.size()>0){
				curr_formulae.addFormulae(hs1, hs2, newFL);
			}
		}
	}
	
	private boolean acceptenceSet(Object hs1, Object hs2, 
			AcceptanceSet<A> set1, AcceptanceSet<A> set2){
		boolean res = false;
		if(set1 != null && set2 != null) {
			for (ActionSet<A> as1 : set1) {
				for (ActionSet<A> as2 : set2) {
					ActionSet<A> sub1 = new ActionSet<A>(as1);
					sub1.removeAll(as2);

					ActionSet<A> sub2 = new ActionSet<A>(as1);
					sub2.retainAll(as2);

					ActionSet<A> sub3 = new ActionSet<A>(as2);
					sub3.removeAll(as1);

					if (sub1.size() == 0 && sub3.size() > 0) {
						HmlFormula<S, A> f = Ffactory.create(as1, sub3);
						curr_formulae.addFormula(hs1, hs2, f);
						res = true;
					} else if (sub1.size() > 0 && sub3.size() == 0){ 
						HmlFormula<S, A> f = Ffactory.create(as2, sub1);
						curr_formulae.addFormula(hs2, hs1, f);
						res = true;
					} 
				}
			}
		}
		return res;
	}
	
	private boolean isDeadlock(AcceptanceSet<A> set){
		if(set != null && set.size() == 1){
			ActionSet<A> s = set.iterator().next();
			if(s.size() == 0)
				return true;
			else
				return false;
		}		
		return false;
	}
	
	private boolean acceptance(Object hs1, Object hs2){
		HashSet<AcceptanceState<A>> newHS1 = partitions.get(hs1); 
		HashSet<AcceptanceState<A>> newHS2 = partitions.get(hs2); 
		boolean res = false;
		for (AcceptanceState<A> nas1 : newHS1) {
			for (AcceptanceState<A> nas2 : newHS2) {
				AcceptanceSet<A> set1 = nas1.getLabel().getAcceptanceSet();
				AcceptanceSet<A> set2 = nas2.getLabel().getAcceptanceSet();
				
				boolean b1 = isDeadlock(set1);
				boolean b2 = isDeadlock(set2);
				if(b1 && !b2){
					HmlFormula<S, A> f = Ffactory.createBox(new False<S, A>());
					curr_formulae.addFormula(hs1, hs2, f);
					res = true;
				}else if(b2 && !b1){
					HmlFormula<S, A> f = Ffactory.createBox(new False<S, A>());
					curr_formulae.addFormula(hs2, hs1, f);
					res = true;
				}else
					res = acceptenceSet(hs1, hs2, set1, set2);
			}
		}
		return res;
	}
	
	protected void init(){
		LinkedList<Object> blockList = old_formulae.getPartitionList();	
		for (int i = 0; i < blockList.size(); i++) {
			Object o1 = blockList.get(i);
			for (int j = i+1; j < blockList.size(); j++) {
				Object o2 = blockList.get(j);
				boolean added = false; 

				//CONVERGENCE
				if(convergenceFlag){
					added = convergenceFormualae(i, o1, j, o2);
				}
				
				//FINAL
				if(finalSensitive && !added)
					added = finalFormula(o1, o2);

				//ACCEPTANCE
				if(acceptanceFlag && !added){
					added = acceptance(o1, o2);					
				}
				
				//TRACE
				if(!added){
					Set<A> l1 = nextMap.get(o1).getActions();
					Set<A> l2 = nextMap.get(o2).getActions();
					if(l1 != null && l2 != null)
						actions(i, l1, j, l2);
				}
			}
		}
	}

	protected void step(Object hs1, Object hs2) {
		Next next1 = nextMap.get(hs1);
		Next next2 = nextMap.get(hs2);
		
		Set<A> actions = next1.getActions();
		for (A act : actions) {
			// postSetMap dello stato hs1 eseguendo l'azione act
			Set<Object> pSet1 =  next1.getDest(act);
			
			// postSetMap dello stato hs2 eseguendo l'azione act
			Set<Object> pSet2 =  next2.getDest(act);
	
			//s1 = insime degli stati raggiungibili solo da hs1
			Set<Object> s1 = new HashSet<Object>(pSet1);
			s1.removeAll(pSet2);
	
			//s2 = insime degli stati raggiungibili sia da hs1 che da hs2
			Set<Object> s2 = new HashSet<Object>(pSet1);
			s2.retainAll(pSet2);
	
			//s3 = insime degli stati raggiungibili solo da hs2
			Set<Object> s3 = new HashSet<Object>(pSet2);
			s3.removeAll(pSet1);
	
			if (s1.size() == 0 && s3.size() > 0) {
				// pSet1 è un sotto insieme di pSet2 
				subSet(hs1, s2, act, hs2, s3);
			}else if (s1.size() > 0 && s3.size() == 0){ 
				// pSet2 è un sotto insieme di pSet1 
				subSet(hs2, s2, act, hs1, s1);
			} else if (s1.size() > 0 && s3.size() > 0) {
				disjoinSet(hs1, hs2, act, s1, s3);
				disjoinSet(hs2, hs1, act, s3, s1);
			}
		}
	}

	@Override
	protected Object getPartition(S s) {
		if(s.equals(p1))
			return partitions.get(evaluator.eval(s1));
		else if(s.equals(p2))
			return partitions.get(evaluator.eval(s2));
		
		return null;
	}
	
	class Next{ 	
		private HashMap<A, HashSet<Object>> nextMap;

		public Next(){
			nextMap = new HashMap<A, HashSet<Object>>();
		}
		
		public void add(A act, Set<AcceptanceState<A>> dest){
			HashSet<Object> next = nextMap.get(act);
			if(next == null){
				next = new HashSet<Object>();
				nextMap.put(act, next);
			}
			Set<Object> newDest = new HashSet<Object>(); 
			for (AcceptanceState<A> as : dest) {
				newDest.add(evaluator.eval(as));
			}
			next.addAll(newDest);
		}

		public Set<A> getActions(){
			return nextMap.keySet(); 
		}
		
		public HashSet<Object> getDest(A act){
			HashSet<Object> res = nextMap.get(act);
			if(res != null) 
				return res;
			else 
				return new HashSet<Object>();
		}
	}
}




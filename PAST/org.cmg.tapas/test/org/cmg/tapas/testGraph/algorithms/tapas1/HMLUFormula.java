package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

public class HMLUFormula {
	
	private HMLUFormula head = null;	
	private Action a = null;	
	private boolean not = false;	
	private Vector<HMLUFormula> queue = new Vector<HMLUFormula>();	
	private Vector<StatesSet> couplesSatisfied = new Vector<StatesSet>();

	public HMLUFormula(Action a, boolean not) {
		this.a = a;
		this.not = not;
	}

	public HMLUFormula getHead() {
		return head;	
	}

	public Action getAction() {
		return a;	
	}

	public Vector<HMLUFormula> getQueue() {
		return this.queue;	
	}
	
	public boolean isNot() {
		return not;	
	}	
	
	public void setAction(Action a) {
		this.a = a;
	}	

	public void setNot(boolean not) {
		this.not = not;
	}	

	public void setHead(HMLUFormula head) {
		this.head = head;
	}
	
	public void addQueue(HMLUFormula formula) {
		queue.addElement(formula);
	}		
	
	public int getCouplesSatisfiedDimension() {
		return couplesSatisfied.size();
	}	
	
	public StatesSet getCoupleSatisfiedAt(int index) {
		return couplesSatisfied.elementAt(index);
	}	
	
	public void addCoupleSatisfied(State s1, State s2) {
		StatesSet ss = new StatesSet();
		ss.addState(s1);
		ss.addState(s2);
		couplesSatisfied.addElement(ss);
	}

	public void addCoupleSatisfied(StatesSet couple) {
		if( couple.getDimension()==2 )
			couplesSatisfied.addElement(couple);
	}
	
	public boolean containCoupleSatisfied(State s1, State s2) {
		StatesSet couple = null;
		
		for( int i=0; i<getCouplesSatisfiedDimension(); i++ ) {
			couple = getCoupleSatisfiedAt(i);
			if (couple.containState(s1))
				if (couple.containState(s2))
					return true;
		}
		
		return false;
	}
	
	public boolean containCoupleSatisfied(StatesSet couple) {
		for( int i=0; i<getCouplesSatisfiedDimension(); i++ )
			if( getCoupleSatisfiedAt(i).equals(couple) ) 
				return true;
				
		return false;
	}	
	
	public void print(java.io.PrintStream console) {
		
		if( isNot() ) 
			console.append("not(");
		
		// Stampo la testa (until)
		console.append("(");
		if( head != null ) 
			head.print(console);
		else 
			console.append("tt");
		console.append(")");
		
		// Stampo l'azione		
		console.append("<<");
		getAction().print(console);
		console.append(">>");
		
		// Stampo la coda 
		Vector<HMLUFormula> formulaes = getQueue();
		console.append("(");
		if( formulaes.size()==0 ) {
			console.append("tt");
		} else {
			for( int i=0; i<formulaes.size(); i++ ) {
				if(i>0) 
					console.append(" /\\ ");
				formulaes.elementAt(i).print(console);
			}
		}		
		console.append(")");
		if( isNot() ) 
			console.append(")");
	}	
}
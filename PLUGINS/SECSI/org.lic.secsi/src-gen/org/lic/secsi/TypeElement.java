/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.lic.secsi.SecsiPackage#getTypeElement()
 * @model
 * @generated
 */
public interface TypeElement extends ReferenceableElement
{
} // TypeElement

package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessAG;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;



/**
 * La classe TestingChecker realizza l'algoritmo di verifica 
 * per l'equivalenza testig. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class TestingChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {
    /** 
     * Costruisce una nuova istanza del checker.
     */
	public TestingChecker() {
		super("Testing");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 */	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );

		// acceptance graphs
		ProcessAG p1AG = ProcessAG.buildAcceptanceGraph(p1);
		ProcessAG p2AG = ProcessAG.buildAcceptanceGraph(p2);
		
		// fusion
		// console.println("process fusion ...");
		ProcessAG pAG = ProcessAG.createProcessFusion( p1AG, p2AG );

		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();
		
		// initializing partition 
		PTPartition initialRo = TestingUtil.initPTPartition( pAG );
		
		// refinement
		// console.println("refining ...");	
		PTState initialPTState1 = initialRo.getPTState(pAG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pAG.getSecondInitState());

		// controformula
		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pAG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		// check
		boolean result = ro.checkSameClass(
			pAG.getInitState(), pAG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			TestingUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName(),
				pAG
			);
		}
		
		return result;
	}	
}
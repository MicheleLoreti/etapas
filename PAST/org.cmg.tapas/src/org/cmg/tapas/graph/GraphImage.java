/**
 * 
 */
package org.cmg.tapas.graph;


import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.cmg.tapas.graph.filter.Filter;

/**
 * @author loreti
 *
 */
public class GraphImage<S extends StateInterface,A extends ActionInterface>  implements GraphInterface<StateSet<S>,A> {

	private Graph<StateSet<S>, A> graph;
	private Map<S, StateSet<S>> map;

	public GraphImage( GraphData<S,A> g , Map<S,? extends Set<S>> map) {
		computeGraphImage(g, map);
	}
	
	public GraphImage(GraphImage<S, A> graphImage) {
		this.graph = graphImage.graph;
		this.map = graphImage.map;
	}

	protected void computeGraphImage( GraphData<S,A> g , Map<S, ? extends Set<S>> m ) {
		graph = new Graph<StateSet<S> , A>();
		map = new HashMap<S, StateSet<S>>();
		Iterator<S> iter = g.getStatesIterator();
		S state;
		
		Set<S> start;
		Set<S> end;
		Set<S> poset;
		Set<A> actions = g.getActionSet();
		iter = g.getStatesIterator();
		while( iter.hasNext() ) {
			state = iter.next();
			start = m.get(state);
			if (start != null) {
				StateSet<S> sState = map.get(state);
				if (sState == null) {
					sState = new StateSet<S>(start);
					map.put(state, sState);
				}			
				for( A action: actions ){				
					poset = g.getPostset(state,action);
					if (poset != null) {
						for( S reached: poset ) {
							end = m.get(reached);
							if (end != null) {
								StateSet<S> eState = map.get(reached);
								if (eState == null) {
									eState = new StateSet<S>(end);
									map.put(reached, eState);
								}								
								graph.addEdge( sState , action, eState );
							}
						}
					}
				}
			}
		}
	}

	public boolean edgeIsIn(StateSet<S> from, A action, StateSet<S> to) {
		return graph.edgeIsIn(from, action, to);
	}
	
	public boolean edgeIsIn(StateSet<S> from, StateSet<S> to) {
		return graph.edgeIsIn(from, to);
	}	

	public Set<A> getActionSet() {
		return graph.getActionSet();
	}

	public LinkedList<StateSet<S>> getAllStates() {
		return graph.getAllStates();
	}

	public int getEdgeCount(A action) {
		return graph.getEdgeCount(action);
	}

	public Iterator<Entry<A, StateSet<S>>> getImageIterator(StateSet<S> state,
			boolean post, EdgeFilter<StateSet<S>, A> filter) {
		return graph.getImageIterator(state, post, filter);
	}

	public int getMultiplicity(StateSet<S> src, A action, StateSet<S> dest) {
		return graph.getMultiplicity(src, action, dest);
	}

	public Set<StateSet<S>> getPostset(StateSet<S> state, A action) {
		return graph.getPostset(state, action);
	}

	public Set<StateSet<S>> getPostset(StateSet<S> state, Filter<A> filter) {
		return graph.getPostset(state, filter);
	}

	public Set<StateSet<S>> getPostset(StateSet<S> state) {
		return graph.getPostset(state);
	}

	public Map<A, ? extends Set<StateSet<S>>> getPostsetMap(StateSet<S> state) {
		return graph.getPostsetMap(state);
	}

	public Set<StateSet<S>> getPreset(StateSet<S> state, A action) {
		return graph.getPostset(state,action);
	}

	public Map<A, ? extends Set<StateSet<S>>> getPresetMap(StateSet<S> state) {
		return graph.getPresetMap(state);
	}

	public LinkedList<StateSet<S>> getStates(Filter<StateSet<S>> f) {
		return graph.getStates(f);
	}

	public Iterator<StateSet<S>> getStatesIterator() {
		return graph.getStatesIterator();
	}

	public boolean isFinal(StateSet<S> state) {
		return graph.isFinal(state);
	}

	public int numEdges() {
		return graph.numEdges();
	}

	public int numEdges(boolean multiplicity) {
		return graph.numEdges(multiplicity);
	}

	public int numStates() {
		return graph.numStates();
	}

	public boolean stateIsIn(StateSet<S> state) {
		return graph.stateIsIn(state);
	}

	public boolean addEdge(StateSet<S> src, A action, StateSet<S> dest) {
		return graph.addEdge(src, action, dest);
	}

	public void addState(StateSet<S> s) {
		graph.addState(s);
	}

	public String getNewStateName() {
		return graph.getNewStateName();
	}

	public StateSet<S> getState(String name) {
		return graph.getState(name);
	}

	public int removeAllEdge(StateSet<S> st) {
		return graph.removeAllEdge(st);
	}

	public boolean removeEdge(StateSet<S> src, A action, StateSet<S> dest) {
		return graph.removeEdge(src, action, dest);
	}

	public int removeIncomingEdge(StateSet<S> d) {
		return graph.removeIncomingEdge(d);
	}

	public int removeOutgoingEdge(StateSet<S> s) {
		return graph.removeOutgoingEdge(s);
	}

	public boolean removeState(StateSet<S> s) {
		return graph.removeState(s);
	}

	public boolean stateIsIn(String name) {
		return graph.stateIsIn(name);
	}

	public GraphImage<S,A> clone() {
		return new GraphImage<S,A>(this);
	}
	
	public StateSet<S> getStateSet( S s) {
		return map.get(s);
	}

	public Graph<StateSet<S>, A> getGraph() {
		return graph;
	}
}











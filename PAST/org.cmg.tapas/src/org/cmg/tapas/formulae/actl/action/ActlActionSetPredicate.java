/**
 * 
 */
package org.cmg.tapas.formulae.actl.action;


import java.util.HashSet;

import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlActionPredicateVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.HashSetActionFilter;
import org.cmg.tapas.processAlgebra.APProcess;



/**
 * @author loreti
 *
 */
public class ActlActionSetPredicate<S extends APProcess<S,A> , A extends ActionInterface> implements
		ActlActionPredicate<S,A> {

	private HashSet<A> set;
	private Filter<A> filter;

	public ActlActionSetPredicate(HashSet<A> set) {
		this.set = set;
		this.filter = null;
	}

	public ActlActionSetPredicate(Filter<A> filter) {
		this.set = null;
		this.filter = filter;
	}

	public ActlActionSetPredicate(String text) {
	}

	/* (non-Javadoc)
	 * @see org.cmg.actl.ActlActionPredicate#acceptVisitor(org.cmg.actl.ActlActionPredicateVisitor)
	 */
	public void acceptVisitor(ActlActionPredicateVisitor<S,A> v) {
		v.accept(this);
	}

	/* (non-Javadoc)
	 * @see org.cmg.actl.ActlActionPredicate#getFilter()
	 */
	public Filter<A> getFilter() {
		if(set != null)
			return new HashSetActionFilter<A>(set);
		
		return filter;
	}

	public String toString() {
		String toReturn = " ";
		String sep = "";
		for (A a : set) {
			toReturn += sep+a;
			sep = ", ";
		}
		return toReturn;
	}
}

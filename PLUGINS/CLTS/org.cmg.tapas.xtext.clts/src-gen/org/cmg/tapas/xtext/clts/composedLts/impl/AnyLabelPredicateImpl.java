/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.AnyLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Any Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AnyLabelPredicateImpl extends LabelPredicateImpl implements AnyLabelPredicate
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AnyLabelPredicateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.ANY_LABEL_PREDICATE;
  }

} //AnyLabelPredicateImpl

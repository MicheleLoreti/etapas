/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Any Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getAnyLabelPredicate()
 * @model
 * @generated
 */
public interface AnyLabelPredicate extends LabelPredicate
{
} // AnyLabelPredicate

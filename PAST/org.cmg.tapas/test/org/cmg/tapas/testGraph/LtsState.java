package org.cmg.tapas.testGraph;

import org.cmg.tapas.graph.StateInterface;

/**
 * @author Francesco Calzolai
 */
public class LtsState implements StateInterface{
	private String name;

	public LtsState(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the name.
	 */
	public String getId() {
		return name;
	}

	/**
	 * @param name The name to set.
	 */
	public void setId(String name) {
		this.name = name;
	}

	public void print(){
		System.out.println("Stato: "+name);
	}
	
	@Override
	public String toString(){
		return name;
	}

}

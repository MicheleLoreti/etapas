/**
 * 
 */
package org.cmg.tapas.process.exception;

/**
 * @author loreti
 *
 */
public class ProcessException extends Exception {
	
	public ProcessException() {
		super();
	}
	
	public ProcessException( String str ) {
		super( str );
	}

}

package org.cmg.tapas.xtext.clts.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.cmg.tapas.xtext.clts.services.ComposedLtsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalComposedLtsParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'model'", "';'", "'lts'", "'='", "'||'", "'|*|'", "'|'", "','", "'{'", "'}'", "'('", "')'", "'->'", "'states'", "':'", "'init'", "'rules'", "'endrules'", "'labels'", "'endlabels'", "'-'", "'label'", "'action'", "'hml'", "'formula'", "'&'", "'#['", "']'", "'true'", "'false'", "'!'", "'<'", "'>'", "'['", "'min'", "'.'", "'max'", "'ltl'", "'\\\\U'", "'\\\\X'", "'\\\\G'", "'\\\\F'", "'*'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalComposedLtsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalComposedLtsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalComposedLtsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalComposedLts.g"; }


     
     	private ComposedLtsGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ComposedLtsGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // InternalComposedLts.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalComposedLts.g:61:1: ( ruleModel EOF )
            // InternalComposedLts.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalComposedLts.g:69:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:73:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalComposedLts.g:74:1: ( ( rule__Model__Group__0 ) )
            {
            // InternalComposedLts.g:74:1: ( ( rule__Model__Group__0 ) )
            // InternalComposedLts.g:75:1: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalComposedLts.g:76:1: ( rule__Model__Group__0 )
            // InternalComposedLts.g:76:2: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleElement"
    // InternalComposedLts.g:88:1: entryRuleElement : ruleElement EOF ;
    public final void entryRuleElement() throws RecognitionException {
        try {
            // InternalComposedLts.g:89:1: ( ruleElement EOF )
            // InternalComposedLts.g:90:1: ruleElement EOF
            {
             before(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_1);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // InternalComposedLts.g:97:1: ruleElement : ( ( rule__Element__Alternatives ) ) ;
    public final void ruleElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:101:2: ( ( ( rule__Element__Alternatives ) ) )
            // InternalComposedLts.g:102:1: ( ( rule__Element__Alternatives ) )
            {
            // InternalComposedLts.g:102:1: ( ( rule__Element__Alternatives ) )
            // InternalComposedLts.g:103:1: ( rule__Element__Alternatives )
            {
             before(grammarAccess.getElementAccess().getAlternatives()); 
            // InternalComposedLts.g:104:1: ( rule__Element__Alternatives )
            // InternalComposedLts.g:104:2: rule__Element__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Element__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleLts"
    // InternalComposedLts.g:116:1: entryRuleLts : ruleLts EOF ;
    public final void entryRuleLts() throws RecognitionException {
        try {
            // InternalComposedLts.g:117:1: ( ruleLts EOF )
            // InternalComposedLts.g:118:1: ruleLts EOF
            {
             before(grammarAccess.getLtsRule()); 
            pushFollow(FOLLOW_1);
            ruleLts();

            state._fsp--;

             after(grammarAccess.getLtsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLts"


    // $ANTLR start "ruleLts"
    // InternalComposedLts.g:125:1: ruleLts : ( ( rule__Lts__Group__0 ) ) ;
    public final void ruleLts() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:129:2: ( ( ( rule__Lts__Group__0 ) ) )
            // InternalComposedLts.g:130:1: ( ( rule__Lts__Group__0 ) )
            {
            // InternalComposedLts.g:130:1: ( ( rule__Lts__Group__0 ) )
            // InternalComposedLts.g:131:1: ( rule__Lts__Group__0 )
            {
             before(grammarAccess.getLtsAccess().getGroup()); 
            // InternalComposedLts.g:132:1: ( rule__Lts__Group__0 )
            // InternalComposedLts.g:132:2: rule__Lts__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Lts__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLts"


    // $ANTLR start "entryRuleLtsBody"
    // InternalComposedLts.g:144:1: entryRuleLtsBody : ruleLtsBody EOF ;
    public final void entryRuleLtsBody() throws RecognitionException {
        try {
            // InternalComposedLts.g:145:1: ( ruleLtsBody EOF )
            // InternalComposedLts.g:146:1: ruleLtsBody EOF
            {
             before(grammarAccess.getLtsBodyRule()); 
            pushFollow(FOLLOW_1);
            ruleLtsBody();

            state._fsp--;

             after(grammarAccess.getLtsBodyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtsBody"


    // $ANTLR start "ruleLtsBody"
    // InternalComposedLts.g:153:1: ruleLtsBody : ( ( rule__LtsBody__Alternatives ) ) ;
    public final void ruleLtsBody() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:157:2: ( ( ( rule__LtsBody__Alternatives ) ) )
            // InternalComposedLts.g:158:1: ( ( rule__LtsBody__Alternatives ) )
            {
            // InternalComposedLts.g:158:1: ( ( rule__LtsBody__Alternatives ) )
            // InternalComposedLts.g:159:1: ( rule__LtsBody__Alternatives )
            {
             before(grammarAccess.getLtsBodyAccess().getAlternatives()); 
            // InternalComposedLts.g:160:1: ( rule__LtsBody__Alternatives )
            // InternalComposedLts.g:160:2: rule__LtsBody__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__LtsBody__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLtsBodyAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtsBody"


    // $ANTLR start "entryRuleLtsComposition"
    // InternalComposedLts.g:172:1: entryRuleLtsComposition : ruleLtsComposition EOF ;
    public final void entryRuleLtsComposition() throws RecognitionException {
        try {
            // InternalComposedLts.g:173:1: ( ruleLtsComposition EOF )
            // InternalComposedLts.g:174:1: ruleLtsComposition EOF
            {
             before(grammarAccess.getLtsCompositionRule()); 
            pushFollow(FOLLOW_1);
            ruleLtsComposition();

            state._fsp--;

             after(grammarAccess.getLtsCompositionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtsComposition"


    // $ANTLR start "ruleLtsComposition"
    // InternalComposedLts.g:181:1: ruleLtsComposition : ( ( rule__LtsComposition__Group__0 ) ) ;
    public final void ruleLtsComposition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:185:2: ( ( ( rule__LtsComposition__Group__0 ) ) )
            // InternalComposedLts.g:186:1: ( ( rule__LtsComposition__Group__0 ) )
            {
            // InternalComposedLts.g:186:1: ( ( rule__LtsComposition__Group__0 ) )
            // InternalComposedLts.g:187:1: ( rule__LtsComposition__Group__0 )
            {
             before(grammarAccess.getLtsCompositionAccess().getGroup()); 
            // InternalComposedLts.g:188:1: ( rule__LtsComposition__Group__0 )
            // InternalComposedLts.g:188:2: rule__LtsComposition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtsComposition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtsCompositionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtsComposition"


    // $ANTLR start "entryRuleCompositionBody"
    // InternalComposedLts.g:200:1: entryRuleCompositionBody : ruleCompositionBody EOF ;
    public final void entryRuleCompositionBody() throws RecognitionException {
        try {
            // InternalComposedLts.g:201:1: ( ruleCompositionBody EOF )
            // InternalComposedLts.g:202:1: ruleCompositionBody EOF
            {
             before(grammarAccess.getCompositionBodyRule()); 
            pushFollow(FOLLOW_1);
            ruleCompositionBody();

            state._fsp--;

             after(grammarAccess.getCompositionBodyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompositionBody"


    // $ANTLR start "ruleCompositionBody"
    // InternalComposedLts.g:209:1: ruleCompositionBody : ( ruleInterleaving ) ;
    public final void ruleCompositionBody() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:213:2: ( ( ruleInterleaving ) )
            // InternalComposedLts.g:214:1: ( ruleInterleaving )
            {
            // InternalComposedLts.g:214:1: ( ruleInterleaving )
            // InternalComposedLts.g:215:1: ruleInterleaving
            {
             before(grammarAccess.getCompositionBodyAccess().getInterleavingParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleInterleaving();

            state._fsp--;

             after(grammarAccess.getCompositionBodyAccess().getInterleavingParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompositionBody"


    // $ANTLR start "entryRuleInterleaving"
    // InternalComposedLts.g:228:1: entryRuleInterleaving : ruleInterleaving EOF ;
    public final void entryRuleInterleaving() throws RecognitionException {
        try {
            // InternalComposedLts.g:229:1: ( ruleInterleaving EOF )
            // InternalComposedLts.g:230:1: ruleInterleaving EOF
            {
             before(grammarAccess.getInterleavingRule()); 
            pushFollow(FOLLOW_1);
            ruleInterleaving();

            state._fsp--;

             after(grammarAccess.getInterleavingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterleaving"


    // $ANTLR start "ruleInterleaving"
    // InternalComposedLts.g:237:1: ruleInterleaving : ( ( rule__Interleaving__Group__0 ) ) ;
    public final void ruleInterleaving() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:241:2: ( ( ( rule__Interleaving__Group__0 ) ) )
            // InternalComposedLts.g:242:1: ( ( rule__Interleaving__Group__0 ) )
            {
            // InternalComposedLts.g:242:1: ( ( rule__Interleaving__Group__0 ) )
            // InternalComposedLts.g:243:1: ( rule__Interleaving__Group__0 )
            {
             before(grammarAccess.getInterleavingAccess().getGroup()); 
            // InternalComposedLts.g:244:1: ( rule__Interleaving__Group__0 )
            // InternalComposedLts.g:244:2: rule__Interleaving__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Interleaving__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInterleavingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterleaving"


    // $ANTLR start "entryRuleSynchronization"
    // InternalComposedLts.g:256:1: entryRuleSynchronization : ruleSynchronization EOF ;
    public final void entryRuleSynchronization() throws RecognitionException {
        try {
            // InternalComposedLts.g:257:1: ( ruleSynchronization EOF )
            // InternalComposedLts.g:258:1: ruleSynchronization EOF
            {
             before(grammarAccess.getSynchronizationRule()); 
            pushFollow(FOLLOW_1);
            ruleSynchronization();

            state._fsp--;

             after(grammarAccess.getSynchronizationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSynchronization"


    // $ANTLR start "ruleSynchronization"
    // InternalComposedLts.g:265:1: ruleSynchronization : ( ( rule__Synchronization__Group__0 ) ) ;
    public final void ruleSynchronization() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:269:2: ( ( ( rule__Synchronization__Group__0 ) ) )
            // InternalComposedLts.g:270:1: ( ( rule__Synchronization__Group__0 ) )
            {
            // InternalComposedLts.g:270:1: ( ( rule__Synchronization__Group__0 ) )
            // InternalComposedLts.g:271:1: ( rule__Synchronization__Group__0 )
            {
             before(grammarAccess.getSynchronizationAccess().getGroup()); 
            // InternalComposedLts.g:272:1: ( rule__Synchronization__Group__0 )
            // InternalComposedLts.g:272:2: rule__Synchronization__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Synchronization__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSynchronizationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSynchronization"


    // $ANTLR start "entryRuleControlledInteraction"
    // InternalComposedLts.g:284:1: entryRuleControlledInteraction : ruleControlledInteraction EOF ;
    public final void entryRuleControlledInteraction() throws RecognitionException {
        try {
            // InternalComposedLts.g:285:1: ( ruleControlledInteraction EOF )
            // InternalComposedLts.g:286:1: ruleControlledInteraction EOF
            {
             before(grammarAccess.getControlledInteractionRule()); 
            pushFollow(FOLLOW_1);
            ruleControlledInteraction();

            state._fsp--;

             after(grammarAccess.getControlledInteractionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleControlledInteraction"


    // $ANTLR start "ruleControlledInteraction"
    // InternalComposedLts.g:293:1: ruleControlledInteraction : ( ( rule__ControlledInteraction__Group__0 ) ) ;
    public final void ruleControlledInteraction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:297:2: ( ( ( rule__ControlledInteraction__Group__0 ) ) )
            // InternalComposedLts.g:298:1: ( ( rule__ControlledInteraction__Group__0 ) )
            {
            // InternalComposedLts.g:298:1: ( ( rule__ControlledInteraction__Group__0 ) )
            // InternalComposedLts.g:299:1: ( rule__ControlledInteraction__Group__0 )
            {
             before(grammarAccess.getControlledInteractionAccess().getGroup()); 
            // InternalComposedLts.g:300:1: ( rule__ControlledInteraction__Group__0 )
            // InternalComposedLts.g:300:2: rule__ControlledInteraction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getControlledInteractionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleControlledInteraction"


    // $ANTLR start "entryRuleRenaming"
    // InternalComposedLts.g:312:1: entryRuleRenaming : ruleRenaming EOF ;
    public final void entryRuleRenaming() throws RecognitionException {
        try {
            // InternalComposedLts.g:313:1: ( ruleRenaming EOF )
            // InternalComposedLts.g:314:1: ruleRenaming EOF
            {
             before(grammarAccess.getRenamingRule()); 
            pushFollow(FOLLOW_1);
            ruleRenaming();

            state._fsp--;

             after(grammarAccess.getRenamingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRenaming"


    // $ANTLR start "ruleRenaming"
    // InternalComposedLts.g:321:1: ruleRenaming : ( ( rule__Renaming__Group__0 ) ) ;
    public final void ruleRenaming() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:325:2: ( ( ( rule__Renaming__Group__0 ) ) )
            // InternalComposedLts.g:326:1: ( ( rule__Renaming__Group__0 ) )
            {
            // InternalComposedLts.g:326:1: ( ( rule__Renaming__Group__0 ) )
            // InternalComposedLts.g:327:1: ( rule__Renaming__Group__0 )
            {
             before(grammarAccess.getRenamingAccess().getGroup()); 
            // InternalComposedLts.g:328:1: ( rule__Renaming__Group__0 )
            // InternalComposedLts.g:328:2: rule__Renaming__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Renaming__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRenamingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRenaming"


    // $ANTLR start "entryRuleBaseLts"
    // InternalComposedLts.g:340:1: entryRuleBaseLts : ruleBaseLts EOF ;
    public final void entryRuleBaseLts() throws RecognitionException {
        try {
            // InternalComposedLts.g:341:1: ( ruleBaseLts EOF )
            // InternalComposedLts.g:342:1: ruleBaseLts EOF
            {
             before(grammarAccess.getBaseLtsRule()); 
            pushFollow(FOLLOW_1);
            ruleBaseLts();

            state._fsp--;

             after(grammarAccess.getBaseLtsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBaseLts"


    // $ANTLR start "ruleBaseLts"
    // InternalComposedLts.g:349:1: ruleBaseLts : ( ( rule__BaseLts__Alternatives ) ) ;
    public final void ruleBaseLts() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:353:2: ( ( ( rule__BaseLts__Alternatives ) ) )
            // InternalComposedLts.g:354:1: ( ( rule__BaseLts__Alternatives ) )
            {
            // InternalComposedLts.g:354:1: ( ( rule__BaseLts__Alternatives ) )
            // InternalComposedLts.g:355:1: ( rule__BaseLts__Alternatives )
            {
             before(grammarAccess.getBaseLtsAccess().getAlternatives()); 
            // InternalComposedLts.g:356:1: ( rule__BaseLts__Alternatives )
            // InternalComposedLts.g:356:2: rule__BaseLts__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BaseLts__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBaseLtsAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBaseLts"


    // $ANTLR start "entryRuleReference"
    // InternalComposedLts.g:368:1: entryRuleReference : ruleReference EOF ;
    public final void entryRuleReference() throws RecognitionException {
        try {
            // InternalComposedLts.g:369:1: ( ruleReference EOF )
            // InternalComposedLts.g:370:1: ruleReference EOF
            {
             before(grammarAccess.getReferenceRule()); 
            pushFollow(FOLLOW_1);
            ruleReference();

            state._fsp--;

             after(grammarAccess.getReferenceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // InternalComposedLts.g:377:1: ruleReference : ( ( rule__Reference__LtsAssignment ) ) ;
    public final void ruleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:381:2: ( ( ( rule__Reference__LtsAssignment ) ) )
            // InternalComposedLts.g:382:1: ( ( rule__Reference__LtsAssignment ) )
            {
            // InternalComposedLts.g:382:1: ( ( rule__Reference__LtsAssignment ) )
            // InternalComposedLts.g:383:1: ( rule__Reference__LtsAssignment )
            {
             before(grammarAccess.getReferenceAccess().getLtsAssignment()); 
            // InternalComposedLts.g:384:1: ( rule__Reference__LtsAssignment )
            // InternalComposedLts.g:384:2: rule__Reference__LtsAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Reference__LtsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getLtsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleMapping"
    // InternalComposedLts.g:396:1: entryRuleMapping : ruleMapping EOF ;
    public final void entryRuleMapping() throws RecognitionException {
        try {
            // InternalComposedLts.g:397:1: ( ruleMapping EOF )
            // InternalComposedLts.g:398:1: ruleMapping EOF
            {
             before(grammarAccess.getMappingRule()); 
            pushFollow(FOLLOW_1);
            ruleMapping();

            state._fsp--;

             after(grammarAccess.getMappingRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMapping"


    // $ANTLR start "ruleMapping"
    // InternalComposedLts.g:405:1: ruleMapping : ( ( rule__Mapping__Group__0 ) ) ;
    public final void ruleMapping() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:409:2: ( ( ( rule__Mapping__Group__0 ) ) )
            // InternalComposedLts.g:410:1: ( ( rule__Mapping__Group__0 ) )
            {
            // InternalComposedLts.g:410:1: ( ( rule__Mapping__Group__0 ) )
            // InternalComposedLts.g:411:1: ( rule__Mapping__Group__0 )
            {
             before(grammarAccess.getMappingAccess().getGroup()); 
            // InternalComposedLts.g:412:1: ( rule__Mapping__Group__0 )
            // InternalComposedLts.g:412:2: rule__Mapping__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMapping"


    // $ANTLR start "entryRuleLtsDeclarationBody"
    // InternalComposedLts.g:424:1: entryRuleLtsDeclarationBody : ruleLtsDeclarationBody EOF ;
    public final void entryRuleLtsDeclarationBody() throws RecognitionException {
        try {
            // InternalComposedLts.g:425:1: ( ruleLtsDeclarationBody EOF )
            // InternalComposedLts.g:426:1: ruleLtsDeclarationBody EOF
            {
             before(grammarAccess.getLtsDeclarationBodyRule()); 
            pushFollow(FOLLOW_1);
            ruleLtsDeclarationBody();

            state._fsp--;

             after(grammarAccess.getLtsDeclarationBodyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtsDeclarationBody"


    // $ANTLR start "ruleLtsDeclarationBody"
    // InternalComposedLts.g:433:1: ruleLtsDeclarationBody : ( ( rule__LtsDeclarationBody__Group__0 ) ) ;
    public final void ruleLtsDeclarationBody() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:437:2: ( ( ( rule__LtsDeclarationBody__Group__0 ) ) )
            // InternalComposedLts.g:438:1: ( ( rule__LtsDeclarationBody__Group__0 ) )
            {
            // InternalComposedLts.g:438:1: ( ( rule__LtsDeclarationBody__Group__0 ) )
            // InternalComposedLts.g:439:1: ( rule__LtsDeclarationBody__Group__0 )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getGroup()); 
            // InternalComposedLts.g:440:1: ( rule__LtsDeclarationBody__Group__0 )
            // InternalComposedLts.g:440:2: rule__LtsDeclarationBody__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtsDeclarationBody"


    // $ANTLR start "entryRuleLtsRule"
    // InternalComposedLts.g:452:1: entryRuleLtsRule : ruleLtsRule EOF ;
    public final void entryRuleLtsRule() throws RecognitionException {
        try {
            // InternalComposedLts.g:453:1: ( ruleLtsRule EOF )
            // InternalComposedLts.g:454:1: ruleLtsRule EOF
            {
             before(grammarAccess.getLtsRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleLtsRule();

            state._fsp--;

             after(grammarAccess.getLtsRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtsRule"


    // $ANTLR start "ruleLtsRule"
    // InternalComposedLts.g:461:1: ruleLtsRule : ( ( rule__LtsRule__Group__0 ) ) ;
    public final void ruleLtsRule() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:465:2: ( ( ( rule__LtsRule__Group__0 ) ) )
            // InternalComposedLts.g:466:1: ( ( rule__LtsRule__Group__0 ) )
            {
            // InternalComposedLts.g:466:1: ( ( rule__LtsRule__Group__0 ) )
            // InternalComposedLts.g:467:1: ( rule__LtsRule__Group__0 )
            {
             before(grammarAccess.getLtsRuleAccess().getGroup()); 
            // InternalComposedLts.g:468:1: ( rule__LtsRule__Group__0 )
            // InternalComposedLts.g:468:2: rule__LtsRule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtsRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtsRule"


    // $ANTLR start "entryRuleLtsState"
    // InternalComposedLts.g:480:1: entryRuleLtsState : ruleLtsState EOF ;
    public final void entryRuleLtsState() throws RecognitionException {
        try {
            // InternalComposedLts.g:481:1: ( ruleLtsState EOF )
            // InternalComposedLts.g:482:1: ruleLtsState EOF
            {
             before(grammarAccess.getLtsStateRule()); 
            pushFollow(FOLLOW_1);
            ruleLtsState();

            state._fsp--;

             after(grammarAccess.getLtsStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtsState"


    // $ANTLR start "ruleLtsState"
    // InternalComposedLts.g:489:1: ruleLtsState : ( ( rule__LtsState__NameAssignment ) ) ;
    public final void ruleLtsState() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:493:2: ( ( ( rule__LtsState__NameAssignment ) ) )
            // InternalComposedLts.g:494:1: ( ( rule__LtsState__NameAssignment ) )
            {
            // InternalComposedLts.g:494:1: ( ( rule__LtsState__NameAssignment ) )
            // InternalComposedLts.g:495:1: ( rule__LtsState__NameAssignment )
            {
             before(grammarAccess.getLtsStateAccess().getNameAssignment()); 
            // InternalComposedLts.g:496:1: ( rule__LtsState__NameAssignment )
            // InternalComposedLts.g:496:2: rule__LtsState__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__LtsState__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLtsStateAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtsState"


    // $ANTLR start "entryRuleLabelRule"
    // InternalComposedLts.g:508:1: entryRuleLabelRule : ruleLabelRule EOF ;
    public final void entryRuleLabelRule() throws RecognitionException {
        try {
            // InternalComposedLts.g:509:1: ( ruleLabelRule EOF )
            // InternalComposedLts.g:510:1: ruleLabelRule EOF
            {
             before(grammarAccess.getLabelRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleLabelRule();

            state._fsp--;

             after(grammarAccess.getLabelRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLabelRule"


    // $ANTLR start "ruleLabelRule"
    // InternalComposedLts.g:517:1: ruleLabelRule : ( ( rule__LabelRule__Group__0 ) ) ;
    public final void ruleLabelRule() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:521:2: ( ( ( rule__LabelRule__Group__0 ) ) )
            // InternalComposedLts.g:522:1: ( ( rule__LabelRule__Group__0 ) )
            {
            // InternalComposedLts.g:522:1: ( ( rule__LabelRule__Group__0 ) )
            // InternalComposedLts.g:523:1: ( rule__LabelRule__Group__0 )
            {
             before(grammarAccess.getLabelRuleAccess().getGroup()); 
            // InternalComposedLts.g:524:1: ( rule__LabelRule__Group__0 )
            // InternalComposedLts.g:524:2: rule__LabelRule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLabelRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLabelRule"


    // $ANTLR start "entryRuleRule"
    // InternalComposedLts.g:538:1: entryRuleRule : ruleRule EOF ;
    public final void entryRuleRule() throws RecognitionException {
        try {
            // InternalComposedLts.g:539:1: ( ruleRule EOF )
            // InternalComposedLts.g:540:1: ruleRule EOF
            {
             before(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleRule();

            state._fsp--;

             after(grammarAccess.getRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // InternalComposedLts.g:547:1: ruleRule : ( ( rule__Rule__Group__0 ) ) ;
    public final void ruleRule() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:551:2: ( ( ( rule__Rule__Group__0 ) ) )
            // InternalComposedLts.g:552:1: ( ( rule__Rule__Group__0 ) )
            {
            // InternalComposedLts.g:552:1: ( ( rule__Rule__Group__0 ) )
            // InternalComposedLts.g:553:1: ( rule__Rule__Group__0 )
            {
             before(grammarAccess.getRuleAccess().getGroup()); 
            // InternalComposedLts.g:554:1: ( rule__Rule__Group__0 )
            // InternalComposedLts.g:554:2: rule__Rule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRuleLabel"
    // InternalComposedLts.g:566:1: entryRuleLabel : ruleLabel EOF ;
    public final void entryRuleLabel() throws RecognitionException {
        try {
            // InternalComposedLts.g:567:1: ( ruleLabel EOF )
            // InternalComposedLts.g:568:1: ruleLabel EOF
            {
             before(grammarAccess.getLabelRule()); 
            pushFollow(FOLLOW_1);
            ruleLabel();

            state._fsp--;

             after(grammarAccess.getLabelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLabel"


    // $ANTLR start "ruleLabel"
    // InternalComposedLts.g:575:1: ruleLabel : ( ( rule__Label__Group__0 ) ) ;
    public final void ruleLabel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:579:2: ( ( ( rule__Label__Group__0 ) ) )
            // InternalComposedLts.g:580:1: ( ( rule__Label__Group__0 ) )
            {
            // InternalComposedLts.g:580:1: ( ( rule__Label__Group__0 ) )
            // InternalComposedLts.g:581:1: ( rule__Label__Group__0 )
            {
             before(grammarAccess.getLabelAccess().getGroup()); 
            // InternalComposedLts.g:582:1: ( rule__Label__Group__0 )
            // InternalComposedLts.g:582:2: rule__Label__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Label__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLabelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLabel"


    // $ANTLR start "entryRuleAction"
    // InternalComposedLts.g:594:1: entryRuleAction : ruleAction EOF ;
    public final void entryRuleAction() throws RecognitionException {
        try {
            // InternalComposedLts.g:595:1: ( ruleAction EOF )
            // InternalComposedLts.g:596:1: ruleAction EOF
            {
             before(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            ruleAction();

            state._fsp--;

             after(grammarAccess.getActionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalComposedLts.g:603:1: ruleAction : ( ( rule__Action__Group__0 ) ) ;
    public final void ruleAction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:607:2: ( ( ( rule__Action__Group__0 ) ) )
            // InternalComposedLts.g:608:1: ( ( rule__Action__Group__0 ) )
            {
            // InternalComposedLts.g:608:1: ( ( rule__Action__Group__0 ) )
            // InternalComposedLts.g:609:1: ( rule__Action__Group__0 )
            {
             before(grammarAccess.getActionAccess().getGroup()); 
            // InternalComposedLts.g:610:1: ( rule__Action__Group__0 )
            // InternalComposedLts.g:610:2: rule__Action__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleFormula"
    // InternalComposedLts.g:622:1: entryRuleFormula : ruleFormula EOF ;
    public final void entryRuleFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:623:1: ( ruleFormula EOF )
            // InternalComposedLts.g:624:1: ruleFormula EOF
            {
             before(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalComposedLts.g:631:1: ruleFormula : ( ( rule__Formula__Alternatives ) ) ;
    public final void ruleFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:635:2: ( ( ( rule__Formula__Alternatives ) ) )
            // InternalComposedLts.g:636:1: ( ( rule__Formula__Alternatives ) )
            {
            // InternalComposedLts.g:636:1: ( ( rule__Formula__Alternatives ) )
            // InternalComposedLts.g:637:1: ( rule__Formula__Alternatives )
            {
             before(grammarAccess.getFormulaAccess().getAlternatives()); 
            // InternalComposedLts.g:638:1: ( rule__Formula__Alternatives )
            // InternalComposedLts.g:638:2: rule__Formula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleHmlFormulaDeclaration"
    // InternalComposedLts.g:650:1: entryRuleHmlFormulaDeclaration : ruleHmlFormulaDeclaration EOF ;
    public final void entryRuleHmlFormulaDeclaration() throws RecognitionException {
        try {
            // InternalComposedLts.g:651:1: ( ruleHmlFormulaDeclaration EOF )
            // InternalComposedLts.g:652:1: ruleHmlFormulaDeclaration EOF
            {
             before(grammarAccess.getHmlFormulaDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlFormulaDeclaration();

            state._fsp--;

             after(grammarAccess.getHmlFormulaDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlFormulaDeclaration"


    // $ANTLR start "ruleHmlFormulaDeclaration"
    // InternalComposedLts.g:659:1: ruleHmlFormulaDeclaration : ( ( rule__HmlFormulaDeclaration__Group__0 ) ) ;
    public final void ruleHmlFormulaDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:663:2: ( ( ( rule__HmlFormulaDeclaration__Group__0 ) ) )
            // InternalComposedLts.g:664:1: ( ( rule__HmlFormulaDeclaration__Group__0 ) )
            {
            // InternalComposedLts.g:664:1: ( ( rule__HmlFormulaDeclaration__Group__0 ) )
            // InternalComposedLts.g:665:1: ( rule__HmlFormulaDeclaration__Group__0 )
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getGroup()); 
            // InternalComposedLts.g:666:1: ( rule__HmlFormulaDeclaration__Group__0 )
            // InternalComposedLts.g:666:2: rule__HmlFormulaDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlFormulaDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlFormulaDeclaration"


    // $ANTLR start "entryRuleHmlFormula"
    // InternalComposedLts.g:678:1: entryRuleHmlFormula : ruleHmlFormula EOF ;
    public final void entryRuleHmlFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:679:1: ( ruleHmlFormula EOF )
            // InternalComposedLts.g:680:1: ruleHmlFormula EOF
            {
             before(grammarAccess.getHmlFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlFormula();

            state._fsp--;

             after(grammarAccess.getHmlFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlFormula"


    // $ANTLR start "ruleHmlFormula"
    // InternalComposedLts.g:687:1: ruleHmlFormula : ( ruleHmlOrFormula ) ;
    public final void ruleHmlFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:691:2: ( ( ruleHmlOrFormula ) )
            // InternalComposedLts.g:692:1: ( ruleHmlOrFormula )
            {
            // InternalComposedLts.g:692:1: ( ruleHmlOrFormula )
            // InternalComposedLts.g:693:1: ruleHmlOrFormula
            {
             before(grammarAccess.getHmlFormulaAccess().getHmlOrFormulaParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleHmlOrFormula();

            state._fsp--;

             after(grammarAccess.getHmlFormulaAccess().getHmlOrFormulaParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlFormula"


    // $ANTLR start "entryRuleHmlOrFormula"
    // InternalComposedLts.g:706:1: entryRuleHmlOrFormula : ruleHmlOrFormula EOF ;
    public final void entryRuleHmlOrFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:707:1: ( ruleHmlOrFormula EOF )
            // InternalComposedLts.g:708:1: ruleHmlOrFormula EOF
            {
             before(grammarAccess.getHmlOrFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlOrFormula();

            state._fsp--;

             after(grammarAccess.getHmlOrFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlOrFormula"


    // $ANTLR start "ruleHmlOrFormula"
    // InternalComposedLts.g:715:1: ruleHmlOrFormula : ( ( rule__HmlOrFormula__Group__0 ) ) ;
    public final void ruleHmlOrFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:719:2: ( ( ( rule__HmlOrFormula__Group__0 ) ) )
            // InternalComposedLts.g:720:1: ( ( rule__HmlOrFormula__Group__0 ) )
            {
            // InternalComposedLts.g:720:1: ( ( rule__HmlOrFormula__Group__0 ) )
            // InternalComposedLts.g:721:1: ( rule__HmlOrFormula__Group__0 )
            {
             before(grammarAccess.getHmlOrFormulaAccess().getGroup()); 
            // InternalComposedLts.g:722:1: ( rule__HmlOrFormula__Group__0 )
            // InternalComposedLts.g:722:2: rule__HmlOrFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlOrFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlOrFormula"


    // $ANTLR start "entryRuleHmlAndFormula"
    // InternalComposedLts.g:734:1: entryRuleHmlAndFormula : ruleHmlAndFormula EOF ;
    public final void entryRuleHmlAndFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:735:1: ( ruleHmlAndFormula EOF )
            // InternalComposedLts.g:736:1: ruleHmlAndFormula EOF
            {
             before(grammarAccess.getHmlAndFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlAndFormula();

            state._fsp--;

             after(grammarAccess.getHmlAndFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlAndFormula"


    // $ANTLR start "ruleHmlAndFormula"
    // InternalComposedLts.g:743:1: ruleHmlAndFormula : ( ( rule__HmlAndFormula__Group__0 ) ) ;
    public final void ruleHmlAndFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:747:2: ( ( ( rule__HmlAndFormula__Group__0 ) ) )
            // InternalComposedLts.g:748:1: ( ( rule__HmlAndFormula__Group__0 ) )
            {
            // InternalComposedLts.g:748:1: ( ( rule__HmlAndFormula__Group__0 ) )
            // InternalComposedLts.g:749:1: ( rule__HmlAndFormula__Group__0 )
            {
             before(grammarAccess.getHmlAndFormulaAccess().getGroup()); 
            // InternalComposedLts.g:750:1: ( rule__HmlAndFormula__Group__0 )
            // InternalComposedLts.g:750:2: rule__HmlAndFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlAndFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlAndFormula"


    // $ANTLR start "entryRuleHmlBaseFormula"
    // InternalComposedLts.g:762:1: entryRuleHmlBaseFormula : ruleHmlBaseFormula EOF ;
    public final void entryRuleHmlBaseFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:763:1: ( ruleHmlBaseFormula EOF )
            // InternalComposedLts.g:764:1: ruleHmlBaseFormula EOF
            {
             before(grammarAccess.getHmlBaseFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlBaseFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlBaseFormula"


    // $ANTLR start "ruleHmlBaseFormula"
    // InternalComposedLts.g:771:1: ruleHmlBaseFormula : ( ( rule__HmlBaseFormula__Alternatives ) ) ;
    public final void ruleHmlBaseFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:775:2: ( ( ( rule__HmlBaseFormula__Alternatives ) ) )
            // InternalComposedLts.g:776:1: ( ( rule__HmlBaseFormula__Alternatives ) )
            {
            // InternalComposedLts.g:776:1: ( ( rule__HmlBaseFormula__Alternatives ) )
            // InternalComposedLts.g:777:1: ( rule__HmlBaseFormula__Alternatives )
            {
             before(grammarAccess.getHmlBaseFormulaAccess().getAlternatives()); 
            // InternalComposedLts.g:778:1: ( rule__HmlBaseFormula__Alternatives )
            // InternalComposedLts.g:778:2: rule__HmlBaseFormula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__HmlBaseFormula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getHmlBaseFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlBaseFormula"


    // $ANTLR start "entryRuleHmlPredicate"
    // InternalComposedLts.g:790:1: entryRuleHmlPredicate : ruleHmlPredicate EOF ;
    public final void entryRuleHmlPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:791:1: ( ruleHmlPredicate EOF )
            // InternalComposedLts.g:792:1: ruleHmlPredicate EOF
            {
             before(grammarAccess.getHmlPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlPredicate();

            state._fsp--;

             after(grammarAccess.getHmlPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlPredicate"


    // $ANTLR start "ruleHmlPredicate"
    // InternalComposedLts.g:799:1: ruleHmlPredicate : ( ( rule__HmlPredicate__Group__0 ) ) ;
    public final void ruleHmlPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:803:2: ( ( ( rule__HmlPredicate__Group__0 ) ) )
            // InternalComposedLts.g:804:1: ( ( rule__HmlPredicate__Group__0 ) )
            {
            // InternalComposedLts.g:804:1: ( ( rule__HmlPredicate__Group__0 ) )
            // InternalComposedLts.g:805:1: ( rule__HmlPredicate__Group__0 )
            {
             before(grammarAccess.getHmlPredicateAccess().getGroup()); 
            // InternalComposedLts.g:806:1: ( rule__HmlPredicate__Group__0 )
            // InternalComposedLts.g:806:2: rule__HmlPredicate__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlPredicate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlPredicateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlPredicate"


    // $ANTLR start "entryRuleHmlTrue"
    // InternalComposedLts.g:818:1: entryRuleHmlTrue : ruleHmlTrue EOF ;
    public final void entryRuleHmlTrue() throws RecognitionException {
        try {
            // InternalComposedLts.g:819:1: ( ruleHmlTrue EOF )
            // InternalComposedLts.g:820:1: ruleHmlTrue EOF
            {
             before(grammarAccess.getHmlTrueRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlTrue();

            state._fsp--;

             after(grammarAccess.getHmlTrueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlTrue"


    // $ANTLR start "ruleHmlTrue"
    // InternalComposedLts.g:827:1: ruleHmlTrue : ( ( rule__HmlTrue__Group__0 ) ) ;
    public final void ruleHmlTrue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:831:2: ( ( ( rule__HmlTrue__Group__0 ) ) )
            // InternalComposedLts.g:832:1: ( ( rule__HmlTrue__Group__0 ) )
            {
            // InternalComposedLts.g:832:1: ( ( rule__HmlTrue__Group__0 ) )
            // InternalComposedLts.g:833:1: ( rule__HmlTrue__Group__0 )
            {
             before(grammarAccess.getHmlTrueAccess().getGroup()); 
            // InternalComposedLts.g:834:1: ( rule__HmlTrue__Group__0 )
            // InternalComposedLts.g:834:2: rule__HmlTrue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlTrue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlTrueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlTrue"


    // $ANTLR start "entryRuleHmlFalse"
    // InternalComposedLts.g:846:1: entryRuleHmlFalse : ruleHmlFalse EOF ;
    public final void entryRuleHmlFalse() throws RecognitionException {
        try {
            // InternalComposedLts.g:847:1: ( ruleHmlFalse EOF )
            // InternalComposedLts.g:848:1: ruleHmlFalse EOF
            {
             before(grammarAccess.getHmlFalseRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlFalse();

            state._fsp--;

             after(grammarAccess.getHmlFalseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlFalse"


    // $ANTLR start "ruleHmlFalse"
    // InternalComposedLts.g:855:1: ruleHmlFalse : ( ( rule__HmlFalse__Group__0 ) ) ;
    public final void ruleHmlFalse() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:859:2: ( ( ( rule__HmlFalse__Group__0 ) ) )
            // InternalComposedLts.g:860:1: ( ( rule__HmlFalse__Group__0 ) )
            {
            // InternalComposedLts.g:860:1: ( ( rule__HmlFalse__Group__0 ) )
            // InternalComposedLts.g:861:1: ( rule__HmlFalse__Group__0 )
            {
             before(grammarAccess.getHmlFalseAccess().getGroup()); 
            // InternalComposedLts.g:862:1: ( rule__HmlFalse__Group__0 )
            // InternalComposedLts.g:862:2: rule__HmlFalse__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlFalse__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlFalseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlFalse"


    // $ANTLR start "entryRuleHmlNotFormula"
    // InternalComposedLts.g:874:1: entryRuleHmlNotFormula : ruleHmlNotFormula EOF ;
    public final void entryRuleHmlNotFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:875:1: ( ruleHmlNotFormula EOF )
            // InternalComposedLts.g:876:1: ruleHmlNotFormula EOF
            {
             before(grammarAccess.getHmlNotFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlNotFormula();

            state._fsp--;

             after(grammarAccess.getHmlNotFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlNotFormula"


    // $ANTLR start "ruleHmlNotFormula"
    // InternalComposedLts.g:883:1: ruleHmlNotFormula : ( ( rule__HmlNotFormula__Group__0 ) ) ;
    public final void ruleHmlNotFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:887:2: ( ( ( rule__HmlNotFormula__Group__0 ) ) )
            // InternalComposedLts.g:888:1: ( ( rule__HmlNotFormula__Group__0 ) )
            {
            // InternalComposedLts.g:888:1: ( ( rule__HmlNotFormula__Group__0 ) )
            // InternalComposedLts.g:889:1: ( rule__HmlNotFormula__Group__0 )
            {
             before(grammarAccess.getHmlNotFormulaAccess().getGroup()); 
            // InternalComposedLts.g:890:1: ( rule__HmlNotFormula__Group__0 )
            // InternalComposedLts.g:890:2: rule__HmlNotFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlNotFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlNotFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlNotFormula"


    // $ANTLR start "entryRuleHmlDiamondFormula"
    // InternalComposedLts.g:902:1: entryRuleHmlDiamondFormula : ruleHmlDiamondFormula EOF ;
    public final void entryRuleHmlDiamondFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:903:1: ( ruleHmlDiamondFormula EOF )
            // InternalComposedLts.g:904:1: ruleHmlDiamondFormula EOF
            {
             before(grammarAccess.getHmlDiamondFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlDiamondFormula();

            state._fsp--;

             after(grammarAccess.getHmlDiamondFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlDiamondFormula"


    // $ANTLR start "ruleHmlDiamondFormula"
    // InternalComposedLts.g:911:1: ruleHmlDiamondFormula : ( ( rule__HmlDiamondFormula__Group__0 ) ) ;
    public final void ruleHmlDiamondFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:915:2: ( ( ( rule__HmlDiamondFormula__Group__0 ) ) )
            // InternalComposedLts.g:916:1: ( ( rule__HmlDiamondFormula__Group__0 ) )
            {
            // InternalComposedLts.g:916:1: ( ( rule__HmlDiamondFormula__Group__0 ) )
            // InternalComposedLts.g:917:1: ( rule__HmlDiamondFormula__Group__0 )
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getGroup()); 
            // InternalComposedLts.g:918:1: ( rule__HmlDiamondFormula__Group__0 )
            // InternalComposedLts.g:918:2: rule__HmlDiamondFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlDiamondFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlDiamondFormula"


    // $ANTLR start "entryRuleHmlBoxFormula"
    // InternalComposedLts.g:930:1: entryRuleHmlBoxFormula : ruleHmlBoxFormula EOF ;
    public final void entryRuleHmlBoxFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:931:1: ( ruleHmlBoxFormula EOF )
            // InternalComposedLts.g:932:1: ruleHmlBoxFormula EOF
            {
             before(grammarAccess.getHmlBoxFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlBoxFormula();

            state._fsp--;

             after(grammarAccess.getHmlBoxFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlBoxFormula"


    // $ANTLR start "ruleHmlBoxFormula"
    // InternalComposedLts.g:939:1: ruleHmlBoxFormula : ( ( rule__HmlBoxFormula__Group__0 ) ) ;
    public final void ruleHmlBoxFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:943:2: ( ( ( rule__HmlBoxFormula__Group__0 ) ) )
            // InternalComposedLts.g:944:1: ( ( rule__HmlBoxFormula__Group__0 ) )
            {
            // InternalComposedLts.g:944:1: ( ( rule__HmlBoxFormula__Group__0 ) )
            // InternalComposedLts.g:945:1: ( rule__HmlBoxFormula__Group__0 )
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getGroup()); 
            // InternalComposedLts.g:946:1: ( rule__HmlBoxFormula__Group__0 )
            // InternalComposedLts.g:946:2: rule__HmlBoxFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlBoxFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlBoxFormula"


    // $ANTLR start "entryRuleHmlRecursionFormula"
    // InternalComposedLts.g:958:1: entryRuleHmlRecursionFormula : ruleHmlRecursionFormula EOF ;
    public final void entryRuleHmlRecursionFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:959:1: ( ruleHmlRecursionFormula EOF )
            // InternalComposedLts.g:960:1: ruleHmlRecursionFormula EOF
            {
             before(grammarAccess.getHmlRecursionFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlRecursionFormula();

            state._fsp--;

             after(grammarAccess.getHmlRecursionFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlRecursionFormula"


    // $ANTLR start "ruleHmlRecursionFormula"
    // InternalComposedLts.g:967:1: ruleHmlRecursionFormula : ( ( rule__HmlRecursionFormula__Alternatives ) ) ;
    public final void ruleHmlRecursionFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:971:2: ( ( ( rule__HmlRecursionFormula__Alternatives ) ) )
            // InternalComposedLts.g:972:1: ( ( rule__HmlRecursionFormula__Alternatives ) )
            {
            // InternalComposedLts.g:972:1: ( ( rule__HmlRecursionFormula__Alternatives ) )
            // InternalComposedLts.g:973:1: ( rule__HmlRecursionFormula__Alternatives )
            {
             before(grammarAccess.getHmlRecursionFormulaAccess().getAlternatives()); 
            // InternalComposedLts.g:974:1: ( rule__HmlRecursionFormula__Alternatives )
            // InternalComposedLts.g:974:2: rule__HmlRecursionFormula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__HmlRecursionFormula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getHmlRecursionFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlRecursionFormula"


    // $ANTLR start "entryRuleHmlMinFixPoint"
    // InternalComposedLts.g:986:1: entryRuleHmlMinFixPoint : ruleHmlMinFixPoint EOF ;
    public final void entryRuleHmlMinFixPoint() throws RecognitionException {
        try {
            // InternalComposedLts.g:987:1: ( ruleHmlMinFixPoint EOF )
            // InternalComposedLts.g:988:1: ruleHmlMinFixPoint EOF
            {
             before(grammarAccess.getHmlMinFixPointRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlMinFixPoint();

            state._fsp--;

             after(grammarAccess.getHmlMinFixPointRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlMinFixPoint"


    // $ANTLR start "ruleHmlMinFixPoint"
    // InternalComposedLts.g:995:1: ruleHmlMinFixPoint : ( ( rule__HmlMinFixPoint__Group__0 ) ) ;
    public final void ruleHmlMinFixPoint() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:999:2: ( ( ( rule__HmlMinFixPoint__Group__0 ) ) )
            // InternalComposedLts.g:1000:1: ( ( rule__HmlMinFixPoint__Group__0 ) )
            {
            // InternalComposedLts.g:1000:1: ( ( rule__HmlMinFixPoint__Group__0 ) )
            // InternalComposedLts.g:1001:1: ( rule__HmlMinFixPoint__Group__0 )
            {
             before(grammarAccess.getHmlMinFixPointAccess().getGroup()); 
            // InternalComposedLts.g:1002:1: ( rule__HmlMinFixPoint__Group__0 )
            // InternalComposedLts.g:1002:2: rule__HmlMinFixPoint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlMinFixPointAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlMinFixPoint"


    // $ANTLR start "entryRuleHmlMaxFixPoint"
    // InternalComposedLts.g:1014:1: entryRuleHmlMaxFixPoint : ruleHmlMaxFixPoint EOF ;
    public final void entryRuleHmlMaxFixPoint() throws RecognitionException {
        try {
            // InternalComposedLts.g:1015:1: ( ruleHmlMaxFixPoint EOF )
            // InternalComposedLts.g:1016:1: ruleHmlMaxFixPoint EOF
            {
             before(grammarAccess.getHmlMaxFixPointRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlMaxFixPoint();

            state._fsp--;

             after(grammarAccess.getHmlMaxFixPointRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlMaxFixPoint"


    // $ANTLR start "ruleHmlMaxFixPoint"
    // InternalComposedLts.g:1023:1: ruleHmlMaxFixPoint : ( ( rule__HmlMaxFixPoint__Group__0 ) ) ;
    public final void ruleHmlMaxFixPoint() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1027:2: ( ( ( rule__HmlMaxFixPoint__Group__0 ) ) )
            // InternalComposedLts.g:1028:1: ( ( rule__HmlMaxFixPoint__Group__0 ) )
            {
            // InternalComposedLts.g:1028:1: ( ( rule__HmlMaxFixPoint__Group__0 ) )
            // InternalComposedLts.g:1029:1: ( rule__HmlMaxFixPoint__Group__0 )
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getGroup()); 
            // InternalComposedLts.g:1030:1: ( rule__HmlMaxFixPoint__Group__0 )
            // InternalComposedLts.g:1030:2: rule__HmlMaxFixPoint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getHmlMaxFixPointAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlMaxFixPoint"


    // $ANTLR start "entryRuleHmlRecursionVariable"
    // InternalComposedLts.g:1042:1: entryRuleHmlRecursionVariable : ruleHmlRecursionVariable EOF ;
    public final void entryRuleHmlRecursionVariable() throws RecognitionException {
        try {
            // InternalComposedLts.g:1043:1: ( ruleHmlRecursionVariable EOF )
            // InternalComposedLts.g:1044:1: ruleHmlRecursionVariable EOF
            {
             before(grammarAccess.getHmlRecursionVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleHmlRecursionVariable();

            state._fsp--;

             after(grammarAccess.getHmlRecursionVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHmlRecursionVariable"


    // $ANTLR start "ruleHmlRecursionVariable"
    // InternalComposedLts.g:1051:1: ruleHmlRecursionVariable : ( ( rule__HmlRecursionVariable__RefeerenceAssignment ) ) ;
    public final void ruleHmlRecursionVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1055:2: ( ( ( rule__HmlRecursionVariable__RefeerenceAssignment ) ) )
            // InternalComposedLts.g:1056:1: ( ( rule__HmlRecursionVariable__RefeerenceAssignment ) )
            {
            // InternalComposedLts.g:1056:1: ( ( rule__HmlRecursionVariable__RefeerenceAssignment ) )
            // InternalComposedLts.g:1057:1: ( rule__HmlRecursionVariable__RefeerenceAssignment )
            {
             before(grammarAccess.getHmlRecursionVariableAccess().getRefeerenceAssignment()); 
            // InternalComposedLts.g:1058:1: ( rule__HmlRecursionVariable__RefeerenceAssignment )
            // InternalComposedLts.g:1058:2: rule__HmlRecursionVariable__RefeerenceAssignment
            {
            pushFollow(FOLLOW_2);
            rule__HmlRecursionVariable__RefeerenceAssignment();

            state._fsp--;


            }

             after(grammarAccess.getHmlRecursionVariableAccess().getRefeerenceAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHmlRecursionVariable"


    // $ANTLR start "entryRuleLtlFormulaDeclaration"
    // InternalComposedLts.g:1070:1: entryRuleLtlFormulaDeclaration : ruleLtlFormulaDeclaration EOF ;
    public final void entryRuleLtlFormulaDeclaration() throws RecognitionException {
        try {
            // InternalComposedLts.g:1071:1: ( ruleLtlFormulaDeclaration EOF )
            // InternalComposedLts.g:1072:1: ruleLtlFormulaDeclaration EOF
            {
             before(grammarAccess.getLtlFormulaDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlFormulaDeclaration();

            state._fsp--;

             after(grammarAccess.getLtlFormulaDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlFormulaDeclaration"


    // $ANTLR start "ruleLtlFormulaDeclaration"
    // InternalComposedLts.g:1079:1: ruleLtlFormulaDeclaration : ( ( rule__LtlFormulaDeclaration__Group__0 ) ) ;
    public final void ruleLtlFormulaDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1083:2: ( ( ( rule__LtlFormulaDeclaration__Group__0 ) ) )
            // InternalComposedLts.g:1084:1: ( ( rule__LtlFormulaDeclaration__Group__0 ) )
            {
            // InternalComposedLts.g:1084:1: ( ( rule__LtlFormulaDeclaration__Group__0 ) )
            // InternalComposedLts.g:1085:1: ( rule__LtlFormulaDeclaration__Group__0 )
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getGroup()); 
            // InternalComposedLts.g:1086:1: ( rule__LtlFormulaDeclaration__Group__0 )
            // InternalComposedLts.g:1086:2: rule__LtlFormulaDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlFormulaDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlFormulaDeclaration"


    // $ANTLR start "entryRuleLtlFormula"
    // InternalComposedLts.g:1098:1: entryRuleLtlFormula : ruleLtlFormula EOF ;
    public final void entryRuleLtlFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1099:1: ( ruleLtlFormula EOF )
            // InternalComposedLts.g:1100:1: ruleLtlFormula EOF
            {
             before(grammarAccess.getLtlFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlFormula();

            state._fsp--;

             after(grammarAccess.getLtlFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlFormula"


    // $ANTLR start "ruleLtlFormula"
    // InternalComposedLts.g:1107:1: ruleLtlFormula : ( ruleLtlOrFormula ) ;
    public final void ruleLtlFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1111:2: ( ( ruleLtlOrFormula ) )
            // InternalComposedLts.g:1112:1: ( ruleLtlOrFormula )
            {
            // InternalComposedLts.g:1112:1: ( ruleLtlOrFormula )
            // InternalComposedLts.g:1113:1: ruleLtlOrFormula
            {
             before(grammarAccess.getLtlFormulaAccess().getLtlOrFormulaParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleLtlOrFormula();

            state._fsp--;

             after(grammarAccess.getLtlFormulaAccess().getLtlOrFormulaParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlFormula"


    // $ANTLR start "entryRuleLtlOrFormula"
    // InternalComposedLts.g:1126:1: entryRuleLtlOrFormula : ruleLtlOrFormula EOF ;
    public final void entryRuleLtlOrFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1127:1: ( ruleLtlOrFormula EOF )
            // InternalComposedLts.g:1128:1: ruleLtlOrFormula EOF
            {
             before(grammarAccess.getLtlOrFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlOrFormula();

            state._fsp--;

             after(grammarAccess.getLtlOrFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlOrFormula"


    // $ANTLR start "ruleLtlOrFormula"
    // InternalComposedLts.g:1135:1: ruleLtlOrFormula : ( ( rule__LtlOrFormula__Group__0 ) ) ;
    public final void ruleLtlOrFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1139:2: ( ( ( rule__LtlOrFormula__Group__0 ) ) )
            // InternalComposedLts.g:1140:1: ( ( rule__LtlOrFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1140:1: ( ( rule__LtlOrFormula__Group__0 ) )
            // InternalComposedLts.g:1141:1: ( rule__LtlOrFormula__Group__0 )
            {
             before(grammarAccess.getLtlOrFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1142:1: ( rule__LtlOrFormula__Group__0 )
            // InternalComposedLts.g:1142:2: rule__LtlOrFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlOrFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlOrFormula"


    // $ANTLR start "entryRuleLtlAndFormula"
    // InternalComposedLts.g:1154:1: entryRuleLtlAndFormula : ruleLtlAndFormula EOF ;
    public final void entryRuleLtlAndFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1155:1: ( ruleLtlAndFormula EOF )
            // InternalComposedLts.g:1156:1: ruleLtlAndFormula EOF
            {
             before(grammarAccess.getLtlAndFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlAndFormula();

            state._fsp--;

             after(grammarAccess.getLtlAndFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlAndFormula"


    // $ANTLR start "ruleLtlAndFormula"
    // InternalComposedLts.g:1163:1: ruleLtlAndFormula : ( ( rule__LtlAndFormula__Group__0 ) ) ;
    public final void ruleLtlAndFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1167:2: ( ( ( rule__LtlAndFormula__Group__0 ) ) )
            // InternalComposedLts.g:1168:1: ( ( rule__LtlAndFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1168:1: ( ( rule__LtlAndFormula__Group__0 ) )
            // InternalComposedLts.g:1169:1: ( rule__LtlAndFormula__Group__0 )
            {
             before(grammarAccess.getLtlAndFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1170:1: ( rule__LtlAndFormula__Group__0 )
            // InternalComposedLts.g:1170:2: rule__LtlAndFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlAndFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlAndFormula"


    // $ANTLR start "entryRuleLtlUntilFormula"
    // InternalComposedLts.g:1182:1: entryRuleLtlUntilFormula : ruleLtlUntilFormula EOF ;
    public final void entryRuleLtlUntilFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1183:1: ( ruleLtlUntilFormula EOF )
            // InternalComposedLts.g:1184:1: ruleLtlUntilFormula EOF
            {
             before(grammarAccess.getLtlUntilFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlUntilFormula();

            state._fsp--;

             after(grammarAccess.getLtlUntilFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlUntilFormula"


    // $ANTLR start "ruleLtlUntilFormula"
    // InternalComposedLts.g:1191:1: ruleLtlUntilFormula : ( ( rule__LtlUntilFormula__Group__0 ) ) ;
    public final void ruleLtlUntilFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1195:2: ( ( ( rule__LtlUntilFormula__Group__0 ) ) )
            // InternalComposedLts.g:1196:1: ( ( rule__LtlUntilFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1196:1: ( ( rule__LtlUntilFormula__Group__0 ) )
            // InternalComposedLts.g:1197:1: ( rule__LtlUntilFormula__Group__0 )
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1198:1: ( rule__LtlUntilFormula__Group__0 )
            // InternalComposedLts.g:1198:2: rule__LtlUntilFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlUntilFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlUntilFormula"


    // $ANTLR start "entryRuleLtlBaseFormula"
    // InternalComposedLts.g:1210:1: entryRuleLtlBaseFormula : ruleLtlBaseFormula EOF ;
    public final void entryRuleLtlBaseFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1211:1: ( ruleLtlBaseFormula EOF )
            // InternalComposedLts.g:1212:1: ruleLtlBaseFormula EOF
            {
             before(grammarAccess.getLtlBaseFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlBaseFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlBaseFormula"


    // $ANTLR start "ruleLtlBaseFormula"
    // InternalComposedLts.g:1219:1: ruleLtlBaseFormula : ( ( rule__LtlBaseFormula__Alternatives ) ) ;
    public final void ruleLtlBaseFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1223:2: ( ( ( rule__LtlBaseFormula__Alternatives ) ) )
            // InternalComposedLts.g:1224:1: ( ( rule__LtlBaseFormula__Alternatives ) )
            {
            // InternalComposedLts.g:1224:1: ( ( rule__LtlBaseFormula__Alternatives ) )
            // InternalComposedLts.g:1225:1: ( rule__LtlBaseFormula__Alternatives )
            {
             before(grammarAccess.getLtlBaseFormulaAccess().getAlternatives()); 
            // InternalComposedLts.g:1226:1: ( rule__LtlBaseFormula__Alternatives )
            // InternalComposedLts.g:1226:2: rule__LtlBaseFormula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__LtlBaseFormula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLtlBaseFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlBaseFormula"


    // $ANTLR start "entryRuleLtlAtomicProposition"
    // InternalComposedLts.g:1238:1: entryRuleLtlAtomicProposition : ruleLtlAtomicProposition EOF ;
    public final void entryRuleLtlAtomicProposition() throws RecognitionException {
        try {
            // InternalComposedLts.g:1239:1: ( ruleLtlAtomicProposition EOF )
            // InternalComposedLts.g:1240:1: ruleLtlAtomicProposition EOF
            {
             before(grammarAccess.getLtlAtomicPropositionRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlAtomicProposition();

            state._fsp--;

             after(grammarAccess.getLtlAtomicPropositionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlAtomicProposition"


    // $ANTLR start "ruleLtlAtomicProposition"
    // InternalComposedLts.g:1247:1: ruleLtlAtomicProposition : ( ( rule__LtlAtomicProposition__PropAssignment ) ) ;
    public final void ruleLtlAtomicProposition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1251:2: ( ( ( rule__LtlAtomicProposition__PropAssignment ) ) )
            // InternalComposedLts.g:1252:1: ( ( rule__LtlAtomicProposition__PropAssignment ) )
            {
            // InternalComposedLts.g:1252:1: ( ( rule__LtlAtomicProposition__PropAssignment ) )
            // InternalComposedLts.g:1253:1: ( rule__LtlAtomicProposition__PropAssignment )
            {
             before(grammarAccess.getLtlAtomicPropositionAccess().getPropAssignment()); 
            // InternalComposedLts.g:1254:1: ( rule__LtlAtomicProposition__PropAssignment )
            // InternalComposedLts.g:1254:2: rule__LtlAtomicProposition__PropAssignment
            {
            pushFollow(FOLLOW_2);
            rule__LtlAtomicProposition__PropAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLtlAtomicPropositionAccess().getPropAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlAtomicProposition"


    // $ANTLR start "entryRuleLtlNextFormula"
    // InternalComposedLts.g:1266:1: entryRuleLtlNextFormula : ruleLtlNextFormula EOF ;
    public final void entryRuleLtlNextFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1267:1: ( ruleLtlNextFormula EOF )
            // InternalComposedLts.g:1268:1: ruleLtlNextFormula EOF
            {
             before(grammarAccess.getLtlNextFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlNextFormula();

            state._fsp--;

             after(grammarAccess.getLtlNextFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlNextFormula"


    // $ANTLR start "ruleLtlNextFormula"
    // InternalComposedLts.g:1275:1: ruleLtlNextFormula : ( ( rule__LtlNextFormula__Group__0 ) ) ;
    public final void ruleLtlNextFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1279:2: ( ( ( rule__LtlNextFormula__Group__0 ) ) )
            // InternalComposedLts.g:1280:1: ( ( rule__LtlNextFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1280:1: ( ( rule__LtlNextFormula__Group__0 ) )
            // InternalComposedLts.g:1281:1: ( rule__LtlNextFormula__Group__0 )
            {
             before(grammarAccess.getLtlNextFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1282:1: ( rule__LtlNextFormula__Group__0 )
            // InternalComposedLts.g:1282:2: rule__LtlNextFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlNextFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlNextFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlNextFormula"


    // $ANTLR start "entryRuleLtlAlwaysFormula"
    // InternalComposedLts.g:1294:1: entryRuleLtlAlwaysFormula : ruleLtlAlwaysFormula EOF ;
    public final void entryRuleLtlAlwaysFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1295:1: ( ruleLtlAlwaysFormula EOF )
            // InternalComposedLts.g:1296:1: ruleLtlAlwaysFormula EOF
            {
             before(grammarAccess.getLtlAlwaysFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlAlwaysFormula();

            state._fsp--;

             after(grammarAccess.getLtlAlwaysFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlAlwaysFormula"


    // $ANTLR start "ruleLtlAlwaysFormula"
    // InternalComposedLts.g:1303:1: ruleLtlAlwaysFormula : ( ( rule__LtlAlwaysFormula__Group__0 ) ) ;
    public final void ruleLtlAlwaysFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1307:2: ( ( ( rule__LtlAlwaysFormula__Group__0 ) ) )
            // InternalComposedLts.g:1308:1: ( ( rule__LtlAlwaysFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1308:1: ( ( rule__LtlAlwaysFormula__Group__0 ) )
            // InternalComposedLts.g:1309:1: ( rule__LtlAlwaysFormula__Group__0 )
            {
             before(grammarAccess.getLtlAlwaysFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1310:1: ( rule__LtlAlwaysFormula__Group__0 )
            // InternalComposedLts.g:1310:2: rule__LtlAlwaysFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlAlwaysFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlAlwaysFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlAlwaysFormula"


    // $ANTLR start "entryRuleLtlEventuallyFormula"
    // InternalComposedLts.g:1322:1: entryRuleLtlEventuallyFormula : ruleLtlEventuallyFormula EOF ;
    public final void entryRuleLtlEventuallyFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1323:1: ( ruleLtlEventuallyFormula EOF )
            // InternalComposedLts.g:1324:1: ruleLtlEventuallyFormula EOF
            {
             before(grammarAccess.getLtlEventuallyFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlEventuallyFormula();

            state._fsp--;

             after(grammarAccess.getLtlEventuallyFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlEventuallyFormula"


    // $ANTLR start "ruleLtlEventuallyFormula"
    // InternalComposedLts.g:1331:1: ruleLtlEventuallyFormula : ( ( rule__LtlEventuallyFormula__Group__0 ) ) ;
    public final void ruleLtlEventuallyFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1335:2: ( ( ( rule__LtlEventuallyFormula__Group__0 ) ) )
            // InternalComposedLts.g:1336:1: ( ( rule__LtlEventuallyFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1336:1: ( ( rule__LtlEventuallyFormula__Group__0 ) )
            // InternalComposedLts.g:1337:1: ( rule__LtlEventuallyFormula__Group__0 )
            {
             before(grammarAccess.getLtlEventuallyFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1338:1: ( rule__LtlEventuallyFormula__Group__0 )
            // InternalComposedLts.g:1338:2: rule__LtlEventuallyFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlEventuallyFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlEventuallyFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlEventuallyFormula"


    // $ANTLR start "entryRuleLtlNotFormula"
    // InternalComposedLts.g:1350:1: entryRuleLtlNotFormula : ruleLtlNotFormula EOF ;
    public final void entryRuleLtlNotFormula() throws RecognitionException {
        try {
            // InternalComposedLts.g:1351:1: ( ruleLtlNotFormula EOF )
            // InternalComposedLts.g:1352:1: ruleLtlNotFormula EOF
            {
             before(grammarAccess.getLtlNotFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlNotFormula();

            state._fsp--;

             after(grammarAccess.getLtlNotFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlNotFormula"


    // $ANTLR start "ruleLtlNotFormula"
    // InternalComposedLts.g:1359:1: ruleLtlNotFormula : ( ( rule__LtlNotFormula__Group__0 ) ) ;
    public final void ruleLtlNotFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1363:2: ( ( ( rule__LtlNotFormula__Group__0 ) ) )
            // InternalComposedLts.g:1364:1: ( ( rule__LtlNotFormula__Group__0 ) )
            {
            // InternalComposedLts.g:1364:1: ( ( rule__LtlNotFormula__Group__0 ) )
            // InternalComposedLts.g:1365:1: ( rule__LtlNotFormula__Group__0 )
            {
             before(grammarAccess.getLtlNotFormulaAccess().getGroup()); 
            // InternalComposedLts.g:1366:1: ( rule__LtlNotFormula__Group__0 )
            // InternalComposedLts.g:1366:2: rule__LtlNotFormula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlNotFormula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlNotFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlNotFormula"


    // $ANTLR start "entryRuleLtlTrue"
    // InternalComposedLts.g:1378:1: entryRuleLtlTrue : ruleLtlTrue EOF ;
    public final void entryRuleLtlTrue() throws RecognitionException {
        try {
            // InternalComposedLts.g:1379:1: ( ruleLtlTrue EOF )
            // InternalComposedLts.g:1380:1: ruleLtlTrue EOF
            {
             before(grammarAccess.getLtlTrueRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlTrue();

            state._fsp--;

             after(grammarAccess.getLtlTrueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlTrue"


    // $ANTLR start "ruleLtlTrue"
    // InternalComposedLts.g:1387:1: ruleLtlTrue : ( ( rule__LtlTrue__Group__0 ) ) ;
    public final void ruleLtlTrue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1391:2: ( ( ( rule__LtlTrue__Group__0 ) ) )
            // InternalComposedLts.g:1392:1: ( ( rule__LtlTrue__Group__0 ) )
            {
            // InternalComposedLts.g:1392:1: ( ( rule__LtlTrue__Group__0 ) )
            // InternalComposedLts.g:1393:1: ( rule__LtlTrue__Group__0 )
            {
             before(grammarAccess.getLtlTrueAccess().getGroup()); 
            // InternalComposedLts.g:1394:1: ( rule__LtlTrue__Group__0 )
            // InternalComposedLts.g:1394:2: rule__LtlTrue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlTrue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlTrueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlTrue"


    // $ANTLR start "entryRuleLtlFalse"
    // InternalComposedLts.g:1406:1: entryRuleLtlFalse : ruleLtlFalse EOF ;
    public final void entryRuleLtlFalse() throws RecognitionException {
        try {
            // InternalComposedLts.g:1407:1: ( ruleLtlFalse EOF )
            // InternalComposedLts.g:1408:1: ruleLtlFalse EOF
            {
             before(grammarAccess.getLtlFalseRule()); 
            pushFollow(FOLLOW_1);
            ruleLtlFalse();

            state._fsp--;

             after(grammarAccess.getLtlFalseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLtlFalse"


    // $ANTLR start "ruleLtlFalse"
    // InternalComposedLts.g:1415:1: ruleLtlFalse : ( ( rule__LtlFalse__Group__0 ) ) ;
    public final void ruleLtlFalse() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1419:2: ( ( ( rule__LtlFalse__Group__0 ) ) )
            // InternalComposedLts.g:1420:1: ( ( rule__LtlFalse__Group__0 ) )
            {
            // InternalComposedLts.g:1420:1: ( ( rule__LtlFalse__Group__0 ) )
            // InternalComposedLts.g:1421:1: ( rule__LtlFalse__Group__0 )
            {
             before(grammarAccess.getLtlFalseAccess().getGroup()); 
            // InternalComposedLts.g:1422:1: ( rule__LtlFalse__Group__0 )
            // InternalComposedLts.g:1422:2: rule__LtlFalse__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LtlFalse__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLtlFalseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLtlFalse"


    // $ANTLR start "entryRuleLabelPredicate"
    // InternalComposedLts.g:1434:1: entryRuleLabelPredicate : ruleLabelPredicate EOF ;
    public final void entryRuleLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1435:1: ( ruleLabelPredicate EOF )
            // InternalComposedLts.g:1436:1: ruleLabelPredicate EOF
            {
             before(grammarAccess.getLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleLabelPredicate();

            state._fsp--;

             after(grammarAccess.getLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLabelPredicate"


    // $ANTLR start "ruleLabelPredicate"
    // InternalComposedLts.g:1443:1: ruleLabelPredicate : ( ruleCupLabelPredicate ) ;
    public final void ruleLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1447:2: ( ( ruleCupLabelPredicate ) )
            // InternalComposedLts.g:1448:1: ( ruleCupLabelPredicate )
            {
            // InternalComposedLts.g:1448:1: ( ruleCupLabelPredicate )
            // InternalComposedLts.g:1449:1: ruleCupLabelPredicate
            {
             before(grammarAccess.getLabelPredicateAccess().getCupLabelPredicateParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleCupLabelPredicate();

            state._fsp--;

             after(grammarAccess.getLabelPredicateAccess().getCupLabelPredicateParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLabelPredicate"


    // $ANTLR start "entryRuleCupLabelPredicate"
    // InternalComposedLts.g:1462:1: entryRuleCupLabelPredicate : ruleCupLabelPredicate EOF ;
    public final void entryRuleCupLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1463:1: ( ruleCupLabelPredicate EOF )
            // InternalComposedLts.g:1464:1: ruleCupLabelPredicate EOF
            {
             before(grammarAccess.getCupLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleCupLabelPredicate();

            state._fsp--;

             after(grammarAccess.getCupLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCupLabelPredicate"


    // $ANTLR start "ruleCupLabelPredicate"
    // InternalComposedLts.g:1471:1: ruleCupLabelPredicate : ( ( rule__CupLabelPredicate__Group__0 ) ) ;
    public final void ruleCupLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1475:2: ( ( ( rule__CupLabelPredicate__Group__0 ) ) )
            // InternalComposedLts.g:1476:1: ( ( rule__CupLabelPredicate__Group__0 ) )
            {
            // InternalComposedLts.g:1476:1: ( ( rule__CupLabelPredicate__Group__0 ) )
            // InternalComposedLts.g:1477:1: ( rule__CupLabelPredicate__Group__0 )
            {
             before(grammarAccess.getCupLabelPredicateAccess().getGroup()); 
            // InternalComposedLts.g:1478:1: ( rule__CupLabelPredicate__Group__0 )
            // InternalComposedLts.g:1478:2: rule__CupLabelPredicate__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCupLabelPredicateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCupLabelPredicate"


    // $ANTLR start "entryRuleCapLabelPredicate"
    // InternalComposedLts.g:1490:1: entryRuleCapLabelPredicate : ruleCapLabelPredicate EOF ;
    public final void entryRuleCapLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1491:1: ( ruleCapLabelPredicate EOF )
            // InternalComposedLts.g:1492:1: ruleCapLabelPredicate EOF
            {
             before(grammarAccess.getCapLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleCapLabelPredicate();

            state._fsp--;

             after(grammarAccess.getCapLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCapLabelPredicate"


    // $ANTLR start "ruleCapLabelPredicate"
    // InternalComposedLts.g:1499:1: ruleCapLabelPredicate : ( ( rule__CapLabelPredicate__Group__0 ) ) ;
    public final void ruleCapLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1503:2: ( ( ( rule__CapLabelPredicate__Group__0 ) ) )
            // InternalComposedLts.g:1504:1: ( ( rule__CapLabelPredicate__Group__0 ) )
            {
            // InternalComposedLts.g:1504:1: ( ( rule__CapLabelPredicate__Group__0 ) )
            // InternalComposedLts.g:1505:1: ( rule__CapLabelPredicate__Group__0 )
            {
             before(grammarAccess.getCapLabelPredicateAccess().getGroup()); 
            // InternalComposedLts.g:1506:1: ( rule__CapLabelPredicate__Group__0 )
            // InternalComposedLts.g:1506:2: rule__CapLabelPredicate__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCapLabelPredicateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCapLabelPredicate"


    // $ANTLR start "entryRuleBaseLabelPredicate"
    // InternalComposedLts.g:1518:1: entryRuleBaseLabelPredicate : ruleBaseLabelPredicate EOF ;
    public final void entryRuleBaseLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1519:1: ( ruleBaseLabelPredicate EOF )
            // InternalComposedLts.g:1520:1: ruleBaseLabelPredicate EOF
            {
             before(grammarAccess.getBaseLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleBaseLabelPredicate();

            state._fsp--;

             after(grammarAccess.getBaseLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBaseLabelPredicate"


    // $ANTLR start "ruleBaseLabelPredicate"
    // InternalComposedLts.g:1527:1: ruleBaseLabelPredicate : ( ( rule__BaseLabelPredicate__Alternatives ) ) ;
    public final void ruleBaseLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1531:2: ( ( ( rule__BaseLabelPredicate__Alternatives ) ) )
            // InternalComposedLts.g:1532:1: ( ( rule__BaseLabelPredicate__Alternatives ) )
            {
            // InternalComposedLts.g:1532:1: ( ( rule__BaseLabelPredicate__Alternatives ) )
            // InternalComposedLts.g:1533:1: ( rule__BaseLabelPredicate__Alternatives )
            {
             before(grammarAccess.getBaseLabelPredicateAccess().getAlternatives()); 
            // InternalComposedLts.g:1534:1: ( rule__BaseLabelPredicate__Alternatives )
            // InternalComposedLts.g:1534:2: rule__BaseLabelPredicate__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BaseLabelPredicate__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBaseLabelPredicateAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBaseLabelPredicate"


    // $ANTLR start "entryRuleNotLabelPredicate"
    // InternalComposedLts.g:1546:1: entryRuleNotLabelPredicate : ruleNotLabelPredicate EOF ;
    public final void entryRuleNotLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1547:1: ( ruleNotLabelPredicate EOF )
            // InternalComposedLts.g:1548:1: ruleNotLabelPredicate EOF
            {
             before(grammarAccess.getNotLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleNotLabelPredicate();

            state._fsp--;

             after(grammarAccess.getNotLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNotLabelPredicate"


    // $ANTLR start "ruleNotLabelPredicate"
    // InternalComposedLts.g:1555:1: ruleNotLabelPredicate : ( ( rule__NotLabelPredicate__Group__0 ) ) ;
    public final void ruleNotLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1559:2: ( ( ( rule__NotLabelPredicate__Group__0 ) ) )
            // InternalComposedLts.g:1560:1: ( ( rule__NotLabelPredicate__Group__0 ) )
            {
            // InternalComposedLts.g:1560:1: ( ( rule__NotLabelPredicate__Group__0 ) )
            // InternalComposedLts.g:1561:1: ( rule__NotLabelPredicate__Group__0 )
            {
             before(grammarAccess.getNotLabelPredicateAccess().getGroup()); 
            // InternalComposedLts.g:1562:1: ( rule__NotLabelPredicate__Group__0 )
            // InternalComposedLts.g:1562:2: rule__NotLabelPredicate__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NotLabelPredicate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotLabelPredicateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNotLabelPredicate"


    // $ANTLR start "entryRuleActionLabelPredicate"
    // InternalComposedLts.g:1574:1: entryRuleActionLabelPredicate : ruleActionLabelPredicate EOF ;
    public final void entryRuleActionLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1575:1: ( ruleActionLabelPredicate EOF )
            // InternalComposedLts.g:1576:1: ruleActionLabelPredicate EOF
            {
             before(grammarAccess.getActionLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleActionLabelPredicate();

            state._fsp--;

             after(grammarAccess.getActionLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleActionLabelPredicate"


    // $ANTLR start "ruleActionLabelPredicate"
    // InternalComposedLts.g:1583:1: ruleActionLabelPredicate : ( ( rule__ActionLabelPredicate__ActAssignment ) ) ;
    public final void ruleActionLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1587:2: ( ( ( rule__ActionLabelPredicate__ActAssignment ) ) )
            // InternalComposedLts.g:1588:1: ( ( rule__ActionLabelPredicate__ActAssignment ) )
            {
            // InternalComposedLts.g:1588:1: ( ( rule__ActionLabelPredicate__ActAssignment ) )
            // InternalComposedLts.g:1589:1: ( rule__ActionLabelPredicate__ActAssignment )
            {
             before(grammarAccess.getActionLabelPredicateAccess().getActAssignment()); 
            // InternalComposedLts.g:1590:1: ( rule__ActionLabelPredicate__ActAssignment )
            // InternalComposedLts.g:1590:2: rule__ActionLabelPredicate__ActAssignment
            {
            pushFollow(FOLLOW_2);
            rule__ActionLabelPredicate__ActAssignment();

            state._fsp--;


            }

             after(grammarAccess.getActionLabelPredicateAccess().getActAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleActionLabelPredicate"


    // $ANTLR start "entryRuleAnyLabelPredicate"
    // InternalComposedLts.g:1602:1: entryRuleAnyLabelPredicate : ruleAnyLabelPredicate EOF ;
    public final void entryRuleAnyLabelPredicate() throws RecognitionException {
        try {
            // InternalComposedLts.g:1603:1: ( ruleAnyLabelPredicate EOF )
            // InternalComposedLts.g:1604:1: ruleAnyLabelPredicate EOF
            {
             before(grammarAccess.getAnyLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            ruleAnyLabelPredicate();

            state._fsp--;

             after(grammarAccess.getAnyLabelPredicateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnyLabelPredicate"


    // $ANTLR start "ruleAnyLabelPredicate"
    // InternalComposedLts.g:1611:1: ruleAnyLabelPredicate : ( ( rule__AnyLabelPredicate__Group__0 ) ) ;
    public final void ruleAnyLabelPredicate() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1615:2: ( ( ( rule__AnyLabelPredicate__Group__0 ) ) )
            // InternalComposedLts.g:1616:1: ( ( rule__AnyLabelPredicate__Group__0 ) )
            {
            // InternalComposedLts.g:1616:1: ( ( rule__AnyLabelPredicate__Group__0 ) )
            // InternalComposedLts.g:1617:1: ( rule__AnyLabelPredicate__Group__0 )
            {
             before(grammarAccess.getAnyLabelPredicateAccess().getGroup()); 
            // InternalComposedLts.g:1618:1: ( rule__AnyLabelPredicate__Group__0 )
            // InternalComposedLts.g:1618:2: rule__AnyLabelPredicate__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AnyLabelPredicate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnyLabelPredicateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnyLabelPredicate"


    // $ANTLR start "rule__Element__Alternatives"
    // InternalComposedLts.g:1630:1: rule__Element__Alternatives : ( ( ruleAction ) | ( ruleLts ) | ( ruleFormula ) | ( ruleLabel ) );
    public final void rule__Element__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1634:1: ( ( ruleAction ) | ( ruleLts ) | ( ruleFormula ) | ( ruleLabel ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt1=1;
                }
                break;
            case 13:
                {
                alt1=2;
                }
                break;
            case 34:
            case 48:
                {
                alt1=3;
                }
                break;
            case 32:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalComposedLts.g:1635:1: ( ruleAction )
                    {
                    // InternalComposedLts.g:1635:1: ( ruleAction )
                    // InternalComposedLts.g:1636:1: ruleAction
                    {
                     before(grammarAccess.getElementAccess().getActionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAction();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getActionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1641:6: ( ruleLts )
                    {
                    // InternalComposedLts.g:1641:6: ( ruleLts )
                    // InternalComposedLts.g:1642:1: ruleLts
                    {
                     before(grammarAccess.getElementAccess().getLtsParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLts();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getLtsParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:1647:6: ( ruleFormula )
                    {
                    // InternalComposedLts.g:1647:6: ( ruleFormula )
                    // InternalComposedLts.g:1648:1: ruleFormula
                    {
                     before(grammarAccess.getElementAccess().getFormulaParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleFormula();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getFormulaParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalComposedLts.g:1653:6: ( ruleLabel )
                    {
                    // InternalComposedLts.g:1653:6: ( ruleLabel )
                    // InternalComposedLts.g:1654:1: ruleLabel
                    {
                     before(grammarAccess.getElementAccess().getLabelParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleLabel();

                    state._fsp--;

                     after(grammarAccess.getElementAccess().getLabelParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Element__Alternatives"


    // $ANTLR start "rule__LtsBody__Alternatives"
    // InternalComposedLts.g:1664:1: rule__LtsBody__Alternatives : ( ( ruleLtsDeclarationBody ) | ( ruleLtsComposition ) );
    public final void rule__LtsBody__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1668:1: ( ( ruleLtsDeclarationBody ) | ( ruleLtsComposition ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==19) ) {
                alt2=1;
            }
            else if ( (LA2_0==14) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalComposedLts.g:1669:1: ( ruleLtsDeclarationBody )
                    {
                    // InternalComposedLts.g:1669:1: ( ruleLtsDeclarationBody )
                    // InternalComposedLts.g:1670:1: ruleLtsDeclarationBody
                    {
                     before(grammarAccess.getLtsBodyAccess().getLtsDeclarationBodyParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLtsDeclarationBody();

                    state._fsp--;

                     after(grammarAccess.getLtsBodyAccess().getLtsDeclarationBodyParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1675:6: ( ruleLtsComposition )
                    {
                    // InternalComposedLts.g:1675:6: ( ruleLtsComposition )
                    // InternalComposedLts.g:1676:1: ruleLtsComposition
                    {
                     before(grammarAccess.getLtsBodyAccess().getLtsCompositionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLtsComposition();

                    state._fsp--;

                     after(grammarAccess.getLtsBodyAccess().getLtsCompositionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsBody__Alternatives"


    // $ANTLR start "rule__BaseLts__Alternatives"
    // InternalComposedLts.g:1686:1: rule__BaseLts__Alternatives : ( ( ruleReference ) | ( ( rule__BaseLts__Group_1__0 ) ) );
    public final void rule__BaseLts__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1690:1: ( ( ruleReference ) | ( ( rule__BaseLts__Group_1__0 ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            else if ( (LA3_0==21) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalComposedLts.g:1691:1: ( ruleReference )
                    {
                    // InternalComposedLts.g:1691:1: ( ruleReference )
                    // InternalComposedLts.g:1692:1: ruleReference
                    {
                     before(grammarAccess.getBaseLtsAccess().getReferenceParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleReference();

                    state._fsp--;

                     after(grammarAccess.getBaseLtsAccess().getReferenceParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1697:6: ( ( rule__BaseLts__Group_1__0 ) )
                    {
                    // InternalComposedLts.g:1697:6: ( ( rule__BaseLts__Group_1__0 ) )
                    // InternalComposedLts.g:1698:1: ( rule__BaseLts__Group_1__0 )
                    {
                     before(grammarAccess.getBaseLtsAccess().getGroup_1()); 
                    // InternalComposedLts.g:1699:1: ( rule__BaseLts__Group_1__0 )
                    // InternalComposedLts.g:1699:2: rule__BaseLts__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BaseLts__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBaseLtsAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Alternatives"


    // $ANTLR start "rule__Formula__Alternatives"
    // InternalComposedLts.g:1708:1: rule__Formula__Alternatives : ( ( ruleLtlFormulaDeclaration ) | ( ruleHmlFormulaDeclaration ) );
    public final void rule__Formula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1712:1: ( ( ruleLtlFormulaDeclaration ) | ( ruleHmlFormulaDeclaration ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==48) ) {
                alt4=1;
            }
            else if ( (LA4_0==34) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalComposedLts.g:1713:1: ( ruleLtlFormulaDeclaration )
                    {
                    // InternalComposedLts.g:1713:1: ( ruleLtlFormulaDeclaration )
                    // InternalComposedLts.g:1714:1: ruleLtlFormulaDeclaration
                    {
                     before(grammarAccess.getFormulaAccess().getLtlFormulaDeclarationParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlFormulaDeclaration();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getLtlFormulaDeclarationParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1719:6: ( ruleHmlFormulaDeclaration )
                    {
                    // InternalComposedLts.g:1719:6: ( ruleHmlFormulaDeclaration )
                    // InternalComposedLts.g:1720:1: ruleHmlFormulaDeclaration
                    {
                     before(grammarAccess.getFormulaAccess().getHmlFormulaDeclarationParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlFormulaDeclaration();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getHmlFormulaDeclarationParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Alternatives"


    // $ANTLR start "rule__HmlBaseFormula__Alternatives"
    // InternalComposedLts.g:1730:1: rule__HmlBaseFormula__Alternatives : ( ( ruleHmlTrue ) | ( ruleHmlFalse ) | ( ( rule__HmlBaseFormula__Group_2__0 ) ) | ( ruleHmlNotFormula ) | ( ruleHmlDiamondFormula ) | ( ruleHmlBoxFormula ) | ( ruleHmlRecursionFormula ) | ( ruleHmlRecursionVariable ) | ( ruleHmlPredicate ) );
    public final void rule__HmlBaseFormula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1734:1: ( ( ruleHmlTrue ) | ( ruleHmlFalse ) | ( ( rule__HmlBaseFormula__Group_2__0 ) ) | ( ruleHmlNotFormula ) | ( ruleHmlDiamondFormula ) | ( ruleHmlBoxFormula ) | ( ruleHmlRecursionFormula ) | ( ruleHmlRecursionVariable ) | ( ruleHmlPredicate ) )
            int alt5=9;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt5=1;
                }
                break;
            case 40:
                {
                alt5=2;
                }
                break;
            case 21:
                {
                alt5=3;
                }
                break;
            case 41:
                {
                alt5=4;
                }
                break;
            case 42:
                {
                alt5=5;
                }
                break;
            case 44:
                {
                alt5=6;
                }
                break;
            case 45:
            case 47:
                {
                alt5=7;
                }
                break;
            case RULE_ID:
                {
                alt5=8;
                }
                break;
            case 37:
                {
                alt5=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalComposedLts.g:1735:1: ( ruleHmlTrue )
                    {
                    // InternalComposedLts.g:1735:1: ( ruleHmlTrue )
                    // InternalComposedLts.g:1736:1: ruleHmlTrue
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlTrueParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlTrue();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlTrueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1741:6: ( ruleHmlFalse )
                    {
                    // InternalComposedLts.g:1741:6: ( ruleHmlFalse )
                    // InternalComposedLts.g:1742:1: ruleHmlFalse
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlFalseParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlFalse();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlFalseParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:1747:6: ( ( rule__HmlBaseFormula__Group_2__0 ) )
                    {
                    // InternalComposedLts.g:1747:6: ( ( rule__HmlBaseFormula__Group_2__0 ) )
                    // InternalComposedLts.g:1748:1: ( rule__HmlBaseFormula__Group_2__0 )
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getGroup_2()); 
                    // InternalComposedLts.g:1749:1: ( rule__HmlBaseFormula__Group_2__0 )
                    // InternalComposedLts.g:1749:2: rule__HmlBaseFormula__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__HmlBaseFormula__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getHmlBaseFormulaAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalComposedLts.g:1753:6: ( ruleHmlNotFormula )
                    {
                    // InternalComposedLts.g:1753:6: ( ruleHmlNotFormula )
                    // InternalComposedLts.g:1754:1: ruleHmlNotFormula
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlNotFormulaParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlNotFormula();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlNotFormulaParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalComposedLts.g:1759:6: ( ruleHmlDiamondFormula )
                    {
                    // InternalComposedLts.g:1759:6: ( ruleHmlDiamondFormula )
                    // InternalComposedLts.g:1760:1: ruleHmlDiamondFormula
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlDiamondFormulaParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlDiamondFormula();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlDiamondFormulaParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalComposedLts.g:1765:6: ( ruleHmlBoxFormula )
                    {
                    // InternalComposedLts.g:1765:6: ( ruleHmlBoxFormula )
                    // InternalComposedLts.g:1766:1: ruleHmlBoxFormula
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlBoxFormulaParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlBoxFormula();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlBoxFormulaParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalComposedLts.g:1771:6: ( ruleHmlRecursionFormula )
                    {
                    // InternalComposedLts.g:1771:6: ( ruleHmlRecursionFormula )
                    // InternalComposedLts.g:1772:1: ruleHmlRecursionFormula
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlRecursionFormulaParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlRecursionFormula();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlRecursionFormulaParserRuleCall_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalComposedLts.g:1777:6: ( ruleHmlRecursionVariable )
                    {
                    // InternalComposedLts.g:1777:6: ( ruleHmlRecursionVariable )
                    // InternalComposedLts.g:1778:1: ruleHmlRecursionVariable
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlRecursionVariableParserRuleCall_7()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlRecursionVariable();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlRecursionVariableParserRuleCall_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalComposedLts.g:1783:6: ( ruleHmlPredicate )
                    {
                    // InternalComposedLts.g:1783:6: ( ruleHmlPredicate )
                    // InternalComposedLts.g:1784:1: ruleHmlPredicate
                    {
                     before(grammarAccess.getHmlBaseFormulaAccess().getHmlPredicateParserRuleCall_8()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlPredicate();

                    state._fsp--;

                     after(grammarAccess.getHmlBaseFormulaAccess().getHmlPredicateParserRuleCall_8()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Alternatives"


    // $ANTLR start "rule__HmlRecursionFormula__Alternatives"
    // InternalComposedLts.g:1794:1: rule__HmlRecursionFormula__Alternatives : ( ( ruleHmlMinFixPoint ) | ( ruleHmlMaxFixPoint ) );
    public final void rule__HmlRecursionFormula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1798:1: ( ( ruleHmlMinFixPoint ) | ( ruleHmlMaxFixPoint ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==45) ) {
                alt6=1;
            }
            else if ( (LA6_0==47) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalComposedLts.g:1799:1: ( ruleHmlMinFixPoint )
                    {
                    // InternalComposedLts.g:1799:1: ( ruleHmlMinFixPoint )
                    // InternalComposedLts.g:1800:1: ruleHmlMinFixPoint
                    {
                     before(grammarAccess.getHmlRecursionFormulaAccess().getHmlMinFixPointParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlMinFixPoint();

                    state._fsp--;

                     after(grammarAccess.getHmlRecursionFormulaAccess().getHmlMinFixPointParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1805:6: ( ruleHmlMaxFixPoint )
                    {
                    // InternalComposedLts.g:1805:6: ( ruleHmlMaxFixPoint )
                    // InternalComposedLts.g:1806:1: ruleHmlMaxFixPoint
                    {
                     before(grammarAccess.getHmlRecursionFormulaAccess().getHmlMaxFixPointParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleHmlMaxFixPoint();

                    state._fsp--;

                     after(grammarAccess.getHmlRecursionFormulaAccess().getHmlMaxFixPointParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlRecursionFormula__Alternatives"


    // $ANTLR start "rule__LtlBaseFormula__Alternatives"
    // InternalComposedLts.g:1816:1: rule__LtlBaseFormula__Alternatives : ( ( ruleLtlTrue ) | ( ruleLtlFalse ) | ( ( rule__LtlBaseFormula__Group_2__0 ) ) | ( ruleLtlNotFormula ) | ( ruleLtlNextFormula ) | ( ruleLtlAlwaysFormula ) | ( ruleLtlEventuallyFormula ) | ( ruleLtlAtomicProposition ) );
    public final void rule__LtlBaseFormula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1820:1: ( ( ruleLtlTrue ) | ( ruleLtlFalse ) | ( ( rule__LtlBaseFormula__Group_2__0 ) ) | ( ruleLtlNotFormula ) | ( ruleLtlNextFormula ) | ( ruleLtlAlwaysFormula ) | ( ruleLtlEventuallyFormula ) | ( ruleLtlAtomicProposition ) )
            int alt7=8;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt7=1;
                }
                break;
            case 40:
                {
                alt7=2;
                }
                break;
            case 21:
                {
                alt7=3;
                }
                break;
            case 41:
                {
                alt7=4;
                }
                break;
            case 50:
                {
                alt7=5;
                }
                break;
            case 51:
                {
                alt7=6;
                }
                break;
            case 52:
                {
                alt7=7;
                }
                break;
            case RULE_ID:
                {
                alt7=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalComposedLts.g:1821:1: ( ruleLtlTrue )
                    {
                    // InternalComposedLts.g:1821:1: ( ruleLtlTrue )
                    // InternalComposedLts.g:1822:1: ruleLtlTrue
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlTrueParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlTrue();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlTrueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1827:6: ( ruleLtlFalse )
                    {
                    // InternalComposedLts.g:1827:6: ( ruleLtlFalse )
                    // InternalComposedLts.g:1828:1: ruleLtlFalse
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlFalseParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlFalse();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlFalseParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:1833:6: ( ( rule__LtlBaseFormula__Group_2__0 ) )
                    {
                    // InternalComposedLts.g:1833:6: ( ( rule__LtlBaseFormula__Group_2__0 ) )
                    // InternalComposedLts.g:1834:1: ( rule__LtlBaseFormula__Group_2__0 )
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getGroup_2()); 
                    // InternalComposedLts.g:1835:1: ( rule__LtlBaseFormula__Group_2__0 )
                    // InternalComposedLts.g:1835:2: rule__LtlBaseFormula__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LtlBaseFormula__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLtlBaseFormulaAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalComposedLts.g:1839:6: ( ruleLtlNotFormula )
                    {
                    // InternalComposedLts.g:1839:6: ( ruleLtlNotFormula )
                    // InternalComposedLts.g:1840:1: ruleLtlNotFormula
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlNotFormulaParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlNotFormula();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlNotFormulaParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalComposedLts.g:1845:6: ( ruleLtlNextFormula )
                    {
                    // InternalComposedLts.g:1845:6: ( ruleLtlNextFormula )
                    // InternalComposedLts.g:1846:1: ruleLtlNextFormula
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlNextFormulaParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlNextFormula();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlNextFormulaParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalComposedLts.g:1851:6: ( ruleLtlAlwaysFormula )
                    {
                    // InternalComposedLts.g:1851:6: ( ruleLtlAlwaysFormula )
                    // InternalComposedLts.g:1852:1: ruleLtlAlwaysFormula
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlAlwaysFormulaParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlAlwaysFormula();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlAlwaysFormulaParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalComposedLts.g:1857:6: ( ruleLtlEventuallyFormula )
                    {
                    // InternalComposedLts.g:1857:6: ( ruleLtlEventuallyFormula )
                    // InternalComposedLts.g:1858:1: ruleLtlEventuallyFormula
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlEventuallyFormulaParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlEventuallyFormula();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlEventuallyFormulaParserRuleCall_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalComposedLts.g:1863:6: ( ruleLtlAtomicProposition )
                    {
                    // InternalComposedLts.g:1863:6: ( ruleLtlAtomicProposition )
                    // InternalComposedLts.g:1864:1: ruleLtlAtomicProposition
                    {
                     before(grammarAccess.getLtlBaseFormulaAccess().getLtlAtomicPropositionParserRuleCall_7()); 
                    pushFollow(FOLLOW_2);
                    ruleLtlAtomicProposition();

                    state._fsp--;

                     after(grammarAccess.getLtlBaseFormulaAccess().getLtlAtomicPropositionParserRuleCall_7()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Alternatives"


    // $ANTLR start "rule__BaseLabelPredicate__Alternatives"
    // InternalComposedLts.g:1874:1: rule__BaseLabelPredicate__Alternatives : ( ( ruleAnyLabelPredicate ) | ( ruleActionLabelPredicate ) | ( ruleNotLabelPredicate ) );
    public final void rule__BaseLabelPredicate__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1878:1: ( ( ruleAnyLabelPredicate ) | ( ruleActionLabelPredicate ) | ( ruleNotLabelPredicate ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 53:
                {
                alt8=1;
                }
                break;
            case RULE_ID:
                {
                alt8=2;
                }
                break;
            case 41:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalComposedLts.g:1879:1: ( ruleAnyLabelPredicate )
                    {
                    // InternalComposedLts.g:1879:1: ( ruleAnyLabelPredicate )
                    // InternalComposedLts.g:1880:1: ruleAnyLabelPredicate
                    {
                     before(grammarAccess.getBaseLabelPredicateAccess().getAnyLabelPredicateParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAnyLabelPredicate();

                    state._fsp--;

                     after(grammarAccess.getBaseLabelPredicateAccess().getAnyLabelPredicateParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1885:6: ( ruleActionLabelPredicate )
                    {
                    // InternalComposedLts.g:1885:6: ( ruleActionLabelPredicate )
                    // InternalComposedLts.g:1886:1: ruleActionLabelPredicate
                    {
                     before(grammarAccess.getBaseLabelPredicateAccess().getActionLabelPredicateParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleActionLabelPredicate();

                    state._fsp--;

                     after(grammarAccess.getBaseLabelPredicateAccess().getActionLabelPredicateParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:1891:6: ( ruleNotLabelPredicate )
                    {
                    // InternalComposedLts.g:1891:6: ( ruleNotLabelPredicate )
                    // InternalComposedLts.g:1892:1: ruleNotLabelPredicate
                    {
                     before(grammarAccess.getBaseLabelPredicateAccess().getNotLabelPredicateParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNotLabelPredicate();

                    state._fsp--;

                     after(grammarAccess.getBaseLabelPredicateAccess().getNotLabelPredicateParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLabelPredicate__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // InternalComposedLts.g:1904:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1908:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalComposedLts.g:1909:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalComposedLts.g:1916:1: rule__Model__Group__0__Impl : ( () ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1920:1: ( ( () ) )
            // InternalComposedLts.g:1921:1: ( () )
            {
            // InternalComposedLts.g:1921:1: ( () )
            // InternalComposedLts.g:1922:1: ()
            {
             before(grammarAccess.getModelAccess().getModelAction_0()); 
            // InternalComposedLts.g:1923:1: ()
            // InternalComposedLts.g:1925:1: 
            {
            }

             after(grammarAccess.getModelAccess().getModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalComposedLts.g:1935:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1939:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // InternalComposedLts.g:1940:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalComposedLts.g:1947:1: rule__Model__Group__1__Impl : ( 'model' ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1951:1: ( ( 'model' ) )
            // InternalComposedLts.g:1952:1: ( 'model' )
            {
            // InternalComposedLts.g:1952:1: ( 'model' )
            // InternalComposedLts.g:1953:1: 'model'
            {
             before(grammarAccess.getModelAccess().getModelKeyword_1()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getModelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // InternalComposedLts.g:1966:1: rule__Model__Group__2 : rule__Model__Group__2__Impl rule__Model__Group__3 ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1970:1: ( rule__Model__Group__2__Impl rule__Model__Group__3 )
            // InternalComposedLts.g:1971:2: rule__Model__Group__2__Impl rule__Model__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Model__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // InternalComposedLts.g:1978:1: rule__Model__Group__2__Impl : ( ( rule__Model__NameAssignment_2 ) ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1982:1: ( ( ( rule__Model__NameAssignment_2 ) ) )
            // InternalComposedLts.g:1983:1: ( ( rule__Model__NameAssignment_2 ) )
            {
            // InternalComposedLts.g:1983:1: ( ( rule__Model__NameAssignment_2 ) )
            // InternalComposedLts.g:1984:1: ( rule__Model__NameAssignment_2 )
            {
             before(grammarAccess.getModelAccess().getNameAssignment_2()); 
            // InternalComposedLts.g:1985:1: ( rule__Model__NameAssignment_2 )
            // InternalComposedLts.g:1985:2: rule__Model__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Model__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Model__Group__3"
    // InternalComposedLts.g:1995:1: rule__Model__Group__3 : rule__Model__Group__3__Impl rule__Model__Group__4 ;
    public final void rule__Model__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:1999:1: ( rule__Model__Group__3__Impl rule__Model__Group__4 )
            // InternalComposedLts.g:2000:2: rule__Model__Group__3__Impl rule__Model__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Model__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3"


    // $ANTLR start "rule__Model__Group__3__Impl"
    // InternalComposedLts.g:2007:1: rule__Model__Group__3__Impl : ( ';' ) ;
    public final void rule__Model__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2011:1: ( ( ';' ) )
            // InternalComposedLts.g:2012:1: ( ';' )
            {
            // InternalComposedLts.g:2012:1: ( ';' )
            // InternalComposedLts.g:2013:1: ';'
            {
             before(grammarAccess.getModelAccess().getSemicolonKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3__Impl"


    // $ANTLR start "rule__Model__Group__4"
    // InternalComposedLts.g:2026:1: rule__Model__Group__4 : rule__Model__Group__4__Impl ;
    public final void rule__Model__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2030:1: ( rule__Model__Group__4__Impl )
            // InternalComposedLts.g:2031:2: rule__Model__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4"


    // $ANTLR start "rule__Model__Group__4__Impl"
    // InternalComposedLts.g:2037:1: rule__Model__Group__4__Impl : ( ( rule__Model__ElementsAssignment_4 )* ) ;
    public final void rule__Model__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2041:1: ( ( ( rule__Model__ElementsAssignment_4 )* ) )
            // InternalComposedLts.g:2042:1: ( ( rule__Model__ElementsAssignment_4 )* )
            {
            // InternalComposedLts.g:2042:1: ( ( rule__Model__ElementsAssignment_4 )* )
            // InternalComposedLts.g:2043:1: ( rule__Model__ElementsAssignment_4 )*
            {
             before(grammarAccess.getModelAccess().getElementsAssignment_4()); 
            // InternalComposedLts.g:2044:1: ( rule__Model__ElementsAssignment_4 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==13||(LA9_0>=32 && LA9_0<=34)||LA9_0==48) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalComposedLts.g:2044:2: rule__Model__ElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Model__ElementsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getElementsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__4__Impl"


    // $ANTLR start "rule__Lts__Group__0"
    // InternalComposedLts.g:2064:1: rule__Lts__Group__0 : rule__Lts__Group__0__Impl rule__Lts__Group__1 ;
    public final void rule__Lts__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2068:1: ( rule__Lts__Group__0__Impl rule__Lts__Group__1 )
            // InternalComposedLts.g:2069:2: rule__Lts__Group__0__Impl rule__Lts__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Lts__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lts__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__Group__0"


    // $ANTLR start "rule__Lts__Group__0__Impl"
    // InternalComposedLts.g:2076:1: rule__Lts__Group__0__Impl : ( 'lts' ) ;
    public final void rule__Lts__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2080:1: ( ( 'lts' ) )
            // InternalComposedLts.g:2081:1: ( 'lts' )
            {
            // InternalComposedLts.g:2081:1: ( 'lts' )
            // InternalComposedLts.g:2082:1: 'lts'
            {
             before(grammarAccess.getLtsAccess().getLtsKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getLtsAccess().getLtsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__Group__0__Impl"


    // $ANTLR start "rule__Lts__Group__1"
    // InternalComposedLts.g:2095:1: rule__Lts__Group__1 : rule__Lts__Group__1__Impl rule__Lts__Group__2 ;
    public final void rule__Lts__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2099:1: ( rule__Lts__Group__1__Impl rule__Lts__Group__2 )
            // InternalComposedLts.g:2100:2: rule__Lts__Group__1__Impl rule__Lts__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Lts__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Lts__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__Group__1"


    // $ANTLR start "rule__Lts__Group__1__Impl"
    // InternalComposedLts.g:2107:1: rule__Lts__Group__1__Impl : ( ( rule__Lts__NameAssignment_1 ) ) ;
    public final void rule__Lts__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2111:1: ( ( ( rule__Lts__NameAssignment_1 ) ) )
            // InternalComposedLts.g:2112:1: ( ( rule__Lts__NameAssignment_1 ) )
            {
            // InternalComposedLts.g:2112:1: ( ( rule__Lts__NameAssignment_1 ) )
            // InternalComposedLts.g:2113:1: ( rule__Lts__NameAssignment_1 )
            {
             before(grammarAccess.getLtsAccess().getNameAssignment_1()); 
            // InternalComposedLts.g:2114:1: ( rule__Lts__NameAssignment_1 )
            // InternalComposedLts.g:2114:2: rule__Lts__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Lts__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLtsAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__Group__1__Impl"


    // $ANTLR start "rule__Lts__Group__2"
    // InternalComposedLts.g:2124:1: rule__Lts__Group__2 : rule__Lts__Group__2__Impl ;
    public final void rule__Lts__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2128:1: ( rule__Lts__Group__2__Impl )
            // InternalComposedLts.g:2129:2: rule__Lts__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Lts__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__Group__2"


    // $ANTLR start "rule__Lts__Group__2__Impl"
    // InternalComposedLts.g:2135:1: rule__Lts__Group__2__Impl : ( ( rule__Lts__BodyAssignment_2 ) ) ;
    public final void rule__Lts__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2139:1: ( ( ( rule__Lts__BodyAssignment_2 ) ) )
            // InternalComposedLts.g:2140:1: ( ( rule__Lts__BodyAssignment_2 ) )
            {
            // InternalComposedLts.g:2140:1: ( ( rule__Lts__BodyAssignment_2 ) )
            // InternalComposedLts.g:2141:1: ( rule__Lts__BodyAssignment_2 )
            {
             before(grammarAccess.getLtsAccess().getBodyAssignment_2()); 
            // InternalComposedLts.g:2142:1: ( rule__Lts__BodyAssignment_2 )
            // InternalComposedLts.g:2142:2: rule__Lts__BodyAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Lts__BodyAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLtsAccess().getBodyAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__Group__2__Impl"


    // $ANTLR start "rule__LtsComposition__Group__0"
    // InternalComposedLts.g:2158:1: rule__LtsComposition__Group__0 : rule__LtsComposition__Group__0__Impl rule__LtsComposition__Group__1 ;
    public final void rule__LtsComposition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2162:1: ( rule__LtsComposition__Group__0__Impl rule__LtsComposition__Group__1 )
            // InternalComposedLts.g:2163:2: rule__LtsComposition__Group__0__Impl rule__LtsComposition__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__LtsComposition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsComposition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__Group__0"


    // $ANTLR start "rule__LtsComposition__Group__0__Impl"
    // InternalComposedLts.g:2170:1: rule__LtsComposition__Group__0__Impl : ( '=' ) ;
    public final void rule__LtsComposition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2174:1: ( ( '=' ) )
            // InternalComposedLts.g:2175:1: ( '=' )
            {
            // InternalComposedLts.g:2175:1: ( '=' )
            // InternalComposedLts.g:2176:1: '='
            {
             before(grammarAccess.getLtsCompositionAccess().getEqualsSignKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLtsCompositionAccess().getEqualsSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__Group__0__Impl"


    // $ANTLR start "rule__LtsComposition__Group__1"
    // InternalComposedLts.g:2189:1: rule__LtsComposition__Group__1 : rule__LtsComposition__Group__1__Impl rule__LtsComposition__Group__2 ;
    public final void rule__LtsComposition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2193:1: ( rule__LtsComposition__Group__1__Impl rule__LtsComposition__Group__2 )
            // InternalComposedLts.g:2194:2: rule__LtsComposition__Group__1__Impl rule__LtsComposition__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__LtsComposition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsComposition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__Group__1"


    // $ANTLR start "rule__LtsComposition__Group__1__Impl"
    // InternalComposedLts.g:2201:1: rule__LtsComposition__Group__1__Impl : ( ( rule__LtsComposition__BodyAssignment_1 ) ) ;
    public final void rule__LtsComposition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2205:1: ( ( ( rule__LtsComposition__BodyAssignment_1 ) ) )
            // InternalComposedLts.g:2206:1: ( ( rule__LtsComposition__BodyAssignment_1 ) )
            {
            // InternalComposedLts.g:2206:1: ( ( rule__LtsComposition__BodyAssignment_1 ) )
            // InternalComposedLts.g:2207:1: ( rule__LtsComposition__BodyAssignment_1 )
            {
             before(grammarAccess.getLtsCompositionAccess().getBodyAssignment_1()); 
            // InternalComposedLts.g:2208:1: ( rule__LtsComposition__BodyAssignment_1 )
            // InternalComposedLts.g:2208:2: rule__LtsComposition__BodyAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__LtsComposition__BodyAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLtsCompositionAccess().getBodyAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__Group__1__Impl"


    // $ANTLR start "rule__LtsComposition__Group__2"
    // InternalComposedLts.g:2218:1: rule__LtsComposition__Group__2 : rule__LtsComposition__Group__2__Impl ;
    public final void rule__LtsComposition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2222:1: ( rule__LtsComposition__Group__2__Impl )
            // InternalComposedLts.g:2223:2: rule__LtsComposition__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsComposition__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__Group__2"


    // $ANTLR start "rule__LtsComposition__Group__2__Impl"
    // InternalComposedLts.g:2229:1: rule__LtsComposition__Group__2__Impl : ( ';' ) ;
    public final void rule__LtsComposition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2233:1: ( ( ';' ) )
            // InternalComposedLts.g:2234:1: ( ';' )
            {
            // InternalComposedLts.g:2234:1: ( ';' )
            // InternalComposedLts.g:2235:1: ';'
            {
             before(grammarAccess.getLtsCompositionAccess().getSemicolonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLtsCompositionAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__Group__2__Impl"


    // $ANTLR start "rule__Interleaving__Group__0"
    // InternalComposedLts.g:2254:1: rule__Interleaving__Group__0 : rule__Interleaving__Group__0__Impl rule__Interleaving__Group__1 ;
    public final void rule__Interleaving__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2258:1: ( rule__Interleaving__Group__0__Impl rule__Interleaving__Group__1 )
            // InternalComposedLts.g:2259:2: rule__Interleaving__Group__0__Impl rule__Interleaving__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Interleaving__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interleaving__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group__0"


    // $ANTLR start "rule__Interleaving__Group__0__Impl"
    // InternalComposedLts.g:2266:1: rule__Interleaving__Group__0__Impl : ( ruleSynchronization ) ;
    public final void rule__Interleaving__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2270:1: ( ( ruleSynchronization ) )
            // InternalComposedLts.g:2271:1: ( ruleSynchronization )
            {
            // InternalComposedLts.g:2271:1: ( ruleSynchronization )
            // InternalComposedLts.g:2272:1: ruleSynchronization
            {
             before(grammarAccess.getInterleavingAccess().getSynchronizationParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleSynchronization();

            state._fsp--;

             after(grammarAccess.getInterleavingAccess().getSynchronizationParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group__0__Impl"


    // $ANTLR start "rule__Interleaving__Group__1"
    // InternalComposedLts.g:2283:1: rule__Interleaving__Group__1 : rule__Interleaving__Group__1__Impl ;
    public final void rule__Interleaving__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2287:1: ( rule__Interleaving__Group__1__Impl )
            // InternalComposedLts.g:2288:2: rule__Interleaving__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interleaving__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group__1"


    // $ANTLR start "rule__Interleaving__Group__1__Impl"
    // InternalComposedLts.g:2294:1: rule__Interleaving__Group__1__Impl : ( ( rule__Interleaving__Group_1__0 )? ) ;
    public final void rule__Interleaving__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2298:1: ( ( ( rule__Interleaving__Group_1__0 )? ) )
            // InternalComposedLts.g:2299:1: ( ( rule__Interleaving__Group_1__0 )? )
            {
            // InternalComposedLts.g:2299:1: ( ( rule__Interleaving__Group_1__0 )? )
            // InternalComposedLts.g:2300:1: ( rule__Interleaving__Group_1__0 )?
            {
             before(grammarAccess.getInterleavingAccess().getGroup_1()); 
            // InternalComposedLts.g:2301:1: ( rule__Interleaving__Group_1__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==15) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalComposedLts.g:2301:2: rule__Interleaving__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Interleaving__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInterleavingAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group__1__Impl"


    // $ANTLR start "rule__Interleaving__Group_1__0"
    // InternalComposedLts.g:2315:1: rule__Interleaving__Group_1__0 : rule__Interleaving__Group_1__0__Impl rule__Interleaving__Group_1__1 ;
    public final void rule__Interleaving__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2319:1: ( rule__Interleaving__Group_1__0__Impl rule__Interleaving__Group_1__1 )
            // InternalComposedLts.g:2320:2: rule__Interleaving__Group_1__0__Impl rule__Interleaving__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Interleaving__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interleaving__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group_1__0"


    // $ANTLR start "rule__Interleaving__Group_1__0__Impl"
    // InternalComposedLts.g:2327:1: rule__Interleaving__Group_1__0__Impl : ( () ) ;
    public final void rule__Interleaving__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2331:1: ( ( () ) )
            // InternalComposedLts.g:2332:1: ( () )
            {
            // InternalComposedLts.g:2332:1: ( () )
            // InternalComposedLts.g:2333:1: ()
            {
             before(grammarAccess.getInterleavingAccess().getInterleavingLeftAction_1_0()); 
            // InternalComposedLts.g:2334:1: ()
            // InternalComposedLts.g:2336:1: 
            {
            }

             after(grammarAccess.getInterleavingAccess().getInterleavingLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group_1__0__Impl"


    // $ANTLR start "rule__Interleaving__Group_1__1"
    // InternalComposedLts.g:2346:1: rule__Interleaving__Group_1__1 : rule__Interleaving__Group_1__1__Impl rule__Interleaving__Group_1__2 ;
    public final void rule__Interleaving__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2350:1: ( rule__Interleaving__Group_1__1__Impl rule__Interleaving__Group_1__2 )
            // InternalComposedLts.g:2351:2: rule__Interleaving__Group_1__1__Impl rule__Interleaving__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Interleaving__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interleaving__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group_1__1"


    // $ANTLR start "rule__Interleaving__Group_1__1__Impl"
    // InternalComposedLts.g:2358:1: rule__Interleaving__Group_1__1__Impl : ( '||' ) ;
    public final void rule__Interleaving__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2362:1: ( ( '||' ) )
            // InternalComposedLts.g:2363:1: ( '||' )
            {
            // InternalComposedLts.g:2363:1: ( '||' )
            // InternalComposedLts.g:2364:1: '||'
            {
             before(grammarAccess.getInterleavingAccess().getVerticalLineVerticalLineKeyword_1_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getInterleavingAccess().getVerticalLineVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group_1__1__Impl"


    // $ANTLR start "rule__Interleaving__Group_1__2"
    // InternalComposedLts.g:2377:1: rule__Interleaving__Group_1__2 : rule__Interleaving__Group_1__2__Impl ;
    public final void rule__Interleaving__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2381:1: ( rule__Interleaving__Group_1__2__Impl )
            // InternalComposedLts.g:2382:2: rule__Interleaving__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interleaving__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group_1__2"


    // $ANTLR start "rule__Interleaving__Group_1__2__Impl"
    // InternalComposedLts.g:2388:1: rule__Interleaving__Group_1__2__Impl : ( ( rule__Interleaving__RightAssignment_1_2 ) ) ;
    public final void rule__Interleaving__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2392:1: ( ( ( rule__Interleaving__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:2393:1: ( ( rule__Interleaving__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:2393:1: ( ( rule__Interleaving__RightAssignment_1_2 ) )
            // InternalComposedLts.g:2394:1: ( rule__Interleaving__RightAssignment_1_2 )
            {
             before(grammarAccess.getInterleavingAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:2395:1: ( rule__Interleaving__RightAssignment_1_2 )
            // InternalComposedLts.g:2395:2: rule__Interleaving__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Interleaving__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getInterleavingAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__Group_1__2__Impl"


    // $ANTLR start "rule__Synchronization__Group__0"
    // InternalComposedLts.g:2411:1: rule__Synchronization__Group__0 : rule__Synchronization__Group__0__Impl rule__Synchronization__Group__1 ;
    public final void rule__Synchronization__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2415:1: ( rule__Synchronization__Group__0__Impl rule__Synchronization__Group__1 )
            // InternalComposedLts.g:2416:2: rule__Synchronization__Group__0__Impl rule__Synchronization__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Synchronization__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Synchronization__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group__0"


    // $ANTLR start "rule__Synchronization__Group__0__Impl"
    // InternalComposedLts.g:2423:1: rule__Synchronization__Group__0__Impl : ( ruleControlledInteraction ) ;
    public final void rule__Synchronization__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2427:1: ( ( ruleControlledInteraction ) )
            // InternalComposedLts.g:2428:1: ( ruleControlledInteraction )
            {
            // InternalComposedLts.g:2428:1: ( ruleControlledInteraction )
            // InternalComposedLts.g:2429:1: ruleControlledInteraction
            {
             before(grammarAccess.getSynchronizationAccess().getControlledInteractionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleControlledInteraction();

            state._fsp--;

             after(grammarAccess.getSynchronizationAccess().getControlledInteractionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group__0__Impl"


    // $ANTLR start "rule__Synchronization__Group__1"
    // InternalComposedLts.g:2440:1: rule__Synchronization__Group__1 : rule__Synchronization__Group__1__Impl ;
    public final void rule__Synchronization__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2444:1: ( rule__Synchronization__Group__1__Impl )
            // InternalComposedLts.g:2445:2: rule__Synchronization__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Synchronization__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group__1"


    // $ANTLR start "rule__Synchronization__Group__1__Impl"
    // InternalComposedLts.g:2451:1: rule__Synchronization__Group__1__Impl : ( ( rule__Synchronization__Group_1__0 )? ) ;
    public final void rule__Synchronization__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2455:1: ( ( ( rule__Synchronization__Group_1__0 )? ) )
            // InternalComposedLts.g:2456:1: ( ( rule__Synchronization__Group_1__0 )? )
            {
            // InternalComposedLts.g:2456:1: ( ( rule__Synchronization__Group_1__0 )? )
            // InternalComposedLts.g:2457:1: ( rule__Synchronization__Group_1__0 )?
            {
             before(grammarAccess.getSynchronizationAccess().getGroup_1()); 
            // InternalComposedLts.g:2458:1: ( rule__Synchronization__Group_1__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==16) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalComposedLts.g:2458:2: rule__Synchronization__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Synchronization__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSynchronizationAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group__1__Impl"


    // $ANTLR start "rule__Synchronization__Group_1__0"
    // InternalComposedLts.g:2472:1: rule__Synchronization__Group_1__0 : rule__Synchronization__Group_1__0__Impl rule__Synchronization__Group_1__1 ;
    public final void rule__Synchronization__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2476:1: ( rule__Synchronization__Group_1__0__Impl rule__Synchronization__Group_1__1 )
            // InternalComposedLts.g:2477:2: rule__Synchronization__Group_1__0__Impl rule__Synchronization__Group_1__1
            {
            pushFollow(FOLLOW_11);
            rule__Synchronization__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Synchronization__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group_1__0"


    // $ANTLR start "rule__Synchronization__Group_1__0__Impl"
    // InternalComposedLts.g:2484:1: rule__Synchronization__Group_1__0__Impl : ( () ) ;
    public final void rule__Synchronization__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2488:1: ( ( () ) )
            // InternalComposedLts.g:2489:1: ( () )
            {
            // InternalComposedLts.g:2489:1: ( () )
            // InternalComposedLts.g:2490:1: ()
            {
             before(grammarAccess.getSynchronizationAccess().getSynchronizationLeftAction_1_0()); 
            // InternalComposedLts.g:2491:1: ()
            // InternalComposedLts.g:2493:1: 
            {
            }

             after(grammarAccess.getSynchronizationAccess().getSynchronizationLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group_1__0__Impl"


    // $ANTLR start "rule__Synchronization__Group_1__1"
    // InternalComposedLts.g:2503:1: rule__Synchronization__Group_1__1 : rule__Synchronization__Group_1__1__Impl rule__Synchronization__Group_1__2 ;
    public final void rule__Synchronization__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2507:1: ( rule__Synchronization__Group_1__1__Impl rule__Synchronization__Group_1__2 )
            // InternalComposedLts.g:2508:2: rule__Synchronization__Group_1__1__Impl rule__Synchronization__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Synchronization__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Synchronization__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group_1__1"


    // $ANTLR start "rule__Synchronization__Group_1__1__Impl"
    // InternalComposedLts.g:2515:1: rule__Synchronization__Group_1__1__Impl : ( '|*|' ) ;
    public final void rule__Synchronization__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2519:1: ( ( '|*|' ) )
            // InternalComposedLts.g:2520:1: ( '|*|' )
            {
            // InternalComposedLts.g:2520:1: ( '|*|' )
            // InternalComposedLts.g:2521:1: '|*|'
            {
             before(grammarAccess.getSynchronizationAccess().getVerticalLineAsteriskVerticalLineKeyword_1_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getSynchronizationAccess().getVerticalLineAsteriskVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group_1__1__Impl"


    // $ANTLR start "rule__Synchronization__Group_1__2"
    // InternalComposedLts.g:2534:1: rule__Synchronization__Group_1__2 : rule__Synchronization__Group_1__2__Impl ;
    public final void rule__Synchronization__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2538:1: ( rule__Synchronization__Group_1__2__Impl )
            // InternalComposedLts.g:2539:2: rule__Synchronization__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Synchronization__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group_1__2"


    // $ANTLR start "rule__Synchronization__Group_1__2__Impl"
    // InternalComposedLts.g:2545:1: rule__Synchronization__Group_1__2__Impl : ( ( rule__Synchronization__RightAssignment_1_2 ) ) ;
    public final void rule__Synchronization__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2549:1: ( ( ( rule__Synchronization__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:2550:1: ( ( rule__Synchronization__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:2550:1: ( ( rule__Synchronization__RightAssignment_1_2 ) )
            // InternalComposedLts.g:2551:1: ( rule__Synchronization__RightAssignment_1_2 )
            {
             before(grammarAccess.getSynchronizationAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:2552:1: ( rule__Synchronization__RightAssignment_1_2 )
            // InternalComposedLts.g:2552:2: rule__Synchronization__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Synchronization__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getSynchronizationAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__Group_1__2__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group__0"
    // InternalComposedLts.g:2568:1: rule__ControlledInteraction__Group__0 : rule__ControlledInteraction__Group__0__Impl rule__ControlledInteraction__Group__1 ;
    public final void rule__ControlledInteraction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2572:1: ( rule__ControlledInteraction__Group__0__Impl rule__ControlledInteraction__Group__1 )
            // InternalComposedLts.g:2573:2: rule__ControlledInteraction__Group__0__Impl rule__ControlledInteraction__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__ControlledInteraction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group__0"


    // $ANTLR start "rule__ControlledInteraction__Group__0__Impl"
    // InternalComposedLts.g:2580:1: rule__ControlledInteraction__Group__0__Impl : ( ruleRenaming ) ;
    public final void rule__ControlledInteraction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2584:1: ( ( ruleRenaming ) )
            // InternalComposedLts.g:2585:1: ( ruleRenaming )
            {
            // InternalComposedLts.g:2585:1: ( ruleRenaming )
            // InternalComposedLts.g:2586:1: ruleRenaming
            {
             before(grammarAccess.getControlledInteractionAccess().getRenamingParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleRenaming();

            state._fsp--;

             after(grammarAccess.getControlledInteractionAccess().getRenamingParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group__0__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group__1"
    // InternalComposedLts.g:2597:1: rule__ControlledInteraction__Group__1 : rule__ControlledInteraction__Group__1__Impl ;
    public final void rule__ControlledInteraction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2601:1: ( rule__ControlledInteraction__Group__1__Impl )
            // InternalComposedLts.g:2602:2: rule__ControlledInteraction__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group__1"


    // $ANTLR start "rule__ControlledInteraction__Group__1__Impl"
    // InternalComposedLts.g:2608:1: rule__ControlledInteraction__Group__1__Impl : ( ( rule__ControlledInteraction__Group_1__0 )? ) ;
    public final void rule__ControlledInteraction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2612:1: ( ( ( rule__ControlledInteraction__Group_1__0 )? ) )
            // InternalComposedLts.g:2613:1: ( ( rule__ControlledInteraction__Group_1__0 )? )
            {
            // InternalComposedLts.g:2613:1: ( ( rule__ControlledInteraction__Group_1__0 )? )
            // InternalComposedLts.g:2614:1: ( rule__ControlledInteraction__Group_1__0 )?
            {
             before(grammarAccess.getControlledInteractionAccess().getGroup_1()); 
            // InternalComposedLts.g:2615:1: ( rule__ControlledInteraction__Group_1__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==17) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalComposedLts.g:2615:2: rule__ControlledInteraction__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ControlledInteraction__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getControlledInteractionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group__1__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1__0"
    // InternalComposedLts.g:2629:1: rule__ControlledInteraction__Group_1__0 : rule__ControlledInteraction__Group_1__0__Impl rule__ControlledInteraction__Group_1__1 ;
    public final void rule__ControlledInteraction__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2633:1: ( rule__ControlledInteraction__Group_1__0__Impl rule__ControlledInteraction__Group_1__1 )
            // InternalComposedLts.g:2634:2: rule__ControlledInteraction__Group_1__0__Impl rule__ControlledInteraction__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__ControlledInteraction__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__0"


    // $ANTLR start "rule__ControlledInteraction__Group_1__0__Impl"
    // InternalComposedLts.g:2641:1: rule__ControlledInteraction__Group_1__0__Impl : ( () ) ;
    public final void rule__ControlledInteraction__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2645:1: ( ( () ) )
            // InternalComposedLts.g:2646:1: ( () )
            {
            // InternalComposedLts.g:2646:1: ( () )
            // InternalComposedLts.g:2647:1: ()
            {
             before(grammarAccess.getControlledInteractionAccess().getControlledInteractionLeftAction_1_0()); 
            // InternalComposedLts.g:2648:1: ()
            // InternalComposedLts.g:2650:1: 
            {
            }

             after(grammarAccess.getControlledInteractionAccess().getControlledInteractionLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__0__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1__1"
    // InternalComposedLts.g:2660:1: rule__ControlledInteraction__Group_1__1 : rule__ControlledInteraction__Group_1__1__Impl rule__ControlledInteraction__Group_1__2 ;
    public final void rule__ControlledInteraction__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2664:1: ( rule__ControlledInteraction__Group_1__1__Impl rule__ControlledInteraction__Group_1__2 )
            // InternalComposedLts.g:2665:2: rule__ControlledInteraction__Group_1__1__Impl rule__ControlledInteraction__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__ControlledInteraction__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__1"


    // $ANTLR start "rule__ControlledInteraction__Group_1__1__Impl"
    // InternalComposedLts.g:2672:1: rule__ControlledInteraction__Group_1__1__Impl : ( '|' ) ;
    public final void rule__ControlledInteraction__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2676:1: ( ( '|' ) )
            // InternalComposedLts.g:2677:1: ( '|' )
            {
            // InternalComposedLts.g:2677:1: ( '|' )
            // InternalComposedLts.g:2678:1: '|'
            {
             before(grammarAccess.getControlledInteractionAccess().getVerticalLineKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getControlledInteractionAccess().getVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__1__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1__2"
    // InternalComposedLts.g:2691:1: rule__ControlledInteraction__Group_1__2 : rule__ControlledInteraction__Group_1__2__Impl rule__ControlledInteraction__Group_1__3 ;
    public final void rule__ControlledInteraction__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2695:1: ( rule__ControlledInteraction__Group_1__2__Impl rule__ControlledInteraction__Group_1__3 )
            // InternalComposedLts.g:2696:2: rule__ControlledInteraction__Group_1__2__Impl rule__ControlledInteraction__Group_1__3
            {
            pushFollow(FOLLOW_12);
            rule__ControlledInteraction__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__2"


    // $ANTLR start "rule__ControlledInteraction__Group_1__2__Impl"
    // InternalComposedLts.g:2703:1: rule__ControlledInteraction__Group_1__2__Impl : ( ( rule__ControlledInteraction__Group_1_2__0 ) ) ;
    public final void rule__ControlledInteraction__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2707:1: ( ( ( rule__ControlledInteraction__Group_1_2__0 ) ) )
            // InternalComposedLts.g:2708:1: ( ( rule__ControlledInteraction__Group_1_2__0 ) )
            {
            // InternalComposedLts.g:2708:1: ( ( rule__ControlledInteraction__Group_1_2__0 ) )
            // InternalComposedLts.g:2709:1: ( rule__ControlledInteraction__Group_1_2__0 )
            {
             before(grammarAccess.getControlledInteractionAccess().getGroup_1_2()); 
            // InternalComposedLts.g:2710:1: ( rule__ControlledInteraction__Group_1_2__0 )
            // InternalComposedLts.g:2710:2: rule__ControlledInteraction__Group_1_2__0
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1_2__0();

            state._fsp--;


            }

             after(grammarAccess.getControlledInteractionAccess().getGroup_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__2__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1__3"
    // InternalComposedLts.g:2720:1: rule__ControlledInteraction__Group_1__3 : rule__ControlledInteraction__Group_1__3__Impl rule__ControlledInteraction__Group_1__4 ;
    public final void rule__ControlledInteraction__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2724:1: ( rule__ControlledInteraction__Group_1__3__Impl rule__ControlledInteraction__Group_1__4 )
            // InternalComposedLts.g:2725:2: rule__ControlledInteraction__Group_1__3__Impl rule__ControlledInteraction__Group_1__4
            {
            pushFollow(FOLLOW_9);
            rule__ControlledInteraction__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__3"


    // $ANTLR start "rule__ControlledInteraction__Group_1__3__Impl"
    // InternalComposedLts.g:2732:1: rule__ControlledInteraction__Group_1__3__Impl : ( '|' ) ;
    public final void rule__ControlledInteraction__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2736:1: ( ( '|' ) )
            // InternalComposedLts.g:2737:1: ( '|' )
            {
            // InternalComposedLts.g:2737:1: ( '|' )
            // InternalComposedLts.g:2738:1: '|'
            {
             before(grammarAccess.getControlledInteractionAccess().getVerticalLineKeyword_1_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getControlledInteractionAccess().getVerticalLineKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__3__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1__4"
    // InternalComposedLts.g:2751:1: rule__ControlledInteraction__Group_1__4 : rule__ControlledInteraction__Group_1__4__Impl ;
    public final void rule__ControlledInteraction__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2755:1: ( rule__ControlledInteraction__Group_1__4__Impl )
            // InternalComposedLts.g:2756:2: rule__ControlledInteraction__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__4"


    // $ANTLR start "rule__ControlledInteraction__Group_1__4__Impl"
    // InternalComposedLts.g:2762:1: rule__ControlledInteraction__Group_1__4__Impl : ( ( rule__ControlledInteraction__RightAssignment_1_4 ) ) ;
    public final void rule__ControlledInteraction__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2766:1: ( ( ( rule__ControlledInteraction__RightAssignment_1_4 ) ) )
            // InternalComposedLts.g:2767:1: ( ( rule__ControlledInteraction__RightAssignment_1_4 ) )
            {
            // InternalComposedLts.g:2767:1: ( ( rule__ControlledInteraction__RightAssignment_1_4 ) )
            // InternalComposedLts.g:2768:1: ( rule__ControlledInteraction__RightAssignment_1_4 )
            {
             before(grammarAccess.getControlledInteractionAccess().getRightAssignment_1_4()); 
            // InternalComposedLts.g:2769:1: ( rule__ControlledInteraction__RightAssignment_1_4 )
            // InternalComposedLts.g:2769:2: rule__ControlledInteraction__RightAssignment_1_4
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__RightAssignment_1_4();

            state._fsp--;


            }

             after(grammarAccess.getControlledInteractionAccess().getRightAssignment_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1__4__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2__0"
    // InternalComposedLts.g:2789:1: rule__ControlledInteraction__Group_1_2__0 : rule__ControlledInteraction__Group_1_2__0__Impl rule__ControlledInteraction__Group_1_2__1 ;
    public final void rule__ControlledInteraction__Group_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2793:1: ( rule__ControlledInteraction__Group_1_2__0__Impl rule__ControlledInteraction__Group_1_2__1 )
            // InternalComposedLts.g:2794:2: rule__ControlledInteraction__Group_1_2__0__Impl rule__ControlledInteraction__Group_1_2__1
            {
            pushFollow(FOLLOW_13);
            rule__ControlledInteraction__Group_1_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2__0"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2__0__Impl"
    // InternalComposedLts.g:2801:1: rule__ControlledInteraction__Group_1_2__0__Impl : ( ( rule__ControlledInteraction__ActionAssignment_1_2_0 ) ) ;
    public final void rule__ControlledInteraction__Group_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2805:1: ( ( ( rule__ControlledInteraction__ActionAssignment_1_2_0 ) ) )
            // InternalComposedLts.g:2806:1: ( ( rule__ControlledInteraction__ActionAssignment_1_2_0 ) )
            {
            // InternalComposedLts.g:2806:1: ( ( rule__ControlledInteraction__ActionAssignment_1_2_0 ) )
            // InternalComposedLts.g:2807:1: ( rule__ControlledInteraction__ActionAssignment_1_2_0 )
            {
             before(grammarAccess.getControlledInteractionAccess().getActionAssignment_1_2_0()); 
            // InternalComposedLts.g:2808:1: ( rule__ControlledInteraction__ActionAssignment_1_2_0 )
            // InternalComposedLts.g:2808:2: rule__ControlledInteraction__ActionAssignment_1_2_0
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__ActionAssignment_1_2_0();

            state._fsp--;


            }

             after(grammarAccess.getControlledInteractionAccess().getActionAssignment_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2__0__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2__1"
    // InternalComposedLts.g:2818:1: rule__ControlledInteraction__Group_1_2__1 : rule__ControlledInteraction__Group_1_2__1__Impl ;
    public final void rule__ControlledInteraction__Group_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2822:1: ( rule__ControlledInteraction__Group_1_2__1__Impl )
            // InternalComposedLts.g:2823:2: rule__ControlledInteraction__Group_1_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2__1"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2__1__Impl"
    // InternalComposedLts.g:2829:1: rule__ControlledInteraction__Group_1_2__1__Impl : ( ( rule__ControlledInteraction__Group_1_2_1__0 )* ) ;
    public final void rule__ControlledInteraction__Group_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2833:1: ( ( ( rule__ControlledInteraction__Group_1_2_1__0 )* ) )
            // InternalComposedLts.g:2834:1: ( ( rule__ControlledInteraction__Group_1_2_1__0 )* )
            {
            // InternalComposedLts.g:2834:1: ( ( rule__ControlledInteraction__Group_1_2_1__0 )* )
            // InternalComposedLts.g:2835:1: ( rule__ControlledInteraction__Group_1_2_1__0 )*
            {
             before(grammarAccess.getControlledInteractionAccess().getGroup_1_2_1()); 
            // InternalComposedLts.g:2836:1: ( rule__ControlledInteraction__Group_1_2_1__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==18) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalComposedLts.g:2836:2: rule__ControlledInteraction__Group_1_2_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__ControlledInteraction__Group_1_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getControlledInteractionAccess().getGroup_1_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2__1__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2_1__0"
    // InternalComposedLts.g:2850:1: rule__ControlledInteraction__Group_1_2_1__0 : rule__ControlledInteraction__Group_1_2_1__0__Impl rule__ControlledInteraction__Group_1_2_1__1 ;
    public final void rule__ControlledInteraction__Group_1_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2854:1: ( rule__ControlledInteraction__Group_1_2_1__0__Impl rule__ControlledInteraction__Group_1_2_1__1 )
            // InternalComposedLts.g:2855:2: rule__ControlledInteraction__Group_1_2_1__0__Impl rule__ControlledInteraction__Group_1_2_1__1
            {
            pushFollow(FOLLOW_4);
            rule__ControlledInteraction__Group_1_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2_1__0"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2_1__0__Impl"
    // InternalComposedLts.g:2862:1: rule__ControlledInteraction__Group_1_2_1__0__Impl : ( ',' ) ;
    public final void rule__ControlledInteraction__Group_1_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2866:1: ( ( ',' ) )
            // InternalComposedLts.g:2867:1: ( ',' )
            {
            // InternalComposedLts.g:2867:1: ( ',' )
            // InternalComposedLts.g:2868:1: ','
            {
             before(grammarAccess.getControlledInteractionAccess().getCommaKeyword_1_2_1_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getControlledInteractionAccess().getCommaKeyword_1_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2_1__0__Impl"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2_1__1"
    // InternalComposedLts.g:2881:1: rule__ControlledInteraction__Group_1_2_1__1 : rule__ControlledInteraction__Group_1_2_1__1__Impl ;
    public final void rule__ControlledInteraction__Group_1_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2885:1: ( rule__ControlledInteraction__Group_1_2_1__1__Impl )
            // InternalComposedLts.g:2886:2: rule__ControlledInteraction__Group_1_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__Group_1_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2_1__1"


    // $ANTLR start "rule__ControlledInteraction__Group_1_2_1__1__Impl"
    // InternalComposedLts.g:2892:1: rule__ControlledInteraction__Group_1_2_1__1__Impl : ( ( rule__ControlledInteraction__ActionAssignment_1_2_1_1 ) ) ;
    public final void rule__ControlledInteraction__Group_1_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2896:1: ( ( ( rule__ControlledInteraction__ActionAssignment_1_2_1_1 ) ) )
            // InternalComposedLts.g:2897:1: ( ( rule__ControlledInteraction__ActionAssignment_1_2_1_1 ) )
            {
            // InternalComposedLts.g:2897:1: ( ( rule__ControlledInteraction__ActionAssignment_1_2_1_1 ) )
            // InternalComposedLts.g:2898:1: ( rule__ControlledInteraction__ActionAssignment_1_2_1_1 )
            {
             before(grammarAccess.getControlledInteractionAccess().getActionAssignment_1_2_1_1()); 
            // InternalComposedLts.g:2899:1: ( rule__ControlledInteraction__ActionAssignment_1_2_1_1 )
            // InternalComposedLts.g:2899:2: rule__ControlledInteraction__ActionAssignment_1_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ControlledInteraction__ActionAssignment_1_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getControlledInteractionAccess().getActionAssignment_1_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__Group_1_2_1__1__Impl"


    // $ANTLR start "rule__Renaming__Group__0"
    // InternalComposedLts.g:2913:1: rule__Renaming__Group__0 : rule__Renaming__Group__0__Impl rule__Renaming__Group__1 ;
    public final void rule__Renaming__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2917:1: ( rule__Renaming__Group__0__Impl rule__Renaming__Group__1 )
            // InternalComposedLts.g:2918:2: rule__Renaming__Group__0__Impl rule__Renaming__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Renaming__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Renaming__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group__0"


    // $ANTLR start "rule__Renaming__Group__0__Impl"
    // InternalComposedLts.g:2925:1: rule__Renaming__Group__0__Impl : ( ruleBaseLts ) ;
    public final void rule__Renaming__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2929:1: ( ( ruleBaseLts ) )
            // InternalComposedLts.g:2930:1: ( ruleBaseLts )
            {
            // InternalComposedLts.g:2930:1: ( ruleBaseLts )
            // InternalComposedLts.g:2931:1: ruleBaseLts
            {
             before(grammarAccess.getRenamingAccess().getBaseLtsParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleBaseLts();

            state._fsp--;

             after(grammarAccess.getRenamingAccess().getBaseLtsParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group__0__Impl"


    // $ANTLR start "rule__Renaming__Group__1"
    // InternalComposedLts.g:2942:1: rule__Renaming__Group__1 : rule__Renaming__Group__1__Impl ;
    public final void rule__Renaming__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2946:1: ( rule__Renaming__Group__1__Impl )
            // InternalComposedLts.g:2947:2: rule__Renaming__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Renaming__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group__1"


    // $ANTLR start "rule__Renaming__Group__1__Impl"
    // InternalComposedLts.g:2953:1: rule__Renaming__Group__1__Impl : ( ( rule__Renaming__Group_1__0 )? ) ;
    public final void rule__Renaming__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2957:1: ( ( ( rule__Renaming__Group_1__0 )? ) )
            // InternalComposedLts.g:2958:1: ( ( rule__Renaming__Group_1__0 )? )
            {
            // InternalComposedLts.g:2958:1: ( ( rule__Renaming__Group_1__0 )? )
            // InternalComposedLts.g:2959:1: ( rule__Renaming__Group_1__0 )?
            {
             before(grammarAccess.getRenamingAccess().getGroup_1()); 
            // InternalComposedLts.g:2960:1: ( rule__Renaming__Group_1__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==19) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalComposedLts.g:2960:2: rule__Renaming__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Renaming__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRenamingAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group__1__Impl"


    // $ANTLR start "rule__Renaming__Group_1__0"
    // InternalComposedLts.g:2974:1: rule__Renaming__Group_1__0 : rule__Renaming__Group_1__0__Impl rule__Renaming__Group_1__1 ;
    public final void rule__Renaming__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2978:1: ( rule__Renaming__Group_1__0__Impl rule__Renaming__Group_1__1 )
            // InternalComposedLts.g:2979:2: rule__Renaming__Group_1__0__Impl rule__Renaming__Group_1__1
            {
            pushFollow(FOLLOW_15);
            rule__Renaming__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__0"


    // $ANTLR start "rule__Renaming__Group_1__0__Impl"
    // InternalComposedLts.g:2986:1: rule__Renaming__Group_1__0__Impl : ( () ) ;
    public final void rule__Renaming__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:2990:1: ( ( () ) )
            // InternalComposedLts.g:2991:1: ( () )
            {
            // InternalComposedLts.g:2991:1: ( () )
            // InternalComposedLts.g:2992:1: ()
            {
             before(grammarAccess.getRenamingAccess().getRenamingArgAction_1_0()); 
            // InternalComposedLts.g:2993:1: ()
            // InternalComposedLts.g:2995:1: 
            {
            }

             after(grammarAccess.getRenamingAccess().getRenamingArgAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__0__Impl"


    // $ANTLR start "rule__Renaming__Group_1__1"
    // InternalComposedLts.g:3005:1: rule__Renaming__Group_1__1 : rule__Renaming__Group_1__1__Impl rule__Renaming__Group_1__2 ;
    public final void rule__Renaming__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3009:1: ( rule__Renaming__Group_1__1__Impl rule__Renaming__Group_1__2 )
            // InternalComposedLts.g:3010:2: rule__Renaming__Group_1__1__Impl rule__Renaming__Group_1__2
            {
            pushFollow(FOLLOW_4);
            rule__Renaming__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__1"


    // $ANTLR start "rule__Renaming__Group_1__1__Impl"
    // InternalComposedLts.g:3017:1: rule__Renaming__Group_1__1__Impl : ( '{' ) ;
    public final void rule__Renaming__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3021:1: ( ( '{' ) )
            // InternalComposedLts.g:3022:1: ( '{' )
            {
            // InternalComposedLts.g:3022:1: ( '{' )
            // InternalComposedLts.g:3023:1: '{'
            {
             before(grammarAccess.getRenamingAccess().getLeftCurlyBracketKeyword_1_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getRenamingAccess().getLeftCurlyBracketKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__1__Impl"


    // $ANTLR start "rule__Renaming__Group_1__2"
    // InternalComposedLts.g:3036:1: rule__Renaming__Group_1__2 : rule__Renaming__Group_1__2__Impl rule__Renaming__Group_1__3 ;
    public final void rule__Renaming__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3040:1: ( rule__Renaming__Group_1__2__Impl rule__Renaming__Group_1__3 )
            // InternalComposedLts.g:3041:2: rule__Renaming__Group_1__2__Impl rule__Renaming__Group_1__3
            {
            pushFollow(FOLLOW_16);
            rule__Renaming__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__2"


    // $ANTLR start "rule__Renaming__Group_1__2__Impl"
    // InternalComposedLts.g:3048:1: rule__Renaming__Group_1__2__Impl : ( ( rule__Renaming__MapsAssignment_1_2 ) ) ;
    public final void rule__Renaming__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3052:1: ( ( ( rule__Renaming__MapsAssignment_1_2 ) ) )
            // InternalComposedLts.g:3053:1: ( ( rule__Renaming__MapsAssignment_1_2 ) )
            {
            // InternalComposedLts.g:3053:1: ( ( rule__Renaming__MapsAssignment_1_2 ) )
            // InternalComposedLts.g:3054:1: ( rule__Renaming__MapsAssignment_1_2 )
            {
             before(grammarAccess.getRenamingAccess().getMapsAssignment_1_2()); 
            // InternalComposedLts.g:3055:1: ( rule__Renaming__MapsAssignment_1_2 )
            // InternalComposedLts.g:3055:2: rule__Renaming__MapsAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Renaming__MapsAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getRenamingAccess().getMapsAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__2__Impl"


    // $ANTLR start "rule__Renaming__Group_1__3"
    // InternalComposedLts.g:3065:1: rule__Renaming__Group_1__3 : rule__Renaming__Group_1__3__Impl rule__Renaming__Group_1__4 ;
    public final void rule__Renaming__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3069:1: ( rule__Renaming__Group_1__3__Impl rule__Renaming__Group_1__4 )
            // InternalComposedLts.g:3070:2: rule__Renaming__Group_1__3__Impl rule__Renaming__Group_1__4
            {
            pushFollow(FOLLOW_16);
            rule__Renaming__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__3"


    // $ANTLR start "rule__Renaming__Group_1__3__Impl"
    // InternalComposedLts.g:3077:1: rule__Renaming__Group_1__3__Impl : ( ( rule__Renaming__Group_1_3__0 )* ) ;
    public final void rule__Renaming__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3081:1: ( ( ( rule__Renaming__Group_1_3__0 )* ) )
            // InternalComposedLts.g:3082:1: ( ( rule__Renaming__Group_1_3__0 )* )
            {
            // InternalComposedLts.g:3082:1: ( ( rule__Renaming__Group_1_3__0 )* )
            // InternalComposedLts.g:3083:1: ( rule__Renaming__Group_1_3__0 )*
            {
             before(grammarAccess.getRenamingAccess().getGroup_1_3()); 
            // InternalComposedLts.g:3084:1: ( rule__Renaming__Group_1_3__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==18) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalComposedLts.g:3084:2: rule__Renaming__Group_1_3__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Renaming__Group_1_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getRenamingAccess().getGroup_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__3__Impl"


    // $ANTLR start "rule__Renaming__Group_1__4"
    // InternalComposedLts.g:3094:1: rule__Renaming__Group_1__4 : rule__Renaming__Group_1__4__Impl ;
    public final void rule__Renaming__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3098:1: ( rule__Renaming__Group_1__4__Impl )
            // InternalComposedLts.g:3099:2: rule__Renaming__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__4"


    // $ANTLR start "rule__Renaming__Group_1__4__Impl"
    // InternalComposedLts.g:3105:1: rule__Renaming__Group_1__4__Impl : ( '}' ) ;
    public final void rule__Renaming__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3109:1: ( ( '}' ) )
            // InternalComposedLts.g:3110:1: ( '}' )
            {
            // InternalComposedLts.g:3110:1: ( '}' )
            // InternalComposedLts.g:3111:1: '}'
            {
             before(grammarAccess.getRenamingAccess().getRightCurlyBracketKeyword_1_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getRenamingAccess().getRightCurlyBracketKeyword_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1__4__Impl"


    // $ANTLR start "rule__Renaming__Group_1_3__0"
    // InternalComposedLts.g:3134:1: rule__Renaming__Group_1_3__0 : rule__Renaming__Group_1_3__0__Impl rule__Renaming__Group_1_3__1 ;
    public final void rule__Renaming__Group_1_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3138:1: ( rule__Renaming__Group_1_3__0__Impl rule__Renaming__Group_1_3__1 )
            // InternalComposedLts.g:3139:2: rule__Renaming__Group_1_3__0__Impl rule__Renaming__Group_1_3__1
            {
            pushFollow(FOLLOW_4);
            rule__Renaming__Group_1_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1_3__0"


    // $ANTLR start "rule__Renaming__Group_1_3__0__Impl"
    // InternalComposedLts.g:3146:1: rule__Renaming__Group_1_3__0__Impl : ( ',' ) ;
    public final void rule__Renaming__Group_1_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3150:1: ( ( ',' ) )
            // InternalComposedLts.g:3151:1: ( ',' )
            {
            // InternalComposedLts.g:3151:1: ( ',' )
            // InternalComposedLts.g:3152:1: ','
            {
             before(grammarAccess.getRenamingAccess().getCommaKeyword_1_3_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getRenamingAccess().getCommaKeyword_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1_3__0__Impl"


    // $ANTLR start "rule__Renaming__Group_1_3__1"
    // InternalComposedLts.g:3165:1: rule__Renaming__Group_1_3__1 : rule__Renaming__Group_1_3__1__Impl ;
    public final void rule__Renaming__Group_1_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3169:1: ( rule__Renaming__Group_1_3__1__Impl )
            // InternalComposedLts.g:3170:2: rule__Renaming__Group_1_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Renaming__Group_1_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1_3__1"


    // $ANTLR start "rule__Renaming__Group_1_3__1__Impl"
    // InternalComposedLts.g:3176:1: rule__Renaming__Group_1_3__1__Impl : ( ( rule__Renaming__MapsAssignment_1_3_1 ) ) ;
    public final void rule__Renaming__Group_1_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3180:1: ( ( ( rule__Renaming__MapsAssignment_1_3_1 ) ) )
            // InternalComposedLts.g:3181:1: ( ( rule__Renaming__MapsAssignment_1_3_1 ) )
            {
            // InternalComposedLts.g:3181:1: ( ( rule__Renaming__MapsAssignment_1_3_1 ) )
            // InternalComposedLts.g:3182:1: ( rule__Renaming__MapsAssignment_1_3_1 )
            {
             before(grammarAccess.getRenamingAccess().getMapsAssignment_1_3_1()); 
            // InternalComposedLts.g:3183:1: ( rule__Renaming__MapsAssignment_1_3_1 )
            // InternalComposedLts.g:3183:2: rule__Renaming__MapsAssignment_1_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Renaming__MapsAssignment_1_3_1();

            state._fsp--;


            }

             after(grammarAccess.getRenamingAccess().getMapsAssignment_1_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__Group_1_3__1__Impl"


    // $ANTLR start "rule__BaseLts__Group_1__0"
    // InternalComposedLts.g:3197:1: rule__BaseLts__Group_1__0 : rule__BaseLts__Group_1__0__Impl rule__BaseLts__Group_1__1 ;
    public final void rule__BaseLts__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3201:1: ( rule__BaseLts__Group_1__0__Impl rule__BaseLts__Group_1__1 )
            // InternalComposedLts.g:3202:2: rule__BaseLts__Group_1__0__Impl rule__BaseLts__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__BaseLts__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BaseLts__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Group_1__0"


    // $ANTLR start "rule__BaseLts__Group_1__0__Impl"
    // InternalComposedLts.g:3209:1: rule__BaseLts__Group_1__0__Impl : ( '(' ) ;
    public final void rule__BaseLts__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3213:1: ( ( '(' ) )
            // InternalComposedLts.g:3214:1: ( '(' )
            {
            // InternalComposedLts.g:3214:1: ( '(' )
            // InternalComposedLts.g:3215:1: '('
            {
             before(grammarAccess.getBaseLtsAccess().getLeftParenthesisKeyword_1_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getBaseLtsAccess().getLeftParenthesisKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Group_1__0__Impl"


    // $ANTLR start "rule__BaseLts__Group_1__1"
    // InternalComposedLts.g:3228:1: rule__BaseLts__Group_1__1 : rule__BaseLts__Group_1__1__Impl rule__BaseLts__Group_1__2 ;
    public final void rule__BaseLts__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3232:1: ( rule__BaseLts__Group_1__1__Impl rule__BaseLts__Group_1__2 )
            // InternalComposedLts.g:3233:2: rule__BaseLts__Group_1__1__Impl rule__BaseLts__Group_1__2
            {
            pushFollow(FOLLOW_17);
            rule__BaseLts__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BaseLts__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Group_1__1"


    // $ANTLR start "rule__BaseLts__Group_1__1__Impl"
    // InternalComposedLts.g:3240:1: rule__BaseLts__Group_1__1__Impl : ( ruleCompositionBody ) ;
    public final void rule__BaseLts__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3244:1: ( ( ruleCompositionBody ) )
            // InternalComposedLts.g:3245:1: ( ruleCompositionBody )
            {
            // InternalComposedLts.g:3245:1: ( ruleCompositionBody )
            // InternalComposedLts.g:3246:1: ruleCompositionBody
            {
             before(grammarAccess.getBaseLtsAccess().getCompositionBodyParserRuleCall_1_1()); 
            pushFollow(FOLLOW_2);
            ruleCompositionBody();

            state._fsp--;

             after(grammarAccess.getBaseLtsAccess().getCompositionBodyParserRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Group_1__1__Impl"


    // $ANTLR start "rule__BaseLts__Group_1__2"
    // InternalComposedLts.g:3257:1: rule__BaseLts__Group_1__2 : rule__BaseLts__Group_1__2__Impl ;
    public final void rule__BaseLts__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3261:1: ( rule__BaseLts__Group_1__2__Impl )
            // InternalComposedLts.g:3262:2: rule__BaseLts__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BaseLts__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Group_1__2"


    // $ANTLR start "rule__BaseLts__Group_1__2__Impl"
    // InternalComposedLts.g:3268:1: rule__BaseLts__Group_1__2__Impl : ( ')' ) ;
    public final void rule__BaseLts__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3272:1: ( ( ')' ) )
            // InternalComposedLts.g:3273:1: ( ')' )
            {
            // InternalComposedLts.g:3273:1: ( ')' )
            // InternalComposedLts.g:3274:1: ')'
            {
             before(grammarAccess.getBaseLtsAccess().getRightParenthesisKeyword_1_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getBaseLtsAccess().getRightParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BaseLts__Group_1__2__Impl"


    // $ANTLR start "rule__Mapping__Group__0"
    // InternalComposedLts.g:3293:1: rule__Mapping__Group__0 : rule__Mapping__Group__0__Impl rule__Mapping__Group__1 ;
    public final void rule__Mapping__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3297:1: ( rule__Mapping__Group__0__Impl rule__Mapping__Group__1 )
            // InternalComposedLts.g:3298:2: rule__Mapping__Group__0__Impl rule__Mapping__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Mapping__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__0"


    // $ANTLR start "rule__Mapping__Group__0__Impl"
    // InternalComposedLts.g:3305:1: rule__Mapping__Group__0__Impl : ( ( rule__Mapping__SrcAssignment_0 ) ) ;
    public final void rule__Mapping__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3309:1: ( ( ( rule__Mapping__SrcAssignment_0 ) ) )
            // InternalComposedLts.g:3310:1: ( ( rule__Mapping__SrcAssignment_0 ) )
            {
            // InternalComposedLts.g:3310:1: ( ( rule__Mapping__SrcAssignment_0 ) )
            // InternalComposedLts.g:3311:1: ( rule__Mapping__SrcAssignment_0 )
            {
             before(grammarAccess.getMappingAccess().getSrcAssignment_0()); 
            // InternalComposedLts.g:3312:1: ( rule__Mapping__SrcAssignment_0 )
            // InternalComposedLts.g:3312:2: rule__Mapping__SrcAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__SrcAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getSrcAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__0__Impl"


    // $ANTLR start "rule__Mapping__Group__1"
    // InternalComposedLts.g:3322:1: rule__Mapping__Group__1 : rule__Mapping__Group__1__Impl rule__Mapping__Group__2 ;
    public final void rule__Mapping__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3326:1: ( rule__Mapping__Group__1__Impl rule__Mapping__Group__2 )
            // InternalComposedLts.g:3327:2: rule__Mapping__Group__1__Impl rule__Mapping__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Mapping__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Mapping__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__1"


    // $ANTLR start "rule__Mapping__Group__1__Impl"
    // InternalComposedLts.g:3334:1: rule__Mapping__Group__1__Impl : ( '->' ) ;
    public final void rule__Mapping__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3338:1: ( ( '->' ) )
            // InternalComposedLts.g:3339:1: ( '->' )
            {
            // InternalComposedLts.g:3339:1: ( '->' )
            // InternalComposedLts.g:3340:1: '->'
            {
             before(grammarAccess.getMappingAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getHyphenMinusGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__1__Impl"


    // $ANTLR start "rule__Mapping__Group__2"
    // InternalComposedLts.g:3353:1: rule__Mapping__Group__2 : rule__Mapping__Group__2__Impl ;
    public final void rule__Mapping__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3357:1: ( rule__Mapping__Group__2__Impl )
            // InternalComposedLts.g:3358:2: rule__Mapping__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__2"


    // $ANTLR start "rule__Mapping__Group__2__Impl"
    // InternalComposedLts.g:3364:1: rule__Mapping__Group__2__Impl : ( ( rule__Mapping__TrgAssignment_2 ) ) ;
    public final void rule__Mapping__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3368:1: ( ( ( rule__Mapping__TrgAssignment_2 ) ) )
            // InternalComposedLts.g:3369:1: ( ( rule__Mapping__TrgAssignment_2 ) )
            {
            // InternalComposedLts.g:3369:1: ( ( rule__Mapping__TrgAssignment_2 ) )
            // InternalComposedLts.g:3370:1: ( rule__Mapping__TrgAssignment_2 )
            {
             before(grammarAccess.getMappingAccess().getTrgAssignment_2()); 
            // InternalComposedLts.g:3371:1: ( rule__Mapping__TrgAssignment_2 )
            // InternalComposedLts.g:3371:2: rule__Mapping__TrgAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Mapping__TrgAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMappingAccess().getTrgAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__Group__2__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__0"
    // InternalComposedLts.g:3387:1: rule__LtsDeclarationBody__Group__0 : rule__LtsDeclarationBody__Group__0__Impl rule__LtsDeclarationBody__Group__1 ;
    public final void rule__LtsDeclarationBody__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3391:1: ( rule__LtsDeclarationBody__Group__0__Impl rule__LtsDeclarationBody__Group__1 )
            // InternalComposedLts.g:3392:2: rule__LtsDeclarationBody__Group__0__Impl rule__LtsDeclarationBody__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__LtsDeclarationBody__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__0"


    // $ANTLR start "rule__LtsDeclarationBody__Group__0__Impl"
    // InternalComposedLts.g:3399:1: rule__LtsDeclarationBody__Group__0__Impl : ( '{' ) ;
    public final void rule__LtsDeclarationBody__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3403:1: ( ( '{' ) )
            // InternalComposedLts.g:3404:1: ( '{' )
            {
            // InternalComposedLts.g:3404:1: ( '{' )
            // InternalComposedLts.g:3405:1: '{'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getLeftCurlyBracketKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getLeftCurlyBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__0__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__1"
    // InternalComposedLts.g:3418:1: rule__LtsDeclarationBody__Group__1 : rule__LtsDeclarationBody__Group__1__Impl rule__LtsDeclarationBody__Group__2 ;
    public final void rule__LtsDeclarationBody__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3422:1: ( rule__LtsDeclarationBody__Group__1__Impl rule__LtsDeclarationBody__Group__2 )
            // InternalComposedLts.g:3423:2: rule__LtsDeclarationBody__Group__1__Impl rule__LtsDeclarationBody__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__LtsDeclarationBody__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__1"


    // $ANTLR start "rule__LtsDeclarationBody__Group__1__Impl"
    // InternalComposedLts.g:3430:1: rule__LtsDeclarationBody__Group__1__Impl : ( 'states' ) ;
    public final void rule__LtsDeclarationBody__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3434:1: ( ( 'states' ) )
            // InternalComposedLts.g:3435:1: ( 'states' )
            {
            // InternalComposedLts.g:3435:1: ( 'states' )
            // InternalComposedLts.g:3436:1: 'states'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getStatesKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getStatesKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__1__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__2"
    // InternalComposedLts.g:3449:1: rule__LtsDeclarationBody__Group__2 : rule__LtsDeclarationBody__Group__2__Impl rule__LtsDeclarationBody__Group__3 ;
    public final void rule__LtsDeclarationBody__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3453:1: ( rule__LtsDeclarationBody__Group__2__Impl rule__LtsDeclarationBody__Group__3 )
            // InternalComposedLts.g:3454:2: rule__LtsDeclarationBody__Group__2__Impl rule__LtsDeclarationBody__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__LtsDeclarationBody__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__2"


    // $ANTLR start "rule__LtsDeclarationBody__Group__2__Impl"
    // InternalComposedLts.g:3461:1: rule__LtsDeclarationBody__Group__2__Impl : ( ':' ) ;
    public final void rule__LtsDeclarationBody__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3465:1: ( ( ':' ) )
            // InternalComposedLts.g:3466:1: ( ':' )
            {
            // InternalComposedLts.g:3466:1: ( ':' )
            // InternalComposedLts.g:3467:1: ':'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_2()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__2__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__3"
    // InternalComposedLts.g:3480:1: rule__LtsDeclarationBody__Group__3 : rule__LtsDeclarationBody__Group__3__Impl rule__LtsDeclarationBody__Group__4 ;
    public final void rule__LtsDeclarationBody__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3484:1: ( rule__LtsDeclarationBody__Group__3__Impl rule__LtsDeclarationBody__Group__4 )
            // InternalComposedLts.g:3485:2: rule__LtsDeclarationBody__Group__3__Impl rule__LtsDeclarationBody__Group__4
            {
            pushFollow(FOLLOW_21);
            rule__LtsDeclarationBody__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__3"


    // $ANTLR start "rule__LtsDeclarationBody__Group__3__Impl"
    // InternalComposedLts.g:3492:1: rule__LtsDeclarationBody__Group__3__Impl : ( ( rule__LtsDeclarationBody__StatesAssignment_3 ) ) ;
    public final void rule__LtsDeclarationBody__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3496:1: ( ( ( rule__LtsDeclarationBody__StatesAssignment_3 ) ) )
            // InternalComposedLts.g:3497:1: ( ( rule__LtsDeclarationBody__StatesAssignment_3 ) )
            {
            // InternalComposedLts.g:3497:1: ( ( rule__LtsDeclarationBody__StatesAssignment_3 ) )
            // InternalComposedLts.g:3498:1: ( rule__LtsDeclarationBody__StatesAssignment_3 )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getStatesAssignment_3()); 
            // InternalComposedLts.g:3499:1: ( rule__LtsDeclarationBody__StatesAssignment_3 )
            // InternalComposedLts.g:3499:2: rule__LtsDeclarationBody__StatesAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__StatesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getStatesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__3__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__4"
    // InternalComposedLts.g:3509:1: rule__LtsDeclarationBody__Group__4 : rule__LtsDeclarationBody__Group__4__Impl rule__LtsDeclarationBody__Group__5 ;
    public final void rule__LtsDeclarationBody__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3513:1: ( rule__LtsDeclarationBody__Group__4__Impl rule__LtsDeclarationBody__Group__5 )
            // InternalComposedLts.g:3514:2: rule__LtsDeclarationBody__Group__4__Impl rule__LtsDeclarationBody__Group__5
            {
            pushFollow(FOLLOW_21);
            rule__LtsDeclarationBody__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__4"


    // $ANTLR start "rule__LtsDeclarationBody__Group__4__Impl"
    // InternalComposedLts.g:3521:1: rule__LtsDeclarationBody__Group__4__Impl : ( ( rule__LtsDeclarationBody__Group_4__0 )* ) ;
    public final void rule__LtsDeclarationBody__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3525:1: ( ( ( rule__LtsDeclarationBody__Group_4__0 )* ) )
            // InternalComposedLts.g:3526:1: ( ( rule__LtsDeclarationBody__Group_4__0 )* )
            {
            // InternalComposedLts.g:3526:1: ( ( rule__LtsDeclarationBody__Group_4__0 )* )
            // InternalComposedLts.g:3527:1: ( rule__LtsDeclarationBody__Group_4__0 )*
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getGroup_4()); 
            // InternalComposedLts.g:3528:1: ( rule__LtsDeclarationBody__Group_4__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==18) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalComposedLts.g:3528:2: rule__LtsDeclarationBody__Group_4__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__LtsDeclarationBody__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getLtsDeclarationBodyAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__4__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__5"
    // InternalComposedLts.g:3538:1: rule__LtsDeclarationBody__Group__5 : rule__LtsDeclarationBody__Group__5__Impl rule__LtsDeclarationBody__Group__6 ;
    public final void rule__LtsDeclarationBody__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3542:1: ( rule__LtsDeclarationBody__Group__5__Impl rule__LtsDeclarationBody__Group__6 )
            // InternalComposedLts.g:3543:2: rule__LtsDeclarationBody__Group__5__Impl rule__LtsDeclarationBody__Group__6
            {
            pushFollow(FOLLOW_22);
            rule__LtsDeclarationBody__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__5"


    // $ANTLR start "rule__LtsDeclarationBody__Group__5__Impl"
    // InternalComposedLts.g:3550:1: rule__LtsDeclarationBody__Group__5__Impl : ( ';' ) ;
    public final void rule__LtsDeclarationBody__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3554:1: ( ( ';' ) )
            // InternalComposedLts.g:3555:1: ( ';' )
            {
            // InternalComposedLts.g:3555:1: ( ';' )
            // InternalComposedLts.g:3556:1: ';'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getSemicolonKeyword_5()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__5__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__6"
    // InternalComposedLts.g:3569:1: rule__LtsDeclarationBody__Group__6 : rule__LtsDeclarationBody__Group__6__Impl rule__LtsDeclarationBody__Group__7 ;
    public final void rule__LtsDeclarationBody__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3573:1: ( rule__LtsDeclarationBody__Group__6__Impl rule__LtsDeclarationBody__Group__7 )
            // InternalComposedLts.g:3574:2: rule__LtsDeclarationBody__Group__6__Impl rule__LtsDeclarationBody__Group__7
            {
            pushFollow(FOLLOW_20);
            rule__LtsDeclarationBody__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__6"


    // $ANTLR start "rule__LtsDeclarationBody__Group__6__Impl"
    // InternalComposedLts.g:3581:1: rule__LtsDeclarationBody__Group__6__Impl : ( 'init' ) ;
    public final void rule__LtsDeclarationBody__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3585:1: ( ( 'init' ) )
            // InternalComposedLts.g:3586:1: ( 'init' )
            {
            // InternalComposedLts.g:3586:1: ( 'init' )
            // InternalComposedLts.g:3587:1: 'init'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitKeyword_6()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getInitKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__6__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__7"
    // InternalComposedLts.g:3600:1: rule__LtsDeclarationBody__Group__7 : rule__LtsDeclarationBody__Group__7__Impl rule__LtsDeclarationBody__Group__8 ;
    public final void rule__LtsDeclarationBody__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3604:1: ( rule__LtsDeclarationBody__Group__7__Impl rule__LtsDeclarationBody__Group__8 )
            // InternalComposedLts.g:3605:2: rule__LtsDeclarationBody__Group__7__Impl rule__LtsDeclarationBody__Group__8
            {
            pushFollow(FOLLOW_4);
            rule__LtsDeclarationBody__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__7"


    // $ANTLR start "rule__LtsDeclarationBody__Group__7__Impl"
    // InternalComposedLts.g:3612:1: rule__LtsDeclarationBody__Group__7__Impl : ( ':' ) ;
    public final void rule__LtsDeclarationBody__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3616:1: ( ( ':' ) )
            // InternalComposedLts.g:3617:1: ( ':' )
            {
            // InternalComposedLts.g:3617:1: ( ':' )
            // InternalComposedLts.g:3618:1: ':'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_7()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__7__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__8"
    // InternalComposedLts.g:3631:1: rule__LtsDeclarationBody__Group__8 : rule__LtsDeclarationBody__Group__8__Impl rule__LtsDeclarationBody__Group__9 ;
    public final void rule__LtsDeclarationBody__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3635:1: ( rule__LtsDeclarationBody__Group__8__Impl rule__LtsDeclarationBody__Group__9 )
            // InternalComposedLts.g:3636:2: rule__LtsDeclarationBody__Group__8__Impl rule__LtsDeclarationBody__Group__9
            {
            pushFollow(FOLLOW_21);
            rule__LtsDeclarationBody__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__8"


    // $ANTLR start "rule__LtsDeclarationBody__Group__8__Impl"
    // InternalComposedLts.g:3643:1: rule__LtsDeclarationBody__Group__8__Impl : ( ( rule__LtsDeclarationBody__InitAssignment_8 ) ) ;
    public final void rule__LtsDeclarationBody__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3647:1: ( ( ( rule__LtsDeclarationBody__InitAssignment_8 ) ) )
            // InternalComposedLts.g:3648:1: ( ( rule__LtsDeclarationBody__InitAssignment_8 ) )
            {
            // InternalComposedLts.g:3648:1: ( ( rule__LtsDeclarationBody__InitAssignment_8 ) )
            // InternalComposedLts.g:3649:1: ( rule__LtsDeclarationBody__InitAssignment_8 )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitAssignment_8()); 
            // InternalComposedLts.g:3650:1: ( rule__LtsDeclarationBody__InitAssignment_8 )
            // InternalComposedLts.g:3650:2: rule__LtsDeclarationBody__InitAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__InitAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getInitAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__8__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__9"
    // InternalComposedLts.g:3660:1: rule__LtsDeclarationBody__Group__9 : rule__LtsDeclarationBody__Group__9__Impl rule__LtsDeclarationBody__Group__10 ;
    public final void rule__LtsDeclarationBody__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3664:1: ( rule__LtsDeclarationBody__Group__9__Impl rule__LtsDeclarationBody__Group__10 )
            // InternalComposedLts.g:3665:2: rule__LtsDeclarationBody__Group__9__Impl rule__LtsDeclarationBody__Group__10
            {
            pushFollow(FOLLOW_21);
            rule__LtsDeclarationBody__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__9"


    // $ANTLR start "rule__LtsDeclarationBody__Group__9__Impl"
    // InternalComposedLts.g:3672:1: rule__LtsDeclarationBody__Group__9__Impl : ( ( rule__LtsDeclarationBody__Group_9__0 )* ) ;
    public final void rule__LtsDeclarationBody__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3676:1: ( ( ( rule__LtsDeclarationBody__Group_9__0 )* ) )
            // InternalComposedLts.g:3677:1: ( ( rule__LtsDeclarationBody__Group_9__0 )* )
            {
            // InternalComposedLts.g:3677:1: ( ( rule__LtsDeclarationBody__Group_9__0 )* )
            // InternalComposedLts.g:3678:1: ( rule__LtsDeclarationBody__Group_9__0 )*
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getGroup_9()); 
            // InternalComposedLts.g:3679:1: ( rule__LtsDeclarationBody__Group_9__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==18) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalComposedLts.g:3679:2: rule__LtsDeclarationBody__Group_9__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__LtsDeclarationBody__Group_9__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getLtsDeclarationBodyAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__9__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__10"
    // InternalComposedLts.g:3689:1: rule__LtsDeclarationBody__Group__10 : rule__LtsDeclarationBody__Group__10__Impl rule__LtsDeclarationBody__Group__11 ;
    public final void rule__LtsDeclarationBody__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3693:1: ( rule__LtsDeclarationBody__Group__10__Impl rule__LtsDeclarationBody__Group__11 )
            // InternalComposedLts.g:3694:2: rule__LtsDeclarationBody__Group__10__Impl rule__LtsDeclarationBody__Group__11
            {
            pushFollow(FOLLOW_23);
            rule__LtsDeclarationBody__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__10"


    // $ANTLR start "rule__LtsDeclarationBody__Group__10__Impl"
    // InternalComposedLts.g:3701:1: rule__LtsDeclarationBody__Group__10__Impl : ( ';' ) ;
    public final void rule__LtsDeclarationBody__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3705:1: ( ( ';' ) )
            // InternalComposedLts.g:3706:1: ( ';' )
            {
            // InternalComposedLts.g:3706:1: ( ';' )
            // InternalComposedLts.g:3707:1: ';'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getSemicolonKeyword_10()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getSemicolonKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__10__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__11"
    // InternalComposedLts.g:3720:1: rule__LtsDeclarationBody__Group__11 : rule__LtsDeclarationBody__Group__11__Impl rule__LtsDeclarationBody__Group__12 ;
    public final void rule__LtsDeclarationBody__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3724:1: ( rule__LtsDeclarationBody__Group__11__Impl rule__LtsDeclarationBody__Group__12 )
            // InternalComposedLts.g:3725:2: rule__LtsDeclarationBody__Group__11__Impl rule__LtsDeclarationBody__Group__12
            {
            pushFollow(FOLLOW_23);
            rule__LtsDeclarationBody__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__11"


    // $ANTLR start "rule__LtsDeclarationBody__Group__11__Impl"
    // InternalComposedLts.g:3732:1: rule__LtsDeclarationBody__Group__11__Impl : ( ( rule__LtsDeclarationBody__Group_11__0 )? ) ;
    public final void rule__LtsDeclarationBody__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3736:1: ( ( ( rule__LtsDeclarationBody__Group_11__0 )? ) )
            // InternalComposedLts.g:3737:1: ( ( rule__LtsDeclarationBody__Group_11__0 )? )
            {
            // InternalComposedLts.g:3737:1: ( ( rule__LtsDeclarationBody__Group_11__0 )? )
            // InternalComposedLts.g:3738:1: ( rule__LtsDeclarationBody__Group_11__0 )?
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getGroup_11()); 
            // InternalComposedLts.g:3739:1: ( rule__LtsDeclarationBody__Group_11__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==27) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalComposedLts.g:3739:2: rule__LtsDeclarationBody__Group_11__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LtsDeclarationBody__Group_11__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getGroup_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__11__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__12"
    // InternalComposedLts.g:3749:1: rule__LtsDeclarationBody__Group__12 : rule__LtsDeclarationBody__Group__12__Impl rule__LtsDeclarationBody__Group__13 ;
    public final void rule__LtsDeclarationBody__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3753:1: ( rule__LtsDeclarationBody__Group__12__Impl rule__LtsDeclarationBody__Group__13 )
            // InternalComposedLts.g:3754:2: rule__LtsDeclarationBody__Group__12__Impl rule__LtsDeclarationBody__Group__13
            {
            pushFollow(FOLLOW_23);
            rule__LtsDeclarationBody__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__12"


    // $ANTLR start "rule__LtsDeclarationBody__Group__12__Impl"
    // InternalComposedLts.g:3761:1: rule__LtsDeclarationBody__Group__12__Impl : ( ( rule__LtsDeclarationBody__Group_12__0 )? ) ;
    public final void rule__LtsDeclarationBody__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3765:1: ( ( ( rule__LtsDeclarationBody__Group_12__0 )? ) )
            // InternalComposedLts.g:3766:1: ( ( rule__LtsDeclarationBody__Group_12__0 )? )
            {
            // InternalComposedLts.g:3766:1: ( ( rule__LtsDeclarationBody__Group_12__0 )? )
            // InternalComposedLts.g:3767:1: ( rule__LtsDeclarationBody__Group_12__0 )?
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getGroup_12()); 
            // InternalComposedLts.g:3768:1: ( rule__LtsDeclarationBody__Group_12__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==29) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalComposedLts.g:3768:2: rule__LtsDeclarationBody__Group_12__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LtsDeclarationBody__Group_12__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getGroup_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__12__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group__13"
    // InternalComposedLts.g:3778:1: rule__LtsDeclarationBody__Group__13 : rule__LtsDeclarationBody__Group__13__Impl ;
    public final void rule__LtsDeclarationBody__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3782:1: ( rule__LtsDeclarationBody__Group__13__Impl )
            // InternalComposedLts.g:3783:2: rule__LtsDeclarationBody__Group__13__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group__13__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__13"


    // $ANTLR start "rule__LtsDeclarationBody__Group__13__Impl"
    // InternalComposedLts.g:3789:1: rule__LtsDeclarationBody__Group__13__Impl : ( '}' ) ;
    public final void rule__LtsDeclarationBody__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3793:1: ( ( '}' ) )
            // InternalComposedLts.g:3794:1: ( '}' )
            {
            // InternalComposedLts.g:3794:1: ( '}' )
            // InternalComposedLts.g:3795:1: '}'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getRightCurlyBracketKeyword_13()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getRightCurlyBracketKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group__13__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_4__0"
    // InternalComposedLts.g:3836:1: rule__LtsDeclarationBody__Group_4__0 : rule__LtsDeclarationBody__Group_4__0__Impl rule__LtsDeclarationBody__Group_4__1 ;
    public final void rule__LtsDeclarationBody__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3840:1: ( rule__LtsDeclarationBody__Group_4__0__Impl rule__LtsDeclarationBody__Group_4__1 )
            // InternalComposedLts.g:3841:2: rule__LtsDeclarationBody__Group_4__0__Impl rule__LtsDeclarationBody__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__LtsDeclarationBody__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_4__0"


    // $ANTLR start "rule__LtsDeclarationBody__Group_4__0__Impl"
    // InternalComposedLts.g:3848:1: rule__LtsDeclarationBody__Group_4__0__Impl : ( ',' ) ;
    public final void rule__LtsDeclarationBody__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3852:1: ( ( ',' ) )
            // InternalComposedLts.g:3853:1: ( ',' )
            {
            // InternalComposedLts.g:3853:1: ( ',' )
            // InternalComposedLts.g:3854:1: ','
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getCommaKeyword_4_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_4__0__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_4__1"
    // InternalComposedLts.g:3867:1: rule__LtsDeclarationBody__Group_4__1 : rule__LtsDeclarationBody__Group_4__1__Impl ;
    public final void rule__LtsDeclarationBody__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3871:1: ( rule__LtsDeclarationBody__Group_4__1__Impl )
            // InternalComposedLts.g:3872:2: rule__LtsDeclarationBody__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_4__1"


    // $ANTLR start "rule__LtsDeclarationBody__Group_4__1__Impl"
    // InternalComposedLts.g:3878:1: rule__LtsDeclarationBody__Group_4__1__Impl : ( ( rule__LtsDeclarationBody__StatesAssignment_4_1 ) ) ;
    public final void rule__LtsDeclarationBody__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3882:1: ( ( ( rule__LtsDeclarationBody__StatesAssignment_4_1 ) ) )
            // InternalComposedLts.g:3883:1: ( ( rule__LtsDeclarationBody__StatesAssignment_4_1 ) )
            {
            // InternalComposedLts.g:3883:1: ( ( rule__LtsDeclarationBody__StatesAssignment_4_1 ) )
            // InternalComposedLts.g:3884:1: ( rule__LtsDeclarationBody__StatesAssignment_4_1 )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getStatesAssignment_4_1()); 
            // InternalComposedLts.g:3885:1: ( rule__LtsDeclarationBody__StatesAssignment_4_1 )
            // InternalComposedLts.g:3885:2: rule__LtsDeclarationBody__StatesAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__StatesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getStatesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_4__1__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_9__0"
    // InternalComposedLts.g:3899:1: rule__LtsDeclarationBody__Group_9__0 : rule__LtsDeclarationBody__Group_9__0__Impl rule__LtsDeclarationBody__Group_9__1 ;
    public final void rule__LtsDeclarationBody__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3903:1: ( rule__LtsDeclarationBody__Group_9__0__Impl rule__LtsDeclarationBody__Group_9__1 )
            // InternalComposedLts.g:3904:2: rule__LtsDeclarationBody__Group_9__0__Impl rule__LtsDeclarationBody__Group_9__1
            {
            pushFollow(FOLLOW_4);
            rule__LtsDeclarationBody__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_9__0"


    // $ANTLR start "rule__LtsDeclarationBody__Group_9__0__Impl"
    // InternalComposedLts.g:3911:1: rule__LtsDeclarationBody__Group_9__0__Impl : ( ',' ) ;
    public final void rule__LtsDeclarationBody__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3915:1: ( ( ',' ) )
            // InternalComposedLts.g:3916:1: ( ',' )
            {
            // InternalComposedLts.g:3916:1: ( ',' )
            // InternalComposedLts.g:3917:1: ','
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getCommaKeyword_9_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getCommaKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_9__0__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_9__1"
    // InternalComposedLts.g:3930:1: rule__LtsDeclarationBody__Group_9__1 : rule__LtsDeclarationBody__Group_9__1__Impl ;
    public final void rule__LtsDeclarationBody__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3934:1: ( rule__LtsDeclarationBody__Group_9__1__Impl )
            // InternalComposedLts.g:3935:2: rule__LtsDeclarationBody__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_9__1"


    // $ANTLR start "rule__LtsDeclarationBody__Group_9__1__Impl"
    // InternalComposedLts.g:3941:1: rule__LtsDeclarationBody__Group_9__1__Impl : ( ( rule__LtsDeclarationBody__InitAssignment_9_1 ) ) ;
    public final void rule__LtsDeclarationBody__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3945:1: ( ( ( rule__LtsDeclarationBody__InitAssignment_9_1 ) ) )
            // InternalComposedLts.g:3946:1: ( ( rule__LtsDeclarationBody__InitAssignment_9_1 ) )
            {
            // InternalComposedLts.g:3946:1: ( ( rule__LtsDeclarationBody__InitAssignment_9_1 ) )
            // InternalComposedLts.g:3947:1: ( rule__LtsDeclarationBody__InitAssignment_9_1 )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitAssignment_9_1()); 
            // InternalComposedLts.g:3948:1: ( rule__LtsDeclarationBody__InitAssignment_9_1 )
            // InternalComposedLts.g:3948:2: rule__LtsDeclarationBody__InitAssignment_9_1
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__InitAssignment_9_1();

            state._fsp--;


            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getInitAssignment_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_9__1__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__0"
    // InternalComposedLts.g:3962:1: rule__LtsDeclarationBody__Group_11__0 : rule__LtsDeclarationBody__Group_11__0__Impl rule__LtsDeclarationBody__Group_11__1 ;
    public final void rule__LtsDeclarationBody__Group_11__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3966:1: ( rule__LtsDeclarationBody__Group_11__0__Impl rule__LtsDeclarationBody__Group_11__1 )
            // InternalComposedLts.g:3967:2: rule__LtsDeclarationBody__Group_11__0__Impl rule__LtsDeclarationBody__Group_11__1
            {
            pushFollow(FOLLOW_20);
            rule__LtsDeclarationBody__Group_11__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_11__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__0"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__0__Impl"
    // InternalComposedLts.g:3974:1: rule__LtsDeclarationBody__Group_11__0__Impl : ( 'rules' ) ;
    public final void rule__LtsDeclarationBody__Group_11__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3978:1: ( ( 'rules' ) )
            // InternalComposedLts.g:3979:1: ( 'rules' )
            {
            // InternalComposedLts.g:3979:1: ( 'rules' )
            // InternalComposedLts.g:3980:1: 'rules'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getRulesKeyword_11_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getRulesKeyword_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__0__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__1"
    // InternalComposedLts.g:3993:1: rule__LtsDeclarationBody__Group_11__1 : rule__LtsDeclarationBody__Group_11__1__Impl rule__LtsDeclarationBody__Group_11__2 ;
    public final void rule__LtsDeclarationBody__Group_11__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:3997:1: ( rule__LtsDeclarationBody__Group_11__1__Impl rule__LtsDeclarationBody__Group_11__2 )
            // InternalComposedLts.g:3998:2: rule__LtsDeclarationBody__Group_11__1__Impl rule__LtsDeclarationBody__Group_11__2
            {
            pushFollow(FOLLOW_24);
            rule__LtsDeclarationBody__Group_11__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_11__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__1"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__1__Impl"
    // InternalComposedLts.g:4005:1: rule__LtsDeclarationBody__Group_11__1__Impl : ( ':' ) ;
    public final void rule__LtsDeclarationBody__Group_11__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4009:1: ( ( ':' ) )
            // InternalComposedLts.g:4010:1: ( ':' )
            {
            // InternalComposedLts.g:4010:1: ( ':' )
            // InternalComposedLts.g:4011:1: ':'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_11_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_11_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__1__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__2"
    // InternalComposedLts.g:4024:1: rule__LtsDeclarationBody__Group_11__2 : rule__LtsDeclarationBody__Group_11__2__Impl rule__LtsDeclarationBody__Group_11__3 ;
    public final void rule__LtsDeclarationBody__Group_11__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4028:1: ( rule__LtsDeclarationBody__Group_11__2__Impl rule__LtsDeclarationBody__Group_11__3 )
            // InternalComposedLts.g:4029:2: rule__LtsDeclarationBody__Group_11__2__Impl rule__LtsDeclarationBody__Group_11__3
            {
            pushFollow(FOLLOW_24);
            rule__LtsDeclarationBody__Group_11__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_11__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__2"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__2__Impl"
    // InternalComposedLts.g:4036:1: rule__LtsDeclarationBody__Group_11__2__Impl : ( ( rule__LtsDeclarationBody__RulesAssignment_11_2 )* ) ;
    public final void rule__LtsDeclarationBody__Group_11__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4040:1: ( ( ( rule__LtsDeclarationBody__RulesAssignment_11_2 )* ) )
            // InternalComposedLts.g:4041:1: ( ( rule__LtsDeclarationBody__RulesAssignment_11_2 )* )
            {
            // InternalComposedLts.g:4041:1: ( ( rule__LtsDeclarationBody__RulesAssignment_11_2 )* )
            // InternalComposedLts.g:4042:1: ( rule__LtsDeclarationBody__RulesAssignment_11_2 )*
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getRulesAssignment_11_2()); 
            // InternalComposedLts.g:4043:1: ( rule__LtsDeclarationBody__RulesAssignment_11_2 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==RULE_ID) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalComposedLts.g:4043:2: rule__LtsDeclarationBody__RulesAssignment_11_2
            	    {
            	    pushFollow(FOLLOW_25);
            	    rule__LtsDeclarationBody__RulesAssignment_11_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getLtsDeclarationBodyAccess().getRulesAssignment_11_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__2__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__3"
    // InternalComposedLts.g:4053:1: rule__LtsDeclarationBody__Group_11__3 : rule__LtsDeclarationBody__Group_11__3__Impl ;
    public final void rule__LtsDeclarationBody__Group_11__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4057:1: ( rule__LtsDeclarationBody__Group_11__3__Impl )
            // InternalComposedLts.g:4058:2: rule__LtsDeclarationBody__Group_11__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_11__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__3"


    // $ANTLR start "rule__LtsDeclarationBody__Group_11__3__Impl"
    // InternalComposedLts.g:4064:1: rule__LtsDeclarationBody__Group_11__3__Impl : ( 'endrules' ) ;
    public final void rule__LtsDeclarationBody__Group_11__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4068:1: ( ( 'endrules' ) )
            // InternalComposedLts.g:4069:1: ( 'endrules' )
            {
            // InternalComposedLts.g:4069:1: ( 'endrules' )
            // InternalComposedLts.g:4070:1: 'endrules'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getEndrulesKeyword_11_3()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getEndrulesKeyword_11_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_11__3__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__0"
    // InternalComposedLts.g:4091:1: rule__LtsDeclarationBody__Group_12__0 : rule__LtsDeclarationBody__Group_12__0__Impl rule__LtsDeclarationBody__Group_12__1 ;
    public final void rule__LtsDeclarationBody__Group_12__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4095:1: ( rule__LtsDeclarationBody__Group_12__0__Impl rule__LtsDeclarationBody__Group_12__1 )
            // InternalComposedLts.g:4096:2: rule__LtsDeclarationBody__Group_12__0__Impl rule__LtsDeclarationBody__Group_12__1
            {
            pushFollow(FOLLOW_20);
            rule__LtsDeclarationBody__Group_12__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_12__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__0"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__0__Impl"
    // InternalComposedLts.g:4103:1: rule__LtsDeclarationBody__Group_12__0__Impl : ( 'labels' ) ;
    public final void rule__LtsDeclarationBody__Group_12__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4107:1: ( ( 'labels' ) )
            // InternalComposedLts.g:4108:1: ( 'labels' )
            {
            // InternalComposedLts.g:4108:1: ( 'labels' )
            // InternalComposedLts.g:4109:1: 'labels'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getLabelsKeyword_12_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getLabelsKeyword_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__0__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__1"
    // InternalComposedLts.g:4122:1: rule__LtsDeclarationBody__Group_12__1 : rule__LtsDeclarationBody__Group_12__1__Impl rule__LtsDeclarationBody__Group_12__2 ;
    public final void rule__LtsDeclarationBody__Group_12__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4126:1: ( rule__LtsDeclarationBody__Group_12__1__Impl rule__LtsDeclarationBody__Group_12__2 )
            // InternalComposedLts.g:4127:2: rule__LtsDeclarationBody__Group_12__1__Impl rule__LtsDeclarationBody__Group_12__2
            {
            pushFollow(FOLLOW_26);
            rule__LtsDeclarationBody__Group_12__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_12__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__1"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__1__Impl"
    // InternalComposedLts.g:4134:1: rule__LtsDeclarationBody__Group_12__1__Impl : ( ':' ) ;
    public final void rule__LtsDeclarationBody__Group_12__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4138:1: ( ( ':' ) )
            // InternalComposedLts.g:4139:1: ( ':' )
            {
            // InternalComposedLts.g:4139:1: ( ':' )
            // InternalComposedLts.g:4140:1: ':'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_12_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_12_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__1__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__2"
    // InternalComposedLts.g:4153:1: rule__LtsDeclarationBody__Group_12__2 : rule__LtsDeclarationBody__Group_12__2__Impl rule__LtsDeclarationBody__Group_12__3 ;
    public final void rule__LtsDeclarationBody__Group_12__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4157:1: ( rule__LtsDeclarationBody__Group_12__2__Impl rule__LtsDeclarationBody__Group_12__3 )
            // InternalComposedLts.g:4158:2: rule__LtsDeclarationBody__Group_12__2__Impl rule__LtsDeclarationBody__Group_12__3
            {
            pushFollow(FOLLOW_26);
            rule__LtsDeclarationBody__Group_12__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_12__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__2"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__2__Impl"
    // InternalComposedLts.g:4165:1: rule__LtsDeclarationBody__Group_12__2__Impl : ( ( rule__LtsDeclarationBody__LabelsAssignment_12_2 )* ) ;
    public final void rule__LtsDeclarationBody__Group_12__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4169:1: ( ( ( rule__LtsDeclarationBody__LabelsAssignment_12_2 )* ) )
            // InternalComposedLts.g:4170:1: ( ( rule__LtsDeclarationBody__LabelsAssignment_12_2 )* )
            {
            // InternalComposedLts.g:4170:1: ( ( rule__LtsDeclarationBody__LabelsAssignment_12_2 )* )
            // InternalComposedLts.g:4171:1: ( rule__LtsDeclarationBody__LabelsAssignment_12_2 )*
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getLabelsAssignment_12_2()); 
            // InternalComposedLts.g:4172:1: ( rule__LtsDeclarationBody__LabelsAssignment_12_2 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==RULE_ID) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalComposedLts.g:4172:2: rule__LtsDeclarationBody__LabelsAssignment_12_2
            	    {
            	    pushFollow(FOLLOW_25);
            	    rule__LtsDeclarationBody__LabelsAssignment_12_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getLtsDeclarationBodyAccess().getLabelsAssignment_12_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__2__Impl"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__3"
    // InternalComposedLts.g:4182:1: rule__LtsDeclarationBody__Group_12__3 : rule__LtsDeclarationBody__Group_12__3__Impl ;
    public final void rule__LtsDeclarationBody__Group_12__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4186:1: ( rule__LtsDeclarationBody__Group_12__3__Impl )
            // InternalComposedLts.g:4187:2: rule__LtsDeclarationBody__Group_12__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsDeclarationBody__Group_12__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__3"


    // $ANTLR start "rule__LtsDeclarationBody__Group_12__3__Impl"
    // InternalComposedLts.g:4193:1: rule__LtsDeclarationBody__Group_12__3__Impl : ( 'endlabels' ) ;
    public final void rule__LtsDeclarationBody__Group_12__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4197:1: ( ( 'endlabels' ) )
            // InternalComposedLts.g:4198:1: ( 'endlabels' )
            {
            // InternalComposedLts.g:4198:1: ( 'endlabels' )
            // InternalComposedLts.g:4199:1: 'endlabels'
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getEndlabelsKeyword_12_3()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getEndlabelsKeyword_12_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__Group_12__3__Impl"


    // $ANTLR start "rule__LtsRule__Group__0"
    // InternalComposedLts.g:4220:1: rule__LtsRule__Group__0 : rule__LtsRule__Group__0__Impl rule__LtsRule__Group__1 ;
    public final void rule__LtsRule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4224:1: ( rule__LtsRule__Group__0__Impl rule__LtsRule__Group__1 )
            // InternalComposedLts.g:4225:2: rule__LtsRule__Group__0__Impl rule__LtsRule__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__LtsRule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__0"


    // $ANTLR start "rule__LtsRule__Group__0__Impl"
    // InternalComposedLts.g:4232:1: rule__LtsRule__Group__0__Impl : ( ( rule__LtsRule__SrcAssignment_0 ) ) ;
    public final void rule__LtsRule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4236:1: ( ( ( rule__LtsRule__SrcAssignment_0 ) ) )
            // InternalComposedLts.g:4237:1: ( ( rule__LtsRule__SrcAssignment_0 ) )
            {
            // InternalComposedLts.g:4237:1: ( ( rule__LtsRule__SrcAssignment_0 ) )
            // InternalComposedLts.g:4238:1: ( rule__LtsRule__SrcAssignment_0 )
            {
             before(grammarAccess.getLtsRuleAccess().getSrcAssignment_0()); 
            // InternalComposedLts.g:4239:1: ( rule__LtsRule__SrcAssignment_0 )
            // InternalComposedLts.g:4239:2: rule__LtsRule__SrcAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__LtsRule__SrcAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLtsRuleAccess().getSrcAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__0__Impl"


    // $ANTLR start "rule__LtsRule__Group__1"
    // InternalComposedLts.g:4249:1: rule__LtsRule__Group__1 : rule__LtsRule__Group__1__Impl rule__LtsRule__Group__2 ;
    public final void rule__LtsRule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4253:1: ( rule__LtsRule__Group__1__Impl rule__LtsRule__Group__2 )
            // InternalComposedLts.g:4254:2: rule__LtsRule__Group__1__Impl rule__LtsRule__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__LtsRule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__1"


    // $ANTLR start "rule__LtsRule__Group__1__Impl"
    // InternalComposedLts.g:4261:1: rule__LtsRule__Group__1__Impl : ( '-' ) ;
    public final void rule__LtsRule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4265:1: ( ( '-' ) )
            // InternalComposedLts.g:4266:1: ( '-' )
            {
            // InternalComposedLts.g:4266:1: ( '-' )
            // InternalComposedLts.g:4267:1: '-'
            {
             before(grammarAccess.getLtsRuleAccess().getHyphenMinusKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getLtsRuleAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__1__Impl"


    // $ANTLR start "rule__LtsRule__Group__2"
    // InternalComposedLts.g:4280:1: rule__LtsRule__Group__2 : rule__LtsRule__Group__2__Impl rule__LtsRule__Group__3 ;
    public final void rule__LtsRule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4284:1: ( rule__LtsRule__Group__2__Impl rule__LtsRule__Group__3 )
            // InternalComposedLts.g:4285:2: rule__LtsRule__Group__2__Impl rule__LtsRule__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__LtsRule__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__2"


    // $ANTLR start "rule__LtsRule__Group__2__Impl"
    // InternalComposedLts.g:4292:1: rule__LtsRule__Group__2__Impl : ( ( rule__LtsRule__ActAssignment_2 ) ) ;
    public final void rule__LtsRule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4296:1: ( ( ( rule__LtsRule__ActAssignment_2 ) ) )
            // InternalComposedLts.g:4297:1: ( ( rule__LtsRule__ActAssignment_2 ) )
            {
            // InternalComposedLts.g:4297:1: ( ( rule__LtsRule__ActAssignment_2 ) )
            // InternalComposedLts.g:4298:1: ( rule__LtsRule__ActAssignment_2 )
            {
             before(grammarAccess.getLtsRuleAccess().getActAssignment_2()); 
            // InternalComposedLts.g:4299:1: ( rule__LtsRule__ActAssignment_2 )
            // InternalComposedLts.g:4299:2: rule__LtsRule__ActAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__LtsRule__ActAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLtsRuleAccess().getActAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__2__Impl"


    // $ANTLR start "rule__LtsRule__Group__3"
    // InternalComposedLts.g:4309:1: rule__LtsRule__Group__3 : rule__LtsRule__Group__3__Impl rule__LtsRule__Group__4 ;
    public final void rule__LtsRule__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4313:1: ( rule__LtsRule__Group__3__Impl rule__LtsRule__Group__4 )
            // InternalComposedLts.g:4314:2: rule__LtsRule__Group__3__Impl rule__LtsRule__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__LtsRule__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__3"


    // $ANTLR start "rule__LtsRule__Group__3__Impl"
    // InternalComposedLts.g:4321:1: rule__LtsRule__Group__3__Impl : ( '->' ) ;
    public final void rule__LtsRule__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4325:1: ( ( '->' ) )
            // InternalComposedLts.g:4326:1: ( '->' )
            {
            // InternalComposedLts.g:4326:1: ( '->' )
            // InternalComposedLts.g:4327:1: '->'
            {
             before(grammarAccess.getLtsRuleAccess().getHyphenMinusGreaterThanSignKeyword_3()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getLtsRuleAccess().getHyphenMinusGreaterThanSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__3__Impl"


    // $ANTLR start "rule__LtsRule__Group__4"
    // InternalComposedLts.g:4340:1: rule__LtsRule__Group__4 : rule__LtsRule__Group__4__Impl rule__LtsRule__Group__5 ;
    public final void rule__LtsRule__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4344:1: ( rule__LtsRule__Group__4__Impl rule__LtsRule__Group__5 )
            // InternalComposedLts.g:4345:2: rule__LtsRule__Group__4__Impl rule__LtsRule__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__LtsRule__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__4"


    // $ANTLR start "rule__LtsRule__Group__4__Impl"
    // InternalComposedLts.g:4352:1: rule__LtsRule__Group__4__Impl : ( ( rule__LtsRule__TrgAssignment_4 ) ) ;
    public final void rule__LtsRule__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4356:1: ( ( ( rule__LtsRule__TrgAssignment_4 ) ) )
            // InternalComposedLts.g:4357:1: ( ( rule__LtsRule__TrgAssignment_4 ) )
            {
            // InternalComposedLts.g:4357:1: ( ( rule__LtsRule__TrgAssignment_4 ) )
            // InternalComposedLts.g:4358:1: ( rule__LtsRule__TrgAssignment_4 )
            {
             before(grammarAccess.getLtsRuleAccess().getTrgAssignment_4()); 
            // InternalComposedLts.g:4359:1: ( rule__LtsRule__TrgAssignment_4 )
            // InternalComposedLts.g:4359:2: rule__LtsRule__TrgAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__LtsRule__TrgAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getLtsRuleAccess().getTrgAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__4__Impl"


    // $ANTLR start "rule__LtsRule__Group__5"
    // InternalComposedLts.g:4369:1: rule__LtsRule__Group__5 : rule__LtsRule__Group__5__Impl ;
    public final void rule__LtsRule__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4373:1: ( rule__LtsRule__Group__5__Impl )
            // InternalComposedLts.g:4374:2: rule__LtsRule__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtsRule__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__5"


    // $ANTLR start "rule__LtsRule__Group__5__Impl"
    // InternalComposedLts.g:4380:1: rule__LtsRule__Group__5__Impl : ( ';' ) ;
    public final void rule__LtsRule__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4384:1: ( ( ';' ) )
            // InternalComposedLts.g:4385:1: ( ';' )
            {
            // InternalComposedLts.g:4385:1: ( ';' )
            // InternalComposedLts.g:4386:1: ';'
            {
             before(grammarAccess.getLtsRuleAccess().getSemicolonKeyword_5()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLtsRuleAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__Group__5__Impl"


    // $ANTLR start "rule__LabelRule__Group__0"
    // InternalComposedLts.g:4411:1: rule__LabelRule__Group__0 : rule__LabelRule__Group__0__Impl rule__LabelRule__Group__1 ;
    public final void rule__LabelRule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4415:1: ( rule__LabelRule__Group__0__Impl rule__LabelRule__Group__1 )
            // InternalComposedLts.g:4416:2: rule__LabelRule__Group__0__Impl rule__LabelRule__Group__1
            {
            pushFollow(FOLLOW_20);
            rule__LabelRule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelRule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__0"


    // $ANTLR start "rule__LabelRule__Group__0__Impl"
    // InternalComposedLts.g:4423:1: rule__LabelRule__Group__0__Impl : ( ( rule__LabelRule__LabelAssignment_0 ) ) ;
    public final void rule__LabelRule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4427:1: ( ( ( rule__LabelRule__LabelAssignment_0 ) ) )
            // InternalComposedLts.g:4428:1: ( ( rule__LabelRule__LabelAssignment_0 ) )
            {
            // InternalComposedLts.g:4428:1: ( ( rule__LabelRule__LabelAssignment_0 ) )
            // InternalComposedLts.g:4429:1: ( rule__LabelRule__LabelAssignment_0 )
            {
             before(grammarAccess.getLabelRuleAccess().getLabelAssignment_0()); 
            // InternalComposedLts.g:4430:1: ( rule__LabelRule__LabelAssignment_0 )
            // InternalComposedLts.g:4430:2: rule__LabelRule__LabelAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__LabelAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLabelRuleAccess().getLabelAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__0__Impl"


    // $ANTLR start "rule__LabelRule__Group__1"
    // InternalComposedLts.g:4440:1: rule__LabelRule__Group__1 : rule__LabelRule__Group__1__Impl rule__LabelRule__Group__2 ;
    public final void rule__LabelRule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4444:1: ( rule__LabelRule__Group__1__Impl rule__LabelRule__Group__2 )
            // InternalComposedLts.g:4445:2: rule__LabelRule__Group__1__Impl rule__LabelRule__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__LabelRule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelRule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__1"


    // $ANTLR start "rule__LabelRule__Group__1__Impl"
    // InternalComposedLts.g:4452:1: rule__LabelRule__Group__1__Impl : ( ':' ) ;
    public final void rule__LabelRule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4456:1: ( ( ':' ) )
            // InternalComposedLts.g:4457:1: ( ':' )
            {
            // InternalComposedLts.g:4457:1: ( ':' )
            // InternalComposedLts.g:4458:1: ':'
            {
             before(grammarAccess.getLabelRuleAccess().getColonKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getLabelRuleAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__1__Impl"


    // $ANTLR start "rule__LabelRule__Group__2"
    // InternalComposedLts.g:4471:1: rule__LabelRule__Group__2 : rule__LabelRule__Group__2__Impl rule__LabelRule__Group__3 ;
    public final void rule__LabelRule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4475:1: ( rule__LabelRule__Group__2__Impl rule__LabelRule__Group__3 )
            // InternalComposedLts.g:4476:2: rule__LabelRule__Group__2__Impl rule__LabelRule__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__LabelRule__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelRule__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__2"


    // $ANTLR start "rule__LabelRule__Group__2__Impl"
    // InternalComposedLts.g:4483:1: rule__LabelRule__Group__2__Impl : ( ( rule__LabelRule__Group_2__0 ) ) ;
    public final void rule__LabelRule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4487:1: ( ( ( rule__LabelRule__Group_2__0 ) ) )
            // InternalComposedLts.g:4488:1: ( ( rule__LabelRule__Group_2__0 ) )
            {
            // InternalComposedLts.g:4488:1: ( ( rule__LabelRule__Group_2__0 ) )
            // InternalComposedLts.g:4489:1: ( rule__LabelRule__Group_2__0 )
            {
             before(grammarAccess.getLabelRuleAccess().getGroup_2()); 
            // InternalComposedLts.g:4490:1: ( rule__LabelRule__Group_2__0 )
            // InternalComposedLts.g:4490:2: rule__LabelRule__Group_2__0
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__Group_2__0();

            state._fsp--;


            }

             after(grammarAccess.getLabelRuleAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__2__Impl"


    // $ANTLR start "rule__LabelRule__Group__3"
    // InternalComposedLts.g:4500:1: rule__LabelRule__Group__3 : rule__LabelRule__Group__3__Impl ;
    public final void rule__LabelRule__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4504:1: ( rule__LabelRule__Group__3__Impl )
            // InternalComposedLts.g:4505:2: rule__LabelRule__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__3"


    // $ANTLR start "rule__LabelRule__Group__3__Impl"
    // InternalComposedLts.g:4511:1: rule__LabelRule__Group__3__Impl : ( ';' ) ;
    public final void rule__LabelRule__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4515:1: ( ( ';' ) )
            // InternalComposedLts.g:4516:1: ( ';' )
            {
            // InternalComposedLts.g:4516:1: ( ';' )
            // InternalComposedLts.g:4517:1: ';'
            {
             before(grammarAccess.getLabelRuleAccess().getSemicolonKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLabelRuleAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group__3__Impl"


    // $ANTLR start "rule__LabelRule__Group_2__0"
    // InternalComposedLts.g:4538:1: rule__LabelRule__Group_2__0 : rule__LabelRule__Group_2__0__Impl rule__LabelRule__Group_2__1 ;
    public final void rule__LabelRule__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4542:1: ( rule__LabelRule__Group_2__0__Impl rule__LabelRule__Group_2__1 )
            // InternalComposedLts.g:4543:2: rule__LabelRule__Group_2__0__Impl rule__LabelRule__Group_2__1
            {
            pushFollow(FOLLOW_13);
            rule__LabelRule__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelRule__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2__0"


    // $ANTLR start "rule__LabelRule__Group_2__0__Impl"
    // InternalComposedLts.g:4550:1: rule__LabelRule__Group_2__0__Impl : ( ( rule__LabelRule__StatesAssignment_2_0 ) ) ;
    public final void rule__LabelRule__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4554:1: ( ( ( rule__LabelRule__StatesAssignment_2_0 ) ) )
            // InternalComposedLts.g:4555:1: ( ( rule__LabelRule__StatesAssignment_2_0 ) )
            {
            // InternalComposedLts.g:4555:1: ( ( rule__LabelRule__StatesAssignment_2_0 ) )
            // InternalComposedLts.g:4556:1: ( rule__LabelRule__StatesAssignment_2_0 )
            {
             before(grammarAccess.getLabelRuleAccess().getStatesAssignment_2_0()); 
            // InternalComposedLts.g:4557:1: ( rule__LabelRule__StatesAssignment_2_0 )
            // InternalComposedLts.g:4557:2: rule__LabelRule__StatesAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__StatesAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getLabelRuleAccess().getStatesAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2__0__Impl"


    // $ANTLR start "rule__LabelRule__Group_2__1"
    // InternalComposedLts.g:4567:1: rule__LabelRule__Group_2__1 : rule__LabelRule__Group_2__1__Impl ;
    public final void rule__LabelRule__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4571:1: ( rule__LabelRule__Group_2__1__Impl )
            // InternalComposedLts.g:4572:2: rule__LabelRule__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2__1"


    // $ANTLR start "rule__LabelRule__Group_2__1__Impl"
    // InternalComposedLts.g:4578:1: rule__LabelRule__Group_2__1__Impl : ( ( rule__LabelRule__Group_2_1__0 )* ) ;
    public final void rule__LabelRule__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4582:1: ( ( ( rule__LabelRule__Group_2_1__0 )* ) )
            // InternalComposedLts.g:4583:1: ( ( rule__LabelRule__Group_2_1__0 )* )
            {
            // InternalComposedLts.g:4583:1: ( ( rule__LabelRule__Group_2_1__0 )* )
            // InternalComposedLts.g:4584:1: ( rule__LabelRule__Group_2_1__0 )*
            {
             before(grammarAccess.getLabelRuleAccess().getGroup_2_1()); 
            // InternalComposedLts.g:4585:1: ( rule__LabelRule__Group_2_1__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==18) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalComposedLts.g:4585:2: rule__LabelRule__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__LabelRule__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getLabelRuleAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2__1__Impl"


    // $ANTLR start "rule__LabelRule__Group_2_1__0"
    // InternalComposedLts.g:4599:1: rule__LabelRule__Group_2_1__0 : rule__LabelRule__Group_2_1__0__Impl rule__LabelRule__Group_2_1__1 ;
    public final void rule__LabelRule__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4603:1: ( rule__LabelRule__Group_2_1__0__Impl rule__LabelRule__Group_2_1__1 )
            // InternalComposedLts.g:4604:2: rule__LabelRule__Group_2_1__0__Impl rule__LabelRule__Group_2_1__1
            {
            pushFollow(FOLLOW_4);
            rule__LabelRule__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LabelRule__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2_1__0"


    // $ANTLR start "rule__LabelRule__Group_2_1__0__Impl"
    // InternalComposedLts.g:4611:1: rule__LabelRule__Group_2_1__0__Impl : ( ',' ) ;
    public final void rule__LabelRule__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4615:1: ( ( ',' ) )
            // InternalComposedLts.g:4616:1: ( ',' )
            {
            // InternalComposedLts.g:4616:1: ( ',' )
            // InternalComposedLts.g:4617:1: ','
            {
             before(grammarAccess.getLabelRuleAccess().getCommaKeyword_2_1_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getLabelRuleAccess().getCommaKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2_1__0__Impl"


    // $ANTLR start "rule__LabelRule__Group_2_1__1"
    // InternalComposedLts.g:4630:1: rule__LabelRule__Group_2_1__1 : rule__LabelRule__Group_2_1__1__Impl ;
    public final void rule__LabelRule__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4634:1: ( rule__LabelRule__Group_2_1__1__Impl )
            // InternalComposedLts.g:4635:2: rule__LabelRule__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2_1__1"


    // $ANTLR start "rule__LabelRule__Group_2_1__1__Impl"
    // InternalComposedLts.g:4641:1: rule__LabelRule__Group_2_1__1__Impl : ( ( rule__LabelRule__StatesAssignment_2_1_1 ) ) ;
    public final void rule__LabelRule__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4645:1: ( ( ( rule__LabelRule__StatesAssignment_2_1_1 ) ) )
            // InternalComposedLts.g:4646:1: ( ( rule__LabelRule__StatesAssignment_2_1_1 ) )
            {
            // InternalComposedLts.g:4646:1: ( ( rule__LabelRule__StatesAssignment_2_1_1 ) )
            // InternalComposedLts.g:4647:1: ( rule__LabelRule__StatesAssignment_2_1_1 )
            {
             before(grammarAccess.getLabelRuleAccess().getStatesAssignment_2_1_1()); 
            // InternalComposedLts.g:4648:1: ( rule__LabelRule__StatesAssignment_2_1_1 )
            // InternalComposedLts.g:4648:2: rule__LabelRule__StatesAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LabelRule__StatesAssignment_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLabelRuleAccess().getStatesAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__Group_2_1__1__Impl"


    // $ANTLR start "rule__Rule__Group__0"
    // InternalComposedLts.g:4664:1: rule__Rule__Group__0 : rule__Rule__Group__0__Impl rule__Rule__Group__1 ;
    public final void rule__Rule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4668:1: ( rule__Rule__Group__0__Impl rule__Rule__Group__1 )
            // InternalComposedLts.g:4669:2: rule__Rule__Group__0__Impl rule__Rule__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Rule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__0"


    // $ANTLR start "rule__Rule__Group__0__Impl"
    // InternalComposedLts.g:4676:1: rule__Rule__Group__0__Impl : ( ( rule__Rule__ActionAssignment_0 ) ) ;
    public final void rule__Rule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4680:1: ( ( ( rule__Rule__ActionAssignment_0 ) ) )
            // InternalComposedLts.g:4681:1: ( ( rule__Rule__ActionAssignment_0 ) )
            {
            // InternalComposedLts.g:4681:1: ( ( rule__Rule__ActionAssignment_0 ) )
            // InternalComposedLts.g:4682:1: ( rule__Rule__ActionAssignment_0 )
            {
             before(grammarAccess.getRuleAccess().getActionAssignment_0()); 
            // InternalComposedLts.g:4683:1: ( rule__Rule__ActionAssignment_0 )
            // InternalComposedLts.g:4683:2: rule__Rule__ActionAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Rule__ActionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getActionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__0__Impl"


    // $ANTLR start "rule__Rule__Group__1"
    // InternalComposedLts.g:4693:1: rule__Rule__Group__1 : rule__Rule__Group__1__Impl rule__Rule__Group__2 ;
    public final void rule__Rule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4697:1: ( rule__Rule__Group__1__Impl rule__Rule__Group__2 )
            // InternalComposedLts.g:4698:2: rule__Rule__Group__1__Impl rule__Rule__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Rule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Rule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__1"


    // $ANTLR start "rule__Rule__Group__1__Impl"
    // InternalComposedLts.g:4705:1: rule__Rule__Group__1__Impl : ( '->' ) ;
    public final void rule__Rule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4709:1: ( ( '->' ) )
            // InternalComposedLts.g:4710:1: ( '->' )
            {
            // InternalComposedLts.g:4710:1: ( '->' )
            // InternalComposedLts.g:4711:1: '->'
            {
             before(grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__1__Impl"


    // $ANTLR start "rule__Rule__Group__2"
    // InternalComposedLts.g:4724:1: rule__Rule__Group__2 : rule__Rule__Group__2__Impl ;
    public final void rule__Rule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4728:1: ( rule__Rule__Group__2__Impl )
            // InternalComposedLts.g:4729:2: rule__Rule__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Rule__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__2"


    // $ANTLR start "rule__Rule__Group__2__Impl"
    // InternalComposedLts.g:4735:1: rule__Rule__Group__2__Impl : ( ( rule__Rule__NextAssignment_2 ) ) ;
    public final void rule__Rule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4739:1: ( ( ( rule__Rule__NextAssignment_2 ) ) )
            // InternalComposedLts.g:4740:1: ( ( rule__Rule__NextAssignment_2 ) )
            {
            // InternalComposedLts.g:4740:1: ( ( rule__Rule__NextAssignment_2 ) )
            // InternalComposedLts.g:4741:1: ( rule__Rule__NextAssignment_2 )
            {
             before(grammarAccess.getRuleAccess().getNextAssignment_2()); 
            // InternalComposedLts.g:4742:1: ( rule__Rule__NextAssignment_2 )
            // InternalComposedLts.g:4742:2: rule__Rule__NextAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Rule__NextAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRuleAccess().getNextAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__Group__2__Impl"


    // $ANTLR start "rule__Label__Group__0"
    // InternalComposedLts.g:4758:1: rule__Label__Group__0 : rule__Label__Group__0__Impl rule__Label__Group__1 ;
    public final void rule__Label__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4762:1: ( rule__Label__Group__0__Impl rule__Label__Group__1 )
            // InternalComposedLts.g:4763:2: rule__Label__Group__0__Impl rule__Label__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Label__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Label__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__Group__0"


    // $ANTLR start "rule__Label__Group__0__Impl"
    // InternalComposedLts.g:4770:1: rule__Label__Group__0__Impl : ( 'label' ) ;
    public final void rule__Label__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4774:1: ( ( 'label' ) )
            // InternalComposedLts.g:4775:1: ( 'label' )
            {
            // InternalComposedLts.g:4775:1: ( 'label' )
            // InternalComposedLts.g:4776:1: 'label'
            {
             before(grammarAccess.getLabelAccess().getLabelKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getLabelAccess().getLabelKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__Group__0__Impl"


    // $ANTLR start "rule__Label__Group__1"
    // InternalComposedLts.g:4789:1: rule__Label__Group__1 : rule__Label__Group__1__Impl rule__Label__Group__2 ;
    public final void rule__Label__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4793:1: ( rule__Label__Group__1__Impl rule__Label__Group__2 )
            // InternalComposedLts.g:4794:2: rule__Label__Group__1__Impl rule__Label__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Label__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Label__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__Group__1"


    // $ANTLR start "rule__Label__Group__1__Impl"
    // InternalComposedLts.g:4801:1: rule__Label__Group__1__Impl : ( ( rule__Label__NameAssignment_1 ) ) ;
    public final void rule__Label__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4805:1: ( ( ( rule__Label__NameAssignment_1 ) ) )
            // InternalComposedLts.g:4806:1: ( ( rule__Label__NameAssignment_1 ) )
            {
            // InternalComposedLts.g:4806:1: ( ( rule__Label__NameAssignment_1 ) )
            // InternalComposedLts.g:4807:1: ( rule__Label__NameAssignment_1 )
            {
             before(grammarAccess.getLabelAccess().getNameAssignment_1()); 
            // InternalComposedLts.g:4808:1: ( rule__Label__NameAssignment_1 )
            // InternalComposedLts.g:4808:2: rule__Label__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Label__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLabelAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__Group__1__Impl"


    // $ANTLR start "rule__Label__Group__2"
    // InternalComposedLts.g:4818:1: rule__Label__Group__2 : rule__Label__Group__2__Impl ;
    public final void rule__Label__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4822:1: ( rule__Label__Group__2__Impl )
            // InternalComposedLts.g:4823:2: rule__Label__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Label__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__Group__2"


    // $ANTLR start "rule__Label__Group__2__Impl"
    // InternalComposedLts.g:4829:1: rule__Label__Group__2__Impl : ( ';' ) ;
    public final void rule__Label__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4833:1: ( ( ';' ) )
            // InternalComposedLts.g:4834:1: ( ';' )
            {
            // InternalComposedLts.g:4834:1: ( ';' )
            // InternalComposedLts.g:4835:1: ';'
            {
             before(grammarAccess.getLabelAccess().getSemicolonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLabelAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__Group__2__Impl"


    // $ANTLR start "rule__Action__Group__0"
    // InternalComposedLts.g:4854:1: rule__Action__Group__0 : rule__Action__Group__0__Impl rule__Action__Group__1 ;
    public final void rule__Action__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4858:1: ( rule__Action__Group__0__Impl rule__Action__Group__1 )
            // InternalComposedLts.g:4859:2: rule__Action__Group__0__Impl rule__Action__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Action__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0"


    // $ANTLR start "rule__Action__Group__0__Impl"
    // InternalComposedLts.g:4866:1: rule__Action__Group__0__Impl : ( 'action' ) ;
    public final void rule__Action__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4870:1: ( ( 'action' ) )
            // InternalComposedLts.g:4871:1: ( 'action' )
            {
            // InternalComposedLts.g:4871:1: ( 'action' )
            // InternalComposedLts.g:4872:1: 'action'
            {
             before(grammarAccess.getActionAccess().getActionKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getActionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__0__Impl"


    // $ANTLR start "rule__Action__Group__1"
    // InternalComposedLts.g:4885:1: rule__Action__Group__1 : rule__Action__Group__1__Impl rule__Action__Group__2 ;
    public final void rule__Action__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4889:1: ( rule__Action__Group__1__Impl rule__Action__Group__2 )
            // InternalComposedLts.g:4890:2: rule__Action__Group__1__Impl rule__Action__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Action__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Action__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1"


    // $ANTLR start "rule__Action__Group__1__Impl"
    // InternalComposedLts.g:4897:1: rule__Action__Group__1__Impl : ( ( rule__Action__NameAssignment_1 ) ) ;
    public final void rule__Action__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4901:1: ( ( ( rule__Action__NameAssignment_1 ) ) )
            // InternalComposedLts.g:4902:1: ( ( rule__Action__NameAssignment_1 ) )
            {
            // InternalComposedLts.g:4902:1: ( ( rule__Action__NameAssignment_1 ) )
            // InternalComposedLts.g:4903:1: ( rule__Action__NameAssignment_1 )
            {
             before(grammarAccess.getActionAccess().getNameAssignment_1()); 
            // InternalComposedLts.g:4904:1: ( rule__Action__NameAssignment_1 )
            // InternalComposedLts.g:4904:2: rule__Action__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Action__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getActionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__1__Impl"


    // $ANTLR start "rule__Action__Group__2"
    // InternalComposedLts.g:4914:1: rule__Action__Group__2 : rule__Action__Group__2__Impl ;
    public final void rule__Action__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4918:1: ( rule__Action__Group__2__Impl )
            // InternalComposedLts.g:4919:2: rule__Action__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Action__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__2"


    // $ANTLR start "rule__Action__Group__2__Impl"
    // InternalComposedLts.g:4925:1: rule__Action__Group__2__Impl : ( ';' ) ;
    public final void rule__Action__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4929:1: ( ( ';' ) )
            // InternalComposedLts.g:4930:1: ( ';' )
            {
            // InternalComposedLts.g:4930:1: ( ';' )
            // InternalComposedLts.g:4931:1: ';'
            {
             before(grammarAccess.getActionAccess().getSemicolonKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__Group__2__Impl"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__0"
    // InternalComposedLts.g:4950:1: rule__HmlFormulaDeclaration__Group__0 : rule__HmlFormulaDeclaration__Group__0__Impl rule__HmlFormulaDeclaration__Group__1 ;
    public final void rule__HmlFormulaDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4954:1: ( rule__HmlFormulaDeclaration__Group__0__Impl rule__HmlFormulaDeclaration__Group__1 )
            // InternalComposedLts.g:4955:2: rule__HmlFormulaDeclaration__Group__0__Impl rule__HmlFormulaDeclaration__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__HmlFormulaDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__0"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__0__Impl"
    // InternalComposedLts.g:4962:1: rule__HmlFormulaDeclaration__Group__0__Impl : ( 'hml' ) ;
    public final void rule__HmlFormulaDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4966:1: ( ( 'hml' ) )
            // InternalComposedLts.g:4967:1: ( 'hml' )
            {
            // InternalComposedLts.g:4967:1: ( 'hml' )
            // InternalComposedLts.g:4968:1: 'hml'
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getHmlKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getHmlFormulaDeclarationAccess().getHmlKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__0__Impl"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__1"
    // InternalComposedLts.g:4981:1: rule__HmlFormulaDeclaration__Group__1 : rule__HmlFormulaDeclaration__Group__1__Impl rule__HmlFormulaDeclaration__Group__2 ;
    public final void rule__HmlFormulaDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4985:1: ( rule__HmlFormulaDeclaration__Group__1__Impl rule__HmlFormulaDeclaration__Group__2 )
            // InternalComposedLts.g:4986:2: rule__HmlFormulaDeclaration__Group__1__Impl rule__HmlFormulaDeclaration__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__HmlFormulaDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__1"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__1__Impl"
    // InternalComposedLts.g:4993:1: rule__HmlFormulaDeclaration__Group__1__Impl : ( 'formula' ) ;
    public final void rule__HmlFormulaDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:4997:1: ( ( 'formula' ) )
            // InternalComposedLts.g:4998:1: ( 'formula' )
            {
            // InternalComposedLts.g:4998:1: ( 'formula' )
            // InternalComposedLts.g:4999:1: 'formula'
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaKeyword_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__1__Impl"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__2"
    // InternalComposedLts.g:5012:1: rule__HmlFormulaDeclaration__Group__2 : rule__HmlFormulaDeclaration__Group__2__Impl rule__HmlFormulaDeclaration__Group__3 ;
    public final void rule__HmlFormulaDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5016:1: ( rule__HmlFormulaDeclaration__Group__2__Impl rule__HmlFormulaDeclaration__Group__3 )
            // InternalComposedLts.g:5017:2: rule__HmlFormulaDeclaration__Group__2__Impl rule__HmlFormulaDeclaration__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__HmlFormulaDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__2"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__2__Impl"
    // InternalComposedLts.g:5024:1: rule__HmlFormulaDeclaration__Group__2__Impl : ( ( rule__HmlFormulaDeclaration__NameAssignment_2 ) ) ;
    public final void rule__HmlFormulaDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5028:1: ( ( ( rule__HmlFormulaDeclaration__NameAssignment_2 ) ) )
            // InternalComposedLts.g:5029:1: ( ( rule__HmlFormulaDeclaration__NameAssignment_2 ) )
            {
            // InternalComposedLts.g:5029:1: ( ( rule__HmlFormulaDeclaration__NameAssignment_2 ) )
            // InternalComposedLts.g:5030:1: ( rule__HmlFormulaDeclaration__NameAssignment_2 )
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getNameAssignment_2()); 
            // InternalComposedLts.g:5031:1: ( rule__HmlFormulaDeclaration__NameAssignment_2 )
            // InternalComposedLts.g:5031:2: rule__HmlFormulaDeclaration__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getHmlFormulaDeclarationAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__2__Impl"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__3"
    // InternalComposedLts.g:5041:1: rule__HmlFormulaDeclaration__Group__3 : rule__HmlFormulaDeclaration__Group__3__Impl rule__HmlFormulaDeclaration__Group__4 ;
    public final void rule__HmlFormulaDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5045:1: ( rule__HmlFormulaDeclaration__Group__3__Impl rule__HmlFormulaDeclaration__Group__4 )
            // InternalComposedLts.g:5046:2: rule__HmlFormulaDeclaration__Group__3__Impl rule__HmlFormulaDeclaration__Group__4
            {
            pushFollow(FOLLOW_30);
            rule__HmlFormulaDeclaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__3"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__3__Impl"
    // InternalComposedLts.g:5053:1: rule__HmlFormulaDeclaration__Group__3__Impl : ( '=' ) ;
    public final void rule__HmlFormulaDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5057:1: ( ( '=' ) )
            // InternalComposedLts.g:5058:1: ( '=' )
            {
            // InternalComposedLts.g:5058:1: ( '=' )
            // InternalComposedLts.g:5059:1: '='
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getEqualsSignKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getHmlFormulaDeclarationAccess().getEqualsSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__3__Impl"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__4"
    // InternalComposedLts.g:5072:1: rule__HmlFormulaDeclaration__Group__4 : rule__HmlFormulaDeclaration__Group__4__Impl rule__HmlFormulaDeclaration__Group__5 ;
    public final void rule__HmlFormulaDeclaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5076:1: ( rule__HmlFormulaDeclaration__Group__4__Impl rule__HmlFormulaDeclaration__Group__5 )
            // InternalComposedLts.g:5077:2: rule__HmlFormulaDeclaration__Group__4__Impl rule__HmlFormulaDeclaration__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__HmlFormulaDeclaration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__4"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__4__Impl"
    // InternalComposedLts.g:5084:1: rule__HmlFormulaDeclaration__Group__4__Impl : ( ( rule__HmlFormulaDeclaration__FormulaAssignment_4 ) ) ;
    public final void rule__HmlFormulaDeclaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5088:1: ( ( ( rule__HmlFormulaDeclaration__FormulaAssignment_4 ) ) )
            // InternalComposedLts.g:5089:1: ( ( rule__HmlFormulaDeclaration__FormulaAssignment_4 ) )
            {
            // InternalComposedLts.g:5089:1: ( ( rule__HmlFormulaDeclaration__FormulaAssignment_4 ) )
            // InternalComposedLts.g:5090:1: ( rule__HmlFormulaDeclaration__FormulaAssignment_4 )
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaAssignment_4()); 
            // InternalComposedLts.g:5091:1: ( rule__HmlFormulaDeclaration__FormulaAssignment_4 )
            // InternalComposedLts.g:5091:2: rule__HmlFormulaDeclaration__FormulaAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__FormulaAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__4__Impl"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__5"
    // InternalComposedLts.g:5101:1: rule__HmlFormulaDeclaration__Group__5 : rule__HmlFormulaDeclaration__Group__5__Impl ;
    public final void rule__HmlFormulaDeclaration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5105:1: ( rule__HmlFormulaDeclaration__Group__5__Impl )
            // InternalComposedLts.g:5106:2: rule__HmlFormulaDeclaration__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlFormulaDeclaration__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__5"


    // $ANTLR start "rule__HmlFormulaDeclaration__Group__5__Impl"
    // InternalComposedLts.g:5112:1: rule__HmlFormulaDeclaration__Group__5__Impl : ( ';' ) ;
    public final void rule__HmlFormulaDeclaration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5116:1: ( ( ';' ) )
            // InternalComposedLts.g:5117:1: ( ';' )
            {
            // InternalComposedLts.g:5117:1: ( ';' )
            // InternalComposedLts.g:5118:1: ';'
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getSemicolonKeyword_5()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getHmlFormulaDeclarationAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__Group__5__Impl"


    // $ANTLR start "rule__HmlOrFormula__Group__0"
    // InternalComposedLts.g:5143:1: rule__HmlOrFormula__Group__0 : rule__HmlOrFormula__Group__0__Impl rule__HmlOrFormula__Group__1 ;
    public final void rule__HmlOrFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5147:1: ( rule__HmlOrFormula__Group__0__Impl rule__HmlOrFormula__Group__1 )
            // InternalComposedLts.g:5148:2: rule__HmlOrFormula__Group__0__Impl rule__HmlOrFormula__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__HmlOrFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group__0"


    // $ANTLR start "rule__HmlOrFormula__Group__0__Impl"
    // InternalComposedLts.g:5155:1: rule__HmlOrFormula__Group__0__Impl : ( ruleHmlAndFormula ) ;
    public final void rule__HmlOrFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5159:1: ( ( ruleHmlAndFormula ) )
            // InternalComposedLts.g:5160:1: ( ruleHmlAndFormula )
            {
            // InternalComposedLts.g:5160:1: ( ruleHmlAndFormula )
            // InternalComposedLts.g:5161:1: ruleHmlAndFormula
            {
             before(grammarAccess.getHmlOrFormulaAccess().getHmlAndFormulaParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlAndFormula();

            state._fsp--;

             after(grammarAccess.getHmlOrFormulaAccess().getHmlAndFormulaParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group__0__Impl"


    // $ANTLR start "rule__HmlOrFormula__Group__1"
    // InternalComposedLts.g:5172:1: rule__HmlOrFormula__Group__1 : rule__HmlOrFormula__Group__1__Impl ;
    public final void rule__HmlOrFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5176:1: ( rule__HmlOrFormula__Group__1__Impl )
            // InternalComposedLts.g:5177:2: rule__HmlOrFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group__1"


    // $ANTLR start "rule__HmlOrFormula__Group__1__Impl"
    // InternalComposedLts.g:5183:1: rule__HmlOrFormula__Group__1__Impl : ( ( rule__HmlOrFormula__Group_1__0 )* ) ;
    public final void rule__HmlOrFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5187:1: ( ( ( rule__HmlOrFormula__Group_1__0 )* ) )
            // InternalComposedLts.g:5188:1: ( ( rule__HmlOrFormula__Group_1__0 )* )
            {
            // InternalComposedLts.g:5188:1: ( ( rule__HmlOrFormula__Group_1__0 )* )
            // InternalComposedLts.g:5189:1: ( rule__HmlOrFormula__Group_1__0 )*
            {
             before(grammarAccess.getHmlOrFormulaAccess().getGroup_1()); 
            // InternalComposedLts.g:5190:1: ( rule__HmlOrFormula__Group_1__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==17) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalComposedLts.g:5190:2: rule__HmlOrFormula__Group_1__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__HmlOrFormula__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getHmlOrFormulaAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group__1__Impl"


    // $ANTLR start "rule__HmlOrFormula__Group_1__0"
    // InternalComposedLts.g:5204:1: rule__HmlOrFormula__Group_1__0 : rule__HmlOrFormula__Group_1__0__Impl rule__HmlOrFormula__Group_1__1 ;
    public final void rule__HmlOrFormula__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5208:1: ( rule__HmlOrFormula__Group_1__0__Impl rule__HmlOrFormula__Group_1__1 )
            // InternalComposedLts.g:5209:2: rule__HmlOrFormula__Group_1__0__Impl rule__HmlOrFormula__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__HmlOrFormula__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group_1__0"


    // $ANTLR start "rule__HmlOrFormula__Group_1__0__Impl"
    // InternalComposedLts.g:5216:1: rule__HmlOrFormula__Group_1__0__Impl : ( () ) ;
    public final void rule__HmlOrFormula__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5220:1: ( ( () ) )
            // InternalComposedLts.g:5221:1: ( () )
            {
            // InternalComposedLts.g:5221:1: ( () )
            // InternalComposedLts.g:5222:1: ()
            {
             before(grammarAccess.getHmlOrFormulaAccess().getHmlOrFormulaLeftAction_1_0()); 
            // InternalComposedLts.g:5223:1: ()
            // InternalComposedLts.g:5225:1: 
            {
            }

             after(grammarAccess.getHmlOrFormulaAccess().getHmlOrFormulaLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group_1__0__Impl"


    // $ANTLR start "rule__HmlOrFormula__Group_1__1"
    // InternalComposedLts.g:5235:1: rule__HmlOrFormula__Group_1__1 : rule__HmlOrFormula__Group_1__1__Impl rule__HmlOrFormula__Group_1__2 ;
    public final void rule__HmlOrFormula__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5239:1: ( rule__HmlOrFormula__Group_1__1__Impl rule__HmlOrFormula__Group_1__2 )
            // InternalComposedLts.g:5240:2: rule__HmlOrFormula__Group_1__1__Impl rule__HmlOrFormula__Group_1__2
            {
            pushFollow(FOLLOW_30);
            rule__HmlOrFormula__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group_1__1"


    // $ANTLR start "rule__HmlOrFormula__Group_1__1__Impl"
    // InternalComposedLts.g:5247:1: rule__HmlOrFormula__Group_1__1__Impl : ( '|' ) ;
    public final void rule__HmlOrFormula__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5251:1: ( ( '|' ) )
            // InternalComposedLts.g:5252:1: ( '|' )
            {
            // InternalComposedLts.g:5252:1: ( '|' )
            // InternalComposedLts.g:5253:1: '|'
            {
             before(grammarAccess.getHmlOrFormulaAccess().getVerticalLineKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getHmlOrFormulaAccess().getVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group_1__1__Impl"


    // $ANTLR start "rule__HmlOrFormula__Group_1__2"
    // InternalComposedLts.g:5266:1: rule__HmlOrFormula__Group_1__2 : rule__HmlOrFormula__Group_1__2__Impl ;
    public final void rule__HmlOrFormula__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5270:1: ( rule__HmlOrFormula__Group_1__2__Impl )
            // InternalComposedLts.g:5271:2: rule__HmlOrFormula__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group_1__2"


    // $ANTLR start "rule__HmlOrFormula__Group_1__2__Impl"
    // InternalComposedLts.g:5277:1: rule__HmlOrFormula__Group_1__2__Impl : ( ( rule__HmlOrFormula__RightAssignment_1_2 ) ) ;
    public final void rule__HmlOrFormula__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5281:1: ( ( ( rule__HmlOrFormula__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:5282:1: ( ( rule__HmlOrFormula__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:5282:1: ( ( rule__HmlOrFormula__RightAssignment_1_2 ) )
            // InternalComposedLts.g:5283:1: ( rule__HmlOrFormula__RightAssignment_1_2 )
            {
             before(grammarAccess.getHmlOrFormulaAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:5284:1: ( rule__HmlOrFormula__RightAssignment_1_2 )
            // InternalComposedLts.g:5284:2: rule__HmlOrFormula__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__HmlOrFormula__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getHmlOrFormulaAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__Group_1__2__Impl"


    // $ANTLR start "rule__HmlAndFormula__Group__0"
    // InternalComposedLts.g:5300:1: rule__HmlAndFormula__Group__0 : rule__HmlAndFormula__Group__0__Impl rule__HmlAndFormula__Group__1 ;
    public final void rule__HmlAndFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5304:1: ( rule__HmlAndFormula__Group__0__Impl rule__HmlAndFormula__Group__1 )
            // InternalComposedLts.g:5305:2: rule__HmlAndFormula__Group__0__Impl rule__HmlAndFormula__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__HmlAndFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group__0"


    // $ANTLR start "rule__HmlAndFormula__Group__0__Impl"
    // InternalComposedLts.g:5312:1: rule__HmlAndFormula__Group__0__Impl : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlAndFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5316:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:5317:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:5317:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:5318:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlAndFormulaAccess().getHmlBaseFormulaParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlAndFormulaAccess().getHmlBaseFormulaParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group__0__Impl"


    // $ANTLR start "rule__HmlAndFormula__Group__1"
    // InternalComposedLts.g:5329:1: rule__HmlAndFormula__Group__1 : rule__HmlAndFormula__Group__1__Impl ;
    public final void rule__HmlAndFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5333:1: ( rule__HmlAndFormula__Group__1__Impl )
            // InternalComposedLts.g:5334:2: rule__HmlAndFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group__1"


    // $ANTLR start "rule__HmlAndFormula__Group__1__Impl"
    // InternalComposedLts.g:5340:1: rule__HmlAndFormula__Group__1__Impl : ( ( rule__HmlAndFormula__Group_1__0 )* ) ;
    public final void rule__HmlAndFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5344:1: ( ( ( rule__HmlAndFormula__Group_1__0 )* ) )
            // InternalComposedLts.g:5345:1: ( ( rule__HmlAndFormula__Group_1__0 )* )
            {
            // InternalComposedLts.g:5345:1: ( ( rule__HmlAndFormula__Group_1__0 )* )
            // InternalComposedLts.g:5346:1: ( rule__HmlAndFormula__Group_1__0 )*
            {
             before(grammarAccess.getHmlAndFormulaAccess().getGroup_1()); 
            // InternalComposedLts.g:5347:1: ( rule__HmlAndFormula__Group_1__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==36) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalComposedLts.g:5347:2: rule__HmlAndFormula__Group_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__HmlAndFormula__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getHmlAndFormulaAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group__1__Impl"


    // $ANTLR start "rule__HmlAndFormula__Group_1__0"
    // InternalComposedLts.g:5361:1: rule__HmlAndFormula__Group_1__0 : rule__HmlAndFormula__Group_1__0__Impl rule__HmlAndFormula__Group_1__1 ;
    public final void rule__HmlAndFormula__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5365:1: ( rule__HmlAndFormula__Group_1__0__Impl rule__HmlAndFormula__Group_1__1 )
            // InternalComposedLts.g:5366:2: rule__HmlAndFormula__Group_1__0__Impl rule__HmlAndFormula__Group_1__1
            {
            pushFollow(FOLLOW_32);
            rule__HmlAndFormula__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group_1__0"


    // $ANTLR start "rule__HmlAndFormula__Group_1__0__Impl"
    // InternalComposedLts.g:5373:1: rule__HmlAndFormula__Group_1__0__Impl : ( () ) ;
    public final void rule__HmlAndFormula__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5377:1: ( ( () ) )
            // InternalComposedLts.g:5378:1: ( () )
            {
            // InternalComposedLts.g:5378:1: ( () )
            // InternalComposedLts.g:5379:1: ()
            {
             before(grammarAccess.getHmlAndFormulaAccess().getHmlAndFormulaLeftAction_1_0()); 
            // InternalComposedLts.g:5380:1: ()
            // InternalComposedLts.g:5382:1: 
            {
            }

             after(grammarAccess.getHmlAndFormulaAccess().getHmlAndFormulaLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group_1__0__Impl"


    // $ANTLR start "rule__HmlAndFormula__Group_1__1"
    // InternalComposedLts.g:5392:1: rule__HmlAndFormula__Group_1__1 : rule__HmlAndFormula__Group_1__1__Impl rule__HmlAndFormula__Group_1__2 ;
    public final void rule__HmlAndFormula__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5396:1: ( rule__HmlAndFormula__Group_1__1__Impl rule__HmlAndFormula__Group_1__2 )
            // InternalComposedLts.g:5397:2: rule__HmlAndFormula__Group_1__1__Impl rule__HmlAndFormula__Group_1__2
            {
            pushFollow(FOLLOW_30);
            rule__HmlAndFormula__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group_1__1"


    // $ANTLR start "rule__HmlAndFormula__Group_1__1__Impl"
    // InternalComposedLts.g:5404:1: rule__HmlAndFormula__Group_1__1__Impl : ( '&' ) ;
    public final void rule__HmlAndFormula__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5408:1: ( ( '&' ) )
            // InternalComposedLts.g:5409:1: ( '&' )
            {
            // InternalComposedLts.g:5409:1: ( '&' )
            // InternalComposedLts.g:5410:1: '&'
            {
             before(grammarAccess.getHmlAndFormulaAccess().getAmpersandKeyword_1_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getHmlAndFormulaAccess().getAmpersandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group_1__1__Impl"


    // $ANTLR start "rule__HmlAndFormula__Group_1__2"
    // InternalComposedLts.g:5423:1: rule__HmlAndFormula__Group_1__2 : rule__HmlAndFormula__Group_1__2__Impl ;
    public final void rule__HmlAndFormula__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5427:1: ( rule__HmlAndFormula__Group_1__2__Impl )
            // InternalComposedLts.g:5428:2: rule__HmlAndFormula__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group_1__2"


    // $ANTLR start "rule__HmlAndFormula__Group_1__2__Impl"
    // InternalComposedLts.g:5434:1: rule__HmlAndFormula__Group_1__2__Impl : ( ( rule__HmlAndFormula__RightAssignment_1_2 ) ) ;
    public final void rule__HmlAndFormula__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5438:1: ( ( ( rule__HmlAndFormula__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:5439:1: ( ( rule__HmlAndFormula__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:5439:1: ( ( rule__HmlAndFormula__RightAssignment_1_2 ) )
            // InternalComposedLts.g:5440:1: ( rule__HmlAndFormula__RightAssignment_1_2 )
            {
             before(grammarAccess.getHmlAndFormulaAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:5441:1: ( rule__HmlAndFormula__RightAssignment_1_2 )
            // InternalComposedLts.g:5441:2: rule__HmlAndFormula__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__HmlAndFormula__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getHmlAndFormulaAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__Group_1__2__Impl"


    // $ANTLR start "rule__HmlBaseFormula__Group_2__0"
    // InternalComposedLts.g:5457:1: rule__HmlBaseFormula__Group_2__0 : rule__HmlBaseFormula__Group_2__0__Impl rule__HmlBaseFormula__Group_2__1 ;
    public final void rule__HmlBaseFormula__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5461:1: ( rule__HmlBaseFormula__Group_2__0__Impl rule__HmlBaseFormula__Group_2__1 )
            // InternalComposedLts.g:5462:2: rule__HmlBaseFormula__Group_2__0__Impl rule__HmlBaseFormula__Group_2__1
            {
            pushFollow(FOLLOW_30);
            rule__HmlBaseFormula__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlBaseFormula__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Group_2__0"


    // $ANTLR start "rule__HmlBaseFormula__Group_2__0__Impl"
    // InternalComposedLts.g:5469:1: rule__HmlBaseFormula__Group_2__0__Impl : ( '(' ) ;
    public final void rule__HmlBaseFormula__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5473:1: ( ( '(' ) )
            // InternalComposedLts.g:5474:1: ( '(' )
            {
            // InternalComposedLts.g:5474:1: ( '(' )
            // InternalComposedLts.g:5475:1: '('
            {
             before(grammarAccess.getHmlBaseFormulaAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getHmlBaseFormulaAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Group_2__0__Impl"


    // $ANTLR start "rule__HmlBaseFormula__Group_2__1"
    // InternalComposedLts.g:5488:1: rule__HmlBaseFormula__Group_2__1 : rule__HmlBaseFormula__Group_2__1__Impl rule__HmlBaseFormula__Group_2__2 ;
    public final void rule__HmlBaseFormula__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5492:1: ( rule__HmlBaseFormula__Group_2__1__Impl rule__HmlBaseFormula__Group_2__2 )
            // InternalComposedLts.g:5493:2: rule__HmlBaseFormula__Group_2__1__Impl rule__HmlBaseFormula__Group_2__2
            {
            pushFollow(FOLLOW_17);
            rule__HmlBaseFormula__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlBaseFormula__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Group_2__1"


    // $ANTLR start "rule__HmlBaseFormula__Group_2__1__Impl"
    // InternalComposedLts.g:5500:1: rule__HmlBaseFormula__Group_2__1__Impl : ( ruleHmlFormula ) ;
    public final void rule__HmlBaseFormula__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5504:1: ( ( ruleHmlFormula ) )
            // InternalComposedLts.g:5505:1: ( ruleHmlFormula )
            {
            // InternalComposedLts.g:5505:1: ( ruleHmlFormula )
            // InternalComposedLts.g:5506:1: ruleHmlFormula
            {
             before(grammarAccess.getHmlBaseFormulaAccess().getHmlFormulaParserRuleCall_2_1()); 
            pushFollow(FOLLOW_2);
            ruleHmlFormula();

            state._fsp--;

             after(grammarAccess.getHmlBaseFormulaAccess().getHmlFormulaParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Group_2__1__Impl"


    // $ANTLR start "rule__HmlBaseFormula__Group_2__2"
    // InternalComposedLts.g:5517:1: rule__HmlBaseFormula__Group_2__2 : rule__HmlBaseFormula__Group_2__2__Impl ;
    public final void rule__HmlBaseFormula__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5521:1: ( rule__HmlBaseFormula__Group_2__2__Impl )
            // InternalComposedLts.g:5522:2: rule__HmlBaseFormula__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlBaseFormula__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Group_2__2"


    // $ANTLR start "rule__HmlBaseFormula__Group_2__2__Impl"
    // InternalComposedLts.g:5528:1: rule__HmlBaseFormula__Group_2__2__Impl : ( ')' ) ;
    public final void rule__HmlBaseFormula__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5532:1: ( ( ')' ) )
            // InternalComposedLts.g:5533:1: ( ')' )
            {
            // InternalComposedLts.g:5533:1: ( ')' )
            // InternalComposedLts.g:5534:1: ')'
            {
             before(grammarAccess.getHmlBaseFormulaAccess().getRightParenthesisKeyword_2_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getHmlBaseFormulaAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBaseFormula__Group_2__2__Impl"


    // $ANTLR start "rule__HmlPredicate__Group__0"
    // InternalComposedLts.g:5553:1: rule__HmlPredicate__Group__0 : rule__HmlPredicate__Group__0__Impl rule__HmlPredicate__Group__1 ;
    public final void rule__HmlPredicate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5557:1: ( rule__HmlPredicate__Group__0__Impl rule__HmlPredicate__Group__1 )
            // InternalComposedLts.g:5558:2: rule__HmlPredicate__Group__0__Impl rule__HmlPredicate__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__HmlPredicate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlPredicate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__Group__0"


    // $ANTLR start "rule__HmlPredicate__Group__0__Impl"
    // InternalComposedLts.g:5565:1: rule__HmlPredicate__Group__0__Impl : ( '#[' ) ;
    public final void rule__HmlPredicate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5569:1: ( ( '#[' ) )
            // InternalComposedLts.g:5570:1: ( '#[' )
            {
            // InternalComposedLts.g:5570:1: ( '#[' )
            // InternalComposedLts.g:5571:1: '#['
            {
             before(grammarAccess.getHmlPredicateAccess().getNumberSignLeftSquareBracketKeyword_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getHmlPredicateAccess().getNumberSignLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__Group__0__Impl"


    // $ANTLR start "rule__HmlPredicate__Group__1"
    // InternalComposedLts.g:5584:1: rule__HmlPredicate__Group__1 : rule__HmlPredicate__Group__1__Impl rule__HmlPredicate__Group__2 ;
    public final void rule__HmlPredicate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5588:1: ( rule__HmlPredicate__Group__1__Impl rule__HmlPredicate__Group__2 )
            // InternalComposedLts.g:5589:2: rule__HmlPredicate__Group__1__Impl rule__HmlPredicate__Group__2
            {
            pushFollow(FOLLOW_34);
            rule__HmlPredicate__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlPredicate__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__Group__1"


    // $ANTLR start "rule__HmlPredicate__Group__1__Impl"
    // InternalComposedLts.g:5596:1: rule__HmlPredicate__Group__1__Impl : ( ( rule__HmlPredicate__LabelAssignment_1 ) ) ;
    public final void rule__HmlPredicate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5600:1: ( ( ( rule__HmlPredicate__LabelAssignment_1 ) ) )
            // InternalComposedLts.g:5601:1: ( ( rule__HmlPredicate__LabelAssignment_1 ) )
            {
            // InternalComposedLts.g:5601:1: ( ( rule__HmlPredicate__LabelAssignment_1 ) )
            // InternalComposedLts.g:5602:1: ( rule__HmlPredicate__LabelAssignment_1 )
            {
             before(grammarAccess.getHmlPredicateAccess().getLabelAssignment_1()); 
            // InternalComposedLts.g:5603:1: ( rule__HmlPredicate__LabelAssignment_1 )
            // InternalComposedLts.g:5603:2: rule__HmlPredicate__LabelAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__HmlPredicate__LabelAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getHmlPredicateAccess().getLabelAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__Group__1__Impl"


    // $ANTLR start "rule__HmlPredicate__Group__2"
    // InternalComposedLts.g:5613:1: rule__HmlPredicate__Group__2 : rule__HmlPredicate__Group__2__Impl ;
    public final void rule__HmlPredicate__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5617:1: ( rule__HmlPredicate__Group__2__Impl )
            // InternalComposedLts.g:5618:2: rule__HmlPredicate__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlPredicate__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__Group__2"


    // $ANTLR start "rule__HmlPredicate__Group__2__Impl"
    // InternalComposedLts.g:5624:1: rule__HmlPredicate__Group__2__Impl : ( ']' ) ;
    public final void rule__HmlPredicate__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5628:1: ( ( ']' ) )
            // InternalComposedLts.g:5629:1: ( ']' )
            {
            // InternalComposedLts.g:5629:1: ( ']' )
            // InternalComposedLts.g:5630:1: ']'
            {
             before(grammarAccess.getHmlPredicateAccess().getRightSquareBracketKeyword_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getHmlPredicateAccess().getRightSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__Group__2__Impl"


    // $ANTLR start "rule__HmlTrue__Group__0"
    // InternalComposedLts.g:5649:1: rule__HmlTrue__Group__0 : rule__HmlTrue__Group__0__Impl rule__HmlTrue__Group__1 ;
    public final void rule__HmlTrue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5653:1: ( rule__HmlTrue__Group__0__Impl rule__HmlTrue__Group__1 )
            // InternalComposedLts.g:5654:2: rule__HmlTrue__Group__0__Impl rule__HmlTrue__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__HmlTrue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlTrue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlTrue__Group__0"


    // $ANTLR start "rule__HmlTrue__Group__0__Impl"
    // InternalComposedLts.g:5661:1: rule__HmlTrue__Group__0__Impl : ( () ) ;
    public final void rule__HmlTrue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5665:1: ( ( () ) )
            // InternalComposedLts.g:5666:1: ( () )
            {
            // InternalComposedLts.g:5666:1: ( () )
            // InternalComposedLts.g:5667:1: ()
            {
             before(grammarAccess.getHmlTrueAccess().getHmlTrueAction_0()); 
            // InternalComposedLts.g:5668:1: ()
            // InternalComposedLts.g:5670:1: 
            {
            }

             after(grammarAccess.getHmlTrueAccess().getHmlTrueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlTrue__Group__0__Impl"


    // $ANTLR start "rule__HmlTrue__Group__1"
    // InternalComposedLts.g:5680:1: rule__HmlTrue__Group__1 : rule__HmlTrue__Group__1__Impl ;
    public final void rule__HmlTrue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5684:1: ( rule__HmlTrue__Group__1__Impl )
            // InternalComposedLts.g:5685:2: rule__HmlTrue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlTrue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlTrue__Group__1"


    // $ANTLR start "rule__HmlTrue__Group__1__Impl"
    // InternalComposedLts.g:5691:1: rule__HmlTrue__Group__1__Impl : ( 'true' ) ;
    public final void rule__HmlTrue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5695:1: ( ( 'true' ) )
            // InternalComposedLts.g:5696:1: ( 'true' )
            {
            // InternalComposedLts.g:5696:1: ( 'true' )
            // InternalComposedLts.g:5697:1: 'true'
            {
             before(grammarAccess.getHmlTrueAccess().getTrueKeyword_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getHmlTrueAccess().getTrueKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlTrue__Group__1__Impl"


    // $ANTLR start "rule__HmlFalse__Group__0"
    // InternalComposedLts.g:5714:1: rule__HmlFalse__Group__0 : rule__HmlFalse__Group__0__Impl rule__HmlFalse__Group__1 ;
    public final void rule__HmlFalse__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5718:1: ( rule__HmlFalse__Group__0__Impl rule__HmlFalse__Group__1 )
            // InternalComposedLts.g:5719:2: rule__HmlFalse__Group__0__Impl rule__HmlFalse__Group__1
            {
            pushFollow(FOLLOW_36);
            rule__HmlFalse__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlFalse__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFalse__Group__0"


    // $ANTLR start "rule__HmlFalse__Group__0__Impl"
    // InternalComposedLts.g:5726:1: rule__HmlFalse__Group__0__Impl : ( () ) ;
    public final void rule__HmlFalse__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5730:1: ( ( () ) )
            // InternalComposedLts.g:5731:1: ( () )
            {
            // InternalComposedLts.g:5731:1: ( () )
            // InternalComposedLts.g:5732:1: ()
            {
             before(grammarAccess.getHmlFalseAccess().getHmlFalseAction_0()); 
            // InternalComposedLts.g:5733:1: ()
            // InternalComposedLts.g:5735:1: 
            {
            }

             after(grammarAccess.getHmlFalseAccess().getHmlFalseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFalse__Group__0__Impl"


    // $ANTLR start "rule__HmlFalse__Group__1"
    // InternalComposedLts.g:5745:1: rule__HmlFalse__Group__1 : rule__HmlFalse__Group__1__Impl ;
    public final void rule__HmlFalse__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5749:1: ( rule__HmlFalse__Group__1__Impl )
            // InternalComposedLts.g:5750:2: rule__HmlFalse__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlFalse__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFalse__Group__1"


    // $ANTLR start "rule__HmlFalse__Group__1__Impl"
    // InternalComposedLts.g:5756:1: rule__HmlFalse__Group__1__Impl : ( 'false' ) ;
    public final void rule__HmlFalse__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5760:1: ( ( 'false' ) )
            // InternalComposedLts.g:5761:1: ( 'false' )
            {
            // InternalComposedLts.g:5761:1: ( 'false' )
            // InternalComposedLts.g:5762:1: 'false'
            {
             before(grammarAccess.getHmlFalseAccess().getFalseKeyword_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getHmlFalseAccess().getFalseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFalse__Group__1__Impl"


    // $ANTLR start "rule__HmlNotFormula__Group__0"
    // InternalComposedLts.g:5779:1: rule__HmlNotFormula__Group__0 : rule__HmlNotFormula__Group__0__Impl rule__HmlNotFormula__Group__1 ;
    public final void rule__HmlNotFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5783:1: ( rule__HmlNotFormula__Group__0__Impl rule__HmlNotFormula__Group__1 )
            // InternalComposedLts.g:5784:2: rule__HmlNotFormula__Group__0__Impl rule__HmlNotFormula__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__HmlNotFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlNotFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlNotFormula__Group__0"


    // $ANTLR start "rule__HmlNotFormula__Group__0__Impl"
    // InternalComposedLts.g:5791:1: rule__HmlNotFormula__Group__0__Impl : ( '!' ) ;
    public final void rule__HmlNotFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5795:1: ( ( '!' ) )
            // InternalComposedLts.g:5796:1: ( '!' )
            {
            // InternalComposedLts.g:5796:1: ( '!' )
            // InternalComposedLts.g:5797:1: '!'
            {
             before(grammarAccess.getHmlNotFormulaAccess().getExclamationMarkKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getHmlNotFormulaAccess().getExclamationMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlNotFormula__Group__0__Impl"


    // $ANTLR start "rule__HmlNotFormula__Group__1"
    // InternalComposedLts.g:5810:1: rule__HmlNotFormula__Group__1 : rule__HmlNotFormula__Group__1__Impl ;
    public final void rule__HmlNotFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5814:1: ( rule__HmlNotFormula__Group__1__Impl )
            // InternalComposedLts.g:5815:2: rule__HmlNotFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlNotFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlNotFormula__Group__1"


    // $ANTLR start "rule__HmlNotFormula__Group__1__Impl"
    // InternalComposedLts.g:5821:1: rule__HmlNotFormula__Group__1__Impl : ( ( rule__HmlNotFormula__ArgAssignment_1 ) ) ;
    public final void rule__HmlNotFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5825:1: ( ( ( rule__HmlNotFormula__ArgAssignment_1 ) ) )
            // InternalComposedLts.g:5826:1: ( ( rule__HmlNotFormula__ArgAssignment_1 ) )
            {
            // InternalComposedLts.g:5826:1: ( ( rule__HmlNotFormula__ArgAssignment_1 ) )
            // InternalComposedLts.g:5827:1: ( rule__HmlNotFormula__ArgAssignment_1 )
            {
             before(grammarAccess.getHmlNotFormulaAccess().getArgAssignment_1()); 
            // InternalComposedLts.g:5828:1: ( rule__HmlNotFormula__ArgAssignment_1 )
            // InternalComposedLts.g:5828:2: rule__HmlNotFormula__ArgAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__HmlNotFormula__ArgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getHmlNotFormulaAccess().getArgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlNotFormula__Group__1__Impl"


    // $ANTLR start "rule__HmlDiamondFormula__Group__0"
    // InternalComposedLts.g:5842:1: rule__HmlDiamondFormula__Group__0 : rule__HmlDiamondFormula__Group__0__Impl rule__HmlDiamondFormula__Group__1 ;
    public final void rule__HmlDiamondFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5846:1: ( rule__HmlDiamondFormula__Group__0__Impl rule__HmlDiamondFormula__Group__1 )
            // InternalComposedLts.g:5847:2: rule__HmlDiamondFormula__Group__0__Impl rule__HmlDiamondFormula__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__HmlDiamondFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__0"


    // $ANTLR start "rule__HmlDiamondFormula__Group__0__Impl"
    // InternalComposedLts.g:5854:1: rule__HmlDiamondFormula__Group__0__Impl : ( '<' ) ;
    public final void rule__HmlDiamondFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5858:1: ( ( '<' ) )
            // InternalComposedLts.g:5859:1: ( '<' )
            {
            // InternalComposedLts.g:5859:1: ( '<' )
            // InternalComposedLts.g:5860:1: '<'
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getLessThanSignKeyword_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getHmlDiamondFormulaAccess().getLessThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__0__Impl"


    // $ANTLR start "rule__HmlDiamondFormula__Group__1"
    // InternalComposedLts.g:5873:1: rule__HmlDiamondFormula__Group__1 : rule__HmlDiamondFormula__Group__1__Impl rule__HmlDiamondFormula__Group__2 ;
    public final void rule__HmlDiamondFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5877:1: ( rule__HmlDiamondFormula__Group__1__Impl rule__HmlDiamondFormula__Group__2 )
            // InternalComposedLts.g:5878:2: rule__HmlDiamondFormula__Group__1__Impl rule__HmlDiamondFormula__Group__2
            {
            pushFollow(FOLLOW_38);
            rule__HmlDiamondFormula__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__1"


    // $ANTLR start "rule__HmlDiamondFormula__Group__1__Impl"
    // InternalComposedLts.g:5885:1: rule__HmlDiamondFormula__Group__1__Impl : ( ( rule__HmlDiamondFormula__ActionAssignment_1 ) ) ;
    public final void rule__HmlDiamondFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5889:1: ( ( ( rule__HmlDiamondFormula__ActionAssignment_1 ) ) )
            // InternalComposedLts.g:5890:1: ( ( rule__HmlDiamondFormula__ActionAssignment_1 ) )
            {
            // InternalComposedLts.g:5890:1: ( ( rule__HmlDiamondFormula__ActionAssignment_1 ) )
            // InternalComposedLts.g:5891:1: ( rule__HmlDiamondFormula__ActionAssignment_1 )
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getActionAssignment_1()); 
            // InternalComposedLts.g:5892:1: ( rule__HmlDiamondFormula__ActionAssignment_1 )
            // InternalComposedLts.g:5892:2: rule__HmlDiamondFormula__ActionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__ActionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getHmlDiamondFormulaAccess().getActionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__1__Impl"


    // $ANTLR start "rule__HmlDiamondFormula__Group__2"
    // InternalComposedLts.g:5902:1: rule__HmlDiamondFormula__Group__2 : rule__HmlDiamondFormula__Group__2__Impl rule__HmlDiamondFormula__Group__3 ;
    public final void rule__HmlDiamondFormula__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5906:1: ( rule__HmlDiamondFormula__Group__2__Impl rule__HmlDiamondFormula__Group__3 )
            // InternalComposedLts.g:5907:2: rule__HmlDiamondFormula__Group__2__Impl rule__HmlDiamondFormula__Group__3
            {
            pushFollow(FOLLOW_30);
            rule__HmlDiamondFormula__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__2"


    // $ANTLR start "rule__HmlDiamondFormula__Group__2__Impl"
    // InternalComposedLts.g:5914:1: rule__HmlDiamondFormula__Group__2__Impl : ( '>' ) ;
    public final void rule__HmlDiamondFormula__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5918:1: ( ( '>' ) )
            // InternalComposedLts.g:5919:1: ( '>' )
            {
            // InternalComposedLts.g:5919:1: ( '>' )
            // InternalComposedLts.g:5920:1: '>'
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getGreaterThanSignKeyword_2()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getHmlDiamondFormulaAccess().getGreaterThanSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__2__Impl"


    // $ANTLR start "rule__HmlDiamondFormula__Group__3"
    // InternalComposedLts.g:5933:1: rule__HmlDiamondFormula__Group__3 : rule__HmlDiamondFormula__Group__3__Impl ;
    public final void rule__HmlDiamondFormula__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5937:1: ( rule__HmlDiamondFormula__Group__3__Impl )
            // InternalComposedLts.g:5938:2: rule__HmlDiamondFormula__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__3"


    // $ANTLR start "rule__HmlDiamondFormula__Group__3__Impl"
    // InternalComposedLts.g:5944:1: rule__HmlDiamondFormula__Group__3__Impl : ( ( rule__HmlDiamondFormula__ArgAssignment_3 ) ) ;
    public final void rule__HmlDiamondFormula__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5948:1: ( ( ( rule__HmlDiamondFormula__ArgAssignment_3 ) ) )
            // InternalComposedLts.g:5949:1: ( ( rule__HmlDiamondFormula__ArgAssignment_3 ) )
            {
            // InternalComposedLts.g:5949:1: ( ( rule__HmlDiamondFormula__ArgAssignment_3 ) )
            // InternalComposedLts.g:5950:1: ( rule__HmlDiamondFormula__ArgAssignment_3 )
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getArgAssignment_3()); 
            // InternalComposedLts.g:5951:1: ( rule__HmlDiamondFormula__ArgAssignment_3 )
            // InternalComposedLts.g:5951:2: rule__HmlDiamondFormula__ArgAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__HmlDiamondFormula__ArgAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getHmlDiamondFormulaAccess().getArgAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__Group__3__Impl"


    // $ANTLR start "rule__HmlBoxFormula__Group__0"
    // InternalComposedLts.g:5969:1: rule__HmlBoxFormula__Group__0 : rule__HmlBoxFormula__Group__0__Impl rule__HmlBoxFormula__Group__1 ;
    public final void rule__HmlBoxFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5973:1: ( rule__HmlBoxFormula__Group__0__Impl rule__HmlBoxFormula__Group__1 )
            // InternalComposedLts.g:5974:2: rule__HmlBoxFormula__Group__0__Impl rule__HmlBoxFormula__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__HmlBoxFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__0"


    // $ANTLR start "rule__HmlBoxFormula__Group__0__Impl"
    // InternalComposedLts.g:5981:1: rule__HmlBoxFormula__Group__0__Impl : ( '[' ) ;
    public final void rule__HmlBoxFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:5985:1: ( ( '[' ) )
            // InternalComposedLts.g:5986:1: ( '[' )
            {
            // InternalComposedLts.g:5986:1: ( '[' )
            // InternalComposedLts.g:5987:1: '['
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getLeftSquareBracketKeyword_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getHmlBoxFormulaAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__0__Impl"


    // $ANTLR start "rule__HmlBoxFormula__Group__1"
    // InternalComposedLts.g:6000:1: rule__HmlBoxFormula__Group__1 : rule__HmlBoxFormula__Group__1__Impl rule__HmlBoxFormula__Group__2 ;
    public final void rule__HmlBoxFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6004:1: ( rule__HmlBoxFormula__Group__1__Impl rule__HmlBoxFormula__Group__2 )
            // InternalComposedLts.g:6005:2: rule__HmlBoxFormula__Group__1__Impl rule__HmlBoxFormula__Group__2
            {
            pushFollow(FOLLOW_34);
            rule__HmlBoxFormula__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__1"


    // $ANTLR start "rule__HmlBoxFormula__Group__1__Impl"
    // InternalComposedLts.g:6012:1: rule__HmlBoxFormula__Group__1__Impl : ( ( rule__HmlBoxFormula__ActionAssignment_1 ) ) ;
    public final void rule__HmlBoxFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6016:1: ( ( ( rule__HmlBoxFormula__ActionAssignment_1 ) ) )
            // InternalComposedLts.g:6017:1: ( ( rule__HmlBoxFormula__ActionAssignment_1 ) )
            {
            // InternalComposedLts.g:6017:1: ( ( rule__HmlBoxFormula__ActionAssignment_1 ) )
            // InternalComposedLts.g:6018:1: ( rule__HmlBoxFormula__ActionAssignment_1 )
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getActionAssignment_1()); 
            // InternalComposedLts.g:6019:1: ( rule__HmlBoxFormula__ActionAssignment_1 )
            // InternalComposedLts.g:6019:2: rule__HmlBoxFormula__ActionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__ActionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getHmlBoxFormulaAccess().getActionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__1__Impl"


    // $ANTLR start "rule__HmlBoxFormula__Group__2"
    // InternalComposedLts.g:6029:1: rule__HmlBoxFormula__Group__2 : rule__HmlBoxFormula__Group__2__Impl rule__HmlBoxFormula__Group__3 ;
    public final void rule__HmlBoxFormula__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6033:1: ( rule__HmlBoxFormula__Group__2__Impl rule__HmlBoxFormula__Group__3 )
            // InternalComposedLts.g:6034:2: rule__HmlBoxFormula__Group__2__Impl rule__HmlBoxFormula__Group__3
            {
            pushFollow(FOLLOW_30);
            rule__HmlBoxFormula__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__2"


    // $ANTLR start "rule__HmlBoxFormula__Group__2__Impl"
    // InternalComposedLts.g:6041:1: rule__HmlBoxFormula__Group__2__Impl : ( ']' ) ;
    public final void rule__HmlBoxFormula__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6045:1: ( ( ']' ) )
            // InternalComposedLts.g:6046:1: ( ']' )
            {
            // InternalComposedLts.g:6046:1: ( ']' )
            // InternalComposedLts.g:6047:1: ']'
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getRightSquareBracketKeyword_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getHmlBoxFormulaAccess().getRightSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__2__Impl"


    // $ANTLR start "rule__HmlBoxFormula__Group__3"
    // InternalComposedLts.g:6060:1: rule__HmlBoxFormula__Group__3 : rule__HmlBoxFormula__Group__3__Impl ;
    public final void rule__HmlBoxFormula__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6064:1: ( rule__HmlBoxFormula__Group__3__Impl )
            // InternalComposedLts.g:6065:2: rule__HmlBoxFormula__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__3"


    // $ANTLR start "rule__HmlBoxFormula__Group__3__Impl"
    // InternalComposedLts.g:6071:1: rule__HmlBoxFormula__Group__3__Impl : ( ( rule__HmlBoxFormula__ArgAssignment_3 ) ) ;
    public final void rule__HmlBoxFormula__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6075:1: ( ( ( rule__HmlBoxFormula__ArgAssignment_3 ) ) )
            // InternalComposedLts.g:6076:1: ( ( rule__HmlBoxFormula__ArgAssignment_3 ) )
            {
            // InternalComposedLts.g:6076:1: ( ( rule__HmlBoxFormula__ArgAssignment_3 ) )
            // InternalComposedLts.g:6077:1: ( rule__HmlBoxFormula__ArgAssignment_3 )
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getArgAssignment_3()); 
            // InternalComposedLts.g:6078:1: ( rule__HmlBoxFormula__ArgAssignment_3 )
            // InternalComposedLts.g:6078:2: rule__HmlBoxFormula__ArgAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__HmlBoxFormula__ArgAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getHmlBoxFormulaAccess().getArgAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__Group__3__Impl"


    // $ANTLR start "rule__HmlMinFixPoint__Group__0"
    // InternalComposedLts.g:6096:1: rule__HmlMinFixPoint__Group__0 : rule__HmlMinFixPoint__Group__0__Impl rule__HmlMinFixPoint__Group__1 ;
    public final void rule__HmlMinFixPoint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6100:1: ( rule__HmlMinFixPoint__Group__0__Impl rule__HmlMinFixPoint__Group__1 )
            // InternalComposedLts.g:6101:2: rule__HmlMinFixPoint__Group__0__Impl rule__HmlMinFixPoint__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__HmlMinFixPoint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__0"


    // $ANTLR start "rule__HmlMinFixPoint__Group__0__Impl"
    // InternalComposedLts.g:6108:1: rule__HmlMinFixPoint__Group__0__Impl : ( 'min' ) ;
    public final void rule__HmlMinFixPoint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6112:1: ( ( 'min' ) )
            // InternalComposedLts.g:6113:1: ( 'min' )
            {
            // InternalComposedLts.g:6113:1: ( 'min' )
            // InternalComposedLts.g:6114:1: 'min'
            {
             before(grammarAccess.getHmlMinFixPointAccess().getMinKeyword_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getHmlMinFixPointAccess().getMinKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__0__Impl"


    // $ANTLR start "rule__HmlMinFixPoint__Group__1"
    // InternalComposedLts.g:6127:1: rule__HmlMinFixPoint__Group__1 : rule__HmlMinFixPoint__Group__1__Impl rule__HmlMinFixPoint__Group__2 ;
    public final void rule__HmlMinFixPoint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6131:1: ( rule__HmlMinFixPoint__Group__1__Impl rule__HmlMinFixPoint__Group__2 )
            // InternalComposedLts.g:6132:2: rule__HmlMinFixPoint__Group__1__Impl rule__HmlMinFixPoint__Group__2
            {
            pushFollow(FOLLOW_39);
            rule__HmlMinFixPoint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__1"


    // $ANTLR start "rule__HmlMinFixPoint__Group__1__Impl"
    // InternalComposedLts.g:6139:1: rule__HmlMinFixPoint__Group__1__Impl : ( ( rule__HmlMinFixPoint__NameAssignment_1 ) ) ;
    public final void rule__HmlMinFixPoint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6143:1: ( ( ( rule__HmlMinFixPoint__NameAssignment_1 ) ) )
            // InternalComposedLts.g:6144:1: ( ( rule__HmlMinFixPoint__NameAssignment_1 ) )
            {
            // InternalComposedLts.g:6144:1: ( ( rule__HmlMinFixPoint__NameAssignment_1 ) )
            // InternalComposedLts.g:6145:1: ( rule__HmlMinFixPoint__NameAssignment_1 )
            {
             before(grammarAccess.getHmlMinFixPointAccess().getNameAssignment_1()); 
            // InternalComposedLts.g:6146:1: ( rule__HmlMinFixPoint__NameAssignment_1 )
            // InternalComposedLts.g:6146:2: rule__HmlMinFixPoint__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getHmlMinFixPointAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__1__Impl"


    // $ANTLR start "rule__HmlMinFixPoint__Group__2"
    // InternalComposedLts.g:6156:1: rule__HmlMinFixPoint__Group__2 : rule__HmlMinFixPoint__Group__2__Impl rule__HmlMinFixPoint__Group__3 ;
    public final void rule__HmlMinFixPoint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6160:1: ( rule__HmlMinFixPoint__Group__2__Impl rule__HmlMinFixPoint__Group__3 )
            // InternalComposedLts.g:6161:2: rule__HmlMinFixPoint__Group__2__Impl rule__HmlMinFixPoint__Group__3
            {
            pushFollow(FOLLOW_30);
            rule__HmlMinFixPoint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__2"


    // $ANTLR start "rule__HmlMinFixPoint__Group__2__Impl"
    // InternalComposedLts.g:6168:1: rule__HmlMinFixPoint__Group__2__Impl : ( '.' ) ;
    public final void rule__HmlMinFixPoint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6172:1: ( ( '.' ) )
            // InternalComposedLts.g:6173:1: ( '.' )
            {
            // InternalComposedLts.g:6173:1: ( '.' )
            // InternalComposedLts.g:6174:1: '.'
            {
             before(grammarAccess.getHmlMinFixPointAccess().getFullStopKeyword_2()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getHmlMinFixPointAccess().getFullStopKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__2__Impl"


    // $ANTLR start "rule__HmlMinFixPoint__Group__3"
    // InternalComposedLts.g:6187:1: rule__HmlMinFixPoint__Group__3 : rule__HmlMinFixPoint__Group__3__Impl ;
    public final void rule__HmlMinFixPoint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6191:1: ( rule__HmlMinFixPoint__Group__3__Impl )
            // InternalComposedLts.g:6192:2: rule__HmlMinFixPoint__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__3"


    // $ANTLR start "rule__HmlMinFixPoint__Group__3__Impl"
    // InternalComposedLts.g:6198:1: rule__HmlMinFixPoint__Group__3__Impl : ( ( rule__HmlMinFixPoint__ArgAssignment_3 ) ) ;
    public final void rule__HmlMinFixPoint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6202:1: ( ( ( rule__HmlMinFixPoint__ArgAssignment_3 ) ) )
            // InternalComposedLts.g:6203:1: ( ( rule__HmlMinFixPoint__ArgAssignment_3 ) )
            {
            // InternalComposedLts.g:6203:1: ( ( rule__HmlMinFixPoint__ArgAssignment_3 ) )
            // InternalComposedLts.g:6204:1: ( rule__HmlMinFixPoint__ArgAssignment_3 )
            {
             before(grammarAccess.getHmlMinFixPointAccess().getArgAssignment_3()); 
            // InternalComposedLts.g:6205:1: ( rule__HmlMinFixPoint__ArgAssignment_3 )
            // InternalComposedLts.g:6205:2: rule__HmlMinFixPoint__ArgAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__HmlMinFixPoint__ArgAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getHmlMinFixPointAccess().getArgAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__Group__3__Impl"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__0"
    // InternalComposedLts.g:6223:1: rule__HmlMaxFixPoint__Group__0 : rule__HmlMaxFixPoint__Group__0__Impl rule__HmlMaxFixPoint__Group__1 ;
    public final void rule__HmlMaxFixPoint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6227:1: ( rule__HmlMaxFixPoint__Group__0__Impl rule__HmlMaxFixPoint__Group__1 )
            // InternalComposedLts.g:6228:2: rule__HmlMaxFixPoint__Group__0__Impl rule__HmlMaxFixPoint__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__HmlMaxFixPoint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__0"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__0__Impl"
    // InternalComposedLts.g:6235:1: rule__HmlMaxFixPoint__Group__0__Impl : ( 'max' ) ;
    public final void rule__HmlMaxFixPoint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6239:1: ( ( 'max' ) )
            // InternalComposedLts.g:6240:1: ( 'max' )
            {
            // InternalComposedLts.g:6240:1: ( 'max' )
            // InternalComposedLts.g:6241:1: 'max'
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getMaxKeyword_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getHmlMaxFixPointAccess().getMaxKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__0__Impl"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__1"
    // InternalComposedLts.g:6254:1: rule__HmlMaxFixPoint__Group__1 : rule__HmlMaxFixPoint__Group__1__Impl rule__HmlMaxFixPoint__Group__2 ;
    public final void rule__HmlMaxFixPoint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6258:1: ( rule__HmlMaxFixPoint__Group__1__Impl rule__HmlMaxFixPoint__Group__2 )
            // InternalComposedLts.g:6259:2: rule__HmlMaxFixPoint__Group__1__Impl rule__HmlMaxFixPoint__Group__2
            {
            pushFollow(FOLLOW_39);
            rule__HmlMaxFixPoint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__1"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__1__Impl"
    // InternalComposedLts.g:6266:1: rule__HmlMaxFixPoint__Group__1__Impl : ( ( rule__HmlMaxFixPoint__NameAssignment_1 ) ) ;
    public final void rule__HmlMaxFixPoint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6270:1: ( ( ( rule__HmlMaxFixPoint__NameAssignment_1 ) ) )
            // InternalComposedLts.g:6271:1: ( ( rule__HmlMaxFixPoint__NameAssignment_1 ) )
            {
            // InternalComposedLts.g:6271:1: ( ( rule__HmlMaxFixPoint__NameAssignment_1 ) )
            // InternalComposedLts.g:6272:1: ( rule__HmlMaxFixPoint__NameAssignment_1 )
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getNameAssignment_1()); 
            // InternalComposedLts.g:6273:1: ( rule__HmlMaxFixPoint__NameAssignment_1 )
            // InternalComposedLts.g:6273:2: rule__HmlMaxFixPoint__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getHmlMaxFixPointAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__1__Impl"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__2"
    // InternalComposedLts.g:6283:1: rule__HmlMaxFixPoint__Group__2 : rule__HmlMaxFixPoint__Group__2__Impl rule__HmlMaxFixPoint__Group__3 ;
    public final void rule__HmlMaxFixPoint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6287:1: ( rule__HmlMaxFixPoint__Group__2__Impl rule__HmlMaxFixPoint__Group__3 )
            // InternalComposedLts.g:6288:2: rule__HmlMaxFixPoint__Group__2__Impl rule__HmlMaxFixPoint__Group__3
            {
            pushFollow(FOLLOW_30);
            rule__HmlMaxFixPoint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__2"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__2__Impl"
    // InternalComposedLts.g:6295:1: rule__HmlMaxFixPoint__Group__2__Impl : ( '.' ) ;
    public final void rule__HmlMaxFixPoint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6299:1: ( ( '.' ) )
            // InternalComposedLts.g:6300:1: ( '.' )
            {
            // InternalComposedLts.g:6300:1: ( '.' )
            // InternalComposedLts.g:6301:1: '.'
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getFullStopKeyword_2()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getHmlMaxFixPointAccess().getFullStopKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__2__Impl"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__3"
    // InternalComposedLts.g:6314:1: rule__HmlMaxFixPoint__Group__3 : rule__HmlMaxFixPoint__Group__3__Impl ;
    public final void rule__HmlMaxFixPoint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6318:1: ( rule__HmlMaxFixPoint__Group__3__Impl )
            // InternalComposedLts.g:6319:2: rule__HmlMaxFixPoint__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__3"


    // $ANTLR start "rule__HmlMaxFixPoint__Group__3__Impl"
    // InternalComposedLts.g:6325:1: rule__HmlMaxFixPoint__Group__3__Impl : ( ( rule__HmlMaxFixPoint__ArgAssignment_3 ) ) ;
    public final void rule__HmlMaxFixPoint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6329:1: ( ( ( rule__HmlMaxFixPoint__ArgAssignment_3 ) ) )
            // InternalComposedLts.g:6330:1: ( ( rule__HmlMaxFixPoint__ArgAssignment_3 ) )
            {
            // InternalComposedLts.g:6330:1: ( ( rule__HmlMaxFixPoint__ArgAssignment_3 ) )
            // InternalComposedLts.g:6331:1: ( rule__HmlMaxFixPoint__ArgAssignment_3 )
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getArgAssignment_3()); 
            // InternalComposedLts.g:6332:1: ( rule__HmlMaxFixPoint__ArgAssignment_3 )
            // InternalComposedLts.g:6332:2: rule__HmlMaxFixPoint__ArgAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__HmlMaxFixPoint__ArgAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getHmlMaxFixPointAccess().getArgAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__Group__3__Impl"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__0"
    // InternalComposedLts.g:6350:1: rule__LtlFormulaDeclaration__Group__0 : rule__LtlFormulaDeclaration__Group__0__Impl rule__LtlFormulaDeclaration__Group__1 ;
    public final void rule__LtlFormulaDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6354:1: ( rule__LtlFormulaDeclaration__Group__0__Impl rule__LtlFormulaDeclaration__Group__1 )
            // InternalComposedLts.g:6355:2: rule__LtlFormulaDeclaration__Group__0__Impl rule__LtlFormulaDeclaration__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__LtlFormulaDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__0"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__0__Impl"
    // InternalComposedLts.g:6362:1: rule__LtlFormulaDeclaration__Group__0__Impl : ( 'ltl' ) ;
    public final void rule__LtlFormulaDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6366:1: ( ( 'ltl' ) )
            // InternalComposedLts.g:6367:1: ( 'ltl' )
            {
            // InternalComposedLts.g:6367:1: ( 'ltl' )
            // InternalComposedLts.g:6368:1: 'ltl'
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getLtlKeyword_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getLtlFormulaDeclarationAccess().getLtlKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__0__Impl"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__1"
    // InternalComposedLts.g:6381:1: rule__LtlFormulaDeclaration__Group__1 : rule__LtlFormulaDeclaration__Group__1__Impl rule__LtlFormulaDeclaration__Group__2 ;
    public final void rule__LtlFormulaDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6385:1: ( rule__LtlFormulaDeclaration__Group__1__Impl rule__LtlFormulaDeclaration__Group__2 )
            // InternalComposedLts.g:6386:2: rule__LtlFormulaDeclaration__Group__1__Impl rule__LtlFormulaDeclaration__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__LtlFormulaDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__1"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__1__Impl"
    // InternalComposedLts.g:6393:1: rule__LtlFormulaDeclaration__Group__1__Impl : ( 'formula' ) ;
    public final void rule__LtlFormulaDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6397:1: ( ( 'formula' ) )
            // InternalComposedLts.g:6398:1: ( 'formula' )
            {
            // InternalComposedLts.g:6398:1: ( 'formula' )
            // InternalComposedLts.g:6399:1: 'formula'
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaKeyword_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__1__Impl"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__2"
    // InternalComposedLts.g:6412:1: rule__LtlFormulaDeclaration__Group__2 : rule__LtlFormulaDeclaration__Group__2__Impl rule__LtlFormulaDeclaration__Group__3 ;
    public final void rule__LtlFormulaDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6416:1: ( rule__LtlFormulaDeclaration__Group__2__Impl rule__LtlFormulaDeclaration__Group__3 )
            // InternalComposedLts.g:6417:2: rule__LtlFormulaDeclaration__Group__2__Impl rule__LtlFormulaDeclaration__Group__3
            {
            pushFollow(FOLLOW_29);
            rule__LtlFormulaDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__2"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__2__Impl"
    // InternalComposedLts.g:6424:1: rule__LtlFormulaDeclaration__Group__2__Impl : ( ( rule__LtlFormulaDeclaration__NameAssignment_2 ) ) ;
    public final void rule__LtlFormulaDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6428:1: ( ( ( rule__LtlFormulaDeclaration__NameAssignment_2 ) ) )
            // InternalComposedLts.g:6429:1: ( ( rule__LtlFormulaDeclaration__NameAssignment_2 ) )
            {
            // InternalComposedLts.g:6429:1: ( ( rule__LtlFormulaDeclaration__NameAssignment_2 ) )
            // InternalComposedLts.g:6430:1: ( rule__LtlFormulaDeclaration__NameAssignment_2 )
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getNameAssignment_2()); 
            // InternalComposedLts.g:6431:1: ( rule__LtlFormulaDeclaration__NameAssignment_2 )
            // InternalComposedLts.g:6431:2: rule__LtlFormulaDeclaration__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLtlFormulaDeclarationAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__2__Impl"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__3"
    // InternalComposedLts.g:6441:1: rule__LtlFormulaDeclaration__Group__3 : rule__LtlFormulaDeclaration__Group__3__Impl rule__LtlFormulaDeclaration__Group__4 ;
    public final void rule__LtlFormulaDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6445:1: ( rule__LtlFormulaDeclaration__Group__3__Impl rule__LtlFormulaDeclaration__Group__4 )
            // InternalComposedLts.g:6446:2: rule__LtlFormulaDeclaration__Group__3__Impl rule__LtlFormulaDeclaration__Group__4
            {
            pushFollow(FOLLOW_40);
            rule__LtlFormulaDeclaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__3"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__3__Impl"
    // InternalComposedLts.g:6453:1: rule__LtlFormulaDeclaration__Group__3__Impl : ( '=' ) ;
    public final void rule__LtlFormulaDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6457:1: ( ( '=' ) )
            // InternalComposedLts.g:6458:1: ( '=' )
            {
            // InternalComposedLts.g:6458:1: ( '=' )
            // InternalComposedLts.g:6459:1: '='
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getEqualsSignKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getLtlFormulaDeclarationAccess().getEqualsSignKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__3__Impl"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__4"
    // InternalComposedLts.g:6472:1: rule__LtlFormulaDeclaration__Group__4 : rule__LtlFormulaDeclaration__Group__4__Impl rule__LtlFormulaDeclaration__Group__5 ;
    public final void rule__LtlFormulaDeclaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6476:1: ( rule__LtlFormulaDeclaration__Group__4__Impl rule__LtlFormulaDeclaration__Group__5 )
            // InternalComposedLts.g:6477:2: rule__LtlFormulaDeclaration__Group__4__Impl rule__LtlFormulaDeclaration__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__LtlFormulaDeclaration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__4"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__4__Impl"
    // InternalComposedLts.g:6484:1: rule__LtlFormulaDeclaration__Group__4__Impl : ( ( rule__LtlFormulaDeclaration__FormulaAssignment_4 ) ) ;
    public final void rule__LtlFormulaDeclaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6488:1: ( ( ( rule__LtlFormulaDeclaration__FormulaAssignment_4 ) ) )
            // InternalComposedLts.g:6489:1: ( ( rule__LtlFormulaDeclaration__FormulaAssignment_4 ) )
            {
            // InternalComposedLts.g:6489:1: ( ( rule__LtlFormulaDeclaration__FormulaAssignment_4 ) )
            // InternalComposedLts.g:6490:1: ( rule__LtlFormulaDeclaration__FormulaAssignment_4 )
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaAssignment_4()); 
            // InternalComposedLts.g:6491:1: ( rule__LtlFormulaDeclaration__FormulaAssignment_4 )
            // InternalComposedLts.g:6491:2: rule__LtlFormulaDeclaration__FormulaAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__FormulaAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__4__Impl"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__5"
    // InternalComposedLts.g:6501:1: rule__LtlFormulaDeclaration__Group__5 : rule__LtlFormulaDeclaration__Group__5__Impl ;
    public final void rule__LtlFormulaDeclaration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6505:1: ( rule__LtlFormulaDeclaration__Group__5__Impl )
            // InternalComposedLts.g:6506:2: rule__LtlFormulaDeclaration__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlFormulaDeclaration__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__5"


    // $ANTLR start "rule__LtlFormulaDeclaration__Group__5__Impl"
    // InternalComposedLts.g:6512:1: rule__LtlFormulaDeclaration__Group__5__Impl : ( ';' ) ;
    public final void rule__LtlFormulaDeclaration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6516:1: ( ( ';' ) )
            // InternalComposedLts.g:6517:1: ( ';' )
            {
            // InternalComposedLts.g:6517:1: ( ';' )
            // InternalComposedLts.g:6518:1: ';'
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getSemicolonKeyword_5()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLtlFormulaDeclarationAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__Group__5__Impl"


    // $ANTLR start "rule__LtlOrFormula__Group__0"
    // InternalComposedLts.g:6543:1: rule__LtlOrFormula__Group__0 : rule__LtlOrFormula__Group__0__Impl rule__LtlOrFormula__Group__1 ;
    public final void rule__LtlOrFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6547:1: ( rule__LtlOrFormula__Group__0__Impl rule__LtlOrFormula__Group__1 )
            // InternalComposedLts.g:6548:2: rule__LtlOrFormula__Group__0__Impl rule__LtlOrFormula__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__LtlOrFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group__0"


    // $ANTLR start "rule__LtlOrFormula__Group__0__Impl"
    // InternalComposedLts.g:6555:1: rule__LtlOrFormula__Group__0__Impl : ( ruleLtlAndFormula ) ;
    public final void rule__LtlOrFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6559:1: ( ( ruleLtlAndFormula ) )
            // InternalComposedLts.g:6560:1: ( ruleLtlAndFormula )
            {
            // InternalComposedLts.g:6560:1: ( ruleLtlAndFormula )
            // InternalComposedLts.g:6561:1: ruleLtlAndFormula
            {
             before(grammarAccess.getLtlOrFormulaAccess().getLtlAndFormulaParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlAndFormula();

            state._fsp--;

             after(grammarAccess.getLtlOrFormulaAccess().getLtlAndFormulaParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlOrFormula__Group__1"
    // InternalComposedLts.g:6572:1: rule__LtlOrFormula__Group__1 : rule__LtlOrFormula__Group__1__Impl ;
    public final void rule__LtlOrFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6576:1: ( rule__LtlOrFormula__Group__1__Impl )
            // InternalComposedLts.g:6577:2: rule__LtlOrFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group__1"


    // $ANTLR start "rule__LtlOrFormula__Group__1__Impl"
    // InternalComposedLts.g:6583:1: rule__LtlOrFormula__Group__1__Impl : ( ( rule__LtlOrFormula__Group_1__0 )* ) ;
    public final void rule__LtlOrFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6587:1: ( ( ( rule__LtlOrFormula__Group_1__0 )* ) )
            // InternalComposedLts.g:6588:1: ( ( rule__LtlOrFormula__Group_1__0 )* )
            {
            // InternalComposedLts.g:6588:1: ( ( rule__LtlOrFormula__Group_1__0 )* )
            // InternalComposedLts.g:6589:1: ( rule__LtlOrFormula__Group_1__0 )*
            {
             before(grammarAccess.getLtlOrFormulaAccess().getGroup_1()); 
            // InternalComposedLts.g:6590:1: ( rule__LtlOrFormula__Group_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==17) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalComposedLts.g:6590:2: rule__LtlOrFormula__Group_1__0
            	    {
            	    pushFollow(FOLLOW_31);
            	    rule__LtlOrFormula__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getLtlOrFormulaAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlOrFormula__Group_1__0"
    // InternalComposedLts.g:6604:1: rule__LtlOrFormula__Group_1__0 : rule__LtlOrFormula__Group_1__0__Impl rule__LtlOrFormula__Group_1__1 ;
    public final void rule__LtlOrFormula__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6608:1: ( rule__LtlOrFormula__Group_1__0__Impl rule__LtlOrFormula__Group_1__1 )
            // InternalComposedLts.g:6609:2: rule__LtlOrFormula__Group_1__0__Impl rule__LtlOrFormula__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__LtlOrFormula__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group_1__0"


    // $ANTLR start "rule__LtlOrFormula__Group_1__0__Impl"
    // InternalComposedLts.g:6616:1: rule__LtlOrFormula__Group_1__0__Impl : ( () ) ;
    public final void rule__LtlOrFormula__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6620:1: ( ( () ) )
            // InternalComposedLts.g:6621:1: ( () )
            {
            // InternalComposedLts.g:6621:1: ( () )
            // InternalComposedLts.g:6622:1: ()
            {
             before(grammarAccess.getLtlOrFormulaAccess().getLtlOrFormulaLeftAction_1_0()); 
            // InternalComposedLts.g:6623:1: ()
            // InternalComposedLts.g:6625:1: 
            {
            }

             after(grammarAccess.getLtlOrFormulaAccess().getLtlOrFormulaLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group_1__0__Impl"


    // $ANTLR start "rule__LtlOrFormula__Group_1__1"
    // InternalComposedLts.g:6635:1: rule__LtlOrFormula__Group_1__1 : rule__LtlOrFormula__Group_1__1__Impl rule__LtlOrFormula__Group_1__2 ;
    public final void rule__LtlOrFormula__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6639:1: ( rule__LtlOrFormula__Group_1__1__Impl rule__LtlOrFormula__Group_1__2 )
            // InternalComposedLts.g:6640:2: rule__LtlOrFormula__Group_1__1__Impl rule__LtlOrFormula__Group_1__2
            {
            pushFollow(FOLLOW_40);
            rule__LtlOrFormula__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group_1__1"


    // $ANTLR start "rule__LtlOrFormula__Group_1__1__Impl"
    // InternalComposedLts.g:6647:1: rule__LtlOrFormula__Group_1__1__Impl : ( '|' ) ;
    public final void rule__LtlOrFormula__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6651:1: ( ( '|' ) )
            // InternalComposedLts.g:6652:1: ( '|' )
            {
            // InternalComposedLts.g:6652:1: ( '|' )
            // InternalComposedLts.g:6653:1: '|'
            {
             before(grammarAccess.getLtlOrFormulaAccess().getVerticalLineKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getLtlOrFormulaAccess().getVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group_1__1__Impl"


    // $ANTLR start "rule__LtlOrFormula__Group_1__2"
    // InternalComposedLts.g:6666:1: rule__LtlOrFormula__Group_1__2 : rule__LtlOrFormula__Group_1__2__Impl ;
    public final void rule__LtlOrFormula__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6670:1: ( rule__LtlOrFormula__Group_1__2__Impl )
            // InternalComposedLts.g:6671:2: rule__LtlOrFormula__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group_1__2"


    // $ANTLR start "rule__LtlOrFormula__Group_1__2__Impl"
    // InternalComposedLts.g:6677:1: rule__LtlOrFormula__Group_1__2__Impl : ( ( rule__LtlOrFormula__RightAssignment_1_2 ) ) ;
    public final void rule__LtlOrFormula__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6681:1: ( ( ( rule__LtlOrFormula__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:6682:1: ( ( rule__LtlOrFormula__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:6682:1: ( ( rule__LtlOrFormula__RightAssignment_1_2 ) )
            // InternalComposedLts.g:6683:1: ( rule__LtlOrFormula__RightAssignment_1_2 )
            {
             before(grammarAccess.getLtlOrFormulaAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:6684:1: ( rule__LtlOrFormula__RightAssignment_1_2 )
            // InternalComposedLts.g:6684:2: rule__LtlOrFormula__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__LtlOrFormula__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getLtlOrFormulaAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__Group_1__2__Impl"


    // $ANTLR start "rule__LtlAndFormula__Group__0"
    // InternalComposedLts.g:6700:1: rule__LtlAndFormula__Group__0 : rule__LtlAndFormula__Group__0__Impl rule__LtlAndFormula__Group__1 ;
    public final void rule__LtlAndFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6704:1: ( rule__LtlAndFormula__Group__0__Impl rule__LtlAndFormula__Group__1 )
            // InternalComposedLts.g:6705:2: rule__LtlAndFormula__Group__0__Impl rule__LtlAndFormula__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__LtlAndFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group__0"


    // $ANTLR start "rule__LtlAndFormula__Group__0__Impl"
    // InternalComposedLts.g:6712:1: rule__LtlAndFormula__Group__0__Impl : ( ruleLtlUntilFormula ) ;
    public final void rule__LtlAndFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6716:1: ( ( ruleLtlUntilFormula ) )
            // InternalComposedLts.g:6717:1: ( ruleLtlUntilFormula )
            {
            // InternalComposedLts.g:6717:1: ( ruleLtlUntilFormula )
            // InternalComposedLts.g:6718:1: ruleLtlUntilFormula
            {
             before(grammarAccess.getLtlAndFormulaAccess().getLtlUntilFormulaParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlUntilFormula();

            state._fsp--;

             after(grammarAccess.getLtlAndFormulaAccess().getLtlUntilFormulaParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlAndFormula__Group__1"
    // InternalComposedLts.g:6729:1: rule__LtlAndFormula__Group__1 : rule__LtlAndFormula__Group__1__Impl ;
    public final void rule__LtlAndFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6733:1: ( rule__LtlAndFormula__Group__1__Impl )
            // InternalComposedLts.g:6734:2: rule__LtlAndFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group__1"


    // $ANTLR start "rule__LtlAndFormula__Group__1__Impl"
    // InternalComposedLts.g:6740:1: rule__LtlAndFormula__Group__1__Impl : ( ( rule__LtlAndFormula__Group_1__0 )* ) ;
    public final void rule__LtlAndFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6744:1: ( ( ( rule__LtlAndFormula__Group_1__0 )* ) )
            // InternalComposedLts.g:6745:1: ( ( rule__LtlAndFormula__Group_1__0 )* )
            {
            // InternalComposedLts.g:6745:1: ( ( rule__LtlAndFormula__Group_1__0 )* )
            // InternalComposedLts.g:6746:1: ( rule__LtlAndFormula__Group_1__0 )*
            {
             before(grammarAccess.getLtlAndFormulaAccess().getGroup_1()); 
            // InternalComposedLts.g:6747:1: ( rule__LtlAndFormula__Group_1__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==36) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalComposedLts.g:6747:2: rule__LtlAndFormula__Group_1__0
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__LtlAndFormula__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getLtlAndFormulaAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlAndFormula__Group_1__0"
    // InternalComposedLts.g:6761:1: rule__LtlAndFormula__Group_1__0 : rule__LtlAndFormula__Group_1__0__Impl rule__LtlAndFormula__Group_1__1 ;
    public final void rule__LtlAndFormula__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6765:1: ( rule__LtlAndFormula__Group_1__0__Impl rule__LtlAndFormula__Group_1__1 )
            // InternalComposedLts.g:6766:2: rule__LtlAndFormula__Group_1__0__Impl rule__LtlAndFormula__Group_1__1
            {
            pushFollow(FOLLOW_32);
            rule__LtlAndFormula__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group_1__0"


    // $ANTLR start "rule__LtlAndFormula__Group_1__0__Impl"
    // InternalComposedLts.g:6773:1: rule__LtlAndFormula__Group_1__0__Impl : ( () ) ;
    public final void rule__LtlAndFormula__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6777:1: ( ( () ) )
            // InternalComposedLts.g:6778:1: ( () )
            {
            // InternalComposedLts.g:6778:1: ( () )
            // InternalComposedLts.g:6779:1: ()
            {
             before(grammarAccess.getLtlAndFormulaAccess().getLtlAndFormulaLeftAction_1_0()); 
            // InternalComposedLts.g:6780:1: ()
            // InternalComposedLts.g:6782:1: 
            {
            }

             after(grammarAccess.getLtlAndFormulaAccess().getLtlAndFormulaLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group_1__0__Impl"


    // $ANTLR start "rule__LtlAndFormula__Group_1__1"
    // InternalComposedLts.g:6792:1: rule__LtlAndFormula__Group_1__1 : rule__LtlAndFormula__Group_1__1__Impl rule__LtlAndFormula__Group_1__2 ;
    public final void rule__LtlAndFormula__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6796:1: ( rule__LtlAndFormula__Group_1__1__Impl rule__LtlAndFormula__Group_1__2 )
            // InternalComposedLts.g:6797:2: rule__LtlAndFormula__Group_1__1__Impl rule__LtlAndFormula__Group_1__2
            {
            pushFollow(FOLLOW_40);
            rule__LtlAndFormula__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group_1__1"


    // $ANTLR start "rule__LtlAndFormula__Group_1__1__Impl"
    // InternalComposedLts.g:6804:1: rule__LtlAndFormula__Group_1__1__Impl : ( '&' ) ;
    public final void rule__LtlAndFormula__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6808:1: ( ( '&' ) )
            // InternalComposedLts.g:6809:1: ( '&' )
            {
            // InternalComposedLts.g:6809:1: ( '&' )
            // InternalComposedLts.g:6810:1: '&'
            {
             before(grammarAccess.getLtlAndFormulaAccess().getAmpersandKeyword_1_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getLtlAndFormulaAccess().getAmpersandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group_1__1__Impl"


    // $ANTLR start "rule__LtlAndFormula__Group_1__2"
    // InternalComposedLts.g:6823:1: rule__LtlAndFormula__Group_1__2 : rule__LtlAndFormula__Group_1__2__Impl ;
    public final void rule__LtlAndFormula__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6827:1: ( rule__LtlAndFormula__Group_1__2__Impl )
            // InternalComposedLts.g:6828:2: rule__LtlAndFormula__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group_1__2"


    // $ANTLR start "rule__LtlAndFormula__Group_1__2__Impl"
    // InternalComposedLts.g:6834:1: rule__LtlAndFormula__Group_1__2__Impl : ( ( rule__LtlAndFormula__RightAssignment_1_2 ) ) ;
    public final void rule__LtlAndFormula__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6838:1: ( ( ( rule__LtlAndFormula__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:6839:1: ( ( rule__LtlAndFormula__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:6839:1: ( ( rule__LtlAndFormula__RightAssignment_1_2 ) )
            // InternalComposedLts.g:6840:1: ( rule__LtlAndFormula__RightAssignment_1_2 )
            {
             before(grammarAccess.getLtlAndFormulaAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:6841:1: ( rule__LtlAndFormula__RightAssignment_1_2 )
            // InternalComposedLts.g:6841:2: rule__LtlAndFormula__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__LtlAndFormula__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getLtlAndFormulaAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__Group_1__2__Impl"


    // $ANTLR start "rule__LtlUntilFormula__Group__0"
    // InternalComposedLts.g:6857:1: rule__LtlUntilFormula__Group__0 : rule__LtlUntilFormula__Group__0__Impl rule__LtlUntilFormula__Group__1 ;
    public final void rule__LtlUntilFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6861:1: ( rule__LtlUntilFormula__Group__0__Impl rule__LtlUntilFormula__Group__1 )
            // InternalComposedLts.g:6862:2: rule__LtlUntilFormula__Group__0__Impl rule__LtlUntilFormula__Group__1
            {
            pushFollow(FOLLOW_41);
            rule__LtlUntilFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group__0"


    // $ANTLR start "rule__LtlUntilFormula__Group__0__Impl"
    // InternalComposedLts.g:6869:1: rule__LtlUntilFormula__Group__0__Impl : ( ruleLtlBaseFormula ) ;
    public final void rule__LtlUntilFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6873:1: ( ( ruleLtlBaseFormula ) )
            // InternalComposedLts.g:6874:1: ( ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:6874:1: ( ruleLtlBaseFormula )
            // InternalComposedLts.g:6875:1: ruleLtlBaseFormula
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getLtlBaseFormulaParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlUntilFormulaAccess().getLtlBaseFormulaParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlUntilFormula__Group__1"
    // InternalComposedLts.g:6886:1: rule__LtlUntilFormula__Group__1 : rule__LtlUntilFormula__Group__1__Impl ;
    public final void rule__LtlUntilFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6890:1: ( rule__LtlUntilFormula__Group__1__Impl )
            // InternalComposedLts.g:6891:2: rule__LtlUntilFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group__1"


    // $ANTLR start "rule__LtlUntilFormula__Group__1__Impl"
    // InternalComposedLts.g:6897:1: rule__LtlUntilFormula__Group__1__Impl : ( ( rule__LtlUntilFormula__Group_1__0 )* ) ;
    public final void rule__LtlUntilFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6901:1: ( ( ( rule__LtlUntilFormula__Group_1__0 )* ) )
            // InternalComposedLts.g:6902:1: ( ( rule__LtlUntilFormula__Group_1__0 )* )
            {
            // InternalComposedLts.g:6902:1: ( ( rule__LtlUntilFormula__Group_1__0 )* )
            // InternalComposedLts.g:6903:1: ( rule__LtlUntilFormula__Group_1__0 )*
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getGroup_1()); 
            // InternalComposedLts.g:6904:1: ( rule__LtlUntilFormula__Group_1__0 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==49) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalComposedLts.g:6904:2: rule__LtlUntilFormula__Group_1__0
            	    {
            	    pushFollow(FOLLOW_42);
            	    rule__LtlUntilFormula__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getLtlUntilFormulaAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlUntilFormula__Group_1__0"
    // InternalComposedLts.g:6918:1: rule__LtlUntilFormula__Group_1__0 : rule__LtlUntilFormula__Group_1__0__Impl rule__LtlUntilFormula__Group_1__1 ;
    public final void rule__LtlUntilFormula__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6922:1: ( rule__LtlUntilFormula__Group_1__0__Impl rule__LtlUntilFormula__Group_1__1 )
            // InternalComposedLts.g:6923:2: rule__LtlUntilFormula__Group_1__0__Impl rule__LtlUntilFormula__Group_1__1
            {
            pushFollow(FOLLOW_41);
            rule__LtlUntilFormula__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group_1__0"


    // $ANTLR start "rule__LtlUntilFormula__Group_1__0__Impl"
    // InternalComposedLts.g:6930:1: rule__LtlUntilFormula__Group_1__0__Impl : ( () ) ;
    public final void rule__LtlUntilFormula__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6934:1: ( ( () ) )
            // InternalComposedLts.g:6935:1: ( () )
            {
            // InternalComposedLts.g:6935:1: ( () )
            // InternalComposedLts.g:6936:1: ()
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getLtlUntilFormulaLeftAction_1_0()); 
            // InternalComposedLts.g:6937:1: ()
            // InternalComposedLts.g:6939:1: 
            {
            }

             after(grammarAccess.getLtlUntilFormulaAccess().getLtlUntilFormulaLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group_1__0__Impl"


    // $ANTLR start "rule__LtlUntilFormula__Group_1__1"
    // InternalComposedLts.g:6949:1: rule__LtlUntilFormula__Group_1__1 : rule__LtlUntilFormula__Group_1__1__Impl rule__LtlUntilFormula__Group_1__2 ;
    public final void rule__LtlUntilFormula__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6953:1: ( rule__LtlUntilFormula__Group_1__1__Impl rule__LtlUntilFormula__Group_1__2 )
            // InternalComposedLts.g:6954:2: rule__LtlUntilFormula__Group_1__1__Impl rule__LtlUntilFormula__Group_1__2
            {
            pushFollow(FOLLOW_40);
            rule__LtlUntilFormula__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group_1__1"


    // $ANTLR start "rule__LtlUntilFormula__Group_1__1__Impl"
    // InternalComposedLts.g:6961:1: rule__LtlUntilFormula__Group_1__1__Impl : ( '\\\\U' ) ;
    public final void rule__LtlUntilFormula__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6965:1: ( ( '\\\\U' ) )
            // InternalComposedLts.g:6966:1: ( '\\\\U' )
            {
            // InternalComposedLts.g:6966:1: ( '\\\\U' )
            // InternalComposedLts.g:6967:1: '\\\\U'
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getUKeyword_1_1()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getLtlUntilFormulaAccess().getUKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group_1__1__Impl"


    // $ANTLR start "rule__LtlUntilFormula__Group_1__2"
    // InternalComposedLts.g:6980:1: rule__LtlUntilFormula__Group_1__2 : rule__LtlUntilFormula__Group_1__2__Impl ;
    public final void rule__LtlUntilFormula__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6984:1: ( rule__LtlUntilFormula__Group_1__2__Impl )
            // InternalComposedLts.g:6985:2: rule__LtlUntilFormula__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group_1__2"


    // $ANTLR start "rule__LtlUntilFormula__Group_1__2__Impl"
    // InternalComposedLts.g:6991:1: rule__LtlUntilFormula__Group_1__2__Impl : ( ( rule__LtlUntilFormula__RightAssignment_1_2 ) ) ;
    public final void rule__LtlUntilFormula__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:6995:1: ( ( ( rule__LtlUntilFormula__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:6996:1: ( ( rule__LtlUntilFormula__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:6996:1: ( ( rule__LtlUntilFormula__RightAssignment_1_2 ) )
            // InternalComposedLts.g:6997:1: ( rule__LtlUntilFormula__RightAssignment_1_2 )
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:6998:1: ( rule__LtlUntilFormula__RightAssignment_1_2 )
            // InternalComposedLts.g:6998:2: rule__LtlUntilFormula__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__LtlUntilFormula__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getLtlUntilFormulaAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__Group_1__2__Impl"


    // $ANTLR start "rule__LtlBaseFormula__Group_2__0"
    // InternalComposedLts.g:7014:1: rule__LtlBaseFormula__Group_2__0 : rule__LtlBaseFormula__Group_2__0__Impl rule__LtlBaseFormula__Group_2__1 ;
    public final void rule__LtlBaseFormula__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7018:1: ( rule__LtlBaseFormula__Group_2__0__Impl rule__LtlBaseFormula__Group_2__1 )
            // InternalComposedLts.g:7019:2: rule__LtlBaseFormula__Group_2__0__Impl rule__LtlBaseFormula__Group_2__1
            {
            pushFollow(FOLLOW_40);
            rule__LtlBaseFormula__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlBaseFormula__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Group_2__0"


    // $ANTLR start "rule__LtlBaseFormula__Group_2__0__Impl"
    // InternalComposedLts.g:7026:1: rule__LtlBaseFormula__Group_2__0__Impl : ( '(' ) ;
    public final void rule__LtlBaseFormula__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7030:1: ( ( '(' ) )
            // InternalComposedLts.g:7031:1: ( '(' )
            {
            // InternalComposedLts.g:7031:1: ( '(' )
            // InternalComposedLts.g:7032:1: '('
            {
             before(grammarAccess.getLtlBaseFormulaAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getLtlBaseFormulaAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Group_2__0__Impl"


    // $ANTLR start "rule__LtlBaseFormula__Group_2__1"
    // InternalComposedLts.g:7045:1: rule__LtlBaseFormula__Group_2__1 : rule__LtlBaseFormula__Group_2__1__Impl rule__LtlBaseFormula__Group_2__2 ;
    public final void rule__LtlBaseFormula__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7049:1: ( rule__LtlBaseFormula__Group_2__1__Impl rule__LtlBaseFormula__Group_2__2 )
            // InternalComposedLts.g:7050:2: rule__LtlBaseFormula__Group_2__1__Impl rule__LtlBaseFormula__Group_2__2
            {
            pushFollow(FOLLOW_17);
            rule__LtlBaseFormula__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlBaseFormula__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Group_2__1"


    // $ANTLR start "rule__LtlBaseFormula__Group_2__1__Impl"
    // InternalComposedLts.g:7057:1: rule__LtlBaseFormula__Group_2__1__Impl : ( ruleLtlFormula ) ;
    public final void rule__LtlBaseFormula__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7061:1: ( ( ruleLtlFormula ) )
            // InternalComposedLts.g:7062:1: ( ruleLtlFormula )
            {
            // InternalComposedLts.g:7062:1: ( ruleLtlFormula )
            // InternalComposedLts.g:7063:1: ruleLtlFormula
            {
             before(grammarAccess.getLtlBaseFormulaAccess().getLtlFormulaParserRuleCall_2_1()); 
            pushFollow(FOLLOW_2);
            ruleLtlFormula();

            state._fsp--;

             after(grammarAccess.getLtlBaseFormulaAccess().getLtlFormulaParserRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Group_2__1__Impl"


    // $ANTLR start "rule__LtlBaseFormula__Group_2__2"
    // InternalComposedLts.g:7074:1: rule__LtlBaseFormula__Group_2__2 : rule__LtlBaseFormula__Group_2__2__Impl ;
    public final void rule__LtlBaseFormula__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7078:1: ( rule__LtlBaseFormula__Group_2__2__Impl )
            // InternalComposedLts.g:7079:2: rule__LtlBaseFormula__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlBaseFormula__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Group_2__2"


    // $ANTLR start "rule__LtlBaseFormula__Group_2__2__Impl"
    // InternalComposedLts.g:7085:1: rule__LtlBaseFormula__Group_2__2__Impl : ( ')' ) ;
    public final void rule__LtlBaseFormula__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7089:1: ( ( ')' ) )
            // InternalComposedLts.g:7090:1: ( ')' )
            {
            // InternalComposedLts.g:7090:1: ( ')' )
            // InternalComposedLts.g:7091:1: ')'
            {
             before(grammarAccess.getLtlBaseFormulaAccess().getRightParenthesisKeyword_2_2()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getLtlBaseFormulaAccess().getRightParenthesisKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlBaseFormula__Group_2__2__Impl"


    // $ANTLR start "rule__LtlNextFormula__Group__0"
    // InternalComposedLts.g:7110:1: rule__LtlNextFormula__Group__0 : rule__LtlNextFormula__Group__0__Impl rule__LtlNextFormula__Group__1 ;
    public final void rule__LtlNextFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7114:1: ( rule__LtlNextFormula__Group__0__Impl rule__LtlNextFormula__Group__1 )
            // InternalComposedLts.g:7115:2: rule__LtlNextFormula__Group__0__Impl rule__LtlNextFormula__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__LtlNextFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlNextFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__0"


    // $ANTLR start "rule__LtlNextFormula__Group__0__Impl"
    // InternalComposedLts.g:7122:1: rule__LtlNextFormula__Group__0__Impl : ( '\\\\X' ) ;
    public final void rule__LtlNextFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7126:1: ( ( '\\\\X' ) )
            // InternalComposedLts.g:7127:1: ( '\\\\X' )
            {
            // InternalComposedLts.g:7127:1: ( '\\\\X' )
            // InternalComposedLts.g:7128:1: '\\\\X'
            {
             before(grammarAccess.getLtlNextFormulaAccess().getXKeyword_0()); 
            match(input,50,FOLLOW_2); 
             after(grammarAccess.getLtlNextFormulaAccess().getXKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlNextFormula__Group__1"
    // InternalComposedLts.g:7141:1: rule__LtlNextFormula__Group__1 : rule__LtlNextFormula__Group__1__Impl rule__LtlNextFormula__Group__2 ;
    public final void rule__LtlNextFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7145:1: ( rule__LtlNextFormula__Group__1__Impl rule__LtlNextFormula__Group__2 )
            // InternalComposedLts.g:7146:2: rule__LtlNextFormula__Group__1__Impl rule__LtlNextFormula__Group__2
            {
            pushFollow(FOLLOW_40);
            rule__LtlNextFormula__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlNextFormula__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__1"


    // $ANTLR start "rule__LtlNextFormula__Group__1__Impl"
    // InternalComposedLts.g:7153:1: rule__LtlNextFormula__Group__1__Impl : ( '{' ) ;
    public final void rule__LtlNextFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7157:1: ( ( '{' ) )
            // InternalComposedLts.g:7158:1: ( '{' )
            {
            // InternalComposedLts.g:7158:1: ( '{' )
            // InternalComposedLts.g:7159:1: '{'
            {
             before(grammarAccess.getLtlNextFormulaAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getLtlNextFormulaAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlNextFormula__Group__2"
    // InternalComposedLts.g:7172:1: rule__LtlNextFormula__Group__2 : rule__LtlNextFormula__Group__2__Impl rule__LtlNextFormula__Group__3 ;
    public final void rule__LtlNextFormula__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7176:1: ( rule__LtlNextFormula__Group__2__Impl rule__LtlNextFormula__Group__3 )
            // InternalComposedLts.g:7177:2: rule__LtlNextFormula__Group__2__Impl rule__LtlNextFormula__Group__3
            {
            pushFollow(FOLLOW_43);
            rule__LtlNextFormula__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlNextFormula__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__2"


    // $ANTLR start "rule__LtlNextFormula__Group__2__Impl"
    // InternalComposedLts.g:7184:1: rule__LtlNextFormula__Group__2__Impl : ( ( rule__LtlNextFormula__ArgAssignment_2 ) ) ;
    public final void rule__LtlNextFormula__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7188:1: ( ( ( rule__LtlNextFormula__ArgAssignment_2 ) ) )
            // InternalComposedLts.g:7189:1: ( ( rule__LtlNextFormula__ArgAssignment_2 ) )
            {
            // InternalComposedLts.g:7189:1: ( ( rule__LtlNextFormula__ArgAssignment_2 ) )
            // InternalComposedLts.g:7190:1: ( rule__LtlNextFormula__ArgAssignment_2 )
            {
             before(grammarAccess.getLtlNextFormulaAccess().getArgAssignment_2()); 
            // InternalComposedLts.g:7191:1: ( rule__LtlNextFormula__ArgAssignment_2 )
            // InternalComposedLts.g:7191:2: rule__LtlNextFormula__ArgAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__LtlNextFormula__ArgAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLtlNextFormulaAccess().getArgAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__2__Impl"


    // $ANTLR start "rule__LtlNextFormula__Group__3"
    // InternalComposedLts.g:7201:1: rule__LtlNextFormula__Group__3 : rule__LtlNextFormula__Group__3__Impl ;
    public final void rule__LtlNextFormula__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7205:1: ( rule__LtlNextFormula__Group__3__Impl )
            // InternalComposedLts.g:7206:2: rule__LtlNextFormula__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlNextFormula__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__3"


    // $ANTLR start "rule__LtlNextFormula__Group__3__Impl"
    // InternalComposedLts.g:7212:1: rule__LtlNextFormula__Group__3__Impl : ( '}' ) ;
    public final void rule__LtlNextFormula__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7216:1: ( ( '}' ) )
            // InternalComposedLts.g:7217:1: ( '}' )
            {
            // InternalComposedLts.g:7217:1: ( '}' )
            // InternalComposedLts.g:7218:1: '}'
            {
             before(grammarAccess.getLtlNextFormulaAccess().getRightCurlyBracketKeyword_3()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLtlNextFormulaAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__Group__3__Impl"


    // $ANTLR start "rule__LtlAlwaysFormula__Group__0"
    // InternalComposedLts.g:7239:1: rule__LtlAlwaysFormula__Group__0 : rule__LtlAlwaysFormula__Group__0__Impl rule__LtlAlwaysFormula__Group__1 ;
    public final void rule__LtlAlwaysFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7243:1: ( rule__LtlAlwaysFormula__Group__0__Impl rule__LtlAlwaysFormula__Group__1 )
            // InternalComposedLts.g:7244:2: rule__LtlAlwaysFormula__Group__0__Impl rule__LtlAlwaysFormula__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__LtlAlwaysFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlAlwaysFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAlwaysFormula__Group__0"


    // $ANTLR start "rule__LtlAlwaysFormula__Group__0__Impl"
    // InternalComposedLts.g:7251:1: rule__LtlAlwaysFormula__Group__0__Impl : ( '\\\\G' ) ;
    public final void rule__LtlAlwaysFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7255:1: ( ( '\\\\G' ) )
            // InternalComposedLts.g:7256:1: ( '\\\\G' )
            {
            // InternalComposedLts.g:7256:1: ( '\\\\G' )
            // InternalComposedLts.g:7257:1: '\\\\G'
            {
             before(grammarAccess.getLtlAlwaysFormulaAccess().getGKeyword_0()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getLtlAlwaysFormulaAccess().getGKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAlwaysFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlAlwaysFormula__Group__1"
    // InternalComposedLts.g:7270:1: rule__LtlAlwaysFormula__Group__1 : rule__LtlAlwaysFormula__Group__1__Impl ;
    public final void rule__LtlAlwaysFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7274:1: ( rule__LtlAlwaysFormula__Group__1__Impl )
            // InternalComposedLts.g:7275:2: rule__LtlAlwaysFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlAlwaysFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAlwaysFormula__Group__1"


    // $ANTLR start "rule__LtlAlwaysFormula__Group__1__Impl"
    // InternalComposedLts.g:7281:1: rule__LtlAlwaysFormula__Group__1__Impl : ( ( rule__LtlAlwaysFormula__ArgAssignment_1 ) ) ;
    public final void rule__LtlAlwaysFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7285:1: ( ( ( rule__LtlAlwaysFormula__ArgAssignment_1 ) ) )
            // InternalComposedLts.g:7286:1: ( ( rule__LtlAlwaysFormula__ArgAssignment_1 ) )
            {
            // InternalComposedLts.g:7286:1: ( ( rule__LtlAlwaysFormula__ArgAssignment_1 ) )
            // InternalComposedLts.g:7287:1: ( rule__LtlAlwaysFormula__ArgAssignment_1 )
            {
             before(grammarAccess.getLtlAlwaysFormulaAccess().getArgAssignment_1()); 
            // InternalComposedLts.g:7288:1: ( rule__LtlAlwaysFormula__ArgAssignment_1 )
            // InternalComposedLts.g:7288:2: rule__LtlAlwaysFormula__ArgAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__LtlAlwaysFormula__ArgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLtlAlwaysFormulaAccess().getArgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAlwaysFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlEventuallyFormula__Group__0"
    // InternalComposedLts.g:7302:1: rule__LtlEventuallyFormula__Group__0 : rule__LtlEventuallyFormula__Group__0__Impl rule__LtlEventuallyFormula__Group__1 ;
    public final void rule__LtlEventuallyFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7306:1: ( rule__LtlEventuallyFormula__Group__0__Impl rule__LtlEventuallyFormula__Group__1 )
            // InternalComposedLts.g:7307:2: rule__LtlEventuallyFormula__Group__0__Impl rule__LtlEventuallyFormula__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__LtlEventuallyFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlEventuallyFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlEventuallyFormula__Group__0"


    // $ANTLR start "rule__LtlEventuallyFormula__Group__0__Impl"
    // InternalComposedLts.g:7314:1: rule__LtlEventuallyFormula__Group__0__Impl : ( '\\\\F' ) ;
    public final void rule__LtlEventuallyFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7318:1: ( ( '\\\\F' ) )
            // InternalComposedLts.g:7319:1: ( '\\\\F' )
            {
            // InternalComposedLts.g:7319:1: ( '\\\\F' )
            // InternalComposedLts.g:7320:1: '\\\\F'
            {
             before(grammarAccess.getLtlEventuallyFormulaAccess().getFKeyword_0()); 
            match(input,52,FOLLOW_2); 
             after(grammarAccess.getLtlEventuallyFormulaAccess().getFKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlEventuallyFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlEventuallyFormula__Group__1"
    // InternalComposedLts.g:7333:1: rule__LtlEventuallyFormula__Group__1 : rule__LtlEventuallyFormula__Group__1__Impl ;
    public final void rule__LtlEventuallyFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7337:1: ( rule__LtlEventuallyFormula__Group__1__Impl )
            // InternalComposedLts.g:7338:2: rule__LtlEventuallyFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlEventuallyFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlEventuallyFormula__Group__1"


    // $ANTLR start "rule__LtlEventuallyFormula__Group__1__Impl"
    // InternalComposedLts.g:7344:1: rule__LtlEventuallyFormula__Group__1__Impl : ( ( rule__LtlEventuallyFormula__ArgAssignment_1 ) ) ;
    public final void rule__LtlEventuallyFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7348:1: ( ( ( rule__LtlEventuallyFormula__ArgAssignment_1 ) ) )
            // InternalComposedLts.g:7349:1: ( ( rule__LtlEventuallyFormula__ArgAssignment_1 ) )
            {
            // InternalComposedLts.g:7349:1: ( ( rule__LtlEventuallyFormula__ArgAssignment_1 ) )
            // InternalComposedLts.g:7350:1: ( rule__LtlEventuallyFormula__ArgAssignment_1 )
            {
             before(grammarAccess.getLtlEventuallyFormulaAccess().getArgAssignment_1()); 
            // InternalComposedLts.g:7351:1: ( rule__LtlEventuallyFormula__ArgAssignment_1 )
            // InternalComposedLts.g:7351:2: rule__LtlEventuallyFormula__ArgAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__LtlEventuallyFormula__ArgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLtlEventuallyFormulaAccess().getArgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlEventuallyFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlNotFormula__Group__0"
    // InternalComposedLts.g:7365:1: rule__LtlNotFormula__Group__0 : rule__LtlNotFormula__Group__0__Impl rule__LtlNotFormula__Group__1 ;
    public final void rule__LtlNotFormula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7369:1: ( rule__LtlNotFormula__Group__0__Impl rule__LtlNotFormula__Group__1 )
            // InternalComposedLts.g:7370:2: rule__LtlNotFormula__Group__0__Impl rule__LtlNotFormula__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__LtlNotFormula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlNotFormula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNotFormula__Group__0"


    // $ANTLR start "rule__LtlNotFormula__Group__0__Impl"
    // InternalComposedLts.g:7377:1: rule__LtlNotFormula__Group__0__Impl : ( '!' ) ;
    public final void rule__LtlNotFormula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7381:1: ( ( '!' ) )
            // InternalComposedLts.g:7382:1: ( '!' )
            {
            // InternalComposedLts.g:7382:1: ( '!' )
            // InternalComposedLts.g:7383:1: '!'
            {
             before(grammarAccess.getLtlNotFormulaAccess().getExclamationMarkKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getLtlNotFormulaAccess().getExclamationMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNotFormula__Group__0__Impl"


    // $ANTLR start "rule__LtlNotFormula__Group__1"
    // InternalComposedLts.g:7396:1: rule__LtlNotFormula__Group__1 : rule__LtlNotFormula__Group__1__Impl ;
    public final void rule__LtlNotFormula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7400:1: ( rule__LtlNotFormula__Group__1__Impl )
            // InternalComposedLts.g:7401:2: rule__LtlNotFormula__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlNotFormula__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNotFormula__Group__1"


    // $ANTLR start "rule__LtlNotFormula__Group__1__Impl"
    // InternalComposedLts.g:7407:1: rule__LtlNotFormula__Group__1__Impl : ( ( rule__LtlNotFormula__ArgAssignment_1 ) ) ;
    public final void rule__LtlNotFormula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7411:1: ( ( ( rule__LtlNotFormula__ArgAssignment_1 ) ) )
            // InternalComposedLts.g:7412:1: ( ( rule__LtlNotFormula__ArgAssignment_1 ) )
            {
            // InternalComposedLts.g:7412:1: ( ( rule__LtlNotFormula__ArgAssignment_1 ) )
            // InternalComposedLts.g:7413:1: ( rule__LtlNotFormula__ArgAssignment_1 )
            {
             before(grammarAccess.getLtlNotFormulaAccess().getArgAssignment_1()); 
            // InternalComposedLts.g:7414:1: ( rule__LtlNotFormula__ArgAssignment_1 )
            // InternalComposedLts.g:7414:2: rule__LtlNotFormula__ArgAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__LtlNotFormula__ArgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLtlNotFormulaAccess().getArgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNotFormula__Group__1__Impl"


    // $ANTLR start "rule__LtlTrue__Group__0"
    // InternalComposedLts.g:7428:1: rule__LtlTrue__Group__0 : rule__LtlTrue__Group__0__Impl rule__LtlTrue__Group__1 ;
    public final void rule__LtlTrue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7432:1: ( rule__LtlTrue__Group__0__Impl rule__LtlTrue__Group__1 )
            // InternalComposedLts.g:7433:2: rule__LtlTrue__Group__0__Impl rule__LtlTrue__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__LtlTrue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlTrue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlTrue__Group__0"


    // $ANTLR start "rule__LtlTrue__Group__0__Impl"
    // InternalComposedLts.g:7440:1: rule__LtlTrue__Group__0__Impl : ( () ) ;
    public final void rule__LtlTrue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7444:1: ( ( () ) )
            // InternalComposedLts.g:7445:1: ( () )
            {
            // InternalComposedLts.g:7445:1: ( () )
            // InternalComposedLts.g:7446:1: ()
            {
             before(grammarAccess.getLtlTrueAccess().getLtlTrueAction_0()); 
            // InternalComposedLts.g:7447:1: ()
            // InternalComposedLts.g:7449:1: 
            {
            }

             after(grammarAccess.getLtlTrueAccess().getLtlTrueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlTrue__Group__0__Impl"


    // $ANTLR start "rule__LtlTrue__Group__1"
    // InternalComposedLts.g:7459:1: rule__LtlTrue__Group__1 : rule__LtlTrue__Group__1__Impl ;
    public final void rule__LtlTrue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7463:1: ( rule__LtlTrue__Group__1__Impl )
            // InternalComposedLts.g:7464:2: rule__LtlTrue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlTrue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlTrue__Group__1"


    // $ANTLR start "rule__LtlTrue__Group__1__Impl"
    // InternalComposedLts.g:7470:1: rule__LtlTrue__Group__1__Impl : ( 'true' ) ;
    public final void rule__LtlTrue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7474:1: ( ( 'true' ) )
            // InternalComposedLts.g:7475:1: ( 'true' )
            {
            // InternalComposedLts.g:7475:1: ( 'true' )
            // InternalComposedLts.g:7476:1: 'true'
            {
             before(grammarAccess.getLtlTrueAccess().getTrueKeyword_1()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getLtlTrueAccess().getTrueKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlTrue__Group__1__Impl"


    // $ANTLR start "rule__LtlFalse__Group__0"
    // InternalComposedLts.g:7493:1: rule__LtlFalse__Group__0 : rule__LtlFalse__Group__0__Impl rule__LtlFalse__Group__1 ;
    public final void rule__LtlFalse__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7497:1: ( rule__LtlFalse__Group__0__Impl rule__LtlFalse__Group__1 )
            // InternalComposedLts.g:7498:2: rule__LtlFalse__Group__0__Impl rule__LtlFalse__Group__1
            {
            pushFollow(FOLLOW_36);
            rule__LtlFalse__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LtlFalse__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFalse__Group__0"


    // $ANTLR start "rule__LtlFalse__Group__0__Impl"
    // InternalComposedLts.g:7505:1: rule__LtlFalse__Group__0__Impl : ( () ) ;
    public final void rule__LtlFalse__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7509:1: ( ( () ) )
            // InternalComposedLts.g:7510:1: ( () )
            {
            // InternalComposedLts.g:7510:1: ( () )
            // InternalComposedLts.g:7511:1: ()
            {
             before(grammarAccess.getLtlFalseAccess().getLtlFalseAction_0()); 
            // InternalComposedLts.g:7512:1: ()
            // InternalComposedLts.g:7514:1: 
            {
            }

             after(grammarAccess.getLtlFalseAccess().getLtlFalseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFalse__Group__0__Impl"


    // $ANTLR start "rule__LtlFalse__Group__1"
    // InternalComposedLts.g:7524:1: rule__LtlFalse__Group__1 : rule__LtlFalse__Group__1__Impl ;
    public final void rule__LtlFalse__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7528:1: ( rule__LtlFalse__Group__1__Impl )
            // InternalComposedLts.g:7529:2: rule__LtlFalse__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LtlFalse__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFalse__Group__1"


    // $ANTLR start "rule__LtlFalse__Group__1__Impl"
    // InternalComposedLts.g:7535:1: rule__LtlFalse__Group__1__Impl : ( 'false' ) ;
    public final void rule__LtlFalse__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7539:1: ( ( 'false' ) )
            // InternalComposedLts.g:7540:1: ( 'false' )
            {
            // InternalComposedLts.g:7540:1: ( 'false' )
            // InternalComposedLts.g:7541:1: 'false'
            {
             before(grammarAccess.getLtlFalseAccess().getFalseKeyword_1()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getLtlFalseAccess().getFalseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFalse__Group__1__Impl"


    // $ANTLR start "rule__CupLabelPredicate__Group__0"
    // InternalComposedLts.g:7558:1: rule__CupLabelPredicate__Group__0 : rule__CupLabelPredicate__Group__0__Impl rule__CupLabelPredicate__Group__1 ;
    public final void rule__CupLabelPredicate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7562:1: ( rule__CupLabelPredicate__Group__0__Impl rule__CupLabelPredicate__Group__1 )
            // InternalComposedLts.g:7563:2: rule__CupLabelPredicate__Group__0__Impl rule__CupLabelPredicate__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__CupLabelPredicate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group__0"


    // $ANTLR start "rule__CupLabelPredicate__Group__0__Impl"
    // InternalComposedLts.g:7570:1: rule__CupLabelPredicate__Group__0__Impl : ( ruleCapLabelPredicate ) ;
    public final void rule__CupLabelPredicate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7574:1: ( ( ruleCapLabelPredicate ) )
            // InternalComposedLts.g:7575:1: ( ruleCapLabelPredicate )
            {
            // InternalComposedLts.g:7575:1: ( ruleCapLabelPredicate )
            // InternalComposedLts.g:7576:1: ruleCapLabelPredicate
            {
             before(grammarAccess.getCupLabelPredicateAccess().getCapLabelPredicateParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleCapLabelPredicate();

            state._fsp--;

             after(grammarAccess.getCupLabelPredicateAccess().getCapLabelPredicateParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group__0__Impl"


    // $ANTLR start "rule__CupLabelPredicate__Group__1"
    // InternalComposedLts.g:7587:1: rule__CupLabelPredicate__Group__1 : rule__CupLabelPredicate__Group__1__Impl ;
    public final void rule__CupLabelPredicate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7591:1: ( rule__CupLabelPredicate__Group__1__Impl )
            // InternalComposedLts.g:7592:2: rule__CupLabelPredicate__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group__1"


    // $ANTLR start "rule__CupLabelPredicate__Group__1__Impl"
    // InternalComposedLts.g:7598:1: rule__CupLabelPredicate__Group__1__Impl : ( ( rule__CupLabelPredicate__Group_1__0 )? ) ;
    public final void rule__CupLabelPredicate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7602:1: ( ( ( rule__CupLabelPredicate__Group_1__0 )? ) )
            // InternalComposedLts.g:7603:1: ( ( rule__CupLabelPredicate__Group_1__0 )? )
            {
            // InternalComposedLts.g:7603:1: ( ( rule__CupLabelPredicate__Group_1__0 )? )
            // InternalComposedLts.g:7604:1: ( rule__CupLabelPredicate__Group_1__0 )?
            {
             before(grammarAccess.getCupLabelPredicateAccess().getGroup_1()); 
            // InternalComposedLts.g:7605:1: ( rule__CupLabelPredicate__Group_1__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==17) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalComposedLts.g:7605:2: rule__CupLabelPredicate__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CupLabelPredicate__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCupLabelPredicateAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group__1__Impl"


    // $ANTLR start "rule__CupLabelPredicate__Group_1__0"
    // InternalComposedLts.g:7619:1: rule__CupLabelPredicate__Group_1__0 : rule__CupLabelPredicate__Group_1__0__Impl rule__CupLabelPredicate__Group_1__1 ;
    public final void rule__CupLabelPredicate__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7623:1: ( rule__CupLabelPredicate__Group_1__0__Impl rule__CupLabelPredicate__Group_1__1 )
            // InternalComposedLts.g:7624:2: rule__CupLabelPredicate__Group_1__0__Impl rule__CupLabelPredicate__Group_1__1
            {
            pushFollow(FOLLOW_12);
            rule__CupLabelPredicate__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group_1__0"


    // $ANTLR start "rule__CupLabelPredicate__Group_1__0__Impl"
    // InternalComposedLts.g:7631:1: rule__CupLabelPredicate__Group_1__0__Impl : ( () ) ;
    public final void rule__CupLabelPredicate__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7635:1: ( ( () ) )
            // InternalComposedLts.g:7636:1: ( () )
            {
            // InternalComposedLts.g:7636:1: ( () )
            // InternalComposedLts.g:7637:1: ()
            {
             before(grammarAccess.getCupLabelPredicateAccess().getCupLabelPredicateLeftAction_1_0()); 
            // InternalComposedLts.g:7638:1: ()
            // InternalComposedLts.g:7640:1: 
            {
            }

             after(grammarAccess.getCupLabelPredicateAccess().getCupLabelPredicateLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group_1__0__Impl"


    // $ANTLR start "rule__CupLabelPredicate__Group_1__1"
    // InternalComposedLts.g:7650:1: rule__CupLabelPredicate__Group_1__1 : rule__CupLabelPredicate__Group_1__1__Impl rule__CupLabelPredicate__Group_1__2 ;
    public final void rule__CupLabelPredicate__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7654:1: ( rule__CupLabelPredicate__Group_1__1__Impl rule__CupLabelPredicate__Group_1__2 )
            // InternalComposedLts.g:7655:2: rule__CupLabelPredicate__Group_1__1__Impl rule__CupLabelPredicate__Group_1__2
            {
            pushFollow(FOLLOW_37);
            rule__CupLabelPredicate__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group_1__1"


    // $ANTLR start "rule__CupLabelPredicate__Group_1__1__Impl"
    // InternalComposedLts.g:7662:1: rule__CupLabelPredicate__Group_1__1__Impl : ( '|' ) ;
    public final void rule__CupLabelPredicate__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7666:1: ( ( '|' ) )
            // InternalComposedLts.g:7667:1: ( '|' )
            {
            // InternalComposedLts.g:7667:1: ( '|' )
            // InternalComposedLts.g:7668:1: '|'
            {
             before(grammarAccess.getCupLabelPredicateAccess().getVerticalLineKeyword_1_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCupLabelPredicateAccess().getVerticalLineKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group_1__1__Impl"


    // $ANTLR start "rule__CupLabelPredicate__Group_1__2"
    // InternalComposedLts.g:7681:1: rule__CupLabelPredicate__Group_1__2 : rule__CupLabelPredicate__Group_1__2__Impl ;
    public final void rule__CupLabelPredicate__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7685:1: ( rule__CupLabelPredicate__Group_1__2__Impl )
            // InternalComposedLts.g:7686:2: rule__CupLabelPredicate__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group_1__2"


    // $ANTLR start "rule__CupLabelPredicate__Group_1__2__Impl"
    // InternalComposedLts.g:7692:1: rule__CupLabelPredicate__Group_1__2__Impl : ( ( rule__CupLabelPredicate__RighAssignment_1_2 ) ) ;
    public final void rule__CupLabelPredicate__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7696:1: ( ( ( rule__CupLabelPredicate__RighAssignment_1_2 ) ) )
            // InternalComposedLts.g:7697:1: ( ( rule__CupLabelPredicate__RighAssignment_1_2 ) )
            {
            // InternalComposedLts.g:7697:1: ( ( rule__CupLabelPredicate__RighAssignment_1_2 ) )
            // InternalComposedLts.g:7698:1: ( rule__CupLabelPredicate__RighAssignment_1_2 )
            {
             before(grammarAccess.getCupLabelPredicateAccess().getRighAssignment_1_2()); 
            // InternalComposedLts.g:7699:1: ( rule__CupLabelPredicate__RighAssignment_1_2 )
            // InternalComposedLts.g:7699:2: rule__CupLabelPredicate__RighAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__CupLabelPredicate__RighAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getCupLabelPredicateAccess().getRighAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__Group_1__2__Impl"


    // $ANTLR start "rule__CapLabelPredicate__Group__0"
    // InternalComposedLts.g:7715:1: rule__CapLabelPredicate__Group__0 : rule__CapLabelPredicate__Group__0__Impl rule__CapLabelPredicate__Group__1 ;
    public final void rule__CapLabelPredicate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7719:1: ( rule__CapLabelPredicate__Group__0__Impl rule__CapLabelPredicate__Group__1 )
            // InternalComposedLts.g:7720:2: rule__CapLabelPredicate__Group__0__Impl rule__CapLabelPredicate__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__CapLabelPredicate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group__0"


    // $ANTLR start "rule__CapLabelPredicate__Group__0__Impl"
    // InternalComposedLts.g:7727:1: rule__CapLabelPredicate__Group__0__Impl : ( ruleBaseLabelPredicate ) ;
    public final void rule__CapLabelPredicate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7731:1: ( ( ruleBaseLabelPredicate ) )
            // InternalComposedLts.g:7732:1: ( ruleBaseLabelPredicate )
            {
            // InternalComposedLts.g:7732:1: ( ruleBaseLabelPredicate )
            // InternalComposedLts.g:7733:1: ruleBaseLabelPredicate
            {
             before(grammarAccess.getCapLabelPredicateAccess().getBaseLabelPredicateParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleBaseLabelPredicate();

            state._fsp--;

             after(grammarAccess.getCapLabelPredicateAccess().getBaseLabelPredicateParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group__0__Impl"


    // $ANTLR start "rule__CapLabelPredicate__Group__1"
    // InternalComposedLts.g:7744:1: rule__CapLabelPredicate__Group__1 : rule__CapLabelPredicate__Group__1__Impl ;
    public final void rule__CapLabelPredicate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7748:1: ( rule__CapLabelPredicate__Group__1__Impl )
            // InternalComposedLts.g:7749:2: rule__CapLabelPredicate__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group__1"


    // $ANTLR start "rule__CapLabelPredicate__Group__1__Impl"
    // InternalComposedLts.g:7755:1: rule__CapLabelPredicate__Group__1__Impl : ( ( rule__CapLabelPredicate__Group_1__0 )? ) ;
    public final void rule__CapLabelPredicate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7759:1: ( ( ( rule__CapLabelPredicate__Group_1__0 )? ) )
            // InternalComposedLts.g:7760:1: ( ( rule__CapLabelPredicate__Group_1__0 )? )
            {
            // InternalComposedLts.g:7760:1: ( ( rule__CapLabelPredicate__Group_1__0 )? )
            // InternalComposedLts.g:7761:1: ( rule__CapLabelPredicate__Group_1__0 )?
            {
             before(grammarAccess.getCapLabelPredicateAccess().getGroup_1()); 
            // InternalComposedLts.g:7762:1: ( rule__CapLabelPredicate__Group_1__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==36) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalComposedLts.g:7762:2: rule__CapLabelPredicate__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CapLabelPredicate__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCapLabelPredicateAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group__1__Impl"


    // $ANTLR start "rule__CapLabelPredicate__Group_1__0"
    // InternalComposedLts.g:7776:1: rule__CapLabelPredicate__Group_1__0 : rule__CapLabelPredicate__Group_1__0__Impl rule__CapLabelPredicate__Group_1__1 ;
    public final void rule__CapLabelPredicate__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7780:1: ( rule__CapLabelPredicate__Group_1__0__Impl rule__CapLabelPredicate__Group_1__1 )
            // InternalComposedLts.g:7781:2: rule__CapLabelPredicate__Group_1__0__Impl rule__CapLabelPredicate__Group_1__1
            {
            pushFollow(FOLLOW_32);
            rule__CapLabelPredicate__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group_1__0"


    // $ANTLR start "rule__CapLabelPredicate__Group_1__0__Impl"
    // InternalComposedLts.g:7788:1: rule__CapLabelPredicate__Group_1__0__Impl : ( () ) ;
    public final void rule__CapLabelPredicate__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7792:1: ( ( () ) )
            // InternalComposedLts.g:7793:1: ( () )
            {
            // InternalComposedLts.g:7793:1: ( () )
            // InternalComposedLts.g:7794:1: ()
            {
             before(grammarAccess.getCapLabelPredicateAccess().getCapLabelPredicateLeftAction_1_0()); 
            // InternalComposedLts.g:7795:1: ()
            // InternalComposedLts.g:7797:1: 
            {
            }

             after(grammarAccess.getCapLabelPredicateAccess().getCapLabelPredicateLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group_1__0__Impl"


    // $ANTLR start "rule__CapLabelPredicate__Group_1__1"
    // InternalComposedLts.g:7807:1: rule__CapLabelPredicate__Group_1__1 : rule__CapLabelPredicate__Group_1__1__Impl rule__CapLabelPredicate__Group_1__2 ;
    public final void rule__CapLabelPredicate__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7811:1: ( rule__CapLabelPredicate__Group_1__1__Impl rule__CapLabelPredicate__Group_1__2 )
            // InternalComposedLts.g:7812:2: rule__CapLabelPredicate__Group_1__1__Impl rule__CapLabelPredicate__Group_1__2
            {
            pushFollow(FOLLOW_37);
            rule__CapLabelPredicate__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group_1__1"


    // $ANTLR start "rule__CapLabelPredicate__Group_1__1__Impl"
    // InternalComposedLts.g:7819:1: rule__CapLabelPredicate__Group_1__1__Impl : ( '&' ) ;
    public final void rule__CapLabelPredicate__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7823:1: ( ( '&' ) )
            // InternalComposedLts.g:7824:1: ( '&' )
            {
            // InternalComposedLts.g:7824:1: ( '&' )
            // InternalComposedLts.g:7825:1: '&'
            {
             before(grammarAccess.getCapLabelPredicateAccess().getAmpersandKeyword_1_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getCapLabelPredicateAccess().getAmpersandKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group_1__1__Impl"


    // $ANTLR start "rule__CapLabelPredicate__Group_1__2"
    // InternalComposedLts.g:7838:1: rule__CapLabelPredicate__Group_1__2 : rule__CapLabelPredicate__Group_1__2__Impl ;
    public final void rule__CapLabelPredicate__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7842:1: ( rule__CapLabelPredicate__Group_1__2__Impl )
            // InternalComposedLts.g:7843:2: rule__CapLabelPredicate__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group_1__2"


    // $ANTLR start "rule__CapLabelPredicate__Group_1__2__Impl"
    // InternalComposedLts.g:7849:1: rule__CapLabelPredicate__Group_1__2__Impl : ( ( rule__CapLabelPredicate__RightAssignment_1_2 ) ) ;
    public final void rule__CapLabelPredicate__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7853:1: ( ( ( rule__CapLabelPredicate__RightAssignment_1_2 ) ) )
            // InternalComposedLts.g:7854:1: ( ( rule__CapLabelPredicate__RightAssignment_1_2 ) )
            {
            // InternalComposedLts.g:7854:1: ( ( rule__CapLabelPredicate__RightAssignment_1_2 ) )
            // InternalComposedLts.g:7855:1: ( rule__CapLabelPredicate__RightAssignment_1_2 )
            {
             before(grammarAccess.getCapLabelPredicateAccess().getRightAssignment_1_2()); 
            // InternalComposedLts.g:7856:1: ( rule__CapLabelPredicate__RightAssignment_1_2 )
            // InternalComposedLts.g:7856:2: rule__CapLabelPredicate__RightAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__CapLabelPredicate__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getCapLabelPredicateAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__Group_1__2__Impl"


    // $ANTLR start "rule__NotLabelPredicate__Group__0"
    // InternalComposedLts.g:7872:1: rule__NotLabelPredicate__Group__0 : rule__NotLabelPredicate__Group__0__Impl rule__NotLabelPredicate__Group__1 ;
    public final void rule__NotLabelPredicate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7876:1: ( rule__NotLabelPredicate__Group__0__Impl rule__NotLabelPredicate__Group__1 )
            // InternalComposedLts.g:7877:2: rule__NotLabelPredicate__Group__0__Impl rule__NotLabelPredicate__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__NotLabelPredicate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotLabelPredicate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotLabelPredicate__Group__0"


    // $ANTLR start "rule__NotLabelPredicate__Group__0__Impl"
    // InternalComposedLts.g:7884:1: rule__NotLabelPredicate__Group__0__Impl : ( '!' ) ;
    public final void rule__NotLabelPredicate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7888:1: ( ( '!' ) )
            // InternalComposedLts.g:7889:1: ( '!' )
            {
            // InternalComposedLts.g:7889:1: ( '!' )
            // InternalComposedLts.g:7890:1: '!'
            {
             before(grammarAccess.getNotLabelPredicateAccess().getExclamationMarkKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getNotLabelPredicateAccess().getExclamationMarkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotLabelPredicate__Group__0__Impl"


    // $ANTLR start "rule__NotLabelPredicate__Group__1"
    // InternalComposedLts.g:7903:1: rule__NotLabelPredicate__Group__1 : rule__NotLabelPredicate__Group__1__Impl ;
    public final void rule__NotLabelPredicate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7907:1: ( rule__NotLabelPredicate__Group__1__Impl )
            // InternalComposedLts.g:7908:2: rule__NotLabelPredicate__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotLabelPredicate__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotLabelPredicate__Group__1"


    // $ANTLR start "rule__NotLabelPredicate__Group__1__Impl"
    // InternalComposedLts.g:7914:1: rule__NotLabelPredicate__Group__1__Impl : ( ( rule__NotLabelPredicate__ArgAssignment_1 ) ) ;
    public final void rule__NotLabelPredicate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7918:1: ( ( ( rule__NotLabelPredicate__ArgAssignment_1 ) ) )
            // InternalComposedLts.g:7919:1: ( ( rule__NotLabelPredicate__ArgAssignment_1 ) )
            {
            // InternalComposedLts.g:7919:1: ( ( rule__NotLabelPredicate__ArgAssignment_1 ) )
            // InternalComposedLts.g:7920:1: ( rule__NotLabelPredicate__ArgAssignment_1 )
            {
             before(grammarAccess.getNotLabelPredicateAccess().getArgAssignment_1()); 
            // InternalComposedLts.g:7921:1: ( rule__NotLabelPredicate__ArgAssignment_1 )
            // InternalComposedLts.g:7921:2: rule__NotLabelPredicate__ArgAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__NotLabelPredicate__ArgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNotLabelPredicateAccess().getArgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotLabelPredicate__Group__1__Impl"


    // $ANTLR start "rule__AnyLabelPredicate__Group__0"
    // InternalComposedLts.g:7935:1: rule__AnyLabelPredicate__Group__0 : rule__AnyLabelPredicate__Group__0__Impl rule__AnyLabelPredicate__Group__1 ;
    public final void rule__AnyLabelPredicate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7939:1: ( rule__AnyLabelPredicate__Group__0__Impl rule__AnyLabelPredicate__Group__1 )
            // InternalComposedLts.g:7940:2: rule__AnyLabelPredicate__Group__0__Impl rule__AnyLabelPredicate__Group__1
            {
            pushFollow(FOLLOW_44);
            rule__AnyLabelPredicate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AnyLabelPredicate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnyLabelPredicate__Group__0"


    // $ANTLR start "rule__AnyLabelPredicate__Group__0__Impl"
    // InternalComposedLts.g:7947:1: rule__AnyLabelPredicate__Group__0__Impl : ( () ) ;
    public final void rule__AnyLabelPredicate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7951:1: ( ( () ) )
            // InternalComposedLts.g:7952:1: ( () )
            {
            // InternalComposedLts.g:7952:1: ( () )
            // InternalComposedLts.g:7953:1: ()
            {
             before(grammarAccess.getAnyLabelPredicateAccess().getAnyLabelPredicateAction_0()); 
            // InternalComposedLts.g:7954:1: ()
            // InternalComposedLts.g:7956:1: 
            {
            }

             after(grammarAccess.getAnyLabelPredicateAccess().getAnyLabelPredicateAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnyLabelPredicate__Group__0__Impl"


    // $ANTLR start "rule__AnyLabelPredicate__Group__1"
    // InternalComposedLts.g:7966:1: rule__AnyLabelPredicate__Group__1 : rule__AnyLabelPredicate__Group__1__Impl ;
    public final void rule__AnyLabelPredicate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7970:1: ( rule__AnyLabelPredicate__Group__1__Impl )
            // InternalComposedLts.g:7971:2: rule__AnyLabelPredicate__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AnyLabelPredicate__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnyLabelPredicate__Group__1"


    // $ANTLR start "rule__AnyLabelPredicate__Group__1__Impl"
    // InternalComposedLts.g:7977:1: rule__AnyLabelPredicate__Group__1__Impl : ( '*' ) ;
    public final void rule__AnyLabelPredicate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:7981:1: ( ( '*' ) )
            // InternalComposedLts.g:7982:1: ( '*' )
            {
            // InternalComposedLts.g:7982:1: ( '*' )
            // InternalComposedLts.g:7983:1: '*'
            {
             before(grammarAccess.getAnyLabelPredicateAccess().getAsteriskKeyword_1()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getAnyLabelPredicateAccess().getAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnyLabelPredicate__Group__1__Impl"


    // $ANTLR start "rule__Model__NameAssignment_2"
    // InternalComposedLts.g:8001:1: rule__Model__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__Model__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8005:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8006:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8006:1: ( RULE_ID )
            // InternalComposedLts.g:8007:1: RULE_ID
            {
             before(grammarAccess.getModelAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__NameAssignment_2"


    // $ANTLR start "rule__Model__ElementsAssignment_4"
    // InternalComposedLts.g:8016:1: rule__Model__ElementsAssignment_4 : ( ruleElement ) ;
    public final void rule__Model__ElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8020:1: ( ( ruleElement ) )
            // InternalComposedLts.g:8021:1: ( ruleElement )
            {
            // InternalComposedLts.g:8021:1: ( ruleElement )
            // InternalComposedLts.g:8022:1: ruleElement
            {
             before(grammarAccess.getModelAccess().getElementsElementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleElement();

            state._fsp--;

             after(grammarAccess.getModelAccess().getElementsElementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__ElementsAssignment_4"


    // $ANTLR start "rule__Lts__NameAssignment_1"
    // InternalComposedLts.g:8031:1: rule__Lts__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Lts__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8035:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8036:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8036:1: ( RULE_ID )
            // InternalComposedLts.g:8037:1: RULE_ID
            {
             before(grammarAccess.getLtsAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__NameAssignment_1"


    // $ANTLR start "rule__Lts__BodyAssignment_2"
    // InternalComposedLts.g:8046:1: rule__Lts__BodyAssignment_2 : ( ruleLtsBody ) ;
    public final void rule__Lts__BodyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8050:1: ( ( ruleLtsBody ) )
            // InternalComposedLts.g:8051:1: ( ruleLtsBody )
            {
            // InternalComposedLts.g:8051:1: ( ruleLtsBody )
            // InternalComposedLts.g:8052:1: ruleLtsBody
            {
             before(grammarAccess.getLtsAccess().getBodyLtsBodyParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLtsBody();

            state._fsp--;

             after(grammarAccess.getLtsAccess().getBodyLtsBodyParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lts__BodyAssignment_2"


    // $ANTLR start "rule__LtsComposition__BodyAssignment_1"
    // InternalComposedLts.g:8061:1: rule__LtsComposition__BodyAssignment_1 : ( ruleCompositionBody ) ;
    public final void rule__LtsComposition__BodyAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8065:1: ( ( ruleCompositionBody ) )
            // InternalComposedLts.g:8066:1: ( ruleCompositionBody )
            {
            // InternalComposedLts.g:8066:1: ( ruleCompositionBody )
            // InternalComposedLts.g:8067:1: ruleCompositionBody
            {
             before(grammarAccess.getLtsCompositionAccess().getBodyCompositionBodyParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleCompositionBody();

            state._fsp--;

             after(grammarAccess.getLtsCompositionAccess().getBodyCompositionBodyParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsComposition__BodyAssignment_1"


    // $ANTLR start "rule__Interleaving__RightAssignment_1_2"
    // InternalComposedLts.g:8076:1: rule__Interleaving__RightAssignment_1_2 : ( ruleInterleaving ) ;
    public final void rule__Interleaving__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8080:1: ( ( ruleInterleaving ) )
            // InternalComposedLts.g:8081:1: ( ruleInterleaving )
            {
            // InternalComposedLts.g:8081:1: ( ruleInterleaving )
            // InternalComposedLts.g:8082:1: ruleInterleaving
            {
             before(grammarAccess.getInterleavingAccess().getRightInterleavingParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInterleaving();

            state._fsp--;

             after(grammarAccess.getInterleavingAccess().getRightInterleavingParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interleaving__RightAssignment_1_2"


    // $ANTLR start "rule__Synchronization__RightAssignment_1_2"
    // InternalComposedLts.g:8091:1: rule__Synchronization__RightAssignment_1_2 : ( ruleSynchronization ) ;
    public final void rule__Synchronization__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8095:1: ( ( ruleSynchronization ) )
            // InternalComposedLts.g:8096:1: ( ruleSynchronization )
            {
            // InternalComposedLts.g:8096:1: ( ruleSynchronization )
            // InternalComposedLts.g:8097:1: ruleSynchronization
            {
             before(grammarAccess.getSynchronizationAccess().getRightSynchronizationParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleSynchronization();

            state._fsp--;

             after(grammarAccess.getSynchronizationAccess().getRightSynchronizationParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Synchronization__RightAssignment_1_2"


    // $ANTLR start "rule__ControlledInteraction__ActionAssignment_1_2_0"
    // InternalComposedLts.g:8106:1: rule__ControlledInteraction__ActionAssignment_1_2_0 : ( ( RULE_ID ) ) ;
    public final void rule__ControlledInteraction__ActionAssignment_1_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8110:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8111:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8111:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8112:1: ( RULE_ID )
            {
             before(grammarAccess.getControlledInteractionAccess().getActionActionCrossReference_1_2_0_0()); 
            // InternalComposedLts.g:8113:1: ( RULE_ID )
            // InternalComposedLts.g:8114:1: RULE_ID
            {
             before(grammarAccess.getControlledInteractionAccess().getActionActionIDTerminalRuleCall_1_2_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getControlledInteractionAccess().getActionActionIDTerminalRuleCall_1_2_0_0_1()); 

            }

             after(grammarAccess.getControlledInteractionAccess().getActionActionCrossReference_1_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__ActionAssignment_1_2_0"


    // $ANTLR start "rule__ControlledInteraction__ActionAssignment_1_2_1_1"
    // InternalComposedLts.g:8125:1: rule__ControlledInteraction__ActionAssignment_1_2_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__ControlledInteraction__ActionAssignment_1_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8129:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8130:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8130:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8131:1: ( RULE_ID )
            {
             before(grammarAccess.getControlledInteractionAccess().getActionActionCrossReference_1_2_1_1_0()); 
            // InternalComposedLts.g:8132:1: ( RULE_ID )
            // InternalComposedLts.g:8133:1: RULE_ID
            {
             before(grammarAccess.getControlledInteractionAccess().getActionActionIDTerminalRuleCall_1_2_1_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getControlledInteractionAccess().getActionActionIDTerminalRuleCall_1_2_1_1_0_1()); 

            }

             after(grammarAccess.getControlledInteractionAccess().getActionActionCrossReference_1_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__ActionAssignment_1_2_1_1"


    // $ANTLR start "rule__ControlledInteraction__RightAssignment_1_4"
    // InternalComposedLts.g:8144:1: rule__ControlledInteraction__RightAssignment_1_4 : ( ruleControlledInteraction ) ;
    public final void rule__ControlledInteraction__RightAssignment_1_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8148:1: ( ( ruleControlledInteraction ) )
            // InternalComposedLts.g:8149:1: ( ruleControlledInteraction )
            {
            // InternalComposedLts.g:8149:1: ( ruleControlledInteraction )
            // InternalComposedLts.g:8150:1: ruleControlledInteraction
            {
             before(grammarAccess.getControlledInteractionAccess().getRightControlledInteractionParserRuleCall_1_4_0()); 
            pushFollow(FOLLOW_2);
            ruleControlledInteraction();

            state._fsp--;

             after(grammarAccess.getControlledInteractionAccess().getRightControlledInteractionParserRuleCall_1_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ControlledInteraction__RightAssignment_1_4"


    // $ANTLR start "rule__Renaming__MapsAssignment_1_2"
    // InternalComposedLts.g:8159:1: rule__Renaming__MapsAssignment_1_2 : ( ruleMapping ) ;
    public final void rule__Renaming__MapsAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8163:1: ( ( ruleMapping ) )
            // InternalComposedLts.g:8164:1: ( ruleMapping )
            {
            // InternalComposedLts.g:8164:1: ( ruleMapping )
            // InternalComposedLts.g:8165:1: ruleMapping
            {
             before(grammarAccess.getRenamingAccess().getMapsMappingParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleMapping();

            state._fsp--;

             after(grammarAccess.getRenamingAccess().getMapsMappingParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__MapsAssignment_1_2"


    // $ANTLR start "rule__Renaming__MapsAssignment_1_3_1"
    // InternalComposedLts.g:8174:1: rule__Renaming__MapsAssignment_1_3_1 : ( ruleMapping ) ;
    public final void rule__Renaming__MapsAssignment_1_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8178:1: ( ( ruleMapping ) )
            // InternalComposedLts.g:8179:1: ( ruleMapping )
            {
            // InternalComposedLts.g:8179:1: ( ruleMapping )
            // InternalComposedLts.g:8180:1: ruleMapping
            {
             before(grammarAccess.getRenamingAccess().getMapsMappingParserRuleCall_1_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleMapping();

            state._fsp--;

             after(grammarAccess.getRenamingAccess().getMapsMappingParserRuleCall_1_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Renaming__MapsAssignment_1_3_1"


    // $ANTLR start "rule__Reference__LtsAssignment"
    // InternalComposedLts.g:8189:1: rule__Reference__LtsAssignment : ( ( RULE_ID ) ) ;
    public final void rule__Reference__LtsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8193:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8194:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8194:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8195:1: ( RULE_ID )
            {
             before(grammarAccess.getReferenceAccess().getLtsLtsCrossReference_0()); 
            // InternalComposedLts.g:8196:1: ( RULE_ID )
            // InternalComposedLts.g:8197:1: RULE_ID
            {
             before(grammarAccess.getReferenceAccess().getLtsLtsIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getReferenceAccess().getLtsLtsIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getLtsLtsCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__LtsAssignment"


    // $ANTLR start "rule__Mapping__SrcAssignment_0"
    // InternalComposedLts.g:8208:1: rule__Mapping__SrcAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Mapping__SrcAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8212:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8213:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8213:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8214:1: ( RULE_ID )
            {
             before(grammarAccess.getMappingAccess().getSrcActionCrossReference_0_0()); 
            // InternalComposedLts.g:8215:1: ( RULE_ID )
            // InternalComposedLts.g:8216:1: RULE_ID
            {
             before(grammarAccess.getMappingAccess().getSrcActionIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getSrcActionIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getMappingAccess().getSrcActionCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__SrcAssignment_0"


    // $ANTLR start "rule__Mapping__TrgAssignment_2"
    // InternalComposedLts.g:8227:1: rule__Mapping__TrgAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Mapping__TrgAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8231:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8232:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8232:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8233:1: ( RULE_ID )
            {
             before(grammarAccess.getMappingAccess().getTrgActionCrossReference_2_0()); 
            // InternalComposedLts.g:8234:1: ( RULE_ID )
            // InternalComposedLts.g:8235:1: RULE_ID
            {
             before(grammarAccess.getMappingAccess().getTrgActionIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMappingAccess().getTrgActionIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getMappingAccess().getTrgActionCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mapping__TrgAssignment_2"


    // $ANTLR start "rule__LtsDeclarationBody__StatesAssignment_3"
    // InternalComposedLts.g:8246:1: rule__LtsDeclarationBody__StatesAssignment_3 : ( ruleLtsState ) ;
    public final void rule__LtsDeclarationBody__StatesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8250:1: ( ( ruleLtsState ) )
            // InternalComposedLts.g:8251:1: ( ruleLtsState )
            {
            // InternalComposedLts.g:8251:1: ( ruleLtsState )
            // InternalComposedLts.g:8252:1: ruleLtsState
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getStatesLtsStateParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLtsState();

            state._fsp--;

             after(grammarAccess.getLtsDeclarationBodyAccess().getStatesLtsStateParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__StatesAssignment_3"


    // $ANTLR start "rule__LtsDeclarationBody__StatesAssignment_4_1"
    // InternalComposedLts.g:8261:1: rule__LtsDeclarationBody__StatesAssignment_4_1 : ( ruleLtsState ) ;
    public final void rule__LtsDeclarationBody__StatesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8265:1: ( ( ruleLtsState ) )
            // InternalComposedLts.g:8266:1: ( ruleLtsState )
            {
            // InternalComposedLts.g:8266:1: ( ruleLtsState )
            // InternalComposedLts.g:8267:1: ruleLtsState
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getStatesLtsStateParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLtsState();

            state._fsp--;

             after(grammarAccess.getLtsDeclarationBodyAccess().getStatesLtsStateParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__StatesAssignment_4_1"


    // $ANTLR start "rule__LtsDeclarationBody__InitAssignment_8"
    // InternalComposedLts.g:8276:1: rule__LtsDeclarationBody__InitAssignment_8 : ( ( RULE_ID ) ) ;
    public final void rule__LtsDeclarationBody__InitAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8280:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8281:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8281:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8282:1: ( RULE_ID )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateCrossReference_8_0()); 
            // InternalComposedLts.g:8283:1: ( RULE_ID )
            // InternalComposedLts.g:8284:1: RULE_ID
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateIDTerminalRuleCall_8_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateIDTerminalRuleCall_8_0_1()); 

            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateCrossReference_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__InitAssignment_8"


    // $ANTLR start "rule__LtsDeclarationBody__InitAssignment_9_1"
    // InternalComposedLts.g:8295:1: rule__LtsDeclarationBody__InitAssignment_9_1 : ( ( RULE_ID ) ) ;
    public final void rule__LtsDeclarationBody__InitAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8299:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8300:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8300:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8301:1: ( RULE_ID )
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateCrossReference_9_1_0()); 
            // InternalComposedLts.g:8302:1: ( RULE_ID )
            // InternalComposedLts.g:8303:1: RULE_ID
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateIDTerminalRuleCall_9_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateIDTerminalRuleCall_9_1_0_1()); 

            }

             after(grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateCrossReference_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__InitAssignment_9_1"


    // $ANTLR start "rule__LtsDeclarationBody__RulesAssignment_11_2"
    // InternalComposedLts.g:8314:1: rule__LtsDeclarationBody__RulesAssignment_11_2 : ( ruleLtsRule ) ;
    public final void rule__LtsDeclarationBody__RulesAssignment_11_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8318:1: ( ( ruleLtsRule ) )
            // InternalComposedLts.g:8319:1: ( ruleLtsRule )
            {
            // InternalComposedLts.g:8319:1: ( ruleLtsRule )
            // InternalComposedLts.g:8320:1: ruleLtsRule
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getRulesLtsRuleParserRuleCall_11_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLtsRule();

            state._fsp--;

             after(grammarAccess.getLtsDeclarationBodyAccess().getRulesLtsRuleParserRuleCall_11_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__RulesAssignment_11_2"


    // $ANTLR start "rule__LtsDeclarationBody__LabelsAssignment_12_2"
    // InternalComposedLts.g:8329:1: rule__LtsDeclarationBody__LabelsAssignment_12_2 : ( ruleLabelRule ) ;
    public final void rule__LtsDeclarationBody__LabelsAssignment_12_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8333:1: ( ( ruleLabelRule ) )
            // InternalComposedLts.g:8334:1: ( ruleLabelRule )
            {
            // InternalComposedLts.g:8334:1: ( ruleLabelRule )
            // InternalComposedLts.g:8335:1: ruleLabelRule
            {
             before(grammarAccess.getLtsDeclarationBodyAccess().getLabelsLabelRuleParserRuleCall_12_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLabelRule();

            state._fsp--;

             after(grammarAccess.getLtsDeclarationBodyAccess().getLabelsLabelRuleParserRuleCall_12_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsDeclarationBody__LabelsAssignment_12_2"


    // $ANTLR start "rule__LtsRule__SrcAssignment_0"
    // InternalComposedLts.g:8344:1: rule__LtsRule__SrcAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__LtsRule__SrcAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8348:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8349:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8349:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8350:1: ( RULE_ID )
            {
             before(grammarAccess.getLtsRuleAccess().getSrcLtsStateCrossReference_0_0()); 
            // InternalComposedLts.g:8351:1: ( RULE_ID )
            // InternalComposedLts.g:8352:1: RULE_ID
            {
             before(grammarAccess.getLtsRuleAccess().getSrcLtsStateIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsRuleAccess().getSrcLtsStateIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getLtsRuleAccess().getSrcLtsStateCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__SrcAssignment_0"


    // $ANTLR start "rule__LtsRule__ActAssignment_2"
    // InternalComposedLts.g:8363:1: rule__LtsRule__ActAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__LtsRule__ActAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8367:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8368:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8368:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8369:1: ( RULE_ID )
            {
             before(grammarAccess.getLtsRuleAccess().getActActionCrossReference_2_0()); 
            // InternalComposedLts.g:8370:1: ( RULE_ID )
            // InternalComposedLts.g:8371:1: RULE_ID
            {
             before(grammarAccess.getLtsRuleAccess().getActActionIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsRuleAccess().getActActionIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getLtsRuleAccess().getActActionCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__ActAssignment_2"


    // $ANTLR start "rule__LtsRule__TrgAssignment_4"
    // InternalComposedLts.g:8382:1: rule__LtsRule__TrgAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__LtsRule__TrgAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8386:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8387:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8387:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8388:1: ( RULE_ID )
            {
             before(grammarAccess.getLtsRuleAccess().getTrgLtsStateCrossReference_4_0()); 
            // InternalComposedLts.g:8389:1: ( RULE_ID )
            // InternalComposedLts.g:8390:1: RULE_ID
            {
             before(grammarAccess.getLtsRuleAccess().getTrgLtsStateIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsRuleAccess().getTrgLtsStateIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getLtsRuleAccess().getTrgLtsStateCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsRule__TrgAssignment_4"


    // $ANTLR start "rule__LtsState__NameAssignment"
    // InternalComposedLts.g:8401:1: rule__LtsState__NameAssignment : ( RULE_ID ) ;
    public final void rule__LtsState__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8405:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8406:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8406:1: ( RULE_ID )
            // InternalComposedLts.g:8407:1: RULE_ID
            {
             before(grammarAccess.getLtsStateAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtsStateAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtsState__NameAssignment"


    // $ANTLR start "rule__LabelRule__LabelAssignment_0"
    // InternalComposedLts.g:8416:1: rule__LabelRule__LabelAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__LabelRule__LabelAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8420:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8421:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8421:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8422:1: ( RULE_ID )
            {
             before(grammarAccess.getLabelRuleAccess().getLabelLabelCrossReference_0_0()); 
            // InternalComposedLts.g:8423:1: ( RULE_ID )
            // InternalComposedLts.g:8424:1: RULE_ID
            {
             before(grammarAccess.getLabelRuleAccess().getLabelLabelIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLabelRuleAccess().getLabelLabelIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getLabelRuleAccess().getLabelLabelCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__LabelAssignment_0"


    // $ANTLR start "rule__LabelRule__StatesAssignment_2_0"
    // InternalComposedLts.g:8435:1: rule__LabelRule__StatesAssignment_2_0 : ( ( RULE_ID ) ) ;
    public final void rule__LabelRule__StatesAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8439:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8440:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8440:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8441:1: ( RULE_ID )
            {
             before(grammarAccess.getLabelRuleAccess().getStatesLtsStateCrossReference_2_0_0()); 
            // InternalComposedLts.g:8442:1: ( RULE_ID )
            // InternalComposedLts.g:8443:1: RULE_ID
            {
             before(grammarAccess.getLabelRuleAccess().getStatesLtsStateIDTerminalRuleCall_2_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLabelRuleAccess().getStatesLtsStateIDTerminalRuleCall_2_0_0_1()); 

            }

             after(grammarAccess.getLabelRuleAccess().getStatesLtsStateCrossReference_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__StatesAssignment_2_0"


    // $ANTLR start "rule__LabelRule__StatesAssignment_2_1_1"
    // InternalComposedLts.g:8454:1: rule__LabelRule__StatesAssignment_2_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__LabelRule__StatesAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8458:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8459:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8459:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8460:1: ( RULE_ID )
            {
             before(grammarAccess.getLabelRuleAccess().getStatesLtsStateCrossReference_2_1_1_0()); 
            // InternalComposedLts.g:8461:1: ( RULE_ID )
            // InternalComposedLts.g:8462:1: RULE_ID
            {
             before(grammarAccess.getLabelRuleAccess().getStatesLtsStateIDTerminalRuleCall_2_1_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLabelRuleAccess().getStatesLtsStateIDTerminalRuleCall_2_1_1_0_1()); 

            }

             after(grammarAccess.getLabelRuleAccess().getStatesLtsStateCrossReference_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LabelRule__StatesAssignment_2_1_1"


    // $ANTLR start "rule__Rule__ActionAssignment_0"
    // InternalComposedLts.g:8476:1: rule__Rule__ActionAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Rule__ActionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8480:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8481:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8481:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8482:1: ( RULE_ID )
            {
             before(grammarAccess.getRuleAccess().getActionActionCrossReference_0_0()); 
            // InternalComposedLts.g:8483:1: ( RULE_ID )
            // InternalComposedLts.g:8484:1: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getActionActionIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getActionActionIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getRuleAccess().getActionActionCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__ActionAssignment_0"


    // $ANTLR start "rule__Rule__NextAssignment_2"
    // InternalComposedLts.g:8495:1: rule__Rule__NextAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Rule__NextAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8499:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8500:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8500:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8501:1: ( RULE_ID )
            {
             before(grammarAccess.getRuleAccess().getNextStateCrossReference_2_0()); 
            // InternalComposedLts.g:8502:1: ( RULE_ID )
            // InternalComposedLts.g:8503:1: RULE_ID
            {
             before(grammarAccess.getRuleAccess().getNextStateIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRuleAccess().getNextStateIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getRuleAccess().getNextStateCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Rule__NextAssignment_2"


    // $ANTLR start "rule__Label__NameAssignment_1"
    // InternalComposedLts.g:8514:1: rule__Label__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Label__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8518:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8519:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8519:1: ( RULE_ID )
            // InternalComposedLts.g:8520:1: RULE_ID
            {
             before(grammarAccess.getLabelAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLabelAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Label__NameAssignment_1"


    // $ANTLR start "rule__Action__NameAssignment_1"
    // InternalComposedLts.g:8529:1: rule__Action__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Action__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8533:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8534:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8534:1: ( RULE_ID )
            // InternalComposedLts.g:8535:1: RULE_ID
            {
             before(grammarAccess.getActionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Action__NameAssignment_1"


    // $ANTLR start "rule__HmlFormulaDeclaration__NameAssignment_2"
    // InternalComposedLts.g:8544:1: rule__HmlFormulaDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__HmlFormulaDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8548:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8549:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8549:1: ( RULE_ID )
            // InternalComposedLts.g:8550:1: RULE_ID
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHmlFormulaDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__NameAssignment_2"


    // $ANTLR start "rule__HmlFormulaDeclaration__FormulaAssignment_4"
    // InternalComposedLts.g:8559:1: rule__HmlFormulaDeclaration__FormulaAssignment_4 : ( ruleHmlFormula ) ;
    public final void rule__HmlFormulaDeclaration__FormulaAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8563:1: ( ( ruleHmlFormula ) )
            // InternalComposedLts.g:8564:1: ( ruleHmlFormula )
            {
            // InternalComposedLts.g:8564:1: ( ruleHmlFormula )
            // InternalComposedLts.g:8565:1: ruleHmlFormula
            {
             before(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaHmlFormulaParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlFormula();

            state._fsp--;

             after(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaHmlFormulaParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlFormulaDeclaration__FormulaAssignment_4"


    // $ANTLR start "rule__HmlOrFormula__RightAssignment_1_2"
    // InternalComposedLts.g:8574:1: rule__HmlOrFormula__RightAssignment_1_2 : ( ruleHmlAndFormula ) ;
    public final void rule__HmlOrFormula__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8578:1: ( ( ruleHmlAndFormula ) )
            // InternalComposedLts.g:8579:1: ( ruleHmlAndFormula )
            {
            // InternalComposedLts.g:8579:1: ( ruleHmlAndFormula )
            // InternalComposedLts.g:8580:1: ruleHmlAndFormula
            {
             before(grammarAccess.getHmlOrFormulaAccess().getRightHmlAndFormulaParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlAndFormula();

            state._fsp--;

             after(grammarAccess.getHmlOrFormulaAccess().getRightHmlAndFormulaParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlOrFormula__RightAssignment_1_2"


    // $ANTLR start "rule__HmlAndFormula__RightAssignment_1_2"
    // InternalComposedLts.g:8589:1: rule__HmlAndFormula__RightAssignment_1_2 : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlAndFormula__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8593:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:8594:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:8594:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:8595:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlAndFormulaAccess().getRightHmlBaseFormulaParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlAndFormulaAccess().getRightHmlBaseFormulaParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlAndFormula__RightAssignment_1_2"


    // $ANTLR start "rule__HmlPredicate__LabelAssignment_1"
    // InternalComposedLts.g:8604:1: rule__HmlPredicate__LabelAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__HmlPredicate__LabelAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8608:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8609:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8609:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8610:1: ( RULE_ID )
            {
             before(grammarAccess.getHmlPredicateAccess().getLabelLabelCrossReference_1_0()); 
            // InternalComposedLts.g:8611:1: ( RULE_ID )
            // InternalComposedLts.g:8612:1: RULE_ID
            {
             before(grammarAccess.getHmlPredicateAccess().getLabelLabelIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHmlPredicateAccess().getLabelLabelIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getHmlPredicateAccess().getLabelLabelCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlPredicate__LabelAssignment_1"


    // $ANTLR start "rule__HmlNotFormula__ArgAssignment_1"
    // InternalComposedLts.g:8623:1: rule__HmlNotFormula__ArgAssignment_1 : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlNotFormula__ArgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8627:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:8628:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:8628:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:8629:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlNotFormulaAccess().getArgHmlBaseFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlNotFormulaAccess().getArgHmlBaseFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlNotFormula__ArgAssignment_1"


    // $ANTLR start "rule__HmlDiamondFormula__ActionAssignment_1"
    // InternalComposedLts.g:8638:1: rule__HmlDiamondFormula__ActionAssignment_1 : ( ruleLabelPredicate ) ;
    public final void rule__HmlDiamondFormula__ActionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8642:1: ( ( ruleLabelPredicate ) )
            // InternalComposedLts.g:8643:1: ( ruleLabelPredicate )
            {
            // InternalComposedLts.g:8643:1: ( ruleLabelPredicate )
            // InternalComposedLts.g:8644:1: ruleLabelPredicate
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getActionLabelPredicateParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLabelPredicate();

            state._fsp--;

             after(grammarAccess.getHmlDiamondFormulaAccess().getActionLabelPredicateParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__ActionAssignment_1"


    // $ANTLR start "rule__HmlDiamondFormula__ArgAssignment_3"
    // InternalComposedLts.g:8653:1: rule__HmlDiamondFormula__ArgAssignment_3 : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlDiamondFormula__ArgAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8657:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:8658:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:8658:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:8659:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlDiamondFormulaAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlDiamondFormulaAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlDiamondFormula__ArgAssignment_3"


    // $ANTLR start "rule__HmlBoxFormula__ActionAssignment_1"
    // InternalComposedLts.g:8668:1: rule__HmlBoxFormula__ActionAssignment_1 : ( ruleLabelPredicate ) ;
    public final void rule__HmlBoxFormula__ActionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8672:1: ( ( ruleLabelPredicate ) )
            // InternalComposedLts.g:8673:1: ( ruleLabelPredicate )
            {
            // InternalComposedLts.g:8673:1: ( ruleLabelPredicate )
            // InternalComposedLts.g:8674:1: ruleLabelPredicate
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getActionLabelPredicateParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLabelPredicate();

            state._fsp--;

             after(grammarAccess.getHmlBoxFormulaAccess().getActionLabelPredicateParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__ActionAssignment_1"


    // $ANTLR start "rule__HmlBoxFormula__ArgAssignment_3"
    // InternalComposedLts.g:8683:1: rule__HmlBoxFormula__ArgAssignment_3 : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlBoxFormula__ArgAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8687:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:8688:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:8688:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:8689:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlBoxFormulaAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlBoxFormulaAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlBoxFormula__ArgAssignment_3"


    // $ANTLR start "rule__HmlMinFixPoint__NameAssignment_1"
    // InternalComposedLts.g:8698:1: rule__HmlMinFixPoint__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__HmlMinFixPoint__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8702:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8703:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8703:1: ( RULE_ID )
            // InternalComposedLts.g:8704:1: RULE_ID
            {
             before(grammarAccess.getHmlMinFixPointAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHmlMinFixPointAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__NameAssignment_1"


    // $ANTLR start "rule__HmlMinFixPoint__ArgAssignment_3"
    // InternalComposedLts.g:8713:1: rule__HmlMinFixPoint__ArgAssignment_3 : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlMinFixPoint__ArgAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8717:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:8718:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:8718:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:8719:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlMinFixPointAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlMinFixPointAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMinFixPoint__ArgAssignment_3"


    // $ANTLR start "rule__HmlMaxFixPoint__NameAssignment_1"
    // InternalComposedLts.g:8728:1: rule__HmlMaxFixPoint__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__HmlMaxFixPoint__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8732:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8733:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8733:1: ( RULE_ID )
            // InternalComposedLts.g:8734:1: RULE_ID
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHmlMaxFixPointAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__NameAssignment_1"


    // $ANTLR start "rule__HmlMaxFixPoint__ArgAssignment_3"
    // InternalComposedLts.g:8743:1: rule__HmlMaxFixPoint__ArgAssignment_3 : ( ruleHmlBaseFormula ) ;
    public final void rule__HmlMaxFixPoint__ArgAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8747:1: ( ( ruleHmlBaseFormula ) )
            // InternalComposedLts.g:8748:1: ( ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:8748:1: ( ruleHmlBaseFormula )
            // InternalComposedLts.g:8749:1: ruleHmlBaseFormula
            {
             before(grammarAccess.getHmlMaxFixPointAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleHmlBaseFormula();

            state._fsp--;

             after(grammarAccess.getHmlMaxFixPointAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlMaxFixPoint__ArgAssignment_3"


    // $ANTLR start "rule__HmlRecursionVariable__RefeerenceAssignment"
    // InternalComposedLts.g:8758:1: rule__HmlRecursionVariable__RefeerenceAssignment : ( ( RULE_ID ) ) ;
    public final void rule__HmlRecursionVariable__RefeerenceAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8762:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8763:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8763:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8764:1: ( RULE_ID )
            {
             before(grammarAccess.getHmlRecursionVariableAccess().getRefeerenceHmlRecursionFormulaCrossReference_0()); 
            // InternalComposedLts.g:8765:1: ( RULE_ID )
            // InternalComposedLts.g:8766:1: RULE_ID
            {
             before(grammarAccess.getHmlRecursionVariableAccess().getRefeerenceHmlRecursionFormulaIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getHmlRecursionVariableAccess().getRefeerenceHmlRecursionFormulaIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getHmlRecursionVariableAccess().getRefeerenceHmlRecursionFormulaCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HmlRecursionVariable__RefeerenceAssignment"


    // $ANTLR start "rule__LtlFormulaDeclaration__NameAssignment_2"
    // InternalComposedLts.g:8777:1: rule__LtlFormulaDeclaration__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__LtlFormulaDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8781:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8782:1: ( RULE_ID )
            {
            // InternalComposedLts.g:8782:1: ( RULE_ID )
            // InternalComposedLts.g:8783:1: RULE_ID
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtlFormulaDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__NameAssignment_2"


    // $ANTLR start "rule__LtlFormulaDeclaration__FormulaAssignment_4"
    // InternalComposedLts.g:8792:1: rule__LtlFormulaDeclaration__FormulaAssignment_4 : ( ruleLtlFormula ) ;
    public final void rule__LtlFormulaDeclaration__FormulaAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8796:1: ( ( ruleLtlFormula ) )
            // InternalComposedLts.g:8797:1: ( ruleLtlFormula )
            {
            // InternalComposedLts.g:8797:1: ( ruleLtlFormula )
            // InternalComposedLts.g:8798:1: ruleLtlFormula
            {
             before(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaLtlFormulaParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlFormula();

            state._fsp--;

             after(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaLtlFormulaParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlFormulaDeclaration__FormulaAssignment_4"


    // $ANTLR start "rule__LtlOrFormula__RightAssignment_1_2"
    // InternalComposedLts.g:8807:1: rule__LtlOrFormula__RightAssignment_1_2 : ( ruleLtlAndFormula ) ;
    public final void rule__LtlOrFormula__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8811:1: ( ( ruleLtlAndFormula ) )
            // InternalComposedLts.g:8812:1: ( ruleLtlAndFormula )
            {
            // InternalComposedLts.g:8812:1: ( ruleLtlAndFormula )
            // InternalComposedLts.g:8813:1: ruleLtlAndFormula
            {
             before(grammarAccess.getLtlOrFormulaAccess().getRightLtlAndFormulaParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlAndFormula();

            state._fsp--;

             after(grammarAccess.getLtlOrFormulaAccess().getRightLtlAndFormulaParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlOrFormula__RightAssignment_1_2"


    // $ANTLR start "rule__LtlAndFormula__RightAssignment_1_2"
    // InternalComposedLts.g:8822:1: rule__LtlAndFormula__RightAssignment_1_2 : ( ruleLtlUntilFormula ) ;
    public final void rule__LtlAndFormula__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8826:1: ( ( ruleLtlUntilFormula ) )
            // InternalComposedLts.g:8827:1: ( ruleLtlUntilFormula )
            {
            // InternalComposedLts.g:8827:1: ( ruleLtlUntilFormula )
            // InternalComposedLts.g:8828:1: ruleLtlUntilFormula
            {
             before(grammarAccess.getLtlAndFormulaAccess().getRightLtlUntilFormulaParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlUntilFormula();

            state._fsp--;

             after(grammarAccess.getLtlAndFormulaAccess().getRightLtlUntilFormulaParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAndFormula__RightAssignment_1_2"


    // $ANTLR start "rule__LtlUntilFormula__RightAssignment_1_2"
    // InternalComposedLts.g:8837:1: rule__LtlUntilFormula__RightAssignment_1_2 : ( ruleLtlBaseFormula ) ;
    public final void rule__LtlUntilFormula__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8841:1: ( ( ruleLtlBaseFormula ) )
            // InternalComposedLts.g:8842:1: ( ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:8842:1: ( ruleLtlBaseFormula )
            // InternalComposedLts.g:8843:1: ruleLtlBaseFormula
            {
             before(grammarAccess.getLtlUntilFormulaAccess().getRightLtlBaseFormulaParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlUntilFormulaAccess().getRightLtlBaseFormulaParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlUntilFormula__RightAssignment_1_2"


    // $ANTLR start "rule__LtlAtomicProposition__PropAssignment"
    // InternalComposedLts.g:8852:1: rule__LtlAtomicProposition__PropAssignment : ( ( RULE_ID ) ) ;
    public final void rule__LtlAtomicProposition__PropAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8856:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8857:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8857:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8858:1: ( RULE_ID )
            {
             before(grammarAccess.getLtlAtomicPropositionAccess().getPropLabelCrossReference_0()); 
            // InternalComposedLts.g:8859:1: ( RULE_ID )
            // InternalComposedLts.g:8860:1: RULE_ID
            {
             before(grammarAccess.getLtlAtomicPropositionAccess().getPropLabelIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLtlAtomicPropositionAccess().getPropLabelIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getLtlAtomicPropositionAccess().getPropLabelCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAtomicProposition__PropAssignment"


    // $ANTLR start "rule__LtlNextFormula__ArgAssignment_2"
    // InternalComposedLts.g:8871:1: rule__LtlNextFormula__ArgAssignment_2 : ( ruleLtlBaseFormula ) ;
    public final void rule__LtlNextFormula__ArgAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8875:1: ( ( ruleLtlBaseFormula ) )
            // InternalComposedLts.g:8876:1: ( ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:8876:1: ( ruleLtlBaseFormula )
            // InternalComposedLts.g:8877:1: ruleLtlBaseFormula
            {
             before(grammarAccess.getLtlNextFormulaAccess().getArgLtlBaseFormulaParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlNextFormulaAccess().getArgLtlBaseFormulaParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNextFormula__ArgAssignment_2"


    // $ANTLR start "rule__LtlAlwaysFormula__ArgAssignment_1"
    // InternalComposedLts.g:8886:1: rule__LtlAlwaysFormula__ArgAssignment_1 : ( ruleLtlBaseFormula ) ;
    public final void rule__LtlAlwaysFormula__ArgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8890:1: ( ( ruleLtlBaseFormula ) )
            // InternalComposedLts.g:8891:1: ( ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:8891:1: ( ruleLtlBaseFormula )
            // InternalComposedLts.g:8892:1: ruleLtlBaseFormula
            {
             before(grammarAccess.getLtlAlwaysFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlAlwaysFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlAlwaysFormula__ArgAssignment_1"


    // $ANTLR start "rule__LtlEventuallyFormula__ArgAssignment_1"
    // InternalComposedLts.g:8901:1: rule__LtlEventuallyFormula__ArgAssignment_1 : ( ruleLtlBaseFormula ) ;
    public final void rule__LtlEventuallyFormula__ArgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8905:1: ( ( ruleLtlBaseFormula ) )
            // InternalComposedLts.g:8906:1: ( ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:8906:1: ( ruleLtlBaseFormula )
            // InternalComposedLts.g:8907:1: ruleLtlBaseFormula
            {
             before(grammarAccess.getLtlEventuallyFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlEventuallyFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlEventuallyFormula__ArgAssignment_1"


    // $ANTLR start "rule__LtlNotFormula__ArgAssignment_1"
    // InternalComposedLts.g:8916:1: rule__LtlNotFormula__ArgAssignment_1 : ( ruleLtlBaseFormula ) ;
    public final void rule__LtlNotFormula__ArgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8920:1: ( ( ruleLtlBaseFormula ) )
            // InternalComposedLts.g:8921:1: ( ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:8921:1: ( ruleLtlBaseFormula )
            // InternalComposedLts.g:8922:1: ruleLtlBaseFormula
            {
             before(grammarAccess.getLtlNotFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLtlBaseFormula();

            state._fsp--;

             after(grammarAccess.getLtlNotFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LtlNotFormula__ArgAssignment_1"


    // $ANTLR start "rule__CupLabelPredicate__RighAssignment_1_2"
    // InternalComposedLts.g:8931:1: rule__CupLabelPredicate__RighAssignment_1_2 : ( ruleCupLabelPredicate ) ;
    public final void rule__CupLabelPredicate__RighAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8935:1: ( ( ruleCupLabelPredicate ) )
            // InternalComposedLts.g:8936:1: ( ruleCupLabelPredicate )
            {
            // InternalComposedLts.g:8936:1: ( ruleCupLabelPredicate )
            // InternalComposedLts.g:8937:1: ruleCupLabelPredicate
            {
             before(grammarAccess.getCupLabelPredicateAccess().getRighCupLabelPredicateParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCupLabelPredicate();

            state._fsp--;

             after(grammarAccess.getCupLabelPredicateAccess().getRighCupLabelPredicateParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CupLabelPredicate__RighAssignment_1_2"


    // $ANTLR start "rule__CapLabelPredicate__RightAssignment_1_2"
    // InternalComposedLts.g:8946:1: rule__CapLabelPredicate__RightAssignment_1_2 : ( ruleCapLabelPredicate ) ;
    public final void rule__CapLabelPredicate__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8950:1: ( ( ruleCapLabelPredicate ) )
            // InternalComposedLts.g:8951:1: ( ruleCapLabelPredicate )
            {
            // InternalComposedLts.g:8951:1: ( ruleCapLabelPredicate )
            // InternalComposedLts.g:8952:1: ruleCapLabelPredicate
            {
             before(grammarAccess.getCapLabelPredicateAccess().getRightCapLabelPredicateParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCapLabelPredicate();

            state._fsp--;

             after(grammarAccess.getCapLabelPredicateAccess().getRightCapLabelPredicateParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CapLabelPredicate__RightAssignment_1_2"


    // $ANTLR start "rule__NotLabelPredicate__ArgAssignment_1"
    // InternalComposedLts.g:8961:1: rule__NotLabelPredicate__ArgAssignment_1 : ( ruleBaseLabelPredicate ) ;
    public final void rule__NotLabelPredicate__ArgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8965:1: ( ( ruleBaseLabelPredicate ) )
            // InternalComposedLts.g:8966:1: ( ruleBaseLabelPredicate )
            {
            // InternalComposedLts.g:8966:1: ( ruleBaseLabelPredicate )
            // InternalComposedLts.g:8967:1: ruleBaseLabelPredicate
            {
             before(grammarAccess.getNotLabelPredicateAccess().getArgBaseLabelPredicateParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBaseLabelPredicate();

            state._fsp--;

             after(grammarAccess.getNotLabelPredicateAccess().getArgBaseLabelPredicateParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotLabelPredicate__ArgAssignment_1"


    // $ANTLR start "rule__ActionLabelPredicate__ActAssignment"
    // InternalComposedLts.g:8976:1: rule__ActionLabelPredicate__ActAssignment : ( ( RULE_ID ) ) ;
    public final void rule__ActionLabelPredicate__ActAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // InternalComposedLts.g:8980:1: ( ( ( RULE_ID ) ) )
            // InternalComposedLts.g:8981:1: ( ( RULE_ID ) )
            {
            // InternalComposedLts.g:8981:1: ( ( RULE_ID ) )
            // InternalComposedLts.g:8982:1: ( RULE_ID )
            {
             before(grammarAccess.getActionLabelPredicateAccess().getActActionCrossReference_0()); 
            // InternalComposedLts.g:8983:1: ( RULE_ID )
            // InternalComposedLts.g:8984:1: RULE_ID
            {
             before(grammarAccess.getActionLabelPredicateAccess().getActActionIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getActionLabelPredicateAccess().getActActionIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getActionLabelPredicateAccess().getActActionCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ActionLabelPredicate__ActAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0001000700002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0001000700002002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000084000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000041000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000028100000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000B7A000200010L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0020020000000010L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x001C038000200010L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0020000000000000L});

}
package org.cmg.tapas.xtext.clts.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.cmg.tapas.xtext.clts.services.ComposedLtsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalComposedLtsParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'model'", "';'", "'lts'", "'='", "'||'", "'|*|'", "'|'", "','", "'{'", "'}'", "'('", "')'", "'->'", "'states'", "':'", "'init'", "'rules'", "'endrules'", "'labels'", "'endlabels'", "'-'", "'label'", "'action'", "'hml'", "'formula'", "'&'", "'#['", "']'", "'true'", "'false'", "'!'", "'<'", "'>'", "'['", "'min'", "'.'", "'max'", "'ltl'", "'\\\\U'", "'\\\\X'", "'\\\\G'", "'\\\\F'", "'*'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalComposedLtsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalComposedLtsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalComposedLtsParser.tokenNames; }
    public String getGrammarFileName() { return "InternalComposedLts.g"; }



     	private ComposedLtsGrammarAccess grammarAccess;
     	
        public InternalComposedLtsParser(TokenStream input, ComposedLtsGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected ComposedLtsGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // InternalComposedLts.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalComposedLts.g:68:2: (iv_ruleModel= ruleModel EOF )
            // InternalComposedLts.g:69:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalComposedLts.g:76:1: ruleModel returns [EObject current=null] : ( () otherlv_1= 'model' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ( (lv_elements_4_0= ruleElement ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        EObject lv_elements_4_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:79:28: ( ( () otherlv_1= 'model' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ( (lv_elements_4_0= ruleElement ) )* ) )
            // InternalComposedLts.g:80:1: ( () otherlv_1= 'model' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ( (lv_elements_4_0= ruleElement ) )* )
            {
            // InternalComposedLts.g:80:1: ( () otherlv_1= 'model' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ( (lv_elements_4_0= ruleElement ) )* )
            // InternalComposedLts.g:80:2: () otherlv_1= 'model' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= ';' ( (lv_elements_4_0= ruleElement ) )*
            {
            // InternalComposedLts.g:80:2: ()
            // InternalComposedLts.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getModelAccess().getModelAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getModelAccess().getModelKeyword_1());
                
            // InternalComposedLts.g:90:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalComposedLts.g:91:1: (lv_name_2_0= RULE_ID )
            {
            // InternalComposedLts.g:91:1: (lv_name_2_0= RULE_ID )
            // InternalComposedLts.g:92:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            			newLeafNode(lv_name_2_0, grammarAccess.getModelAccess().getNameIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getModelRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

                	newLeafNode(otherlv_3, grammarAccess.getModelAccess().getSemicolonKeyword_3());
                
            // InternalComposedLts.g:112:1: ( (lv_elements_4_0= ruleElement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13||(LA1_0>=32 && LA1_0<=34)||LA1_0==48) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalComposedLts.g:113:1: (lv_elements_4_0= ruleElement )
            	    {
            	    // InternalComposedLts.g:113:1: (lv_elements_4_0= ruleElement )
            	    // InternalComposedLts.g:114:3: lv_elements_4_0= ruleElement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getModelAccess().getElementsElementParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_5);
            	    lv_elements_4_0=ruleElement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_4_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.Element");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleElement"
    // InternalComposedLts.g:138:1: entryRuleElement returns [EObject current=null] : iv_ruleElement= ruleElement EOF ;
    public final EObject entryRuleElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElement = null;


        try {
            // InternalComposedLts.g:139:2: (iv_ruleElement= ruleElement EOF )
            // InternalComposedLts.g:140:2: iv_ruleElement= ruleElement EOF
            {
             newCompositeNode(grammarAccess.getElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElement=ruleElement();

            state._fsp--;

             current =iv_ruleElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElement"


    // $ANTLR start "ruleElement"
    // InternalComposedLts.g:147:1: ruleElement returns [EObject current=null] : (this_Action_0= ruleAction | this_Lts_1= ruleLts | this_Formula_2= ruleFormula | this_Label_3= ruleLabel ) ;
    public final EObject ruleElement() throws RecognitionException {
        EObject current = null;

        EObject this_Action_0 = null;

        EObject this_Lts_1 = null;

        EObject this_Formula_2 = null;

        EObject this_Label_3 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:150:28: ( (this_Action_0= ruleAction | this_Lts_1= ruleLts | this_Formula_2= ruleFormula | this_Label_3= ruleLabel ) )
            // InternalComposedLts.g:151:1: (this_Action_0= ruleAction | this_Lts_1= ruleLts | this_Formula_2= ruleFormula | this_Label_3= ruleLabel )
            {
            // InternalComposedLts.g:151:1: (this_Action_0= ruleAction | this_Lts_1= ruleLts | this_Formula_2= ruleFormula | this_Label_3= ruleLabel )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt2=1;
                }
                break;
            case 13:
                {
                alt2=2;
                }
                break;
            case 34:
            case 48:
                {
                alt2=3;
                }
                break;
            case 32:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalComposedLts.g:152:5: this_Action_0= ruleAction
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getActionParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Action_0=ruleAction();

                    state._fsp--;

                     
                            current = this_Action_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:162:5: this_Lts_1= ruleLts
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getLtsParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Lts_1=ruleLts();

                    state._fsp--;

                     
                            current = this_Lts_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:172:5: this_Formula_2= ruleFormula
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getFormulaParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Formula_2=ruleFormula();

                    state._fsp--;

                     
                            current = this_Formula_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // InternalComposedLts.g:182:5: this_Label_3= ruleLabel
                    {
                     
                            newCompositeNode(grammarAccess.getElementAccess().getLabelParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Label_3=ruleLabel();

                    state._fsp--;

                     
                            current = this_Label_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElement"


    // $ANTLR start "entryRuleLts"
    // InternalComposedLts.g:198:1: entryRuleLts returns [EObject current=null] : iv_ruleLts= ruleLts EOF ;
    public final EObject entryRuleLts() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLts = null;


        try {
            // InternalComposedLts.g:199:2: (iv_ruleLts= ruleLts EOF )
            // InternalComposedLts.g:200:2: iv_ruleLts= ruleLts EOF
            {
             newCompositeNode(grammarAccess.getLtsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLts=ruleLts();

            state._fsp--;

             current =iv_ruleLts; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLts"


    // $ANTLR start "ruleLts"
    // InternalComposedLts.g:207:1: ruleLts returns [EObject current=null] : (otherlv_0= 'lts' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleLtsBody ) ) ) ;
    public final EObject ruleLts() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_body_2_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:210:28: ( (otherlv_0= 'lts' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleLtsBody ) ) ) )
            // InternalComposedLts.g:211:1: (otherlv_0= 'lts' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleLtsBody ) ) )
            {
            // InternalComposedLts.g:211:1: (otherlv_0= 'lts' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleLtsBody ) ) )
            // InternalComposedLts.g:211:3: otherlv_0= 'lts' ( (lv_name_1_0= RULE_ID ) ) ( (lv_body_2_0= ruleLtsBody ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

                	newLeafNode(otherlv_0, grammarAccess.getLtsAccess().getLtsKeyword_0());
                
            // InternalComposedLts.g:215:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalComposedLts.g:216:1: (lv_name_1_0= RULE_ID )
            {
            // InternalComposedLts.g:216:1: (lv_name_1_0= RULE_ID )
            // InternalComposedLts.g:217:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            			newLeafNode(lv_name_1_0, grammarAccess.getLtsAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLtsRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            // InternalComposedLts.g:233:2: ( (lv_body_2_0= ruleLtsBody ) )
            // InternalComposedLts.g:234:1: (lv_body_2_0= ruleLtsBody )
            {
            // InternalComposedLts.g:234:1: (lv_body_2_0= ruleLtsBody )
            // InternalComposedLts.g:235:3: lv_body_2_0= ruleLtsBody
            {
             
            	        newCompositeNode(grammarAccess.getLtsAccess().getBodyLtsBodyParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_body_2_0=ruleLtsBody();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtsRule());
            	        }
                   		set(
                   			current, 
                   			"body",
                    		lv_body_2_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtsBody");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLts"


    // $ANTLR start "entryRuleLtsBody"
    // InternalComposedLts.g:259:1: entryRuleLtsBody returns [EObject current=null] : iv_ruleLtsBody= ruleLtsBody EOF ;
    public final EObject entryRuleLtsBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtsBody = null;


        try {
            // InternalComposedLts.g:260:2: (iv_ruleLtsBody= ruleLtsBody EOF )
            // InternalComposedLts.g:261:2: iv_ruleLtsBody= ruleLtsBody EOF
            {
             newCompositeNode(grammarAccess.getLtsBodyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtsBody=ruleLtsBody();

            state._fsp--;

             current =iv_ruleLtsBody; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtsBody"


    // $ANTLR start "ruleLtsBody"
    // InternalComposedLts.g:268:1: ruleLtsBody returns [EObject current=null] : (this_LtsDeclarationBody_0= ruleLtsDeclarationBody | this_LtsComposition_1= ruleLtsComposition ) ;
    public final EObject ruleLtsBody() throws RecognitionException {
        EObject current = null;

        EObject this_LtsDeclarationBody_0 = null;

        EObject this_LtsComposition_1 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:271:28: ( (this_LtsDeclarationBody_0= ruleLtsDeclarationBody | this_LtsComposition_1= ruleLtsComposition ) )
            // InternalComposedLts.g:272:1: (this_LtsDeclarationBody_0= ruleLtsDeclarationBody | this_LtsComposition_1= ruleLtsComposition )
            {
            // InternalComposedLts.g:272:1: (this_LtsDeclarationBody_0= ruleLtsDeclarationBody | this_LtsComposition_1= ruleLtsComposition )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==19) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalComposedLts.g:273:5: this_LtsDeclarationBody_0= ruleLtsDeclarationBody
                    {
                     
                            newCompositeNode(grammarAccess.getLtsBodyAccess().getLtsDeclarationBodyParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtsDeclarationBody_0=ruleLtsDeclarationBody();

                    state._fsp--;

                     
                            current = this_LtsDeclarationBody_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:283:5: this_LtsComposition_1= ruleLtsComposition
                    {
                     
                            newCompositeNode(grammarAccess.getLtsBodyAccess().getLtsCompositionParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtsComposition_1=ruleLtsComposition();

                    state._fsp--;

                     
                            current = this_LtsComposition_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtsBody"


    // $ANTLR start "entryRuleLtsComposition"
    // InternalComposedLts.g:299:1: entryRuleLtsComposition returns [EObject current=null] : iv_ruleLtsComposition= ruleLtsComposition EOF ;
    public final EObject entryRuleLtsComposition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtsComposition = null;


        try {
            // InternalComposedLts.g:300:2: (iv_ruleLtsComposition= ruleLtsComposition EOF )
            // InternalComposedLts.g:301:2: iv_ruleLtsComposition= ruleLtsComposition EOF
            {
             newCompositeNode(grammarAccess.getLtsCompositionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtsComposition=ruleLtsComposition();

            state._fsp--;

             current =iv_ruleLtsComposition; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtsComposition"


    // $ANTLR start "ruleLtsComposition"
    // InternalComposedLts.g:308:1: ruleLtsComposition returns [EObject current=null] : (otherlv_0= '=' ( (lv_body_1_0= ruleCompositionBody ) ) otherlv_2= ';' ) ;
    public final EObject ruleLtsComposition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_body_1_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:311:28: ( (otherlv_0= '=' ( (lv_body_1_0= ruleCompositionBody ) ) otherlv_2= ';' ) )
            // InternalComposedLts.g:312:1: (otherlv_0= '=' ( (lv_body_1_0= ruleCompositionBody ) ) otherlv_2= ';' )
            {
            // InternalComposedLts.g:312:1: (otherlv_0= '=' ( (lv_body_1_0= ruleCompositionBody ) ) otherlv_2= ';' )
            // InternalComposedLts.g:312:3: otherlv_0= '=' ( (lv_body_1_0= ruleCompositionBody ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_7); 

                	newLeafNode(otherlv_0, grammarAccess.getLtsCompositionAccess().getEqualsSignKeyword_0());
                
            // InternalComposedLts.g:316:1: ( (lv_body_1_0= ruleCompositionBody ) )
            // InternalComposedLts.g:317:1: (lv_body_1_0= ruleCompositionBody )
            {
            // InternalComposedLts.g:317:1: (lv_body_1_0= ruleCompositionBody )
            // InternalComposedLts.g:318:3: lv_body_1_0= ruleCompositionBody
            {
             
            	        newCompositeNode(grammarAccess.getLtsCompositionAccess().getBodyCompositionBodyParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_4);
            lv_body_1_0=ruleCompositionBody();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtsCompositionRule());
            	        }
                   		set(
                   			current, 
                   			"body",
                    		lv_body_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.CompositionBody");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getLtsCompositionAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtsComposition"


    // $ANTLR start "entryRuleCompositionBody"
    // InternalComposedLts.g:346:1: entryRuleCompositionBody returns [EObject current=null] : iv_ruleCompositionBody= ruleCompositionBody EOF ;
    public final EObject entryRuleCompositionBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompositionBody = null;


        try {
            // InternalComposedLts.g:347:2: (iv_ruleCompositionBody= ruleCompositionBody EOF )
            // InternalComposedLts.g:348:2: iv_ruleCompositionBody= ruleCompositionBody EOF
            {
             newCompositeNode(grammarAccess.getCompositionBodyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompositionBody=ruleCompositionBody();

            state._fsp--;

             current =iv_ruleCompositionBody; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompositionBody"


    // $ANTLR start "ruleCompositionBody"
    // InternalComposedLts.g:355:1: ruleCompositionBody returns [EObject current=null] : this_Interleaving_0= ruleInterleaving ;
    public final EObject ruleCompositionBody() throws RecognitionException {
        EObject current = null;

        EObject this_Interleaving_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:358:28: (this_Interleaving_0= ruleInterleaving )
            // InternalComposedLts.g:360:5: this_Interleaving_0= ruleInterleaving
            {
             
                    newCompositeNode(grammarAccess.getCompositionBodyAccess().getInterleavingParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_Interleaving_0=ruleInterleaving();

            state._fsp--;

             
                    current = this_Interleaving_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompositionBody"


    // $ANTLR start "entryRuleInterleaving"
    // InternalComposedLts.g:376:1: entryRuleInterleaving returns [EObject current=null] : iv_ruleInterleaving= ruleInterleaving EOF ;
    public final EObject entryRuleInterleaving() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterleaving = null;


        try {
            // InternalComposedLts.g:377:2: (iv_ruleInterleaving= ruleInterleaving EOF )
            // InternalComposedLts.g:378:2: iv_ruleInterleaving= ruleInterleaving EOF
            {
             newCompositeNode(grammarAccess.getInterleavingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInterleaving=ruleInterleaving();

            state._fsp--;

             current =iv_ruleInterleaving; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterleaving"


    // $ANTLR start "ruleInterleaving"
    // InternalComposedLts.g:385:1: ruleInterleaving returns [EObject current=null] : (this_Synchronization_0= ruleSynchronization ( () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) ) )? ) ;
    public final EObject ruleInterleaving() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Synchronization_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:388:28: ( (this_Synchronization_0= ruleSynchronization ( () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) ) )? ) )
            // InternalComposedLts.g:389:1: (this_Synchronization_0= ruleSynchronization ( () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) ) )? )
            {
            // InternalComposedLts.g:389:1: (this_Synchronization_0= ruleSynchronization ( () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) ) )? )
            // InternalComposedLts.g:390:5: this_Synchronization_0= ruleSynchronization ( () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getInterleavingAccess().getSynchronizationParserRuleCall_0()); 
                
            pushFollow(FOLLOW_8);
            this_Synchronization_0=ruleSynchronization();

            state._fsp--;

             
                    current = this_Synchronization_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:398:1: ( () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalComposedLts.g:398:2: () otherlv_2= '||' ( (lv_right_3_0= ruleInterleaving ) )
                    {
                    // InternalComposedLts.g:398:2: ()
                    // InternalComposedLts.g:399:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getInterleavingAccess().getInterleavingLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,15,FOLLOW_7); 

                        	newLeafNode(otherlv_2, grammarAccess.getInterleavingAccess().getVerticalLineVerticalLineKeyword_1_1());
                        
                    // InternalComposedLts.g:408:1: ( (lv_right_3_0= ruleInterleaving ) )
                    // InternalComposedLts.g:409:1: (lv_right_3_0= ruleInterleaving )
                    {
                    // InternalComposedLts.g:409:1: (lv_right_3_0= ruleInterleaving )
                    // InternalComposedLts.g:410:3: lv_right_3_0= ruleInterleaving
                    {
                     
                    	        newCompositeNode(grammarAccess.getInterleavingAccess().getRightInterleavingParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleInterleaving();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getInterleavingRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_3_0, 
                            		"org.cmg.tapas.xtext.clts.ComposedLts.Interleaving");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterleaving"


    // $ANTLR start "entryRuleSynchronization"
    // InternalComposedLts.g:434:1: entryRuleSynchronization returns [EObject current=null] : iv_ruleSynchronization= ruleSynchronization EOF ;
    public final EObject entryRuleSynchronization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSynchronization = null;


        try {
            // InternalComposedLts.g:435:2: (iv_ruleSynchronization= ruleSynchronization EOF )
            // InternalComposedLts.g:436:2: iv_ruleSynchronization= ruleSynchronization EOF
            {
             newCompositeNode(grammarAccess.getSynchronizationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSynchronization=ruleSynchronization();

            state._fsp--;

             current =iv_ruleSynchronization; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSynchronization"


    // $ANTLR start "ruleSynchronization"
    // InternalComposedLts.g:443:1: ruleSynchronization returns [EObject current=null] : (this_ControlledInteraction_0= ruleControlledInteraction ( () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) ) )? ) ;
    public final EObject ruleSynchronization() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_ControlledInteraction_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:446:28: ( (this_ControlledInteraction_0= ruleControlledInteraction ( () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) ) )? ) )
            // InternalComposedLts.g:447:1: (this_ControlledInteraction_0= ruleControlledInteraction ( () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) ) )? )
            {
            // InternalComposedLts.g:447:1: (this_ControlledInteraction_0= ruleControlledInteraction ( () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) ) )? )
            // InternalComposedLts.g:448:5: this_ControlledInteraction_0= ruleControlledInteraction ( () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getSynchronizationAccess().getControlledInteractionParserRuleCall_0()); 
                
            pushFollow(FOLLOW_9);
            this_ControlledInteraction_0=ruleControlledInteraction();

            state._fsp--;

             
                    current = this_ControlledInteraction_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:456:1: ( () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalComposedLts.g:456:2: () otherlv_2= '|*|' ( (lv_right_3_0= ruleSynchronization ) )
                    {
                    // InternalComposedLts.g:456:2: ()
                    // InternalComposedLts.g:457:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getSynchronizationAccess().getSynchronizationLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,16,FOLLOW_7); 

                        	newLeafNode(otherlv_2, grammarAccess.getSynchronizationAccess().getVerticalLineAsteriskVerticalLineKeyword_1_1());
                        
                    // InternalComposedLts.g:466:1: ( (lv_right_3_0= ruleSynchronization ) )
                    // InternalComposedLts.g:467:1: (lv_right_3_0= ruleSynchronization )
                    {
                    // InternalComposedLts.g:467:1: (lv_right_3_0= ruleSynchronization )
                    // InternalComposedLts.g:468:3: lv_right_3_0= ruleSynchronization
                    {
                     
                    	        newCompositeNode(grammarAccess.getSynchronizationAccess().getRightSynchronizationParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleSynchronization();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getSynchronizationRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_3_0, 
                            		"org.cmg.tapas.xtext.clts.ComposedLts.Synchronization");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSynchronization"


    // $ANTLR start "entryRuleControlledInteraction"
    // InternalComposedLts.g:492:1: entryRuleControlledInteraction returns [EObject current=null] : iv_ruleControlledInteraction= ruleControlledInteraction EOF ;
    public final EObject entryRuleControlledInteraction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleControlledInteraction = null;


        try {
            // InternalComposedLts.g:493:2: (iv_ruleControlledInteraction= ruleControlledInteraction EOF )
            // InternalComposedLts.g:494:2: iv_ruleControlledInteraction= ruleControlledInteraction EOF
            {
             newCompositeNode(grammarAccess.getControlledInteractionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleControlledInteraction=ruleControlledInteraction();

            state._fsp--;

             current =iv_ruleControlledInteraction; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleControlledInteraction"


    // $ANTLR start "ruleControlledInteraction"
    // InternalComposedLts.g:501:1: ruleControlledInteraction returns [EObject current=null] : (this_Renaming_0= ruleRenaming ( () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) ) )? ) ;
    public final EObject ruleControlledInteraction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject this_Renaming_0 = null;

        EObject lv_right_7_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:504:28: ( (this_Renaming_0= ruleRenaming ( () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) ) )? ) )
            // InternalComposedLts.g:505:1: (this_Renaming_0= ruleRenaming ( () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) ) )? )
            {
            // InternalComposedLts.g:505:1: (this_Renaming_0= ruleRenaming ( () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) ) )? )
            // InternalComposedLts.g:506:5: this_Renaming_0= ruleRenaming ( () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getControlledInteractionAccess().getRenamingParserRuleCall_0()); 
                
            pushFollow(FOLLOW_10);
            this_Renaming_0=ruleRenaming();

            state._fsp--;

             
                    current = this_Renaming_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:514:1: ( () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalComposedLts.g:514:2: () otherlv_2= '|' ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* ) otherlv_6= '|' ( (lv_right_7_0= ruleControlledInteraction ) )
                    {
                    // InternalComposedLts.g:514:2: ()
                    // InternalComposedLts.g:515:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getControlledInteractionAccess().getControlledInteractionLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,17,FOLLOW_3); 

                        	newLeafNode(otherlv_2, grammarAccess.getControlledInteractionAccess().getVerticalLineKeyword_1_1());
                        
                    // InternalComposedLts.g:524:1: ( ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )* )
                    // InternalComposedLts.g:524:2: ( (otherlv_3= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )*
                    {
                    // InternalComposedLts.g:524:2: ( (otherlv_3= RULE_ID ) )
                    // InternalComposedLts.g:525:1: (otherlv_3= RULE_ID )
                    {
                    // InternalComposedLts.g:525:1: (otherlv_3= RULE_ID )
                    // InternalComposedLts.g:526:3: otherlv_3= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getControlledInteractionRule());
                    	        }
                            
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_11); 

                    		newLeafNode(otherlv_3, grammarAccess.getControlledInteractionAccess().getActionActionCrossReference_1_2_0_0()); 
                    	

                    }


                    }

                    // InternalComposedLts.g:537:2: (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==18) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalComposedLts.g:537:4: otherlv_4= ',' ( (otherlv_5= RULE_ID ) )
                    	    {
                    	    otherlv_4=(Token)match(input,18,FOLLOW_3); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getControlledInteractionAccess().getCommaKeyword_1_2_1_0());
                    	        
                    	    // InternalComposedLts.g:541:1: ( (otherlv_5= RULE_ID ) )
                    	    // InternalComposedLts.g:542:1: (otherlv_5= RULE_ID )
                    	    {
                    	    // InternalComposedLts.g:542:1: (otherlv_5= RULE_ID )
                    	    // InternalComposedLts.g:543:3: otherlv_5= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getControlledInteractionRule());
                    	    	        }
                    	            
                    	    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_11); 

                    	    		newLeafNode(otherlv_5, grammarAccess.getControlledInteractionAccess().getActionActionCrossReference_1_2_1_1_0()); 
                    	    	

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }

                    otherlv_6=(Token)match(input,17,FOLLOW_7); 

                        	newLeafNode(otherlv_6, grammarAccess.getControlledInteractionAccess().getVerticalLineKeyword_1_3());
                        
                    // InternalComposedLts.g:558:1: ( (lv_right_7_0= ruleControlledInteraction ) )
                    // InternalComposedLts.g:559:1: (lv_right_7_0= ruleControlledInteraction )
                    {
                    // InternalComposedLts.g:559:1: (lv_right_7_0= ruleControlledInteraction )
                    // InternalComposedLts.g:560:3: lv_right_7_0= ruleControlledInteraction
                    {
                     
                    	        newCompositeNode(grammarAccess.getControlledInteractionAccess().getRightControlledInteractionParserRuleCall_1_4_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_right_7_0=ruleControlledInteraction();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getControlledInteractionRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_7_0, 
                            		"org.cmg.tapas.xtext.clts.ComposedLts.ControlledInteraction");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleControlledInteraction"


    // $ANTLR start "entryRuleRenaming"
    // InternalComposedLts.g:584:1: entryRuleRenaming returns [EObject current=null] : iv_ruleRenaming= ruleRenaming EOF ;
    public final EObject entryRuleRenaming() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRenaming = null;


        try {
            // InternalComposedLts.g:585:2: (iv_ruleRenaming= ruleRenaming EOF )
            // InternalComposedLts.g:586:2: iv_ruleRenaming= ruleRenaming EOF
            {
             newCompositeNode(grammarAccess.getRenamingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRenaming=ruleRenaming();

            state._fsp--;

             current =iv_ruleRenaming; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRenaming"


    // $ANTLR start "ruleRenaming"
    // InternalComposedLts.g:593:1: ruleRenaming returns [EObject current=null] : (this_BaseLts_0= ruleBaseLts ( () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}' )? ) ;
    public final EObject ruleRenaming() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject this_BaseLts_0 = null;

        EObject lv_maps_3_0 = null;

        EObject lv_maps_5_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:596:28: ( (this_BaseLts_0= ruleBaseLts ( () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}' )? ) )
            // InternalComposedLts.g:597:1: (this_BaseLts_0= ruleBaseLts ( () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}' )? )
            {
            // InternalComposedLts.g:597:1: (this_BaseLts_0= ruleBaseLts ( () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}' )? )
            // InternalComposedLts.g:598:5: this_BaseLts_0= ruleBaseLts ( () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}' )?
            {
             
                    newCompositeNode(grammarAccess.getRenamingAccess().getBaseLtsParserRuleCall_0()); 
                
            pushFollow(FOLLOW_12);
            this_BaseLts_0=ruleBaseLts();

            state._fsp--;

             
                    current = this_BaseLts_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:606:1: ( () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalComposedLts.g:606:2: () otherlv_2= '{' ( (lv_maps_3_0= ruleMapping ) ) (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )* otherlv_6= '}'
                    {
                    // InternalComposedLts.g:606:2: ()
                    // InternalComposedLts.g:607:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getRenamingAccess().getRenamingArgAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,19,FOLLOW_3); 

                        	newLeafNode(otherlv_2, grammarAccess.getRenamingAccess().getLeftCurlyBracketKeyword_1_1());
                        
                    // InternalComposedLts.g:616:1: ( (lv_maps_3_0= ruleMapping ) )
                    // InternalComposedLts.g:617:1: (lv_maps_3_0= ruleMapping )
                    {
                    // InternalComposedLts.g:617:1: (lv_maps_3_0= ruleMapping )
                    // InternalComposedLts.g:618:3: lv_maps_3_0= ruleMapping
                    {
                     
                    	        newCompositeNode(grammarAccess.getRenamingAccess().getMapsMappingParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_13);
                    lv_maps_3_0=ruleMapping();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getRenamingRule());
                    	        }
                           		add(
                           			current, 
                           			"maps",
                            		lv_maps_3_0, 
                            		"org.cmg.tapas.xtext.clts.ComposedLts.Mapping");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // InternalComposedLts.g:634:2: (otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==18) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalComposedLts.g:634:4: otherlv_4= ',' ( (lv_maps_5_0= ruleMapping ) )
                    	    {
                    	    otherlv_4=(Token)match(input,18,FOLLOW_3); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getRenamingAccess().getCommaKeyword_1_3_0());
                    	        
                    	    // InternalComposedLts.g:638:1: ( (lv_maps_5_0= ruleMapping ) )
                    	    // InternalComposedLts.g:639:1: (lv_maps_5_0= ruleMapping )
                    	    {
                    	    // InternalComposedLts.g:639:1: (lv_maps_5_0= ruleMapping )
                    	    // InternalComposedLts.g:640:3: lv_maps_5_0= ruleMapping
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getRenamingAccess().getMapsMappingParserRuleCall_1_3_1_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_13);
                    	    lv_maps_5_0=ruleMapping();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getRenamingRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"maps",
                    	            		lv_maps_5_0, 
                    	            		"org.cmg.tapas.xtext.clts.ComposedLts.Mapping");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,20,FOLLOW_2); 

                        	newLeafNode(otherlv_6, grammarAccess.getRenamingAccess().getRightCurlyBracketKeyword_1_4());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRenaming"


    // $ANTLR start "entryRuleBaseLts"
    // InternalComposedLts.g:668:1: entryRuleBaseLts returns [EObject current=null] : iv_ruleBaseLts= ruleBaseLts EOF ;
    public final EObject entryRuleBaseLts() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseLts = null;


        try {
            // InternalComposedLts.g:669:2: (iv_ruleBaseLts= ruleBaseLts EOF )
            // InternalComposedLts.g:670:2: iv_ruleBaseLts= ruleBaseLts EOF
            {
             newCompositeNode(grammarAccess.getBaseLtsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBaseLts=ruleBaseLts();

            state._fsp--;

             current =iv_ruleBaseLts; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseLts"


    // $ANTLR start "ruleBaseLts"
    // InternalComposedLts.g:677:1: ruleBaseLts returns [EObject current=null] : (this_Reference_0= ruleReference | (otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')' ) ) ;
    public final EObject ruleBaseLts() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Reference_0 = null;

        EObject this_CompositionBody_2 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:680:28: ( (this_Reference_0= ruleReference | (otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')' ) ) )
            // InternalComposedLts.g:681:1: (this_Reference_0= ruleReference | (otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')' ) )
            {
            // InternalComposedLts.g:681:1: (this_Reference_0= ruleReference | (otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')' ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                alt10=1;
            }
            else if ( (LA10_0==21) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalComposedLts.g:682:5: this_Reference_0= ruleReference
                    {
                     
                            newCompositeNode(grammarAccess.getBaseLtsAccess().getReferenceParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_Reference_0=ruleReference();

                    state._fsp--;

                     
                            current = this_Reference_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:691:6: (otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')' )
                    {
                    // InternalComposedLts.g:691:6: (otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')' )
                    // InternalComposedLts.g:691:8: otherlv_1= '(' this_CompositionBody_2= ruleCompositionBody otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_7); 

                        	newLeafNode(otherlv_1, grammarAccess.getBaseLtsAccess().getLeftParenthesisKeyword_1_0());
                        
                     
                            newCompositeNode(grammarAccess.getBaseLtsAccess().getCompositionBodyParserRuleCall_1_1()); 
                        
                    pushFollow(FOLLOW_14);
                    this_CompositionBody_2=ruleCompositionBody();

                    state._fsp--;

                     
                            current = this_CompositionBody_2; 
                            afterParserOrEnumRuleCall();
                        
                    otherlv_3=(Token)match(input,22,FOLLOW_2); 

                        	newLeafNode(otherlv_3, grammarAccess.getBaseLtsAccess().getRightParenthesisKeyword_1_2());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseLts"


    // $ANTLR start "entryRuleReference"
    // InternalComposedLts.g:716:1: entryRuleReference returns [EObject current=null] : iv_ruleReference= ruleReference EOF ;
    public final EObject entryRuleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReference = null;


        try {
            // InternalComposedLts.g:717:2: (iv_ruleReference= ruleReference EOF )
            // InternalComposedLts.g:718:2: iv_ruleReference= ruleReference EOF
            {
             newCompositeNode(grammarAccess.getReferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReference=ruleReference();

            state._fsp--;

             current =iv_ruleReference; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // InternalComposedLts.g:725:1: ruleReference returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:728:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalComposedLts.g:729:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalComposedLts.g:729:1: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:730:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:730:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:731:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_0, grammarAccess.getReferenceAccess().getLtsLtsCrossReference_0()); 
            	

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleMapping"
    // InternalComposedLts.g:750:1: entryRuleMapping returns [EObject current=null] : iv_ruleMapping= ruleMapping EOF ;
    public final EObject entryRuleMapping() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapping = null;


        try {
            // InternalComposedLts.g:751:2: (iv_ruleMapping= ruleMapping EOF )
            // InternalComposedLts.g:752:2: iv_ruleMapping= ruleMapping EOF
            {
             newCompositeNode(grammarAccess.getMappingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMapping=ruleMapping();

            state._fsp--;

             current =iv_ruleMapping; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapping"


    // $ANTLR start "ruleMapping"
    // InternalComposedLts.g:759:1: ruleMapping returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleMapping() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:762:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalComposedLts.g:763:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalComposedLts.g:763:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) )
            // InternalComposedLts.g:763:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) )
            {
            // InternalComposedLts.g:763:2: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:764:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:764:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:765:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMappingRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            		newLeafNode(otherlv_0, grammarAccess.getMappingAccess().getSrcActionCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getMappingAccess().getHyphenMinusGreaterThanSignKeyword_1());
                
            // InternalComposedLts.g:780:1: ( (otherlv_2= RULE_ID ) )
            // InternalComposedLts.g:781:1: (otherlv_2= RULE_ID )
            {
            // InternalComposedLts.g:781:1: (otherlv_2= RULE_ID )
            // InternalComposedLts.g:782:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMappingRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_2, grammarAccess.getMappingAccess().getTrgActionCrossReference_2_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapping"


    // $ANTLR start "entryRuleLtsDeclarationBody"
    // InternalComposedLts.g:801:1: entryRuleLtsDeclarationBody returns [EObject current=null] : iv_ruleLtsDeclarationBody= ruleLtsDeclarationBody EOF ;
    public final EObject entryRuleLtsDeclarationBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtsDeclarationBody = null;


        try {
            // InternalComposedLts.g:802:2: (iv_ruleLtsDeclarationBody= ruleLtsDeclarationBody EOF )
            // InternalComposedLts.g:803:2: iv_ruleLtsDeclarationBody= ruleLtsDeclarationBody EOF
            {
             newCompositeNode(grammarAccess.getLtsDeclarationBodyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtsDeclarationBody=ruleLtsDeclarationBody();

            state._fsp--;

             current =iv_ruleLtsDeclarationBody; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtsDeclarationBody"


    // $ANTLR start "ruleLtsDeclarationBody"
    // InternalComposedLts.g:810:1: ruleLtsDeclarationBody returns [EObject current=null] : (otherlv_0= '{' otherlv_1= 'states' otherlv_2= ':' ( (lv_states_3_0= ruleLtsState ) ) (otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) ) )* otherlv_6= ';' otherlv_7= 'init' otherlv_8= ':' ( (otherlv_9= RULE_ID ) ) (otherlv_10= ',' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= ';' (otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules' )? (otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels' )? otherlv_21= '}' ) ;
    public final EObject ruleLtsDeclarationBody() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        EObject lv_states_3_0 = null;

        EObject lv_states_5_0 = null;

        EObject lv_rules_15_0 = null;

        EObject lv_labels_19_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:813:28: ( (otherlv_0= '{' otherlv_1= 'states' otherlv_2= ':' ( (lv_states_3_0= ruleLtsState ) ) (otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) ) )* otherlv_6= ';' otherlv_7= 'init' otherlv_8= ':' ( (otherlv_9= RULE_ID ) ) (otherlv_10= ',' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= ';' (otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules' )? (otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels' )? otherlv_21= '}' ) )
            // InternalComposedLts.g:814:1: (otherlv_0= '{' otherlv_1= 'states' otherlv_2= ':' ( (lv_states_3_0= ruleLtsState ) ) (otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) ) )* otherlv_6= ';' otherlv_7= 'init' otherlv_8= ':' ( (otherlv_9= RULE_ID ) ) (otherlv_10= ',' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= ';' (otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules' )? (otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels' )? otherlv_21= '}' )
            {
            // InternalComposedLts.g:814:1: (otherlv_0= '{' otherlv_1= 'states' otherlv_2= ':' ( (lv_states_3_0= ruleLtsState ) ) (otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) ) )* otherlv_6= ';' otherlv_7= 'init' otherlv_8= ':' ( (otherlv_9= RULE_ID ) ) (otherlv_10= ',' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= ';' (otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules' )? (otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels' )? otherlv_21= '}' )
            // InternalComposedLts.g:814:3: otherlv_0= '{' otherlv_1= 'states' otherlv_2= ':' ( (lv_states_3_0= ruleLtsState ) ) (otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) ) )* otherlv_6= ';' otherlv_7= 'init' otherlv_8= ':' ( (otherlv_9= RULE_ID ) ) (otherlv_10= ',' ( (otherlv_11= RULE_ID ) ) )* otherlv_12= ';' (otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules' )? (otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels' )? otherlv_21= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_16); 

                	newLeafNode(otherlv_0, grammarAccess.getLtsDeclarationBodyAccess().getLeftCurlyBracketKeyword_0());
                
            otherlv_1=(Token)match(input,24,FOLLOW_17); 

                	newLeafNode(otherlv_1, grammarAccess.getLtsDeclarationBodyAccess().getStatesKeyword_1());
                
            otherlv_2=(Token)match(input,25,FOLLOW_3); 

                	newLeafNode(otherlv_2, grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_2());
                
            // InternalComposedLts.g:826:1: ( (lv_states_3_0= ruleLtsState ) )
            // InternalComposedLts.g:827:1: (lv_states_3_0= ruleLtsState )
            {
            // InternalComposedLts.g:827:1: (lv_states_3_0= ruleLtsState )
            // InternalComposedLts.g:828:3: lv_states_3_0= ruleLtsState
            {
             
            	        newCompositeNode(grammarAccess.getLtsDeclarationBodyAccess().getStatesLtsStateParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_18);
            lv_states_3_0=ruleLtsState();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtsDeclarationBodyRule());
            	        }
                   		add(
                   			current, 
                   			"states",
                    		lv_states_3_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtsState");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // InternalComposedLts.g:844:2: (otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==18) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalComposedLts.g:844:4: otherlv_4= ',' ( (lv_states_5_0= ruleLtsState ) )
            	    {
            	    otherlv_4=(Token)match(input,18,FOLLOW_3); 

            	        	newLeafNode(otherlv_4, grammarAccess.getLtsDeclarationBodyAccess().getCommaKeyword_4_0());
            	        
            	    // InternalComposedLts.g:848:1: ( (lv_states_5_0= ruleLtsState ) )
            	    // InternalComposedLts.g:849:1: (lv_states_5_0= ruleLtsState )
            	    {
            	    // InternalComposedLts.g:849:1: (lv_states_5_0= ruleLtsState )
            	    // InternalComposedLts.g:850:3: lv_states_5_0= ruleLtsState
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLtsDeclarationBodyAccess().getStatesLtsStateParserRuleCall_4_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_18);
            	    lv_states_5_0=ruleLtsState();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLtsDeclarationBodyRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"states",
            	            		lv_states_5_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.LtsState");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_6=(Token)match(input,12,FOLLOW_19); 

                	newLeafNode(otherlv_6, grammarAccess.getLtsDeclarationBodyAccess().getSemicolonKeyword_5());
                
            otherlv_7=(Token)match(input,26,FOLLOW_17); 

                	newLeafNode(otherlv_7, grammarAccess.getLtsDeclarationBodyAccess().getInitKeyword_6());
                
            otherlv_8=(Token)match(input,25,FOLLOW_3); 

                	newLeafNode(otherlv_8, grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_7());
                
            // InternalComposedLts.g:878:1: ( (otherlv_9= RULE_ID ) )
            // InternalComposedLts.g:879:1: (otherlv_9= RULE_ID )
            {
            // InternalComposedLts.g:879:1: (otherlv_9= RULE_ID )
            // InternalComposedLts.g:880:3: otherlv_9= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLtsDeclarationBodyRule());
            	        }
                    
            otherlv_9=(Token)match(input,RULE_ID,FOLLOW_18); 

            		newLeafNode(otherlv_9, grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateCrossReference_8_0()); 
            	

            }


            }

            // InternalComposedLts.g:891:2: (otherlv_10= ',' ( (otherlv_11= RULE_ID ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==18) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalComposedLts.g:891:4: otherlv_10= ',' ( (otherlv_11= RULE_ID ) )
            	    {
            	    otherlv_10=(Token)match(input,18,FOLLOW_3); 

            	        	newLeafNode(otherlv_10, grammarAccess.getLtsDeclarationBodyAccess().getCommaKeyword_9_0());
            	        
            	    // InternalComposedLts.g:895:1: ( (otherlv_11= RULE_ID ) )
            	    // InternalComposedLts.g:896:1: (otherlv_11= RULE_ID )
            	    {
            	    // InternalComposedLts.g:896:1: (otherlv_11= RULE_ID )
            	    // InternalComposedLts.g:897:3: otherlv_11= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getLtsDeclarationBodyRule());
            	    	        }
            	            
            	    otherlv_11=(Token)match(input,RULE_ID,FOLLOW_18); 

            	    		newLeafNode(otherlv_11, grammarAccess.getLtsDeclarationBodyAccess().getInitLtsStateCrossReference_9_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_12=(Token)match(input,12,FOLLOW_20); 

                	newLeafNode(otherlv_12, grammarAccess.getLtsDeclarationBodyAccess().getSemicolonKeyword_10());
                
            // InternalComposedLts.g:912:1: (otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==27) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalComposedLts.g:912:3: otherlv_13= 'rules' otherlv_14= ':' ( (lv_rules_15_0= ruleLtsRule ) )* otherlv_16= 'endrules'
                    {
                    otherlv_13=(Token)match(input,27,FOLLOW_17); 

                        	newLeafNode(otherlv_13, grammarAccess.getLtsDeclarationBodyAccess().getRulesKeyword_11_0());
                        
                    otherlv_14=(Token)match(input,25,FOLLOW_21); 

                        	newLeafNode(otherlv_14, grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_11_1());
                        
                    // InternalComposedLts.g:920:1: ( (lv_rules_15_0= ruleLtsRule ) )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==RULE_ID) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalComposedLts.g:921:1: (lv_rules_15_0= ruleLtsRule )
                    	    {
                    	    // InternalComposedLts.g:921:1: (lv_rules_15_0= ruleLtsRule )
                    	    // InternalComposedLts.g:922:3: lv_rules_15_0= ruleLtsRule
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getLtsDeclarationBodyAccess().getRulesLtsRuleParserRuleCall_11_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_21);
                    	    lv_rules_15_0=ruleLtsRule();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getLtsDeclarationBodyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"rules",
                    	            		lv_rules_15_0, 
                    	            		"org.cmg.tapas.xtext.clts.ComposedLts.LtsRule");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,28,FOLLOW_22); 

                        	newLeafNode(otherlv_16, grammarAccess.getLtsDeclarationBodyAccess().getEndrulesKeyword_11_3());
                        

                    }
                    break;

            }

            // InternalComposedLts.g:942:3: (otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==29) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalComposedLts.g:942:5: otherlv_17= 'labels' otherlv_18= ':' ( (lv_labels_19_0= ruleLabelRule ) )* otherlv_20= 'endlabels'
                    {
                    otherlv_17=(Token)match(input,29,FOLLOW_17); 

                        	newLeafNode(otherlv_17, grammarAccess.getLtsDeclarationBodyAccess().getLabelsKeyword_12_0());
                        
                    otherlv_18=(Token)match(input,25,FOLLOW_23); 

                        	newLeafNode(otherlv_18, grammarAccess.getLtsDeclarationBodyAccess().getColonKeyword_12_1());
                        
                    // InternalComposedLts.g:950:1: ( (lv_labels_19_0= ruleLabelRule ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==RULE_ID) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // InternalComposedLts.g:951:1: (lv_labels_19_0= ruleLabelRule )
                    	    {
                    	    // InternalComposedLts.g:951:1: (lv_labels_19_0= ruleLabelRule )
                    	    // InternalComposedLts.g:952:3: lv_labels_19_0= ruleLabelRule
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getLtsDeclarationBodyAccess().getLabelsLabelRuleParserRuleCall_12_2_0()); 
                    	    	    
                    	    pushFollow(FOLLOW_23);
                    	    lv_labels_19_0=ruleLabelRule();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getLtsDeclarationBodyRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"labels",
                    	            		lv_labels_19_0, 
                    	            		"org.cmg.tapas.xtext.clts.ComposedLts.LabelRule");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,30,FOLLOW_24); 

                        	newLeafNode(otherlv_20, grammarAccess.getLtsDeclarationBodyAccess().getEndlabelsKeyword_12_3());
                        

                    }
                    break;

            }

            otherlv_21=(Token)match(input,20,FOLLOW_2); 

                	newLeafNode(otherlv_21, grammarAccess.getLtsDeclarationBodyAccess().getRightCurlyBracketKeyword_13());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtsDeclarationBody"


    // $ANTLR start "entryRuleLtsRule"
    // InternalComposedLts.g:984:1: entryRuleLtsRule returns [EObject current=null] : iv_ruleLtsRule= ruleLtsRule EOF ;
    public final EObject entryRuleLtsRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtsRule = null;


        try {
            // InternalComposedLts.g:985:2: (iv_ruleLtsRule= ruleLtsRule EOF )
            // InternalComposedLts.g:986:2: iv_ruleLtsRule= ruleLtsRule EOF
            {
             newCompositeNode(grammarAccess.getLtsRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtsRule=ruleLtsRule();

            state._fsp--;

             current =iv_ruleLtsRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtsRule"


    // $ANTLR start "ruleLtsRule"
    // InternalComposedLts.g:993:1: ruleLtsRule returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '-' ( (otherlv_2= RULE_ID ) ) otherlv_3= '->' ( (otherlv_4= RULE_ID ) ) otherlv_5= ';' ) ;
    public final EObject ruleLtsRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:996:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '-' ( (otherlv_2= RULE_ID ) ) otherlv_3= '->' ( (otherlv_4= RULE_ID ) ) otherlv_5= ';' ) )
            // InternalComposedLts.g:997:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '-' ( (otherlv_2= RULE_ID ) ) otherlv_3= '->' ( (otherlv_4= RULE_ID ) ) otherlv_5= ';' )
            {
            // InternalComposedLts.g:997:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '-' ( (otherlv_2= RULE_ID ) ) otherlv_3= '->' ( (otherlv_4= RULE_ID ) ) otherlv_5= ';' )
            // InternalComposedLts.g:997:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '-' ( (otherlv_2= RULE_ID ) ) otherlv_3= '->' ( (otherlv_4= RULE_ID ) ) otherlv_5= ';'
            {
            // InternalComposedLts.g:997:2: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:998:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:998:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:999:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLtsRuleRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_25); 

            		newLeafNode(otherlv_0, grammarAccess.getLtsRuleAccess().getSrcLtsStateCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,31,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getLtsRuleAccess().getHyphenMinusKeyword_1());
                
            // InternalComposedLts.g:1014:1: ( (otherlv_2= RULE_ID ) )
            // InternalComposedLts.g:1015:1: (otherlv_2= RULE_ID )
            {
            // InternalComposedLts.g:1015:1: (otherlv_2= RULE_ID )
            // InternalComposedLts.g:1016:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLtsRuleRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_15); 

            		newLeafNode(otherlv_2, grammarAccess.getLtsRuleAccess().getActActionCrossReference_2_0()); 
            	

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_3); 

                	newLeafNode(otherlv_3, grammarAccess.getLtsRuleAccess().getHyphenMinusGreaterThanSignKeyword_3());
                
            // InternalComposedLts.g:1031:1: ( (otherlv_4= RULE_ID ) )
            // InternalComposedLts.g:1032:1: (otherlv_4= RULE_ID )
            {
            // InternalComposedLts.g:1032:1: (otherlv_4= RULE_ID )
            // InternalComposedLts.g:1033:3: otherlv_4= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLtsRuleRule());
            	        }
                    
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_4); 

            		newLeafNode(otherlv_4, grammarAccess.getLtsRuleAccess().getTrgLtsStateCrossReference_4_0()); 
            	

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getLtsRuleAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtsRule"


    // $ANTLR start "entryRuleLtsState"
    // InternalComposedLts.g:1056:1: entryRuleLtsState returns [EObject current=null] : iv_ruleLtsState= ruleLtsState EOF ;
    public final EObject entryRuleLtsState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtsState = null;


        try {
            // InternalComposedLts.g:1057:2: (iv_ruleLtsState= ruleLtsState EOF )
            // InternalComposedLts.g:1058:2: iv_ruleLtsState= ruleLtsState EOF
            {
             newCompositeNode(grammarAccess.getLtsStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtsState=ruleLtsState();

            state._fsp--;

             current =iv_ruleLtsState; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtsState"


    // $ANTLR start "ruleLtsState"
    // InternalComposedLts.g:1065:1: ruleLtsState returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleLtsState() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1068:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // InternalComposedLts.g:1069:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // InternalComposedLts.g:1069:1: ( (lv_name_0_0= RULE_ID ) )
            // InternalComposedLts.g:1070:1: (lv_name_0_0= RULE_ID )
            {
            // InternalComposedLts.g:1070:1: (lv_name_0_0= RULE_ID )
            // InternalComposedLts.g:1071:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            			newLeafNode(lv_name_0_0, grammarAccess.getLtsStateAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLtsStateRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtsState"


    // $ANTLR start "entryRuleLabelRule"
    // InternalComposedLts.g:1095:1: entryRuleLabelRule returns [EObject current=null] : iv_ruleLabelRule= ruleLabelRule EOF ;
    public final EObject entryRuleLabelRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLabelRule = null;


        try {
            // InternalComposedLts.g:1096:2: (iv_ruleLabelRule= ruleLabelRule EOF )
            // InternalComposedLts.g:1097:2: iv_ruleLabelRule= ruleLabelRule EOF
            {
             newCompositeNode(grammarAccess.getLabelRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLabelRule=ruleLabelRule();

            state._fsp--;

             current =iv_ruleLabelRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLabelRule"


    // $ANTLR start "ruleLabelRule"
    // InternalComposedLts.g:1104:1: ruleLabelRule returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* ) otherlv_5= ';' ) ;
    public final EObject ruleLabelRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1107:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* ) otherlv_5= ';' ) )
            // InternalComposedLts.g:1108:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* ) otherlv_5= ';' )
            {
            // InternalComposedLts.g:1108:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* ) otherlv_5= ';' )
            // InternalComposedLts.g:1108:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= ':' ( ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* ) otherlv_5= ';'
            {
            // InternalComposedLts.g:1108:2: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:1109:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:1109:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:1110:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLabelRuleRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_17); 

            		newLeafNode(otherlv_0, grammarAccess.getLabelRuleAccess().getLabelLabelCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getLabelRuleAccess().getColonKeyword_1());
                
            // InternalComposedLts.g:1125:1: ( ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* )
            // InternalComposedLts.g:1125:2: ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
            {
            // InternalComposedLts.g:1125:2: ( (otherlv_2= RULE_ID ) )
            // InternalComposedLts.g:1126:1: (otherlv_2= RULE_ID )
            {
            // InternalComposedLts.g:1126:1: (otherlv_2= RULE_ID )
            // InternalComposedLts.g:1127:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLabelRuleRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_18); 

            		newLeafNode(otherlv_2, grammarAccess.getLabelRuleAccess().getStatesLtsStateCrossReference_2_0_0()); 
            	

            }


            }

            // InternalComposedLts.g:1138:2: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==18) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalComposedLts.g:1138:4: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
            	    {
            	    otherlv_3=(Token)match(input,18,FOLLOW_3); 

            	        	newLeafNode(otherlv_3, grammarAccess.getLabelRuleAccess().getCommaKeyword_2_1_0());
            	        
            	    // InternalComposedLts.g:1142:1: ( (otherlv_4= RULE_ID ) )
            	    // InternalComposedLts.g:1143:1: (otherlv_4= RULE_ID )
            	    {
            	    // InternalComposedLts.g:1143:1: (otherlv_4= RULE_ID )
            	    // InternalComposedLts.g:1144:3: otherlv_4= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getLabelRuleRule());
            	    	        }
            	            
            	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_18); 

            	    		newLeafNode(otherlv_4, grammarAccess.getLabelRuleAccess().getStatesLtsStateCrossReference_2_1_1_0()); 
            	    	

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            otherlv_5=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getLabelRuleAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLabelRule"


    // $ANTLR start "entryRuleRule"
    // InternalComposedLts.g:1169:1: entryRuleRule returns [EObject current=null] : iv_ruleRule= ruleRule EOF ;
    public final EObject entryRuleRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRule = null;


        try {
            // InternalComposedLts.g:1170:2: (iv_ruleRule= ruleRule EOF )
            // InternalComposedLts.g:1171:2: iv_ruleRule= ruleRule EOF
            {
             newCompositeNode(grammarAccess.getRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRule=ruleRule();

            state._fsp--;

             current =iv_ruleRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRule"


    // $ANTLR start "ruleRule"
    // InternalComposedLts.g:1178:1: ruleRule returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1181:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalComposedLts.g:1182:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalComposedLts.g:1182:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) )
            // InternalComposedLts.g:1182:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) )
            {
            // InternalComposedLts.g:1182:2: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:1183:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:1183:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:1184:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRuleRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            		newLeafNode(otherlv_0, grammarAccess.getRuleAccess().getActionActionCrossReference_0_0()); 
            	

            }


            }

            otherlv_1=(Token)match(input,23,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getRuleAccess().getHyphenMinusGreaterThanSignKeyword_1());
                
            // InternalComposedLts.g:1199:1: ( (otherlv_2= RULE_ID ) )
            // InternalComposedLts.g:1200:1: (otherlv_2= RULE_ID )
            {
            // InternalComposedLts.g:1200:1: (otherlv_2= RULE_ID )
            // InternalComposedLts.g:1201:3: otherlv_2= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getRuleRule());
            	        }
                    
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_2, grammarAccess.getRuleAccess().getNextStateCrossReference_2_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRule"


    // $ANTLR start "entryRuleLabel"
    // InternalComposedLts.g:1220:1: entryRuleLabel returns [EObject current=null] : iv_ruleLabel= ruleLabel EOF ;
    public final EObject entryRuleLabel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLabel = null;


        try {
            // InternalComposedLts.g:1221:2: (iv_ruleLabel= ruleLabel EOF )
            // InternalComposedLts.g:1222:2: iv_ruleLabel= ruleLabel EOF
            {
             newCompositeNode(grammarAccess.getLabelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLabel=ruleLabel();

            state._fsp--;

             current =iv_ruleLabel; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLabel"


    // $ANTLR start "ruleLabel"
    // InternalComposedLts.g:1229:1: ruleLabel returns [EObject current=null] : (otherlv_0= 'label' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) ;
    public final EObject ruleLabel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1232:28: ( (otherlv_0= 'label' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) )
            // InternalComposedLts.g:1233:1: (otherlv_0= 'label' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            {
            // InternalComposedLts.g:1233:1: (otherlv_0= 'label' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            // InternalComposedLts.g:1233:3: otherlv_0= 'label' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_3); 

                	newLeafNode(otherlv_0, grammarAccess.getLabelAccess().getLabelKeyword_0());
                
            // InternalComposedLts.g:1237:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalComposedLts.g:1238:1: (lv_name_1_0= RULE_ID )
            {
            // InternalComposedLts.g:1238:1: (lv_name_1_0= RULE_ID )
            // InternalComposedLts.g:1239:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            			newLeafNode(lv_name_1_0, grammarAccess.getLabelAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLabelRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getLabelAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLabel"


    // $ANTLR start "entryRuleAction"
    // InternalComposedLts.g:1267:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalComposedLts.g:1268:2: (iv_ruleAction= ruleAction EOF )
            // InternalComposedLts.g:1269:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalComposedLts.g:1276:1: ruleAction returns [EObject current=null] : (otherlv_0= 'action' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1279:28: ( (otherlv_0= 'action' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) )
            // InternalComposedLts.g:1280:1: (otherlv_0= 'action' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            {
            // InternalComposedLts.g:1280:1: (otherlv_0= 'action' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            // InternalComposedLts.g:1280:3: otherlv_0= 'action' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_3); 

                	newLeafNode(otherlv_0, grammarAccess.getActionAccess().getActionKeyword_0());
                
            // InternalComposedLts.g:1284:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalComposedLts.g:1285:1: (lv_name_1_0= RULE_ID )
            {
            // InternalComposedLts.g:1285:1: (lv_name_1_0= RULE_ID )
            // InternalComposedLts.g:1286:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            			newLeafNode(lv_name_1_0, grammarAccess.getActionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getActionAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleFormula"
    // InternalComposedLts.g:1314:1: entryRuleFormula returns [EObject current=null] : iv_ruleFormula= ruleFormula EOF ;
    public final EObject entryRuleFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormula = null;


        try {
            // InternalComposedLts.g:1315:2: (iv_ruleFormula= ruleFormula EOF )
            // InternalComposedLts.g:1316:2: iv_ruleFormula= ruleFormula EOF
            {
             newCompositeNode(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFormula=ruleFormula();

            state._fsp--;

             current =iv_ruleFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalComposedLts.g:1323:1: ruleFormula returns [EObject current=null] : (this_LtlFormulaDeclaration_0= ruleLtlFormulaDeclaration | this_HmlFormulaDeclaration_1= ruleHmlFormulaDeclaration ) ;
    public final EObject ruleFormula() throws RecognitionException {
        EObject current = null;

        EObject this_LtlFormulaDeclaration_0 = null;

        EObject this_HmlFormulaDeclaration_1 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1326:28: ( (this_LtlFormulaDeclaration_0= ruleLtlFormulaDeclaration | this_HmlFormulaDeclaration_1= ruleHmlFormulaDeclaration ) )
            // InternalComposedLts.g:1327:1: (this_LtlFormulaDeclaration_0= ruleLtlFormulaDeclaration | this_HmlFormulaDeclaration_1= ruleHmlFormulaDeclaration )
            {
            // InternalComposedLts.g:1327:1: (this_LtlFormulaDeclaration_0= ruleLtlFormulaDeclaration | this_HmlFormulaDeclaration_1= ruleHmlFormulaDeclaration )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==48) ) {
                alt18=1;
            }
            else if ( (LA18_0==34) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalComposedLts.g:1328:5: this_LtlFormulaDeclaration_0= ruleLtlFormulaDeclaration
                    {
                     
                            newCompositeNode(grammarAccess.getFormulaAccess().getLtlFormulaDeclarationParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlFormulaDeclaration_0=ruleLtlFormulaDeclaration();

                    state._fsp--;

                     
                            current = this_LtlFormulaDeclaration_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1338:5: this_HmlFormulaDeclaration_1= ruleHmlFormulaDeclaration
                    {
                     
                            newCompositeNode(grammarAccess.getFormulaAccess().getHmlFormulaDeclarationParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlFormulaDeclaration_1=ruleHmlFormulaDeclaration();

                    state._fsp--;

                     
                            current = this_HmlFormulaDeclaration_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleHmlFormulaDeclaration"
    // InternalComposedLts.g:1354:1: entryRuleHmlFormulaDeclaration returns [EObject current=null] : iv_ruleHmlFormulaDeclaration= ruleHmlFormulaDeclaration EOF ;
    public final EObject entryRuleHmlFormulaDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlFormulaDeclaration = null;


        try {
            // InternalComposedLts.g:1355:2: (iv_ruleHmlFormulaDeclaration= ruleHmlFormulaDeclaration EOF )
            // InternalComposedLts.g:1356:2: iv_ruleHmlFormulaDeclaration= ruleHmlFormulaDeclaration EOF
            {
             newCompositeNode(grammarAccess.getHmlFormulaDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlFormulaDeclaration=ruleHmlFormulaDeclaration();

            state._fsp--;

             current =iv_ruleHmlFormulaDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlFormulaDeclaration"


    // $ANTLR start "ruleHmlFormulaDeclaration"
    // InternalComposedLts.g:1363:1: ruleHmlFormulaDeclaration returns [EObject current=null] : (otherlv_0= 'hml' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleHmlFormula ) ) otherlv_5= ';' ) ;
    public final EObject ruleHmlFormulaDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_formula_4_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1366:28: ( (otherlv_0= 'hml' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleHmlFormula ) ) otherlv_5= ';' ) )
            // InternalComposedLts.g:1367:1: (otherlv_0= 'hml' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleHmlFormula ) ) otherlv_5= ';' )
            {
            // InternalComposedLts.g:1367:1: (otherlv_0= 'hml' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleHmlFormula ) ) otherlv_5= ';' )
            // InternalComposedLts.g:1367:3: otherlv_0= 'hml' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleHmlFormula ) ) otherlv_5= ';'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_26); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlFormulaDeclarationAccess().getHmlKeyword_0());
                
            otherlv_1=(Token)match(input,35,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getHmlFormulaDeclarationAccess().getFormulaKeyword_1());
                
            // InternalComposedLts.g:1375:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalComposedLts.g:1376:1: (lv_name_2_0= RULE_ID )
            {
            // InternalComposedLts.g:1376:1: (lv_name_2_0= RULE_ID )
            // InternalComposedLts.g:1377:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_27); 

            			newLeafNode(lv_name_2_0, grammarAccess.getHmlFormulaDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getHmlFormulaDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_28); 

                	newLeafNode(otherlv_3, grammarAccess.getHmlFormulaDeclarationAccess().getEqualsSignKeyword_3());
                
            // InternalComposedLts.g:1397:1: ( (lv_formula_4_0= ruleHmlFormula ) )
            // InternalComposedLts.g:1398:1: (lv_formula_4_0= ruleHmlFormula )
            {
            // InternalComposedLts.g:1398:1: (lv_formula_4_0= ruleHmlFormula )
            // InternalComposedLts.g:1399:3: lv_formula_4_0= ruleHmlFormula
            {
             
            	        newCompositeNode(grammarAccess.getHmlFormulaDeclarationAccess().getFormulaHmlFormulaParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_4);
            lv_formula_4_0=ruleHmlFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlFormulaDeclarationRule());
            	        }
                   		set(
                   			current, 
                   			"formula",
                    		lv_formula_4_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.HmlFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getHmlFormulaDeclarationAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlFormulaDeclaration"


    // $ANTLR start "entryRuleHmlFormula"
    // InternalComposedLts.g:1427:1: entryRuleHmlFormula returns [EObject current=null] : iv_ruleHmlFormula= ruleHmlFormula EOF ;
    public final EObject entryRuleHmlFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlFormula = null;


        try {
            // InternalComposedLts.g:1428:2: (iv_ruleHmlFormula= ruleHmlFormula EOF )
            // InternalComposedLts.g:1429:2: iv_ruleHmlFormula= ruleHmlFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlFormula=ruleHmlFormula();

            state._fsp--;

             current =iv_ruleHmlFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlFormula"


    // $ANTLR start "ruleHmlFormula"
    // InternalComposedLts.g:1436:1: ruleHmlFormula returns [EObject current=null] : this_HmlOrFormula_0= ruleHmlOrFormula ;
    public final EObject ruleHmlFormula() throws RecognitionException {
        EObject current = null;

        EObject this_HmlOrFormula_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1439:28: (this_HmlOrFormula_0= ruleHmlOrFormula )
            // InternalComposedLts.g:1441:5: this_HmlOrFormula_0= ruleHmlOrFormula
            {
             
                    newCompositeNode(grammarAccess.getHmlFormulaAccess().getHmlOrFormulaParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_HmlOrFormula_0=ruleHmlOrFormula();

            state._fsp--;

             
                    current = this_HmlOrFormula_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlFormula"


    // $ANTLR start "entryRuleHmlOrFormula"
    // InternalComposedLts.g:1457:1: entryRuleHmlOrFormula returns [EObject current=null] : iv_ruleHmlOrFormula= ruleHmlOrFormula EOF ;
    public final EObject entryRuleHmlOrFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlOrFormula = null;


        try {
            // InternalComposedLts.g:1458:2: (iv_ruleHmlOrFormula= ruleHmlOrFormula EOF )
            // InternalComposedLts.g:1459:2: iv_ruleHmlOrFormula= ruleHmlOrFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlOrFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlOrFormula=ruleHmlOrFormula();

            state._fsp--;

             current =iv_ruleHmlOrFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlOrFormula"


    // $ANTLR start "ruleHmlOrFormula"
    // InternalComposedLts.g:1466:1: ruleHmlOrFormula returns [EObject current=null] : (this_HmlAndFormula_0= ruleHmlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) ) )* ) ;
    public final EObject ruleHmlOrFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_HmlAndFormula_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1469:28: ( (this_HmlAndFormula_0= ruleHmlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) ) )* ) )
            // InternalComposedLts.g:1470:1: (this_HmlAndFormula_0= ruleHmlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) ) )* )
            {
            // InternalComposedLts.g:1470:1: (this_HmlAndFormula_0= ruleHmlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) ) )* )
            // InternalComposedLts.g:1471:5: this_HmlAndFormula_0= ruleHmlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getHmlOrFormulaAccess().getHmlAndFormulaParserRuleCall_0()); 
                
            pushFollow(FOLLOW_10);
            this_HmlAndFormula_0=ruleHmlAndFormula();

            state._fsp--;

             
                    current = this_HmlAndFormula_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:1479:1: ( () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==17) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalComposedLts.g:1479:2: () otherlv_2= '|' ( (lv_right_3_0= ruleHmlAndFormula ) )
            	    {
            	    // InternalComposedLts.g:1479:2: ()
            	    // InternalComposedLts.g:1480:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getHmlOrFormulaAccess().getHmlOrFormulaLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    otherlv_2=(Token)match(input,17,FOLLOW_28); 

            	        	newLeafNode(otherlv_2, grammarAccess.getHmlOrFormulaAccess().getVerticalLineKeyword_1_1());
            	        
            	    // InternalComposedLts.g:1489:1: ( (lv_right_3_0= ruleHmlAndFormula ) )
            	    // InternalComposedLts.g:1490:1: (lv_right_3_0= ruleHmlAndFormula )
            	    {
            	    // InternalComposedLts.g:1490:1: (lv_right_3_0= ruleHmlAndFormula )
            	    // InternalComposedLts.g:1491:3: lv_right_3_0= ruleHmlAndFormula
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getHmlOrFormulaAccess().getRightHmlAndFormulaParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_10);
            	    lv_right_3_0=ruleHmlAndFormula();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getHmlOrFormulaRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.HmlAndFormula");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlOrFormula"


    // $ANTLR start "entryRuleHmlAndFormula"
    // InternalComposedLts.g:1515:1: entryRuleHmlAndFormula returns [EObject current=null] : iv_ruleHmlAndFormula= ruleHmlAndFormula EOF ;
    public final EObject entryRuleHmlAndFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlAndFormula = null;


        try {
            // InternalComposedLts.g:1516:2: (iv_ruleHmlAndFormula= ruleHmlAndFormula EOF )
            // InternalComposedLts.g:1517:2: iv_ruleHmlAndFormula= ruleHmlAndFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlAndFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlAndFormula=ruleHmlAndFormula();

            state._fsp--;

             current =iv_ruleHmlAndFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlAndFormula"


    // $ANTLR start "ruleHmlAndFormula"
    // InternalComposedLts.g:1524:1: ruleHmlAndFormula returns [EObject current=null] : (this_HmlBaseFormula_0= ruleHmlBaseFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) ) )* ) ;
    public final EObject ruleHmlAndFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_HmlBaseFormula_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1527:28: ( (this_HmlBaseFormula_0= ruleHmlBaseFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) ) )* ) )
            // InternalComposedLts.g:1528:1: (this_HmlBaseFormula_0= ruleHmlBaseFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) ) )* )
            {
            // InternalComposedLts.g:1528:1: (this_HmlBaseFormula_0= ruleHmlBaseFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) ) )* )
            // InternalComposedLts.g:1529:5: this_HmlBaseFormula_0= ruleHmlBaseFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getHmlAndFormulaAccess().getHmlBaseFormulaParserRuleCall_0()); 
                
            pushFollow(FOLLOW_29);
            this_HmlBaseFormula_0=ruleHmlBaseFormula();

            state._fsp--;

             
                    current = this_HmlBaseFormula_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:1537:1: ( () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==36) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalComposedLts.g:1537:2: () otherlv_2= '&' ( (lv_right_3_0= ruleHmlBaseFormula ) )
            	    {
            	    // InternalComposedLts.g:1537:2: ()
            	    // InternalComposedLts.g:1538:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getHmlAndFormulaAccess().getHmlAndFormulaLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    otherlv_2=(Token)match(input,36,FOLLOW_28); 

            	        	newLeafNode(otherlv_2, grammarAccess.getHmlAndFormulaAccess().getAmpersandKeyword_1_1());
            	        
            	    // InternalComposedLts.g:1547:1: ( (lv_right_3_0= ruleHmlBaseFormula ) )
            	    // InternalComposedLts.g:1548:1: (lv_right_3_0= ruleHmlBaseFormula )
            	    {
            	    // InternalComposedLts.g:1548:1: (lv_right_3_0= ruleHmlBaseFormula )
            	    // InternalComposedLts.g:1549:3: lv_right_3_0= ruleHmlBaseFormula
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getHmlAndFormulaAccess().getRightHmlBaseFormulaParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_29);
            	    lv_right_3_0=ruleHmlBaseFormula();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getHmlAndFormulaRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.HmlBaseFormula");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlAndFormula"


    // $ANTLR start "entryRuleHmlBaseFormula"
    // InternalComposedLts.g:1573:1: entryRuleHmlBaseFormula returns [EObject current=null] : iv_ruleHmlBaseFormula= ruleHmlBaseFormula EOF ;
    public final EObject entryRuleHmlBaseFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlBaseFormula = null;


        try {
            // InternalComposedLts.g:1574:2: (iv_ruleHmlBaseFormula= ruleHmlBaseFormula EOF )
            // InternalComposedLts.g:1575:2: iv_ruleHmlBaseFormula= ruleHmlBaseFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlBaseFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlBaseFormula=ruleHmlBaseFormula();

            state._fsp--;

             current =iv_ruleHmlBaseFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlBaseFormula"


    // $ANTLR start "ruleHmlBaseFormula"
    // InternalComposedLts.g:1582:1: ruleHmlBaseFormula returns [EObject current=null] : (this_HmlTrue_0= ruleHmlTrue | this_HmlFalse_1= ruleHmlFalse | (otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')' ) | this_HmlNotFormula_5= ruleHmlNotFormula | this_HmlDiamondFormula_6= ruleHmlDiamondFormula | this_HmlBoxFormula_7= ruleHmlBoxFormula | this_HmlRecursionFormula_8= ruleHmlRecursionFormula | this_HmlRecursionVariable_9= ruleHmlRecursionVariable | this_HmlPredicate_10= ruleHmlPredicate ) ;
    public final EObject ruleHmlBaseFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_HmlTrue_0 = null;

        EObject this_HmlFalse_1 = null;

        EObject this_HmlFormula_3 = null;

        EObject this_HmlNotFormula_5 = null;

        EObject this_HmlDiamondFormula_6 = null;

        EObject this_HmlBoxFormula_7 = null;

        EObject this_HmlRecursionFormula_8 = null;

        EObject this_HmlRecursionVariable_9 = null;

        EObject this_HmlPredicate_10 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1585:28: ( (this_HmlTrue_0= ruleHmlTrue | this_HmlFalse_1= ruleHmlFalse | (otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')' ) | this_HmlNotFormula_5= ruleHmlNotFormula | this_HmlDiamondFormula_6= ruleHmlDiamondFormula | this_HmlBoxFormula_7= ruleHmlBoxFormula | this_HmlRecursionFormula_8= ruleHmlRecursionFormula | this_HmlRecursionVariable_9= ruleHmlRecursionVariable | this_HmlPredicate_10= ruleHmlPredicate ) )
            // InternalComposedLts.g:1586:1: (this_HmlTrue_0= ruleHmlTrue | this_HmlFalse_1= ruleHmlFalse | (otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')' ) | this_HmlNotFormula_5= ruleHmlNotFormula | this_HmlDiamondFormula_6= ruleHmlDiamondFormula | this_HmlBoxFormula_7= ruleHmlBoxFormula | this_HmlRecursionFormula_8= ruleHmlRecursionFormula | this_HmlRecursionVariable_9= ruleHmlRecursionVariable | this_HmlPredicate_10= ruleHmlPredicate )
            {
            // InternalComposedLts.g:1586:1: (this_HmlTrue_0= ruleHmlTrue | this_HmlFalse_1= ruleHmlFalse | (otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')' ) | this_HmlNotFormula_5= ruleHmlNotFormula | this_HmlDiamondFormula_6= ruleHmlDiamondFormula | this_HmlBoxFormula_7= ruleHmlBoxFormula | this_HmlRecursionFormula_8= ruleHmlRecursionFormula | this_HmlRecursionVariable_9= ruleHmlRecursionVariable | this_HmlPredicate_10= ruleHmlPredicate )
            int alt21=9;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt21=1;
                }
                break;
            case 40:
                {
                alt21=2;
                }
                break;
            case 21:
                {
                alt21=3;
                }
                break;
            case 41:
                {
                alt21=4;
                }
                break;
            case 42:
                {
                alt21=5;
                }
                break;
            case 44:
                {
                alt21=6;
                }
                break;
            case 45:
            case 47:
                {
                alt21=7;
                }
                break;
            case RULE_ID:
                {
                alt21=8;
                }
                break;
            case 37:
                {
                alt21=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }

            switch (alt21) {
                case 1 :
                    // InternalComposedLts.g:1587:5: this_HmlTrue_0= ruleHmlTrue
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlTrueParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlTrue_0=ruleHmlTrue();

                    state._fsp--;

                     
                            current = this_HmlTrue_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1597:5: this_HmlFalse_1= ruleHmlFalse
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlFalseParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlFalse_1=ruleHmlFalse();

                    state._fsp--;

                     
                            current = this_HmlFalse_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:1606:6: (otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')' )
                    {
                    // InternalComposedLts.g:1606:6: (otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')' )
                    // InternalComposedLts.g:1606:8: otherlv_2= '(' this_HmlFormula_3= ruleHmlFormula otherlv_4= ')'
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_28); 

                        	newLeafNode(otherlv_2, grammarAccess.getHmlBaseFormulaAccess().getLeftParenthesisKeyword_2_0());
                        
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlFormulaParserRuleCall_2_1()); 
                        
                    pushFollow(FOLLOW_14);
                    this_HmlFormula_3=ruleHmlFormula();

                    state._fsp--;

                     
                            current = this_HmlFormula_3; 
                            afterParserOrEnumRuleCall();
                        
                    otherlv_4=(Token)match(input,22,FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getHmlBaseFormulaAccess().getRightParenthesisKeyword_2_2());
                        

                    }


                    }
                    break;
                case 4 :
                    // InternalComposedLts.g:1625:5: this_HmlNotFormula_5= ruleHmlNotFormula
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlNotFormulaParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlNotFormula_5=ruleHmlNotFormula();

                    state._fsp--;

                     
                            current = this_HmlNotFormula_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalComposedLts.g:1635:5: this_HmlDiamondFormula_6= ruleHmlDiamondFormula
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlDiamondFormulaParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlDiamondFormula_6=ruleHmlDiamondFormula();

                    state._fsp--;

                     
                            current = this_HmlDiamondFormula_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalComposedLts.g:1645:5: this_HmlBoxFormula_7= ruleHmlBoxFormula
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlBoxFormulaParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlBoxFormula_7=ruleHmlBoxFormula();

                    state._fsp--;

                     
                            current = this_HmlBoxFormula_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalComposedLts.g:1655:5: this_HmlRecursionFormula_8= ruleHmlRecursionFormula
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlRecursionFormulaParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlRecursionFormula_8=ruleHmlRecursionFormula();

                    state._fsp--;

                     
                            current = this_HmlRecursionFormula_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // InternalComposedLts.g:1665:5: this_HmlRecursionVariable_9= ruleHmlRecursionVariable
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlRecursionVariableParserRuleCall_7()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlRecursionVariable_9=ruleHmlRecursionVariable();

                    state._fsp--;

                     
                            current = this_HmlRecursionVariable_9; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // InternalComposedLts.g:1675:5: this_HmlPredicate_10= ruleHmlPredicate
                    {
                     
                            newCompositeNode(grammarAccess.getHmlBaseFormulaAccess().getHmlPredicateParserRuleCall_8()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlPredicate_10=ruleHmlPredicate();

                    state._fsp--;

                     
                            current = this_HmlPredicate_10; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlBaseFormula"


    // $ANTLR start "entryRuleHmlPredicate"
    // InternalComposedLts.g:1691:1: entryRuleHmlPredicate returns [EObject current=null] : iv_ruleHmlPredicate= ruleHmlPredicate EOF ;
    public final EObject entryRuleHmlPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlPredicate = null;


        try {
            // InternalComposedLts.g:1692:2: (iv_ruleHmlPredicate= ruleHmlPredicate EOF )
            // InternalComposedLts.g:1693:2: iv_ruleHmlPredicate= ruleHmlPredicate EOF
            {
             newCompositeNode(grammarAccess.getHmlPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlPredicate=ruleHmlPredicate();

            state._fsp--;

             current =iv_ruleHmlPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlPredicate"


    // $ANTLR start "ruleHmlPredicate"
    // InternalComposedLts.g:1700:1: ruleHmlPredicate returns [EObject current=null] : (otherlv_0= '#[' ( (otherlv_1= RULE_ID ) ) otherlv_2= ']' ) ;
    public final EObject ruleHmlPredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1703:28: ( (otherlv_0= '#[' ( (otherlv_1= RULE_ID ) ) otherlv_2= ']' ) )
            // InternalComposedLts.g:1704:1: (otherlv_0= '#[' ( (otherlv_1= RULE_ID ) ) otherlv_2= ']' )
            {
            // InternalComposedLts.g:1704:1: (otherlv_0= '#[' ( (otherlv_1= RULE_ID ) ) otherlv_2= ']' )
            // InternalComposedLts.g:1704:3: otherlv_0= '#[' ( (otherlv_1= RULE_ID ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_3); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlPredicateAccess().getNumberSignLeftSquareBracketKeyword_0());
                
            // InternalComposedLts.g:1708:1: ( (otherlv_1= RULE_ID ) )
            // InternalComposedLts.g:1709:1: (otherlv_1= RULE_ID )
            {
            // InternalComposedLts.g:1709:1: (otherlv_1= RULE_ID )
            // InternalComposedLts.g:1710:3: otherlv_1= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getHmlPredicateRule());
            	        }
                    
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_30); 

            		newLeafNode(otherlv_1, grammarAccess.getHmlPredicateAccess().getLabelLabelCrossReference_1_0()); 
            	

            }


            }

            otherlv_2=(Token)match(input,38,FOLLOW_2); 

                	newLeafNode(otherlv_2, grammarAccess.getHmlPredicateAccess().getRightSquareBracketKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlPredicate"


    // $ANTLR start "entryRuleHmlTrue"
    // InternalComposedLts.g:1733:1: entryRuleHmlTrue returns [EObject current=null] : iv_ruleHmlTrue= ruleHmlTrue EOF ;
    public final EObject entryRuleHmlTrue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlTrue = null;


        try {
            // InternalComposedLts.g:1734:2: (iv_ruleHmlTrue= ruleHmlTrue EOF )
            // InternalComposedLts.g:1735:2: iv_ruleHmlTrue= ruleHmlTrue EOF
            {
             newCompositeNode(grammarAccess.getHmlTrueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlTrue=ruleHmlTrue();

            state._fsp--;

             current =iv_ruleHmlTrue; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlTrue"


    // $ANTLR start "ruleHmlTrue"
    // InternalComposedLts.g:1742:1: ruleHmlTrue returns [EObject current=null] : ( () otherlv_1= 'true' ) ;
    public final EObject ruleHmlTrue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1745:28: ( ( () otherlv_1= 'true' ) )
            // InternalComposedLts.g:1746:1: ( () otherlv_1= 'true' )
            {
            // InternalComposedLts.g:1746:1: ( () otherlv_1= 'true' )
            // InternalComposedLts.g:1746:2: () otherlv_1= 'true'
            {
            // InternalComposedLts.g:1746:2: ()
            // InternalComposedLts.g:1747:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getHmlTrueAccess().getHmlTrueAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,39,FOLLOW_2); 

                	newLeafNode(otherlv_1, grammarAccess.getHmlTrueAccess().getTrueKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlTrue"


    // $ANTLR start "entryRuleHmlFalse"
    // InternalComposedLts.g:1764:1: entryRuleHmlFalse returns [EObject current=null] : iv_ruleHmlFalse= ruleHmlFalse EOF ;
    public final EObject entryRuleHmlFalse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlFalse = null;


        try {
            // InternalComposedLts.g:1765:2: (iv_ruleHmlFalse= ruleHmlFalse EOF )
            // InternalComposedLts.g:1766:2: iv_ruleHmlFalse= ruleHmlFalse EOF
            {
             newCompositeNode(grammarAccess.getHmlFalseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlFalse=ruleHmlFalse();

            state._fsp--;

             current =iv_ruleHmlFalse; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlFalse"


    // $ANTLR start "ruleHmlFalse"
    // InternalComposedLts.g:1773:1: ruleHmlFalse returns [EObject current=null] : ( () otherlv_1= 'false' ) ;
    public final EObject ruleHmlFalse() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:1776:28: ( ( () otherlv_1= 'false' ) )
            // InternalComposedLts.g:1777:1: ( () otherlv_1= 'false' )
            {
            // InternalComposedLts.g:1777:1: ( () otherlv_1= 'false' )
            // InternalComposedLts.g:1777:2: () otherlv_1= 'false'
            {
            // InternalComposedLts.g:1777:2: ()
            // InternalComposedLts.g:1778:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getHmlFalseAccess().getHmlFalseAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,40,FOLLOW_2); 

                	newLeafNode(otherlv_1, grammarAccess.getHmlFalseAccess().getFalseKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlFalse"


    // $ANTLR start "entryRuleHmlNotFormula"
    // InternalComposedLts.g:1795:1: entryRuleHmlNotFormula returns [EObject current=null] : iv_ruleHmlNotFormula= ruleHmlNotFormula EOF ;
    public final EObject entryRuleHmlNotFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlNotFormula = null;


        try {
            // InternalComposedLts.g:1796:2: (iv_ruleHmlNotFormula= ruleHmlNotFormula EOF )
            // InternalComposedLts.g:1797:2: iv_ruleHmlNotFormula= ruleHmlNotFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlNotFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlNotFormula=ruleHmlNotFormula();

            state._fsp--;

             current =iv_ruleHmlNotFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlNotFormula"


    // $ANTLR start "ruleHmlNotFormula"
    // InternalComposedLts.g:1804:1: ruleHmlNotFormula returns [EObject current=null] : (otherlv_0= '!' ( (lv_arg_1_0= ruleHmlBaseFormula ) ) ) ;
    public final EObject ruleHmlNotFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1807:28: ( (otherlv_0= '!' ( (lv_arg_1_0= ruleHmlBaseFormula ) ) ) )
            // InternalComposedLts.g:1808:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleHmlBaseFormula ) ) )
            {
            // InternalComposedLts.g:1808:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleHmlBaseFormula ) ) )
            // InternalComposedLts.g:1808:3: otherlv_0= '!' ( (lv_arg_1_0= ruleHmlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,41,FOLLOW_28); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlNotFormulaAccess().getExclamationMarkKeyword_0());
                
            // InternalComposedLts.g:1812:1: ( (lv_arg_1_0= ruleHmlBaseFormula ) )
            // InternalComposedLts.g:1813:1: (lv_arg_1_0= ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:1813:1: (lv_arg_1_0= ruleHmlBaseFormula )
            // InternalComposedLts.g:1814:3: lv_arg_1_0= ruleHmlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getHmlNotFormulaAccess().getArgHmlBaseFormulaParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_1_0=ruleHmlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlNotFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.HmlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlNotFormula"


    // $ANTLR start "entryRuleHmlDiamondFormula"
    // InternalComposedLts.g:1838:1: entryRuleHmlDiamondFormula returns [EObject current=null] : iv_ruleHmlDiamondFormula= ruleHmlDiamondFormula EOF ;
    public final EObject entryRuleHmlDiamondFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlDiamondFormula = null;


        try {
            // InternalComposedLts.g:1839:2: (iv_ruleHmlDiamondFormula= ruleHmlDiamondFormula EOF )
            // InternalComposedLts.g:1840:2: iv_ruleHmlDiamondFormula= ruleHmlDiamondFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlDiamondFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlDiamondFormula=ruleHmlDiamondFormula();

            state._fsp--;

             current =iv_ruleHmlDiamondFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlDiamondFormula"


    // $ANTLR start "ruleHmlDiamondFormula"
    // InternalComposedLts.g:1847:1: ruleHmlDiamondFormula returns [EObject current=null] : (otherlv_0= '<' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) ;
    public final EObject ruleHmlDiamondFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_action_1_0 = null;

        EObject lv_arg_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1850:28: ( (otherlv_0= '<' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) )
            // InternalComposedLts.g:1851:1: (otherlv_0= '<' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            {
            // InternalComposedLts.g:1851:1: (otherlv_0= '<' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            // InternalComposedLts.g:1851:3: otherlv_0= '<' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_31); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlDiamondFormulaAccess().getLessThanSignKeyword_0());
                
            // InternalComposedLts.g:1855:1: ( (lv_action_1_0= ruleLabelPredicate ) )
            // InternalComposedLts.g:1856:1: (lv_action_1_0= ruleLabelPredicate )
            {
            // InternalComposedLts.g:1856:1: (lv_action_1_0= ruleLabelPredicate )
            // InternalComposedLts.g:1857:3: lv_action_1_0= ruleLabelPredicate
            {
             
            	        newCompositeNode(grammarAccess.getHmlDiamondFormulaAccess().getActionLabelPredicateParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_32);
            lv_action_1_0=ruleLabelPredicate();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlDiamondFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"action",
                    		lv_action_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LabelPredicate");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,43,FOLLOW_28); 

                	newLeafNode(otherlv_2, grammarAccess.getHmlDiamondFormulaAccess().getGreaterThanSignKeyword_2());
                
            // InternalComposedLts.g:1877:1: ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            // InternalComposedLts.g:1878:1: (lv_arg_3_0= ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:1878:1: (lv_arg_3_0= ruleHmlBaseFormula )
            // InternalComposedLts.g:1879:3: lv_arg_3_0= ruleHmlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getHmlDiamondFormulaAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_3_0=ruleHmlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlDiamondFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_3_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.HmlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlDiamondFormula"


    // $ANTLR start "entryRuleHmlBoxFormula"
    // InternalComposedLts.g:1903:1: entryRuleHmlBoxFormula returns [EObject current=null] : iv_ruleHmlBoxFormula= ruleHmlBoxFormula EOF ;
    public final EObject entryRuleHmlBoxFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlBoxFormula = null;


        try {
            // InternalComposedLts.g:1904:2: (iv_ruleHmlBoxFormula= ruleHmlBoxFormula EOF )
            // InternalComposedLts.g:1905:2: iv_ruleHmlBoxFormula= ruleHmlBoxFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlBoxFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlBoxFormula=ruleHmlBoxFormula();

            state._fsp--;

             current =iv_ruleHmlBoxFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlBoxFormula"


    // $ANTLR start "ruleHmlBoxFormula"
    // InternalComposedLts.g:1912:1: ruleHmlBoxFormula returns [EObject current=null] : (otherlv_0= '[' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= ']' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) ;
    public final EObject ruleHmlBoxFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_action_1_0 = null;

        EObject lv_arg_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1915:28: ( (otherlv_0= '[' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= ']' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) )
            // InternalComposedLts.g:1916:1: (otherlv_0= '[' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= ']' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            {
            // InternalComposedLts.g:1916:1: (otherlv_0= '[' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= ']' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            // InternalComposedLts.g:1916:3: otherlv_0= '[' ( (lv_action_1_0= ruleLabelPredicate ) ) otherlv_2= ']' ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,44,FOLLOW_31); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlBoxFormulaAccess().getLeftSquareBracketKeyword_0());
                
            // InternalComposedLts.g:1920:1: ( (lv_action_1_0= ruleLabelPredicate ) )
            // InternalComposedLts.g:1921:1: (lv_action_1_0= ruleLabelPredicate )
            {
            // InternalComposedLts.g:1921:1: (lv_action_1_0= ruleLabelPredicate )
            // InternalComposedLts.g:1922:3: lv_action_1_0= ruleLabelPredicate
            {
             
            	        newCompositeNode(grammarAccess.getHmlBoxFormulaAccess().getActionLabelPredicateParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_30);
            lv_action_1_0=ruleLabelPredicate();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlBoxFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"action",
                    		lv_action_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LabelPredicate");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,38,FOLLOW_28); 

                	newLeafNode(otherlv_2, grammarAccess.getHmlBoxFormulaAccess().getRightSquareBracketKeyword_2());
                
            // InternalComposedLts.g:1942:1: ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            // InternalComposedLts.g:1943:1: (lv_arg_3_0= ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:1943:1: (lv_arg_3_0= ruleHmlBaseFormula )
            // InternalComposedLts.g:1944:3: lv_arg_3_0= ruleHmlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getHmlBoxFormulaAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_3_0=ruleHmlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlBoxFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_3_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.HmlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlBoxFormula"


    // $ANTLR start "entryRuleHmlRecursionFormula"
    // InternalComposedLts.g:1968:1: entryRuleHmlRecursionFormula returns [EObject current=null] : iv_ruleHmlRecursionFormula= ruleHmlRecursionFormula EOF ;
    public final EObject entryRuleHmlRecursionFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlRecursionFormula = null;


        try {
            // InternalComposedLts.g:1969:2: (iv_ruleHmlRecursionFormula= ruleHmlRecursionFormula EOF )
            // InternalComposedLts.g:1970:2: iv_ruleHmlRecursionFormula= ruleHmlRecursionFormula EOF
            {
             newCompositeNode(grammarAccess.getHmlRecursionFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlRecursionFormula=ruleHmlRecursionFormula();

            state._fsp--;

             current =iv_ruleHmlRecursionFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlRecursionFormula"


    // $ANTLR start "ruleHmlRecursionFormula"
    // InternalComposedLts.g:1977:1: ruleHmlRecursionFormula returns [EObject current=null] : (this_HmlMinFixPoint_0= ruleHmlMinFixPoint | this_HmlMaxFixPoint_1= ruleHmlMaxFixPoint ) ;
    public final EObject ruleHmlRecursionFormula() throws RecognitionException {
        EObject current = null;

        EObject this_HmlMinFixPoint_0 = null;

        EObject this_HmlMaxFixPoint_1 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:1980:28: ( (this_HmlMinFixPoint_0= ruleHmlMinFixPoint | this_HmlMaxFixPoint_1= ruleHmlMaxFixPoint ) )
            // InternalComposedLts.g:1981:1: (this_HmlMinFixPoint_0= ruleHmlMinFixPoint | this_HmlMaxFixPoint_1= ruleHmlMaxFixPoint )
            {
            // InternalComposedLts.g:1981:1: (this_HmlMinFixPoint_0= ruleHmlMinFixPoint | this_HmlMaxFixPoint_1= ruleHmlMaxFixPoint )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==45) ) {
                alt22=1;
            }
            else if ( (LA22_0==47) ) {
                alt22=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalComposedLts.g:1982:5: this_HmlMinFixPoint_0= ruleHmlMinFixPoint
                    {
                     
                            newCompositeNode(grammarAccess.getHmlRecursionFormulaAccess().getHmlMinFixPointParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlMinFixPoint_0=ruleHmlMinFixPoint();

                    state._fsp--;

                     
                            current = this_HmlMinFixPoint_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:1992:5: this_HmlMaxFixPoint_1= ruleHmlMaxFixPoint
                    {
                     
                            newCompositeNode(grammarAccess.getHmlRecursionFormulaAccess().getHmlMaxFixPointParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_HmlMaxFixPoint_1=ruleHmlMaxFixPoint();

                    state._fsp--;

                     
                            current = this_HmlMaxFixPoint_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlRecursionFormula"


    // $ANTLR start "entryRuleHmlMinFixPoint"
    // InternalComposedLts.g:2008:1: entryRuleHmlMinFixPoint returns [EObject current=null] : iv_ruleHmlMinFixPoint= ruleHmlMinFixPoint EOF ;
    public final EObject entryRuleHmlMinFixPoint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlMinFixPoint = null;


        try {
            // InternalComposedLts.g:2009:2: (iv_ruleHmlMinFixPoint= ruleHmlMinFixPoint EOF )
            // InternalComposedLts.g:2010:2: iv_ruleHmlMinFixPoint= ruleHmlMinFixPoint EOF
            {
             newCompositeNode(grammarAccess.getHmlMinFixPointRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlMinFixPoint=ruleHmlMinFixPoint();

            state._fsp--;

             current =iv_ruleHmlMinFixPoint; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlMinFixPoint"


    // $ANTLR start "ruleHmlMinFixPoint"
    // InternalComposedLts.g:2017:1: ruleHmlMinFixPoint returns [EObject current=null] : (otherlv_0= 'min' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) ;
    public final EObject ruleHmlMinFixPoint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_arg_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2020:28: ( (otherlv_0= 'min' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) )
            // InternalComposedLts.g:2021:1: (otherlv_0= 'min' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            {
            // InternalComposedLts.g:2021:1: (otherlv_0= 'min' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            // InternalComposedLts.g:2021:3: otherlv_0= 'min' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,45,FOLLOW_3); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlMinFixPointAccess().getMinKeyword_0());
                
            // InternalComposedLts.g:2025:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalComposedLts.g:2026:1: (lv_name_1_0= RULE_ID )
            {
            // InternalComposedLts.g:2026:1: (lv_name_1_0= RULE_ID )
            // InternalComposedLts.g:2027:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_33); 

            			newLeafNode(lv_name_1_0, grammarAccess.getHmlMinFixPointAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getHmlMinFixPointRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,46,FOLLOW_28); 

                	newLeafNode(otherlv_2, grammarAccess.getHmlMinFixPointAccess().getFullStopKeyword_2());
                
            // InternalComposedLts.g:2047:1: ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            // InternalComposedLts.g:2048:1: (lv_arg_3_0= ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:2048:1: (lv_arg_3_0= ruleHmlBaseFormula )
            // InternalComposedLts.g:2049:3: lv_arg_3_0= ruleHmlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getHmlMinFixPointAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_3_0=ruleHmlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlMinFixPointRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_3_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.HmlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlMinFixPoint"


    // $ANTLR start "entryRuleHmlMaxFixPoint"
    // InternalComposedLts.g:2073:1: entryRuleHmlMaxFixPoint returns [EObject current=null] : iv_ruleHmlMaxFixPoint= ruleHmlMaxFixPoint EOF ;
    public final EObject entryRuleHmlMaxFixPoint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlMaxFixPoint = null;


        try {
            // InternalComposedLts.g:2074:2: (iv_ruleHmlMaxFixPoint= ruleHmlMaxFixPoint EOF )
            // InternalComposedLts.g:2075:2: iv_ruleHmlMaxFixPoint= ruleHmlMaxFixPoint EOF
            {
             newCompositeNode(grammarAccess.getHmlMaxFixPointRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlMaxFixPoint=ruleHmlMaxFixPoint();

            state._fsp--;

             current =iv_ruleHmlMaxFixPoint; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlMaxFixPoint"


    // $ANTLR start "ruleHmlMaxFixPoint"
    // InternalComposedLts.g:2082:1: ruleHmlMaxFixPoint returns [EObject current=null] : (otherlv_0= 'max' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) ;
    public final EObject ruleHmlMaxFixPoint() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_arg_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2085:28: ( (otherlv_0= 'max' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) ) )
            // InternalComposedLts.g:2086:1: (otherlv_0= 'max' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            {
            // InternalComposedLts.g:2086:1: (otherlv_0= 'max' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) ) )
            // InternalComposedLts.g:2086:3: otherlv_0= 'max' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '.' ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,47,FOLLOW_3); 

                	newLeafNode(otherlv_0, grammarAccess.getHmlMaxFixPointAccess().getMaxKeyword_0());
                
            // InternalComposedLts.g:2090:1: ( (lv_name_1_0= RULE_ID ) )
            // InternalComposedLts.g:2091:1: (lv_name_1_0= RULE_ID )
            {
            // InternalComposedLts.g:2091:1: (lv_name_1_0= RULE_ID )
            // InternalComposedLts.g:2092:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_33); 

            			newLeafNode(lv_name_1_0, grammarAccess.getHmlMaxFixPointAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getHmlMaxFixPointRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,46,FOLLOW_28); 

                	newLeafNode(otherlv_2, grammarAccess.getHmlMaxFixPointAccess().getFullStopKeyword_2());
                
            // InternalComposedLts.g:2112:1: ( (lv_arg_3_0= ruleHmlBaseFormula ) )
            // InternalComposedLts.g:2113:1: (lv_arg_3_0= ruleHmlBaseFormula )
            {
            // InternalComposedLts.g:2113:1: (lv_arg_3_0= ruleHmlBaseFormula )
            // InternalComposedLts.g:2114:3: lv_arg_3_0= ruleHmlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getHmlMaxFixPointAccess().getArgHmlBaseFormulaParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_3_0=ruleHmlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getHmlMaxFixPointRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_3_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.HmlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlMaxFixPoint"


    // $ANTLR start "entryRuleHmlRecursionVariable"
    // InternalComposedLts.g:2138:1: entryRuleHmlRecursionVariable returns [EObject current=null] : iv_ruleHmlRecursionVariable= ruleHmlRecursionVariable EOF ;
    public final EObject entryRuleHmlRecursionVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHmlRecursionVariable = null;


        try {
            // InternalComposedLts.g:2139:2: (iv_ruleHmlRecursionVariable= ruleHmlRecursionVariable EOF )
            // InternalComposedLts.g:2140:2: iv_ruleHmlRecursionVariable= ruleHmlRecursionVariable EOF
            {
             newCompositeNode(grammarAccess.getHmlRecursionVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleHmlRecursionVariable=ruleHmlRecursionVariable();

            state._fsp--;

             current =iv_ruleHmlRecursionVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHmlRecursionVariable"


    // $ANTLR start "ruleHmlRecursionVariable"
    // InternalComposedLts.g:2147:1: ruleHmlRecursionVariable returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleHmlRecursionVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:2150:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalComposedLts.g:2151:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalComposedLts.g:2151:1: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:2152:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:2152:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:2153:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getHmlRecursionVariableRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_0, grammarAccess.getHmlRecursionVariableAccess().getRefeerenceHmlRecursionFormulaCrossReference_0()); 
            	

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHmlRecursionVariable"


    // $ANTLR start "entryRuleLtlFormulaDeclaration"
    // InternalComposedLts.g:2172:1: entryRuleLtlFormulaDeclaration returns [EObject current=null] : iv_ruleLtlFormulaDeclaration= ruleLtlFormulaDeclaration EOF ;
    public final EObject entryRuleLtlFormulaDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlFormulaDeclaration = null;


        try {
            // InternalComposedLts.g:2173:2: (iv_ruleLtlFormulaDeclaration= ruleLtlFormulaDeclaration EOF )
            // InternalComposedLts.g:2174:2: iv_ruleLtlFormulaDeclaration= ruleLtlFormulaDeclaration EOF
            {
             newCompositeNode(grammarAccess.getLtlFormulaDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlFormulaDeclaration=ruleLtlFormulaDeclaration();

            state._fsp--;

             current =iv_ruleLtlFormulaDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlFormulaDeclaration"


    // $ANTLR start "ruleLtlFormulaDeclaration"
    // InternalComposedLts.g:2181:1: ruleLtlFormulaDeclaration returns [EObject current=null] : (otherlv_0= 'ltl' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleLtlFormula ) ) otherlv_5= ';' ) ;
    public final EObject ruleLtlFormulaDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_formula_4_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2184:28: ( (otherlv_0= 'ltl' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleLtlFormula ) ) otherlv_5= ';' ) )
            // InternalComposedLts.g:2185:1: (otherlv_0= 'ltl' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleLtlFormula ) ) otherlv_5= ';' )
            {
            // InternalComposedLts.g:2185:1: (otherlv_0= 'ltl' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleLtlFormula ) ) otherlv_5= ';' )
            // InternalComposedLts.g:2185:3: otherlv_0= 'ltl' otherlv_1= 'formula' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_formula_4_0= ruleLtlFormula ) ) otherlv_5= ';'
            {
            otherlv_0=(Token)match(input,48,FOLLOW_26); 

                	newLeafNode(otherlv_0, grammarAccess.getLtlFormulaDeclarationAccess().getLtlKeyword_0());
                
            otherlv_1=(Token)match(input,35,FOLLOW_3); 

                	newLeafNode(otherlv_1, grammarAccess.getLtlFormulaDeclarationAccess().getFormulaKeyword_1());
                
            // InternalComposedLts.g:2193:1: ( (lv_name_2_0= RULE_ID ) )
            // InternalComposedLts.g:2194:1: (lv_name_2_0= RULE_ID )
            {
            // InternalComposedLts.g:2194:1: (lv_name_2_0= RULE_ID )
            // InternalComposedLts.g:2195:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_27); 

            			newLeafNode(lv_name_2_0, grammarAccess.getLtlFormulaDeclarationAccess().getNameIDTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getLtlFormulaDeclarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"org.eclipse.xtext.common.Terminals.ID");
            	    

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_34); 

                	newLeafNode(otherlv_3, grammarAccess.getLtlFormulaDeclarationAccess().getEqualsSignKeyword_3());
                
            // InternalComposedLts.g:2215:1: ( (lv_formula_4_0= ruleLtlFormula ) )
            // InternalComposedLts.g:2216:1: (lv_formula_4_0= ruleLtlFormula )
            {
            // InternalComposedLts.g:2216:1: (lv_formula_4_0= ruleLtlFormula )
            // InternalComposedLts.g:2217:3: lv_formula_4_0= ruleLtlFormula
            {
             
            	        newCompositeNode(grammarAccess.getLtlFormulaDeclarationAccess().getFormulaLtlFormulaParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_4);
            lv_formula_4_0=ruleLtlFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtlFormulaDeclarationRule());
            	        }
                   		set(
                   			current, 
                   			"formula",
                    		lv_formula_4_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtlFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_2); 

                	newLeafNode(otherlv_5, grammarAccess.getLtlFormulaDeclarationAccess().getSemicolonKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlFormulaDeclaration"


    // $ANTLR start "entryRuleLtlFormula"
    // InternalComposedLts.g:2245:1: entryRuleLtlFormula returns [EObject current=null] : iv_ruleLtlFormula= ruleLtlFormula EOF ;
    public final EObject entryRuleLtlFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlFormula = null;


        try {
            // InternalComposedLts.g:2246:2: (iv_ruleLtlFormula= ruleLtlFormula EOF )
            // InternalComposedLts.g:2247:2: iv_ruleLtlFormula= ruleLtlFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlFormula=ruleLtlFormula();

            state._fsp--;

             current =iv_ruleLtlFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlFormula"


    // $ANTLR start "ruleLtlFormula"
    // InternalComposedLts.g:2254:1: ruleLtlFormula returns [EObject current=null] : this_LtlOrFormula_0= ruleLtlOrFormula ;
    public final EObject ruleLtlFormula() throws RecognitionException {
        EObject current = null;

        EObject this_LtlOrFormula_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2257:28: (this_LtlOrFormula_0= ruleLtlOrFormula )
            // InternalComposedLts.g:2259:5: this_LtlOrFormula_0= ruleLtlOrFormula
            {
             
                    newCompositeNode(grammarAccess.getLtlFormulaAccess().getLtlOrFormulaParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_LtlOrFormula_0=ruleLtlOrFormula();

            state._fsp--;

             
                    current = this_LtlOrFormula_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlFormula"


    // $ANTLR start "entryRuleLtlOrFormula"
    // InternalComposedLts.g:2275:1: entryRuleLtlOrFormula returns [EObject current=null] : iv_ruleLtlOrFormula= ruleLtlOrFormula EOF ;
    public final EObject entryRuleLtlOrFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlOrFormula = null;


        try {
            // InternalComposedLts.g:2276:2: (iv_ruleLtlOrFormula= ruleLtlOrFormula EOF )
            // InternalComposedLts.g:2277:2: iv_ruleLtlOrFormula= ruleLtlOrFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlOrFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlOrFormula=ruleLtlOrFormula();

            state._fsp--;

             current =iv_ruleLtlOrFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlOrFormula"


    // $ANTLR start "ruleLtlOrFormula"
    // InternalComposedLts.g:2284:1: ruleLtlOrFormula returns [EObject current=null] : (this_LtlAndFormula_0= ruleLtlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) ) )* ) ;
    public final EObject ruleLtlOrFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_LtlAndFormula_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2287:28: ( (this_LtlAndFormula_0= ruleLtlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) ) )* ) )
            // InternalComposedLts.g:2288:1: (this_LtlAndFormula_0= ruleLtlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) ) )* )
            {
            // InternalComposedLts.g:2288:1: (this_LtlAndFormula_0= ruleLtlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) ) )* )
            // InternalComposedLts.g:2289:5: this_LtlAndFormula_0= ruleLtlAndFormula ( () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getLtlOrFormulaAccess().getLtlAndFormulaParserRuleCall_0()); 
                
            pushFollow(FOLLOW_10);
            this_LtlAndFormula_0=ruleLtlAndFormula();

            state._fsp--;

             
                    current = this_LtlAndFormula_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:2297:1: ( () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==17) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalComposedLts.g:2297:2: () otherlv_2= '|' ( (lv_right_3_0= ruleLtlAndFormula ) )
            	    {
            	    // InternalComposedLts.g:2297:2: ()
            	    // InternalComposedLts.g:2298:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getLtlOrFormulaAccess().getLtlOrFormulaLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    otherlv_2=(Token)match(input,17,FOLLOW_34); 

            	        	newLeafNode(otherlv_2, grammarAccess.getLtlOrFormulaAccess().getVerticalLineKeyword_1_1());
            	        
            	    // InternalComposedLts.g:2307:1: ( (lv_right_3_0= ruleLtlAndFormula ) )
            	    // InternalComposedLts.g:2308:1: (lv_right_3_0= ruleLtlAndFormula )
            	    {
            	    // InternalComposedLts.g:2308:1: (lv_right_3_0= ruleLtlAndFormula )
            	    // InternalComposedLts.g:2309:3: lv_right_3_0= ruleLtlAndFormula
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLtlOrFormulaAccess().getRightLtlAndFormulaParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_10);
            	    lv_right_3_0=ruleLtlAndFormula();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLtlOrFormulaRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.LtlAndFormula");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlOrFormula"


    // $ANTLR start "entryRuleLtlAndFormula"
    // InternalComposedLts.g:2333:1: entryRuleLtlAndFormula returns [EObject current=null] : iv_ruleLtlAndFormula= ruleLtlAndFormula EOF ;
    public final EObject entryRuleLtlAndFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlAndFormula = null;


        try {
            // InternalComposedLts.g:2334:2: (iv_ruleLtlAndFormula= ruleLtlAndFormula EOF )
            // InternalComposedLts.g:2335:2: iv_ruleLtlAndFormula= ruleLtlAndFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlAndFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlAndFormula=ruleLtlAndFormula();

            state._fsp--;

             current =iv_ruleLtlAndFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlAndFormula"


    // $ANTLR start "ruleLtlAndFormula"
    // InternalComposedLts.g:2342:1: ruleLtlAndFormula returns [EObject current=null] : (this_LtlUntilFormula_0= ruleLtlUntilFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) ) )* ) ;
    public final EObject ruleLtlAndFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_LtlUntilFormula_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2345:28: ( (this_LtlUntilFormula_0= ruleLtlUntilFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) ) )* ) )
            // InternalComposedLts.g:2346:1: (this_LtlUntilFormula_0= ruleLtlUntilFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) ) )* )
            {
            // InternalComposedLts.g:2346:1: (this_LtlUntilFormula_0= ruleLtlUntilFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) ) )* )
            // InternalComposedLts.g:2347:5: this_LtlUntilFormula_0= ruleLtlUntilFormula ( () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getLtlAndFormulaAccess().getLtlUntilFormulaParserRuleCall_0()); 
                
            pushFollow(FOLLOW_29);
            this_LtlUntilFormula_0=ruleLtlUntilFormula();

            state._fsp--;

             
                    current = this_LtlUntilFormula_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:2355:1: ( () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==36) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalComposedLts.g:2355:2: () otherlv_2= '&' ( (lv_right_3_0= ruleLtlUntilFormula ) )
            	    {
            	    // InternalComposedLts.g:2355:2: ()
            	    // InternalComposedLts.g:2356:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getLtlAndFormulaAccess().getLtlAndFormulaLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    otherlv_2=(Token)match(input,36,FOLLOW_34); 

            	        	newLeafNode(otherlv_2, grammarAccess.getLtlAndFormulaAccess().getAmpersandKeyword_1_1());
            	        
            	    // InternalComposedLts.g:2365:1: ( (lv_right_3_0= ruleLtlUntilFormula ) )
            	    // InternalComposedLts.g:2366:1: (lv_right_3_0= ruleLtlUntilFormula )
            	    {
            	    // InternalComposedLts.g:2366:1: (lv_right_3_0= ruleLtlUntilFormula )
            	    // InternalComposedLts.g:2367:3: lv_right_3_0= ruleLtlUntilFormula
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLtlAndFormulaAccess().getRightLtlUntilFormulaParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_29);
            	    lv_right_3_0=ruleLtlUntilFormula();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLtlAndFormulaRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.LtlUntilFormula");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlAndFormula"


    // $ANTLR start "entryRuleLtlUntilFormula"
    // InternalComposedLts.g:2391:1: entryRuleLtlUntilFormula returns [EObject current=null] : iv_ruleLtlUntilFormula= ruleLtlUntilFormula EOF ;
    public final EObject entryRuleLtlUntilFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlUntilFormula = null;


        try {
            // InternalComposedLts.g:2392:2: (iv_ruleLtlUntilFormula= ruleLtlUntilFormula EOF )
            // InternalComposedLts.g:2393:2: iv_ruleLtlUntilFormula= ruleLtlUntilFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlUntilFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlUntilFormula=ruleLtlUntilFormula();

            state._fsp--;

             current =iv_ruleLtlUntilFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlUntilFormula"


    // $ANTLR start "ruleLtlUntilFormula"
    // InternalComposedLts.g:2400:1: ruleLtlUntilFormula returns [EObject current=null] : (this_LtlBaseFormula_0= ruleLtlBaseFormula ( () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) ) )* ) ;
    public final EObject ruleLtlUntilFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_LtlBaseFormula_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2403:28: ( (this_LtlBaseFormula_0= ruleLtlBaseFormula ( () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) ) )* ) )
            // InternalComposedLts.g:2404:1: (this_LtlBaseFormula_0= ruleLtlBaseFormula ( () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) ) )* )
            {
            // InternalComposedLts.g:2404:1: (this_LtlBaseFormula_0= ruleLtlBaseFormula ( () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) ) )* )
            // InternalComposedLts.g:2405:5: this_LtlBaseFormula_0= ruleLtlBaseFormula ( () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getLtlUntilFormulaAccess().getLtlBaseFormulaParserRuleCall_0()); 
                
            pushFollow(FOLLOW_35);
            this_LtlBaseFormula_0=ruleLtlBaseFormula();

            state._fsp--;

             
                    current = this_LtlBaseFormula_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:2413:1: ( () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==49) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalComposedLts.g:2413:2: () otherlv_2= '\\\\U' ( (lv_right_3_0= ruleLtlBaseFormula ) )
            	    {
            	    // InternalComposedLts.g:2413:2: ()
            	    // InternalComposedLts.g:2414:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getLtlUntilFormulaAccess().getLtlUntilFormulaLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    otherlv_2=(Token)match(input,49,FOLLOW_34); 

            	        	newLeafNode(otherlv_2, grammarAccess.getLtlUntilFormulaAccess().getUKeyword_1_1());
            	        
            	    // InternalComposedLts.g:2423:1: ( (lv_right_3_0= ruleLtlBaseFormula ) )
            	    // InternalComposedLts.g:2424:1: (lv_right_3_0= ruleLtlBaseFormula )
            	    {
            	    // InternalComposedLts.g:2424:1: (lv_right_3_0= ruleLtlBaseFormula )
            	    // InternalComposedLts.g:2425:3: lv_right_3_0= ruleLtlBaseFormula
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLtlUntilFormulaAccess().getRightLtlBaseFormulaParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_35);
            	    lv_right_3_0=ruleLtlBaseFormula();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLtlUntilFormulaRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_3_0, 
            	            		"org.cmg.tapas.xtext.clts.ComposedLts.LtlBaseFormula");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlUntilFormula"


    // $ANTLR start "entryRuleLtlBaseFormula"
    // InternalComposedLts.g:2449:1: entryRuleLtlBaseFormula returns [EObject current=null] : iv_ruleLtlBaseFormula= ruleLtlBaseFormula EOF ;
    public final EObject entryRuleLtlBaseFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlBaseFormula = null;


        try {
            // InternalComposedLts.g:2450:2: (iv_ruleLtlBaseFormula= ruleLtlBaseFormula EOF )
            // InternalComposedLts.g:2451:2: iv_ruleLtlBaseFormula= ruleLtlBaseFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlBaseFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlBaseFormula=ruleLtlBaseFormula();

            state._fsp--;

             current =iv_ruleLtlBaseFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlBaseFormula"


    // $ANTLR start "ruleLtlBaseFormula"
    // InternalComposedLts.g:2458:1: ruleLtlBaseFormula returns [EObject current=null] : (this_LtlTrue_0= ruleLtlTrue | this_LtlFalse_1= ruleLtlFalse | (otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')' ) | this_LtlNotFormula_5= ruleLtlNotFormula | this_LtlNextFormula_6= ruleLtlNextFormula | this_LtlAlwaysFormula_7= ruleLtlAlwaysFormula | this_LtlEventuallyFormula_8= ruleLtlEventuallyFormula | this_LtlAtomicProposition_9= ruleLtlAtomicProposition ) ;
    public final EObject ruleLtlBaseFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject this_LtlTrue_0 = null;

        EObject this_LtlFalse_1 = null;

        EObject this_LtlFormula_3 = null;

        EObject this_LtlNotFormula_5 = null;

        EObject this_LtlNextFormula_6 = null;

        EObject this_LtlAlwaysFormula_7 = null;

        EObject this_LtlEventuallyFormula_8 = null;

        EObject this_LtlAtomicProposition_9 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2461:28: ( (this_LtlTrue_0= ruleLtlTrue | this_LtlFalse_1= ruleLtlFalse | (otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')' ) | this_LtlNotFormula_5= ruleLtlNotFormula | this_LtlNextFormula_6= ruleLtlNextFormula | this_LtlAlwaysFormula_7= ruleLtlAlwaysFormula | this_LtlEventuallyFormula_8= ruleLtlEventuallyFormula | this_LtlAtomicProposition_9= ruleLtlAtomicProposition ) )
            // InternalComposedLts.g:2462:1: (this_LtlTrue_0= ruleLtlTrue | this_LtlFalse_1= ruleLtlFalse | (otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')' ) | this_LtlNotFormula_5= ruleLtlNotFormula | this_LtlNextFormula_6= ruleLtlNextFormula | this_LtlAlwaysFormula_7= ruleLtlAlwaysFormula | this_LtlEventuallyFormula_8= ruleLtlEventuallyFormula | this_LtlAtomicProposition_9= ruleLtlAtomicProposition )
            {
            // InternalComposedLts.g:2462:1: (this_LtlTrue_0= ruleLtlTrue | this_LtlFalse_1= ruleLtlFalse | (otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')' ) | this_LtlNotFormula_5= ruleLtlNotFormula | this_LtlNextFormula_6= ruleLtlNextFormula | this_LtlAlwaysFormula_7= ruleLtlAlwaysFormula | this_LtlEventuallyFormula_8= ruleLtlEventuallyFormula | this_LtlAtomicProposition_9= ruleLtlAtomicProposition )
            int alt26=8;
            switch ( input.LA(1) ) {
            case 39:
                {
                alt26=1;
                }
                break;
            case 40:
                {
                alt26=2;
                }
                break;
            case 21:
                {
                alt26=3;
                }
                break;
            case 41:
                {
                alt26=4;
                }
                break;
            case 50:
                {
                alt26=5;
                }
                break;
            case 51:
                {
                alt26=6;
                }
                break;
            case 52:
                {
                alt26=7;
                }
                break;
            case RULE_ID:
                {
                alt26=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalComposedLts.g:2463:5: this_LtlTrue_0= ruleLtlTrue
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlTrueParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlTrue_0=ruleLtlTrue();

                    state._fsp--;

                     
                            current = this_LtlTrue_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:2473:5: this_LtlFalse_1= ruleLtlFalse
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlFalseParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlFalse_1=ruleLtlFalse();

                    state._fsp--;

                     
                            current = this_LtlFalse_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:2482:6: (otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')' )
                    {
                    // InternalComposedLts.g:2482:6: (otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')' )
                    // InternalComposedLts.g:2482:8: otherlv_2= '(' this_LtlFormula_3= ruleLtlFormula otherlv_4= ')'
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_34); 

                        	newLeafNode(otherlv_2, grammarAccess.getLtlBaseFormulaAccess().getLeftParenthesisKeyword_2_0());
                        
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlFormulaParserRuleCall_2_1()); 
                        
                    pushFollow(FOLLOW_14);
                    this_LtlFormula_3=ruleLtlFormula();

                    state._fsp--;

                     
                            current = this_LtlFormula_3; 
                            afterParserOrEnumRuleCall();
                        
                    otherlv_4=(Token)match(input,22,FOLLOW_2); 

                        	newLeafNode(otherlv_4, grammarAccess.getLtlBaseFormulaAccess().getRightParenthesisKeyword_2_2());
                        

                    }


                    }
                    break;
                case 4 :
                    // InternalComposedLts.g:2501:5: this_LtlNotFormula_5= ruleLtlNotFormula
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlNotFormulaParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlNotFormula_5=ruleLtlNotFormula();

                    state._fsp--;

                     
                            current = this_LtlNotFormula_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // InternalComposedLts.g:2511:5: this_LtlNextFormula_6= ruleLtlNextFormula
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlNextFormulaParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlNextFormula_6=ruleLtlNextFormula();

                    state._fsp--;

                     
                            current = this_LtlNextFormula_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // InternalComposedLts.g:2521:5: this_LtlAlwaysFormula_7= ruleLtlAlwaysFormula
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlAlwaysFormulaParserRuleCall_5()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlAlwaysFormula_7=ruleLtlAlwaysFormula();

                    state._fsp--;

                     
                            current = this_LtlAlwaysFormula_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // InternalComposedLts.g:2531:5: this_LtlEventuallyFormula_8= ruleLtlEventuallyFormula
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlEventuallyFormulaParserRuleCall_6()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlEventuallyFormula_8=ruleLtlEventuallyFormula();

                    state._fsp--;

                     
                            current = this_LtlEventuallyFormula_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // InternalComposedLts.g:2541:5: this_LtlAtomicProposition_9= ruleLtlAtomicProposition
                    {
                     
                            newCompositeNode(grammarAccess.getLtlBaseFormulaAccess().getLtlAtomicPropositionParserRuleCall_7()); 
                        
                    pushFollow(FOLLOW_2);
                    this_LtlAtomicProposition_9=ruleLtlAtomicProposition();

                    state._fsp--;

                     
                            current = this_LtlAtomicProposition_9; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlBaseFormula"


    // $ANTLR start "entryRuleLtlAtomicProposition"
    // InternalComposedLts.g:2557:1: entryRuleLtlAtomicProposition returns [EObject current=null] : iv_ruleLtlAtomicProposition= ruleLtlAtomicProposition EOF ;
    public final EObject entryRuleLtlAtomicProposition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlAtomicProposition = null;


        try {
            // InternalComposedLts.g:2558:2: (iv_ruleLtlAtomicProposition= ruleLtlAtomicProposition EOF )
            // InternalComposedLts.g:2559:2: iv_ruleLtlAtomicProposition= ruleLtlAtomicProposition EOF
            {
             newCompositeNode(grammarAccess.getLtlAtomicPropositionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlAtomicProposition=ruleLtlAtomicProposition();

            state._fsp--;

             current =iv_ruleLtlAtomicProposition; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlAtomicProposition"


    // $ANTLR start "ruleLtlAtomicProposition"
    // InternalComposedLts.g:2566:1: ruleLtlAtomicProposition returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleLtlAtomicProposition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:2569:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalComposedLts.g:2570:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalComposedLts.g:2570:1: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:2571:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:2571:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:2572:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getLtlAtomicPropositionRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_0, grammarAccess.getLtlAtomicPropositionAccess().getPropLabelCrossReference_0()); 
            	

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlAtomicProposition"


    // $ANTLR start "entryRuleLtlNextFormula"
    // InternalComposedLts.g:2591:1: entryRuleLtlNextFormula returns [EObject current=null] : iv_ruleLtlNextFormula= ruleLtlNextFormula EOF ;
    public final EObject entryRuleLtlNextFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlNextFormula = null;


        try {
            // InternalComposedLts.g:2592:2: (iv_ruleLtlNextFormula= ruleLtlNextFormula EOF )
            // InternalComposedLts.g:2593:2: iv_ruleLtlNextFormula= ruleLtlNextFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlNextFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlNextFormula=ruleLtlNextFormula();

            state._fsp--;

             current =iv_ruleLtlNextFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlNextFormula"


    // $ANTLR start "ruleLtlNextFormula"
    // InternalComposedLts.g:2600:1: ruleLtlNextFormula returns [EObject current=null] : (otherlv_0= '\\\\X' otherlv_1= '{' ( (lv_arg_2_0= ruleLtlBaseFormula ) ) otherlv_3= '}' ) ;
    public final EObject ruleLtlNextFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_arg_2_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2603:28: ( (otherlv_0= '\\\\X' otherlv_1= '{' ( (lv_arg_2_0= ruleLtlBaseFormula ) ) otherlv_3= '}' ) )
            // InternalComposedLts.g:2604:1: (otherlv_0= '\\\\X' otherlv_1= '{' ( (lv_arg_2_0= ruleLtlBaseFormula ) ) otherlv_3= '}' )
            {
            // InternalComposedLts.g:2604:1: (otherlv_0= '\\\\X' otherlv_1= '{' ( (lv_arg_2_0= ruleLtlBaseFormula ) ) otherlv_3= '}' )
            // InternalComposedLts.g:2604:3: otherlv_0= '\\\\X' otherlv_1= '{' ( (lv_arg_2_0= ruleLtlBaseFormula ) ) otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,50,FOLLOW_36); 

                	newLeafNode(otherlv_0, grammarAccess.getLtlNextFormulaAccess().getXKeyword_0());
                
            otherlv_1=(Token)match(input,19,FOLLOW_34); 

                	newLeafNode(otherlv_1, grammarAccess.getLtlNextFormulaAccess().getLeftCurlyBracketKeyword_1());
                
            // InternalComposedLts.g:2612:1: ( (lv_arg_2_0= ruleLtlBaseFormula ) )
            // InternalComposedLts.g:2613:1: (lv_arg_2_0= ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:2613:1: (lv_arg_2_0= ruleLtlBaseFormula )
            // InternalComposedLts.g:2614:3: lv_arg_2_0= ruleLtlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getLtlNextFormulaAccess().getArgLtlBaseFormulaParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_24);
            lv_arg_2_0=ruleLtlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtlNextFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_2_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,20,FOLLOW_2); 

                	newLeafNode(otherlv_3, grammarAccess.getLtlNextFormulaAccess().getRightCurlyBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlNextFormula"


    // $ANTLR start "entryRuleLtlAlwaysFormula"
    // InternalComposedLts.g:2642:1: entryRuleLtlAlwaysFormula returns [EObject current=null] : iv_ruleLtlAlwaysFormula= ruleLtlAlwaysFormula EOF ;
    public final EObject entryRuleLtlAlwaysFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlAlwaysFormula = null;


        try {
            // InternalComposedLts.g:2643:2: (iv_ruleLtlAlwaysFormula= ruleLtlAlwaysFormula EOF )
            // InternalComposedLts.g:2644:2: iv_ruleLtlAlwaysFormula= ruleLtlAlwaysFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlAlwaysFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlAlwaysFormula=ruleLtlAlwaysFormula();

            state._fsp--;

             current =iv_ruleLtlAlwaysFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlAlwaysFormula"


    // $ANTLR start "ruleLtlAlwaysFormula"
    // InternalComposedLts.g:2651:1: ruleLtlAlwaysFormula returns [EObject current=null] : (otherlv_0= '\\\\G' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) ) ;
    public final EObject ruleLtlAlwaysFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2654:28: ( (otherlv_0= '\\\\G' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) ) )
            // InternalComposedLts.g:2655:1: (otherlv_0= '\\\\G' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) )
            {
            // InternalComposedLts.g:2655:1: (otherlv_0= '\\\\G' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) )
            // InternalComposedLts.g:2655:3: otherlv_0= '\\\\G' ( (lv_arg_1_0= ruleLtlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,51,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getLtlAlwaysFormulaAccess().getGKeyword_0());
                
            // InternalComposedLts.g:2659:1: ( (lv_arg_1_0= ruleLtlBaseFormula ) )
            // InternalComposedLts.g:2660:1: (lv_arg_1_0= ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:2660:1: (lv_arg_1_0= ruleLtlBaseFormula )
            // InternalComposedLts.g:2661:3: lv_arg_1_0= ruleLtlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getLtlAlwaysFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_1_0=ruleLtlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtlAlwaysFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlAlwaysFormula"


    // $ANTLR start "entryRuleLtlEventuallyFormula"
    // InternalComposedLts.g:2685:1: entryRuleLtlEventuallyFormula returns [EObject current=null] : iv_ruleLtlEventuallyFormula= ruleLtlEventuallyFormula EOF ;
    public final EObject entryRuleLtlEventuallyFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlEventuallyFormula = null;


        try {
            // InternalComposedLts.g:2686:2: (iv_ruleLtlEventuallyFormula= ruleLtlEventuallyFormula EOF )
            // InternalComposedLts.g:2687:2: iv_ruleLtlEventuallyFormula= ruleLtlEventuallyFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlEventuallyFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlEventuallyFormula=ruleLtlEventuallyFormula();

            state._fsp--;

             current =iv_ruleLtlEventuallyFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlEventuallyFormula"


    // $ANTLR start "ruleLtlEventuallyFormula"
    // InternalComposedLts.g:2694:1: ruleLtlEventuallyFormula returns [EObject current=null] : (otherlv_0= '\\\\F' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) ) ;
    public final EObject ruleLtlEventuallyFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2697:28: ( (otherlv_0= '\\\\F' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) ) )
            // InternalComposedLts.g:2698:1: (otherlv_0= '\\\\F' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) )
            {
            // InternalComposedLts.g:2698:1: (otherlv_0= '\\\\F' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) )
            // InternalComposedLts.g:2698:3: otherlv_0= '\\\\F' ( (lv_arg_1_0= ruleLtlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,52,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getLtlEventuallyFormulaAccess().getFKeyword_0());
                
            // InternalComposedLts.g:2702:1: ( (lv_arg_1_0= ruleLtlBaseFormula ) )
            // InternalComposedLts.g:2703:1: (lv_arg_1_0= ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:2703:1: (lv_arg_1_0= ruleLtlBaseFormula )
            // InternalComposedLts.g:2704:3: lv_arg_1_0= ruleLtlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getLtlEventuallyFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_1_0=ruleLtlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtlEventuallyFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlEventuallyFormula"


    // $ANTLR start "entryRuleLtlNotFormula"
    // InternalComposedLts.g:2728:1: entryRuleLtlNotFormula returns [EObject current=null] : iv_ruleLtlNotFormula= ruleLtlNotFormula EOF ;
    public final EObject entryRuleLtlNotFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlNotFormula = null;


        try {
            // InternalComposedLts.g:2729:2: (iv_ruleLtlNotFormula= ruleLtlNotFormula EOF )
            // InternalComposedLts.g:2730:2: iv_ruleLtlNotFormula= ruleLtlNotFormula EOF
            {
             newCompositeNode(grammarAccess.getLtlNotFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlNotFormula=ruleLtlNotFormula();

            state._fsp--;

             current =iv_ruleLtlNotFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlNotFormula"


    // $ANTLR start "ruleLtlNotFormula"
    // InternalComposedLts.g:2737:1: ruleLtlNotFormula returns [EObject current=null] : (otherlv_0= '!' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) ) ;
    public final EObject ruleLtlNotFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2740:28: ( (otherlv_0= '!' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) ) )
            // InternalComposedLts.g:2741:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) )
            {
            // InternalComposedLts.g:2741:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleLtlBaseFormula ) ) )
            // InternalComposedLts.g:2741:3: otherlv_0= '!' ( (lv_arg_1_0= ruleLtlBaseFormula ) )
            {
            otherlv_0=(Token)match(input,41,FOLLOW_34); 

                	newLeafNode(otherlv_0, grammarAccess.getLtlNotFormulaAccess().getExclamationMarkKeyword_0());
                
            // InternalComposedLts.g:2745:1: ( (lv_arg_1_0= ruleLtlBaseFormula ) )
            // InternalComposedLts.g:2746:1: (lv_arg_1_0= ruleLtlBaseFormula )
            {
            // InternalComposedLts.g:2746:1: (lv_arg_1_0= ruleLtlBaseFormula )
            // InternalComposedLts.g:2747:3: lv_arg_1_0= ruleLtlBaseFormula
            {
             
            	        newCompositeNode(grammarAccess.getLtlNotFormulaAccess().getArgLtlBaseFormulaParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_1_0=ruleLtlBaseFormula();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLtlNotFormulaRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.LtlBaseFormula");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlNotFormula"


    // $ANTLR start "entryRuleLtlTrue"
    // InternalComposedLts.g:2771:1: entryRuleLtlTrue returns [EObject current=null] : iv_ruleLtlTrue= ruleLtlTrue EOF ;
    public final EObject entryRuleLtlTrue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlTrue = null;


        try {
            // InternalComposedLts.g:2772:2: (iv_ruleLtlTrue= ruleLtlTrue EOF )
            // InternalComposedLts.g:2773:2: iv_ruleLtlTrue= ruleLtlTrue EOF
            {
             newCompositeNode(grammarAccess.getLtlTrueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlTrue=ruleLtlTrue();

            state._fsp--;

             current =iv_ruleLtlTrue; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlTrue"


    // $ANTLR start "ruleLtlTrue"
    // InternalComposedLts.g:2780:1: ruleLtlTrue returns [EObject current=null] : ( () otherlv_1= 'true' ) ;
    public final EObject ruleLtlTrue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:2783:28: ( ( () otherlv_1= 'true' ) )
            // InternalComposedLts.g:2784:1: ( () otherlv_1= 'true' )
            {
            // InternalComposedLts.g:2784:1: ( () otherlv_1= 'true' )
            // InternalComposedLts.g:2784:2: () otherlv_1= 'true'
            {
            // InternalComposedLts.g:2784:2: ()
            // InternalComposedLts.g:2785:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getLtlTrueAccess().getLtlTrueAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,39,FOLLOW_2); 

                	newLeafNode(otherlv_1, grammarAccess.getLtlTrueAccess().getTrueKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlTrue"


    // $ANTLR start "entryRuleLtlFalse"
    // InternalComposedLts.g:2802:1: entryRuleLtlFalse returns [EObject current=null] : iv_ruleLtlFalse= ruleLtlFalse EOF ;
    public final EObject entryRuleLtlFalse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLtlFalse = null;


        try {
            // InternalComposedLts.g:2803:2: (iv_ruleLtlFalse= ruleLtlFalse EOF )
            // InternalComposedLts.g:2804:2: iv_ruleLtlFalse= ruleLtlFalse EOF
            {
             newCompositeNode(grammarAccess.getLtlFalseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLtlFalse=ruleLtlFalse();

            state._fsp--;

             current =iv_ruleLtlFalse; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLtlFalse"


    // $ANTLR start "ruleLtlFalse"
    // InternalComposedLts.g:2811:1: ruleLtlFalse returns [EObject current=null] : ( () otherlv_1= 'false' ) ;
    public final EObject ruleLtlFalse() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:2814:28: ( ( () otherlv_1= 'false' ) )
            // InternalComposedLts.g:2815:1: ( () otherlv_1= 'false' )
            {
            // InternalComposedLts.g:2815:1: ( () otherlv_1= 'false' )
            // InternalComposedLts.g:2815:2: () otherlv_1= 'false'
            {
            // InternalComposedLts.g:2815:2: ()
            // InternalComposedLts.g:2816:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getLtlFalseAccess().getLtlFalseAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,40,FOLLOW_2); 

                	newLeafNode(otherlv_1, grammarAccess.getLtlFalseAccess().getFalseKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLtlFalse"


    // $ANTLR start "entryRuleLabelPredicate"
    // InternalComposedLts.g:2833:1: entryRuleLabelPredicate returns [EObject current=null] : iv_ruleLabelPredicate= ruleLabelPredicate EOF ;
    public final EObject entryRuleLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLabelPredicate = null;


        try {
            // InternalComposedLts.g:2834:2: (iv_ruleLabelPredicate= ruleLabelPredicate EOF )
            // InternalComposedLts.g:2835:2: iv_ruleLabelPredicate= ruleLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLabelPredicate=ruleLabelPredicate();

            state._fsp--;

             current =iv_ruleLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLabelPredicate"


    // $ANTLR start "ruleLabelPredicate"
    // InternalComposedLts.g:2842:1: ruleLabelPredicate returns [EObject current=null] : this_CupLabelPredicate_0= ruleCupLabelPredicate ;
    public final EObject ruleLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject this_CupLabelPredicate_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2845:28: (this_CupLabelPredicate_0= ruleCupLabelPredicate )
            // InternalComposedLts.g:2847:5: this_CupLabelPredicate_0= ruleCupLabelPredicate
            {
             
                    newCompositeNode(grammarAccess.getLabelPredicateAccess().getCupLabelPredicateParserRuleCall()); 
                
            pushFollow(FOLLOW_2);
            this_CupLabelPredicate_0=ruleCupLabelPredicate();

            state._fsp--;

             
                    current = this_CupLabelPredicate_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLabelPredicate"


    // $ANTLR start "entryRuleCupLabelPredicate"
    // InternalComposedLts.g:2863:1: entryRuleCupLabelPredicate returns [EObject current=null] : iv_ruleCupLabelPredicate= ruleCupLabelPredicate EOF ;
    public final EObject entryRuleCupLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCupLabelPredicate = null;


        try {
            // InternalComposedLts.g:2864:2: (iv_ruleCupLabelPredicate= ruleCupLabelPredicate EOF )
            // InternalComposedLts.g:2865:2: iv_ruleCupLabelPredicate= ruleCupLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getCupLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCupLabelPredicate=ruleCupLabelPredicate();

            state._fsp--;

             current =iv_ruleCupLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCupLabelPredicate"


    // $ANTLR start "ruleCupLabelPredicate"
    // InternalComposedLts.g:2872:1: ruleCupLabelPredicate returns [EObject current=null] : (this_CapLabelPredicate_0= ruleCapLabelPredicate ( () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) ) )? ) ;
    public final EObject ruleCupLabelPredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_CapLabelPredicate_0 = null;

        EObject lv_righ_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2875:28: ( (this_CapLabelPredicate_0= ruleCapLabelPredicate ( () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) ) )? ) )
            // InternalComposedLts.g:2876:1: (this_CapLabelPredicate_0= ruleCapLabelPredicate ( () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) ) )? )
            {
            // InternalComposedLts.g:2876:1: (this_CapLabelPredicate_0= ruleCapLabelPredicate ( () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) ) )? )
            // InternalComposedLts.g:2877:5: this_CapLabelPredicate_0= ruleCapLabelPredicate ( () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getCupLabelPredicateAccess().getCapLabelPredicateParserRuleCall_0()); 
                
            pushFollow(FOLLOW_10);
            this_CapLabelPredicate_0=ruleCapLabelPredicate();

            state._fsp--;

             
                    current = this_CapLabelPredicate_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:2885:1: ( () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==17) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalComposedLts.g:2885:2: () otherlv_2= '|' ( (lv_righ_3_0= ruleCupLabelPredicate ) )
                    {
                    // InternalComposedLts.g:2885:2: ()
                    // InternalComposedLts.g:2886:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getCupLabelPredicateAccess().getCupLabelPredicateLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,17,FOLLOW_31); 

                        	newLeafNode(otherlv_2, grammarAccess.getCupLabelPredicateAccess().getVerticalLineKeyword_1_1());
                        
                    // InternalComposedLts.g:2895:1: ( (lv_righ_3_0= ruleCupLabelPredicate ) )
                    // InternalComposedLts.g:2896:1: (lv_righ_3_0= ruleCupLabelPredicate )
                    {
                    // InternalComposedLts.g:2896:1: (lv_righ_3_0= ruleCupLabelPredicate )
                    // InternalComposedLts.g:2897:3: lv_righ_3_0= ruleCupLabelPredicate
                    {
                     
                    	        newCompositeNode(grammarAccess.getCupLabelPredicateAccess().getRighCupLabelPredicateParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_righ_3_0=ruleCupLabelPredicate();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCupLabelPredicateRule());
                    	        }
                           		set(
                           			current, 
                           			"righ",
                            		lv_righ_3_0, 
                            		"org.cmg.tapas.xtext.clts.ComposedLts.CupLabelPredicate");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCupLabelPredicate"


    // $ANTLR start "entryRuleCapLabelPredicate"
    // InternalComposedLts.g:2921:1: entryRuleCapLabelPredicate returns [EObject current=null] : iv_ruleCapLabelPredicate= ruleCapLabelPredicate EOF ;
    public final EObject entryRuleCapLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCapLabelPredicate = null;


        try {
            // InternalComposedLts.g:2922:2: (iv_ruleCapLabelPredicate= ruleCapLabelPredicate EOF )
            // InternalComposedLts.g:2923:2: iv_ruleCapLabelPredicate= ruleCapLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getCapLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCapLabelPredicate=ruleCapLabelPredicate();

            state._fsp--;

             current =iv_ruleCapLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCapLabelPredicate"


    // $ANTLR start "ruleCapLabelPredicate"
    // InternalComposedLts.g:2930:1: ruleCapLabelPredicate returns [EObject current=null] : (this_BaseLabelPredicate_0= ruleBaseLabelPredicate ( () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) ) )? ) ;
    public final EObject ruleCapLabelPredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_BaseLabelPredicate_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2933:28: ( (this_BaseLabelPredicate_0= ruleBaseLabelPredicate ( () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) ) )? ) )
            // InternalComposedLts.g:2934:1: (this_BaseLabelPredicate_0= ruleBaseLabelPredicate ( () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) ) )? )
            {
            // InternalComposedLts.g:2934:1: (this_BaseLabelPredicate_0= ruleBaseLabelPredicate ( () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) ) )? )
            // InternalComposedLts.g:2935:5: this_BaseLabelPredicate_0= ruleBaseLabelPredicate ( () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) ) )?
            {
             
                    newCompositeNode(grammarAccess.getCapLabelPredicateAccess().getBaseLabelPredicateParserRuleCall_0()); 
                
            pushFollow(FOLLOW_29);
            this_BaseLabelPredicate_0=ruleBaseLabelPredicate();

            state._fsp--;

             
                    current = this_BaseLabelPredicate_0; 
                    afterParserOrEnumRuleCall();
                
            // InternalComposedLts.g:2943:1: ( () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==36) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalComposedLts.g:2943:2: () otherlv_2= '&' ( (lv_right_3_0= ruleCapLabelPredicate ) )
                    {
                    // InternalComposedLts.g:2943:2: ()
                    // InternalComposedLts.g:2944:5: 
                    {

                            current = forceCreateModelElementAndSet(
                                grammarAccess.getCapLabelPredicateAccess().getCapLabelPredicateLeftAction_1_0(),
                                current);
                        

                    }

                    otherlv_2=(Token)match(input,36,FOLLOW_31); 

                        	newLeafNode(otherlv_2, grammarAccess.getCapLabelPredicateAccess().getAmpersandKeyword_1_1());
                        
                    // InternalComposedLts.g:2953:1: ( (lv_right_3_0= ruleCapLabelPredicate ) )
                    // InternalComposedLts.g:2954:1: (lv_right_3_0= ruleCapLabelPredicate )
                    {
                    // InternalComposedLts.g:2954:1: (lv_right_3_0= ruleCapLabelPredicate )
                    // InternalComposedLts.g:2955:3: lv_right_3_0= ruleCapLabelPredicate
                    {
                     
                    	        newCompositeNode(grammarAccess.getCapLabelPredicateAccess().getRightCapLabelPredicateParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FOLLOW_2);
                    lv_right_3_0=ruleCapLabelPredicate();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getCapLabelPredicateRule());
                    	        }
                           		set(
                           			current, 
                           			"right",
                            		lv_right_3_0, 
                            		"org.cmg.tapas.xtext.clts.ComposedLts.CapLabelPredicate");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCapLabelPredicate"


    // $ANTLR start "entryRuleBaseLabelPredicate"
    // InternalComposedLts.g:2979:1: entryRuleBaseLabelPredicate returns [EObject current=null] : iv_ruleBaseLabelPredicate= ruleBaseLabelPredicate EOF ;
    public final EObject entryRuleBaseLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseLabelPredicate = null;


        try {
            // InternalComposedLts.g:2980:2: (iv_ruleBaseLabelPredicate= ruleBaseLabelPredicate EOF )
            // InternalComposedLts.g:2981:2: iv_ruleBaseLabelPredicate= ruleBaseLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getBaseLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBaseLabelPredicate=ruleBaseLabelPredicate();

            state._fsp--;

             current =iv_ruleBaseLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseLabelPredicate"


    // $ANTLR start "ruleBaseLabelPredicate"
    // InternalComposedLts.g:2988:1: ruleBaseLabelPredicate returns [EObject current=null] : (this_AnyLabelPredicate_0= ruleAnyLabelPredicate | this_ActionLabelPredicate_1= ruleActionLabelPredicate | this_NotLabelPredicate_2= ruleNotLabelPredicate ) ;
    public final EObject ruleBaseLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject this_AnyLabelPredicate_0 = null;

        EObject this_ActionLabelPredicate_1 = null;

        EObject this_NotLabelPredicate_2 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:2991:28: ( (this_AnyLabelPredicate_0= ruleAnyLabelPredicate | this_ActionLabelPredicate_1= ruleActionLabelPredicate | this_NotLabelPredicate_2= ruleNotLabelPredicate ) )
            // InternalComposedLts.g:2992:1: (this_AnyLabelPredicate_0= ruleAnyLabelPredicate | this_ActionLabelPredicate_1= ruleActionLabelPredicate | this_NotLabelPredicate_2= ruleNotLabelPredicate )
            {
            // InternalComposedLts.g:2992:1: (this_AnyLabelPredicate_0= ruleAnyLabelPredicate | this_ActionLabelPredicate_1= ruleActionLabelPredicate | this_NotLabelPredicate_2= ruleNotLabelPredicate )
            int alt29=3;
            switch ( input.LA(1) ) {
            case 53:
                {
                alt29=1;
                }
                break;
            case RULE_ID:
                {
                alt29=2;
                }
                break;
            case 41:
                {
                alt29=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalComposedLts.g:2993:5: this_AnyLabelPredicate_0= ruleAnyLabelPredicate
                    {
                     
                            newCompositeNode(grammarAccess.getBaseLabelPredicateAccess().getAnyLabelPredicateParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_2);
                    this_AnyLabelPredicate_0=ruleAnyLabelPredicate();

                    state._fsp--;

                     
                            current = this_AnyLabelPredicate_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // InternalComposedLts.g:3003:5: this_ActionLabelPredicate_1= ruleActionLabelPredicate
                    {
                     
                            newCompositeNode(grammarAccess.getBaseLabelPredicateAccess().getActionLabelPredicateParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_2);
                    this_ActionLabelPredicate_1=ruleActionLabelPredicate();

                    state._fsp--;

                     
                            current = this_ActionLabelPredicate_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // InternalComposedLts.g:3013:5: this_NotLabelPredicate_2= ruleNotLabelPredicate
                    {
                     
                            newCompositeNode(grammarAccess.getBaseLabelPredicateAccess().getNotLabelPredicateParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_2);
                    this_NotLabelPredicate_2=ruleNotLabelPredicate();

                    state._fsp--;

                     
                            current = this_NotLabelPredicate_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseLabelPredicate"


    // $ANTLR start "entryRuleNotLabelPredicate"
    // InternalComposedLts.g:3029:1: entryRuleNotLabelPredicate returns [EObject current=null] : iv_ruleNotLabelPredicate= ruleNotLabelPredicate EOF ;
    public final EObject entryRuleNotLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotLabelPredicate = null;


        try {
            // InternalComposedLts.g:3030:2: (iv_ruleNotLabelPredicate= ruleNotLabelPredicate EOF )
            // InternalComposedLts.g:3031:2: iv_ruleNotLabelPredicate= ruleNotLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getNotLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotLabelPredicate=ruleNotLabelPredicate();

            state._fsp--;

             current =iv_ruleNotLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotLabelPredicate"


    // $ANTLR start "ruleNotLabelPredicate"
    // InternalComposedLts.g:3038:1: ruleNotLabelPredicate returns [EObject current=null] : (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseLabelPredicate ) ) ) ;
    public final EObject ruleNotLabelPredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // InternalComposedLts.g:3041:28: ( (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseLabelPredicate ) ) ) )
            // InternalComposedLts.g:3042:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseLabelPredicate ) ) )
            {
            // InternalComposedLts.g:3042:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseLabelPredicate ) ) )
            // InternalComposedLts.g:3042:3: otherlv_0= '!' ( (lv_arg_1_0= ruleBaseLabelPredicate ) )
            {
            otherlv_0=(Token)match(input,41,FOLLOW_31); 

                	newLeafNode(otherlv_0, grammarAccess.getNotLabelPredicateAccess().getExclamationMarkKeyword_0());
                
            // InternalComposedLts.g:3046:1: ( (lv_arg_1_0= ruleBaseLabelPredicate ) )
            // InternalComposedLts.g:3047:1: (lv_arg_1_0= ruleBaseLabelPredicate )
            {
            // InternalComposedLts.g:3047:1: (lv_arg_1_0= ruleBaseLabelPredicate )
            // InternalComposedLts.g:3048:3: lv_arg_1_0= ruleBaseLabelPredicate
            {
             
            	        newCompositeNode(grammarAccess.getNotLabelPredicateAccess().getArgBaseLabelPredicateParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_2);
            lv_arg_1_0=ruleBaseLabelPredicate();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNotLabelPredicateRule());
            	        }
                   		set(
                   			current, 
                   			"arg",
                    		lv_arg_1_0, 
                    		"org.cmg.tapas.xtext.clts.ComposedLts.BaseLabelPredicate");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotLabelPredicate"


    // $ANTLR start "entryRuleActionLabelPredicate"
    // InternalComposedLts.g:3072:1: entryRuleActionLabelPredicate returns [EObject current=null] : iv_ruleActionLabelPredicate= ruleActionLabelPredicate EOF ;
    public final EObject entryRuleActionLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionLabelPredicate = null;


        try {
            // InternalComposedLts.g:3073:2: (iv_ruleActionLabelPredicate= ruleActionLabelPredicate EOF )
            // InternalComposedLts.g:3074:2: iv_ruleActionLabelPredicate= ruleActionLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getActionLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActionLabelPredicate=ruleActionLabelPredicate();

            state._fsp--;

             current =iv_ruleActionLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionLabelPredicate"


    // $ANTLR start "ruleActionLabelPredicate"
    // InternalComposedLts.g:3081:1: ruleActionLabelPredicate returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleActionLabelPredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:3084:28: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalComposedLts.g:3085:1: ( (otherlv_0= RULE_ID ) )
            {
            // InternalComposedLts.g:3085:1: ( (otherlv_0= RULE_ID ) )
            // InternalComposedLts.g:3086:1: (otherlv_0= RULE_ID )
            {
            // InternalComposedLts.g:3086:1: (otherlv_0= RULE_ID )
            // InternalComposedLts.g:3087:3: otherlv_0= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getActionLabelPredicateRule());
            	        }
                    
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		newLeafNode(otherlv_0, grammarAccess.getActionLabelPredicateAccess().getActActionCrossReference_0()); 
            	

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionLabelPredicate"


    // $ANTLR start "entryRuleAnyLabelPredicate"
    // InternalComposedLts.g:3106:1: entryRuleAnyLabelPredicate returns [EObject current=null] : iv_ruleAnyLabelPredicate= ruleAnyLabelPredicate EOF ;
    public final EObject entryRuleAnyLabelPredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnyLabelPredicate = null;


        try {
            // InternalComposedLts.g:3107:2: (iv_ruleAnyLabelPredicate= ruleAnyLabelPredicate EOF )
            // InternalComposedLts.g:3108:2: iv_ruleAnyLabelPredicate= ruleAnyLabelPredicate EOF
            {
             newCompositeNode(grammarAccess.getAnyLabelPredicateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnyLabelPredicate=ruleAnyLabelPredicate();

            state._fsp--;

             current =iv_ruleAnyLabelPredicate; 
            match(input,EOF,FOLLOW_2); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnyLabelPredicate"


    // $ANTLR start "ruleAnyLabelPredicate"
    // InternalComposedLts.g:3115:1: ruleAnyLabelPredicate returns [EObject current=null] : ( () otherlv_1= '*' ) ;
    public final EObject ruleAnyLabelPredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // InternalComposedLts.g:3118:28: ( ( () otherlv_1= '*' ) )
            // InternalComposedLts.g:3119:1: ( () otherlv_1= '*' )
            {
            // InternalComposedLts.g:3119:1: ( () otherlv_1= '*' )
            // InternalComposedLts.g:3119:2: () otherlv_1= '*'
            {
            // InternalComposedLts.g:3119:2: ()
            // InternalComposedLts.g:3120:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAnyLabelPredicateAccess().getAnyLabelPredicateAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,53,FOLLOW_2); 

                	newLeafNode(otherlv_1, grammarAccess.getAnyLabelPredicateAccess().getAsteriskKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnyLabelPredicate"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0001000700002002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000084000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000200010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000041000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000028100000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000020100000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000B7A000200010L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0020020000000010L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x001C038000200010L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000080000L});

}
package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * Tripla = < int count, Action a, PTStatesSet s >
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class CounterTriple {
	
	/**
	 * Contatore di transizoni uscenti
	 **/		
	private int count = 0;

	/**
	 * Azione con cui sono etichettate le transizioni uscenti
	 **/		
	private Action a = null;

	/**
	 * Insieme di stati di destinazione delle transizioni. 
	 **/
	private PTStatesSet ptstatesSet = null;

	/**
	 * Costruisce una nuova tripla.
	 **/	
	public CounterTriple(PTStatesSet ss, Action a) {
		this.ptstatesSet = ss;
		this.a = a;
	}

	/**
	 * Restituisce il contatore.
	 **/	
	public int getCount() {
		return this.count;	
	}

	/**
	 * Incrementa di 1 il contatore.
	 **/
	public void increment() {
		count++;	
	}

	/**
	 * Restituisce l'insieme di stati.
	 **/
	public PTStatesSet getPTStatesSet() {
		return this.ptstatesSet;	
	}	

	/**
	 * Restituisce l'azione con cui sono etichettate tutte le transizioni
	 **/
	public Action getAction() {
		return this.a;	
	}
}
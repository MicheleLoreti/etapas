package org.cmg.tapas.testGraph.algorithms.branching;

// graph
import org.cmg.tapas.graph.EdgeFilter;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.MappedGraph;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.GraphAnalyzer2;
import org.cmg.tapas.graph.algorithms.branching.gv.GVChecker;
import org.cmg.tapas.graph.algorithms.branching.gvks.GVKSChecker;
import org.cmg.tapas.graph.algorithms.branching.gvs.GVSChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.RandomGraphGenerator;
import org.cmg.tapas.testGraph.algorithms.tapas1.branching.TBranchingChecker;


/**
 * BranchingTiming esegue un confronto tra i vari
 * algoritmi per il calcolo della bisimulazione branching.
 *
 * @author Guzman Tierno
 **/
public class BranchingTiming {
	
	public static void main(String[] args) {
		// Options

		// active checkers
		boolean gv = true;		// must be true, it is used for comparisons
		boolean gvs = false;
		boolean gvks = true;
		boolean tapas1 = false;
				
		// 	tau options
		boolean tauActions = true;
		int actionCount = 10;	
		double tauProbability = 0.3;
		boolean useTauProbability = false;

		// states and edges
		int statesNumber = 2000;
		double connectionProbability = 1.72 * (
			useTauProbability 
			? 1.0 / (statesNumber * tauProbability)
			: (1.0*actionCount) / statesNumber
		);
				
		// test
		int repetitions = 1;
		int printEvery = 1;
		
		// carico critico: 
		// (1.75*actionCount) / statesNumber

		// _____________________________________________________________________

		GraphInterface<LtsState,LtsAction> graph;
		GraphInterface<?,LtsAction> newGraph;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		GraphInterface<LtsState,LtsAction> graph2;
		MappedGraph<LtsState,LtsAction,LtsState> collapsed;
		LtsState state2_1;
		LtsState state2_2;
		GraphItemFactory graphFactory = new GraphItemFactory();
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>(graphFactory);
//		TauDagGenerator<LtsState,LtsAction> generator = 
//			new TauDagGenerator<LtsState,LtsAction>(graphFactory);
		GraphAnalyzer2<LtsState,LtsAction> analyzer; 
		
		LtsAction[] actions = new LtsAction[actionCount];
		if( tauActions )
			actions[0] = graphFactory.getTauAction();
		for( int i=tauActions?1:0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );
		
		boolean gvRes = true;
		boolean tapas1Res = true;
		boolean gvsRes = true;
		boolean gvksRes = true;
		double totalTime=0;
		double genTime=0;
		double gvTime=0, tapas1Time=0, gvsTime=0, gvksTime=0;
		double start;
		double initTime = System.currentTimeMillis();
		Evaluator<LtsState,?> gvMap = null;
		Evaluator<LtsState,?> gvsMap = null;
		Evaluator<LtsState,?> gvksMap = null;
		System.out.println(connectionProbability);
		for( int i=0; i<repetitions; ++i ) {							
			if(i%printEvery==0)
				System.out.print(i + ": ");
			
			// generate random graph (according to settings)			
			start = System.currentTimeMillis();
			if( useTauProbability )
				pointedGraph = generator.generateRandomGraph(
					statesNumber, actions, connectionProbability, tauProbability
				);
			else
				pointedGraph = generator.generateRandomGraph(
					statesNumber, actions, connectionProbability
				);
			graph = pointedGraph.getGraph();			
			analyzer = new GraphAnalyzer2<LtsState,LtsAction>(graph, graphFactory);
			genTime += System.currentTimeMillis() - start;

			// gv
			GVChecker<LtsState,LtsAction> gvChecker = null;
			if( gv ) {
				start = System.currentTimeMillis();
				gvChecker = new GVChecker<LtsState,LtsAction>(graph);
				gvMap = gvChecker.computeEquivalence(null);
				gvRes = 
					gvMap.eval(pointedGraph.getState1()) == 
					gvMap.eval(pointedGraph.getState2());
				gvTime += 
					System.currentTimeMillis() - start - GVChecker.collapseTime;
			}
			
			// gvs
			if( gvs ) {
				start = System.currentTimeMillis();
				GVSChecker<LtsState,LtsAction> gvsChecker = 
				new GVSChecker<LtsState,LtsAction>(graph);
				gvsMap = gvsChecker.computeEquivalence(null);
				gvsRes = 
					gvsMap.eval(pointedGraph.getState1()) == 
					gvsMap.eval(pointedGraph.getState2());
				gvsTime += System.currentTimeMillis() - start;
			}
			
			// gvks
			if( gvks ) {
				start = System.currentTimeMillis();
				GVKSChecker<LtsState,LtsAction> gvksChecker = 
				new GVKSChecker<LtsState,LtsAction>(graph);
				gvksMap = gvksChecker.computeEquivalence(null);
				gvksRes = 
					gvksMap.eval(pointedGraph.getState1()) == 
					gvksMap.eval(pointedGraph.getState2());
				gvksTime += 
					System.currentTimeMillis() - start - GVKSChecker.collapseTime;
			}
			
			// Bisogna collassare il grafo perch� Tapas1 al momento
			// calcola branching correnttamente solo per grafi senza cicli tau.
			start = System.currentTimeMillis();
			collapsed = analyzer.collapseLoops(
				analyzer.TAU_FILTER,
				new EdgeFilter<LtsState,LtsAction>() {
					@Override public boolean check(
						LtsState src, LtsAction action, LtsState dest
					) {
						return !action.isTau() || src!=dest;
					}
				}
			);
			graph2 = collapsed.getGraph();
			state2_1 = collapsed.getMap().get( pointedGraph.getState1() );
			state2_2 = collapsed.getMap().get( pointedGraph.getState2() );
	
			// Tapas1, solo controllo
			if( tapas1 ) {
				TBranchingChecker<LtsState,LtsAction> checker = 
					new TBranchingChecker<LtsState,LtsAction>();
				checker.setParameters(
					graph2, graph2, state2_1, state2_2, false
				); 
				tapas1Res = checker.checkEquivalence();
				tapas1Time += System.currentTimeMillis() - start;
			}

			// _________________________________________________________________
			// gvs
			
			if( gvs && gvRes!=gvsRes ) {
				System.out.println("Errore gvs: ______________________ ");
				System.out.println("gv: "  + gvRes);				
				System.out.println("gvs: " + gvsRes);				
				System.out.println(
					"States: " + 
					pointedGraph.getState1() + ", " +
					pointedGraph.getState2()
				);
				System.out.println( graph );
				break;
			}

			// _________________________________________________________________
			// gvks

			if( gvks && gvRes!=gvksRes ) {
				System.out.println("Errore gvks: ______________________ ");
				System.out.println("gv: "   + gvRes);				
				System.out.println("gvks: " + gvksRes);				
				System.out.println(
					"States: " + 
					pointedGraph.getState1() + ", " +
					pointedGraph.getState2()
				);
				System.out.println( graph );
				break;
			}

			// _________________________________________________________________
			// tapas1

			if( tapas1 && tapas1Res!=gvRes ) {
				System.out.println("Errore tapas1: ______________________ ");
				System.out.println("Tapas1: " + tapas1Res);				
				System.out.println("gv: " + gvRes);				
				System.out.println(
					"States: " + 
					pointedGraph.getState1() + ", " +
					pointedGraph.getState2()
				);
				System.out.println( graph );
				System.out.println(
					"States2: " + state2_1 + ", " + state2_2
				);
				System.out.println( graph2 );
				break;
			}

			if(i%printEvery==0) {
				newGraph = gvChecker.getNewGraph();
				if( newGraph!=null )				
					System.out.print( 
						newGraph.numStates() + " " +
						newGraph.numEdges() + " " +
						newGraph.getEdgeCount( LtsAction.TAU ) + " "
					);
				System.out.println( gvRes ? "True" : "False");
			}
			
			System.gc();
		}
		totalTime = System.currentTimeMillis() - initTime;
				
		// Print results
		System.out.println( );			
		System.out.println( "total: " + totalTime );			
		System.out.println( "generation: " + genTime );			
		System.out.println( );			
		if( tapas1 )
			System.out.println( "tapas1: " + tapas1Time );			
		if( gv )
			System.out.println( "gv    : " + gvTime );			
		if( gvs )
			System.out.println( "gvs   : " + gvsTime );			
		if( gvks )
			System.out.println( "gvks  : " + gvksTime );			
	}	
	
}


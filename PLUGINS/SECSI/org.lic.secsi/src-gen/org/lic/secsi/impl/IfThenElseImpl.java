/**
 */
package org.lic.secsi.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.lic.secsi.Expression;
import org.lic.secsi.IfThenElse;
import org.lic.secsi.SecsiPackage;
import org.lic.secsi.Statement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Then Else</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.lic.secsi.impl.IfThenElseImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.lic.secsi.impl.IfThenElseImpl#getThenBranch <em>Then Branch</em>}</li>
 *   <li>{@link org.lic.secsi.impl.IfThenElseImpl#isWithElse <em>With Else</em>}</li>
 *   <li>{@link org.lic.secsi.impl.IfThenElseImpl#getElseBranch <em>Else Branch</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IfThenElseImpl extends StatementImpl implements IfThenElse
{
  /**
   * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExpression()
   * @generated
   * @ordered
   */
  protected Expression expression;

  /**
   * The cached value of the '{@link #getThenBranch() <em>Then Branch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThenBranch()
   * @generated
   * @ordered
   */
  protected Statement thenBranch;

  /**
   * The default value of the '{@link #isWithElse() <em>With Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isWithElse()
   * @generated
   * @ordered
   */
  protected static final boolean WITH_ELSE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isWithElse() <em>With Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isWithElse()
   * @generated
   * @ordered
   */
  protected boolean withElse = WITH_ELSE_EDEFAULT;

  /**
   * The cached value of the '{@link #getElseBranch() <em>Else Branch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElseBranch()
   * @generated
   * @ordered
   */
  protected Statement elseBranch;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IfThenElseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.IF_THEN_ELSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Expression getExpression()
  {
    return expression;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExpression(Expression newExpression, NotificationChain msgs)
  {
    Expression oldExpression = expression;
    expression = newExpression;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__EXPRESSION, oldExpression, newExpression);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExpression(Expression newExpression)
  {
    if (newExpression != expression)
    {
      NotificationChain msgs = null;
      if (expression != null)
        msgs = ((InternalEObject)expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.IF_THEN_ELSE__EXPRESSION, null, msgs);
      if (newExpression != null)
        msgs = ((InternalEObject)newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.IF_THEN_ELSE__EXPRESSION, null, msgs);
      msgs = basicSetExpression(newExpression, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__EXPRESSION, newExpression, newExpression));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getThenBranch()
  {
    return thenBranch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetThenBranch(Statement newThenBranch, NotificationChain msgs)
  {
    Statement oldThenBranch = thenBranch;
    thenBranch = newThenBranch;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__THEN_BRANCH, oldThenBranch, newThenBranch);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setThenBranch(Statement newThenBranch)
  {
    if (newThenBranch != thenBranch)
    {
      NotificationChain msgs = null;
      if (thenBranch != null)
        msgs = ((InternalEObject)thenBranch).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.IF_THEN_ELSE__THEN_BRANCH, null, msgs);
      if (newThenBranch != null)
        msgs = ((InternalEObject)newThenBranch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.IF_THEN_ELSE__THEN_BRANCH, null, msgs);
      msgs = basicSetThenBranch(newThenBranch, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__THEN_BRANCH, newThenBranch, newThenBranch));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isWithElse()
  {
    return withElse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWithElse(boolean newWithElse)
  {
    boolean oldWithElse = withElse;
    withElse = newWithElse;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__WITH_ELSE, oldWithElse, withElse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Statement getElseBranch()
  {
    return elseBranch;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetElseBranch(Statement newElseBranch, NotificationChain msgs)
  {
    Statement oldElseBranch = elseBranch;
    elseBranch = newElseBranch;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH, oldElseBranch, newElseBranch);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setElseBranch(Statement newElseBranch)
  {
    if (newElseBranch != elseBranch)
    {
      NotificationChain msgs = null;
      if (elseBranch != null)
        msgs = ((InternalEObject)elseBranch).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH, null, msgs);
      if (newElseBranch != null)
        msgs = ((InternalEObject)newElseBranch).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH, null, msgs);
      msgs = basicSetElseBranch(newElseBranch, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH, newElseBranch, newElseBranch));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SecsiPackage.IF_THEN_ELSE__EXPRESSION:
        return basicSetExpression(null, msgs);
      case SecsiPackage.IF_THEN_ELSE__THEN_BRANCH:
        return basicSetThenBranch(null, msgs);
      case SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH:
        return basicSetElseBranch(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SecsiPackage.IF_THEN_ELSE__EXPRESSION:
        return getExpression();
      case SecsiPackage.IF_THEN_ELSE__THEN_BRANCH:
        return getThenBranch();
      case SecsiPackage.IF_THEN_ELSE__WITH_ELSE:
        return isWithElse();
      case SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH:
        return getElseBranch();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SecsiPackage.IF_THEN_ELSE__EXPRESSION:
        setExpression((Expression)newValue);
        return;
      case SecsiPackage.IF_THEN_ELSE__THEN_BRANCH:
        setThenBranch((Statement)newValue);
        return;
      case SecsiPackage.IF_THEN_ELSE__WITH_ELSE:
        setWithElse((Boolean)newValue);
        return;
      case SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH:
        setElseBranch((Statement)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.IF_THEN_ELSE__EXPRESSION:
        setExpression((Expression)null);
        return;
      case SecsiPackage.IF_THEN_ELSE__THEN_BRANCH:
        setThenBranch((Statement)null);
        return;
      case SecsiPackage.IF_THEN_ELSE__WITH_ELSE:
        setWithElse(WITH_ELSE_EDEFAULT);
        return;
      case SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH:
        setElseBranch((Statement)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.IF_THEN_ELSE__EXPRESSION:
        return expression != null;
      case SecsiPackage.IF_THEN_ELSE__THEN_BRANCH:
        return thenBranch != null;
      case SecsiPackage.IF_THEN_ELSE__WITH_ELSE:
        return withElse != WITH_ELSE_EDEFAULT;
      case SecsiPackage.IF_THEN_ELSE__ELSE_BRANCH:
        return elseBranch != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (withElse: ");
    result.append(withElse);
    result.append(')');
    return result.toString();
  }

} //IfThenElseImpl

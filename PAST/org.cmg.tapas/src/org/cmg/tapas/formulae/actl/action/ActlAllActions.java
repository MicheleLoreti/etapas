/**
 * 
 */
package org.cmg.tapas.formulae.actl.action;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlActionPredicateVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.TrueFilter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class ActlAllActions<S extends APProcess<S,A> , A extends ActionInterface> implements ActlActionPredicate<S,A>{

	public Filter<A> getFilter() {
		return new TrueFilter<A>();
	}

	public void acceptVisitor(ActlActionPredicateVisitor<S,A> v) {
		v.accept(this);
	}

	public String toString() {
		return "*";
	}
}

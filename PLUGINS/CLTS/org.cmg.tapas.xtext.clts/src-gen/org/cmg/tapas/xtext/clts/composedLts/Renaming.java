/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Renaming</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.Renaming#getArg <em>Arg</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.Renaming#getMaps <em>Maps</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getRenaming()
 * @model
 * @generated
 */
public interface Renaming extends CompositionBody
{
  /**
   * Returns the value of the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' containment reference.
   * @see #setArg(CompositionBody)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getRenaming_Arg()
   * @model containment="true"
   * @generated
   */
  CompositionBody getArg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.Renaming#getArg <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' containment reference.
   * @see #getArg()
   * @generated
   */
  void setArg(CompositionBody value);

  /**
   * Returns the value of the '<em><b>Maps</b></em>' containment reference list.
   * The list contents are of type {@link org.cmg.tapas.xtext.clts.composedLts.Mapping}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Maps</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Maps</em>' containment reference list.
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getRenaming_Maps()
   * @model containment="true"
   * @generated
   */
  EList<Mapping> getMaps();

} // Renaming

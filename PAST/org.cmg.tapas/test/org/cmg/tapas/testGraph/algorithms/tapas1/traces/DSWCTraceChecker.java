package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessDSG;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;



/**
 * La classe DSWCTraceChecker realizza l'algoritmo di verifica 
 * per l'equivalenza divergence sensitive weak completed trace. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class DSWCTraceChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {

    /** 
     * Costruisce una nuova istanza del checker.
     */
	public DSWCTraceChecker() {
		super("DSWC Trace");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 */	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );
		
		// divergence sensitive graph
		ProcessDSG p1DSG = ProcessDSG.buildDSGraph(p1);
		ProcessDSG p2DSG = ProcessDSG.buildDSGraph(p2);
		
		// fusion
		ProcessDSG pDSG = ProcessDSG.createProcessFusion(p1DSG, p2DSG);

		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();
		
		// initializing partition 
		PTPartition initialRo = DSWCTraceUtil.initPTPartition(pDSG);
		
		// refinement
		PTState initialPTState1 = initialRo.getPTState(pDSG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pDSG.getSecondInitState());

		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pDSG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		boolean result = ro.checkSameClass(
			pDSG.getInitState(), pDSG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			DSWCTraceUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName(),
				pDSG				
			);
		}
		
		return result;
	}
}
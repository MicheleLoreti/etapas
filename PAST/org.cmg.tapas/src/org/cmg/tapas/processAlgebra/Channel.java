/**
 * 
 */
package org.cmg.tapas.processAlgebra;

/**
 * @author loreti
 *
 */
public class Channel implements Comparable<Channel>{
	
	private String name;
	private int hashCode;

	public Channel(String name) {
		this.name = name;
		this.hashCode = name.hashCode();
	}

	public String getName() {
		return name;
	}

	public int compareTo(Channel o) {
		return name.compareTo(o.name);
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Channel)&&(((Channel) obj).name.equals(this.name));
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public String toString() {
		return name;
	}

}

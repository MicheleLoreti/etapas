/**
 * 
 */
package org.cmg.tapas.formulae.hml;


import java.util.Set;

import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.HashSetActionFilter;
import org.cmg.tapas.graph.filter.SingleActionFilter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author Calzolai
 *
 */
public final class WeakBoxFormula<X extends APProcess<X, Y>,Y extends ActionInterface> 
						extends HmlFormula<X,Y> {

	private Filter<Y> filter;
	private HmlFormula<X,Y> arg;
	private Y tauAct;
	private HmlFormula<X, Y> equivalent;
	private boolean isEps;
	
	public WeakBoxFormula(HmlFormula<X,Y> arg, Y tauAct, boolean eps) {
		this.arg = arg;
		this.tauAct = tauAct;		 
		this.filter = new TauFilter<Y>();
		int type = 1;		
		if(eps){
			this.filter = new EpsFilter<Y>();
			type = 2;
		}else{
			this.filter = new TauFilter<Y>();
			type = 0;
		}
		createEquivalent(type);
	}
	
	public WeakBoxFormula(Filter<Y> f, HmlFormula<X,Y> arg, Y tauAct) {
		this.arg = arg;
		this.tauAct = tauAct;
		this.filter = f;
		int type = 1;		
		if(f instanceof TauFilter)
			type = 0;
		else if(f instanceof EpsFilter)
			type = 2;
		createEquivalent(type);
	}
	
	public WeakBoxFormula(Y action , HmlFormula<X, Y> arg, Y tauAct) { 	
		this.arg = arg;
		this.tauAct = tauAct;
		
		int type = 1;
		if(action.isTau()){
			this.filter = new TauFilter<Y>();
			type = 0;
		} else{
			filter = new SingleActionFilter<Y>(action);
		}
		
		createEquivalent(type);
	}
	
	public WeakBoxFormula(Set<Y> action_set, HmlFormula<X, Y> arg, Y tauAct) {
		this.arg = arg;
		this.tauAct = tauAct;
		int type = 1;	
		if(action_set == null || action_set.size() == 0){
			this.filter = new EpsFilter<Y>();
			type = 2;
		}else if(action_set.size() == 1){
			Y act = action_set.iterator().next();
			if(act.isTau()){
				this.filter = new TauFilter<Y>();
				type = 0;
			}
		}
		if(type == 1){
			this.filter = new HashSetActionFilter<Y>(action_set);
		}
		createEquivalent(type);
	}	
	
	private HmlFormula<X,Y>  getEmptyBox(){
		//max X.([tau]X & [tau] arg) 
		String name = "X";
		FixPoint<X, Y> fp = new FixPoint<X, Y>(true, name);
		BoxFormula<X, Y> D1 = new BoxFormula<X, Y>(tauAct, fp.getReference()); 
		BoxFormula<X, Y> D2 = new BoxFormula<X, Y>(tauAct, arg); 
		AndFormula<X, Y> and = new AndFormula<X, Y>(D1, D2);
		fp.setSubformula(and);

//		BoxFormula<X, Y> res = new BoxFormula<X, Y>(tauAct, fp);
		return fp;
	}
	
	private void createEquivalent(int type){
		if(type == 0){
			//max X.([tau]X & [tau] arg) = [[tau]]+ arg 
			equivalent = getEmptyBox();
			isEps = false;
		}else if(type == 1){
			// max X.( [filter] (max Y.(arg & [tau] Y)) & [tau] X )
			String name = "Y";
			FixPoint<X, Y> tmp = new FixPoint<X, Y>(true, name);
			BoxFormula<X, Y> box1 = new BoxFormula<X, Y>(tauAct, tmp.getReference()); 		
			AndFormula<X, Y> and1 = new AndFormula<X, Y>(arg, box1);
			tmp.setSubformula(and1);
			//---------------------------------------
			String name2 = "X";
			FixPoint<X, Y> res = new FixPoint<X, Y>(true, name2);
			BoxFormula<X, Y> box2 = new BoxFormula<X, Y>(filter, tmp);
			BoxFormula<X, Y> box3 = new BoxFormula<X, Y>(tauAct, res.getReference()); 
			AndFormula<X, Y> and2 = new AndFormula<X, Y>(box2, box3);
			res.setSubformula(and2);	
			equivalent = res;
			isEps = false;
		}else if(type == 2){
			//max X.([tau]X & arg) = [[tau]]* arg 
			equivalent = getEpsBox();
			isEps = true;
		}
	}
	
	private HmlFormula<X, Y> getEpsBox() {
		//max X.([tau]X & arg) = [tau]* arg 
		String name = "X";
		FixPoint<X, Y> fp = new FixPoint<X, Y>(true, name);
		BoxFormula<X, Y> D1 = new BoxFormula<X, Y>(tauAct, fp.getReference()); 
		AndFormula<X, Y> and = new AndFormula<X, Y>(D1, arg);
		fp.setSubformula(and);
		return fp;
	}

	public boolean satisfies(X x ) {
		return equivalent.satisfies(x);
	}
	
	@Override
	public Proof<X, Y> getProof(X x) {		
		return equivalent.getProof(x);
	}

	@Override
	protected HmlFormula<X, Y> doClone() {
		return new WeakBoxFormula<X, Y>(filter,arg.clone(), tauAct);
	}

	@Override
	protected void doVisit(FormulaVisitor<X, Y> visitor) {
		visitor.visitWeakBox(this);
	}

	@Override
	protected String _toString() {
		if (filter == null)
			return "[[]]"+arg.getUnicode();
		return "[["+filter.toString()+"]]"+arg;
	}

	@Override
	public String getUnicode() {
		if (filter == null)
			return "[[]]"+arg.getUnicode();
		return "[["+filter.toString()+"]]"+arg.getUnicode();
	}

	@Override
	public String operator() {
		if (filter == null)
			return "WEAK-BOX(TAU)";
		return "WEAK-BOX( "+filter.toString()+" )";
	}

	@Override
	protected boolean _equals(Object obj) {
		if(obj == null)
			return false;
		
		if (obj instanceof WeakBoxFormula) {
			WeakBoxFormula<X, Y> f = (WeakBoxFormula<X, Y>) obj;
			
			if(!filter.equals(f.filter))
				return false;
			
			return arg.equals(f.arg);
			
		}		
		return false;
	}
}

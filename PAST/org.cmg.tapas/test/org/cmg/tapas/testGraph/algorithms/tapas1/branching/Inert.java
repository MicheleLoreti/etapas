package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class Inert {
	
	private Vector<BBState> bbstates = null;
	
	public Inert() {
		this.bbstates = new Vector<BBState>();
  	}
  	
  	public int getDimension() {
  		return bbstates.size();
  	}

	/**
	 * Inserisce una transizione inerte senza duplicati e ordinata.
	 * Restituisce true se la transizione non era gi� presente nell'insieme
	 */
	public boolean addInertTransition(BBState s) {	
		int i = 0;
		int result = 0;		
		for( i=0; i<getDimension(); i++ ){
			result = inertTransitionAt(i).compareTo(s);
			if( result == 0 ) 
				return false;
			if( result > 0 ) 
				break;
		}
		bbstates.add(i,s);
		return true;
	}
	
	
	public BBState removeInertTransitionAt(int index) {
		try {
			return bbstates.remove(index);
		}
		catch(ArrayIndexOutOfBoundsException unused) {
			return null;
		}
	}



	public BBState inertTransitionAt(int index) {
		try {
			return bbstates.elementAt(index);
		}	
		catch(ArrayIndexOutOfBoundsException unused) {
			return null;
		}
	}
	

	public boolean containInertTransition(BBState s) {
		for( int i=0; i<getDimension(); i++ )	
			if( inertTransitionAt(i) == s ) 
				return true;
		return false;
	}

	public boolean isEmpty() {
		if( getDimension()==0 ) 
			return true;
		
		return false;
	}
	
	public void print() {
		print(System.out);
	}



	public void print(java.io.PrintStream console) {
		int i=0;		
		console.append("{");
		for( i=0; i<this.getDimension(); i++ ) {
			if(i>0) 
				console.append(", ");
			inertTransitionAt(i).printAll(console);	
		}
		if(i==0) 
			console.append("empty");
		console.append("}");
	}

}
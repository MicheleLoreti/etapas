/**
 * 
 */
package org.cmg.tapas.formulae.actl.state;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlFormulaVisitor;
import org.cmg.tapas.formulae.actl.macro.StateFormulaMacro;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class Call< S extends APProcess<S, A> , A extends ActionInterface> extends ActlFormula<S, A> {
	
	private StateFormulaMacro<S, A> macro;
	private ActlFormula<S, A>[] parameters;
	
	public Call( StateFormulaMacro<S, A> macro , ActlFormula<S, A> ... parameters ) {
		super();
		if (macro.parameters() != parameters.length) {
			throw new IllegalArgumentException();
		}
		this.macro = macro;
		this.parameters = parameters;
	}
	

	public Call(Call<S, A> call) {
		this( call.macro , call.parameters );
	}


	@Override
	protected String _toString() {
		String toReturn = macro.getName();
		toReturn += "( ";
		boolean first = true;
		for (ActlFormula<S, A> f : parameters) {
			if (first) {
				first = false;
			} else {
				toReturn += " , ";
			}
			toReturn += f.toString(); 
		}
		toReturn += " )";
		return toReturn;
	}

	@Override
	protected ActlFormula<S, A> doClone() {
		return new Call<S, A>( this );
	}

	@Override
	protected void doVisit(ActlFormulaVisitor<S, A> visitor) {
		visitor.visitCall( this );
	}

	@Override
	public String getUnicode() {
		String toReturn = macro.getName();
		toReturn += "( ";
		boolean first = true;
		for (ActlFormula<S, A> f : parameters) {
			if (first) {
				first = false;
			} else {
				toReturn += " , ";
			}
			toReturn += f.getUnicode();
		}
		toReturn += " )";
		return toReturn;	}

	@Override
	public String operator() {
		return null;
	}

	@Override
	public boolean satisfies(S x) {
		return macro.getFormula(parameters).satisfies(x);
	}


	public StateFormulaMacro<S, A> getMacro() {
		return macro;
	}


	public ActlFormula<S, A>[] getParameters() {
		return parameters;
	}
	
}

/**
 */
package org.lic.secsi.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.lic.secsi.Agent;
import org.lic.secsi.ChannelsDeclaration;
import org.lic.secsi.Constant;
import org.lic.secsi.Program;
import org.lic.secsi.PropList;
import org.lic.secsi.SecsiPackage;
import org.lic.secsi.TypeDef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.lic.secsi.impl.ProgramImpl#getGlobalchanels <em>Globalchanels</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProgramImpl#getAgents <em>Agents</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProgramImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProgramImpl#getConsts <em>Consts</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProgramImpl#getSystem <em>System</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ProgramImpl#getProperties <em>Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProgramImpl extends MinimalEObjectImpl.Container implements Program
{
  /**
   * The cached value of the '{@link #getGlobalchanels() <em>Globalchanels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGlobalchanels()
   * @generated
   * @ordered
   */
  protected EList<ChannelsDeclaration> globalchanels;

  /**
   * The cached value of the '{@link #getAgents() <em>Agents</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAgents()
   * @generated
   * @ordered
   */
  protected EList<Agent> agents;

  /**
   * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypes()
   * @generated
   * @ordered
   */
  protected EList<TypeDef> types;

  /**
   * The cached value of the '{@link #getConsts() <em>Consts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsts()
   * @generated
   * @ordered
   */
  protected EList<Constant> consts;

  /**
   * The cached value of the '{@link #getSystem() <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSystem()
   * @generated
   * @ordered
   */
  protected org.lic.secsi.System system;

  /**
   * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProperties()
   * @generated
   * @ordered
   */
  protected PropList properties;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProgramImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.PROGRAM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ChannelsDeclaration> getGlobalchanels()
  {
    if (globalchanels == null)
    {
      globalchanels = new EObjectContainmentEList<ChannelsDeclaration>(ChannelsDeclaration.class, this, SecsiPackage.PROGRAM__GLOBALCHANELS);
    }
    return globalchanels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Agent> getAgents()
  {
    if (agents == null)
    {
      agents = new EObjectContainmentEList<Agent>(Agent.class, this, SecsiPackage.PROGRAM__AGENTS);
    }
    return agents;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TypeDef> getTypes()
  {
    if (types == null)
    {
      types = new EObjectContainmentEList<TypeDef>(TypeDef.class, this, SecsiPackage.PROGRAM__TYPES);
    }
    return types;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Constant> getConsts()
  {
    if (consts == null)
    {
      consts = new EObjectContainmentEList<Constant>(Constant.class, this, SecsiPackage.PROGRAM__CONSTS);
    }
    return consts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.lic.secsi.System getSystem()
  {
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSystem(org.lic.secsi.System newSystem, NotificationChain msgs)
  {
    org.lic.secsi.System oldSystem = system;
    system = newSystem;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.PROGRAM__SYSTEM, oldSystem, newSystem);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSystem(org.lic.secsi.System newSystem)
  {
    if (newSystem != system)
    {
      NotificationChain msgs = null;
      if (system != null)
        msgs = ((InternalEObject)system).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.PROGRAM__SYSTEM, null, msgs);
      if (newSystem != null)
        msgs = ((InternalEObject)newSystem).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.PROGRAM__SYSTEM, null, msgs);
      msgs = basicSetSystem(newSystem, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.PROGRAM__SYSTEM, newSystem, newSystem));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropList getProperties()
  {
    return properties;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetProperties(PropList newProperties, NotificationChain msgs)
  {
    PropList oldProperties = properties;
    properties = newProperties;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.PROGRAM__PROPERTIES, oldProperties, newProperties);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProperties(PropList newProperties)
  {
    if (newProperties != properties)
    {
      NotificationChain msgs = null;
      if (properties != null)
        msgs = ((InternalEObject)properties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.PROGRAM__PROPERTIES, null, msgs);
      if (newProperties != null)
        msgs = ((InternalEObject)newProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.PROGRAM__PROPERTIES, null, msgs);
      msgs = basicSetProperties(newProperties, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.PROGRAM__PROPERTIES, newProperties, newProperties));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SecsiPackage.PROGRAM__GLOBALCHANELS:
        return ((InternalEList<?>)getGlobalchanels()).basicRemove(otherEnd, msgs);
      case SecsiPackage.PROGRAM__AGENTS:
        return ((InternalEList<?>)getAgents()).basicRemove(otherEnd, msgs);
      case SecsiPackage.PROGRAM__TYPES:
        return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
      case SecsiPackage.PROGRAM__CONSTS:
        return ((InternalEList<?>)getConsts()).basicRemove(otherEnd, msgs);
      case SecsiPackage.PROGRAM__SYSTEM:
        return basicSetSystem(null, msgs);
      case SecsiPackage.PROGRAM__PROPERTIES:
        return basicSetProperties(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SecsiPackage.PROGRAM__GLOBALCHANELS:
        return getGlobalchanels();
      case SecsiPackage.PROGRAM__AGENTS:
        return getAgents();
      case SecsiPackage.PROGRAM__TYPES:
        return getTypes();
      case SecsiPackage.PROGRAM__CONSTS:
        return getConsts();
      case SecsiPackage.PROGRAM__SYSTEM:
        return getSystem();
      case SecsiPackage.PROGRAM__PROPERTIES:
        return getProperties();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SecsiPackage.PROGRAM__GLOBALCHANELS:
        getGlobalchanels().clear();
        getGlobalchanels().addAll((Collection<? extends ChannelsDeclaration>)newValue);
        return;
      case SecsiPackage.PROGRAM__AGENTS:
        getAgents().clear();
        getAgents().addAll((Collection<? extends Agent>)newValue);
        return;
      case SecsiPackage.PROGRAM__TYPES:
        getTypes().clear();
        getTypes().addAll((Collection<? extends TypeDef>)newValue);
        return;
      case SecsiPackage.PROGRAM__CONSTS:
        getConsts().clear();
        getConsts().addAll((Collection<? extends Constant>)newValue);
        return;
      case SecsiPackage.PROGRAM__SYSTEM:
        setSystem((org.lic.secsi.System)newValue);
        return;
      case SecsiPackage.PROGRAM__PROPERTIES:
        setProperties((PropList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.PROGRAM__GLOBALCHANELS:
        getGlobalchanels().clear();
        return;
      case SecsiPackage.PROGRAM__AGENTS:
        getAgents().clear();
        return;
      case SecsiPackage.PROGRAM__TYPES:
        getTypes().clear();
        return;
      case SecsiPackage.PROGRAM__CONSTS:
        getConsts().clear();
        return;
      case SecsiPackage.PROGRAM__SYSTEM:
        setSystem((org.lic.secsi.System)null);
        return;
      case SecsiPackage.PROGRAM__PROPERTIES:
        setProperties((PropList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.PROGRAM__GLOBALCHANELS:
        return globalchanels != null && !globalchanels.isEmpty();
      case SecsiPackage.PROGRAM__AGENTS:
        return agents != null && !agents.isEmpty();
      case SecsiPackage.PROGRAM__TYPES:
        return types != null && !types.isEmpty();
      case SecsiPackage.PROGRAM__CONSTS:
        return consts != null && !consts.isEmpty();
      case SecsiPackage.PROGRAM__SYSTEM:
        return system != null;
      case SecsiPackage.PROGRAM__PROPERTIES:
        return properties != null;
    }
    return super.eIsSet(featureID);
  }

} //ProgramImpl

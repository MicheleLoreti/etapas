package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;

/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBState {
	
	private State state = null;
	private Inert inert = null;	
	private boolean mark = false;	
	private BBBloc owner = null;
		
	public BBState(State state) {
		this.state = state;
		inert = new Inert();
	}
	
	public State getState() {
		return state;	
	}

	public Inert getInert() {
		return inert;		
	}

	
	public boolean isMark() {
		return this.mark;		
	}
	
	public BBBloc getOwner() {
		return owner;	
	}
	
	public boolean addInert(BBState s) {
		return this.inert.addInertTransition(s);
	}
	

	public void setMark(boolean mark) {
		this.mark = mark;		
	}

	public void setOwner(BBBloc owner) {
		this.owner = owner;		
	}
	
	public int compareTo(BBState s) {
		if( s!=null )
			return state.compareTo(s.getState());
			
		return -1;
	}	
	public void print() {
		printAll(System.out);
	}

	public void printAll(java.io.PrintStream console) {
		console.append(
				"State " + getState().getName() +
				" Mark: " + mark +
				" Inert: "
		);
		inert.print(console);
	}

}
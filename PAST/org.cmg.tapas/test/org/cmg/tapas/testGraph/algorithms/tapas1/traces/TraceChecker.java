package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;


/**
 * La classe TTraceChecker realizza l'algoritmo di verifica 
 * per l'equivalenza trace. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class TraceChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {

    /** 
     * Costruisce una nuova istanza del checker.
     */
	public TraceChecker() {
		super("Trace");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 */	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );
				
		// deterministic graph's
		Lts p1DG = Lts.buildDeterministicGraph(p1);
		Lts p2DG = Lts.buildDeterministicGraph(p2);
		
		// fusion
		Lts pDG = Lts.createProcessFusion( p1DG, p2DG );
		
		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();
		
		// initializing partition 
		PTPartition initialRo = TraceUtil.initPTPartition( pDG );
		
		// refinement
		PTState initialPTState1 = initialRo.getPTState(pDG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pDG.getSecondInitState());

		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pDG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		boolean result = ro.checkSameClass(
			pDG.getInitState(), pDG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			TraceUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName()
			);
		}

		return result;
	}
}
package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.LinkedList;

import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public interface CounterExampleI<S extends APProcess<S, A>, A extends ActionInterface> {
	
	public LinkedList<HmlFormula<S, A>> getCounterExample(S s1, S s2)
									throws UnableToCreateCounterExample;
	
}
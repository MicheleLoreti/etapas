package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.LinkedList;

import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public abstract class AbstractCounterExample <S extends APProcess<S, A>, A extends ActionInterface> 
			implements CounterExampleI<S, A> {

	protected TableFormulae<S, A> old_formulae;
	protected TableFormulae<S, A> curr_formulae;
	protected Object hs1;
	protected Object hs2;
	
	protected abstract void init();
	protected abstract void step(Object hs1, Object hs2);	
	protected abstract Object getPartition(S s);
	
	private void compute() throws UnableToCreateCounterExample{
		//Init
		init();
		
		LinkedList<Object> pList = old_formulae.getPartitionList();
		
		//Loop
		while(curr_formulae.formulaIsEmpty(hs1, hs2) 
				&& curr_formulae.formulaIsEmpty(hs2, hs1)) {
			old_formulae = curr_formulae.clone();
			curr_formulae.resetAdded();
			for(int i=0; i<pList.size(); i++){
				if(i < pList.size()-1){
					Object s1 = pList.get(i);
					for(int j=i+1; j<pList.size(); j++){
						Object s2 = pList.get(j);
						if(old_formulae.formulaIsEmpty(s1, s2)
								&& old_formulae.formulaIsEmpty(s2, s1)){
							step(s1, s2);
						}
					}
				}
			}
			if(!curr_formulae.added())
				throw new UnableToCreateCounterExample(
				"Unable to create counter example");
		}
	}
	
	public String counterExampleToString(){
		return old_formulae.toString();
	}
	
	public LinkedList<HmlFormula<S, A>> getCounterExample(S s1, S s2)
				throws UnableToCreateCounterExample{
		hs1 = getPartition(s1);
		hs2 = getPartition(s2);
		if(old_formulae.formulaIsEmpty(hs1, hs2) 
				&& old_formulae.formulaIsEmpty(hs2, hs1))
			compute();
		return minimize(curr_formulae.getFormulae(hs1, hs2));
	}
	
	public LinkedList<HmlFormula<S, A>> minimize(LinkedList<HmlFormula<S, A>> fList){
		LinkedList<HmlFormula<S, A>> res = new LinkedList<HmlFormula<S, A>> ();
		int end = fList.size();
		for (int i = 0; i < end; i++) {
			HmlFormula<S, A> f = fList.get(i);
			if(!res.contains(f))
				res.add(f);
		}
		return res;
	}
}
package org.cmg.tapas.testGraph.algorithms.bisimulation;

import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.bisimulation.ksopt.KSOChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.RandomGraphGenerator;


/**
 * Fitting2D produce i dati per un fitting bidimensionale 
 * della bisimulazione. Le due variabili sono
 * il numero di nodi ed il carico del grafo ossia la 
 * percentuale di archi presenti.
 *
 * @author Guzman Tierno
 **/
public class Fitting2D {
	
	private static LtsAction[] actions = { 
		new LtsAction("0"),
		new LtsAction("1"),
		new LtsAction("2"),
		new LtsAction("3"),
		new LtsAction("4"),
		new LtsAction("5"),
		new LtsAction("6"),
		new LtsAction("7")
	};

	private static RandomGraphGenerator<LtsState,LtsAction> generator = 
		new RandomGraphGenerator<LtsState,LtsAction>(
			new GraphItemFactory()
	);

	public static void main(String[] args) {
		int minN = 600;
		int maxN = 3000;
		int gapN = 600;
		int stepsP = 5;
		int qN = (maxN-minN)/gapN;

		double[] measures = new double[ qN*stepsP ];
		int count = 0;
		
		double p;
		double gapP = 1.0/stepsP;
		double start;
		double time;
		System.out.println( "fittingN( [" );
		for( int n = minN; n<maxN; n += gapN ) {
			p = 0;
			for( int j=0; j<stepsP; ++j ) {
				System.out.print( "[" + n + " " + p +"]" );
				if( n<maxN-1 || j<stepsP-1 )
					System.out.print( ";" );
				PointedGraph<LtsState,LtsAction> pointedGraph = 
					generator.generateRandomGraph(n, actions, p);
				GraphInterface<LtsState,LtsAction> graph = 
					pointedGraph.getGraph();
					
				// KSO
				start = System.currentTimeMillis();
				KSOChecker<LtsState,LtsAction> ksoChecker = 
					new KSOChecker<LtsState,LtsAction>(graph);			
				ksoChecker.computeEquivalence(null);
				time = System.currentTimeMillis() - start;
				
				measures[count++] = time;
				p += gapP;
			}
			System.out.println();
		}
		System.out.println( "], [" );
		for( int i=0; i<qN*stepsP; ++i ) {
			System.out.print( measures[i] + " " );			
			if( (i+1)%stepsP==0 ) 
				System.out.println();
		}
		System.out.print( "], 30 )" );
		
	}	

}

// fittingN( [[600  0.0];[600  0.2];[600  0.4];[600  0.6];[600  0.8];[1200 0.0];[1200 0.2];[1200 0.4];[1200 0.6];[1200 0.8];[1800 0.0];[1800 0.2];[1800 0.4];[1800 0.6];[1800 0.8];[2400 0.0];[2400 0.2];[2400 0.4];[2400 0.6];[2400 0.8];], [50.0 110.0 170.0 220.0 280.0 0.0 330.0 660.0 990.0 1310.0 0.0 880.0 1870.0 2960.0 3790.0 0.0 1480.0 3240.0 5380.0 7580.0]', '[x(2)*x(1)*x(1)*log(x(1))]', 30 )






















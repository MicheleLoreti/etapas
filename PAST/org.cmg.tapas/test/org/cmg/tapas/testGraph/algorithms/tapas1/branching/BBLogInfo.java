package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;


/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBLogInfo {
	
	private Vector<BBLogInfoEntry> log = null;
	
	public BBLogInfo() {
		log = new Vector<BBLogInfoEntry>();
	}
	
	public void addEntry(BBLogInfoEntry e) {
		log.addElement(e);	
	}
	
	public int getDimension() {
		return log.size();	
	}
	
	public BBLogInfoEntry entryAt(int index) {
		return log.elementAt(index);	
	}
	
	public Vector<BBLogInfoEntry> getInfo(
		BBBloc bb1, BBBloc bb2, Action cause
	) {	
		BBLogInfoEntry e;
		int trovati = 0;
		Vector<BBLogInfoEntry> result = new Vector<BBLogInfoEntry>();
		
		//Cerco cosa differenzia bb1 e bb2 con l'azione cause
		int i = this.getDimension()-1;
		while( i>=0 && trovati<2 ) {
			e = entryAt(i);
			if( e.getCause().equals(cause) ) {
				if( e.getOrigin().equals(bb1) ){
					result.insertElementAt(e,0);
					trovati++;
				} else if( e.getOrigin().equals(bb2) ) {
					result.addElement(e);
					trovati++;			
				}
			}
			
			i--;
		}
		
		if( trovati == 2 ) 
			return result;
		
		return null;		
	}
	
	
	public void print(java.io.PrintStream console) {
		for( int i=0; i<this.getDimension(); i++ ) {
			console.append("\n");
			entryAt(i).print(console);
			console.append("\n");
		}	
	}
	
}
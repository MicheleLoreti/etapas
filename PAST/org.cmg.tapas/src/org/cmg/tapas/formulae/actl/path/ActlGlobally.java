package org.cmg.tapas.formulae.actl.path;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlPathFormulaVisitor;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.formulae.actl.action.ActlAllActions;
import org.cmg.tapas.formulae.actl.state.ActlNot;
import org.cmg.tapas.formulae.actl.state.ActlTrue;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public class ActlGlobally<X extends APProcess<X ,Y>,Y extends ActionInterface> 
			extends PathFormula<X, Y> {
	
	private ActlUntil<X, Y> equivalent;
	private ActlFormula<X, Y> original;
	
	public ActlGlobally(ActlActionPredicate<X,Y> filter, ActlFormula<X, Y> formula){
		//Equivalent = G phi = not(true {true} U not phi)
		//Equivalent = G {filter} phi = not(true {filter} U not phi)
		original = formula;
		ActlActionPredicate<X,Y> localF;
		if(filter == null)
			localF = new ActlAllActions<X, Y>();
		else
			localF = filter;
		ActlFormula<X, Y> formulaTrue = new ActlTrue<X, Y>();
		ActlNot<X, Y> notF = new ActlNot<X, Y>(formula);
		equivalent = new ActlUntil<X, Y>(formulaTrue, localF, notF);
	}

	@Override
	protected String _toString() {
		return this.operator()+" {"+equivalent.getActionPredicate1()+"} " 
		+" "+original.toString();
	}

	@Override
	protected PathFormula<X, Y> doClone() {
		return new ActlGlobally<X, Y>(equivalent.getActionPredicate1()
					, original.clone());
	}

	@Override
	protected void doVisit(ActlPathFormulaVisitor<X, Y> visitor) {
		visitor.visitGloballyFormula(this);
	}

	@Override
	public String getUnicode() {
		return this.operator()+" {"+equivalent.getActionPredicate1()+"} " 
					+" "+original.getUnicode();
	}

	@Override
	public String operator() {
		return "G";
	}

	@Override
	public boolean sat(X x, boolean forall) {
		return !equivalent.sat(x, !forall);
	}	
	
	public ActlFormula<X,Y> getFormula(){
		return original;
	}
	
	public ActlActionPredicate<X, Y> getFilter(){
		return equivalent.getActionPredicate1();
	}
}

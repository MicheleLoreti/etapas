/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage
 * @generated
 */
public interface ComposedLtsFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ComposedLtsFactory eINSTANCE = org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Element</em>'.
   * @generated
   */
  Element createElement();

  /**
   * Returns a new object of class '<em>Lts</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lts</em>'.
   * @generated
   */
  Lts createLts();

  /**
   * Returns a new object of class '<em>Lts Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lts Body</em>'.
   * @generated
   */
  LtsBody createLtsBody();

  /**
   * Returns a new object of class '<em>Lts Composition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lts Composition</em>'.
   * @generated
   */
  LtsComposition createLtsComposition();

  /**
   * Returns a new object of class '<em>Composition Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Composition Body</em>'.
   * @generated
   */
  CompositionBody createCompositionBody();

  /**
   * Returns a new object of class '<em>Reference</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Reference</em>'.
   * @generated
   */
  Reference createReference();

  /**
   * Returns a new object of class '<em>Mapping</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mapping</em>'.
   * @generated
   */
  Mapping createMapping();

  /**
   * Returns a new object of class '<em>Lts Declaration Body</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lts Declaration Body</em>'.
   * @generated
   */
  LtsDeclarationBody createLtsDeclarationBody();

  /**
   * Returns a new object of class '<em>Lts Rule</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lts Rule</em>'.
   * @generated
   */
  LtsRule createLtsRule();

  /**
   * Returns a new object of class '<em>Lts State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lts State</em>'.
   * @generated
   */
  LtsState createLtsState();

  /**
   * Returns a new object of class '<em>Label Rule</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Label Rule</em>'.
   * @generated
   */
  LabelRule createLabelRule();

  /**
   * Returns a new object of class '<em>State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>State</em>'.
   * @generated
   */
  State createState();

  /**
   * Returns a new object of class '<em>Rule</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Rule</em>'.
   * @generated
   */
  Rule createRule();

  /**
   * Returns a new object of class '<em>Label</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Label</em>'.
   * @generated
   */
  Label createLabel();

  /**
   * Returns a new object of class '<em>Action</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Action</em>'.
   * @generated
   */
  Action createAction();

  /**
   * Returns a new object of class '<em>Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formula</em>'.
   * @generated
   */
  Formula createFormula();

  /**
   * Returns a new object of class '<em>Hml Formula Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Formula Declaration</em>'.
   * @generated
   */
  HmlFormulaDeclaration createHmlFormulaDeclaration();

  /**
   * Returns a new object of class '<em>Hml Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Formula</em>'.
   * @generated
   */
  HmlFormula createHmlFormula();

  /**
   * Returns a new object of class '<em>Hml Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Predicate</em>'.
   * @generated
   */
  HmlPredicate createHmlPredicate();

  /**
   * Returns a new object of class '<em>Hml True</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml True</em>'.
   * @generated
   */
  HmlTrue createHmlTrue();

  /**
   * Returns a new object of class '<em>Hml False</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml False</em>'.
   * @generated
   */
  HmlFalse createHmlFalse();

  /**
   * Returns a new object of class '<em>Hml Not Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Not Formula</em>'.
   * @generated
   */
  HmlNotFormula createHmlNotFormula();

  /**
   * Returns a new object of class '<em>Hml Diamond Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Diamond Formula</em>'.
   * @generated
   */
  HmlDiamondFormula createHmlDiamondFormula();

  /**
   * Returns a new object of class '<em>Hml Box Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Box Formula</em>'.
   * @generated
   */
  HmlBoxFormula createHmlBoxFormula();

  /**
   * Returns a new object of class '<em>Hml Recursion Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Recursion Formula</em>'.
   * @generated
   */
  HmlRecursionFormula createHmlRecursionFormula();

  /**
   * Returns a new object of class '<em>Hml Min Fix Point</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Min Fix Point</em>'.
   * @generated
   */
  HmlMinFixPoint createHmlMinFixPoint();

  /**
   * Returns a new object of class '<em>Hml Max Fix Point</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Max Fix Point</em>'.
   * @generated
   */
  HmlMaxFixPoint createHmlMaxFixPoint();

  /**
   * Returns a new object of class '<em>Hml Recursion Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Recursion Variable</em>'.
   * @generated
   */
  HmlRecursionVariable createHmlRecursionVariable();

  /**
   * Returns a new object of class '<em>Ltl Formula Declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Formula Declaration</em>'.
   * @generated
   */
  LtlFormulaDeclaration createLtlFormulaDeclaration();

  /**
   * Returns a new object of class '<em>Ltl Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Formula</em>'.
   * @generated
   */
  LtlFormula createLtlFormula();

  /**
   * Returns a new object of class '<em>Ltl Atomic Proposition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Atomic Proposition</em>'.
   * @generated
   */
  LtlAtomicProposition createLtlAtomicProposition();

  /**
   * Returns a new object of class '<em>Ltl Next Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Next Formula</em>'.
   * @generated
   */
  LtlNextFormula createLtlNextFormula();

  /**
   * Returns a new object of class '<em>Ltl Always Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Always Formula</em>'.
   * @generated
   */
  LtlAlwaysFormula createLtlAlwaysFormula();

  /**
   * Returns a new object of class '<em>Ltl Eventually Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Eventually Formula</em>'.
   * @generated
   */
  LtlEventuallyFormula createLtlEventuallyFormula();

  /**
   * Returns a new object of class '<em>Ltl Not Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Not Formula</em>'.
   * @generated
   */
  LtlNotFormula createLtlNotFormula();

  /**
   * Returns a new object of class '<em>Ltl True</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl True</em>'.
   * @generated
   */
  LtlTrue createLtlTrue();

  /**
   * Returns a new object of class '<em>Ltl False</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl False</em>'.
   * @generated
   */
  LtlFalse createLtlFalse();

  /**
   * Returns a new object of class '<em>Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Label Predicate</em>'.
   * @generated
   */
  LabelPredicate createLabelPredicate();

  /**
   * Returns a new object of class '<em>Not Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Not Label Predicate</em>'.
   * @generated
   */
  NotLabelPredicate createNotLabelPredicate();

  /**
   * Returns a new object of class '<em>Action Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Action Label Predicate</em>'.
   * @generated
   */
  ActionLabelPredicate createActionLabelPredicate();

  /**
   * Returns a new object of class '<em>Any Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Any Label Predicate</em>'.
   * @generated
   */
  AnyLabelPredicate createAnyLabelPredicate();

  /**
   * Returns a new object of class '<em>Interleaving</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Interleaving</em>'.
   * @generated
   */
  Interleaving createInterleaving();

  /**
   * Returns a new object of class '<em>Synchronization</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Synchronization</em>'.
   * @generated
   */
  Synchronization createSynchronization();

  /**
   * Returns a new object of class '<em>Controlled Interaction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Controlled Interaction</em>'.
   * @generated
   */
  ControlledInteraction createControlledInteraction();

  /**
   * Returns a new object of class '<em>Renaming</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Renaming</em>'.
   * @generated
   */
  Renaming createRenaming();

  /**
   * Returns a new object of class '<em>Hml Or Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml Or Formula</em>'.
   * @generated
   */
  HmlOrFormula createHmlOrFormula();

  /**
   * Returns a new object of class '<em>Hml And Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Hml And Formula</em>'.
   * @generated
   */
  HmlAndFormula createHmlAndFormula();

  /**
   * Returns a new object of class '<em>Ltl Or Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Or Formula</em>'.
   * @generated
   */
  LtlOrFormula createLtlOrFormula();

  /**
   * Returns a new object of class '<em>Ltl And Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl And Formula</em>'.
   * @generated
   */
  LtlAndFormula createLtlAndFormula();

  /**
   * Returns a new object of class '<em>Ltl Until Formula</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ltl Until Formula</em>'.
   * @generated
   */
  LtlUntilFormula createLtlUntilFormula();

  /**
   * Returns a new object of class '<em>Cup Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cup Label Predicate</em>'.
   * @generated
   */
  CupLabelPredicate createCupLabelPredicate();

  /**
   * Returns a new object of class '<em>Cap Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Cap Label Predicate</em>'.
   * @generated
   */
  CapLabelPredicate createCapLabelPredicate();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  ComposedLtsPackage getComposedLtsPackage();

} //ComposedLtsFactory

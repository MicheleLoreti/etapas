package org.cmg.tapas.graph;


import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.TrueFilter;

/** 
 * Graph rappresenta un grafo etichettato sugli archi. <p>
 * Le etichette sugli archi sono dette anche azioni. <p>
 * La classe � parametrica rispetto al tipo S degli stati e
 * e rispetto al tipo A delle azioni. <p>
 * E' possibile inserire e rimuovere stati o archi, 
 * avere informazioni sugli stati e gli archi presenti e 
 * conoscere l'immagine (postset) o la retroimmagine (preset) 
 * di uno stato rispetto a una azione.
 * <p>
 * <p>
 * Invariante:											<p>
 *  - i nomi degli stati devono essere tutti diversi	<p>
 * <p>
 * <p>
 * @param <S> tipo degli stati
 * @param <A> tipo delle azioni
 * 
 * @author Guzman Tierno
 * @author Michele Loreti
 **/
public class Graph0<
	S extends StateInterface,
	A extends ActionInterface
> implements GraphInterface<S,A> { 
	

	/** Mappa stati-nomi **/
	private HashMap< String, S > states = new HashMap< String, S >();
	/** Mappa dei presets: stato->azione->preset **/
	private HashMap< S, HashMap<A, TreeSet<S> >> inEdges =
		new HashMap< S, HashMap<A, TreeSet<S> >>();	
	/** Mappa dei postsets: stato->azione->postset **/
	private HashMap< S, HashMap<A, TreeSet<S> >> outEdges =
		new HashMap< S, HashMap<A, TreeSet<S> >>();	
	/** Mappa del numero di volte con cui � usata ogni azione. **/
	private HashMap< A, Integer> actionMap = new HashMap<A, Integer>();

	/** Numero di archi **/
	private int edgeCount = 0;
	
	/** 
	 * Azione tau, null se non presente. 
	 * Si assume esista una sola azione tau di tipo A.
	 **/	
    // OPTION: getTauAction()
	private A tauAction;
	
	/** Confrontatore di stati. **/
	private final StateNameComparator<S> stateComparator = 
		new StateNameComparator<S>();
	
// _____________________________________________________________________________
// States: get
		
	/**
	 * Rende il numero di stati del grafo.
     * 
     * @return Numero di stati del grafo.
	 * @see org.cmg.tapas.graph.GraphData#numStates()
	 */
	public int numStates() {
		return states.size();
	}
	
	/**
	 * Rende un nuovo nome non ancora usato da nessuno stato
	 * del grafo.
     * 
     * @return Nome non usato da nessuno stato del grafo.
	 * @see org.cmg.tapas.graph.GraphInterface#getNewStateName()
	 */
	public String getNewStateName() {
		int count = states.size();
  		String s = Integer.toString(count);
  		while( states.containsKey(s) ) {
  			++count;	
  			s = Integer.toString(count);
  		}  		
  		return s;
	}
	
	/**
	 * Rende lo stato il cui nome coincide 
	 * con la stringa passata come parametro 
	 * oppure rende <code>null</code> se un tale stato non 
     * � presente nel grafo.
     * 
     * @param name Nome dello stato da ricercare.
     * @return Lo stato col nome specificato.
	 * @see org.cmg.tapas.graph.GraphInterface#getState(java.lang.String)
	 */
	public S getState(String name){
		return states.get(name);
	}

	/**
	 * Rende un iteratore per l'insieme degli stati del grafo.
	 * Il metodo � da preferire a <tt>getAllStates().iterator()</tt>
	 * in quanto evita di dover generare la collezione 
	 * degli stati e scorre invece direttamente la struttura interna
	 * che memorizza gli stati.
     * 
     * @return Un iteratore per gli stati del grafo.
	 * @see org.cmg.tapas.graph.GraphData#getStatesIterator()
	 */
	public Iterator<S> getStatesIterator(){
		return new Iterator<S>(){
			Iterator<Map.Entry<String,S>> iterator = 
                states.entrySet().iterator();
			S current = null;				
			public boolean hasNext() {
				return iterator.hasNext();				
			}			
			public S next() {
				current = iterator.next().getValue();
				return current;
			}
			public void remove() {
				iterator.remove();
				removeAllEdge(current);
			}
		};
	}	

	/**
	 * Rende una lista con gli stati del grafo che soddisfano
	 * il filtro passato come parametro. 
	 * Qualora possibile si consiglia l'uso di getStatesIterator()
	 * che risulta pi� efficiente.
     * 
     * @param stateFilter Filtro per gli stati.
     * @return Lista con gli stati del grafo che soddisfano il 
     * filtro.
	 * @see org.cmg.tapas.graph.GraphData#getStates(org.cmg.tapas.graph.filter.Filter)
	 */
	public LinkedList<S> getStates(Filter<S> stateFilter){
		Collection<S> statesCollection = states.values();
		LinkedList<S> list = new LinkedList<S>(); 
		for( S state : statesCollection ) {
			if( stateFilter.check(state) ) 
				list.add(state);
		}
		return list;
	}

	/**
	 * Rende una lista con gli stati del grafo.
	 * Qualora possibile si consiglia l'uso di getStatesIterator()
	 * che risulta pi� efficiente.
	 **/
	public LinkedList<S> getAllStates(){
		return getStates( new TrueFilter<S>() );
	}	
	
	
// _____________________________________________________________________________
// States: is in

	/**
	 * Dice se lo stato passato come parametro
	 * appartiene al grafo.
	 **/	
	public boolean stateIsIn(S state){
		return states.get( state.getId() ) == state;
	}
	
	/**
	 * Dice se la stringa passata come parametro
	 * � usata come nome da uno stato del grafo.
	 **/
	public boolean stateIsIn(String str){
		return states.containsKey(str);
	}

// _____________________________________________________________________________
// States: add-remove

	/**
	 * Aggiunge un nuovo stato al grafo. 
	 * Il metodo fallisce se uno stato col nome
	 * dello stato passato come argomento � gi� presente.
	 **/
	public void addState(S state) {
		internalAdd(state);
	}

	private boolean internalAdd(S state) {
		if( states.containsKey(state.getId()) )
			return false;			
		states.put(state.getId(), state);
		return true;
	}
	
	/**
	 * Rimuove lo stato passato come parametro.
	 * Rende true se l'operazione ha successo.
	 **/
	public boolean removeState(S state){
		if( !stateIsIn(state) )
			return false;

		if( states.remove(state.getId())==null )
			return false;

		removeAllEdge(state);
		return true;
	}

// _____________________________________________________________________________
// Edges: get
	
	/**
	 * Rende il numero di archi del grafo.
	 **/	
	public int numEdges() {
		return edgeCount;
	}
	
	/** 
	 * Rende il numero di archi etichettati con l'azione
	 * specificata. 
	 **/
	public int getEdgeCount(A action) {
		Integer count = actionMap.get(action);
		return count==null ? 0 : count;
	}
	
	/**
	 * Rende la molteplicit� di un arco.
	 **/
	public int getMultiplicity(S src, A action, S dest) {
		throw new UnsupportedOperationException();
	}

	
// _____________________________________________________________________________
// Edges: is in
	
	/**
	 * Dice se nel grafo vi � un arco tra gli stati passati
	 * come parametri etichettato con l'azione passata come parametro.
	 **/
	public boolean edgeIsIn(S from, A action, S to) {
		Set<S> preset = getPreset( to, action );
		
		return preset!=null && preset.contains( from );
	}

	/**
	 * Dice se nel grafo vi � un arco tra gli stati passati
	 * come parametri.
	 **/
	public boolean edgeIsIn(S from, S to) {
		Map<A, ? extends Set<S>> postsetMap = outEdges.get(from);
		if( postsetMap == null )
			return false;
		
		Set<A> actions = postsetMap.keySet();
		Set<S> postset;
		for (A a : actions) {
			postset = postsetMap.get(a);
			if (postset != null) {
				if (postset.contains(to)) {
					return true;
				}
			}
		}
		
		return false;
	}


// _____________________________________________________________________________
// Edges: add-remove


	/**
	 * Aggiunge un nuovo arco al grafo.
	 * Se uno o entrambe gli estremi dell'arco non fanno
	 * parte del grafo, questi vengono aggiunti al grafo stesso.
	 * Il metodo fallisce se l'arco c'� gi� o se
	 * non � stato possibile aggiungerlo perch� uno dei suoi
	 * estremi non fa parte del grafo e un altro stato
	 * con lo stesso nome � gi� presente.
	 **/	
	public boolean addEdge(S src, A action, S dest){
		return internalAdd(src, action, dest);
	}

	private boolean internalAdd(S src, A action, S dest){
		if( edgeIsIn(src, action, dest) )
			return false;
		
		if( !stateIsIn(src) )
			if( !internalAdd(src) )
				return false;		

		if( !stateIsIn(dest) )
			if( !internalAdd(dest) )
				return false;		
		
		createPreset(dest, action).add(src);	// created, not empty
		createPostset(src, action).add(dest);	// created, not empty
		Integer count = actionMap.get(action);
		actionMap.put( action, count==null ? 1 : count+1 );
		if( action.isTau() )
			tauAction = action;
		
		edgeCount++;
		return true;
	}
	

	
	/**
	 * Rimuove l'arco solo dalla mappa della sorgente.
	 * Per mantenere l'informazione corretta il chiamante
	 * deve rimuovere l'arco dalla mappa della destinazione.
	 **/
	private boolean removeHalfOutgoingEdge(S src, A action, S dest) {
		Set<S> postset = getPostset(src, action);
		if( postset==null )
			return false;		
		
		if( postset.remove(dest) ) {
			if( postset.isEmpty() )
				outEdges.get(src).remove(action);
			return true;
		}

		return false;
	}
	
	/**
	 * Rimuove l'arco solo dalla mappa della destinazione.
	 * Per mantenere l'informazione corretta il chiamante
	 * deve rimuovere l'arco dalla mappa della sorgente.
	 **/
	private boolean removeHalfIncomingEdge(S src, A action, S dest) {
		Set<S> preset = getPreset(dest, action);
		if( preset==null )
			return false;		
		
		if( preset.remove(src) ) {
			if( preset.isEmpty() )
				inEdges.get(dest).remove(action);
			return true;
		}

		return false;
	}
	
	/**
	 * Rimuove un arco dal grafo. Rende true se l'operazione ha successo.
	 **/	
	public boolean removeEdge(S src, A action, S dest){
		boolean res = true;
		res &= removeHalfIncomingEdge(src, action, dest);
		res &= removeHalfOutgoingEdge(src, action, dest);
		
		if( res==true ) {
			edgeCount--;
			int actionCount = actionMap.get(action);
			if( actionCount==1 ) {
				actionMap.remove(action);				
				if( action.isTau() )
					tauAction = null;
			} else {
				actionMap.put( action, actionCount-1 );				
			}
		}
		
		return res;
	}
	
	
	/**
	 * Rimuove tutti gli archi entranti nello stato passato come parametro. 
	 * Rende il numero di archi rimossi.
	 **/
	public int removeIncomingEdge(S dest){
		Map<A, ? extends Set<S>> presetMap = inEdges.get(dest);
		if( presetMap==null ) 
			return 0;
	
		int actionCount;		
		int count = 0;
		Set<S> preset;		
		for( A action: presetMap.keySet() ) {
			preset = presetMap.get(action);
			count += preset.size();
			for( S src: preset ) {			// remove from src map
				removeHalfOutgoingEdge(src, action, dest);
				actionCount = actionMap.get(action);
				if( actionCount==1 ) {
					actionMap.remove(action);
					if( action.isTau() )
						tauAction = null;
				} else {
					actionMap.put( action, actionCount-1 );
				}
			}	
		}
		inEdges.remove(dest);				// removeAll from dest map
		
		edgeCount -= count;
		return count;
	}
	
	/**
	 * Rimuove tutti gli archi uscenti dallo stato passato come parametro. 
	 * Rende il numero di archi rimossi.
	 **/
	public int removeOutgoingEdge(S src){
		Map<A, ? extends Set<S>> postsetMap = outEdges.get(src);
		if( postsetMap==null ) 
			return 0;
			
		int actionCount;		
		int count = 0;
		Set<S> postset;		
		for( A action: postsetMap.keySet() ) {
			postset = postsetMap.get(action);
			count += postset.size();
			for( S dest: postset ) {	// remove from dest map
				removeHalfIncomingEdge(src, action, dest);
				actionCount = actionMap.get(action);
				if( actionCount==1 ) {
					actionMap.remove(action);
					if( action.isTau() )
						tauAction = null;
				} else {
					actionMap.put( action, actionCount-1 );
				}
			}	
		}
		outEdges.remove(src);			// removeAll from src map
		
		edgeCount -= count;
		return count;
	}
	
	/**
	 * Rimuove tutti gli archi uscenti ed entranti nello
	 * stato passato come parametro.
	 * Rende il numero di archi rimossi.
	 **/
	public int removeAllEdge(S state){
		return removeIncomingEdge(state) + removeOutgoingEdge(state);
	}

	
// _____________________________________________________________________________
// Actions

	/**
     * Rende un insieme con le azioni del grafo. 
     **/
    public Set<A> getActionSet() {
    	return actionMap.keySet();
    }

   
// _____________________________________________________________________________
// Pre/post
	
	/**
	 * Rende la mappa dei preset di uno stato 'state': cio�
	 * la mappa che associa ad ogni azione 'a'
	 * la retroimmagine di 'state' sotto l'azione 'a'.
	 * Nel caso che 'state' non abbia nessun arco entrante
	 * il metodo rende null.
	 **/
	public Map<A, ? extends Set<S>> getPresetMap(S state) {
		return inEdges.get( state );
	}

	/**
	 * Rende la mappa dei postset di uno stato 'state': cio�
	 * la mappa che associa ad ogni azione 'a'
	 * l'immagine di 'state' sotto l'azione 'a'.
	 * Nel caso che 'state' non abbia nessun arco uscente
	 * il metodo rende null.
	 **/
	public Map<A, ? extends Set<S>> getPostsetMap(S state) {
		return outEdges.get( state );
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Graph0<S,A> clone() {
		return null;
	}
	
	/** 
	 * Classe usata per rendere valori che sono coppie.
	 * Si � preferito implementare Map.Entry piuttosto che  
	 * creare una classe ad hoc perch� Map.Entry era gi� usata
	 * in vari punti.
	 **/
	private static class Entry<A1,S1> implements Map.Entry<A1,S1> {
    	private A1 key;
    	private S1 value;
    	
    	Entry(A1 key, S1 value) {
    		this.key = key;
    		this.value = value;
    	}
    	/** Rende la chiave di questa entry **/
		public A1 getKey() {
			return key;
		}
        /** Rende il valore di questa entry **/
		public S1 getValue() {
			return value;
		}
        /** Non supportato. **/
		public S1 setValue(S1 value) {
			throw new UnsupportedOperationException();
		}
    }

	/** Iteratore per le transizioni di uno stato. **/
	private class ImageIterator implements Iterator<Map.Entry<A,S>> {
		private Iterator<Map.Entry<A, TreeSet<S>>> imageIter = null; 
		private Iterator<S> postIter = null; 
		private S state = null;
		private A nextAction = null;
		private S nextState = null;
		private EdgeFilter<S,A> filter;

		/** 
		 * Costruisce un ImageIterator per le transizioni
		 * dello stato specifiacto. In uscita se 'post' vale true,
		 * in entrata altrimenti
		 **/
		ImageIterator(S state, boolean post, EdgeFilter<S,A> filter) {	
			this.filter = filter;
			this.state = state;
			Map<A, TreeSet<S>> map = 
				post ? outEdges.get( state ) : inEdges.get( state );
				
			if( map!=null )
				imageIter = map.entrySet().iterator();
				
			if( imageIter!=null ) {
				nextAction();
				internalNext();
			}
		}				
		
		private void nextAction() {
			Map.Entry<A,? extends Set<S>> t = imageIter.next();
			postIter = t.getValue().iterator();
			nextAction = t.getKey();
		}
		
		private void internalNext() {			
			if( imageIter==null )
				return;
						
			while( true ) {
				while( postIter.hasNext() ) {
					nextState = postIter.next();
					if( filter.check(state, nextAction, nextState) )
						return;
				}
				nextState = null;
				nextAction = null;
				
				if( !imageIter.hasNext() )
					return;
				
				nextAction();					
			}
		}
		
		public boolean hasNext() {				
			return nextState!=null;				
		}			

		public Map.Entry<A,S> next() {
			if( !hasNext() )
				throw new NoSuchElementException("ImageIterator.next()");
				
			Entry<A,S> entry = new Entry<A,S>(nextAction,nextState);
			internalNext();					
			return entry;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
	

	/**
     * Rende un iteratore per le transizioni dello stato specificato.
     * Le transizioni considerate sono quelle uscenti se 'post'
     * � true, quelle entranti altrimenti.
     **/	
    public Iterator<Map.Entry<A,S>> getImageIterator(
    	S state, boolean post, EdgeFilter<S,A> filter
    ) {
		return new ImageIterator(state, post, filter);
    }

	/**
	 * Rende il preset di uno stato sotto una azione.
	 * Se lo stato non ha archi entranti con tali azione
	 * pu� rendere null.
	 **/
	public Set<S> getPreset(S state, A action) {
		Map<A, ? extends Set<S>> presetMap = inEdges.get(state);		
		if( presetMap == null )
			return null;
			
		return presetMap.get(action);
	}

	/**
	 * Rende il postset di uno stato sotto una azione.
	 * Se lo stato non ha archi uscenti con tali azione
	 * pu� rendere null.
	 **/
	public Set<S> getPostset(S state, A action) {
		Map<A, ? extends Set<S>> postsetMap = outEdges.get(state);
		if( postsetMap == null )
			return null;
			
		return postsetMap.get( action );
	}


	/** Crea, se necessario, e rende la mappa dei preset di uno stato **/
	private HashMap<A, TreeSet<S>> createPresetMap(S state) {
		HashMap<A, TreeSet<S>> presetMap = inEdges.get(state);
		if( presetMap==null ) {
			presetMap = new HashMap<A, TreeSet<S>>();
			inEdges.put( state, presetMap );
		}
		return presetMap;
	}
	

	/** Crea, se necessario, e rende il preset di uno stato per una azione **/
	private TreeSet<S> createPreset(S state, A action) {
		HashMap<A, TreeSet<S>> presetMap = createPresetMap(state);
		TreeSet<S> preset = presetMap.get(action);
		if( preset==null ) {
			preset = new TreeSet<S>(stateComparator);
			presetMap.put( action, preset );
		}
		return preset;
	}

	/** Crea, se necessario, e rende la mappa dei postset di uno stato **/
	private HashMap<A, TreeSet<S>> createPostsetMap(S state) {
		HashMap<A, TreeSet<S>> postsetMap = outEdges.get(state);
		if( postsetMap==null ) {
			postsetMap = new HashMap<A, TreeSet<S>>();
			outEdges.put( state, postsetMap );
		}
		return postsetMap;
	}

	/** Crea, se necessario, e rende il postset di uno stato per una azione **/
	private TreeSet<S> createPostset(S state, A action) {
		HashMap<A, TreeSet<S>> postsetMap = createPostsetMap(state);
		TreeSet<S> postset = postsetMap.get(action);
		if( postset==null ) {
			postset = new TreeSet<S>(stateComparator);
			postsetMap.put( action, postset );
		}
		return postset;
	}

	/** Rende true se lo stato specificato non ha archi uscenti. **/
	public boolean isFinal(S state) {
		return outEdges.get(state) == null;
	}


// _____________________________________________________________________________
// print, toString

	/**
	 * Stampa gli stati del grafo.
	 **/	
	public void printStates() {
		System.out.println( "Stati: [ \n" );		
		for( String name : states.keySet() ) {
			System.out.println( " " + name );					
		}
		System.out.println("]");
	}
	
	
	/**
	 * D� una descrizione del grafo.
	 **/
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append( "Stati: " + numStates() + "\n" );
		s.append( "Archi: " + numEdges() + "\n"  );
		Iterator<S> iterator1 = getStatesIterator();		
		Iterator<S> iterator2;		
		while( iterator1.hasNext() ) {
			S state1 = iterator1.next();
			iterator2 = getStatesIterator();		
			while( iterator2.hasNext() ) {
				S state2 = iterator2.next();
				for( A action: getActionSet() ) {
					if( edgeIsIn(state1, action, state2) )
						s.append( state1 + " " + action + " " + state2 + "\n" );
				}
			}
		}
		
		return s.toString();
	}

	public int numEdges(boolean multiplicity) {
		// TODO In questa classe non e' implementata la molteplicita'.
		// TOTO Non mi e' chiaro pero' se questa classe serva o e' un revuso. (ML)
		return 0;
	}
	
	public Set<S> getPostset(S state) {
		TreeSet<S> toReturn = new TreeSet<S>();
		Map<A, ? extends Set<S>> postsetMap = outEdges.get(state);
		Set<A> actions = postsetMap.keySet();
		if( postsetMap == null )
			return toReturn;
		
		Set<S> postset;
		for (A a : actions) {
			postset = postsetMap.get(a);
			if (postset != null) {
				toReturn.addAll(postset);
			}
		}
		
		return toReturn;
	}

	public Set<S> getPostset(S state, Filter<A> filter) {
		TreeSet<S> toReturn = new TreeSet<S>();
		Map<A, ? extends Set<S>> postsetMap = outEdges.get(state);
		Set<A> actions = postsetMap.keySet();
		if( postsetMap == null )
			return toReturn;
		
		Set<S> postset;
		for (A a : actions) {
			if (filter.check(a)) {
				postset = postsetMap.get(a);
				if (postset != null) {
					toReturn.addAll(postset);
				}
			}
		}
		
		return toReturn;
	}



} 

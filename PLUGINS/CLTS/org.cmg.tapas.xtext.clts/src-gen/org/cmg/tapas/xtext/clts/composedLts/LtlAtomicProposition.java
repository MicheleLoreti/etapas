/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ltl Atomic Proposition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition#getProp <em>Prop</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtlAtomicProposition()
 * @model
 * @generated
 */
public interface LtlAtomicProposition extends LtlFormula
{
  /**
   * Returns the value of the '<em><b>Prop</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prop</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prop</em>' reference.
   * @see #setProp(Label)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtlAtomicProposition_Prop()
   * @model
   * @generated
   */
  Label getProp();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition#getProp <em>Prop</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prop</em>' reference.
   * @see #getProp()
   * @generated
   */
  void setProp(Label value);

} // LtlAtomicProposition

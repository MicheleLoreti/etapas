/**
 */
package org.lic.secsi;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Program#getGlobalchanels <em>Globalchanels</em>}</li>
 *   <li>{@link org.lic.secsi.Program#getAgents <em>Agents</em>}</li>
 *   <li>{@link org.lic.secsi.Program#getTypes <em>Types</em>}</li>
 *   <li>{@link org.lic.secsi.Program#getConsts <em>Consts</em>}</li>
 *   <li>{@link org.lic.secsi.Program#getSystem <em>System</em>}</li>
 *   <li>{@link org.lic.secsi.Program#getProperties <em>Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getProgram()
 * @model
 * @generated
 */
public interface Program extends EObject
{
  /**
   * Returns the value of the '<em><b>Globalchanels</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.ChannelsDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Globalchanels</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Globalchanels</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getProgram_Globalchanels()
   * @model containment="true"
   * @generated
   */
  EList<ChannelsDeclaration> getGlobalchanels();

  /**
   * Returns the value of the '<em><b>Agents</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.Agent}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Agents</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Agents</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getProgram_Agents()
   * @model containment="true"
   * @generated
   */
  EList<Agent> getAgents();

  /**
   * Returns the value of the '<em><b>Types</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.TypeDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Types</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Types</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getProgram_Types()
   * @model containment="true"
   * @generated
   */
  EList<TypeDef> getTypes();

  /**
   * Returns the value of the '<em><b>Consts</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.Constant}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Consts</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Consts</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getProgram_Consts()
   * @model containment="true"
   * @generated
   */
  EList<Constant> getConsts();

  /**
   * Returns the value of the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System</em>' containment reference.
   * @see #setSystem(org.lic.secsi.System)
   * @see org.lic.secsi.SecsiPackage#getProgram_System()
   * @model containment="true"
   * @generated
   */
  org.lic.secsi.System getSystem();

  /**
   * Sets the value of the '{@link org.lic.secsi.Program#getSystem <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System</em>' containment reference.
   * @see #getSystem()
   * @generated
   */
  void setSystem(org.lic.secsi.System value);

  /**
   * Returns the value of the '<em><b>Properties</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Properties</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Properties</em>' containment reference.
   * @see #setProperties(PropList)
   * @see org.lic.secsi.SecsiPackage#getProgram_Properties()
   * @model containment="true"
   * @generated
   */
  PropList getProperties();

  /**
   * Sets the value of the '{@link org.lic.secsi.Program#getProperties <em>Properties</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Properties</em>' containment reference.
   * @see #getProperties()
   * @generated
   */
  void setProperties(PropList value);

} // Program

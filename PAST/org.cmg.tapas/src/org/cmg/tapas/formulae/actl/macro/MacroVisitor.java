/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface MacroVisitor<S extends APProcess<S, A>, A extends ActionInterface> {
	
	public void visitAndMacro( AndMacro<S, A> macro );
	public void visitConstant( Constant<S, A> macro);
	public void visitHole( Hole<S,A> macro );
	public void visitNotMacro( NotMacro<S, A> macro);
	public void visitOrMacro( OrMacro<S, A> macro);
	public void visitQuantificationMacro( QuantificationMacro<S, A> macro );
	public void visitDiamondMacro(DiamondMacro<S, A> macro);

}

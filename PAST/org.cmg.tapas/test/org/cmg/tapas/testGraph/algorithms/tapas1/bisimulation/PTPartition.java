package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.State;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;


/**
 * La classe PTPartition rappresenta una partizione di un insieme 
 * di stati di tipo <tt>PTState</tt>.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class PTPartition {
	
	/**
	 * Rappresenta la partizione come insieme di insiemi di stati.
	 **/
	private Vector<PTStatesSet> partition = null;

	/**
	 * Costruisce una nuova partizione vuota.
	 **/
	public PTPartition() {
		partition = new Vector<PTStatesSet>();
	}

	/**
	 * Costruisce una nuova partizione inizializzata con il solo insieme 
	 * di stati passato in ingresso.
	 **/		
	public PTPartition(PTStatesSet ss) {
		partition = new Vector<PTStatesSet>();
		partition.addElement(ss);
	}

	/**
	 * Restituisce il numero di elementi contenuti nell'insieme.
	 **/  	
  	public int getDimension() {
  		return partition.size();
  	}

	/**
	 * Aggiunge all'insieme un oggetto di tipo <tt>PTStatesSet</tt>
	 * inserendolo in fondo all'insieme.
	 **/
	public void addPTStatesSet(PTStatesSet ss) {
		this.partition.addElement(ss);	
	}

	/**
	 * Rimuove dall'insieme il primo insieme di stati. 
	 **/		
	public PTStatesSet removeFirstPTStatesSet() {
		return partition.remove(0);	
	}	

	/**
	 * Restituisce l'insieme di stati contenuto nella posizione 
	 * specificata come parametro di ingresso. 
	 **/	
	public PTStatesSet ptstatesSetAt(int index) {
		return partition.elementAt(index);	
	}
	
	/** 
	 *
	 **/
	public PTState getPTState(State s) {
		PTState ptstate = null;		
		for (int i=0; i<this.getDimension(); i++) {
			ptstate = this.ptstatesSetAt(i).getPTState(s);
			if (ptstate != null) return ptstate;
		}
		
		return null;
	}

	/**
	 * Controlla se la partizione � vuota. 
	 **/	
	public boolean isEmpty() {
		return this.partition.isEmpty();	
	}	

	/**
	 * Controlla se i due stati passati come parametri appartengono 
	 * allo stesso insieme all'interno
	 * della partizione. 
	 **/
	public boolean checkSameClass(PTState s1, PTState s2) {
		PTStatesSet ptstatesSet = null;
		int trovato = 0;
		for (int i=0; i<this.getDimension(); i++) {
			ptstatesSet = this.ptstatesSetAt(i);
			trovato = 0;
			if (ptstatesSet.containPTState(s1)) trovato++;
			if (ptstatesSet.containPTState(s2)) trovato++;
			if (trovato == 2) return true;
			else if (trovato == 1) return false;			
		}
		
		return false;
	}

	/**
	 * Converte questo oggetto in un oggetto di tipo <tt>Partition</tt>. 
	 **/ 
	public Partition toPartition() {
		Partition ro = new Partition();
		PTStatesSet ptstatesSet = null;
		StatesSet statesSet = null;
		for (int i=0; i<this.getDimension(); i++) {
			ptstatesSet = this.ptstatesSetAt(i);
			statesSet = new StatesSet();
			for (int j=0; j<ptstatesSet.getDimension(); j++)
				statesSet.addState(ptstatesSet.ptstateAt(j).getState());
			ro.addStatesSet(statesSet);
		}

		return ro;	
	}

	/**
	 * Stampa l'intera partizione.
	 **/ 
	public void print() {
		int i = 0;
		System.out.print("{");
		for (i=0; i<this.getDimension(); i++) {
			if (i>0) System.out.print(", ");
			partition.elementAt(i).print();
		}
		if (i==0) System.out.print("empty");
		System.out.print("}");
	} 
	
}
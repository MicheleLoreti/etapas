/**
 * 
 */
package org.cmg.tapas.formulae.lmc;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface LogicalFormula<S extends APProcess<S, A>, A extends ActionInterface> {

	String getName();
	void setName( String name );
	String getUnicode();
	boolean satisfies( S s );
	Proof<S,A> getProof( S s );

}

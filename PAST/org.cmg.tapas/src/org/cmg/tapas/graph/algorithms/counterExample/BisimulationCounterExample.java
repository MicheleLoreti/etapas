package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.cmg.tapas.formulae.hml.False;
import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.formulae.hml.True;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.GraphData;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.processAlgebra.APProcess;


public class BisimulationCounterExample 
		<S extends APProcess<S, A>
		, A extends ActionInterface> 
	extends AbstractCounterExample<S, A> {

	protected FormulaeFactory<S, A> Ffactory;
	private Evaluator<S, ?> evaluator;
	protected HashMap<Object, Next> nextMap;
	
	public BisimulationCounterExample(GraphData<S,A> graph, boolean strong, A tauAct, Evaluator<S, ?> e){
		this.Ffactory = new FormulaeFactory<S, A>(strong, tauAct);
		this.evaluator = e;
		initNextMap(graph);
	}
	

	private void initNextMap(GraphData<S, A> graph){
		nextMap = new HashMap<Object, Next>();
		LinkedList<S> statesList = graph.getAllStates();
		for (S s : statesList) {
			Object o = evaluator.eval(s);
			Next next = nextMap.get(o); 
			if(next == null){
				next = new Next();
				nextMap.put(o, next);
			}
			
			Map<A, ? extends Set<S>> psMap = graph.getPostsetMap(s);
			if(psMap != null){
				for (A act : psMap.keySet()) {
					next.add(act, psMap.get(act));
				}
			}
		}	
		
		//Inizializza la tabella
		curr_formulae = new TableFormulae<S, A>(new HashSet<Object>(nextMap.keySet()));
		old_formulae = new TableFormulae<S, A>(new HashSet<Object>(nextMap.keySet()));
	}
	

	private LinkedList<HmlFormula<S,A>> getDiamond(Set<Object> diff, A act, Set<Object> dest) {
		LinkedList<HmlFormula<S,A>> res = new LinkedList<HmlFormula<S,A>>();
		for (Object Bh : diff) {
			LinkedList<LinkedList<HmlFormula<S, A>>> oldFormulae = new LinkedList<LinkedList<HmlFormula<S,A>>>();
			boolean allFound = true;
			
			for (Object Bk : dest) {
				LinkedList<HmlFormula<S,A>> tmp = old_formulae.getFormulae(Bh, Bk);
				if(tmp == null || tmp.size() == 0){
					allFound = false;
					break;				
				}
				oldFormulae.add(tmp);
			}
			
			if(allFound){
				res.addAll(Ffactory.createDiamond(act, oldFormulae));
			}
		}
		return res;
	}


	private LinkedList<HmlFormula<S, A>> getBox(Set<Object> dest, A act, Set<Object> diff) {
		LinkedList<HmlFormula<S, A>> res = new LinkedList<HmlFormula<S,A>>();
		LinkedList<LinkedList<HmlFormula<S, A>>> andList = new LinkedList<LinkedList<HmlFormula<S, A>>>();
		for (Object Bh : dest) {
			LinkedList<LinkedList<HmlFormula<S, A>>> oldFormulae = new LinkedList<LinkedList<HmlFormula<S,A>>>();
			for (Object Bk : diff) {
				LinkedList<HmlFormula<S,A>> tmp = old_formulae.getFormulae(Bh, Bk);
				if(tmp == null || tmp.size() == 0)
					return null;
				oldFormulae.add(tmp);
			}	
			if(oldFormulae.size() > 0)
				andList.add(Ffactory.createAnd(oldFormulae));
		}

		if(andList.size() > 0)
			res.addAll(Ffactory.createBox(act, andList));
		return res;
	}


	protected void init(){
		LinkedList<Object> blockList = old_formulae.getPartitionList();	
		for (int i = 0; i < blockList.size(); i++) {
			Set<A> l1 = nextMap.get(blockList.get(i)).getActions();
			for (int j = i+1; j < blockList.size(); j++) {
				Set<A> l2 = nextMap.get(blockList.get(j)).getActions();
				actions(i, l1, j, l2);
			}
		}			
	}


	protected void actions(int i, Set<A> l1, int j, Set<A> l2){
		//Azioni che possono essere fatte solamente dalla prima partizione
		Set<A> actSet = new TreeSet<A>(l1);
		actSet.removeAll(l2);
		if(actSet.size() > 0){
			HmlFormula<S,A> diamond = Ffactory.createDiamond(actSet, new True<S, A>());			
			curr_formulae.addFormula(i, j, diamond);
			HmlFormula<S,A> box = Ffactory.createBox(actSet, new False<S, A>());
			curr_formulae.addFormula(j, i, box);
		}

		//Azioni che possono essere fatte solamente dalla seconda partizione
		actSet = new TreeSet<A>(l2);
		actSet.removeAll(l1);
		if(actSet.size() > 0){
			HmlFormula<S,A> diamond = Ffactory.createDiamond(actSet, new True<S, A>());
			curr_formulae.addFormula(j, i, diamond);
			HmlFormula<S,A> box = Ffactory.createBox(actSet, new False<S, A>());
			curr_formulae.addFormula(i, j, box);
		}
	}
	
	protected void step(Object hs1, Object hs2){
		Next next1 = nextMap.get(hs1);
		Next next2 = nextMap.get(hs2);		
		Set<A> actions = next1.getActions();
				
		for (A act : actions) {
			// postSetMap dello stato hs1 eseguendo l'azione act
			Set<Object> dest1 =  next1.getDest(act);
			
			// postSetMap dello stato hs2 eseguendo l'azione act
			Set<Object> dest2 =  next2.getDest(act);

			Set<Object> S1 = new HashSet<Object>(dest1);
			S1.removeAll(dest2);
			
			Set<Object> S2 = new HashSet<Object>(dest1);
			S2.retainAll(dest2);

			Set<Object> S3 = new HashSet<Object>(dest2);
			S3.removeAll(dest1);
			
			if(S1.contains(hs1) && S3.contains(hs2)){
				S1.remove(hs1);
				S3.remove(hs2);
			}

			LinkedList<HmlFormula<S, A>> diamond;
			LinkedList<HmlFormula<S, A>> box;
			if(S1.size() > 0){
				diamond = getDiamond(S1, act, dest2);
				if(diamond != null && diamond.size()>0)
					curr_formulae.addFormulae(hs1, hs2, diamond);
				
				box = getBox(dest2, act, S1);
				if(box != null && box.size()>0)
					curr_formulae.addFormulae(hs2, hs1, box);
			}

			if(S3.size() > 0){
				box = getBox(dest1, act, S3);
				if(box != null && box.size()>0)
					curr_formulae.addFormulae(hs1, hs2, box);
				
				diamond = getDiamond(S3, act, dest1);
				if(diamond != null && diamond.size()>0)
					curr_formulae.addFormulae(hs2, hs1, diamond);
			}
		}
	}

	@Override
	protected Object getPartition(S s) {
		return evaluator.eval(s);
	}
	
	protected class Next{ 	
		
		private HashMap<A, HashSet<Object>> nextMap;

		protected Next(){
			nextMap = new HashMap<A, HashSet<Object>>();
		}
		
		protected void add(A act, Set<S> dest){
			HashSet<Object> next = nextMap.get(act);
			if(next == null){
				next = new HashSet<Object>();
				nextMap.put(act, next);
			}
			Set<Object> newDest = new HashSet<Object>(); 
			for (S as : dest) {
				newDest.add(evaluator.eval(as));
			}
			next.addAll(newDest);
		}

		protected Set<A> getActions(){
			return nextMap.keySet(); 
		}
		
		protected Set<Object> getDest(A act){
			Set<Object> res = nextMap.get(act);
			if(res != null) 
				return res;
			else 
				return new HashSet<Object>();
		}
		
		public String toString(){
			return this.nextMap.toString();
		}
	}
}

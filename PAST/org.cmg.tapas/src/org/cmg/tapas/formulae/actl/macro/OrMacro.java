/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.state.ActlOr;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class OrMacro < S extends APProcess<S,A>,A extends ActionInterface> extends StateFormulaMacro<S, A> {
	
	private StateFormulaMacro<S, A> macroLeft;
	private StateFormulaMacro<S, A> macroRight;
	

	/**
	 * @param macroLeft
	 * @param macroRight
	 */
	public OrMacro(StateFormulaMacro<S, A> macroLeft,
			StateFormulaMacro<S, A> macroRight) {
		super();
		this.macroLeft = macroLeft;
		this.macroRight = macroRight;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ActlFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		if (f.length != parameters()) {
			throw new IllegalArgumentException();
		}
		ActlFormula<S, A>[] left = new ActlFormula[ macroLeft.parameters() ];
		ActlFormula<S, A>[] right = new ActlFormula[ macroRight.parameters() ];
		System.arraycopy(f, 0, left, 0, macroLeft.parameters());
		System.arraycopy(f, 0, right, macroLeft.parameters(), macroRight.parameters());
		
		return new ActlOr<S, A>(macroLeft.getFormula(left),macroRight.getFormula(right) );
	}

	@Override
	public int parameters() {
		return macroLeft.parameters()+macroRight.parameters();
	}

	@Override
	public void acceptVisitor(MacroVisitor<S, A> visitor) {
		visitor.visitOrMacro(this);
	}

	public StateFormulaMacro<S, A> getLeft() {
		return macroLeft;
	}

	public StateFormulaMacro<S, A> getRight() {
		return macroRight;
	}

}

package org.cmg.tapas.util;

public class DebugMode {

	private boolean debug;
	private TapasOptions opt;
		
	public DebugMode(Class<?> cl){
		opt = TapasOptions.getIstance();		 
		try {
			String str = opt.getProperty(cl.toString(), Boolean.valueOf(debug).toString());
			this.debug = Boolean.parseBoolean(str);
		} catch (Exception e) {
			//XXX optional parameter: no problem if fail
			this.debug = false; 
		}
	}
	
	public DebugMode(Class<?> cl, boolean debug){
		opt = TapasOptions.getIstance();
		this.debug = debug;
	}
	
	public void setDebug(boolean b){
		debug = b;
	}
	
	public boolean isDebug(){
		return debug;
	}
	
	public void println(String str){
		if(debug){
			System.out.println(str);
			System.out.flush();
		}
	}
	
	public void print(String str){
		if(debug){
			System.out.print(str);
			System.out.flush();
		}
	}
	
	public void println(Object o){
		if(debug)
			System.out.println(o.toString());
	}
	
	public void print(Object o){
		if(debug)
			System.out.print(o.toString());
	}
}

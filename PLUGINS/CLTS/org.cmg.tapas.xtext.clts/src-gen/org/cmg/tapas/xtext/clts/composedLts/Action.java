/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends Element
{
} // Action

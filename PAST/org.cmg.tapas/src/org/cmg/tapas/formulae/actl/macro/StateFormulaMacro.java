/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public abstract class StateFormulaMacro<S extends APProcess<S, A>, A extends ActionInterface> {

	private String name = "";
	
	public abstract ActlFormula<S, A> getFormula( ActlFormula<S,A> ... f );
	public abstract int parameters();
	public abstract void acceptVisitor( MacroVisitor<S,A> visitor);

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}

/**
 */
package org.lic.secsi;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ren</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Ren#getLeflists <em>Leflists</em>}</li>
 *   <li>{@link org.lic.secsi.Ren#getRightlist <em>Rightlist</em>}</li>
 *   <li>{@link org.lic.secsi.Ren#getR <em>R</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getRen()
 * @model
 * @generated
 */
public interface Ren extends EObject
{
  /**
   * Returns the value of the '<em><b>Leflists</b></em>' reference list.
   * The list contents are of type {@link org.lic.secsi.Channel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Leflists</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Leflists</em>' reference list.
   * @see org.lic.secsi.SecsiPackage#getRen_Leflists()
   * @model
   * @generated
   */
  EList<Channel> getLeflists();

  /**
   * Returns the value of the '<em><b>Rightlist</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Rightlist</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rightlist</em>' attribute list.
   * @see org.lic.secsi.SecsiPackage#getRen_Rightlist()
   * @model unique="false"
   * @generated
   */
  EList<String> getRightlist();

  /**
   * Returns the value of the '<em><b>R</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>R</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>R</em>' containment reference.
   * @see #setR(Res)
   * @see org.lic.secsi.SecsiPackage#getRen_R()
   * @model containment="true"
   * @generated
   */
  Res getR();

  /**
   * Sets the value of the '{@link org.lic.secsi.Ren#getR <em>R</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>R</em>' containment reference.
   * @see #getR()
   * @generated
   */
  void setR(Res value);

} // Ren

package org.cmg.tapas.testGraph;

import org.cmg.tapas.graph.ActionInterface;

/**
 * @author Francesco Calzolai
 */
public class LtsAction implements ActionInterface {

	/** Nome dell'azione. **/	
	private String name;
	
	/** L'azione silente. **/
	public static final LtsAction TAU = new LtsAction("tau");

	/** Costruisce una azione col nome specificato. **/
	public LtsAction(String name) {
		this.name = name;
	}
	
	/**
	 * Rende il nome dell'azione.
	 */
	public String getId() {
		return name;
	}
	
	/**
	 * Setta il nome dell'azione.
	 */
	public void setId(String name) {
		this.name = name;
	}
	
	/** Dice se questa azione ha lo stesso nome di quella specificata. **/
	@Override 
	public boolean equals(Object o){
		if( o==null || !(o instanceof LtsAction) )
			return false;

		LtsAction a = (LtsAction) o;
		return name.equals( a.name );
	}
	
	/** 
	 * HashCode coerente con il metodo equals. Oggetti uguali
	 * hanno lo stesso hashCode.
	 **/
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	
	/** Rende una descrizione (costituita dal nome) dell'azione. **/
	@Override
	public String toString(){
		return name;
	}
	
	/** 
	 * Dice se si tratta di una azione silente:
	 * controlla se il nome � "tau".
	 **/
	public boolean isTau() {
		return name.equals( TAU.name );
	}
	
	public int compareTo(ActionInterface action) {
		return name.compareTo( action.getId() );
	}
}

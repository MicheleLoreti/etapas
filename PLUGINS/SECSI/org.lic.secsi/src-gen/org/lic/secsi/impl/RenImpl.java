/**
 */
package org.lic.secsi.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.lic.secsi.Channel;
import org.lic.secsi.Ren;
import org.lic.secsi.Res;
import org.lic.secsi.SecsiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ren</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.lic.secsi.impl.RenImpl#getLeflists <em>Leflists</em>}</li>
 *   <li>{@link org.lic.secsi.impl.RenImpl#getRightlist <em>Rightlist</em>}</li>
 *   <li>{@link org.lic.secsi.impl.RenImpl#getR <em>R</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RenImpl extends MinimalEObjectImpl.Container implements Ren
{
  /**
   * The cached value of the '{@link #getLeflists() <em>Leflists</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeflists()
   * @generated
   * @ordered
   */
  protected EList<Channel> leflists;

  /**
   * The cached value of the '{@link #getRightlist() <em>Rightlist</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRightlist()
   * @generated
   * @ordered
   */
  protected EList<String> rightlist;

  /**
   * The cached value of the '{@link #getR() <em>R</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getR()
   * @generated
   * @ordered
   */
  protected Res r;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RenImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.REN;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Channel> getLeflists()
  {
    if (leflists == null)
    {
      leflists = new EObjectResolvingEList<Channel>(Channel.class, this, SecsiPackage.REN__LEFLISTS);
    }
    return leflists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getRightlist()
  {
    if (rightlist == null)
    {
      rightlist = new EDataTypeEList<String>(String.class, this, SecsiPackage.REN__RIGHTLIST);
    }
    return rightlist;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Res getR()
  {
    return r;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetR(Res newR, NotificationChain msgs)
  {
    Res oldR = r;
    r = newR;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecsiPackage.REN__R, oldR, newR);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setR(Res newR)
  {
    if (newR != r)
    {
      NotificationChain msgs = null;
      if (r != null)
        msgs = ((InternalEObject)r).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.REN__R, null, msgs);
      if (newR != null)
        msgs = ((InternalEObject)newR).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecsiPackage.REN__R, null, msgs);
      msgs = basicSetR(newR, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.REN__R, newR, newR));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SecsiPackage.REN__R:
        return basicSetR(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SecsiPackage.REN__LEFLISTS:
        return getLeflists();
      case SecsiPackage.REN__RIGHTLIST:
        return getRightlist();
      case SecsiPackage.REN__R:
        return getR();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SecsiPackage.REN__LEFLISTS:
        getLeflists().clear();
        getLeflists().addAll((Collection<? extends Channel>)newValue);
        return;
      case SecsiPackage.REN__RIGHTLIST:
        getRightlist().clear();
        getRightlist().addAll((Collection<? extends String>)newValue);
        return;
      case SecsiPackage.REN__R:
        setR((Res)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.REN__LEFLISTS:
        getLeflists().clear();
        return;
      case SecsiPackage.REN__RIGHTLIST:
        getRightlist().clear();
        return;
      case SecsiPackage.REN__R:
        setR((Res)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.REN__LEFLISTS:
        return leflists != null && !leflists.isEmpty();
      case SecsiPackage.REN__RIGHTLIST:
        return rightlist != null && !rightlist.isEmpty();
      case SecsiPackage.REN__R:
        return r != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (rightlist: ");
    result.append(rightlist);
    result.append(')');
    return result.toString();
  }

} //RenImpl

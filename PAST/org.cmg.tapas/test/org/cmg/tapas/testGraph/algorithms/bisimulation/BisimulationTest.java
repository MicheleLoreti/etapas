package org.cmg.tapas.testGraph.algorithms.bisimulation;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.bisimulation.ks.KSChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.ksopt.KSOChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.mpt.MPTChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.pt.PTChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.rankBased.RankBasedChecker;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.TBisimulationChecker;


/**
 * Test per gli algoritmi di bisimulazione.
 * Si tratta di un piccolo test che verifica solo
 * un grafo specifico. Per test pi� intensivi
 * si usi BisimulationTiming che oltre alle misurazioni
 * esegue una verifica dei risultati. 
 *
 * @author Guzman Tierno
 **/
public class BisimulationTest extends TestCase {
	
    private GraphInterface<LtsState,LtsAction> lts1;
    //private GraphInterface<LtsState,LtsAction> lts2;
	private LtsState s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11;
	private LtsAction a0, a1;
	
	
//	public static void main(String[] args) {
//		System.out.print("Run BisimulationTest.java");
//		junit.textui.TestRunner.run(BisimulationTest.class);
//	}
    
	
    public BisimulationTest(String s){
    	super(s);
    }

	@Override 
    protected void setUp() {  	
    	lts1 = new Graph<LtsState, LtsAction>();

    	s0  = new LtsState("0" );
    	s1  = new LtsState("1" );
    	s2  = new LtsState("2" );
    	s3  = new LtsState("3" );
    	s4  = new LtsState("4" );
    	s5  = new LtsState("5" );
    	s6  = new LtsState("6" );
    	s7  = new LtsState("7" );
    	s8  = new LtsState("8" );
    	s9  = new LtsState("9" );
    	s10 = new LtsState("10");
    	s11 = new LtsState("11");

    	a0 = new LtsAction("0");
    	a1 = new LtsAction("1");

		lts1.addState(s0 ); 
		lts1.addState(s1 ); 
		lts1.addState(s2 ); 
		lts1.addState(s3 ); 
		lts1.addState(s4 ); 
		lts1.addState(s5 ); 
		lts1.addState(s6 );		 
		lts1.addState(s7 );		
		lts1.addState(s8 ); 
		lts1.addState(s9 ); 
		lts1.addState(s10); 
		lts1.addState(s11); 
	
		lts1.addEdge(s0, a0, s0); 
		lts1.addEdge(s0, a0, s10); 
		lts1.addEdge(s1, a1, s3); 
		lts1.addEdge(s1, a0, s4); 
		lts1.addEdge(s1, a0, s6); 
		lts1.addEdge(s1, a0, s7); 
		lts1.addEdge(s1, a0, s10); 
		lts1.addEdge(s2, a0, s5); 
		lts1.addEdge(s2, a0, s7); 
		lts1.addEdge(s2, a1, s9); 
		lts1.addEdge(s2, a1, s10); 
		lts1.addEdge(s3, a1, s0); 
		lts1.addEdge(s3, a0, s2); 
		lts1.addEdge(s4, a0, s2); 
		lts1.addEdge(s4, a1, s5); 
		lts1.addEdge(s4, a1, s8); 
		lts1.addEdge(s4, a0, s9); 
		lts1.addEdge(s5, a1, s1); 
		lts1.addEdge(s5, a0, s3); 
		lts1.addEdge(s5, a1, s5); 
		lts1.addEdge(s5, a0, s10); 
		lts1.addEdge(s6, a0, s6); 
		lts1.addEdge(s6, a1, s11); 
		lts1.addEdge(s7, a1, s1); 
		lts1.addEdge(s7, a0, s3); 
		lts1.addEdge(s7, a1, s5); 
		lts1.addEdge(s7, a1, s11); 
		lts1.addEdge(s9, a0, s4); 
		lts1.addEdge(s9, a1, s6); 
		lts1.addEdge(s10,a0, s6); 
		lts1.addEdge(s10,a0, s7); 
		lts1.addEdge(s10,a1, s8); 
		lts1.addEdge(s11,a0, s1); 
		lts1.addEdge(s11,a1, s4); 
		lts1.addEdge(s11,a0, s10); 
    }
    
    public void testBisimulation(){
    	LtsState init1 = s1;
    	LtsState init2 = s2;

		boolean res;
		{//  Tapas1
			TBisimulationChecker<LtsState,LtsAction> checker = 
				new TBisimulationChecker<LtsState,LtsAction>();
			checker.setParameters(lts1, lts1, init1, init2, false); 
			res = checker.checkEquivalence();
			Assert.assertFalse( res );
		}

		{//	PT 
			PTChecker<LtsState,LtsAction> ptChecker = 
				new PTChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapPT = ptChecker.computeEquivalence(null);
			res = mapPT.eval(init1)==mapPT.eval(init2);
			Assert.assertFalse( res );
		}
	
		{//	MPT
			MPTChecker<LtsState,LtsAction> mptChecker = 
				new MPTChecker<LtsState,LtsAction>(3, lts1);
			Evaluator<LtsState,?> mapMPT = mptChecker.computeEquivalence(null);
			res = mapMPT.eval(init1)==mapMPT.eval(init2);
			Assert.assertFalse( res );
		}

		{//  KS
			KSChecker<LtsState,LtsAction> ksChecker = 
				new KSChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapKS = ksChecker.computeEquivalence(null);
			res = mapKS.eval(init1)==mapKS.eval(init2);
			Assert.assertFalse( res );
		}
		
		{//	KSO
			KSOChecker<LtsState,LtsAction> ksoChecker = 
				new KSOChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapKSO = ksoChecker.computeEquivalence(null);
			res = mapKSO.eval(init1)==mapKSO.eval(init2);
			Assert.assertFalse( res );
		}
	
		{//	Rank
			RankBasedChecker<LtsState,LtsAction> rankChecker = 
				new RankBasedChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapRank = rankChecker.computeEquivalence(null);
			res = mapRank.eval(init1)==mapRank.eval(init2);
			Assert.assertFalse( res );
		}
	}
 
	/* Metodo dinamico: aggiungendo un test questo viene 
	 * chiamato automaticamente.*/
	public static Test suite() {
	    return new TestSuite(BisimulationTest.class);
	}
	
	@Override 
    protected void tearDown() {
        // 
    }
    
}

/**
 */
package org.cmg.tapas.xtext.clts.composedLts.util;

import org.cmg.tapas.xtext.clts.composedLts.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage
 * @generated
 */
public class ComposedLtsSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static ComposedLtsPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComposedLtsSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = ComposedLtsPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case ComposedLtsPackage.MODEL:
      {
        Model model = (Model)theEObject;
        T result = caseModel(model);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.ELEMENT:
      {
        Element element = (Element)theEObject;
        T result = caseElement(element);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTS:
      {
        Lts lts = (Lts)theEObject;
        T result = caseLts(lts);
        if (result == null) result = caseElement(lts);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTS_BODY:
      {
        LtsBody ltsBody = (LtsBody)theEObject;
        T result = caseLtsBody(ltsBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTS_COMPOSITION:
      {
        LtsComposition ltsComposition = (LtsComposition)theEObject;
        T result = caseLtsComposition(ltsComposition);
        if (result == null) result = caseLtsBody(ltsComposition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.COMPOSITION_BODY:
      {
        CompositionBody compositionBody = (CompositionBody)theEObject;
        T result = caseCompositionBody(compositionBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.REFERENCE:
      {
        Reference reference = (Reference)theEObject;
        T result = caseReference(reference);
        if (result == null) result = caseCompositionBody(reference);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.MAPPING:
      {
        Mapping mapping = (Mapping)theEObject;
        T result = caseMapping(mapping);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTS_DECLARATION_BODY:
      {
        LtsDeclarationBody ltsDeclarationBody = (LtsDeclarationBody)theEObject;
        T result = caseLtsDeclarationBody(ltsDeclarationBody);
        if (result == null) result = caseLtsBody(ltsDeclarationBody);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTS_RULE:
      {
        LtsRule ltsRule = (LtsRule)theEObject;
        T result = caseLtsRule(ltsRule);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTS_STATE:
      {
        LtsState ltsState = (LtsState)theEObject;
        T result = caseLtsState(ltsState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LABEL_RULE:
      {
        LabelRule labelRule = (LabelRule)theEObject;
        T result = caseLabelRule(labelRule);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.STATE:
      {
        State state = (State)theEObject;
        T result = caseState(state);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.RULE:
      {
        Rule rule = (Rule)theEObject;
        T result = caseRule(rule);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LABEL:
      {
        Label label = (Label)theEObject;
        T result = caseLabel(label);
        if (result == null) result = caseElement(label);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.ACTION:
      {
        Action action = (Action)theEObject;
        T result = caseAction(action);
        if (result == null) result = caseElement(action);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.FORMULA:
      {
        Formula formula = (Formula)theEObject;
        T result = caseFormula(formula);
        if (result == null) result = caseElement(formula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_FORMULA_DECLARATION:
      {
        HmlFormulaDeclaration hmlFormulaDeclaration = (HmlFormulaDeclaration)theEObject;
        T result = caseHmlFormulaDeclaration(hmlFormulaDeclaration);
        if (result == null) result = caseFormula(hmlFormulaDeclaration);
        if (result == null) result = caseElement(hmlFormulaDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_FORMULA:
      {
        HmlFormula hmlFormula = (HmlFormula)theEObject;
        T result = caseHmlFormula(hmlFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_PREDICATE:
      {
        HmlPredicate hmlPredicate = (HmlPredicate)theEObject;
        T result = caseHmlPredicate(hmlPredicate);
        if (result == null) result = caseHmlFormula(hmlPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_TRUE:
      {
        HmlTrue hmlTrue = (HmlTrue)theEObject;
        T result = caseHmlTrue(hmlTrue);
        if (result == null) result = caseHmlFormula(hmlTrue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_FALSE:
      {
        HmlFalse hmlFalse = (HmlFalse)theEObject;
        T result = caseHmlFalse(hmlFalse);
        if (result == null) result = caseHmlFormula(hmlFalse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_NOT_FORMULA:
      {
        HmlNotFormula hmlNotFormula = (HmlNotFormula)theEObject;
        T result = caseHmlNotFormula(hmlNotFormula);
        if (result == null) result = caseHmlFormula(hmlNotFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_DIAMOND_FORMULA:
      {
        HmlDiamondFormula hmlDiamondFormula = (HmlDiamondFormula)theEObject;
        T result = caseHmlDiamondFormula(hmlDiamondFormula);
        if (result == null) result = caseHmlFormula(hmlDiamondFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_BOX_FORMULA:
      {
        HmlBoxFormula hmlBoxFormula = (HmlBoxFormula)theEObject;
        T result = caseHmlBoxFormula(hmlBoxFormula);
        if (result == null) result = caseHmlFormula(hmlBoxFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_RECURSION_FORMULA:
      {
        HmlRecursionFormula hmlRecursionFormula = (HmlRecursionFormula)theEObject;
        T result = caseHmlRecursionFormula(hmlRecursionFormula);
        if (result == null) result = caseHmlFormula(hmlRecursionFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_MIN_FIX_POINT:
      {
        HmlMinFixPoint hmlMinFixPoint = (HmlMinFixPoint)theEObject;
        T result = caseHmlMinFixPoint(hmlMinFixPoint);
        if (result == null) result = caseHmlRecursionFormula(hmlMinFixPoint);
        if (result == null) result = caseHmlFormula(hmlMinFixPoint);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_MAX_FIX_POINT:
      {
        HmlMaxFixPoint hmlMaxFixPoint = (HmlMaxFixPoint)theEObject;
        T result = caseHmlMaxFixPoint(hmlMaxFixPoint);
        if (result == null) result = caseHmlRecursionFormula(hmlMaxFixPoint);
        if (result == null) result = caseHmlFormula(hmlMaxFixPoint);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_RECURSION_VARIABLE:
      {
        HmlRecursionVariable hmlRecursionVariable = (HmlRecursionVariable)theEObject;
        T result = caseHmlRecursionVariable(hmlRecursionVariable);
        if (result == null) result = caseHmlFormula(hmlRecursionVariable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_FORMULA_DECLARATION:
      {
        LtlFormulaDeclaration ltlFormulaDeclaration = (LtlFormulaDeclaration)theEObject;
        T result = caseLtlFormulaDeclaration(ltlFormulaDeclaration);
        if (result == null) result = caseFormula(ltlFormulaDeclaration);
        if (result == null) result = caseElement(ltlFormulaDeclaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_FORMULA:
      {
        LtlFormula ltlFormula = (LtlFormula)theEObject;
        T result = caseLtlFormula(ltlFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_ATOMIC_PROPOSITION:
      {
        LtlAtomicProposition ltlAtomicProposition = (LtlAtomicProposition)theEObject;
        T result = caseLtlAtomicProposition(ltlAtomicProposition);
        if (result == null) result = caseLtlFormula(ltlAtomicProposition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_NEXT_FORMULA:
      {
        LtlNextFormula ltlNextFormula = (LtlNextFormula)theEObject;
        T result = caseLtlNextFormula(ltlNextFormula);
        if (result == null) result = caseLtlFormula(ltlNextFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_ALWAYS_FORMULA:
      {
        LtlAlwaysFormula ltlAlwaysFormula = (LtlAlwaysFormula)theEObject;
        T result = caseLtlAlwaysFormula(ltlAlwaysFormula);
        if (result == null) result = caseLtlFormula(ltlAlwaysFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_EVENTUALLY_FORMULA:
      {
        LtlEventuallyFormula ltlEventuallyFormula = (LtlEventuallyFormula)theEObject;
        T result = caseLtlEventuallyFormula(ltlEventuallyFormula);
        if (result == null) result = caseLtlFormula(ltlEventuallyFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_NOT_FORMULA:
      {
        LtlNotFormula ltlNotFormula = (LtlNotFormula)theEObject;
        T result = caseLtlNotFormula(ltlNotFormula);
        if (result == null) result = caseLtlFormula(ltlNotFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_TRUE:
      {
        LtlTrue ltlTrue = (LtlTrue)theEObject;
        T result = caseLtlTrue(ltlTrue);
        if (result == null) result = caseLtlFormula(ltlTrue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_FALSE:
      {
        LtlFalse ltlFalse = (LtlFalse)theEObject;
        T result = caseLtlFalse(ltlFalse);
        if (result == null) result = caseLtlFormula(ltlFalse);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LABEL_PREDICATE:
      {
        LabelPredicate labelPredicate = (LabelPredicate)theEObject;
        T result = caseLabelPredicate(labelPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.NOT_LABEL_PREDICATE:
      {
        NotLabelPredicate notLabelPredicate = (NotLabelPredicate)theEObject;
        T result = caseNotLabelPredicate(notLabelPredicate);
        if (result == null) result = caseLabelPredicate(notLabelPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.ACTION_LABEL_PREDICATE:
      {
        ActionLabelPredicate actionLabelPredicate = (ActionLabelPredicate)theEObject;
        T result = caseActionLabelPredicate(actionLabelPredicate);
        if (result == null) result = caseLabelPredicate(actionLabelPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.ANY_LABEL_PREDICATE:
      {
        AnyLabelPredicate anyLabelPredicate = (AnyLabelPredicate)theEObject;
        T result = caseAnyLabelPredicate(anyLabelPredicate);
        if (result == null) result = caseLabelPredicate(anyLabelPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.INTERLEAVING:
      {
        Interleaving interleaving = (Interleaving)theEObject;
        T result = caseInterleaving(interleaving);
        if (result == null) result = caseCompositionBody(interleaving);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.SYNCHRONIZATION:
      {
        Synchronization synchronization = (Synchronization)theEObject;
        T result = caseSynchronization(synchronization);
        if (result == null) result = caseCompositionBody(synchronization);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.CONTROLLED_INTERACTION:
      {
        ControlledInteraction controlledInteraction = (ControlledInteraction)theEObject;
        T result = caseControlledInteraction(controlledInteraction);
        if (result == null) result = caseCompositionBody(controlledInteraction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.RENAMING:
      {
        Renaming renaming = (Renaming)theEObject;
        T result = caseRenaming(renaming);
        if (result == null) result = caseCompositionBody(renaming);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_OR_FORMULA:
      {
        HmlOrFormula hmlOrFormula = (HmlOrFormula)theEObject;
        T result = caseHmlOrFormula(hmlOrFormula);
        if (result == null) result = caseHmlFormula(hmlOrFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.HML_AND_FORMULA:
      {
        HmlAndFormula hmlAndFormula = (HmlAndFormula)theEObject;
        T result = caseHmlAndFormula(hmlAndFormula);
        if (result == null) result = caseHmlFormula(hmlAndFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_OR_FORMULA:
      {
        LtlOrFormula ltlOrFormula = (LtlOrFormula)theEObject;
        T result = caseLtlOrFormula(ltlOrFormula);
        if (result == null) result = caseLtlFormula(ltlOrFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_AND_FORMULA:
      {
        LtlAndFormula ltlAndFormula = (LtlAndFormula)theEObject;
        T result = caseLtlAndFormula(ltlAndFormula);
        if (result == null) result = caseLtlFormula(ltlAndFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.LTL_UNTIL_FORMULA:
      {
        LtlUntilFormula ltlUntilFormula = (LtlUntilFormula)theEObject;
        T result = caseLtlUntilFormula(ltlUntilFormula);
        if (result == null) result = caseLtlFormula(ltlUntilFormula);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.CUP_LABEL_PREDICATE:
      {
        CupLabelPredicate cupLabelPredicate = (CupLabelPredicate)theEObject;
        T result = caseCupLabelPredicate(cupLabelPredicate);
        if (result == null) result = caseLabelPredicate(cupLabelPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ComposedLtsPackage.CAP_LABEL_PREDICATE:
      {
        CapLabelPredicate capLabelPredicate = (CapLabelPredicate)theEObject;
        T result = caseCapLabelPredicate(capLabelPredicate);
        if (result == null) result = caseLabelPredicate(capLabelPredicate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Model</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModel(Model object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseElement(Element object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lts</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lts</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLts(Lts object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lts Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lts Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtsBody(LtsBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lts Composition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lts Composition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtsComposition(LtsComposition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Composition Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Composition Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCompositionBody(CompositionBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Reference</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Reference</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseReference(Reference object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mapping</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mapping</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMapping(Mapping object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lts Declaration Body</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lts Declaration Body</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtsDeclarationBody(LtsDeclarationBody object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lts Rule</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lts Rule</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtsRule(LtsRule object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Lts State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Lts State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtsState(LtsState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Label Rule</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Label Rule</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabelRule(LabelRule object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseState(State object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Rule</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Rule</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRule(Rule object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Label</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Label</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabel(Label object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Action</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAction(Action object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFormula(Formula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Formula Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Formula Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlFormulaDeclaration(HmlFormulaDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlFormula(HmlFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlPredicate(HmlPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml True</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml True</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlTrue(HmlTrue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml False</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml False</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlFalse(HmlFalse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Not Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Not Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlNotFormula(HmlNotFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Diamond Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Diamond Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlDiamondFormula(HmlDiamondFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Box Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Box Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlBoxFormula(HmlBoxFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Recursion Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Recursion Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlRecursionFormula(HmlRecursionFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Min Fix Point</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Min Fix Point</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlMinFixPoint(HmlMinFixPoint object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Max Fix Point</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Max Fix Point</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlMaxFixPoint(HmlMaxFixPoint object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Recursion Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Recursion Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlRecursionVariable(HmlRecursionVariable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Formula Declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Formula Declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlFormulaDeclaration(LtlFormulaDeclaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlFormula(LtlFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Atomic Proposition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Atomic Proposition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlAtomicProposition(LtlAtomicProposition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Next Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Next Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlNextFormula(LtlNextFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Always Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Always Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlAlwaysFormula(LtlAlwaysFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Eventually Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Eventually Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlEventuallyFormula(LtlEventuallyFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Not Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Not Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlNotFormula(LtlNotFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl True</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl True</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlTrue(LtlTrue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl False</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl False</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlFalse(LtlFalse object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Label Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLabelPredicate(LabelPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Not Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Not Label Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNotLabelPredicate(NotLabelPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Action Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Action Label Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseActionLabelPredicate(ActionLabelPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Any Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Any Label Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnyLabelPredicate(AnyLabelPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Interleaving</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Interleaving</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInterleaving(Interleaving object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Synchronization</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Synchronization</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSynchronization(Synchronization object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Controlled Interaction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Controlled Interaction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseControlledInteraction(ControlledInteraction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Renaming</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Renaming</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRenaming(Renaming object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml Or Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml Or Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlOrFormula(HmlOrFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Hml And Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Hml And Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseHmlAndFormula(HmlAndFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Or Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Or Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlOrFormula(LtlOrFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl And Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl And Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlAndFormula(LtlAndFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ltl Until Formula</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ltl Until Formula</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLtlUntilFormula(LtlUntilFormula object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cup Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cup Label Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCupLabelPredicate(CupLabelPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cap Label Predicate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cap Label Predicate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCapLabelPredicate(CapLabelPredicate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //ComposedLtsSwitch

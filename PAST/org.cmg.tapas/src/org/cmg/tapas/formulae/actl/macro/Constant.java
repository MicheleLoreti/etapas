package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public class Constant<S extends APProcess<S, A>, A extends ActionInterface > extends StateFormulaMacro<S, A> {

	private ActlFormula<S, A> formula;
	
	public Constant( ActlFormula<S, A> formula ) {
		this.formula = formula;
	}

	@Override
	public ActlFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		if (f.length > 0) throw new IllegalArgumentException();
		return formula;
	}

	@Override
	public int parameters() {
		return 0;
	}

	@Override
	public void acceptVisitor(MacroVisitor<S, A> visitor) {
		visitor.visitConstant(this);
	}

}

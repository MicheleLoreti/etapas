/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.lic.secsi.SecsiPackage#getBooleanType()
 * @model
 * @generated
 */
public interface BooleanType extends Type, TypeExpression
{
} // BooleanType

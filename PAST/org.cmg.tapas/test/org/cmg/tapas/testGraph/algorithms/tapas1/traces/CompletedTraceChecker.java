package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessFG;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;


/**
 * La classe CompletedTraceChecker realizza l'algoritmo di verifica 
 * per l'equivalenza completed trace. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class CompletedTraceChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {

    /** 
     * Costruisce una nuova istanza del checker.
     */
	public CompletedTraceChecker() {
		super("Completed Trace");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 */	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );
		
		// final graph
		ProcessFG p1FG = ProcessFG.buildFinalGraph(p1);
		ProcessFG p2FG = ProcessFG.buildFinalGraph(p2);
		
		// fusion
		ProcessFG pFG = ProcessFG.createProcessFusion(p1FG, p2FG);
		
		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();

		// initializing partition 
		PTPartition initialRo = CompletedTraceUtil.initPTPartition(pFG);
		
		// refinement
		PTState initialPTState1 = initialRo.getPTState(pFG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pFG.getSecondInitState());

		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pFG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		boolean result = ro.checkSameClass(
			pFG.getInitState(), pFG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			CompletedTraceUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName(),
				pFG				
			);
		}
		
		return result;
	}

}
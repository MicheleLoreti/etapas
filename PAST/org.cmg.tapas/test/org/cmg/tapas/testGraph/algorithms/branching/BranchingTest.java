package org.cmg.tapas.testGraph.algorithms.branching;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.branching.gv.GVChecker;
import org.cmg.tapas.graph.algorithms.branching.gvks.GVKSChecker;
import org.cmg.tapas.graph.algorithms.branching.gvs.GVSChecker;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.tapas1.branching.TBranchingChecker;


/**
 * Test per gli algoritmi di bisimulazione branching.
 * Si tratta di un piccolo test che verifica solo
 * un grafo specifico. 
 *
 * @author Guzman Tierno
 **/
public class BranchingTest extends TestCase {
	
    private GraphInterface<LtsState,LtsAction> lts1;
    //private GraphInterface<LtsState,LtsAction> lts2;
	private LtsState s0, s1, s2, s3; //s4, s5, s6, s7, s8, s9, s10, s11;
	private LtsAction tau, a1;
	
	
	public static void main(String[] args) {
		System.out.print("Run BranchingTest.java");
		junit.textui.TestRunner.run(BranchingTest.class);
	}
    
	
    public BranchingTest(String s){
    	super(s);
    }

	@Override 
    protected void setUp() {  	
    	lts1 = new Graph<LtsState, LtsAction>();

    	s0  = new LtsState("0" );
    	s1  = new LtsState("1" );
    	s2  = new LtsState("2" );
    	s3  = new LtsState("3" );

    	a1 = new LtsAction("1");
    	tau = LtsAction.TAU;

		lts1.addState(s0); 
		lts1.addState(s1); 
		lts1.addState(s2); 
		lts1.addState(s3); 

		lts1.addEdge(s3, a1 , s1); 
		lts1.addEdge(s0, a1 , s2); 		
		lts1.addEdge(s1, a1 , s3); 		
		lts1.addEdge(s1, tau, s2); 
    }
    
    public void testBranching(){
    	LtsState init1 = s3;
    	LtsState init2 = s1;    	
    	Evaluator<LtsState, ?> map;
		boolean res;
		System.out.println();

		GVKSChecker<LtsState,LtsAction> gvksChecker = 
			new GVKSChecker<LtsState,LtsAction>(lts1);
		map = gvksChecker.computeEquivalence(
			new Evaluator<LtsState,Object>() {
                @Override public Object eval(LtsState s) {
					return this;
				}
			}
		);
		res = map.eval(init1)==map.eval(init2);
		Assert.assertFalse( res );		
		
		GVChecker<LtsState,LtsAction> gvChecker = 
			new GVChecker<LtsState,LtsAction>(lts1);
		map = gvChecker.computeEquivalence(
			new Evaluator<LtsState,Object>() {
                @Override public Object eval(LtsState s) {
					return this;
				}
			}
		);
		res = map.eval(init1)==map.eval(init2);
		Assert.assertFalse( res );
		
		GVSChecker<LtsState,LtsAction> gvsChecker = 
			new GVSChecker<LtsState,LtsAction>(lts1);
		map = gvsChecker.computeEquivalence(
			new Evaluator<LtsState,Object>() {
                @Override public Object eval(LtsState s) {
					return this;
				}
			}
		);
		res = map.eval(init1)==map.eval(init2);
		Assert.assertFalse( res );
		
		// Tapas1
		for( int i=0; i<1; i++ ) {
			TBranchingChecker<LtsState,LtsAction> tapas1Checker = 
				new TBranchingChecker<LtsState,LtsAction>();
			tapas1Checker.setParameters(lts1, lts1, init1, init2, false); 
			res = tapas1Checker.checkEquivalence();
			Assert.assertFalse( res );
		}
	}
 
	public static Test suite() {
	    return new TestSuite(BranchingTest.class);
	}
	
	@Override 
    protected void tearDown() {
        //   
    }
    
}

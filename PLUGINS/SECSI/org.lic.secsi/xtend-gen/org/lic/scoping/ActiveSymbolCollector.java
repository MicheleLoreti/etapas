package org.lic.scoping;

import java.util.Arrays;
import java.util.LinkedList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.lic.secsi.Agent;
import org.lic.secsi.Block;
import org.lic.secsi.Constant;
import org.lic.secsi.IfThenElse;
import org.lic.secsi.Program;
import org.lic.secsi.ReferenceableElement;
import org.lic.secsi.Statement;
import org.lic.secsi.Switch_Case;
import org.lic.secsi.TypeDef;
import org.lic.secsi.TypeElement;
import org.lic.secsi.Variable;
import org.lic.secsi.While;

@SuppressWarnings("all")
public class ActiveSymbolCollector {
  public IScope getActiveSymbol(final Statement s) {
    EObject _eContainer = s.eContainer();
    return this.getActiveSymbol(_eContainer, s);
  }
  
  public IScope getActiveSymbol(final Agent p) {
    IScope _xblockexpression = null;
    {
      EObject _eContainer = p.eContainer();
      Program program = ((Program) _eContainer);
      LinkedList<ReferenceableElement> list = new LinkedList<ReferenceableElement>();
      EList<TypeDef> _types = program.getTypes();
      for (final TypeDef t : _types) {
        EList<TypeElement> _elements = t.getElements();
        list.addAll(_elements);
      }
      EList<Constant> _consts = program.getConsts();
      list.addAll(_consts);
      _xblockexpression = Scopes.scopeFor(list);
    }
    return _xblockexpression;
  }
  
  protected IScope _getActiveSymbol(final EObject container, final EObject element) {
    return IScope.NULLSCOPE;
  }
  
  protected IScope _getActiveSymbol(final Block b, final Statement s) {
    throw new Error("Unresolved compilation problems:"
      + "\nType mismatch: cannot convert from void to IScope");
  }
  
  protected IScope _getActiveSymbol(final IfThenElse c, final Statement s) {
    return this.getActiveSymbol(c);
  }
  
  protected IScope _getActiveSymbol(final While c, final Statement s) {
    return this.getActiveSymbol(c);
  }
  
  protected IScope _getActiveSymbol(final Switch_Case c, final Statement s) {
    return this.getActiveSymbol(c);
  }
  
  protected IScope _getActiveSymbol(final Agent p, final Statement s) {
    EList<Variable> _params = p.getParams();
    IScope _activeSymbol = this.getActiveSymbol(p);
    return Scopes.scopeFor(_params, _activeSymbol);
  }
  
  public IScope getActiveSymbol(final EObject b, final EObject s) {
    if (b instanceof Block
         && s instanceof Statement) {
      return _getActiveSymbol((Block)b, (Statement)s);
    } else if (b instanceof IfThenElse
         && s instanceof Statement) {
      return _getActiveSymbol((IfThenElse)b, (Statement)s);
    } else if (b instanceof Switch_Case
         && s instanceof Statement) {
      return _getActiveSymbol((Switch_Case)b, (Statement)s);
    } else if (b instanceof While
         && s instanceof Statement) {
      return _getActiveSymbol((While)b, (Statement)s);
    } else if (b instanceof Agent
         && s instanceof Statement) {
      return _getActiveSymbol((Agent)b, (Statement)s);
    } else if (b != null
         && s != null) {
      return _getActiveSymbol(b, s);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(b, s).toString());
    }
  }
}

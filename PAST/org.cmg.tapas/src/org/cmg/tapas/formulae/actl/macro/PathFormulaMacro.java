/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public abstract class PathFormulaMacro<S extends APProcess<S, A>, A extends ActionInterface> {

	public abstract PathFormula<S, A> getFormula( ActlFormula<S,A> ... f );
	public abstract int parameters();
	public abstract void accept( PathMacroVisitor<S,A> visitor );
	
}

package org.cmg.tapas.testGraph.algorithms.tapas1;

/**
 * La classe Action rappresenta un'azione di un lts.
 * <p>
 *
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class Action {
	
	/**
	 * Rappresenta l'azione invisibile TAU. 
	 **/
	public static final String TAU = new String("tau");

	/**
	 * Rappresenta l'azione invisibile TAU. 
	 **/
	public static final Action ACTION_TAU = new Action(TAU);

	/**
	 * Rappresenta l'azione vuota EPSILON. 
	 **/	
	public static final String EPSILON = new String("epsilon");

	/**
	 * Rappresenta l'azione invisibile EPSILON. 
	 **/
	public static final Action ACTION_EPSILON = new Action(EPSILON);

	/**
	 * Nome dell'azione.
	 **/
	private String name = null;
		
	/**
	 * Costruisce una nuova azione.
	 **/
  	public Action(String name) {
  		this.name = name;
  	}

	/**
	 * Restituisce il nome dell'azione.
	 **/
  	public String getName() {
  		return name;
  	}

	/**
	 * Confronta questa azione con un'altra azione. 
	 **/
	public boolean equals(Action a){
		return name.equals( a.name );
	}


	/**
	 * Esegue un confronto lessicografico tra il nome di questa 
	 * azione ed il nome dell'azione passata come parametro.
	 **/
	public int compareTo(Action a) {
		return name.compareTo( a.name );
	}

	/**
	 * Controlla se l'azione corrisponde all'azione invisibile <tt>TAU</tt>.
	 **/
	public boolean isTau() {
		return name.equals(TAU);
	}

	/**
	 * Controlla se l'azione corrisponde all'azione vuota <tt>EPSILON</tt>.
	 **/
	public boolean isEpsilon() {
		return name.equals(EPSILON);
	}

	/**
	 * Stampa il nome dell'azione sullo standard output. 
	 **/
  	public void print() {
  		System.out.print(name);
  	}

	/**
	 * Stampa il nome dell'azione sullo standard output. 
	 **/
  	public void print(java.io.PrintStream console) {
  		console.print(name);
  	}	
  	
}
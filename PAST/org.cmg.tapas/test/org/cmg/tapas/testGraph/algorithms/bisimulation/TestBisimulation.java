package org.cmg.tapas.testGraph.algorithms.bisimulation;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.bisimulation.ks.KSChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.ksopt.KSOChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.mpt.MPTChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.pt.PTChecker;
import org.cmg.tapas.graph.algorithms.bisimulation.rankBased.RankBasedChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.RandomGraphGenerator;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.TBisimulationChecker;

public class TestBisimulation extends TestCase {

	private GraphInterface<LtsState,LtsAction> lts1;
	private LtsState s0;
	private LtsState s1;
	private LtsState s2;
	private LtsState s3;
	private LtsState s4;
	private LtsState s5;
	private LtsState s6;
	private LtsState s7;
	private LtsState s8;
	private LtsState s9;
	private LtsState s10;
	private LtsState s11;
	private LtsAction a0;
	private LtsAction a1;

	public void testTiming(){
		// active checkers
		boolean ks = true;		// must be true, it is used for comparisons
		boolean kso = true;	
		boolean pt = true;
		boolean mpt = true;	
		int mpt_degree = 3;
		boolean rank = true;
		boolean tapas1 = true;
		
		int repetitions = 100;
		int actionCount = 4;	
		int statesNumber = 100;
		double connectionProbability = 0.2;

		// _____________________________________________________________________
	
		GraphInterface<LtsState,LtsAction> graph;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>(new GraphItemFactory());
		
		LtsAction[] actions = new LtsAction[actionCount];
		for( int i=0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );
		
		boolean ptRes, mptRes, ksRes, tapas1Res, rankRes, ksoRes;
		// make compiler happy
		ptRes = mptRes = ksRes = tapas1Res = rankRes = ksoRes = true; 
		double genTime=0;
		double ptTime=0, mptTime=0, ksTime=0, tapas1Time=0, rankTime=0, ksoTime=0;
		double start;
		Evaluator<LtsState,?> map;
		for( int i=0; i<repetitions; ++i ) {							
			
			// generate random graph (according to settings)			
			start = System.currentTimeMillis();
			pointedGraph = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph = pointedGraph.getGraph();
			genTime += System.currentTimeMillis() - start;
			
			// KS
			if( ks ) {
				start = System.currentTimeMillis();
				KSChecker<LtsState,LtsAction> ksChecker = 
					new KSChecker<LtsState,LtsAction>(graph);
				map = ksChecker.computeEquivalence(null);
				ksRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				ksTime += System.currentTimeMillis() - start;
			}
				
			// PT	
			if( pt ) {
				start = System.currentTimeMillis();
				PTChecker<LtsState,LtsAction> ptChecker = 
					new PTChecker<LtsState,LtsAction>(graph);
				map = ptChecker.computeEquivalence(null);
				ptRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				ptTime += System.currentTimeMillis() - start;
			}
			
			// MPT	
			if( mpt ) {
				start = System.currentTimeMillis();
				MPTChecker<LtsState,LtsAction> mptChecker = 
					new MPTChecker<LtsState,LtsAction>(mpt_degree, graph);
				map = mptChecker.computeEquivalence(null);
				mptRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				mptTime += System.currentTimeMillis() - start;
			}
			
			// RANK-BASED
			if( rank ) {
				start = System.currentTimeMillis();
				RankBasedChecker<LtsState,LtsAction> rankChecker = 
					new RankBasedChecker<LtsState,LtsAction>(graph);
				map = rankChecker.computeEquivalence(null);
				rankRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				rankTime += System.currentTimeMillis() - start;
			}
	
			// KSO
			if( kso ) {
				start = System.currentTimeMillis();
				KSOChecker<LtsState,LtsAction> ksoChecker = 
					new KSOChecker<LtsState,LtsAction>(graph);
				map = ksoChecker.computeEquivalence(null);
				ksoRes = 
					map.eval(pointedGraph.getState1()) == 
					map.eval(pointedGraph.getState2());
				ksoTime += System.currentTimeMillis() - start;
			}
			
			//	Tapas1, solo controllo
			//	if( tapas1 ) {
			//		start = System.currentTimeMillis();
			//		TBisimulationChecker<LtsState,LtsAction> checker = 
			//			new TBisimulationChecker<LtsState,LtsAction>();
			//		checker.setParameters(
			//			graph, graph, 
			//			pointedGraph.getState1(), pointedGraph.getState2(), 
			//			false
			//		); 
			//		tapas1Res = checker.checkEquivalence();
			//		tapas1Time += System.currentTimeMillis() - start;
			//	}
	
			// Tapas1, calcolo della partizione
			if( tapas1 ) {
				start = System.currentTimeMillis();
				Lts lts = Lts.createProcess(
					graph, pointedGraph.getState1(), pointedGraph.getState2()
				);
				PTPartition initialRo = Bisimulation.initPTPartition(lts);
				Partition partition = 
					Bisimulation.computePTPartitionRefinement(lts, initialRo);
				tapas1Res = partition.checkSameClass(
					lts.getInitState(), lts.getSecondInitState()
				);
				tapas1Time += System.currentTimeMillis() - start;
			}
	
			// check equality of results
			if( kso && ksoRes != ksRes ) {
				fail();
			}
			if( pt && ptRes != ksRes ) {
				fail();
			}
			if( mpt && mptRes != ksRes ) {
				fail();
			}
			if( rank && rankRes != ksRes ) {
				fail();
			}
			if( tapas1 &&  tapas1Res != ksRes) {
				fail();
			}
				
			System.gc();
		}
	}

	@Override 
	protected void setUp() {  	
		lts1 = new Graph<LtsState, LtsAction>();
	
		s0  = new LtsState("0" );
		s1  = new LtsState("1" );
		s2  = new LtsState("2" );
		s3  = new LtsState("3" );
		s4  = new LtsState("4" );
		s5  = new LtsState("5" );
		s6  = new LtsState("6" );
		s7  = new LtsState("7" );
		s8  = new LtsState("8" );
		s9  = new LtsState("9" );
		s10 = new LtsState("10");
		s11 = new LtsState("11");
	
		a0 = new LtsAction("0");
		a1 = new LtsAction("1");
	
		lts1.addState(s0 ); 
		lts1.addState(s1 ); 
		lts1.addState(s2 ); 
		lts1.addState(s3 ); 
		lts1.addState(s4 ); 
		lts1.addState(s5 ); 
		lts1.addState(s6 );		 
		lts1.addState(s7 );		
		lts1.addState(s8 ); 
		lts1.addState(s9 ); 
		lts1.addState(s10); 
		lts1.addState(s11); 
	
		lts1.addEdge(s0, a0, s0); 
		lts1.addEdge(s0, a0, s10); 
		lts1.addEdge(s1, a1, s3); 
		lts1.addEdge(s1, a0, s4); 
		lts1.addEdge(s1, a0, s6); 
		lts1.addEdge(s1, a0, s7); 
		lts1.addEdge(s1, a0, s10); 
		lts1.addEdge(s2, a0, s5); 
		lts1.addEdge(s2, a0, s7); 
		lts1.addEdge(s2, a1, s9); 
		lts1.addEdge(s2, a1, s10); 
		lts1.addEdge(s3, a1, s0); 
		lts1.addEdge(s3, a0, s2); 
		lts1.addEdge(s4, a0, s2); 
		lts1.addEdge(s4, a1, s5); 
		lts1.addEdge(s4, a1, s8); 
		lts1.addEdge(s4, a0, s9); 
		lts1.addEdge(s5, a1, s1); 
		lts1.addEdge(s5, a0, s3); 
		lts1.addEdge(s5, a1, s5); 
		lts1.addEdge(s5, a0, s10); 
		lts1.addEdge(s6, a0, s6); 
		lts1.addEdge(s6, a1, s11); 
		lts1.addEdge(s7, a1, s1); 
		lts1.addEdge(s7, a0, s3); 
		lts1.addEdge(s7, a1, s5); 
		lts1.addEdge(s7, a1, s11); 
		lts1.addEdge(s9, a0, s4); 
		lts1.addEdge(s9, a1, s6); 
		lts1.addEdge(s10,a0, s6); 
		lts1.addEdge(s10,a0, s7); 
		lts1.addEdge(s10,a1, s8); 
		lts1.addEdge(s11,a0, s1); 
		lts1.addEdge(s11,a1, s4); 
		lts1.addEdge(s11,a0, s10); 
	}

	public void testBisimulation(){
		LtsState init1 = s1;
		LtsState init2 = s2;
	
		boolean res;
		{//  Tapas1
			TBisimulationChecker<LtsState,LtsAction> checker = 
				new TBisimulationChecker<LtsState,LtsAction>();
			checker.setParameters(lts1, lts1, init1, init2, false); 
			res = checker.checkEquivalence();
			Assert.assertFalse( res );
		}
	
		{//	PT 
			PTChecker<LtsState,LtsAction> ptChecker = 
				new PTChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapPT = ptChecker.computeEquivalence(null);
			res = mapPT.eval(init1)==mapPT.eval(init2);
			Assert.assertFalse( res );
		}
	
		{//	MPT
			MPTChecker<LtsState,LtsAction> mptChecker = 
				new MPTChecker<LtsState,LtsAction>(3, lts1);
			Evaluator<LtsState,?> mapMPT = mptChecker.computeEquivalence(null);
			res = mapMPT.eval(init1)==mapMPT.eval(init2);
			Assert.assertFalse( res );
		}
	
		{//  KS
			KSChecker<LtsState,LtsAction> ksChecker = 
				new KSChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapKS = ksChecker.computeEquivalence(null);
			res = mapKS.eval(init1)==mapKS.eval(init2);
			Assert.assertFalse( res );
		}
		
		{//	KSO
			KSOChecker<LtsState,LtsAction> ksoChecker = 
				new KSOChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapKSO = ksoChecker.computeEquivalence(null);
			res = mapKSO.eval(init1)==mapKSO.eval(init2);
			Assert.assertFalse( res );
		}
	
		{//	Rank
			RankBasedChecker<LtsState,LtsAction> rankChecker = 
				new RankBasedChecker<LtsState,LtsAction>(lts1);
			Evaluator<LtsState,?> mapRank = rankChecker.computeEquivalence(null);
			res = mapRank.eval(init1)==mapRank.eval(init2);
			Assert.assertFalse( res );
		}
	}	
}

package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;
import org.cmg.tapas.testGraph.algorithms.tapas1.State;


/**
 * La classe PTState rappresenta uno stato di un processo 
 * con una struttura adhoc simili a quelle usate 
 * nell'algoritmo di Paige-Tarjan
 *
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class PTState {
	
	/**
	 * Rappresenta lo stato corrispondente al PTState corrente. 
	 **/		
	private State state = null;

	/**
	 * Rappresenta le transizioni entranti in questo stato. 
	 **/	
	private Vector<PresetCouple> presetCouples = null;

	/**
	 * Rappresenta i contatori di transizione per questo stato. 
	 **/	
	private Vector<CounterTriple> counterTriples = null;

	/**
	 * Costruisce un nuovo PTState.
	 **/		
	public PTState(State state) {
		this.state = state;
		presetCouples = new Vector<PresetCouple>();
		counterTriples = new Vector<CounterTriple>();
	}
	
	/**
	 * Restituisce lo stato corrispondente al PTState corrente.
	 **/ 	
	public State getState() {
		return this.state;	
	}

	/**
	 * Restituisce la tripla contenente il contatore relativo all'insieme 
	 * di stati ed all'azione passati
	 * come parametri. 
	 * Il vettore delle triple � ordinato rispetto al nome delle azioni 
	 * delle triple ma a parit� di azione non
	 * � ordinato rispetto al <tt>PTStatesSet</tt> contenuto nella tripla. 
	 **/ 
	private CounterTriple getCounterTriple(PTStatesSet ptss, Action a) {
		CounterTriple ct = null;
		int compare = 0;
		Action action = null;		
		for (int i=0; i<counterTriples.size(); i++) {
			ct = counterTriples.elementAt(i);
			action = ct.getAction();
			compare = action.compareTo(a);
			if (compare == 0) { 
				if ((ct.getPTStatesSet()) == ptss) return ct;	// == ?
			}
			else if (compare > 0) return null;
		}	
		return null;		
	}

	/**
	 * Restituisce il contatore relativo all'insieme di stati 
	 * ed all'azione passati come parametri di ingresso. 
	 **/
	public int getCount(PTStatesSet ptss,  Action a) {
		CounterTriple ct = this.getCounterTriple(ptss, a);		
		if( ct!=null ) 
			return ct.getCount();

		return 0;
	}

	/**
	 * Incrementa di 1 il contatore relativo all'insieme di stati 
	 * ed all'azione passati come parametri.
	 **/ 	
	public void incrementCount(PTStatesSet ptss,  Action a) {
		CounterTriple ct = null;
		int i = 0;
		int compare = 0;
		boolean incremented = false;
		Action action = null;		
		for (i=0; i<counterTriples.size(); i++) {
			ct = counterTriples.elementAt(i);
			action = ct.getAction();
			compare = action.compareTo(a);
			if (compare == 0) {
				if ((ct.getPTStatesSet())==ptss) {
					ct.increment();
					incremented = true;
					break;
				}
			}
			else if (compare > 0) break;
		}	
		if (!incremented) {
			ct = new CounterTriple(ptss, a);
			ct.increment();
			counterTriples.add(i,ct);
		}
	}

	/**
	 * Rimuove una tripla dal vettore delle triple corrispondente 
	 * all'insieme <tt>ptss</tt> ed all'azione <tt>a</tt> 
	 * passati come parametri.
	 */ 	
	public void removeCount(PTStatesSet ptss,  Action a) {
		CounterTriple ct = null;
		int i = 0;
		int compare = 0;
		Action action = null;		
		for (i=0; i<counterTriples.size(); i++) {
			ct = counterTriples.elementAt(i);
			action = ct.getAction();
			compare = action.compareTo(a);
			if (compare == 0) {
				if ((ct.getPTStatesSet()) == ptss) {
					this.counterTriples.remove(i);
					break;
				}
			}
			else if (compare > 0) break;
		}
	}

	/**
	 * Aggiunge una nuova transizione entrante nello stato corrente. 
	 **/ 
	public void addPresetCouple(Action a, PTState s) {
		PresetCouple pc = null;
		Action action = null;
		int compare = 0;
		int i = 0;
		boolean inserito = false;
		
		for (i=0; i<presetCouples.size(); i++) {
			pc = presetCouples.elementAt(i);
			action = pc.getAction();
			compare = action.compareTo(a);
			if (compare == 0) {
				pc.addPTState(s);
				inserito = true;
				break;
			}
			else if (compare > 0) break;
		}
		if (!inserito) {
			pc = new PresetCouple(a);
			presetCouples.add(i,pc);
			pc.addPTState(s);
		}
	}

	/**
	 * Restituisce un oggetto di tipo <tt>PresetCouple</tt> 
	 * che rappresenta tutte le transizioni entranti 
	 * nello stato corrente etichettate con l'azione passata come parametro.
	 **/ 	
	private PresetCouple getPresetCouple(Action a) {
		PresetCouple pc = null;
		int compare = 0;
		int low = 0;
		int high = this.presetCouples.size() - 1;
		int mid = 0;
		while (low <= high) {
			mid = (low + high) / 2;
			pc = presetCouples.elementAt(mid);
			compare = pc.getAction().compareTo(a);
			if (compare < 0) low = mid + 1;
			else if (compare > 0) high = mid -1;
			else return pc;
		}
		return null;
	}
	
	/**
	 * Restituisce un oggetto di tipo <tt>PTStatesSet</tt> che rappresenta 
	 * tutti gli stati che possono
	 * raggiungere lo stato corrente con l'azione passata come parametro.
	 **/ 		
	public PTStatesSet getPTStatesPreset(Action a) {
		PresetCouple pc = this.getPresetCouple(a);
		if( pc==null ) 
			return null;
		return pc.getPTStatesPreset();
	}


	/**
	 * Stampa interamente il <tt>PTState</tt> corrente sullo standard output. 
	 **/
	public void printAll() {
		int i=0;
		System.out.print("State "+state.getName()+" preset:");
		for (i=0; i<presetCouples.size(); i++) {
			System.out.print("\n   ");
			presetCouples.elementAt(i).print();
		}
		if (i==0) System.out.print("empty");
	}

	/**
	 * Stampa lo stato associato al PTState corrente sullo standard output. 
	 **/	
	public void print() {
		this.state.print();
	}	
}
package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;


/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBLogSplitting {
	private Vector<BBLogSplittingEntry> log = null;
	
	public BBLogSplitting() {
		log = new Vector<BBLogSplittingEntry>();
	}
	
	public void addEntry(BBLogSplittingEntry e) {
		log.addElement(e);
	}
	
	public int getDimension() {
		return log.size();	
	}
	
	public BBLogSplittingEntry entryAt(int index) {
		return log.elementAt(index);	
	}

	public BBLogSplittingEntry getDeppestCause(
		State s1, State s2
	) {
		BBLogSplittingEntry e;
		BBBloc bbbloc;
		
		//Cerco il blocco pi� in profondit� che contiene s1 s2
		for( int i=getDimension()-1; i>=0; i-- ) {
			e = entryAt(i);	
			bbbloc = e.getBBBloc();
			if( bbbloc.containState(s1) )
				if( bbbloc.containState(s2) )
					if( e.getBBBloc1().containState(s1) ){
						if( !e.getBBBloc1().containState(s2) ) 
							return e;
					}
					else {
						if( !e.getBBBloc2().containState(s2) ) 
							return e;
					}
		}
		
		return null;
	}	

	//	public LogSplittingEntry getDeppestCause(
	//		PTState s1, PTState s2
	//	) {
	//		LogSplittingEntry e;
	//		PTStatesSet ptstatesSet;
	//		
	//		//Cerco il blocco pi� in profondit� che contiene s1 s2
	//		for( int i=getDimension()-1; i>=0; i-- ) {
	//			e = entryAt(i);	
	//			ptstatesSet = e.getPTStatesSet();
	//			if( ptstatesSet.containPTState(s1) )
	//				if( ptstatesSet.containPTState(s2) )
	//					return e;
	//		}
	//		
	//		return null;
	//	}	
	//	
	//	public LogSplittingEntry getDeppestCause(
	//		PTStatesSet ss1, PTStatesSet ss2
	//	) {
	//		LogSplittingEntry e;
	//		PTStatesSet ptstatesSet;
	//		
	//		//Cerco il blocco pi� in profondit� che contiene ss1 ss2
	//		for( int i=getDimension()-1; i>=0; i-- ) {
	//			e = entryAt(i);	
	//			ptstatesSet = e.getPTStatesSet();
	//			if( ptstatesSet.containPTStatesSet(ss1) )
	//				if( ptstatesSet.containPTStatesSet(ss2) )
	//					return e;
	//		}
	//		
	//		return null;
	//	}	

	public void print(java.io.PrintStream console) {
		for( int i=0; i<this.getDimension(); i++ ) {
			console.append("\n");
			entryAt(i).print(console);
			console.append("\n");
		}	
	}
	
}
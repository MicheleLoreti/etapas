package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;


/**
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class PTLogInfo {
	
	private Vector<PTLogInfoEntry> log = null;
	
	public PTLogInfo() {
		log = new Vector<PTLogInfoEntry>();
	}
	
	public void addEntry(PTLogInfoEntry e) {
		if( e!=null ) 
			log.addElement(e);	
	}
	
	public int getDimension() {
		return log.size();	
	}
	
	public PTLogInfoEntry entryAt(int index) {
		try	{ 
			return log.elementAt(index); 
		}
		catch(ArrayIndexOutOfBoundsException e) { 
			return null; 
		}
	}
	
	public Vector<PTLogInfoEntry> getInfo(
		PTStatesSet ss1, 
	    PTStatesSet ss2,
	    Action cause
	) {	
		PTLogInfoEntry e = null;
		int trovati = 0;
		Vector<PTLogInfoEntry> result = new Vector<PTLogInfoEntry>();		
		
		//Cerco cosa differenzia ss1 e ss2 con l'azione cause
		int i = getDimension()-1;
		while( i>=0 && trovati< 2 ) {
			e = entryAt(i);
			if( e.getCause().equals(cause) ) {
				if( e.getOrigin().equals(ss1) ) {
					result.insertElementAt(e,0);
					trovati++;
				}				
				else if( e.getOrigin().equals(ss2) ){
					result.addElement(e);
					trovati++;			
				}
			}
			i--;
		}
		
		if( trovati == 2 ) 
			return result;

		return null;		
	}
	
	
}
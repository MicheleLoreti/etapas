package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessAG;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.Bisimulation;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;



/**
 * La classe MayChecker realizza l'algoritmo di verifica 
 * per l'equivalenza may. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class MayChecker<
	S extends StateInterface,
	A extends ActionInterface
> extends EquivalenceChecker<S, A> {
	
    /** 
     * Costruisce una nuova istanza del checker.
     */
	public MayChecker() {
		super("May");
	}

	/**
	 * Implementa la procedura di verifica dell'equivalenza.
	 */	
	@Override 
	public boolean checkEquivalence() {
		// conversion: automaton -> process
		Lts p1 = Lts.createProcess(	getFirstAutomaton(), getInitState1(), null );
		Lts p2 = Lts.createProcess(	getSecondAutomaton(), getInitState2(), null );
		
		// weak acceptance graph's
		ProcessAG p1WAG = ProcessAG.buildWeakAcceptanceGraph(p1);
		ProcessAG p2WAG = ProcessAG.buildWeakAcceptanceGraph(p2);		
				
		// fusion
		ProcessAG pWAG = ProcessAG.createProcessFusion(p1WAG,p2WAG);

		// clean
		// p1 = null; p2 = null; //p1SAG = null;	p2SAG = null; //System.gc();
		
		// initializing partition 
		PTPartition initialRo = MayUtil.initPTPartition(pWAG);
		
		// refinement
		PTState initialPTState1 = initialRo.getPTState(pWAG.getInitState());
		PTState initialPTState2 = initialRo.getPTState(pWAG.getSecondInitState());
		
		PTLogSplitting logSplitting = null;
		PTLogInfo logInfo = null;
		if( getVerboseSolution() ) {
			logSplitting = new PTLogSplitting();
			logInfo = new PTLogInfo();
		}
		Partition ro = Bisimulation.computePTPartitionRefinement(
			pWAG, initialRo, initialPTState1, initialPTState2, 
			logSplitting, logInfo
		);

		boolean result = ro.checkSameClass(
			pWAG.getInitState(), pWAG.getSecondInitState()
		);
		if( !result && getVerboseSolution() ) {
			MayUtil.whyNotEquivalent(
				logSplitting, logInfo, 
				initialPTState1, initialPTState2,
				initialPTState1.getState().getName(), 
				initialPTState2.getState().getName()
			);
		}
		
		return result;
	}
}
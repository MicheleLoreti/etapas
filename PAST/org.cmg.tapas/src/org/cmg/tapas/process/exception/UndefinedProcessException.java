/**
 * 
 */
package org.cmg.tapas.process.exception;

/**
 * @author loreti
 *
 */
public class UndefinedProcessException extends UnknownNameException {

	public UndefinedProcessException(String process) {
		super( process );
	}

}

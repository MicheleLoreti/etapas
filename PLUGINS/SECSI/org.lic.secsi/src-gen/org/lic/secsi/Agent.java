/**
 */
package org.lic.secsi;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Agent#getName <em>Name</em>}</li>
 *   <li>{@link org.lic.secsi.Agent#getParams <em>Params</em>}</li>
 *   <li>{@link org.lic.secsi.Agent#getLcDecs <em>Lc Decs</em>}</li>
 *   <li>{@link org.lic.secsi.Agent#getS <em>S</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getAgent()
 * @model
 * @generated
 */
public interface Agent extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.lic.secsi.SecsiPackage#getAgent_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.lic.secsi.Agent#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.Variable}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Params</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getAgent_Params()
   * @model containment="true"
   * @generated
   */
  EList<Variable> getParams();

  /**
   * Returns the value of the '<em><b>Lc Decs</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.ChannelsDeclaration}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lc Decs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lc Decs</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getAgent_LcDecs()
   * @model containment="true"
   * @generated
   */
  EList<ChannelsDeclaration> getLcDecs();

  /**
   * Returns the value of the '<em><b>S</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>S</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>S</em>' containment reference.
   * @see #setS(Statement)
   * @see org.lic.secsi.SecsiPackage#getAgent_S()
   * @model containment="true"
   * @generated
   */
  Statement getS();

  /**
   * Sets the value of the '{@link org.lic.secsi.Agent#getS <em>S</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>S</em>' containment reference.
   * @see #getS()
   * @generated
   */
  void setS(Statement value);

} // Agent

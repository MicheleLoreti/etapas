package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Partition;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;


/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBPartition {
	
	private Vector<BBBloc> partition = null;

	public BBPartition() {
		partition = new Vector<BBBloc>();
	}
	
	public BBPartition(BBBloc bloc) {
		partition = new Vector<BBBloc>();
		addBBBloc(bloc);
	}
	
  	public int getDimension() {
  		return partition.size();
  	}

	public void addBBBloc(BBBloc bloc) {
		partition.addElement(bloc);
		bloc.setListOwner(this);
	}

	public BBState getBBState(String name) {
		BBState bbstate = null;
		
		for( int i=0; i<this.getDimension(); i++ ) {
			bbstate = bbblocAt(i).getBBState(name);
			if( bbstate!=null ) 
				return bbstate;
		}
		return bbstate;
	}

	public BBBloc getFirstBBBloc() {
		return partition.elementAt(0);	
	}	
	
	public BBBloc removeFirstBBBloc() {
		return partition.remove(0);	
	}	

	public BBBloc removeBBBloc(int index) {
		return partition.remove(index);	
	}	
	
	public BBBloc removeBBBloc(BBBloc b) {
		int index = getBBBlocIndex(b);
		if( index != -1 ) 
			return partition.remove(index);
			
		return null;	
	}	
	
	public BBBloc bbblocAt(int index) {
		return partition.elementAt(index);	
	}
	
	public int getBBBlocIndex(BBBloc bloc) {
		for( int i=0; i<this.getDimension(); i++ )
			if( bbblocAt(i)==bloc) 
				return i;
				
		return -1;			
	}
	
	public void takeFrom(BBPartition p) {
		while( p.getDimension()>0 )
			addBBBloc(p.removeBBBloc(0));
	}
	
	public boolean isEmpty() {
		return partition.isEmpty();	
	}	

	public boolean containBBBloc(BBBloc bloc) {
		for( int i=0; i<this.getDimension(); i++ )
			if( bbblocAt(i)==bloc ) 
				return true;
				
		return false;
	}

	public Partition toPartition() {
		Partition ro = new Partition();
		BBBloc bbbloc = null;
		BBStatesSetBottom ultimi = null;
		BBStatesSetNotBottom nonultimi = null;
		StatesSet statesSet = null;

		for( int i=0; i<this.getDimension(); i++ ) {
			bbbloc = bbblocAt(i);
			statesSet = new StatesSet();
			ultimi = bbbloc.getBBStatesSetBottom();
			for( int j=0; j<ultimi.getDimension(); j++ )
				statesSet.addState( ultimi.bbstateAt(j).getState() );
			nonultimi = bbbloc.getBBStatesSetNotBottom();
			for( int j=0; j<nonultimi.getDimension(); j++ )
				statesSet.addState( nonultimi.bbstateAt(j).getState() );			
			ro.addStatesSet(statesSet);
		}

		return ro;	
	}

	public void print() {
		printAll(System.out);
	}

	public void printAll(java.io.PrintStream console) {
		int i = 0;
		
		console.append("{\n");
		for( i=0; i<this.getDimension(); i++ ) {
			if(i>0) 
				console.append("\n");
			partition.elementAt(i).printAll(console);
		}
		if(i==0) 
			console.append("empty");
		console.append("\n}");
	}  
}
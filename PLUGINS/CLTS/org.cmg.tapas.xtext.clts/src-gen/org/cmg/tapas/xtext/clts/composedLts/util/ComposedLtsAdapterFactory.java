/**
 */
package org.cmg.tapas.xtext.clts.composedLts.util;

import org.cmg.tapas.xtext.clts.composedLts.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage
 * @generated
 */
public class ComposedLtsAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static ComposedLtsPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComposedLtsAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = ComposedLtsPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComposedLtsSwitch<Adapter> modelSwitch =
    new ComposedLtsSwitch<Adapter>()
    {
      @Override
      public Adapter caseModel(Model object)
      {
        return createModelAdapter();
      }
      @Override
      public Adapter caseElement(Element object)
      {
        return createElementAdapter();
      }
      @Override
      public Adapter caseLts(Lts object)
      {
        return createLtsAdapter();
      }
      @Override
      public Adapter caseLtsBody(LtsBody object)
      {
        return createLtsBodyAdapter();
      }
      @Override
      public Adapter caseLtsComposition(LtsComposition object)
      {
        return createLtsCompositionAdapter();
      }
      @Override
      public Adapter caseCompositionBody(CompositionBody object)
      {
        return createCompositionBodyAdapter();
      }
      @Override
      public Adapter caseReference(Reference object)
      {
        return createReferenceAdapter();
      }
      @Override
      public Adapter caseMapping(Mapping object)
      {
        return createMappingAdapter();
      }
      @Override
      public Adapter caseLtsDeclarationBody(LtsDeclarationBody object)
      {
        return createLtsDeclarationBodyAdapter();
      }
      @Override
      public Adapter caseLtsRule(LtsRule object)
      {
        return createLtsRuleAdapter();
      }
      @Override
      public Adapter caseLtsState(LtsState object)
      {
        return createLtsStateAdapter();
      }
      @Override
      public Adapter caseLabelRule(LabelRule object)
      {
        return createLabelRuleAdapter();
      }
      @Override
      public Adapter caseState(State object)
      {
        return createStateAdapter();
      }
      @Override
      public Adapter caseRule(Rule object)
      {
        return createRuleAdapter();
      }
      @Override
      public Adapter caseLabel(Label object)
      {
        return createLabelAdapter();
      }
      @Override
      public Adapter caseAction(Action object)
      {
        return createActionAdapter();
      }
      @Override
      public Adapter caseFormula(Formula object)
      {
        return createFormulaAdapter();
      }
      @Override
      public Adapter caseHmlFormulaDeclaration(HmlFormulaDeclaration object)
      {
        return createHmlFormulaDeclarationAdapter();
      }
      @Override
      public Adapter caseHmlFormula(HmlFormula object)
      {
        return createHmlFormulaAdapter();
      }
      @Override
      public Adapter caseHmlPredicate(HmlPredicate object)
      {
        return createHmlPredicateAdapter();
      }
      @Override
      public Adapter caseHmlTrue(HmlTrue object)
      {
        return createHmlTrueAdapter();
      }
      @Override
      public Adapter caseHmlFalse(HmlFalse object)
      {
        return createHmlFalseAdapter();
      }
      @Override
      public Adapter caseHmlNotFormula(HmlNotFormula object)
      {
        return createHmlNotFormulaAdapter();
      }
      @Override
      public Adapter caseHmlDiamondFormula(HmlDiamondFormula object)
      {
        return createHmlDiamondFormulaAdapter();
      }
      @Override
      public Adapter caseHmlBoxFormula(HmlBoxFormula object)
      {
        return createHmlBoxFormulaAdapter();
      }
      @Override
      public Adapter caseHmlRecursionFormula(HmlRecursionFormula object)
      {
        return createHmlRecursionFormulaAdapter();
      }
      @Override
      public Adapter caseHmlMinFixPoint(HmlMinFixPoint object)
      {
        return createHmlMinFixPointAdapter();
      }
      @Override
      public Adapter caseHmlMaxFixPoint(HmlMaxFixPoint object)
      {
        return createHmlMaxFixPointAdapter();
      }
      @Override
      public Adapter caseHmlRecursionVariable(HmlRecursionVariable object)
      {
        return createHmlRecursionVariableAdapter();
      }
      @Override
      public Adapter caseLtlFormulaDeclaration(LtlFormulaDeclaration object)
      {
        return createLtlFormulaDeclarationAdapter();
      }
      @Override
      public Adapter caseLtlFormula(LtlFormula object)
      {
        return createLtlFormulaAdapter();
      }
      @Override
      public Adapter caseLtlAtomicProposition(LtlAtomicProposition object)
      {
        return createLtlAtomicPropositionAdapter();
      }
      @Override
      public Adapter caseLtlNextFormula(LtlNextFormula object)
      {
        return createLtlNextFormulaAdapter();
      }
      @Override
      public Adapter caseLtlAlwaysFormula(LtlAlwaysFormula object)
      {
        return createLtlAlwaysFormulaAdapter();
      }
      @Override
      public Adapter caseLtlEventuallyFormula(LtlEventuallyFormula object)
      {
        return createLtlEventuallyFormulaAdapter();
      }
      @Override
      public Adapter caseLtlNotFormula(LtlNotFormula object)
      {
        return createLtlNotFormulaAdapter();
      }
      @Override
      public Adapter caseLtlTrue(LtlTrue object)
      {
        return createLtlTrueAdapter();
      }
      @Override
      public Adapter caseLtlFalse(LtlFalse object)
      {
        return createLtlFalseAdapter();
      }
      @Override
      public Adapter caseLabelPredicate(LabelPredicate object)
      {
        return createLabelPredicateAdapter();
      }
      @Override
      public Adapter caseNotLabelPredicate(NotLabelPredicate object)
      {
        return createNotLabelPredicateAdapter();
      }
      @Override
      public Adapter caseActionLabelPredicate(ActionLabelPredicate object)
      {
        return createActionLabelPredicateAdapter();
      }
      @Override
      public Adapter caseAnyLabelPredicate(AnyLabelPredicate object)
      {
        return createAnyLabelPredicateAdapter();
      }
      @Override
      public Adapter caseInterleaving(Interleaving object)
      {
        return createInterleavingAdapter();
      }
      @Override
      public Adapter caseSynchronization(Synchronization object)
      {
        return createSynchronizationAdapter();
      }
      @Override
      public Adapter caseControlledInteraction(ControlledInteraction object)
      {
        return createControlledInteractionAdapter();
      }
      @Override
      public Adapter caseRenaming(Renaming object)
      {
        return createRenamingAdapter();
      }
      @Override
      public Adapter caseHmlOrFormula(HmlOrFormula object)
      {
        return createHmlOrFormulaAdapter();
      }
      @Override
      public Adapter caseHmlAndFormula(HmlAndFormula object)
      {
        return createHmlAndFormulaAdapter();
      }
      @Override
      public Adapter caseLtlOrFormula(LtlOrFormula object)
      {
        return createLtlOrFormulaAdapter();
      }
      @Override
      public Adapter caseLtlAndFormula(LtlAndFormula object)
      {
        return createLtlAndFormulaAdapter();
      }
      @Override
      public Adapter caseLtlUntilFormula(LtlUntilFormula object)
      {
        return createLtlUntilFormulaAdapter();
      }
      @Override
      public Adapter caseCupLabelPredicate(CupLabelPredicate object)
      {
        return createCupLabelPredicateAdapter();
      }
      @Override
      public Adapter caseCapLabelPredicate(CapLabelPredicate object)
      {
        return createCapLabelPredicateAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Model
   * @generated
   */
  public Adapter createModelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Element
   * @generated
   */
  public Adapter createElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Lts <em>Lts</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Lts
   * @generated
   */
  public Adapter createLtsAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsBody <em>Lts Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsBody
   * @generated
   */
  public Adapter createLtsBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsComposition <em>Lts Composition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsComposition
   * @generated
   */
  public Adapter createLtsCompositionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.CompositionBody <em>Composition Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.CompositionBody
   * @generated
   */
  public Adapter createCompositionBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Reference
   * @generated
   */
  public Adapter createReferenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Mapping <em>Mapping</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Mapping
   * @generated
   */
  public Adapter createMappingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody <em>Lts Declaration Body</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody
   * @generated
   */
  public Adapter createLtsDeclarationBodyAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule <em>Lts Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsRule
   * @generated
   */
  public Adapter createLtsRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsState <em>Lts State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsState
   * @generated
   */
  public Adapter createLtsStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule <em>Label Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LabelRule
   * @generated
   */
  public Adapter createLabelRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.State <em>State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.State
   * @generated
   */
  public Adapter createStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Rule <em>Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Rule
   * @generated
   */
  public Adapter createRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Label <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Label
   * @generated
   */
  public Adapter createLabelAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Action
   * @generated
   */
  public Adapter createActionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Formula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Formula
   * @generated
   */
  public Adapter createFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration <em>Hml Formula Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration
   * @generated
   */
  public Adapter createHmlFormulaDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormula <em>Hml Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFormula
   * @generated
   */
  public Adapter createHmlFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlPredicate <em>Hml Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlPredicate
   * @generated
   */
  public Adapter createHmlPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlTrue <em>Hml True</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlTrue
   * @generated
   */
  public Adapter createHmlTrueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFalse <em>Hml False</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFalse
   * @generated
   */
  public Adapter createHmlFalseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula <em>Hml Not Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula
   * @generated
   */
  public Adapter createHmlNotFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula <em>Hml Diamond Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula
   * @generated
   */
  public Adapter createHmlDiamondFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula <em>Hml Box Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula
   * @generated
   */
  public Adapter createHmlBoxFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula <em>Hml Recursion Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula
   * @generated
   */
  public Adapter createHmlRecursionFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlMinFixPoint <em>Hml Min Fix Point</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlMinFixPoint
   * @generated
   */
  public Adapter createHmlMinFixPointAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlMaxFixPoint <em>Hml Max Fix Point</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlMaxFixPoint
   * @generated
   */
  public Adapter createHmlMaxFixPointAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable <em>Hml Recursion Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable
   * @generated
   */
  public Adapter createHmlRecursionVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration <em>Ltl Formula Declaration</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration
   * @generated
   */
  public Adapter createLtlFormulaDeclarationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormula <em>Ltl Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFormula
   * @generated
   */
  public Adapter createLtlFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition <em>Ltl Atomic Proposition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition
   * @generated
   */
  public Adapter createLtlAtomicPropositionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula <em>Ltl Next Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula
   * @generated
   */
  public Adapter createLtlNextFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula <em>Ltl Always Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula
   * @generated
   */
  public Adapter createLtlAlwaysFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula <em>Ltl Eventually Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula
   * @generated
   */
  public Adapter createLtlEventuallyFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula <em>Ltl Not Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula
   * @generated
   */
  public Adapter createLtlNotFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlTrue <em>Ltl True</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlTrue
   * @generated
   */
  public Adapter createLtlTrueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFalse <em>Ltl False</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFalse
   * @generated
   */
  public Adapter createLtlFalseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LabelPredicate <em>Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LabelPredicate
   * @generated
   */
  public Adapter createLabelPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate <em>Not Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate
   * @generated
   */
  public Adapter createNotLabelPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate <em>Action Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate
   * @generated
   */
  public Adapter createActionLabelPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.AnyLabelPredicate <em>Any Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.AnyLabelPredicate
   * @generated
   */
  public Adapter createAnyLabelPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Interleaving <em>Interleaving</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Interleaving
   * @generated
   */
  public Adapter createInterleavingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Synchronization <em>Synchronization</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Synchronization
   * @generated
   */
  public Adapter createSynchronizationAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction <em>Controlled Interaction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction
   * @generated
   */
  public Adapter createControlledInteractionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.Renaming <em>Renaming</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.Renaming
   * @generated
   */
  public Adapter createRenamingAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula <em>Hml Or Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula
   * @generated
   */
  public Adapter createHmlOrFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula <em>Hml And Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula
   * @generated
   */
  public Adapter createHmlAndFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula <em>Ltl Or Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula
   * @generated
   */
  public Adapter createLtlOrFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula <em>Ltl And Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula
   * @generated
   */
  public Adapter createLtlAndFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula <em>Ltl Until Formula</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula
   * @generated
   */
  public Adapter createLtlUntilFormulaAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate <em>Cup Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate
   * @generated
   */
  public Adapter createCupLabelPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate <em>Cap Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate
   * @generated
   */
  public Adapter createCapLabelPredicateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //ComposedLtsAdapterFactory

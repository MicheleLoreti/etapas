/**
 */
package org.lic.secsi;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Channels Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.ChannelsDeclaration#getDeclared <em>Declared</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getChannelsDeclaration()
 * @model
 * @generated
 */
public interface ChannelsDeclaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Declared</b></em>' containment reference list.
   * The list contents are of type {@link org.lic.secsi.Channel}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Declared</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Declared</em>' containment reference list.
   * @see org.lic.secsi.SecsiPackage#getChannelsDeclaration_Declared()
   * @model containment="true"
   * @generated
   */
  EList<Channel> getDeclared();

} // ChannelsDeclaration

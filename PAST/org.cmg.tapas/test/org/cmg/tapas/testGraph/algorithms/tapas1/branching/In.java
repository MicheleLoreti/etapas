package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;


/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class In {
	
	private Vector<NotInertTransition> transitions = null;	

	public In()	{
		this.transitions = new Vector<NotInertTransition>();
  	}
  	
  	public int getDimension() {
  		return transitions.size();
  	}

	/**
	 * Inserisce una transizione non inerte senza duplicati e ordinata 
	 * lessicograficamente.
	 * Restituisce true se la transizione non era gi� presente nell'insieme
	 **/
	public int addNotInertTransition(NotInertTransition t) {	
		int i = 0;
		
		for( i=0; i<getDimension(); i++ )
			if( notInertTransitionAt(i).compareTo(t)>=0 )
				break;
				
		transitions.add(i,t);
		return i;
	}

	public Vector<NotInertTransition> getNotInertTransitionsWith(Action a) {
		NotInertTransition t = null;
		int compare;
		Vector<NotInertTransition> result = new Vector<NotInertTransition>();
		
		if( a.isTau() ) {
			for( int j=0; j<getDimension(); j++ ) {
				t = notInertTransitionAt(j);
				compare = t.getAction().compareTo(a);
				if( compare == 0 ) 
					result.add(t);
				else 
					break;
			}			
		} else {
			for( int j=0; j<getDimension(); j++) {
				t = notInertTransitionAt(j);
				if( !t.getAction().isTau() ) {
					compare = t.getAction().compareTo(a);
					if( compare == 0 ) 
						result.add(t);
					else if (compare > 0) 
						break;
				}
			}
		}
		
		return result;
	}
	
	public NotInertTransition removeNotInertTransitionAt(int index) {
		try {
			return transitions.remove(index);
		}
		catch(ArrayIndexOutOfBoundsException unused) {
			return null;
		}
	}

	public NotInertTransition notInertTransitionAt(int index) {
		try {
			return transitions.elementAt(index);
		}	
		catch(ArrayIndexOutOfBoundsException unused) {
			return null;
		}
	}
	
	public boolean isEmpty() {
		if( getDimension()==0 ) 
			return true;
		
		return false;
	}

	public void print(java.io.PrintStream console) {
		int i = 0;
		
		for( i=0; i<getDimension(); i++ ){
			if( i>0 ) 
				console.append("\n           ");
			notInertTransitionAt(i).print(console);
		}
				
		if(i==0) 
			console.append("empty");
	}

}
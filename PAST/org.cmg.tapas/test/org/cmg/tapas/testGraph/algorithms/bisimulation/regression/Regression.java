package org.cmg.tapas.testGraph.algorithms.bisimulation.regression;


/** 
 * Calcola, col metodo dei minimi quadrati,
 * la dipendenza tra due vettori di valori.
 *
 * @author Guzman Tierno
 **/
public class Regression {		
	// variabile
	private double[] x; 
	// valori associati
	private double[] y; 

	// pendenza
	private double slope;
	// altezza sulle ordinate
	private double quote;
	// livello di correlazione tra la variabile e i valore
	private double correlation;
	// tipo di regressione
	private int regType;
	
	// Tipi possibili di regressione
	public static final int LINEAR = 0; 			// y = A*x + B
	public static final int LOGARITMIC = 1;			// y = A*ln(x) + B
	public static final int EXPONENTIAL = 2;		// y = exp( A*x + B )
	public static final int POLYNOMIAL = 3;			// y = exp(B) * x^A
	public static final int QUADRATIC = 4;			// y = A*x^2 + B*x
	public static final int LINEARITMIC = 5;		// y = A*x*ln(x) + B*x
	public static final int LINEAR_LOG = 6;			// y = A*x + B*ln(x)
	
	// Costruttore
	public Regression(double[] vx, double[] vy,int regType) {
		x = vx;
		y = vy;
		this.regType = regType;		
		 
		switch(regType) {
			case LOGARITMIC: 	// y = A*x + B <-> vy = A*ln(vx) + B
				x = ln(vx);									
				break;
			case POLYNOMIAL: 	// y = A*x + B <-> vy = exp(B) * vx^A
				x = ln(vx);		
				y = ln(vy);
				break;
			case EXPONENTIAL: 	// y = A*x + B <-> vy = exp(A*vx) * exp(B)
				y = ln(vy);		
				break;
			case QUADRATIC: 	// y = A*x + B <-> vy = A*vx^2 + B*vx
				y = div(vy,vx);	
				break;
			case LINEARITMIC:	// y = A*x + B <-> vy = A*vx*ln(vx) + B*vx 
				y = div(vy,vx);
				x = ln(vx);
				break;
			case LINEAR_LOG:	// y = A*x + B*ln(x) 
				linearLog();
				return;			// RETURN
		}		
				
		int s0 = x.length;
		double s1 = sum(x);							 
		double s2 = cross(x,x); 
		
		double t1 = sum(y);
		double t2 = cross(x,y);
		double avgX = s1/s0;
		double avgY = t1/s0;
		
		double[] deltaX = new double[s0];
		double[] deltaY = new double[s0];
		for( int i=0; i<s0; ++i ) {
			deltaX[i] = x[i] - avgX;
			deltaY[i] = y[i] - avgY;
		}	
		
		slope = (s1*t1 - s0*t2) / (s1*s1 - s0*s2);
			  
		quote = (s1*t2 - s2*t1) / (s1*s1 - s0*s2);
			
		correlation = 
			Math.abs(cross(deltaX,deltaY)) 
			/ Math.sqrt( cross(deltaX,deltaX) * cross(deltaY,deltaY) );
		
	}
	
	private void linearLog() {
		double[] lnx = ln(x);
		double xx = cross(x,x); 
		double lnx2 = cross(lnx,lnx); 
		double xlnx = cross(x,lnx); 
		double ylnx = cross(y,lnx); 
		double xy = cross(x,y);
		
		double det = xx * lnx2 - xlnx * xlnx;
		
		slope = (xy * lnx2 - xlnx * ylnx) / det;
			  
		quote = (xx * ylnx - xlnx * xy ) / det;
			  
		correlation = 0;  //unknown formula
	}
		
	public double getType() {
		return regType;
	}

	public double getSlope() {
		return slope;
	}

	public double getQuote() {
		return slope;
	}

	public double getCorrelation() {
		return correlation;
	}

	public static double sum(double[] x) {
		double s=0;
		for( int i=0; i<x.length; ++i ) 
			s += x[i];
		
		return s;
	}		
		
	public static double cross(double[] x, double[] y) {
		double s=0;
		for( int i=0; i<x.length; ++i ) 
			s += x[i]*y[i];
		
		return s;
	}	
	
	public static double[] ln(double[] x) {
		double[] r = new double[x.length];
		for( int i=0; i<x.length; ++i ) 
			r[i] = Math.log(x[i]);
		
		return r;
	}
	
	public static double[] div(double[] a,double[] b) {
		double[] r = new double[a.length];
		for( int i=0; i<a.length; ++i ) 
			r[i] = a[i]/b[i];
		
		return r;
	}
	
	public String toString() {
		String s = "";
		double A = ((int)(slope*1000))/1000.0;
		//double A = slope;
		double B = ((int)(quote*1000))/1000.0;
		//double B = quote;
		double expB = ((int)(Math.exp(quote)*1000))/1000.0;
		//double expB = Math.exp(quote);
		
		switch(regType) {
			case LINEAR:
				s = "Dipendenza lineare, y = " 
					+ A + " * x + " + B;
			break;
			case LOGARITMIC: 
				s = "Dipendenza logaritmica, y = " 
					+ A + " * ln(x) + " + B;
			break;
			case POLYNOMIAL: 
				s = "Dipendenza polinomiale, y = " 
					+ expB + " * x ^ " + A;
			break;
			case EXPONENTIAL: 
				s = "Dipendenza esponenziale, y = " 
					+ expB + " * exp(" + A + " * x)";
			break;
			case QUADRATIC: 
				s = "Dipendenza quadratica, y = " 
					+ A + " * x^2 + " + B + " * x";
			break;
			case LINEARITMIC: 
				s = "Dipendenza linearitmica, y = " 
					+ A + " *x*ln(x) + " + B + " *x";
			break;
			case LINEAR_LOG: 
				s = "Dipendenza lineare+logaritmica, y = " 
					+ A + " *x + " + B + " *ln(x)";
			break;
		}	
				
		s += " \n corr. = " + correlation;
		
		return s;		
	}
	
}	
		
		
		
		
	

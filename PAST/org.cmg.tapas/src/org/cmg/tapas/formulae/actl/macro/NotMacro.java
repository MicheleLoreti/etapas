/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.state.ActlNot;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class NotMacro< S extends APProcess<S,A>,A extends ActionInterface> extends StateFormulaMacro<S, A>{
	
	private StateFormulaMacro<S, A> macro;
	
	public NotMacro( StateFormulaMacro<S, A> macro ) {
		this.macro = macro;
	}
	
	@Override
	public ActlFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		return new ActlNot<S, A>(macro.getFormula(f));
	}

	@Override
	public int parameters() {
		return macro.parameters();
	}

	@Override
	public void acceptVisitor(MacroVisitor<S, A> visitor) {
		visitor.visitNotMacro(this);
	}

	public StateFormulaMacro<S, A> getArgument() {
		return macro;
	}
	
}

/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.HmlMaxFixPoint;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hml Max Fix Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HmlMaxFixPointImpl extends HmlRecursionFormulaImpl implements HmlMaxFixPoint
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected HmlMaxFixPointImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.HML_MAX_FIX_POINT;
  }

} //HmlMaxFixPointImpl

package org.cmg.tapas.graph.algorithms;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;

/** 
 * DepthVisitorAdapter d� una implementazione a vuoto 
 * di DepthVisitor.
 * <p>
 * @param <S> tipo degli stati
 * @param <A> tipo delle azioni
 * 
 * @author Guzman Tierno
 **/
public class DepthVisitorAdapter<
	S extends StateInterface,
	A extends ActionInterface
> implements DepthVisitor<S,A> {
	public boolean goingTo(
		int fromDepth, S from, int fromIndex, A action, S to, int toIndex
	) {
		return true;
	}

	public boolean comingBack(
		int toDepth, S to, int toIndex, A action, S from, int fromIndex
	) {
		return true;
	}

	public boolean hitMarked(
		int fromDepth, S from, int fromIndex, A action, S hit, int hitIndex
	) {
		return true;
	}
}
	

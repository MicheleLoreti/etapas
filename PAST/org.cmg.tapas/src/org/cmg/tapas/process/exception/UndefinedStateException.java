/**
 * 
 */
package org.cmg.tapas.process.exception;

/**
 * @author loreti
 *
 */
public class UndefinedStateException extends UnknownNameException {

	private String name;

	public UndefinedStateException(String state) {
		super( "Unknown name "+state );
		this.name = state;
	}

	public String getName() {
		return name;
	}
}

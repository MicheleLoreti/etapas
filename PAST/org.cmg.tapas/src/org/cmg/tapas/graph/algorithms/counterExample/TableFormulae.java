package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public class TableFormulae<S extends APProcess<S, A>, A extends ActionInterface> 
			implements Cloneable{

	private LinkedList<Object> partition;
	private Vector<Vector<LinkedList<HmlFormula<S, A>>>> formulae;
	private boolean added;
	
	protected TableFormulae(){
		partition = new LinkedList<Object>();
		formulae = new Vector<Vector<LinkedList<HmlFormula<S,A>>>>();
		added = false;
	}
	
	protected TableFormulae(Set<Object> partition){
		this.partition = new LinkedList<Object>(partition);
		added = false;
		
		int size = partition.size();
		formulae = new Vector<Vector<LinkedList<HmlFormula<S,A>>>>(size);
		
		for (int i = 0; i < size; i++) {
			Vector<LinkedList<HmlFormula<S,A>>> v = new Vector<LinkedList<HmlFormula<S,A>>>(size);
			for (int j = 0; j < size; j++) {
				LinkedList<HmlFormula<S,A>> subV = new LinkedList<HmlFormula<S,A>>();
				v.add(j, subV);
			}
			formulae.add(i, v);
		}
	}
	
	private boolean formulaIsEmpty(int i, int j){
		LinkedList<HmlFormula<S,A>> l = formulae.get(i).get(j);
		if(l == null || l.size() == 0)
			return true;
		
		return false;
	}

	private LinkedList<HmlFormula<S, A>> getFormulae(int i, int j){
		return formulae.get(i).get(j);
	}

	protected void addFormula(Object s1, Object s2, HmlFormula<S,A> f){
		int i = partition.indexOf(s1);
		int j = partition.indexOf(s2);
		addFormula(i, j, f);
	}
	
	protected void addFormula(int i, int j, HmlFormula<S,A> f){
		LinkedList<HmlFormula<S, A>> l = formulae.get(i).get(j);
		l.add(f);
		added = true;
	}
	
	protected void addFormulae(int i, int j, LinkedList<HmlFormula<S, A>> f){
		if(f != null && f.size() > 0){
			LinkedList<HmlFormula<S, A>> l = formulae.get(i).get(j);
			l.addAll(f);
			added = true;
		}
	}
	
	protected void addFormulae(Object s1, Object s2, LinkedList<HmlFormula<S, A>> f){
		int i = partition.indexOf(s1);
		int j = partition.indexOf(s2);
		addFormulae(i, j, f);
	}	
	
	protected boolean formulaIsEmpty(Object hs1, Object hs2){
		int i = partition.indexOf(hs1);
		int j = partition.indexOf(hs2);
		return formulaIsEmpty(i, j);
	}
	
	protected LinkedList<HmlFormula<S, A>> getFormulae(Object hs1, Object hs2){
		int i = partition.indexOf(hs1);
		int j = partition.indexOf(hs2);
		return getFormulae(i, j);
	}
	
	protected HmlFormula<S, A> getFormula(Object hs1, Object hs2){
		LinkedList<HmlFormula<S, A>> fList = getFormulae(hs1, hs2);
		if(fList.size()>0)
			return fList.getFirst();
		else
			return null;
	}
	
	protected LinkedList<Object> getPartitionList(){
		return partition;
	}
	
	protected Object getObject(int index){
		return partition.get(index);
	}
	
	public String toString(){
		String res = "";
		
		for (Object hs : partition) {
			res += hs.toString()+": \n";
			Vector<LinkedList<HmlFormula<S, A>>> f = formulae.get(partition.indexOf(hs));
			for (LinkedList<HmlFormula<S, A>> ll : f) {
				res += "\t\t"+ll.toString()+"\n";
			}
			res += "\n";
		}
		
		return res;
	}
	
	public String getFormattedTable(){
		String res = "";		
		//Per ogni riga
		String sep = ""; 
		for (Vector<LinkedList<HmlFormula<S, A>>> row : formulae) {
			for (LinkedList<HmlFormula<S, A>> cell : row) {
					res += "\"";
					for (HmlFormula<S, A> f : cell) {
						res += sep+f.toString();
						sep = "\n";
					}
					sep = "";
					res+= "\"\t";;
			}
			res += "\n";
		}
		
		return res;
	}
	
	protected void reset(){
		int size = partition.size();
		added = false;
		
		formulae = new Vector<Vector<LinkedList<HmlFormula<S,A>>>>(size);
		
		for (int i = 0; i < size; i++) {
			Vector<LinkedList<HmlFormula<S,A>>> v = new Vector<LinkedList<HmlFormula<S,A>>>(size);
			for (int j = 0; j < size; j++) {
				LinkedList<HmlFormula<S,A>> subV = new LinkedList<HmlFormula<S,A>>();
				v.add(j, subV);
			}
			formulae.add(i, v);
		}
	}
	
	protected boolean added(){
		return added;
	}
	
	protected void resetAdded(){
		added = false;
	}
	
	public TableFormulae<S, A> clone(){
		TableFormulae<S, A> res = new TableFormulae<S, A>();
		res.partition = (LinkedList) partition.clone();
		int size = res.partition.size();
		res.formulae = new Vector<Vector<LinkedList<HmlFormula<S,A>>>>(size);
		
		for (int i = 0; i < size; i++) {
			Vector<LinkedList<HmlFormula<S,A>>> v = new Vector<LinkedList<HmlFormula<S,A>>>(size);
			for (int j = 0; j < size; j++) {
				LinkedList<HmlFormula<S,A>> subV = (LinkedList<HmlFormula<S, A>>) formulae.get(i).get(j).clone();
				v.add(j, subV);
			}
			res.formulae.add(i, v);
		}

		res.added = false;
		return res;
	}
}

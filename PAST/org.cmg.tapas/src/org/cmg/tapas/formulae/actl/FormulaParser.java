package org.cmg.tapas.formulae.actl;

import java.io.File;
import java.io.IOException;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * 
 */

/**
 * @author loreti
 *
 */
public interface FormulaParser< S extends APProcess<S, A> , A extends ActionInterface > {

	public ActlSuite<S, A> loadSuite( File file ) throws Exception;
	public void saveSuite( File file , ActlSuite<S,A> suite ) throws IOException;
	
}

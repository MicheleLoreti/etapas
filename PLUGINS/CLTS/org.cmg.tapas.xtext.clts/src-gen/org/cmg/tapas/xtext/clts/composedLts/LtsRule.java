/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lts Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getSrc <em>Src</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getAct <em>Act</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getTrg <em>Trg</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsRule()
 * @model
 * @generated
 */
public interface LtsRule extends EObject
{
  /**
   * Returns the value of the '<em><b>Src</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src</em>' reference.
   * @see #setSrc(LtsState)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsRule_Src()
   * @model
   * @generated
   */
  LtsState getSrc();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getSrc <em>Src</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src</em>' reference.
   * @see #getSrc()
   * @generated
   */
  void setSrc(LtsState value);

  /**
   * Returns the value of the '<em><b>Act</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Act</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Act</em>' reference.
   * @see #setAct(Action)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsRule_Act()
   * @model
   * @generated
   */
  Action getAct();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getAct <em>Act</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Act</em>' reference.
   * @see #getAct()
   * @generated
   */
  void setAct(Action value);

  /**
   * Returns the value of the '<em><b>Trg</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trg</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trg</em>' reference.
   * @see #setTrg(LtsState)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtsRule_Trg()
   * @model
   * @generated
   */
  LtsState getTrg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getTrg <em>Trg</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trg</em>' reference.
   * @see #getTrg()
   * @generated
   */
  void setTrg(LtsState value);

} // LtsRule

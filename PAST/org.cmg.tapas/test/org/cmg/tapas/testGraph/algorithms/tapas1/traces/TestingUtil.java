package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.AcceptanceSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessAG;
import org.cmg.tapas.testGraph.algorithms.tapas1.State;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.Trace;
import org.cmg.tapas.testGraph.algorithms.tapas1.TransitionCouple;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfoEntry;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplittingEntry;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTStatesSet;



/**
 * La classe TestingUtil contiene metodi statici per calcolare 
 * l'acceptance graph associato ad un processo.  
 * <p>
 * Definizioni utili:
 *
 *		CP(p) = azioni uscenti da p
 *		DP(p,a) = immagine di p sotto a
 * 		S^epsilon = chiusura di S rispetto a tau
 * 		AP(S) = { CP(p) | p in S e DP(p,tau) vuoto }
 * 		BuildAcceptanceGraph(P) = 
 *			costruisce l'acceptance graph associato ad un processo P
 *
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class TestingUtil {
	public static void whyNotEquivalent(
		PTLogSplitting log1,
		PTLogInfo log2,
	    PTState s1, 
	    PTState s2,
	    String nome1,
	    String nome2,
	    ProcessAG pAG
	) {
		java.io.PrintStream console = System.out;
		PTLogSplittingEntry e1 = null;
		PTLogInfoEntry e2 = null;
		PTPartition splittedSet = null;
		PTStatesSet ss1 = null;
		PTStatesSet ss2 = null;
		PTStatesSet ss1d1 = null;
		PTStatesSet ss2d1 = null;
		State final1 = null;
		State final2 = null;
		StatesSet finals1 = null;
		StatesSet finals2 = null;
		boolean canDo1 = true;
		boolean canDo2 = true;
		boolean ss1not = false;
		boolean ss2not = false;
		Trace traccia = new Trace();
		Vector<PTLogInfoEntry> info = null;

		console.println("because ");

		// Cerco la causa dello splitting in LogSplitting		
		e1 = log1.getDeppestCause(s1,s2);
		
		// Se non la trovo allora mi fermo
		// I due stati non erano nella stessa classe fin dall'inizio
		if (e1==null) {
			final1 = s1.getState();	
			final2 = s2.getState();
			canDo1 = true;			
			canDo2 = true;			
		} else {
			// Recupero i blocchi che contengono s1 e s2, 
			// rispettivamente ss1 e ss2
			splittedSet = e1.getSplittedSet();
			if (splittedSet.ptstatesSetAt(0).containPTState(s1)) {
				ss1 = splittedSet.ptstatesSetAt(0);
				ss2 = splittedSet.ptstatesSetAt(1);
			} else {
				ss2 = splittedSet.ptstatesSetAt(0);
				ss1 = splittedSet.ptstatesSetAt(1);
			}

			while (true) {
				// Cerco in LogInfo i blocchi rispetto ai quale sono 
				// stati splittati ss1 e ss2
				info = log2.getInfo(ss1,ss2,e1.getCause());
				e2 = info.elementAt(0);
				ss1d1 = e2.getFirstDestination();
				ss1not = e2.isNot();
				e2 = info.elementAt(1);
				ss2d1 = e2.getFirstDestination();
				ss2not = e2.isNot();
				// aggiungi cause alla soluzione
				traccia.append(e1.getCause());
				if( !( ss1not || ss2not ) ) {
					ss1 = ss1d1;
					ss2 = ss2d1;
					// Cerco la causa dello splitting in LogSplitting
					e1 = log1.getDeppestCause(ss1,ss2);
					if( e1==null ) {
						final1 = ss1.ptstateAt(0).getState();	
						final2 = ss2.ptstateAt(0).getState();
						canDo1 = true;			
						canDo2 = true;		
						break;	
					}					
				} else {
					// prima di uscire devo controllare che veramente non siano
					// equivalenti a tracce
					if (ss1not)	{
						canDo1 = false;
						canDo2 = true;
						for (int i=0; i<ss1.getDimension(); i++) {
							finals1 = ss1.ptstateAt(i).getState().dP(e1.getCause());
							if (finals1.getDimension() > 0) {
								final1 = finals1.stateAt(0);
								for (int k=0; k<ss2.getDimension(); k++) {
									finals2 = ss2.ptstateAt(k).getState().dP(e1.getCause());
									finals2 = ss2d1.intersection(finals2);
									if (finals2.getDimension()>0) {
										final2 = finals2.stateAt(0);
										break;
									}
								}
								canDo1 = true;
								break;	
							}
						}						
					} else {
						canDo1 = true;
						canDo2 = false;
						for (int i=0; i<ss2.getDimension(); i++) {
							finals2 = ss2.ptstateAt(i).getState().dP(e1.getCause());
							if (finals2.getDimension() > 0) {
								final2 = finals2.stateAt(0);
								for (int k=0; k<ss1.getDimension(); k++) {
									finals1 = ss1.ptstateAt(k).getState().dP(e1.getCause());
									finals1 = ss1d1.intersection(finals1);
									if (finals1.getDimension()>0) {
										final1 = finals1.stateAt(0);
										break;
									}
								}								
								canDo2 = true;
								break;	
							}
						}						
					}
					// prima di uscire, devo controllare che gli stati finali
					// non siano stati separati da un precedente splitting
					if( canDo1 && canDo2 ) {
						// Cerco la causa dello splitting in LogSplitting
						e1 = log1.getDeppestCause(final1,final2);
						// Se non la trovo allora mi fermo
						// I due stati non erano nella stessa classe fin dall'inizio
						if (e1==null) 
							break;

						// Recupero i blocchi che contengono final1 e final2, 
						// rispettivamente ss1 e ss2
						splittedSet = e1.getSplittedSet();
						if (splittedSet.ptstatesSetAt(0).containState(final1)) {
							ss1 = splittedSet.ptstatesSetAt(0);
							ss2 = splittedSet.ptstatesSetAt(1);
						} else {
							ss2 = splittedSet.ptstatesSetAt(0);
							ss1 = splittedSet.ptstatesSetAt(1);
						}
					}
					else 
						break;
				}
			}
		}

		//Stampa della soluzione
		//Caso 2: i due stati iniziali non sono eq. a tracce deboli
		if( !( canDo1 && canDo2 ) ) {
			console.println(nome1+" and "+nome2+" are not MAY EQUIVALENT");
			console.println("\n since, setting S = ");
			traccia.print(console);	
			if (canDo1) {
				console.println(" S is contained in Language("+nome1+")");
				console.println("\n whereas\n");
				console.println(" S is NOT contained in Language("+nome2+")");
			} else {
				console.println(" S is contained in Language("+nome2+")");
				console.println("\n whereas\n");
				console.println(" S is NOT contained in Language("+nome1+")");
			}			
		}
		//Caso 3: i due stati iniziali sono eq. a tracce ma una 
		//        traccia porta in due stati con diverse propriet�
		//Caso 1: i due stati iniziali sono gi� diversi
		else {
			AcceptanceSet properties1 = pAG.getAcceptanceSet(final1);
			AcceptanceSet properties2 = pAG.getAcceptanceSet(final2);
			console.println(nome1+" and "+nome2+" are not MUST EQUIVALENT");
			console.println("\nindeed exist a sequence of actions\n S = ");
			traccia.print(console);	
			console.println("\nsuch that\n");
			if( properties1 == null ) {
				console.println(" "+nome1+" can diverge on S");
				console.println("\nwhereas\n");
				console.println(" "+nome2+" can NOT diverge on S");
			} else if ( properties2 == null ) {
				console.println(" "+nome2+" can diverge on S");
				console.println("\nwhereas\n");
				console.println(" "+nome1+" can NOT diverge on S");
			} else {
				console.println(" "+nome1+" =S=> "+nome1+"' and "+nome1+"' MUST ");	
				properties1.print(console);
				console.println("\nwhereas\n");
				console.println(" "+nome2+" =S=> "+nome2+"' and "+nome2+"' MUST ");
				properties2.print(console);
			}
		}
		//fine stampa soluzione
	}

	public static PTPartition initPTPartition(
		ProcessAG processAG
	) {
		PTPartition p = null;	
		PTStatesSet ptstatesSet = null;
		PTStatesSet globalPTStatesSet = null;
		StatesSet statesSet = null;
		StatesSet destinations = null;
		Vector<AcceptanceSet> acceptanceSetSet = null;
		AcceptanceSet accBase = null;
		AcceptanceSet accConfronto = null;
		Vector<Boolean> controllato = new Vector<Boolean>();
		Vector<TransitionCouple> transitionCouples = null;
		TransitionCouple tc = null;
		PTState ptstate = null;
		PTState ptstatePre = null;

		p = new PTPartition();
		globalPTStatesSet = new PTStatesSet();
		statesSet= processAG.getStatesSet();
		acceptanceSetSet = processAG.getAcceptanceSetSet();	
		for( int i=0; i<statesSet.getDimension(); i++ )
			controllato.add( false );
		for( int i=0; i<statesSet.getDimension(); i++ ) {
			if( !controllato.elementAt(i) ) {
				ptstatesSet = new PTStatesSet();
				ptstate = new PTState(statesSet.stateAt(i));
				ptstatesSet.addPTState(ptstate);
				globalPTStatesSet.addPTState(ptstate);
				controllato.setElementAt( true, i );
				accBase = acceptanceSetSet.elementAt(i);
				if (accBase == null) {
					for (int j=i+1; j<statesSet.getDimension(); j++) {
						accConfronto = 
							acceptanceSetSet.elementAt(j);		
						if( accConfronto == null ) {
							ptstate = new PTState(statesSet.stateAt(j));
							ptstatesSet.addPTState(ptstate);
							globalPTStatesSet.addPTState(ptstate);
							controllato.setElementAt( true, j );							
						}
					}
				} else {
					for (int j=i+1; j<statesSet.getDimension(); j++) {
						if( ! controllato.elementAt(j) ) {
							accConfronto = 
								acceptanceSetSet.elementAt(j);	
							if( accConfronto != null ) {
								if( accBase.getMinAcceptanceSet().equals(
									accConfronto.getMinAcceptanceSet()
								)) {
									ptstate = new PTState(statesSet.stateAt(j));
									ptstatesSet.addPTState(ptstate);
									globalPTStatesSet.addPTState(ptstate);
									controllato.setElementAt( true, j );							
								}
							}
						}
					}
				}
				p.addPTStatesSet(ptstatesSet);
			}
		}	

		for( int i=0; i<statesSet.getDimension(); i++ ) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			ptstatePre = globalPTStatesSet.getPTState(statesSet.stateAt(i));
			for( int j=0; j<transitionCouples.size(); j++ ) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for( int k=0; k<destinations.getDimension(); k++ ) {
					ptstate = globalPTStatesSet.getPTState(
						destinations.stateAt(k)
					);
					ptstate.addPresetCouple(tc.getAction(), ptstatePre);
				}
			}
		}
		
		return p;
	}

}
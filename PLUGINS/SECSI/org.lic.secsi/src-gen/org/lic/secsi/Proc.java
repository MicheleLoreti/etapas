/**
 */
package org.lic.secsi;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Proc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Proc#getName <em>Name</em>}</li>
 *   <li>{@link org.lic.secsi.Proc#getName1 <em>Name1</em>}</li>
 *   <li>{@link org.lic.secsi.Proc#getRe <em>Re</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getProc()
 * @model
 * @generated
 */
public interface Proc extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' reference.
   * @see #setName(Agent)
   * @see org.lic.secsi.SecsiPackage#getProc_Name()
   * @model
   * @generated
   */
  Agent getName();

  /**
   * Sets the value of the '{@link org.lic.secsi.Proc#getName <em>Name</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' reference.
   * @see #getName()
   * @generated
   */
  void setName(Agent value);

  /**
   * Returns the value of the '<em><b>Name1</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name1</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name1</em>' reference.
   * @see #setName1(Agent)
   * @see org.lic.secsi.SecsiPackage#getProc_Name1()
   * @model
   * @generated
   */
  Agent getName1();

  /**
   * Sets the value of the '{@link org.lic.secsi.Proc#getName1 <em>Name1</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name1</em>' reference.
   * @see #getName1()
   * @generated
   */
  void setName1(Agent value);

  /**
   * Returns the value of the '<em><b>Re</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Re</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Re</em>' containment reference.
   * @see #setRe(Ren)
   * @see org.lic.secsi.SecsiPackage#getProc_Re()
   * @model containment="true"
   * @generated
   */
  Ren getRe();

  /**
   * Sets the value of the '{@link org.lic.secsi.Proc#getRe <em>Re</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Re</em>' containment reference.
   * @see #getRe()
   * @generated
   */
  void setRe(Ren value);

} // Proc

package org.lic.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.lic.secsi.Action;
import org.lic.secsi.Agent;
import org.lic.secsi.AndExpression;
import org.lic.secsi.Assignment;
import org.lic.secsi.Block;
import org.lic.secsi.BoolConstant;
import org.lic.secsi.BooleanType;
import org.lic.secsi.Channel;
import org.lic.secsi.ChannelsDeclaration;
import org.lic.secsi.Constant;
import org.lic.secsi.IfThenElse;
import org.lic.secsi.IntConstant;
import org.lic.secsi.IntegerType;
import org.lic.secsi.MulExpression;
import org.lic.secsi.Negation;
import org.lic.secsi.NotProp;
import org.lic.secsi.OrExpression;
import org.lic.secsi.OrProp;
import org.lic.secsi.Par;
import org.lic.secsi.Proc;
import org.lic.secsi.Program;
import org.lic.secsi.PropBody;
import org.lic.secsi.PropList;
import org.lic.secsi.ReferenceInExpression;
import org.lic.secsi.Relation;
import org.lic.secsi.Ren;
import org.lic.secsi.Res;
import org.lic.secsi.SecsiPackage;
import org.lic.secsi.Signal;
import org.lic.secsi.Sum;
import org.lic.secsi.SumExpression;
import org.lic.secsi.Switch_Case;
import org.lic.secsi.TypeDef;
import org.lic.secsi.TypeElement;
import org.lic.secsi.TypeReference;
import org.lic.secsi.VarDeclaration;
import org.lic.secsi.Variable;
import org.lic.secsi.While;
import org.lic.services.SecsiGrammarAccess;

@SuppressWarnings("all")
public class SecsiSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private SecsiGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == SecsiPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case SecsiPackage.ACTION:
				if(context == grammarAccess.getActionRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_Action(context, (Action) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.AGENT:
				if(context == grammarAccess.getAgentRule()) {
					sequence_Agent(context, (Agent) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.AND_EXPRESSION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_AndExpression(context, (AndExpression) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.ASSIGNMENT:
				if(context == grammarAccess.getAssignmentRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_Assignment(context, (Assignment) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.BLOCK:
				if(context == grammarAccess.getBlockRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_Block(context, (Block) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.BOOL_CONSTANT:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getBoolConstantRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_BoolConstant(context, (BoolConstant) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.BOOLEAN_TYPE:
				if(context == grammarAccess.getBooleanTypeRule() ||
				   context == grammarAccess.getTypeRule() ||
				   context == grammarAccess.getTypeExpressionRule()) {
					sequence_BooleanType(context, (BooleanType) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.CHANNEL:
				if(context == grammarAccess.getChannelRule()) {
					sequence_Channel(context, (Channel) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.CHANNELS_DECLARATION:
				if(context == grammarAccess.getChannelsDeclarationRule()) {
					sequence_ChannelsDeclaration(context, (ChannelsDeclaration) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.CONSTANT:
				if(context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getReferenceableElementRule()) {
					sequence_Constant(context, (Constant) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.IF_THEN_ELSE:
				if(context == grammarAccess.getIfThenElseRule() ||
				   context == grammarAccess.getStatementRule()) {
					sequence_IfThenElse(context, (IfThenElse) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.INT_CONSTANT:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getIntConstantRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_IntConstant(context, (IntConstant) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.INTEGER_TYPE:
				if(context == grammarAccess.getIntegerTypeRule() ||
				   context == grammarAccess.getTypeRule() ||
				   context == grammarAccess.getTypeExpressionRule()) {
					sequence_IntegerType(context, (IntegerType) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.MUL_EXPRESSION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_MulExpression(context, (MulExpression) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.NEGATION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getNegationRule() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_Negation(context, (Negation) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.NOT_PROP:
				if(context == grammarAccess.getNotPropRule() ||
				   context == grammarAccess.getTermRule()) {
					sequence_NotProp(context, (NotProp) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.OR_EXPRESSION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_OrExpression(context, (OrExpression) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.OR_PROP:
				if(context == grammarAccess.getOrPropRule() ||
				   context == grammarAccess.getPropRule() ||
				   context == grammarAccess.getPropBodyRule()) {
					sequence_OrProp(context, (OrProp) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.PAR:
				if(context == grammarAccess.getParRule()) {
					sequence_Par(context, (Par) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getParAccess().getParLeftAction_1_0()) {
					sequence_Par_Par_1_0(context, (Par) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.PROC:
				if(context == grammarAccess.getProcRule()) {
					sequence_Proc(context, (Proc) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.PROGRAM:
				if(context == grammarAccess.getProgramRule()) {
					sequence_Program(context, (Program) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.PROP_BODY:
				if(context == grammarAccess.getActPropRule() ||
				   context == grammarAccess.getOrPropRule() ||
				   context == grammarAccess.getOrPropAccess().getOrPropLeftAction_1_0() ||
				   context == grammarAccess.getPropRule() ||
				   context == grammarAccess.getPropBodyRule()) {
					sequence_ActProp(context, (PropBody) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getTermRule()) {
					sequence_Term(context, (PropBody) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.PROP_LIST:
				if(context == grammarAccess.getPropListRule()) {
					sequence_PropList(context, (PropList) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.REFERENCE_IN_EXPRESSION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getReferenceInExpressionRule() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_ReferenceInExpression(context, (ReferenceInExpression) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.RELATION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_Relation(context, (Relation) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.REN:
				if(context == grammarAccess.getRenRule()) {
					sequence_Ren(context, (Ren) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.RES:
				if(context == grammarAccess.getResRule()) {
					sequence_Res(context, (Res) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.SIGNAL:
				if(context == grammarAccess.getTypeExpressionRule()) {
					sequence_TypeExpression(context, (Signal) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.SUM:
				if(context == grammarAccess.getSumRule()) {
					sequence_Sum(context, (Sum) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getSumAccess().getSumLeftAction_1_0()) {
					sequence_Sum_Sum_1_0(context, (Sum) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.SUM_EXPRESSION:
				if(context == grammarAccess.getAndExpressionRule() ||
				   context == grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0() ||
				   context == grammarAccess.getBaseExpressionRule() ||
				   context == grammarAccess.getExpressionRule() ||
				   context == grammarAccess.getMulExpressionRule() ||
				   context == grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0() ||
				   context == grammarAccess.getOrExpressionRule() ||
				   context == grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0() ||
				   context == grammarAccess.getRelationRule() ||
				   context == grammarAccess.getRelationAccess().getRelationLeftAction_1_0() ||
				   context == grammarAccess.getSumExpressionRule() ||
				   context == grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0()) {
					sequence_SumExpression(context, (SumExpression) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.SWITCH_CASE:
				if(context == grammarAccess.getCaseRule()) {
					sequence_Case(context, (Switch_Case) semanticObject); 
					return; 
				}
				else if(context == grammarAccess.getStatementRule() ||
				   context == grammarAccess.getSwitch_CaseRule()) {
					sequence_Switch_Case(context, (Switch_Case) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.SYSTEM:
				if(context == grammarAccess.getSystemRule()) {
					sequence_System(context, (org.lic.secsi.System) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.TYPE_DEF:
				if(context == grammarAccess.getTypeRule() ||
				   context == grammarAccess.getTypeDefRule()) {
					sequence_TypeDef(context, (TypeDef) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.TYPE_ELEMENT:
				if(context == grammarAccess.getReferenceableElementRule() ||
				   context == grammarAccess.getTypeElementRule()) {
					sequence_TypeElement(context, (TypeElement) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.TYPE_REFERENCE:
				if(context == grammarAccess.getTypeExpressionRule() ||
				   context == grammarAccess.getTypeReferenceRule()) {
					sequence_TypeReference(context, (TypeReference) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.VAR_DECLARATION:
				if(context == grammarAccess.getStatementRule() ||
				   context == grammarAccess.getVarDeclarationRule()) {
					sequence_VarDeclaration(context, (VarDeclaration) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.VARIABLE:
				if(context == grammarAccess.getReferenceableElementRule() ||
				   context == grammarAccess.getVariableRule()) {
					sequence_Variable(context, (Variable) semanticObject); 
					return; 
				}
				else break;
			case SecsiPackage.WHILE:
				if(context == grammarAccess.getStatementRule() ||
				   context == grammarAccess.getWhileRule()) {
					sequence_While(context, (While) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     ((action=[Channel|ID] arg=Term) | (action=[Channel|ID] arg=Term) | arg=Term)
	 */
	protected void sequence_ActProp(EObject context, PropBody semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (channel=[Channel|ID] modality=ActionModality (notSignal?='(' e=Expression)?)
	 */
	protected void sequence_Action(EObject context, Action semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID (params+=Variable params+=Variable*)? lcDecs+=ChannelsDeclaration* s=Statement)
	 */
	protected void sequence_Agent(EObject context, Agent semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=AndExpression_AndExpression_1_0 right=AndExpression)
	 */
	protected void sequence_AndExpression(EObject context, AndExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.AND_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.AND_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.AND_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.AND_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getAndExpressionAccess().getRightAndExpressionParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (id=[Variable|ID] in=Expression)
	 */
	protected void sequence_Assignment(EObject context, Assignment semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.ASSIGNMENT__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.ASSIGNMENT__ID));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.ASSIGNMENT__IN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.ASSIGNMENT__IN));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAssignmentAccess().getIdVariableIDTerminalRuleCall_0_0_1(), semanticObject.getId());
		feeder.accept(grammarAccess.getAssignmentAccess().getInExpressionParserRuleCall_2_0(), semanticObject.getIn());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (cmds+=Statement*)
	 */
	protected void sequence_Block(EObject context, Block semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=BoolValue
	 */
	protected void sequence_BoolConstant(EObject context, BoolConstant semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.BOOL_CONSTANT__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.BOOL_CONSTANT__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getBoolConstantAccess().getValueBoolValueEnumRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     {BooleanType}
	 */
	protected void sequence_BooleanType(EObject context, BooleanType semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value=Expression cmd=Statement)
	 */
	protected void sequence_Case(EObject context, Switch_Case semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID type=TypeExpression)
	 */
	protected void sequence_Channel(EObject context, Channel semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.CHANNEL__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.CHANNEL__NAME));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.CHANNEL__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.CHANNEL__TYPE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getChannelAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getChannelAccess().getTypeTypeExpressionParserRuleCall_2_0(), semanticObject.getType());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (declared+=Channel declared+=Channel*)
	 */
	protected void sequence_ChannelsDeclaration(EObject context, ChannelsDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type=TypeExpression name=ID value=Expression)
	 */
	protected void sequence_Constant(EObject context, Constant semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.REFERENCEABLE_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.REFERENCEABLE_ELEMENT__NAME));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.CONSTANT__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.CONSTANT__TYPE));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.CONSTANT__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.CONSTANT__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getConstantAccess().getTypeTypeExpressionParserRuleCall_1_0(), semanticObject.getType());
		feeder.accept(grammarAccess.getConstantAccess().getNameIDTerminalRuleCall_2_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getConstantAccess().getValueExpressionParserRuleCall_4_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (expression=Expression thenBranch=Statement (withElse?='else' elseBranch=Statement)?)
	 */
	protected void sequence_IfThenElse(EObject context, IfThenElse semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=INT
	 */
	protected void sequence_IntConstant(EObject context, IntConstant semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.INT_CONSTANT__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.INT_CONSTANT__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntConstantAccess().getValueINTTerminalRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (min=Expression max=Expression)
	 */
	protected void sequence_IntegerType(EObject context, IntegerType semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.INTEGER_TYPE__MIN) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.INTEGER_TYPE__MIN));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.INTEGER_TYPE__MAX) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.INTEGER_TYPE__MAX));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getIntegerTypeAccess().getMinExpressionParserRuleCall_1_0(), semanticObject.getMin());
		feeder.accept(grammarAccess.getIntegerTypeAccess().getMaxExpressionParserRuleCall_3_0(), semanticObject.getMax());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=MulExpression_MulExpression_1_0 (op='*' | op='/') right=MulExpression)
	 */
	protected void sequence_MulExpression(EObject context, MulExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     arg=BaseExpression
	 */
	protected void sequence_Negation(EObject context, Negation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.NEGATION__ARG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.NEGATION__ARG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getNegationAccess().getArgBaseExpressionParserRuleCall_1_0(), semanticObject.getArg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     arg=Term
	 */
	protected void sequence_NotProp(EObject context, NotProp semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.PROP__ARG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.PROP__ARG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getNotPropAccess().getArgTermParserRuleCall_1_0(), semanticObject.getArg());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=OrExpression_OrExpression_1_0 right=OrExpression)
	 */
	protected void sequence_OrExpression(EObject context, OrExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.OR_EXPRESSION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.OR_EXPRESSION__LEFT));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.OR_EXPRESSION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.OR_EXPRESSION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getOrExpressionAccess().getRightOrExpressionParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=OrProp_OrProp_1_0 (op='||' | op='&&') right=OrProp)
	 */
	protected void sequence_OrProp(EObject context, OrProp semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((left=Par_Par_1_0 right=Par) | action=Sum)
	 */
	protected void sequence_Par(EObject context, Par semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     action=Sum
	 */
	protected void sequence_Par_Par_1_0(EObject context, Par semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=[Agent|ID] | (name1=[Agent|ID] re=Ren))
	 */
	protected void sequence_Proc(EObject context, Proc semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((globalchanels+=ChannelsDeclaration | agents+=Agent | types+=TypeDef | consts+=Constant)* system=System? properties=PropList?)
	 */
	protected void sequence_Program(EObject context, Program semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (element+=Prop*)
	 */
	protected void sequence_PropList(EObject context, PropList semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     reference=[ReferenceableElement|ID]
	 */
	protected void sequence_ReferenceInExpression(EObject context, ReferenceInExpression semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.REFERENCE_IN_EXPRESSION__REFERENCE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.REFERENCE_IN_EXPRESSION__REFERENCE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getReferenceInExpressionAccess().getReferenceReferenceableElementIDTerminalRuleCall_0_1(), semanticObject.getReference());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (left=Relation_Relation_1_0 op=Relop right=SumExpression)
	 */
	protected void sequence_Relation(EObject context, Relation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.RELATION__LEFT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.RELATION__LEFT));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.RELATION__OP) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.RELATION__OP));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.RELATION__RIGHT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.RELATION__RIGHT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRelationAccess().getRelationLeftAction_1_0(), semanticObject.getLeft());
		feeder.accept(grammarAccess.getRelationAccess().getOpRelopEnumRuleCall_1_1_0(), semanticObject.getOp());
		feeder.accept(grammarAccess.getRelationAccess().getRightSumExpressionParserRuleCall_1_2_0(), semanticObject.getRight());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (leflists+=[Channel|ID] rightlist+=ID (leflists+=[Channel|ID] rightlist+=ID)* r=Res?)
	 */
	protected void sequence_Ren(EObject context, Ren semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (leftlists=[Channel|ID] leflists+=[Channel|ID]*)
	 */
	protected void sequence_Res(EObject context, Res semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=SumExpression_SumExpression_1_0 (op='+' | op='-' | op='%') right=SumExpression)
	 */
	protected void sequence_SumExpression(EObject context, SumExpression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((left=Sum_Sum_1_0 right=Sum) | action=Proc)
	 */
	protected void sequence_Sum(EObject context, Sum semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     action=Proc
	 */
	protected void sequence_Sum_Sum_1_0(EObject context, Sum semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value=Expression cases+=Case* default=Statement)
	 */
	protected void sequence_Switch_Case(EObject context, Switch_Case semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     body=Par
	 */
	protected void sequence_System(EObject context, org.lic.secsi.System semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.SYSTEM__BODY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.SYSTEM__BODY));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSystemAccess().getBodyParParserRuleCall_2_0(), semanticObject.getBody());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     exp=PropBody
	 */
	protected void sequence_Term(EObject context, PropBody semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID elements+=TypeElement elements+=TypeElement*)
	 */
	protected void sequence_TypeDef(EObject context, TypeDef semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_TypeElement(EObject context, TypeElement semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.REFERENCEABLE_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.REFERENCEABLE_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getTypeElementAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     {Signal}
	 */
	protected void sequence_TypeExpression(EObject context, Signal semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     reference=[TypeDef|ID]
	 */
	protected void sequence_TypeReference(EObject context, TypeReference semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.TYPE_REFERENCE__REFERENCE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.TYPE_REFERENCE__REFERENCE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getTypeReferenceAccess().getReferenceTypeDefIDTerminalRuleCall_0_1(), semanticObject.getReference());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (variable=Variable init=Expression?)
	 */
	protected void sequence_VarDeclaration(EObject context, VarDeclaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (type=TypeExpression name=ID)
	 */
	protected void sequence_Variable(EObject context, Variable semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.REFERENCEABLE_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.REFERENCEABLE_ELEMENT__NAME));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.VARIABLE__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.VARIABLE__TYPE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getVariableAccess().getTypeTypeExpressionParserRuleCall_0_0(), semanticObject.getType());
		feeder.accept(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (expresion=Expression body=Statement)
	 */
	protected void sequence_While(EObject context, While semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.WHILE__EXPRESION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.WHILE__EXPRESION));
			if(transientValues.isValueTransient(semanticObject, SecsiPackage.Literals.WHILE__BODY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, SecsiPackage.Literals.WHILE__BODY));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getWhileAccess().getExpresionExpressionParserRuleCall_2_0(), semanticObject.getExpresion());
		feeder.accept(grammarAccess.getWhileAccess().getBodyStatementParserRuleCall_4_0(), semanticObject.getBody());
		feeder.finish();
	}
}

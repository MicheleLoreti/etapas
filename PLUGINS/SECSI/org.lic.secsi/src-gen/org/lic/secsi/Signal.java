/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.lic.secsi.SecsiPackage#getSignal()
 * @model
 * @generated
 */
public interface Signal extends TypeExpression
{
} // Signal

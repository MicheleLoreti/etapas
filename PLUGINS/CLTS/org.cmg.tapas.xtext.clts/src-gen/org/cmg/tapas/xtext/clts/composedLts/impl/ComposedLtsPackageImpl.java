/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.Action;
import org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.AnyLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsFactory;
import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.CompositionBody;
import org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction;
import org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.Element;
import org.cmg.tapas.xtext.clts.composedLts.Formula;
import org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlFalse;
import org.cmg.tapas.xtext.clts.composedLts.HmlFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration;
import org.cmg.tapas.xtext.clts.composedLts.HmlMaxFixPoint;
import org.cmg.tapas.xtext.clts.composedLts.HmlMinFixPoint;
import org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlPredicate;
import org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable;
import org.cmg.tapas.xtext.clts.composedLts.HmlTrue;
import org.cmg.tapas.xtext.clts.composedLts.Interleaving;
import org.cmg.tapas.xtext.clts.composedLts.Label;
import org.cmg.tapas.xtext.clts.composedLts.LabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.LabelRule;
import org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition;
import org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlFalse;
import org.cmg.tapas.xtext.clts.composedLts.LtlFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration;
import org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula;
import org.cmg.tapas.xtext.clts.composedLts.LtlTrue;
import org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula;
import org.cmg.tapas.xtext.clts.composedLts.Lts;
import org.cmg.tapas.xtext.clts.composedLts.LtsBody;
import org.cmg.tapas.xtext.clts.composedLts.LtsComposition;
import org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody;
import org.cmg.tapas.xtext.clts.composedLts.LtsRule;
import org.cmg.tapas.xtext.clts.composedLts.LtsState;
import org.cmg.tapas.xtext.clts.composedLts.Mapping;
import org.cmg.tapas.xtext.clts.composedLts.Model;
import org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.Reference;
import org.cmg.tapas.xtext.clts.composedLts.Renaming;
import org.cmg.tapas.xtext.clts.composedLts.Rule;
import org.cmg.tapas.xtext.clts.composedLts.State;
import org.cmg.tapas.xtext.clts.composedLts.Synchronization;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComposedLtsPackageImpl extends EPackageImpl implements ComposedLtsPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass elementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltsEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltsBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltsCompositionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass compositionBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass referenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mappingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltsDeclarationBodyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltsRuleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltsStateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labelRuleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ruleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass actionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlFormulaDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlPredicateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlTrueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlFalseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlNotFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlDiamondFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlBoxFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlRecursionFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlMinFixPointEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlMaxFixPointEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlRecursionVariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlFormulaDeclarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlAtomicPropositionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlNextFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlAlwaysFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlEventuallyFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlNotFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlTrueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlFalseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass labelPredicateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass notLabelPredicateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass actionLabelPredicateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass anyLabelPredicateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass interleavingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass synchronizationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass controlledInteractionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass renamingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlOrFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass hmlAndFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlOrFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlAndFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlUntilFormulaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass cupLabelPredicateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass capLabelPredicateEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private ComposedLtsPackageImpl()
  {
    super(eNS_URI, ComposedLtsFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link ComposedLtsPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static ComposedLtsPackage init()
  {
    if (isInited) return (ComposedLtsPackage)EPackage.Registry.INSTANCE.getEPackage(ComposedLtsPackage.eNS_URI);

    // Obtain or create and register package
    ComposedLtsPackageImpl theComposedLtsPackage = (ComposedLtsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ComposedLtsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ComposedLtsPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theComposedLtsPackage.createPackageContents();

    // Initialize created meta-data
    theComposedLtsPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theComposedLtsPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(ComposedLtsPackage.eNS_URI, theComposedLtsPackage);
    return theComposedLtsPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModel()
  {
    return modelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModel_Name()
  {
    return (EAttribute)modelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Elements()
  {
    return (EReference)modelEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getElement()
  {
    return elementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getElement_Name()
  {
    return (EAttribute)elementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLts()
  {
    return ltsEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLts_Body()
  {
    return (EReference)ltsEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtsBody()
  {
    return ltsBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtsComposition()
  {
    return ltsCompositionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsComposition_Body()
  {
    return (EReference)ltsCompositionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCompositionBody()
  {
    return compositionBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getReference()
  {
    return referenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getReference_Lts()
  {
    return (EReference)referenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMapping()
  {
    return mappingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMapping_Src()
  {
    return (EReference)mappingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMapping_Trg()
  {
    return (EReference)mappingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtsDeclarationBody()
  {
    return ltsDeclarationBodyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsDeclarationBody_States()
  {
    return (EReference)ltsDeclarationBodyEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsDeclarationBody_Init()
  {
    return (EReference)ltsDeclarationBodyEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsDeclarationBody_Rules()
  {
    return (EReference)ltsDeclarationBodyEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsDeclarationBody_Labels()
  {
    return (EReference)ltsDeclarationBodyEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtsRule()
  {
    return ltsRuleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsRule_Src()
  {
    return (EReference)ltsRuleEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsRule_Act()
  {
    return (EReference)ltsRuleEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtsRule_Trg()
  {
    return (EReference)ltsRuleEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtsState()
  {
    return ltsStateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLtsState_Name()
  {
    return (EAttribute)ltsStateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabelRule()
  {
    return labelRuleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLabelRule_Label()
  {
    return (EReference)labelRuleEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLabelRule_States()
  {
    return (EReference)labelRuleEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getState()
  {
    return stateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getState_IsInitial()
  {
    return (EAttribute)stateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getState_Name()
  {
    return (EAttribute)stateEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getState_Rules()
  {
    return (EReference)stateEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRule()
  {
    return ruleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRule_Action()
  {
    return (EReference)ruleEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRule_Next()
  {
    return (EReference)ruleEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabel()
  {
    return labelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAction()
  {
    return actionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormula()
  {
    return formulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlFormulaDeclaration()
  {
    return hmlFormulaDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlFormulaDeclaration_Formula()
  {
    return (EReference)hmlFormulaDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlFormula()
  {
    return hmlFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlPredicate()
  {
    return hmlPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlPredicate_Label()
  {
    return (EReference)hmlPredicateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlTrue()
  {
    return hmlTrueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlFalse()
  {
    return hmlFalseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlNotFormula()
  {
    return hmlNotFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlNotFormula_Arg()
  {
    return (EReference)hmlNotFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlDiamondFormula()
  {
    return hmlDiamondFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlDiamondFormula_Action()
  {
    return (EReference)hmlDiamondFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlDiamondFormula_Arg()
  {
    return (EReference)hmlDiamondFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlBoxFormula()
  {
    return hmlBoxFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlBoxFormula_Action()
  {
    return (EReference)hmlBoxFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlBoxFormula_Arg()
  {
    return (EReference)hmlBoxFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlRecursionFormula()
  {
    return hmlRecursionFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getHmlRecursionFormula_Name()
  {
    return (EAttribute)hmlRecursionFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlRecursionFormula_Arg()
  {
    return (EReference)hmlRecursionFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlMinFixPoint()
  {
    return hmlMinFixPointEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlMaxFixPoint()
  {
    return hmlMaxFixPointEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlRecursionVariable()
  {
    return hmlRecursionVariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlRecursionVariable_Refeerence()
  {
    return (EReference)hmlRecursionVariableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlFormulaDeclaration()
  {
    return ltlFormulaDeclarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlFormulaDeclaration_Formula()
  {
    return (EReference)ltlFormulaDeclarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlFormula()
  {
    return ltlFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlAtomicProposition()
  {
    return ltlAtomicPropositionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlAtomicProposition_Prop()
  {
    return (EReference)ltlAtomicPropositionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlNextFormula()
  {
    return ltlNextFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlNextFormula_Arg()
  {
    return (EReference)ltlNextFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlAlwaysFormula()
  {
    return ltlAlwaysFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlAlwaysFormula_Arg()
  {
    return (EReference)ltlAlwaysFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlEventuallyFormula()
  {
    return ltlEventuallyFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlEventuallyFormula_Arg()
  {
    return (EReference)ltlEventuallyFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlNotFormula()
  {
    return ltlNotFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlNotFormula_Arg()
  {
    return (EReference)ltlNotFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlTrue()
  {
    return ltlTrueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlFalse()
  {
    return ltlFalseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLabelPredicate()
  {
    return labelPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNotLabelPredicate()
  {
    return notLabelPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNotLabelPredicate_Arg()
  {
    return (EReference)notLabelPredicateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getActionLabelPredicate()
  {
    return actionLabelPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getActionLabelPredicate_Act()
  {
    return (EReference)actionLabelPredicateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAnyLabelPredicate()
  {
    return anyLabelPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInterleaving()
  {
    return interleavingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInterleaving_Left()
  {
    return (EReference)interleavingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInterleaving_Right()
  {
    return (EReference)interleavingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSynchronization()
  {
    return synchronizationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSynchronization_Left()
  {
    return (EReference)synchronizationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSynchronization_Right()
  {
    return (EReference)synchronizationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getControlledInteraction()
  {
    return controlledInteractionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlledInteraction_Left()
  {
    return (EReference)controlledInteractionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlledInteraction_Action()
  {
    return (EReference)controlledInteractionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getControlledInteraction_Right()
  {
    return (EReference)controlledInteractionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRenaming()
  {
    return renamingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRenaming_Arg()
  {
    return (EReference)renamingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRenaming_Maps()
  {
    return (EReference)renamingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlOrFormula()
  {
    return hmlOrFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlOrFormula_Left()
  {
    return (EReference)hmlOrFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlOrFormula_Right()
  {
    return (EReference)hmlOrFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getHmlAndFormula()
  {
    return hmlAndFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlAndFormula_Left()
  {
    return (EReference)hmlAndFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getHmlAndFormula_Right()
  {
    return (EReference)hmlAndFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlOrFormula()
  {
    return ltlOrFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlOrFormula_Left()
  {
    return (EReference)ltlOrFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlOrFormula_Right()
  {
    return (EReference)ltlOrFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlAndFormula()
  {
    return ltlAndFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlAndFormula_Left()
  {
    return (EReference)ltlAndFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlAndFormula_Right()
  {
    return (EReference)ltlAndFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLtlUntilFormula()
  {
    return ltlUntilFormulaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlUntilFormula_Left()
  {
    return (EReference)ltlUntilFormulaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLtlUntilFormula_Right()
  {
    return (EReference)ltlUntilFormulaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCupLabelPredicate()
  {
    return cupLabelPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCupLabelPredicate_Left()
  {
    return (EReference)cupLabelPredicateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCupLabelPredicate_Righ()
  {
    return (EReference)cupLabelPredicateEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCapLabelPredicate()
  {
    return capLabelPredicateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCapLabelPredicate_Left()
  {
    return (EReference)capLabelPredicateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCapLabelPredicate_Right()
  {
    return (EReference)capLabelPredicateEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComposedLtsFactory getComposedLtsFactory()
  {
    return (ComposedLtsFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    modelEClass = createEClass(MODEL);
    createEAttribute(modelEClass, MODEL__NAME);
    createEReference(modelEClass, MODEL__ELEMENTS);

    elementEClass = createEClass(ELEMENT);
    createEAttribute(elementEClass, ELEMENT__NAME);

    ltsEClass = createEClass(LTS);
    createEReference(ltsEClass, LTS__BODY);

    ltsBodyEClass = createEClass(LTS_BODY);

    ltsCompositionEClass = createEClass(LTS_COMPOSITION);
    createEReference(ltsCompositionEClass, LTS_COMPOSITION__BODY);

    compositionBodyEClass = createEClass(COMPOSITION_BODY);

    referenceEClass = createEClass(REFERENCE);
    createEReference(referenceEClass, REFERENCE__LTS);

    mappingEClass = createEClass(MAPPING);
    createEReference(mappingEClass, MAPPING__SRC);
    createEReference(mappingEClass, MAPPING__TRG);

    ltsDeclarationBodyEClass = createEClass(LTS_DECLARATION_BODY);
    createEReference(ltsDeclarationBodyEClass, LTS_DECLARATION_BODY__STATES);
    createEReference(ltsDeclarationBodyEClass, LTS_DECLARATION_BODY__INIT);
    createEReference(ltsDeclarationBodyEClass, LTS_DECLARATION_BODY__RULES);
    createEReference(ltsDeclarationBodyEClass, LTS_DECLARATION_BODY__LABELS);

    ltsRuleEClass = createEClass(LTS_RULE);
    createEReference(ltsRuleEClass, LTS_RULE__SRC);
    createEReference(ltsRuleEClass, LTS_RULE__ACT);
    createEReference(ltsRuleEClass, LTS_RULE__TRG);

    ltsStateEClass = createEClass(LTS_STATE);
    createEAttribute(ltsStateEClass, LTS_STATE__NAME);

    labelRuleEClass = createEClass(LABEL_RULE);
    createEReference(labelRuleEClass, LABEL_RULE__LABEL);
    createEReference(labelRuleEClass, LABEL_RULE__STATES);

    stateEClass = createEClass(STATE);
    createEAttribute(stateEClass, STATE__IS_INITIAL);
    createEAttribute(stateEClass, STATE__NAME);
    createEReference(stateEClass, STATE__RULES);

    ruleEClass = createEClass(RULE);
    createEReference(ruleEClass, RULE__ACTION);
    createEReference(ruleEClass, RULE__NEXT);

    labelEClass = createEClass(LABEL);

    actionEClass = createEClass(ACTION);

    formulaEClass = createEClass(FORMULA);

    hmlFormulaDeclarationEClass = createEClass(HML_FORMULA_DECLARATION);
    createEReference(hmlFormulaDeclarationEClass, HML_FORMULA_DECLARATION__FORMULA);

    hmlFormulaEClass = createEClass(HML_FORMULA);

    hmlPredicateEClass = createEClass(HML_PREDICATE);
    createEReference(hmlPredicateEClass, HML_PREDICATE__LABEL);

    hmlTrueEClass = createEClass(HML_TRUE);

    hmlFalseEClass = createEClass(HML_FALSE);

    hmlNotFormulaEClass = createEClass(HML_NOT_FORMULA);
    createEReference(hmlNotFormulaEClass, HML_NOT_FORMULA__ARG);

    hmlDiamondFormulaEClass = createEClass(HML_DIAMOND_FORMULA);
    createEReference(hmlDiamondFormulaEClass, HML_DIAMOND_FORMULA__ACTION);
    createEReference(hmlDiamondFormulaEClass, HML_DIAMOND_FORMULA__ARG);

    hmlBoxFormulaEClass = createEClass(HML_BOX_FORMULA);
    createEReference(hmlBoxFormulaEClass, HML_BOX_FORMULA__ACTION);
    createEReference(hmlBoxFormulaEClass, HML_BOX_FORMULA__ARG);

    hmlRecursionFormulaEClass = createEClass(HML_RECURSION_FORMULA);
    createEAttribute(hmlRecursionFormulaEClass, HML_RECURSION_FORMULA__NAME);
    createEReference(hmlRecursionFormulaEClass, HML_RECURSION_FORMULA__ARG);

    hmlMinFixPointEClass = createEClass(HML_MIN_FIX_POINT);

    hmlMaxFixPointEClass = createEClass(HML_MAX_FIX_POINT);

    hmlRecursionVariableEClass = createEClass(HML_RECURSION_VARIABLE);
    createEReference(hmlRecursionVariableEClass, HML_RECURSION_VARIABLE__REFEERENCE);

    ltlFormulaDeclarationEClass = createEClass(LTL_FORMULA_DECLARATION);
    createEReference(ltlFormulaDeclarationEClass, LTL_FORMULA_DECLARATION__FORMULA);

    ltlFormulaEClass = createEClass(LTL_FORMULA);

    ltlAtomicPropositionEClass = createEClass(LTL_ATOMIC_PROPOSITION);
    createEReference(ltlAtomicPropositionEClass, LTL_ATOMIC_PROPOSITION__PROP);

    ltlNextFormulaEClass = createEClass(LTL_NEXT_FORMULA);
    createEReference(ltlNextFormulaEClass, LTL_NEXT_FORMULA__ARG);

    ltlAlwaysFormulaEClass = createEClass(LTL_ALWAYS_FORMULA);
    createEReference(ltlAlwaysFormulaEClass, LTL_ALWAYS_FORMULA__ARG);

    ltlEventuallyFormulaEClass = createEClass(LTL_EVENTUALLY_FORMULA);
    createEReference(ltlEventuallyFormulaEClass, LTL_EVENTUALLY_FORMULA__ARG);

    ltlNotFormulaEClass = createEClass(LTL_NOT_FORMULA);
    createEReference(ltlNotFormulaEClass, LTL_NOT_FORMULA__ARG);

    ltlTrueEClass = createEClass(LTL_TRUE);

    ltlFalseEClass = createEClass(LTL_FALSE);

    labelPredicateEClass = createEClass(LABEL_PREDICATE);

    notLabelPredicateEClass = createEClass(NOT_LABEL_PREDICATE);
    createEReference(notLabelPredicateEClass, NOT_LABEL_PREDICATE__ARG);

    actionLabelPredicateEClass = createEClass(ACTION_LABEL_PREDICATE);
    createEReference(actionLabelPredicateEClass, ACTION_LABEL_PREDICATE__ACT);

    anyLabelPredicateEClass = createEClass(ANY_LABEL_PREDICATE);

    interleavingEClass = createEClass(INTERLEAVING);
    createEReference(interleavingEClass, INTERLEAVING__LEFT);
    createEReference(interleavingEClass, INTERLEAVING__RIGHT);

    synchronizationEClass = createEClass(SYNCHRONIZATION);
    createEReference(synchronizationEClass, SYNCHRONIZATION__LEFT);
    createEReference(synchronizationEClass, SYNCHRONIZATION__RIGHT);

    controlledInteractionEClass = createEClass(CONTROLLED_INTERACTION);
    createEReference(controlledInteractionEClass, CONTROLLED_INTERACTION__LEFT);
    createEReference(controlledInteractionEClass, CONTROLLED_INTERACTION__ACTION);
    createEReference(controlledInteractionEClass, CONTROLLED_INTERACTION__RIGHT);

    renamingEClass = createEClass(RENAMING);
    createEReference(renamingEClass, RENAMING__ARG);
    createEReference(renamingEClass, RENAMING__MAPS);

    hmlOrFormulaEClass = createEClass(HML_OR_FORMULA);
    createEReference(hmlOrFormulaEClass, HML_OR_FORMULA__LEFT);
    createEReference(hmlOrFormulaEClass, HML_OR_FORMULA__RIGHT);

    hmlAndFormulaEClass = createEClass(HML_AND_FORMULA);
    createEReference(hmlAndFormulaEClass, HML_AND_FORMULA__LEFT);
    createEReference(hmlAndFormulaEClass, HML_AND_FORMULA__RIGHT);

    ltlOrFormulaEClass = createEClass(LTL_OR_FORMULA);
    createEReference(ltlOrFormulaEClass, LTL_OR_FORMULA__LEFT);
    createEReference(ltlOrFormulaEClass, LTL_OR_FORMULA__RIGHT);

    ltlAndFormulaEClass = createEClass(LTL_AND_FORMULA);
    createEReference(ltlAndFormulaEClass, LTL_AND_FORMULA__LEFT);
    createEReference(ltlAndFormulaEClass, LTL_AND_FORMULA__RIGHT);

    ltlUntilFormulaEClass = createEClass(LTL_UNTIL_FORMULA);
    createEReference(ltlUntilFormulaEClass, LTL_UNTIL_FORMULA__LEFT);
    createEReference(ltlUntilFormulaEClass, LTL_UNTIL_FORMULA__RIGHT);

    cupLabelPredicateEClass = createEClass(CUP_LABEL_PREDICATE);
    createEReference(cupLabelPredicateEClass, CUP_LABEL_PREDICATE__LEFT);
    createEReference(cupLabelPredicateEClass, CUP_LABEL_PREDICATE__RIGH);

    capLabelPredicateEClass = createEClass(CAP_LABEL_PREDICATE);
    createEReference(capLabelPredicateEClass, CAP_LABEL_PREDICATE__LEFT);
    createEReference(capLabelPredicateEClass, CAP_LABEL_PREDICATE__RIGHT);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    ltsEClass.getESuperTypes().add(this.getElement());
    ltsCompositionEClass.getESuperTypes().add(this.getLtsBody());
    referenceEClass.getESuperTypes().add(this.getCompositionBody());
    ltsDeclarationBodyEClass.getESuperTypes().add(this.getLtsBody());
    labelEClass.getESuperTypes().add(this.getElement());
    actionEClass.getESuperTypes().add(this.getElement());
    formulaEClass.getESuperTypes().add(this.getElement());
    hmlFormulaDeclarationEClass.getESuperTypes().add(this.getFormula());
    hmlPredicateEClass.getESuperTypes().add(this.getHmlFormula());
    hmlTrueEClass.getESuperTypes().add(this.getHmlFormula());
    hmlFalseEClass.getESuperTypes().add(this.getHmlFormula());
    hmlNotFormulaEClass.getESuperTypes().add(this.getHmlFormula());
    hmlDiamondFormulaEClass.getESuperTypes().add(this.getHmlFormula());
    hmlBoxFormulaEClass.getESuperTypes().add(this.getHmlFormula());
    hmlRecursionFormulaEClass.getESuperTypes().add(this.getHmlFormula());
    hmlMinFixPointEClass.getESuperTypes().add(this.getHmlRecursionFormula());
    hmlMaxFixPointEClass.getESuperTypes().add(this.getHmlRecursionFormula());
    hmlRecursionVariableEClass.getESuperTypes().add(this.getHmlFormula());
    ltlFormulaDeclarationEClass.getESuperTypes().add(this.getFormula());
    ltlAtomicPropositionEClass.getESuperTypes().add(this.getLtlFormula());
    ltlNextFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    ltlAlwaysFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    ltlEventuallyFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    ltlNotFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    ltlTrueEClass.getESuperTypes().add(this.getLtlFormula());
    ltlFalseEClass.getESuperTypes().add(this.getLtlFormula());
    notLabelPredicateEClass.getESuperTypes().add(this.getLabelPredicate());
    actionLabelPredicateEClass.getESuperTypes().add(this.getLabelPredicate());
    anyLabelPredicateEClass.getESuperTypes().add(this.getLabelPredicate());
    interleavingEClass.getESuperTypes().add(this.getCompositionBody());
    synchronizationEClass.getESuperTypes().add(this.getCompositionBody());
    controlledInteractionEClass.getESuperTypes().add(this.getCompositionBody());
    renamingEClass.getESuperTypes().add(this.getCompositionBody());
    hmlOrFormulaEClass.getESuperTypes().add(this.getHmlFormula());
    hmlAndFormulaEClass.getESuperTypes().add(this.getHmlFormula());
    ltlOrFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    ltlAndFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    ltlUntilFormulaEClass.getESuperTypes().add(this.getLtlFormula());
    cupLabelPredicateEClass.getESuperTypes().add(this.getLabelPredicate());
    capLabelPredicateEClass.getESuperTypes().add(this.getLabelPredicate());

    // Initialize classes and features; add operations and parameters
    initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getModel_Name(), ecorePackage.getEString(), "name", null, 0, 1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModel_Elements(), this.getElement(), null, "elements", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(elementEClass, Element.class, "Element", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, Element.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltsEClass, Lts.class, "Lts", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLts_Body(), this.getLtsBody(), null, "body", null, 0, 1, Lts.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltsBodyEClass, LtsBody.class, "LtsBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(ltsCompositionEClass, LtsComposition.class, "LtsComposition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtsComposition_Body(), this.getCompositionBody(), null, "body", null, 0, 1, LtsComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(compositionBodyEClass, CompositionBody.class, "CompositionBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(referenceEClass, Reference.class, "Reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getReference_Lts(), this.getLts(), null, "lts", null, 0, 1, Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(mappingEClass, Mapping.class, "Mapping", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMapping_Src(), this.getAction(), null, "src", null, 0, 1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMapping_Trg(), this.getAction(), null, "trg", null, 0, 1, Mapping.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltsDeclarationBodyEClass, LtsDeclarationBody.class, "LtsDeclarationBody", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtsDeclarationBody_States(), this.getLtsState(), null, "states", null, 0, -1, LtsDeclarationBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtsDeclarationBody_Init(), this.getLtsState(), null, "init", null, 0, -1, LtsDeclarationBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtsDeclarationBody_Rules(), this.getLtsRule(), null, "rules", null, 0, -1, LtsDeclarationBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtsDeclarationBody_Labels(), this.getLabelRule(), null, "labels", null, 0, -1, LtsDeclarationBody.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltsRuleEClass, LtsRule.class, "LtsRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtsRule_Src(), this.getLtsState(), null, "src", null, 0, 1, LtsRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtsRule_Act(), this.getAction(), null, "act", null, 0, 1, LtsRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtsRule_Trg(), this.getLtsState(), null, "trg", null, 0, 1, LtsRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltsStateEClass, LtsState.class, "LtsState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLtsState_Name(), ecorePackage.getEString(), "name", null, 0, 1, LtsState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(labelRuleEClass, LabelRule.class, "LabelRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLabelRule_Label(), this.getLabel(), null, "label", null, 0, 1, LabelRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLabelRule_States(), this.getLtsState(), null, "states", null, 0, -1, LabelRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(stateEClass, State.class, "State", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getState_IsInitial(), ecorePackage.getEBoolean(), "isInitial", null, 0, 1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getState_Name(), ecorePackage.getEString(), "name", null, 0, 1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getState_Rules(), this.getRule(), null, "rules", null, 0, -1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ruleEClass, Rule.class, "Rule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRule_Action(), this.getAction(), null, "action", null, 0, 1, Rule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRule_Next(), this.getState(), null, "next", null, 0, 1, Rule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(actionEClass, Action.class, "Action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(formulaEClass, Formula.class, "Formula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(hmlFormulaDeclarationEClass, HmlFormulaDeclaration.class, "HmlFormulaDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlFormulaDeclaration_Formula(), this.getHmlFormula(), null, "formula", null, 0, 1, HmlFormulaDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlFormulaEClass, HmlFormula.class, "HmlFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(hmlPredicateEClass, HmlPredicate.class, "HmlPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlPredicate_Label(), this.getLabel(), null, "label", null, 0, 1, HmlPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlTrueEClass, HmlTrue.class, "HmlTrue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(hmlFalseEClass, HmlFalse.class, "HmlFalse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(hmlNotFormulaEClass, HmlNotFormula.class, "HmlNotFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlNotFormula_Arg(), this.getHmlFormula(), null, "arg", null, 0, 1, HmlNotFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlDiamondFormulaEClass, HmlDiamondFormula.class, "HmlDiamondFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlDiamondFormula_Action(), this.getLabelPredicate(), null, "action", null, 0, 1, HmlDiamondFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHmlDiamondFormula_Arg(), this.getHmlFormula(), null, "arg", null, 0, 1, HmlDiamondFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlBoxFormulaEClass, HmlBoxFormula.class, "HmlBoxFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlBoxFormula_Action(), this.getLabelPredicate(), null, "action", null, 0, 1, HmlBoxFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHmlBoxFormula_Arg(), this.getHmlFormula(), null, "arg", null, 0, 1, HmlBoxFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlRecursionFormulaEClass, HmlRecursionFormula.class, "HmlRecursionFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getHmlRecursionFormula_Name(), ecorePackage.getEString(), "name", null, 0, 1, HmlRecursionFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHmlRecursionFormula_Arg(), this.getHmlFormula(), null, "arg", null, 0, 1, HmlRecursionFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlMinFixPointEClass, HmlMinFixPoint.class, "HmlMinFixPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(hmlMaxFixPointEClass, HmlMaxFixPoint.class, "HmlMaxFixPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(hmlRecursionVariableEClass, HmlRecursionVariable.class, "HmlRecursionVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlRecursionVariable_Refeerence(), this.getHmlRecursionFormula(), null, "refeerence", null, 0, 1, HmlRecursionVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlFormulaDeclarationEClass, LtlFormulaDeclaration.class, "LtlFormulaDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlFormulaDeclaration_Formula(), this.getLtlFormula(), null, "formula", null, 0, 1, LtlFormulaDeclaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlFormulaEClass, LtlFormula.class, "LtlFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(ltlAtomicPropositionEClass, LtlAtomicProposition.class, "LtlAtomicProposition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlAtomicProposition_Prop(), this.getLabel(), null, "prop", null, 0, 1, LtlAtomicProposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlNextFormulaEClass, LtlNextFormula.class, "LtlNextFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlNextFormula_Arg(), this.getLtlFormula(), null, "arg", null, 0, 1, LtlNextFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlAlwaysFormulaEClass, LtlAlwaysFormula.class, "LtlAlwaysFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlAlwaysFormula_Arg(), this.getLtlFormula(), null, "arg", null, 0, 1, LtlAlwaysFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlEventuallyFormulaEClass, LtlEventuallyFormula.class, "LtlEventuallyFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlEventuallyFormula_Arg(), this.getLtlFormula(), null, "arg", null, 0, 1, LtlEventuallyFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlNotFormulaEClass, LtlNotFormula.class, "LtlNotFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlNotFormula_Arg(), this.getLtlFormula(), null, "arg", null, 0, 1, LtlNotFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlTrueEClass, LtlTrue.class, "LtlTrue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(ltlFalseEClass, LtlFalse.class, "LtlFalse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(labelPredicateEClass, LabelPredicate.class, "LabelPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(notLabelPredicateEClass, NotLabelPredicate.class, "NotLabelPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNotLabelPredicate_Arg(), this.getLabelPredicate(), null, "arg", null, 0, 1, NotLabelPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(actionLabelPredicateEClass, ActionLabelPredicate.class, "ActionLabelPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getActionLabelPredicate_Act(), this.getAction(), null, "act", null, 0, 1, ActionLabelPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(anyLabelPredicateEClass, AnyLabelPredicate.class, "AnyLabelPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(interleavingEClass, Interleaving.class, "Interleaving", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getInterleaving_Left(), this.getCompositionBody(), null, "left", null, 0, 1, Interleaving.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getInterleaving_Right(), this.getCompositionBody(), null, "right", null, 0, 1, Interleaving.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(synchronizationEClass, Synchronization.class, "Synchronization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSynchronization_Left(), this.getCompositionBody(), null, "left", null, 0, 1, Synchronization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSynchronization_Right(), this.getCompositionBody(), null, "right", null, 0, 1, Synchronization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(controlledInteractionEClass, ControlledInteraction.class, "ControlledInteraction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getControlledInteraction_Left(), this.getCompositionBody(), null, "left", null, 0, 1, ControlledInteraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getControlledInteraction_Action(), this.getAction(), null, "action", null, 0, -1, ControlledInteraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getControlledInteraction_Right(), this.getCompositionBody(), null, "right", null, 0, 1, ControlledInteraction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(renamingEClass, Renaming.class, "Renaming", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRenaming_Arg(), this.getCompositionBody(), null, "arg", null, 0, 1, Renaming.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRenaming_Maps(), this.getMapping(), null, "maps", null, 0, -1, Renaming.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlOrFormulaEClass, HmlOrFormula.class, "HmlOrFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlOrFormula_Left(), this.getHmlFormula(), null, "left", null, 0, 1, HmlOrFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHmlOrFormula_Right(), this.getHmlFormula(), null, "right", null, 0, 1, HmlOrFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(hmlAndFormulaEClass, HmlAndFormula.class, "HmlAndFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getHmlAndFormula_Left(), this.getHmlFormula(), null, "left", null, 0, 1, HmlAndFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getHmlAndFormula_Right(), this.getHmlFormula(), null, "right", null, 0, 1, HmlAndFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlOrFormulaEClass, LtlOrFormula.class, "LtlOrFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlOrFormula_Left(), this.getLtlFormula(), null, "left", null, 0, 1, LtlOrFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtlOrFormula_Right(), this.getLtlFormula(), null, "right", null, 0, 1, LtlOrFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlAndFormulaEClass, LtlAndFormula.class, "LtlAndFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlAndFormula_Left(), this.getLtlFormula(), null, "left", null, 0, 1, LtlAndFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtlAndFormula_Right(), this.getLtlFormula(), null, "right", null, 0, 1, LtlAndFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlUntilFormulaEClass, LtlUntilFormula.class, "LtlUntilFormula", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLtlUntilFormula_Left(), this.getLtlFormula(), null, "left", null, 0, 1, LtlUntilFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLtlUntilFormula_Right(), this.getLtlFormula(), null, "right", null, 0, 1, LtlUntilFormula.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(cupLabelPredicateEClass, CupLabelPredicate.class, "CupLabelPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCupLabelPredicate_Left(), this.getLabelPredicate(), null, "left", null, 0, 1, CupLabelPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCupLabelPredicate_Righ(), this.getLabelPredicate(), null, "righ", null, 0, 1, CupLabelPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(capLabelPredicateEClass, CapLabelPredicate.class, "CapLabelPredicate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCapLabelPredicate_Left(), this.getLabelPredicate(), null, "left", null, 0, 1, CapLabelPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCapLabelPredicate_Right(), this.getLabelPredicate(), null, "right", null, 0, 1, CapLabelPredicate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //ComposedLtsPackageImpl

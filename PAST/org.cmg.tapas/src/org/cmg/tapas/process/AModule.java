package org.cmg.tapas.process;

import java.util.Observable;

import org.cmg.tapas.process.exception.UndefinedProcessException;

public abstract class AModule extends Observable {

	public abstract int getStatesNumber(String process);
	
	public abstract String getProcessCode(String process) throws UndefinedProcessException;	
}

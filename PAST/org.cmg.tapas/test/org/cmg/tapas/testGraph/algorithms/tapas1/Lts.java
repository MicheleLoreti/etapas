package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.StateInterface;

/**
 * La classe Lts rappresenta un lts, contenente un insieme di azioni,
 * un insieme di stati collegati tra loro ed uno
 * stato iniziale. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class Lts {
	/**
	 * Insieme degli stati.
	 **/	
	private StatesSet sp = null;

	/**
	 * Insieme delle azioni.
	 **/		
	private ActionsSet act = null;

	/**
	 * Stato iniziale.
	 **/		
	private State p0 = null;

	/**
	 * Secondo stato iniziale 
	 * qualora il sistema sia stato ottenuto come fusione
	 **/			
	private State p1 = null;

	/**
	 * Costruisce una nuovo sistema.
	 **/
	public Lts() {
		sp = new StatesSet();
		act = new ActionsSet();
  	}

	/**
	 * Restituisce l'insieme di azioni del processo.
	 **/	
	public ActionsSet getActionsSet() {
		return act;	
	}

	/**
	 * Restituisce l'insieme di stati del sistema.
	 **/		
	public StatesSet getStatesSet() {
		return sp;	
	}

	/**
	 * Restituisce lo stato iniziale del processo.
	 **/	
	public State getInitState() {
		return p0;	
	}	  	

	/**
	 * Setta lo stato iniziale del sistema.
	 **/	
	public void setInitState(State s) {
		p0 = s;	
	}

	/**
	 * Restituisce il secondo stato iniziale del processo.
	 **/	
	public State getSecondInitState() {
		return p1;	
	}	  	

	/**
	 * Setta il secondo stato iniziale del processo.
	 */	
	public void setSecondInitState(State s) {
		p1 = s;	
	}

	/**
	 * Aggiunge all'insieme delle azioni un oggetto.
	 **/
  	public int addAction(Action a) {
  		return act.addAction(a);
  	}
			
	/**
	 * Aggiunge all'insieme degli stati un oggetto di tipo <tt>State</tt>. 
	 **/	
  	public void addState(State s) {
		sp.addState(s);
  	}

	/**
	 * Aggiunge all'insieme degli stati un nuovo oggetto di tipo <tt>State</tt>. 
	 **/	
  	public State addNewState() {	
  		String s = getNewStateName();
  		State state = new State(s);
  		sp.addState(state);
  		
  		return state;	
  	}

	/**
	 * Costruisce un nuovo nome di stato che non sia gi� utilizzato.
	 **/	
	protected String getNewStateName() {
  		int count = sp.getDimension();
  		String s = Integer.toString(count);
  		while( sp.getState(s) != null ) {
  			count++;	
  			s = Integer.toString(count);
  		}
  		
  		return s;
	}

	/**
	 * Permette di ricercare uno stato all'interno dell'insieme degli stati.
	 **/	
	public State getState(String name) {
		return sp.getState(name);
	}

	/**
	 * Permette di ricercare un'azione all'interno dell'insieme delle azioni. 
	 **/		
	public Action getAction(String name) {
		return act.getAction(name);
	}

	/**
	 * Crea un nuovo sistema corrispondente alla chiusura tau-transitiva 
	 * di questo sistema.
	 * In particolare tutte le transizioni tau vengono rietichettate 
	 * con l'azione <tt>Action.EPSILON</tt>, per
	 * ciascuno stato viene aggiunta una transizione 
	 * riflessiva etichettata con <tt>Action.EPSILON</tt>, ed 
	 * infine viene costruita la chiusura transitiva di tutte le 
	 * transizioni etichettate con <tt>Action.EPSILON</tt>.
	 * Il sistema corrente viene lasciato inalterato.
	 **/		
	public Lts buildClosure() {
		Lts newGraph = new Lts();			
		StatesSet statesSet = null;
		StatesSet chiusuraCurrentState = null;
		StatesSet dp = null;
		StatesSet chiusuraDp = null;
		State currentState = null;
		State newState = null;
		ActionsSet actionsSet = null;
		Action currentAction = null;
		Action epsilon = null;
		Action newAction = null;
		HashMap<State, State> hmS = new HashMap<State, State>();
		HashMap<Action, Action> hmA = new HashMap<Action, Action>();

		// Creo l'insieme degli stati
		statesSet = this.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = newGraph.addNewState();
			hmS.put( statesSet.stateAt(i), newState );
		}
		
		// Creo l'insieme delle azioni, tolgo tau ed aggiungo epsilon
		actionsSet = this.getActionsSet();
		epsilon = Action.ACTION_EPSILON;
		newGraph.addAction(epsilon);
		for (int i=0; i<actionsSet.getDimension(); i++) {
			if (actionsSet.actionAt(i).isTau()) 
				hmA.put(actionsSet.actionAt(i), epsilon);
			else {
				newAction = new Action(actionsSet.actionAt(i).getName());
				newGraph.addAction(newAction);
				hmA.put(actionsSet.actionAt(i),newAction);
			}
		}
		
					
		// Creo la relazione di transizione con la epsilon chiusura
		for (int i=0; i<statesSet.getDimension(); i++) {		
			currentState = statesSet.stateAt(i); 
			chiusuraCurrentState = currentState.sEpsilon();
			for (int j=0; j<chiusuraCurrentState.getDimension(); j++)
				hmS.get(currentState).addTransition( 
					epsilon,
					hmS.get(chiusuraCurrentState.stateAt(j))
				);
			for (int j=0; j<actionsSet.getDimension(); j++){				
				currentAction = actionsSet.actionAt(j);
				if (!(currentAction.isTau())) {
					dp = chiusuraCurrentState.dP(currentAction);
					chiusuraDp = dp.sEpsilon();
					for (int k=0; k<chiusuraDp.getDimension(); k++)
						hmS.get(currentState).addTransition( 
							hmA.get(currentAction),
							hmS.get(chiusuraDp.stateAt(k))
						);															 		  
				}
			}
		}
		
		// Creo lo stato iniziale
		newGraph.setInitState( hmS.get(this.getInitState()) );
			
		return newGraph;
	}

	/**
	 * Crea un nuovo processo annullando gli effetti della chiusura 
	 * tau-transitiva dal processo corrente.
	 * In particolare tutte le transizioni epsilon vengono 
	 * rietichettate con l'azione <tt>Action.TAU</tt>, per
	 * ciascuno stato viene eliminata la transizione riflessiva 
	 * etichettata con <tt>Action.TAU</tt>, ed 
	 * infine viene annullata la chiusura transitiva di tutte le 
	 * transizioni etichettate con <tt>Action.TAU</tt>.
	 * Il sistema corrente viene lasciato inalterato.
	 **/	
	public Lts buildDeClosure() {
		Lts newGraph = new Lts();			
		StatesSet statesSet = null;
		ActionsSet actionsSet = null;
		State newState = null;
		Action newAction = null;
		State s1 = null;
		State s2 = null;
		Action a = null;
		Action tau = null;
		StatesSet init = null;
		StatesSet initTau = null;
		HashMap<State, State> hmS = new HashMap<State, State>();
		HashMap<Action, Action> hmA = new HashMap<Action, Action>();
		Vector<TransitionCouple> transitionCouples = null;
		TransitionCouple tc = null;
		StatesSet destinations = null;

		
		// Creo l'insieme degli stati
		statesSet = this.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = newGraph.addNewState();
			hmS.put(statesSet.stateAt(i),newState);			
		}
		
		// Creo l'insieme delle azioni
		actionsSet = this.getActionsSet();
		for (int i=0; i<actionsSet.getDimension(); i++) {
			if( actionsSet.actionAt(i).isEpsilon() ) 
				newAction = Action.ACTION_TAU;
			else 
				newAction = new Action(actionsSet.actionAt(i).getName());
			newGraph.addAction(newAction);
			hmA.put(actionsSet.actionAt(i),newAction);
		}
					

		// Creo la relazione di transizione rinominando epsilon in tau
		// ed eliminando le transizioni tau riflessive
		statesSet = this.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					s1 = statesSet.stateAt(i);
					a = tc.getAction();
					s2 = destinations.stateAt(k);					
					if (!((a.isEpsilon())&&(s1 == s2)))
						hmS.get(s1).addTransition( hmA.get(a), hmS.get(s2) );
				}
			}
		}
		
		// Annullando la chiusura transitiva eliminando alcune transizioni superflue di newGraph
		statesSet = newGraph.getStatesSet();
		tau = newGraph.getAction(Action.TAU);
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					s1 = statesSet.stateAt(i);
					a = tc.getAction();
					s2 = destinations.stateAt(k);
					init = s1.dP(a);
					initTau = init.dP(tau);
					if (initTau.containState(s2)) s1.removeTransition(a, s2);
					else {
						initTau = s1.dP(tau);
						init = initTau.dP(a);
						if (init.containState(s2)) s1.removeTransition(a, s2);
					}
				}
			}
		}

		// Creo lo stato iniziale
		newGraph.setInitState( hmS.get(this.getInitState()) );
			
		return newGraph;
	}

	/**
	 * Crea un nuovo sistema corrispondente 
	 * al grafo passato come parametro. 
	 **/
	public static <S extends StateInterface, A extends ActionInterface> 
	Lts createProcess(
		GraphInterface<S,A> graph, 
		S initState1,
		S initState2
	) {
		Lts p = new Lts();
		HashMap<S,State> hm = new HashMap<S,State>();
		
		// creazione dell'insieme degli stati		
		State state = null;
		for( S oldState: graph.getAllStates() ) {
			state = p.addNewState();
			hm.put( oldState, state );
			if( oldState==initState1 )
				p.setInitState(state);
			else if( oldState==initState2 )
				p.setSecondInitState(state);
		}

		// creazione della relazione di transizione 
		for( S oldState: graph.getAllStates() ) {
			Map<A, ? extends Set<S>> map = graph.getPostsetMap(oldState);
			if( map==null )
				continue;
			Set<S> postset;		
			for( A action: map.keySet() ) {
				Action newAction = p.getAction(action.getId());
				if( newAction == null ) {
					newAction = new Action(action.getId());
					p.addAction(newAction);
				}
				postset = map.get(action);
				if( postset!=null ) {
					for( S oldState2: postset ){
						hm.get(oldState).addTransition(
							newAction, hm.get(oldState2)
						);
					}
				}
			}
		}

		return p;
	}

	/**
	 * Crea un nuovo sistema derivato dalla fusione dei due sistemi
	 * passati come parametri. 
	 * Durante l'operazione di fusione vengono generati stati completamente
	 * nuovi all'interno del nuovo sistema. 
	 */
	public static Lts createProcessFusion(
		Lts p1, 
		Lts p2
	) {
		Lts p = new Lts();
		ActionsSet actionsSet = null;
		StatesSet statesSet = null;
		State stateOrigin = null;
		State stateDest = null;
		Action a = null;		
		State newState = null;
		int actualIndex = 0;
		StatesSet destinations = null;
		TransitionCouple tc = null;
		Vector<TransitionCouple> transitionCouples = null;
		HashMap<State, State> hmS = new HashMap<State, State>();
		HashMap<Action, Action> hmA = new HashMap<Action, Action>();
		
		// Fondo l'insieme delle azioni
		actionsSet = p1.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex));
		}
		actionsSet = p2.getActionsSet();
		for (int i=0; i<actionsSet.getDimension();i++) {
			actualIndex = p.addAction(actionsSet.actionAt(i));
			hmA.put(actionsSet.actionAt(i),p.getActionsSet().actionAt(actualIndex));
		}

		// Fondo l'insieme degli stati
		statesSet = p1.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = p.addNewState();
			hmS.put(statesSet.stateAt(i),newState);
		}
		statesSet = p2.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			newState = p.addNewState();
			hmS.put(statesSet.stateAt(i),newState);
		}
		
		// Creo la relazione di transizione del processo fuso
		statesSet = p1.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}

		statesSet = p2.getStatesSet();
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					stateOrigin = hmS.get(statesSet.stateAt(i));
					stateDest = hmS.get(destinations.stateAt(k));
					a = hmA.get(tc.getAction());
					stateOrigin.addTransition(a,stateDest);
				}
			}
		}

		//Setto i 2 stati iniziali del nuovo processo
		p.setInitState( hmS.get(p1.getInitState()) );
		p.setSecondInitState( hmS.get(p2.getInitState()));
		
		return p;
	}

	/**
	 * Costruisce ricorsivamente il Deterministic Graph 
	 * corrispondente al sistema passato come parametro
	 * il cui stato iniziale sia associato all'insieme di stati 
	 * del processo originale passato come parametro. 
	 **/
	private static State buildDG(
	 	Lts p,
		Lts p1,
		StatesSet s,
		HMap h
	) {
		State tnew = h.getElement(s,true);
		if( tnew != null ) 
			return tnew;

		State t = p1.addNewState();
		h.addItem(s, true, t);
		ActionsSet as = p.getActionsSet();
		for( int i=0; i<as.getDimension(); i++ ) {
			Action alfa = as.actionAt(i); 
			StatesSet image = s.dP(alfa);
			if( !image.isEmpty() ) {
				tnew = buildDG( p, p1, image, h );
				t.addTransition( alfa, tnew );
			}
		}

		return t;
	}

	/**
	 * Costruisce il Deterministic Graph corrispondente 
	 * al processo passato come parametro.
	 **/	
	public static Lts buildDeterministicGraph( Lts p ) {
		Lts p1 = new Lts();
		StatesSet ss = new StatesSet();
		State initState = null;
		ActionsSet as = null;
		Action alfa = null;
		HMap h = new HMap();

		as = p.getActionsSet();
		for (int i=0; i<as.getDimension(); i++) {
			alfa = as.actionAt(i);
			p1.addAction(alfa);
		}
		
		ss.addState(p.getInitState());
		initState = buildDG(p,p1,ss,h);
		
		p1.setInitState(initState);
		
		return p1;
	}
	
	public static Lts buildNoCicleGraph( Lts p ) {
		return p;
	}	
	

	/**
	 * Stampa l'intero processo sullo standard output. 
	 **/ 
	public void print() {
		System.out.print("  |-- States Set: ");
		this.sp.print();
		System.out.print("\n  |-- Actions Set: ");
		this.act.print();
		System.out.print("\n  |-- Transition Relation:\n");
		this.sp.printTransitions();
		System.out.print("  |-- Initial State: ");
		if (p0 == null)	System.out.print("nessuno");
		else this.p0.print();
		if (p1 != null)	{
			System.out.print("\n  |-- Second Initial State: ");
			this.p1.print();
		}
		System.out.print("\n\n");
	}	


}


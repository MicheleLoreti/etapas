package org.cmg.tapas.graph;

public class NoTauFilter<S extends StateInterface,
						A extends ActionInterface	
						> extends EdgeFilter<S,A> {

	@Override
	public boolean check(StateInterface src, ActionInterface action,
			StateInterface dest) {
		return !action.isTau();
	}

}

/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Or Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getLeft <em>Left</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlOrFormula()
 * @model
 * @generated
 */
public interface HmlOrFormula extends HmlFormula
{
  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(HmlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlOrFormula_Left()
   * @model containment="true"
   * @generated
   */
  HmlFormula getLeft();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(HmlFormula value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(HmlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlOrFormula_Right()
   * @model containment="true"
   * @generated
   */
  HmlFormula getRight();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(HmlFormula value);

} // HmlOrFormula

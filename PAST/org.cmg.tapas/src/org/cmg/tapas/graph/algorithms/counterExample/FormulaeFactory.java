package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import org.cmg.tapas.formulae.hml.AndFormula;
import org.cmg.tapas.formulae.hml.BoxFormula;
import org.cmg.tapas.formulae.hml.DiamondFormula;
import org.cmg.tapas.formulae.hml.False;
import org.cmg.tapas.formulae.hml.HMLUntilFormula;
import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.formulae.hml.OrFormula;
import org.cmg.tapas.formulae.hml.True;
import org.cmg.tapas.formulae.hml.WeakBoxFormula;
import org.cmg.tapas.formulae.hml.WeakDiamondFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.TrueFilter;
import org.cmg.tapas.processAlgebra.APProcess;


public class FormulaeFactory<
		  S extends APProcess<S, A>
		, A extends ActionInterface> {
	
	private boolean strong;
	private A tauAct;
	
	public FormulaeFactory(boolean strong, A tauAct){
		this.strong = strong;
		this.tauAct= tauAct;
	}
	
	private boolean intersectionEmpty(LinkedList<HmlFormula<S, A>> l1, LinkedList<HmlFormula<S, A>> l2){
		for (HmlFormula<S, A> f : l1) {
			if(l2.contains(f))
				return false;
		}
		return true;		
	}

	private LinkedList<LinkedList<HmlFormula<S,A>>> minimize(LinkedList<LinkedList<HmlFormula<S, A>>> list){
		Vector<LinkedList<LinkedList<HmlFormula<S,A>>>> res = new Vector<LinkedList<LinkedList<HmlFormula<S,A>>>>();
		
		//init
		LinkedList<HmlFormula<S,A>> first =list.getFirst();
		LinkedList<LinkedList<HmlFormula<S,A>>> V0 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
		for (HmlFormula<S, A> f : first) {
			LinkedList<HmlFormula<S,A>> tmp = new LinkedList<HmlFormula<S,A>>();
			tmp.add(f);
			V0.add(tmp);
		}
		res.add(V0);
		
		//Step
		int end = list.size()-1;
		for (int i = 0; i < end; i++) {
			res.add(i+1, new LinkedList<LinkedList<HmlFormula<S,A>>>());
			LinkedList<LinkedList<HmlFormula<S,A>>> Vi = res.get(i);
			for (LinkedList<HmlFormula<S, A>> fList : Vi) {
				if(!intersectionEmpty(fList, list.get(i+1))){
					res.get(i+1).add(fList);
				}
			}
			
			if(res.get(i+1).size() == 0){
				for (LinkedList<HmlFormula<S, A>> fList : Vi) {
					LinkedList<HmlFormula<S,A>> foo = list.get(i+1);
					for (HmlFormula<S, A> f : foo) {
						fList.add(f);
						res.get(i+1).add(fList);
					}
				}
			}
			
		}
		
		return res.get(res.size()-1);
	}

	protected HmlFormula<S,A> createDiamond(Set<A> actSet, HmlFormula<S,A> next){
		Iterator<A> iter = actSet.iterator();
		LinkedList<HmlFormula<S,A>> list = new LinkedList<HmlFormula<S,A>>();
		while (iter.hasNext()) {
			A act = iter.next();
			
			HmlFormula<S,A> f;
			if(strong)
				f = new DiamondFormula<S, A>(act, next);
			else 
				f = new WeakDiamondFormula<S, A>(act, next, tauAct);
			list.add(f);
		}
		if(list.size() == 1){
			return list.getFirst();
		}else{
			return new AndFormula<S, A>(list);
		}
	}

	protected HmlFormula<S,A> createBox(Set<A> actSet, HmlFormula<S,A> next){
		if(strong)
			return new BoxFormula<S, A>(actSet, next);
		else
			return new WeakBoxFormula<S, A>(actSet, next, tauAct);
	}
	
	protected LinkedList<HmlFormula<S,A>> createBox(A act, LinkedList<LinkedList<HmlFormula<S, A>>> list){
		LinkedList<HmlFormula<S,A>> res = new LinkedList<HmlFormula<S,A>>();
		LinkedList<LinkedList<HmlFormula<S,A>>> newList = minimize(list);
		for (LinkedList<HmlFormula<S, A>> fList : newList) {
			HmlFormula<S, A> box = null;
			if(fList.size() == 1){
				if(strong){
					box = new BoxFormula<S, A>(act, fList.getFirst());
				} else{
					if(act.isTau())
						box = new WeakBoxFormula<S, A>(fList.getFirst(), tauAct, true);
					else
						box = new WeakBoxFormula<S, A>(act, fList.getFirst(), tauAct);
				}
			}else if(fList.size() > 1){
				OrFormula<S, A> OrF = new OrFormula<S, A>(fList);
				if(strong){
					box = new BoxFormula<S, A>(act, OrF);
				} else{
					box = new WeakBoxFormula<S, A>(act, OrF, tauAct);
				}
			}
			if(box != null)
				res.add(box);
		}
		return res;
	}
	
	protected LinkedList<HmlFormula<S,A>> createAnd(LinkedList<LinkedList<HmlFormula<S, A>>> list){
		LinkedList<HmlFormula<S,A>> res = new LinkedList<HmlFormula<S,A>>();
		LinkedList<LinkedList<HmlFormula<S,A>>> newList = minimize(list);
		for (LinkedList<HmlFormula<S, A>> fList : newList) {
			HmlFormula<S, A> diamond = null;
			if(fList.size() == 1){
				diamond = fList.getFirst();	
			}else if(fList.size() > 1){						
				diamond = new AndFormula<S, A>(fList);
			}
			if(diamond != null)
				res.add(diamond);
		}
		return res;
	}
	
	protected LinkedList<HmlFormula<S,A>> createDiamond(A act, LinkedList<LinkedList<HmlFormula<S, A>>> list){
		LinkedList<HmlFormula<S,A>> res = new LinkedList<HmlFormula<S,A>>();
		LinkedList<LinkedList<HmlFormula<S,A>>> newList = minimize(list);
		for (LinkedList<HmlFormula<S, A>> fList : newList) {
			HmlFormula<S, A> diamond = null;
			if(fList.size() == 1){
				if(strong){
					diamond = new DiamondFormula<S, A>(act, fList.getFirst());
				} else{
					if(act.isTau())
						diamond = new WeakDiamondFormula<S, A>(fList.getFirst(), tauAct, true);
					else
						diamond = new WeakDiamondFormula<S, A>(act, fList.getFirst(), tauAct);
				}			
			}else if(fList.size() > 1){						
				AndFormula<S, A> andF = new AndFormula<S, A>(fList);
				if(strong){
					diamond = new DiamondFormula<S, A>(act, andF);
				} else{
					diamond = new WeakDiamondFormula<S, A>(act, andF, tauAct);
				}
			}
			if(diamond != null)
				res.add(diamond);
		}
		return res;
	}
	
	protected HmlFormula<S,A> createDiamond(A act, HmlFormula<S, A> f){
		if (strong)
			return new DiamondFormula<S, A>(act, f);
		else
			return new WeakDiamondFormula<S, A>(act, f, tauAct);
	}

	protected HmlFormula<S, A> createBox(A act, HmlFormula<S, A> f) {
		if (strong)
			return new BoxFormula<S, A>(act, f);
		else
			return new WeakBoxFormula<S, A>(act, f, tauAct);
	}
	
	protected HmlFormula<S, A> createBox(HmlFormula<S, A> f) {
		if (strong)
			return new BoxFormula<S, A>(new TrueFilter<A>(), f);
		else
			return new WeakBoxFormula<S, A>(new TrueFilter<A>(), f, tauAct);
	}

	protected HmlFormula<S, A> createDiamond(HmlFormula<S, A> f) {
		if (strong)
			return new DiamondFormula<S, A>(new TrueFilter<A>(), f);
		else
			return new WeakDiamondFormula<S, A>(new TrueFilter<A>(), f, tauAct);
	}
	
	protected HmlFormula<S,A> create(Set<A> exist, Set<A> notExist){
		if(exist == null || exist.size() == 0){
			return createBox(notExist, new False<S, A>());
		}else if(notExist == null || notExist.size() == 0){
			return createDiamond(exist, new True<S, A>());
		}
		HmlFormula<S, A> f1 = createDiamond(exist, new True<S, A>());
		HmlFormula<S, A> f2 = createBox(notExist, new False<S, A>());
		return new AndFormula<S, A>(f1, f2);
	}

	protected HmlFormula<S,A> createHMLUntil(HmlFormula<S,A> f1, Set<A> actSet){
		Iterator<A> iter = actSet.iterator();
		LinkedList<HmlFormula<S,A>> list = new LinkedList<HmlFormula<S,A>>();
		while (iter.hasNext()) {
			A act = iter.next();
			HmlFormula<S,A> f = new HMLUntilFormula<S, A>(act);
			list.add(f);
		}
		if(list.size() == 1){
			return list.getFirst();
		}else{
			return new AndFormula<S, A>(list);
		}
	}

	protected HmlFormula<S,A> createHMLUntil(Set<A> actSet){
		Iterator<A> iter = actSet.iterator();
		LinkedList<HmlFormula<S,A>> list = new LinkedList<HmlFormula<S,A>>();
		while (iter.hasNext()) {
			A act = iter.next();
			HmlFormula<S,A> f = new HMLUntilFormula<S, A>(act);
			list.add(f);
		}
		if(list.size() == 1){
			return list.getFirst();
		}else{
			return new AndFormula<S, A>(list);
		}
	}

	protected LinkedList<HmlFormula<S,A>> createHmlUntil(A act, LinkedList<LinkedList<HmlFormula<S, A>>> list){
		LinkedList<HmlFormula<S,A>> res = new LinkedList<HmlFormula<S,A>>();
		LinkedList<LinkedList<HmlFormula<S,A>>> newList = minimize(list);
		for (LinkedList<HmlFormula<S, A>> fList : newList) {
			HmlFormula<S, A> formula = null;
			if(fList.size() == 1){   
				formula = new HMLUntilFormula<S, A>(fList.getFirst(), act, new True<S, A>());
			}else if(fList.size() > 1){	  
				AndFormula<S, A> andF = new AndFormula<S, A>(fList);
				formula = new HMLUntilFormula<S, A>(andF, act, new True<S, A>());
			}
			if(formula != null)
				res.add(formula);
		}
		return res;
	}
}

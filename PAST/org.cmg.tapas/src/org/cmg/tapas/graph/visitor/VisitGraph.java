package org.cmg.tapas.graph.visitor;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.EdgeFilter;
import org.cmg.tapas.graph.GraphData;
import org.cmg.tapas.graph.StateInterface;


public abstract class VisitGraph<P extends StateInterface,R extends ActionInterface> {
	
	protected String result = null;
	protected GraphData<P, R> graph;
	
	public VisitGraph( GraphData<P, R> graph ) {
		this.graph = graph;
	}
		
	public VisitGraph() {
		this.graph = null;
	}
	
	public void setGraph( GraphData<P, R> graph ) {
		this.graph = graph;
	}
	
	/**Data un <i>CCSGraph</i> restituisce una stringa che lo
	 * rappresenta.
	 *  
	 * @return Una rappresentazione del grafo.
	 */
	public final void visit(EdgeFilter<P,R> f) {
		
		doStart();
		LinkedList<P> states = graph.getAllStates();
		for (P current : states) {
			doVisit( current );
			Iterator<Entry<R, P>> i = graph.getImageIterator(current, true, f);
			Entry<R,P> foo;
			while (i.hasNext()) {
				foo = i.next();
				doVisitTransition( current , foo.getKey() , foo.getValue() );
			}
		}
		
		doEnd();
	}
	
	protected abstract void doVisit(P current);	
	protected abstract void doStart( );
	protected abstract void doEnd( );
	protected abstract void doVisitTransition(P src, R act, P dest);
	
	
}

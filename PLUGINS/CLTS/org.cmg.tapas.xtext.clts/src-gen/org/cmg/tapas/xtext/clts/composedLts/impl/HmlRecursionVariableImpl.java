/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hml Recursion Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionVariableImpl#getRefeerence <em>Refeerence</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HmlRecursionVariableImpl extends HmlFormulaImpl implements HmlRecursionVariable
{
  /**
   * The cached value of the '{@link #getRefeerence() <em>Refeerence</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRefeerence()
   * @generated
   * @ordered
   */
  protected HmlRecursionFormula refeerence;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected HmlRecursionVariableImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.HML_RECURSION_VARIABLE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlRecursionFormula getRefeerence()
  {
    if (refeerence != null && refeerence.eIsProxy())
    {
      InternalEObject oldRefeerence = (InternalEObject)refeerence;
      refeerence = (HmlRecursionFormula)eResolveProxy(oldRefeerence);
      if (refeerence != oldRefeerence)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComposedLtsPackage.HML_RECURSION_VARIABLE__REFEERENCE, oldRefeerence, refeerence));
      }
    }
    return refeerence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlRecursionFormula basicGetRefeerence()
  {
    return refeerence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRefeerence(HmlRecursionFormula newRefeerence)
  {
    HmlRecursionFormula oldRefeerence = refeerence;
    refeerence = newRefeerence;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.HML_RECURSION_VARIABLE__REFEERENCE, oldRefeerence, refeerence));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_RECURSION_VARIABLE__REFEERENCE:
        if (resolve) return getRefeerence();
        return basicGetRefeerence();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_RECURSION_VARIABLE__REFEERENCE:
        setRefeerence((HmlRecursionFormula)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_RECURSION_VARIABLE__REFEERENCE:
        setRefeerence((HmlRecursionFormula)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_RECURSION_VARIABLE__REFEERENCE:
        return refeerence != null;
    }
    return super.eIsSet(featureID);
  }

} //HmlRecursionVariableImpl

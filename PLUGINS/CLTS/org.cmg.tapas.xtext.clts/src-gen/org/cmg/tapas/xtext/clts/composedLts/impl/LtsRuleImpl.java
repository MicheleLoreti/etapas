/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.Action;
import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.LtsRule;
import org.cmg.tapas.xtext.clts.composedLts.LtsState;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lts Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl#getSrc <em>Src</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl#getAct <em>Act</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl#getTrg <em>Trg</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LtsRuleImpl extends MinimalEObjectImpl.Container implements LtsRule
{
  /**
   * The cached value of the '{@link #getSrc() <em>Src</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSrc()
   * @generated
   * @ordered
   */
  protected LtsState src;

  /**
   * The cached value of the '{@link #getAct() <em>Act</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAct()
   * @generated
   * @ordered
   */
  protected Action act;

  /**
   * The cached value of the '{@link #getTrg() <em>Trg</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTrg()
   * @generated
   * @ordered
   */
  protected LtsState trg;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LtsRuleImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.LTS_RULE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsState getSrc()
  {
    if (src != null && src.eIsProxy())
    {
      InternalEObject oldSrc = (InternalEObject)src;
      src = (LtsState)eResolveProxy(oldSrc);
      if (src != oldSrc)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComposedLtsPackage.LTS_RULE__SRC, oldSrc, src));
      }
    }
    return src;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsState basicGetSrc()
  {
    return src;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSrc(LtsState newSrc)
  {
    LtsState oldSrc = src;
    src = newSrc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.LTS_RULE__SRC, oldSrc, src));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action getAct()
  {
    if (act != null && act.eIsProxy())
    {
      InternalEObject oldAct = (InternalEObject)act;
      act = (Action)eResolveProxy(oldAct);
      if (act != oldAct)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComposedLtsPackage.LTS_RULE__ACT, oldAct, act));
      }
    }
    return act;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action basicGetAct()
  {
    return act;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAct(Action newAct)
  {
    Action oldAct = act;
    act = newAct;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.LTS_RULE__ACT, oldAct, act));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsState getTrg()
  {
    if (trg != null && trg.eIsProxy())
    {
      InternalEObject oldTrg = (InternalEObject)trg;
      trg = (LtsState)eResolveProxy(oldTrg);
      if (trg != oldTrg)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComposedLtsPackage.LTS_RULE__TRG, oldTrg, trg));
      }
    }
    return trg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LtsState basicGetTrg()
  {
    return trg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTrg(LtsState newTrg)
  {
    LtsState oldTrg = trg;
    trg = newTrg;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.LTS_RULE__TRG, oldTrg, trg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_RULE__SRC:
        if (resolve) return getSrc();
        return basicGetSrc();
      case ComposedLtsPackage.LTS_RULE__ACT:
        if (resolve) return getAct();
        return basicGetAct();
      case ComposedLtsPackage.LTS_RULE__TRG:
        if (resolve) return getTrg();
        return basicGetTrg();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_RULE__SRC:
        setSrc((LtsState)newValue);
        return;
      case ComposedLtsPackage.LTS_RULE__ACT:
        setAct((Action)newValue);
        return;
      case ComposedLtsPackage.LTS_RULE__TRG:
        setTrg((LtsState)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_RULE__SRC:
        setSrc((LtsState)null);
        return;
      case ComposedLtsPackage.LTS_RULE__ACT:
        setAct((Action)null);
        return;
      case ComposedLtsPackage.LTS_RULE__TRG:
        setTrg((LtsState)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_RULE__SRC:
        return src != null;
      case ComposedLtsPackage.LTS_RULE__ACT:
        return act != null;
      case ComposedLtsPackage.LTS_RULE__TRG:
        return trg != null;
    }
    return super.eIsSet(featureID);
  }

} //LtsRuleImpl

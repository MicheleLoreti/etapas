/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.IntegerType#getMin <em>Min</em>}</li>
 *   <li>{@link org.lic.secsi.IntegerType#getMax <em>Max</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getIntegerType()
 * @model
 * @generated
 */
public interface IntegerType extends Type, TypeExpression
{
  /**
   * Returns the value of the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Min</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Min</em>' containment reference.
   * @see #setMin(Expression)
   * @see org.lic.secsi.SecsiPackage#getIntegerType_Min()
   * @model containment="true"
   * @generated
   */
  Expression getMin();

  /**
   * Sets the value of the '{@link org.lic.secsi.IntegerType#getMin <em>Min</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Min</em>' containment reference.
   * @see #getMin()
   * @generated
   */
  void setMin(Expression value);

  /**
   * Returns the value of the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Max</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Max</em>' containment reference.
   * @see #setMax(Expression)
   * @see org.lic.secsi.SecsiPackage#getIntegerType_Max()
   * @model containment="true"
   * @generated
   */
  Expression getMax();

  /**
   * Sets the value of the '{@link org.lic.secsi.IntegerType#getMax <em>Max</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Max</em>' containment reference.
   * @see #getMax()
   * @generated
   */
  void setMax(Expression value);

} // IntegerType

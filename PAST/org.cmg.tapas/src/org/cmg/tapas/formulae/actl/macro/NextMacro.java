/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.formulae.actl.path.ActlNext;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class NextMacro< S extends APProcess<S, A> , A extends ActionInterface> extends PathFormulaMacro< S, A>{

	private ActlActionPredicate<S,A> predicate;
	private StateFormulaMacro< S, A > macro;
	
	public NextMacro( ActlActionPredicate<S,A> predicate , StateFormulaMacro<S, A> macro ) {
		super();
		this.predicate = predicate;
		this.macro = macro;
	}
	
	@Override
	public PathFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		return new ActlNext<S, A>(predicate , macro.getFormula(f));
	}

	@Override
	public int parameters() {
		return macro.parameters();
	}

	@Override
	public void accept(PathMacroVisitor<S, A> visitor) {
		visitor.visitNext( this );
	}

	public ActlActionPredicate<S, A> getPredicate() {
		return predicate;
	}

	public StateFormulaMacro<S, A> getMacro() {
		return macro;
	}

}

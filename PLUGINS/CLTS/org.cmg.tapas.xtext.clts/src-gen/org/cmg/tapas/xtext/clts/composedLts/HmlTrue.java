/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml True</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlTrue()
 * @model
 * @generated
 */
public interface HmlTrue extends HmlFormula
{
} // HmlTrue

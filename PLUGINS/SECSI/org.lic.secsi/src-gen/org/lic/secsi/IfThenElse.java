/**
 */
package org.lic.secsi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Then Else</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.IfThenElse#getExpression <em>Expression</em>}</li>
 *   <li>{@link org.lic.secsi.IfThenElse#getThenBranch <em>Then Branch</em>}</li>
 *   <li>{@link org.lic.secsi.IfThenElse#isWithElse <em>With Else</em>}</li>
 *   <li>{@link org.lic.secsi.IfThenElse#getElseBranch <em>Else Branch</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getIfThenElse()
 * @model
 * @generated
 */
public interface IfThenElse extends Statement
{
  /**
   * Returns the value of the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expression</em>' containment reference.
   * @see #setExpression(Expression)
   * @see org.lic.secsi.SecsiPackage#getIfThenElse_Expression()
   * @model containment="true"
   * @generated
   */
  Expression getExpression();

  /**
   * Sets the value of the '{@link org.lic.secsi.IfThenElse#getExpression <em>Expression</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Expression</em>' containment reference.
   * @see #getExpression()
   * @generated
   */
  void setExpression(Expression value);

  /**
   * Returns the value of the '<em><b>Then Branch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Then Branch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Then Branch</em>' containment reference.
   * @see #setThenBranch(Statement)
   * @see org.lic.secsi.SecsiPackage#getIfThenElse_ThenBranch()
   * @model containment="true"
   * @generated
   */
  Statement getThenBranch();

  /**
   * Sets the value of the '{@link org.lic.secsi.IfThenElse#getThenBranch <em>Then Branch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Then Branch</em>' containment reference.
   * @see #getThenBranch()
   * @generated
   */
  void setThenBranch(Statement value);

  /**
   * Returns the value of the '<em><b>With Else</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>With Else</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>With Else</em>' attribute.
   * @see #setWithElse(boolean)
   * @see org.lic.secsi.SecsiPackage#getIfThenElse_WithElse()
   * @model
   * @generated
   */
  boolean isWithElse();

  /**
   * Sets the value of the '{@link org.lic.secsi.IfThenElse#isWithElse <em>With Else</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>With Else</em>' attribute.
   * @see #isWithElse()
   * @generated
   */
  void setWithElse(boolean value);

  /**
   * Returns the value of the '<em><b>Else Branch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Else Branch</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else Branch</em>' containment reference.
   * @see #setElseBranch(Statement)
   * @see org.lic.secsi.SecsiPackage#getIfThenElse_ElseBranch()
   * @model containment="true"
   * @generated
   */
  Statement getElseBranch();

  /**
   * Sets the value of the '{@link org.lic.secsi.IfThenElse#getElseBranch <em>Else Branch</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else Branch</em>' containment reference.
   * @see #getElseBranch()
   * @generated
   */
  void setElseBranch(Statement value);

} // IfThenElse

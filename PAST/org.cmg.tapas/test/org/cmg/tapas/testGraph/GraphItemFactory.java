package org.cmg.tapas.testGraph;

import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.GraphItemFactoryI;

/** 
 * GraphItemFactory � una fabbrica di nodi e di azioni.
 * <p>
 * 
 * @author Guzman Tierno, Michele Loreti
 **/
public class GraphItemFactory 
implements GraphItemFactoryI<LtsState,LtsAction> {
	// TODO: rinominare in LTSFactory (spostare in src.graph?)
	
	/** Rende un nuovo nodo (stato). **/
	public LtsState newState(String name) {
		return new LtsState(name);
	}

	/** Rende una nuova azione. **/
	public LtsAction newAction(String name) {
		return new LtsAction(name);
	}
	
	/** Rende l'azione silente. **/
	public LtsAction getTauAction() {
		return LtsAction.TAU;
	}
	
	/** Rende un nuovo grafo. **/
	public GraphInterface<LtsState,LtsAction> newGraph() {
		return new Graph<LtsState,LtsAction>();
	}

	public LtsState newState(LtsState s) {
		return newState(s.getId());
	}
	
}

 

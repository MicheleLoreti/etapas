/**
 * 
 */
package org.cmg.tapas.formulae.hml;


import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public final class NotFormula<X extends APProcess<X, Y>,Y extends ActionInterface> 
						extends HmlFormula<X,Y> {

	private HmlFormula<X,Y> arg;
	
	public NotFormula( HmlFormula<X,Y> arg ) {
		this.arg = arg;
	}
	
	@Override
	public boolean satisfies(X x ) {
		return !arg.satisfies(x);
	}

	@Override
	public Proof<X,Y> getProof(X x ) {
		Proof<X, Y> p = arg.getProof(x);
		return new Proof<X, Y>(x,this, new Object[] { p } , !p.isSuccess() );
	}


	@Override
	protected HmlFormula<X, Y> doClone() {
		return new NotFormula<X, Y>(arg.clone());
	}

	@Override
	protected void doVisit(FormulaVisitor<X, Y> visitor) {
		visitor.visitNot(this);
	}

	@Override
	protected String _toString() {
		return "not "+arg;
	}

	@Override
	public String getUnicode() {
		return "\u00AC"+arg.getUnicode();
	}
	
	@Override
	public String operator() {
		return "NOT";	
	}


	@Override
	protected boolean _equals(Object obj) {
		if(obj == null)
			return false;
		
		if (obj instanceof NotFormula) {
			NotFormula<X, Y> f = (NotFormula<X, Y>) obj;
						
			return arg.equals(f.arg);			
		}
		
		return false;
	}

}

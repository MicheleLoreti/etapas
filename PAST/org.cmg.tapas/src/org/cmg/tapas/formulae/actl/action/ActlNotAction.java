/**
 * 
 */
package org.cmg.tapas.formulae.actl.action;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlActionPredicateVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.NotFilter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class ActlNotAction<S extends APProcess<S, A> , A extends ActionInterface> implements ActlActionPredicate<S,A> {

	private ActlActionPredicate<S, A> chi;

	public ActlNotAction(ActlActionPredicate<S,A> chi) {
		this.chi = chi;
	}

	/* (non-Javadoc)
	 * @see org.cmg.actl.ActlActionPredicate#acceptVisitor(org.cmg.actl.ActlActionPredicateVisitor)
	 */
	public void acceptVisitor(ActlActionPredicateVisitor<S,A> v) {
		v.accept(this);
	}

	/* (non-Javadoc)
	 * @see org.cmg.actl.ActlActionPredicate#getFilter()
	 */
	public Filter<A> getFilter() {
		return new NotFilter<A>( chi.getFilter() );
	}
	
	public String toString() {
		return "-(" + chi + ")";
	}

}

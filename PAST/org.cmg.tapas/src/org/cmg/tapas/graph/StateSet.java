/**
 * 
 */
package org.cmg.tapas.graph;

import java.util.Set;

/**
 * @author loreti
 *
 */
public class StateSet<T> implements StateInterface {
	
	private Set<T> elements;
	
	public StateSet( Set<T> elements ) {
		this.elements = elements;
	}
	
	public String getId() {
		return ""+elements.hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if ((obj == null)||!(obj instanceof StateSet)) {
			return false;
		}
		return elements.equals(((StateSet<?>) obj).elements);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return elements.hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return elements.toString();
	}


}

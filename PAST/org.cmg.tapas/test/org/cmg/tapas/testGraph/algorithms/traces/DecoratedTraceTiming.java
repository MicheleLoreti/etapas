package org.cmg.tapas.testGraph.algorithms.traces;

// graph
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.GraphAnalyzer;
import org.cmg.tapas.graph.algorithms.LoopInfo;
import org.cmg.tapas.graph.algorithms.traces.DecoratedTraceChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.RandomGraphGenerator;
import org.cmg.tapas.testGraph.algorithms.tapas1.EquivalenceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.CompletedTraceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.DSWCTraceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.MayChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.MustChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.TestingChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.TraceChecker;
import org.cmg.tapas.testGraph.algorithms.tapas1.traces.WCTraceChecker;



/**
 * DecoratedTraceTiming esegue un confronto tra i vari
 * algoritmi per il calcolo di varie equivalenze a tracce decorate.
 *
 * @author Guzman Tierno
 **/
public class DecoratedTraceTiming {
	
	public static void main(String[] args) {
		// Options

		int repetitions = 100;
		int actionCount = 2;		
		int statesNumber = 80;
		double connectionProbability = 0.16;
		boolean tauActions = true;
		int equivalence = DecoratedTraceChecker.MUST_W;
		
		// _____________________________________________________________________

		GraphInterface<LtsState,LtsAction> graph;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		GraphItemFactory graphItemFactory = new GraphItemFactory();
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>(graphItemFactory);
		
		LtsAction[] actions = new LtsAction[actionCount];
		if( tauActions )
			actions[0] = graphItemFactory.getTauAction();
		for( int i=tauActions?1:0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );
		
		boolean tapas2Res, tapas1Res;
		double totalTime=0;
		double genTime=0;
		double tapas2Time=0, tapas1Time=0;
		double start;
		double initTime = System.currentTimeMillis();
		EquivalenceChecker<LtsState,LtsAction> checker = null;

		for( int i=0; i<repetitions; ++i ) {							
			System.out.print(i + ": ");
			
			// generate random graph (according to settings)			
			start = System.currentTimeMillis();
			pointedGraph = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph = pointedGraph.getGraph();
			genTime += System.currentTimeMillis() - start;
			
			// Tapas2
			start = System.currentTimeMillis();
			DecoratedTraceChecker<LtsState,LtsAction> tapas2Checker = 
				new DecoratedTraceChecker<LtsState,LtsAction>(
					graph, equivalence
				);
			tapas2Res = tapas2Checker.checkEquivalence(
				pointedGraph.getState1(), pointedGraph.getState2()
			);
			tapas2Time += System.currentTimeMillis() - start;
			
			// Tapas1
			start = System.currentTimeMillis();			
			switch(equivalence) {
				case DecoratedTraceChecker.MUST_W:
					checker = new MustChecker<LtsState,LtsAction>();
					break;
				case DecoratedTraceChecker.TESTING_W:
					checker = new TestingChecker<LtsState,LtsAction>();
					break;
				case DecoratedTraceChecker.TRACE_W:
					checker = new MayChecker<LtsState,LtsAction>();
					break;
				case DecoratedTraceChecker.TRACE:
					checker = new TraceChecker<LtsState,LtsAction>();
					break;
				case DecoratedTraceChecker.TRACE_C:
					checker = new CompletedTraceChecker<LtsState,LtsAction>();
					break;
				case DecoratedTraceChecker.TRACE_WC:
					checker = new WCTraceChecker<LtsState,LtsAction>();
					break;
				case DecoratedTraceChecker.TRACE_WCD:
					checker = new DSWCTraceChecker<LtsState,LtsAction>();
					break;				
			}
			if( checker!= null ) {
				checker.setParameters(
					graph, graph, 
					pointedGraph.getState1(), pointedGraph.getState2(), 
					false
				); 
				tapas1Res = checker.checkEquivalence();
			} else {
				tapas1Res = tapas2Res;
			}
			tapas1Time += System.currentTimeMillis() - start;

			// check equality of results
			if( tapas2Res != tapas1Res ) {
				System.out.println("Errore");
				System.out.println("tapas1: " + tapas1Res);
				System.out.println("tapas2: " + tapas2Res);
				System.out.println("stato1: " + pointedGraph.getState1());
				System.out.println("stato2: " + pointedGraph.getState2());
				System.out.println( graph.toString() );
				GraphAnalyzer<LtsState,LtsAction> analyzer =
					new GraphAnalyzer<LtsState,LtsAction>(graph);
				LoopInfo<LtsState,LtsAction> loopInfo = analyzer.computeComponents(
					analyzer.TAU_FILTER, null
				);
				loopInfo.print();
				break;
			}

			System.out.println( tapas2Res ? "True" : "False");						
		}
		totalTime = System.currentTimeMillis() - initTime;
						
		// Print results
		System.out.println( );			
		System.out.println( "total: " + totalTime );			
		System.out.println( "generation: " + genTime );			
		System.out.println( );			
		if( checker!= null )
			System.out.println( "tapas1: " + tapas1Time );			
		System.out.println( "tapas2: " + tapas2Time );			
		
	}	
	
}




//	tapas1: false
//	tapas2: true
//	stato1: 7
//	stato2: 3
//	Stati: 10
//	3 tau 5
//	3 tau 7
//	3 1 6
//	5 1 8
//	7 1 7
//	7 1 9
//	7 tau 6
//	2 tau 5
//	0 2 4
//	0 2 6
//	4 tau 7
//	4 1 8
//	8 tau 6
//	6 2 3

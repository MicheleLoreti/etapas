package org.cmg.tapas.graph.algorithms.counterExample;

public class UnableToCreateCounterExample extends Exception {

	private static final long serialVersionUID = 5034341391512266608L;

	public UnableToCreateCounterExample(String msg){
		super(msg);
	}
}

package org.lic.validation;

import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;
import org.lic.secsi.AndExpression;
import org.lic.secsi.BoolConstant;
import org.lic.secsi.BooleanType;
import org.lic.secsi.Channel;
import org.lic.secsi.Constant;
import org.lic.secsi.Expression;
import org.lic.secsi.IntConstant;
import org.lic.secsi.IntegerType;
import org.lic.secsi.MulExpression;
import org.lic.secsi.Negation;
import org.lic.secsi.OrExpression;
import org.lic.secsi.ReferenceInExpression;
import org.lic.secsi.ReferenceableElement;
import org.lic.secsi.Relation;
import org.lic.secsi.SumExpression;
import org.lic.secsi.TypeDef;
import org.lic.secsi.TypeElement;
import org.lic.secsi.TypeExpression;
import org.lic.secsi.TypeReference;
import org.lic.secsi.Variable;
import org.lic.validation.Type;

@SuppressWarnings("all")
public class TypeInference {
  protected Type _getType(final Object o) {
    return null;
  }
  
  protected Type _getType(final OrExpression e) {
    return Type.getBoolean();
  }
  
  protected Type _getType(final AndExpression e) {
    return Type.getBoolean();
  }
  
  protected Type _getType(final Relation r) {
    return Type.getBoolean();
  }
  
  protected Type _getType(final BoolConstant c) {
    return Type.getBoolean();
  }
  
  protected Type _getType(final IntConstant i) {
    return Type.getInteger();
  }
  
  protected Type _getType(final Negation n) {
    return Type.getBoolean();
  }
  
  protected Type _getType(final TypeElement e) {
    EObject _eContainer = e.eContainer();
    return this.getType(_eContainer);
  }
  
  protected Type _getType(final Variable tv) {
    TypeExpression _type = tv.getType();
    return this.getType(_type);
  }
  
  protected Type _getType(final Constant c) {
    TypeExpression _type = c.getType();
    return this.getType(_type);
  }
  
  protected Type _getType(final TypeReference r) {
    TypeDef _reference = r.getReference();
    return Type.getCustomType(_reference);
  }
  
  protected Type _getType(final IntegerType t) {
    return Type.getInteger();
  }
  
  protected Type _getType(final BooleanType b) {
    return Type.getBoolean();
  }
  
  protected Type _getType(final SumExpression e) {
    return Type.getInteger();
  }
  
  protected Type _getType(final MulExpression e) {
    return Type.getInteger();
  }
  
  protected Type _getType(final ReferenceInExpression r) {
    ReferenceableElement _reference = r.getReference();
    return this.getType(_reference);
  }
  
  protected Type _getType(final Channel c) {
    TypeExpression _type = c.getType();
    return this.getType(_type);
  }
  
  protected Type _getType(final TypeDef td) {
    return Type.getCustomType(td);
  }
  
  protected boolean _isAnInput(final Expression e) {
    return false;
  }
  
  protected boolean _isAnInput(final ReferenceInExpression ref) {
    boolean _switchResult = false;
    ReferenceableElement _reference = ref.getReference();
    boolean _matched = false;
    if (_reference instanceof Variable) {
      _matched=true;
      _switchResult = true;
    }
    if (!_matched) {
      _switchResult = false;
    }
    return _switchResult;
  }
  
  public Type getType(final Object e) {
    if (e instanceof AndExpression) {
      return _getType((AndExpression)e);
    } else if (e instanceof BoolConstant) {
      return _getType((BoolConstant)e);
    } else if (e instanceof BooleanType) {
      return _getType((BooleanType)e);
    } else if (e instanceof Constant) {
      return _getType((Constant)e);
    } else if (e instanceof IntConstant) {
      return _getType((IntConstant)e);
    } else if (e instanceof IntegerType) {
      return _getType((IntegerType)e);
    } else if (e instanceof MulExpression) {
      return _getType((MulExpression)e);
    } else if (e instanceof Negation) {
      return _getType((Negation)e);
    } else if (e instanceof OrExpression) {
      return _getType((OrExpression)e);
    } else if (e instanceof ReferenceInExpression) {
      return _getType((ReferenceInExpression)e);
    } else if (e instanceof Relation) {
      return _getType((Relation)e);
    } else if (e instanceof SumExpression) {
      return _getType((SumExpression)e);
    } else if (e instanceof TypeDef) {
      return _getType((TypeDef)e);
    } else if (e instanceof TypeElement) {
      return _getType((TypeElement)e);
    } else if (e instanceof TypeReference) {
      return _getType((TypeReference)e);
    } else if (e instanceof Variable) {
      return _getType((Variable)e);
    } else if (e instanceof Channel) {
      return _getType((Channel)e);
    } else if (e != null) {
      return _getType(e);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(e).toString());
    }
  }
  
  public boolean isAnInput(final Expression ref) {
    if (ref instanceof ReferenceInExpression) {
      return _isAnInput((ReferenceInExpression)ref);
    } else if (ref != null) {
      return _isAnInput(ref);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(ref).toString());
    }
  }
}

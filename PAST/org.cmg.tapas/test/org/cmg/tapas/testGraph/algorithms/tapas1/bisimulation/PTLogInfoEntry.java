package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class PTLogInfoEntry {
	
	private PTStatesSet origin = null;	
	private Action cause = null;	
	private PTStatesSet destination1 = null;	
	private PTStatesSet destination2 = null;
		private boolean negation = false;
	
	public PTLogInfoEntry(
		PTStatesSet origin, 
		Action actionCause, 
		PTStatesSet destination1, 
		PTStatesSet destination2, 
		boolean negation
	) {
		this.origin = origin;
		this.cause = actionCause;
		this.destination1 = destination1;
		this.destination2 = destination2;
		this.negation = negation;
	}
	
	public PTStatesSet getOrigin() {
		return origin;
	}


	public Action getCause() {
		return cause;	
	}


	public PTStatesSet getFirstDestination() {
		return destination1;
	}
	
	public PTStatesSet getSecondDestination() {
		return destination2;
	}
	
	public boolean isNot() {
		return negation;
	}		


}
package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBLogSplittingEntry {
	
	private BBBloc bbbloc = null;	
	private BBBloc bbbloc1 = null;	
	private BBBloc bbbloc2 = null;	
	private Action cause = null;
	
	public BBLogSplittingEntry(
		BBBloc bbbloc, 
		BBBloc bbbloc1, 
		BBBloc bbbloc2, 
		Action cause
	) {
		this.bbbloc = bbbloc;
		this.bbbloc1 = bbbloc1;
		this.bbbloc2 = bbbloc2;
		this.cause = cause;
	}

	public BBBloc getBBBloc() {
		return bbbloc;
	}

	public BBBloc getBBBloc1() {
		return bbbloc1;
	}
	
	public BBBloc getBBBloc2() {
		return bbbloc2;
	}
	
	public Action getCause() {
		return cause;	
	}

	public void print() {
		print(System.out);
	}

	public void print(java.io.PrintStream console) {		
		getBBBloc().printAll(console);
		console.append(" ---> {");
		getBBBloc1().printAll(console);
		console.append(", ");
		getBBBloc2().printAll(console);
		console.append("}");
		console.append(" , cause ");
		getCause().print(console);
	}
}
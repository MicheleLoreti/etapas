/**
 * 
 */
package org.cmg.tapas.formulae.hml;


import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.cmg.tapas.formulae.lmc.ActionLoader;
import org.cmg.tapas.formulae.lmc.LogicalFormula;
import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * @author loreti
 *
 */
public abstract class HmlFormula<X extends APProcess<X, Y>,Y extends ActionInterface> 
						implements Cloneable,LogicalFormula<X,Y> {

	private String name="";
	private String description="";
	public abstract boolean satisfies( X s ); 
	public abstract Proof<X,Y> getProof( X s );
	
	public final HmlFormula<X,Y> clone() {
		return doClone();
	}

	protected abstract HmlFormula<X, Y> doClone();

	public void visit( FormulaVisitor<X,Y> visitor ) {
		doVisit(visitor);
	}

	protected abstract void doVisit(FormulaVisitor<X,Y> visitor);

	@Override
	public String toString() {
		return _toString();
	}

	protected abstract String _toString();

	@Override
	public int hashCode() {
		return (toString()).hashCode();
	}	
	
	public static <S extends APProcess<S,A>, A extends ActionInterface>  HmlFormula<S,  A> loadFormula( InputSource i , ActionLoader<S,A> loader) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(true);
		SAXParser parser = spf.newSAXParser();
		XMLFormulaHandler<S,A> xfh = new XMLFormulaHandler<S,A>(loader);
		parser.parse(i, xfh);
		return xfh.getFormula(); 
	}

	public abstract String getUnicode();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public abstract String operator();
	
	@Override
	public boolean equals(Object o){
		return _equals(o);
	}
	
	protected abstract boolean _equals(Object o);
	
}

/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula;
import org.cmg.tapas.xtext.clts.composedLts.HmlFormula;
import org.cmg.tapas.xtext.clts.composedLts.LabelPredicate;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hml Box Formula</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlBoxFormulaImpl#getAction <em>Action</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlBoxFormulaImpl#getArg <em>Arg</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HmlBoxFormulaImpl extends HmlFormulaImpl implements HmlBoxFormula
{
  /**
   * The cached value of the '{@link #getAction() <em>Action</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAction()
   * @generated
   * @ordered
   */
  protected LabelPredicate action;

  /**
   * The cached value of the '{@link #getArg() <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArg()
   * @generated
   * @ordered
   */
  protected HmlFormula arg;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected HmlBoxFormulaImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.HML_BOX_FORMULA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelPredicate getAction()
  {
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAction(LabelPredicate newAction, NotificationChain msgs)
  {
    LabelPredicate oldAction = action;
    action = newAction;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.HML_BOX_FORMULA__ACTION, oldAction, newAction);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAction(LabelPredicate newAction)
  {
    if (newAction != action)
    {
      NotificationChain msgs = null;
      if (action != null)
        msgs = ((InternalEObject)action).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.HML_BOX_FORMULA__ACTION, null, msgs);
      if (newAction != null)
        msgs = ((InternalEObject)newAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.HML_BOX_FORMULA__ACTION, null, msgs);
      msgs = basicSetAction(newAction, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.HML_BOX_FORMULA__ACTION, newAction, newAction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public HmlFormula getArg()
  {
    return arg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetArg(HmlFormula newArg, NotificationChain msgs)
  {
    HmlFormula oldArg = arg;
    arg = newArg;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.HML_BOX_FORMULA__ARG, oldArg, newArg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArg(HmlFormula newArg)
  {
    if (newArg != arg)
    {
      NotificationChain msgs = null;
      if (arg != null)
        msgs = ((InternalEObject)arg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.HML_BOX_FORMULA__ARG, null, msgs);
      if (newArg != null)
        msgs = ((InternalEObject)newArg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.HML_BOX_FORMULA__ARG, null, msgs);
      msgs = basicSetArg(newArg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.HML_BOX_FORMULA__ARG, newArg, newArg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_BOX_FORMULA__ACTION:
        return basicSetAction(null, msgs);
      case ComposedLtsPackage.HML_BOX_FORMULA__ARG:
        return basicSetArg(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_BOX_FORMULA__ACTION:
        return getAction();
      case ComposedLtsPackage.HML_BOX_FORMULA__ARG:
        return getArg();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_BOX_FORMULA__ACTION:
        setAction((LabelPredicate)newValue);
        return;
      case ComposedLtsPackage.HML_BOX_FORMULA__ARG:
        setArg((HmlFormula)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_BOX_FORMULA__ACTION:
        setAction((LabelPredicate)null);
        return;
      case ComposedLtsPackage.HML_BOX_FORMULA__ARG:
        setArg((HmlFormula)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.HML_BOX_FORMULA__ACTION:
        return action != null;
      case ComposedLtsPackage.HML_BOX_FORMULA__ARG:
        return arg != null;
    }
    return super.eIsSet(featureID);
  }

} //HmlBoxFormulaImpl

/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Recursion Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getName <em>Name</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getArg <em>Arg</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlRecursionFormula()
 * @model
 * @generated
 */
public interface HmlRecursionFormula extends HmlFormula
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlRecursionFormula_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' containment reference.
   * @see #setArg(HmlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlRecursionFormula_Arg()
   * @model containment="true"
   * @generated
   */
  HmlFormula getArg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getArg <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' containment reference.
   * @see #getArg()
   * @generated
   */
  void setArg(HmlFormula value);

} // HmlRecursionFormula

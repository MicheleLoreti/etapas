package org.lic.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.lic.services.SecsiGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalSecsiParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_STRING", "RULE_WS", "RULE_ANY_OTHER", "'const'", "'='", "';'", "'process'", "'('", "','", "')'", "'channel'", "':'", "'signal'", "'['", "'..'", "']'", "'boolean'", "'{'", "'}'", "'match'", "'otherwise'", "'with '", "'if'", "'else'", "'while'", "'System'", "'||'", "'+'", "'/'", "'\\\\'", "'Prop'", "'&&'", "'<'", "'>'", "'true'", "'false'", "'!'", "'-'", "'%'", "'*'", "'typedef'", "'|'", "'?'", "'=='", "'!='", "'<='", "'>='"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int T__19=19;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=7;
    public static final int RULE_ML_COMMENT=6;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=8;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalSecsiParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSecsiParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSecsiParser.tokenNames; }
    public String getGrammarFileName() { return "../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g"; }



     	private SecsiGrammarAccess grammarAccess;
     	
        public InternalSecsiParser(TokenStream input, SecsiGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Program";	
       	}
       	
       	@Override
       	protected SecsiGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleProgram"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:68:1: entryRuleProgram returns [EObject current=null] : iv_ruleProgram= ruleProgram EOF ;
    public final EObject entryRuleProgram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProgram = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:69:2: (iv_ruleProgram= ruleProgram EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:70:2: iv_ruleProgram= ruleProgram EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProgramRule()); 
            }
            pushFollow(FOLLOW_ruleProgram_in_entryRuleProgram75);
            iv_ruleProgram=ruleProgram();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProgram; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProgram85); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProgram"


    // $ANTLR start "ruleProgram"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:77:1: ruleProgram returns [EObject current=null] : ( ( ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) ) | ( (lv_agents_1_0= ruleAgent ) ) | ( (lv_types_2_0= ruleTypeDef ) ) | ( (lv_consts_3_0= ruleConstant ) ) )* ( (lv_system_4_0= ruleSystem ) )? ( (lv_properties_5_0= rulePropList ) )? ) ;
    public final EObject ruleProgram() throws RecognitionException {
        EObject current = null;

        EObject lv_globalchanels_0_0 = null;

        EObject lv_agents_1_0 = null;

        EObject lv_types_2_0 = null;

        EObject lv_consts_3_0 = null;

        EObject lv_system_4_0 = null;

        EObject lv_properties_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:80:28: ( ( ( ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) ) | ( (lv_agents_1_0= ruleAgent ) ) | ( (lv_types_2_0= ruleTypeDef ) ) | ( (lv_consts_3_0= ruleConstant ) ) )* ( (lv_system_4_0= ruleSystem ) )? ( (lv_properties_5_0= rulePropList ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:81:1: ( ( ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) ) | ( (lv_agents_1_0= ruleAgent ) ) | ( (lv_types_2_0= ruleTypeDef ) ) | ( (lv_consts_3_0= ruleConstant ) ) )* ( (lv_system_4_0= ruleSystem ) )? ( (lv_properties_5_0= rulePropList ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:81:1: ( ( ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) ) | ( (lv_agents_1_0= ruleAgent ) ) | ( (lv_types_2_0= ruleTypeDef ) ) | ( (lv_consts_3_0= ruleConstant ) ) )* ( (lv_system_4_0= ruleSystem ) )? ( (lv_properties_5_0= rulePropList ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:81:2: ( ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) ) | ( (lv_agents_1_0= ruleAgent ) ) | ( (lv_types_2_0= ruleTypeDef ) ) | ( (lv_consts_3_0= ruleConstant ) ) )* ( (lv_system_4_0= ruleSystem ) )? ( (lv_properties_5_0= rulePropList ) )?
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:81:2: ( ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) ) | ( (lv_agents_1_0= ruleAgent ) ) | ( (lv_types_2_0= ruleTypeDef ) ) | ( (lv_consts_3_0= ruleConstant ) ) )*
            loop1:
            do {
                int alt1=5;
                switch ( input.LA(1) ) {
                case 18:
                    {
                    alt1=1;
                    }
                    break;
                case 14:
                    {
                    alt1=2;
                    }
                    break;
                case 48:
                    {
                    alt1=3;
                    }
                    break;
                case 11:
                    {
                    alt1=4;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:81:3: ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:81:3: ( (lv_globalchanels_0_0= ruleChannelsDeclaration ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:82:1: (lv_globalchanels_0_0= ruleChannelsDeclaration )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:82:1: (lv_globalchanels_0_0= ruleChannelsDeclaration )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:83:3: lv_globalchanels_0_0= ruleChannelsDeclaration
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getGlobalchanelsChannelsDeclarationParserRuleCall_0_0_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleChannelsDeclaration_in_ruleProgram132);
            	    lv_globalchanels_0_0=ruleChannelsDeclaration();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"globalchanels",
            	              		lv_globalchanels_0_0, 
            	              		"ChannelsDeclaration");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:100:6: ( (lv_agents_1_0= ruleAgent ) )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:100:6: ( (lv_agents_1_0= ruleAgent ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:101:1: (lv_agents_1_0= ruleAgent )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:101:1: (lv_agents_1_0= ruleAgent )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:102:3: lv_agents_1_0= ruleAgent
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getAgentsAgentParserRuleCall_0_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAgent_in_ruleProgram159);
            	    lv_agents_1_0=ruleAgent();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"agents",
            	              		lv_agents_1_0, 
            	              		"Agent");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:119:6: ( (lv_types_2_0= ruleTypeDef ) )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:119:6: ( (lv_types_2_0= ruleTypeDef ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:120:1: (lv_types_2_0= ruleTypeDef )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:120:1: (lv_types_2_0= ruleTypeDef )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:121:3: lv_types_2_0= ruleTypeDef
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getTypesTypeDefParserRuleCall_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleTypeDef_in_ruleProgram186);
            	    lv_types_2_0=ruleTypeDef();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"types",
            	              		lv_types_2_0, 
            	              		"TypeDef");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:138:6: ( (lv_consts_3_0= ruleConstant ) )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:138:6: ( (lv_consts_3_0= ruleConstant ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:139:1: (lv_consts_3_0= ruleConstant )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:139:1: (lv_consts_3_0= ruleConstant )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:140:3: lv_consts_3_0= ruleConstant
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProgramAccess().getConstsConstantParserRuleCall_0_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleConstant_in_ruleProgram213);
            	    lv_consts_3_0=ruleConstant();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProgramRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"consts",
            	              		lv_consts_3_0, 
            	              		"Constant");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:156:4: ( (lv_system_4_0= ruleSystem ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==33) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:157:1: (lv_system_4_0= ruleSystem )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:157:1: (lv_system_4_0= ruleSystem )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:158:3: lv_system_4_0= ruleSystem
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getProgramAccess().getSystemSystemParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSystem_in_ruleProgram236);
                    lv_system_4_0=ruleSystem();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getProgramRule());
                      	        }
                             		set(
                             			current, 
                             			"system",
                              		lv_system_4_0, 
                              		"System");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:174:3: ( (lv_properties_5_0= rulePropList ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==38) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:175:1: (lv_properties_5_0= rulePropList )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:175:1: (lv_properties_5_0= rulePropList )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:176:3: lv_properties_5_0= rulePropList
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getProgramAccess().getPropertiesPropListParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePropList_in_ruleProgram258);
                    lv_properties_5_0=rulePropList();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getProgramRule());
                      	        }
                             		set(
                             			current, 
                             			"properties",
                              		lv_properties_5_0, 
                              		"PropList");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProgram"


    // $ANTLR start "entryRuleConstant"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:200:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:201:2: (iv_ruleConstant= ruleConstant EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:202:2: iv_ruleConstant= ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstantRule()); 
            }
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant295);
            iv_ruleConstant=ruleConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant305); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:209:1: ruleConstant returns [EObject current=null] : (otherlv_0= 'const' ( (lv_type_1_0= ruleTypeExpression ) ) ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_value_4_0= ruleExpression ) ) otherlv_5= ';' ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_type_1_0 = null;

        EObject lv_value_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:212:28: ( (otherlv_0= 'const' ( (lv_type_1_0= ruleTypeExpression ) ) ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_value_4_0= ruleExpression ) ) otherlv_5= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:213:1: (otherlv_0= 'const' ( (lv_type_1_0= ruleTypeExpression ) ) ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_value_4_0= ruleExpression ) ) otherlv_5= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:213:1: (otherlv_0= 'const' ( (lv_type_1_0= ruleTypeExpression ) ) ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_value_4_0= ruleExpression ) ) otherlv_5= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:213:3: otherlv_0= 'const' ( (lv_type_1_0= ruleTypeExpression ) ) ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '=' ( (lv_value_4_0= ruleExpression ) ) otherlv_5= ';'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleConstant342); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getConstantAccess().getConstKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:217:1: ( (lv_type_1_0= ruleTypeExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:218:1: (lv_type_1_0= ruleTypeExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:218:1: (lv_type_1_0= ruleTypeExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:219:3: lv_type_1_0= ruleTypeExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConstantAccess().getTypeTypeExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTypeExpression_in_ruleConstant363);
            lv_type_1_0=ruleTypeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConstantRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_1_0, 
                      		"TypeExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:235:2: ( (lv_name_2_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:236:1: (lv_name_2_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:236:1: (lv_name_2_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:237:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleConstant380); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getConstantAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getConstantRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleConstant397); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getConstantAccess().getEqualsSignKeyword_3());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:257:1: ( (lv_value_4_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:258:1: (lv_value_4_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:258:1: (lv_value_4_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:259:3: lv_value_4_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getConstantAccess().getValueExpressionParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleConstant418);
            lv_value_4_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getConstantRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_4_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,13,FOLLOW_13_in_ruleConstant430); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getConstantAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleAgent"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:287:1: entryRuleAgent returns [EObject current=null] : iv_ruleAgent= ruleAgent EOF ;
    public final EObject entryRuleAgent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAgent = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:288:2: (iv_ruleAgent= ruleAgent EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:289:2: iv_ruleAgent= ruleAgent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAgentRule()); 
            }
            pushFollow(FOLLOW_ruleAgent_in_entryRuleAgent466);
            iv_ruleAgent=ruleAgent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAgent; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAgent476); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAgent"


    // $ANTLR start "ruleAgent"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:296:1: ruleAgent returns [EObject current=null] : (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )* )? otherlv_6= ')' ( (lv_lcDecs_7_0= ruleChannelsDeclaration ) )* ( (lv_s_8_0= ruleStatement ) ) ) ;
    public final EObject ruleAgent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_params_3_0 = null;

        EObject lv_params_5_0 = null;

        EObject lv_lcDecs_7_0 = null;

        EObject lv_s_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:299:28: ( (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )* )? otherlv_6= ')' ( (lv_lcDecs_7_0= ruleChannelsDeclaration ) )* ( (lv_s_8_0= ruleStatement ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:300:1: (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )* )? otherlv_6= ')' ( (lv_lcDecs_7_0= ruleChannelsDeclaration ) )* ( (lv_s_8_0= ruleStatement ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:300:1: (otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )* )? otherlv_6= ')' ( (lv_lcDecs_7_0= ruleChannelsDeclaration ) )* ( (lv_s_8_0= ruleStatement ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:300:3: otherlv_0= 'process' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )* )? otherlv_6= ')' ( (lv_lcDecs_7_0= ruleChannelsDeclaration ) )* ( (lv_s_8_0= ruleStatement ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleAgent513); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getAgentAccess().getProcessKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:304:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:305:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:305:1: (lv_name_1_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:306:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAgent530); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getAgentAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getAgentRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleAgent547); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getAgentAccess().getLeftParenthesisKeyword_2());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:326:1: ( ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )* )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID||(LA5_0>=20 && LA5_0<=21)||LA5_0==24) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:326:2: ( (lv_params_3_0= ruleVariable ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )*
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:326:2: ( (lv_params_3_0= ruleVariable ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:327:1: (lv_params_3_0= ruleVariable )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:327:1: (lv_params_3_0= ruleVariable )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:328:3: lv_params_3_0= ruleVariable
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAgentAccess().getParamsVariableParserRuleCall_3_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleVariable_in_ruleAgent569);
                    lv_params_3_0=ruleVariable();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAgentRule());
                      	        }
                             		add(
                             			current, 
                             			"params",
                              		lv_params_3_0, 
                              		"Variable");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:344:2: (otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==16) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:344:4: otherlv_4= ',' ( (lv_params_5_0= ruleVariable ) )
                    	    {
                    	    otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleAgent582); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_4, grammarAccess.getAgentAccess().getCommaKeyword_3_1_0());
                    	          
                    	    }
                    	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:348:1: ( (lv_params_5_0= ruleVariable ) )
                    	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:349:1: (lv_params_5_0= ruleVariable )
                    	    {
                    	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:349:1: (lv_params_5_0= ruleVariable )
                    	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:350:3: lv_params_5_0= ruleVariable
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getAgentAccess().getParamsVariableParserRuleCall_3_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleVariable_in_ruleAgent603);
                    	    lv_params_5_0=ruleVariable();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getAgentRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"params",
                    	              		lv_params_5_0, 
                    	              		"Variable");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleAgent619); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getAgentAccess().getRightParenthesisKeyword_4());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:370:1: ( (lv_lcDecs_7_0= ruleChannelsDeclaration ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:371:1: (lv_lcDecs_7_0= ruleChannelsDeclaration )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:371:1: (lv_lcDecs_7_0= ruleChannelsDeclaration )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:372:3: lv_lcDecs_7_0= ruleChannelsDeclaration
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAgentAccess().getLcDecsChannelsDeclarationParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleChannelsDeclaration_in_ruleAgent640);
            	    lv_lcDecs_7_0=ruleChannelsDeclaration();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAgentRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"lcDecs",
            	              		lv_lcDecs_7_0, 
            	              		"ChannelsDeclaration");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:388:3: ( (lv_s_8_0= ruleStatement ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:389:1: (lv_s_8_0= ruleStatement )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:389:1: (lv_s_8_0= ruleStatement )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:390:3: lv_s_8_0= ruleStatement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAgentAccess().getSStatementParserRuleCall_6_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleStatement_in_ruleAgent662);
            lv_s_8_0=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAgentRule());
              	        }
                     		set(
                     			current, 
                     			"s",
                      		lv_s_8_0, 
                      		"Statement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAgent"


    // $ANTLR start "entryRuleChannelsDeclaration"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:414:1: entryRuleChannelsDeclaration returns [EObject current=null] : iv_ruleChannelsDeclaration= ruleChannelsDeclaration EOF ;
    public final EObject entryRuleChannelsDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChannelsDeclaration = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:415:2: (iv_ruleChannelsDeclaration= ruleChannelsDeclaration EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:416:2: iv_ruleChannelsDeclaration= ruleChannelsDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChannelsDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleChannelsDeclaration_in_entryRuleChannelsDeclaration698);
            iv_ruleChannelsDeclaration=ruleChannelsDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChannelsDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleChannelsDeclaration708); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChannelsDeclaration"


    // $ANTLR start "ruleChannelsDeclaration"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:423:1: ruleChannelsDeclaration returns [EObject current=null] : (otherlv_0= 'channel' ( (lv_declared_1_0= ruleChannel ) ) (otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) ) )* otherlv_4= ';' ) ;
    public final EObject ruleChannelsDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_declared_1_0 = null;

        EObject lv_declared_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:426:28: ( (otherlv_0= 'channel' ( (lv_declared_1_0= ruleChannel ) ) (otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) ) )* otherlv_4= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:427:1: (otherlv_0= 'channel' ( (lv_declared_1_0= ruleChannel ) ) (otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) ) )* otherlv_4= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:427:1: (otherlv_0= 'channel' ( (lv_declared_1_0= ruleChannel ) ) (otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) ) )* otherlv_4= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:427:3: otherlv_0= 'channel' ( (lv_declared_1_0= ruleChannel ) ) (otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) ) )* otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleChannelsDeclaration745); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getChannelsDeclarationAccess().getChannelKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:431:1: ( (lv_declared_1_0= ruleChannel ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:432:1: (lv_declared_1_0= ruleChannel )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:432:1: (lv_declared_1_0= ruleChannel )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:433:3: lv_declared_1_0= ruleChannel
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getChannelsDeclarationAccess().getDeclaredChannelParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleChannel_in_ruleChannelsDeclaration766);
            lv_declared_1_0=ruleChannel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getChannelsDeclarationRule());
              	        }
                     		add(
                     			current, 
                     			"declared",
                      		lv_declared_1_0, 
                      		"Channel");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:449:2: (otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:449:4: otherlv_2= ',' ( (lv_declared_3_0= ruleChannel ) )
            	    {
            	    otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleChannelsDeclaration779); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getChannelsDeclarationAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:453:1: ( (lv_declared_3_0= ruleChannel ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:454:1: (lv_declared_3_0= ruleChannel )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:454:1: (lv_declared_3_0= ruleChannel )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:455:3: lv_declared_3_0= ruleChannel
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getChannelsDeclarationAccess().getDeclaredChannelParserRuleCall_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleChannel_in_ruleChannelsDeclaration800);
            	    lv_declared_3_0=ruleChannel();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getChannelsDeclarationRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"declared",
            	              		lv_declared_3_0, 
            	              		"Channel");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleChannelsDeclaration814); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getChannelsDeclarationAccess().getSemicolonKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChannelsDeclaration"


    // $ANTLR start "entryRuleChannel"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:483:1: entryRuleChannel returns [EObject current=null] : iv_ruleChannel= ruleChannel EOF ;
    public final EObject entryRuleChannel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChannel = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:484:2: (iv_ruleChannel= ruleChannel EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:485:2: iv_ruleChannel= ruleChannel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChannelRule()); 
            }
            pushFollow(FOLLOW_ruleChannel_in_entryRuleChannel850);
            iv_ruleChannel=ruleChannel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChannel; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleChannel860); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChannel"


    // $ANTLR start "ruleChannel"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:492:1: ruleChannel returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleTypeExpression ) ) ) ;
    public final EObject ruleChannel() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        EObject lv_type_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:495:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleTypeExpression ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:496:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleTypeExpression ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:496:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleTypeExpression ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:496:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleTypeExpression ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:496:2: ( (lv_name_0_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:497:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:497:1: (lv_name_0_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:498:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleChannel902); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getChannelAccess().getNameIDTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getChannelRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,19,FOLLOW_19_in_ruleChannel919); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getChannelAccess().getColonKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:518:1: ( (lv_type_2_0= ruleTypeExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:519:1: (lv_type_2_0= ruleTypeExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:519:1: (lv_type_2_0= ruleTypeExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:520:3: lv_type_2_0= ruleTypeExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getChannelAccess().getTypeTypeExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTypeExpression_in_ruleChannel940);
            lv_type_2_0=ruleTypeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getChannelRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_2_0, 
                      		"TypeExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChannel"


    // $ANTLR start "entryRuleTypeExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:546:1: entryRuleTypeExpression returns [EObject current=null] : iv_ruleTypeExpression= ruleTypeExpression EOF ;
    public final EObject entryRuleTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:547:2: (iv_ruleTypeExpression= ruleTypeExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:548:2: iv_ruleTypeExpression= ruleTypeExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleTypeExpression_in_entryRuleTypeExpression978);
            iv_ruleTypeExpression=ruleTypeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeExpression988); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeExpression"


    // $ANTLR start "ruleTypeExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:555:1: ruleTypeExpression returns [EObject current=null] : ( (otherlv_0= 'signal' () ) | this_TypeReference_2= ruleTypeReference | this_IntegerType_3= ruleIntegerType | this_BooleanType_4= ruleBooleanType ) ;
    public final EObject ruleTypeExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_TypeReference_2 = null;

        EObject this_IntegerType_3 = null;

        EObject this_BooleanType_4 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:558:28: ( ( (otherlv_0= 'signal' () ) | this_TypeReference_2= ruleTypeReference | this_IntegerType_3= ruleIntegerType | this_BooleanType_4= ruleBooleanType ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:559:1: ( (otherlv_0= 'signal' () ) | this_TypeReference_2= ruleTypeReference | this_IntegerType_3= ruleIntegerType | this_BooleanType_4= ruleBooleanType )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:559:1: ( (otherlv_0= 'signal' () ) | this_TypeReference_2= ruleTypeReference | this_IntegerType_3= ruleIntegerType | this_BooleanType_4= ruleBooleanType )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt8=1;
                }
                break;
            case RULE_ID:
                {
                alt8=2;
                }
                break;
            case 21:
                {
                alt8=3;
                }
                break;
            case 24:
                {
                alt8=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:559:2: (otherlv_0= 'signal' () )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:559:2: (otherlv_0= 'signal' () )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:559:4: otherlv_0= 'signal' ()
                    {
                    otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleTypeExpression1026); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_0, grammarAccess.getTypeExpressionAccess().getSignalKeyword_0_0());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:563:1: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:564:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getTypeExpressionAccess().getSignalAction_0_1(),
                                  current);
                          
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:571:5: this_TypeReference_2= ruleTypeReference
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeExpressionAccess().getTypeReferenceParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTypeReference_in_ruleTypeExpression1064);
                    this_TypeReference_2=ruleTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TypeReference_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:581:5: this_IntegerType_3= ruleIntegerType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeExpressionAccess().getIntegerTypeParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerType_in_ruleTypeExpression1091);
                    this_IntegerType_3=ruleIntegerType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerType_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:591:5: this_BooleanType_4= ruleBooleanType
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTypeExpressionAccess().getBooleanTypeParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBooleanType_in_ruleTypeExpression1118);
                    this_BooleanType_4=ruleBooleanType();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanType_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeExpression"


    // $ANTLR start "entryRuleIntegerType"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:607:1: entryRuleIntegerType returns [EObject current=null] : iv_ruleIntegerType= ruleIntegerType EOF ;
    public final EObject entryRuleIntegerType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerType = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:608:2: (iv_ruleIntegerType= ruleIntegerType EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:609:2: iv_ruleIntegerType= ruleIntegerType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerTypeRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerType_in_entryRuleIntegerType1153);
            iv_ruleIntegerType=ruleIntegerType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerType1163); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerType"


    // $ANTLR start "ruleIntegerType"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:616:1: ruleIntegerType returns [EObject current=null] : (otherlv_0= '[' ( (lv_min_1_0= ruleExpression ) ) otherlv_2= '..' ( (lv_max_3_0= ruleExpression ) ) otherlv_4= ']' ) ;
    public final EObject ruleIntegerType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_min_1_0 = null;

        EObject lv_max_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:619:28: ( (otherlv_0= '[' ( (lv_min_1_0= ruleExpression ) ) otherlv_2= '..' ( (lv_max_3_0= ruleExpression ) ) otherlv_4= ']' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:620:1: (otherlv_0= '[' ( (lv_min_1_0= ruleExpression ) ) otherlv_2= '..' ( (lv_max_3_0= ruleExpression ) ) otherlv_4= ']' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:620:1: (otherlv_0= '[' ( (lv_min_1_0= ruleExpression ) ) otherlv_2= '..' ( (lv_max_3_0= ruleExpression ) ) otherlv_4= ']' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:620:3: otherlv_0= '[' ( (lv_min_1_0= ruleExpression ) ) otherlv_2= '..' ( (lv_max_3_0= ruleExpression ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleIntegerType1200); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIntegerTypeAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:624:1: ( (lv_min_1_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:625:1: (lv_min_1_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:625:1: (lv_min_1_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:626:3: lv_min_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIntegerTypeAccess().getMinExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleIntegerType1221);
            lv_min_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIntegerTypeRule());
              	        }
                     		set(
                     			current, 
                     			"min",
                      		lv_min_1_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,22,FOLLOW_22_in_ruleIntegerType1233); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getIntegerTypeAccess().getFullStopFullStopKeyword_2());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:646:1: ( (lv_max_3_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:647:1: (lv_max_3_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:647:1: (lv_max_3_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:648:3: lv_max_3_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIntegerTypeAccess().getMaxExpressionParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleIntegerType1254);
            lv_max_3_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIntegerTypeRule());
              	        }
                     		set(
                     			current, 
                     			"max",
                      		lv_max_3_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,23,FOLLOW_23_in_ruleIntegerType1266); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getIntegerTypeAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerType"


    // $ANTLR start "entryRuleBooleanType"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:676:1: entryRuleBooleanType returns [EObject current=null] : iv_ruleBooleanType= ruleBooleanType EOF ;
    public final EObject entryRuleBooleanType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanType = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:677:2: (iv_ruleBooleanType= ruleBooleanType EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:678:2: iv_ruleBooleanType= ruleBooleanType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanTypeRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanType_in_entryRuleBooleanType1302);
            iv_ruleBooleanType=ruleBooleanType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanType; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanType1312); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanType"


    // $ANTLR start "ruleBooleanType"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:685:1: ruleBooleanType returns [EObject current=null] : ( () otherlv_1= 'boolean' ) ;
    public final EObject ruleBooleanType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:688:28: ( ( () otherlv_1= 'boolean' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:689:1: ( () otherlv_1= 'boolean' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:689:1: ( () otherlv_1= 'boolean' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:689:2: () otherlv_1= 'boolean'
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:689:2: ()
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:690:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getBooleanTypeAccess().getBooleanTypeAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,24,FOLLOW_24_in_ruleBooleanType1358); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getBooleanTypeAccess().getBooleanKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanType"


    // $ANTLR start "entryRuleTypeReference"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:707:1: entryRuleTypeReference returns [EObject current=null] : iv_ruleTypeReference= ruleTypeReference EOF ;
    public final EObject entryRuleTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeReference = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:708:2: (iv_ruleTypeReference= ruleTypeReference EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:709:2: iv_ruleTypeReference= ruleTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_ruleTypeReference_in_entryRuleTypeReference1394);
            iv_ruleTypeReference=ruleTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeReference; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeReference1404); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeReference"


    // $ANTLR start "ruleTypeReference"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:716:1: ruleTypeReference returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleTypeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:719:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:720:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:720:1: ( (otherlv_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:721:1: (otherlv_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:721:1: (otherlv_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:722:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTypeReferenceRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTypeReference1448); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getTypeReferenceAccess().getReferenceTypeDefCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeReference"


    // $ANTLR start "entryRuleBlock"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:741:1: entryRuleBlock returns [EObject current=null] : iv_ruleBlock= ruleBlock EOF ;
    public final EObject entryRuleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:742:2: (iv_ruleBlock= ruleBlock EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:743:2: iv_ruleBlock= ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockRule()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_entryRuleBlock1483);
            iv_ruleBlock=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlock1493); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:750:1: ruleBlock returns [EObject current=null] : ( () ( (lv_cmds_1_0= ruleStatement ) )* ) ;
    public final EObject ruleBlock() throws RecognitionException {
        EObject current = null;

        EObject lv_cmds_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:753:28: ( ( () ( (lv_cmds_1_0= ruleStatement ) )* ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:754:1: ( () ( (lv_cmds_1_0= ruleStatement ) )* )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:754:1: ( () ( (lv_cmds_1_0= ruleStatement ) )* )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:754:2: () ( (lv_cmds_1_0= ruleStatement ) )*
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:754:2: ()
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:755:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getBlockAccess().getBlockAction_0(),
                          current);
                  
            }

            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:760:2: ( (lv_cmds_1_0= ruleStatement ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID||(LA9_0>=20 && LA9_0<=21)||(LA9_0>=24 && LA9_0<=25)||LA9_0==27||LA9_0==30||LA9_0==32) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:761:1: (lv_cmds_1_0= ruleStatement )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:761:1: (lv_cmds_1_0= ruleStatement )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:762:3: lv_cmds_1_0= ruleStatement
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getBlockAccess().getCmdsStatementParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleStatement_in_ruleBlock1548);
            	    lv_cmds_1_0=ruleStatement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cmds",
            	              		lv_cmds_1_0, 
            	              		"Statement");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRuleStatement"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:786:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:787:2: (iv_ruleStatement= ruleStatement EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:788:2: iv_ruleStatement= ruleStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementRule()); 
            }
            pushFollow(FOLLOW_ruleStatement_in_entryRuleStatement1585);
            iv_ruleStatement=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStatement1595); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:795:1: ruleStatement returns [EObject current=null] : (this_IfThenElse_0= ruleIfThenElse | this_While_1= ruleWhile | this_Action_2= ruleAction | this_Switch_Case_3= ruleSwitch_Case | this_Assignment_4= ruleAssignment | this_VarDeclaration_5= ruleVarDeclaration | (otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}' ) ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject this_IfThenElse_0 = null;

        EObject this_While_1 = null;

        EObject this_Action_2 = null;

        EObject this_Switch_Case_3 = null;

        EObject this_Assignment_4 = null;

        EObject this_VarDeclaration_5 = null;

        EObject this_Block_7 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:798:28: ( (this_IfThenElse_0= ruleIfThenElse | this_While_1= ruleWhile | this_Action_2= ruleAction | this_Switch_Case_3= ruleSwitch_Case | this_Assignment_4= ruleAssignment | this_VarDeclaration_5= ruleVarDeclaration | (otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}' ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:799:1: (this_IfThenElse_0= ruleIfThenElse | this_While_1= ruleWhile | this_Action_2= ruleAction | this_Switch_Case_3= ruleSwitch_Case | this_Assignment_4= ruleAssignment | this_VarDeclaration_5= ruleVarDeclaration | (otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}' ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:799:1: (this_IfThenElse_0= ruleIfThenElse | this_While_1= ruleWhile | this_Action_2= ruleAction | this_Switch_Case_3= ruleSwitch_Case | this_Assignment_4= ruleAssignment | this_VarDeclaration_5= ruleVarDeclaration | (otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}' ) )
            int alt10=7;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt10=1;
                }
                break;
            case 32:
                {
                alt10=2;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 44:
                case 50:
                    {
                    alt10=3;
                    }
                    break;
                case 12:
                    {
                    alt10=5;
                    }
                    break;
                case RULE_ID:
                    {
                    alt10=6;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 3, input);

                    throw nvae;
                }

                }
                break;
            case 27:
                {
                alt10=4;
                }
                break;
            case 20:
            case 21:
            case 24:
                {
                alt10=6;
                }
                break;
            case 25:
                {
                alt10=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:800:5: this_IfThenElse_0= ruleIfThenElse
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getIfThenElseParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIfThenElse_in_ruleStatement1642);
                    this_IfThenElse_0=ruleIfThenElse();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IfThenElse_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:810:5: this_While_1= ruleWhile
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getWhileParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWhile_in_ruleStatement1669);
                    this_While_1=ruleWhile();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_While_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:820:5: this_Action_2= ruleAction
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getActionParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAction_in_ruleStatement1696);
                    this_Action_2=ruleAction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Action_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:830:5: this_Switch_Case_3= ruleSwitch_Case
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getSwitch_CaseParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSwitch_Case_in_ruleStatement1723);
                    this_Switch_Case_3=ruleSwitch_Case();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Switch_Case_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:840:5: this_Assignment_4= ruleAssignment
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getAssignmentParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAssignment_in_ruleStatement1750);
                    this_Assignment_4=ruleAssignment();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Assignment_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:850:5: this_VarDeclaration_5= ruleVarDeclaration
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getVarDeclarationParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleVarDeclaration_in_ruleStatement1777);
                    this_VarDeclaration_5=ruleVarDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_VarDeclaration_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:859:6: (otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:859:6: (otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:859:8: otherlv_6= '{' this_Block_7= ruleBlock otherlv_8= '}'
                    {
                    otherlv_6=(Token)match(input,25,FOLLOW_25_in_ruleStatement1795); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getStatementAccess().getLeftCurlyBracketKeyword_6_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getStatementAccess().getBlockParserRuleCall_6_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBlock_in_ruleStatement1817);
                    this_Block_7=ruleBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Block_7; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_8=(Token)match(input,26,FOLLOW_26_in_ruleStatement1828); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getStatementAccess().getRightCurlyBracketKeyword_6_2());
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleAction"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:884:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:885:2: (iv_ruleAction= ruleAction EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:886:2: iv_ruleAction= ruleAction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActionRule()); 
            }
            pushFollow(FOLLOW_ruleAction_in_entryRuleAction1865);
            iv_ruleAction=ruleAction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAction; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAction1875); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:893:1: ruleAction returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_modality_1_0= ruleActionModality ) ) ( ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')' )? otherlv_5= ';' ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_notSignal_2_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Enumerator lv_modality_1_0 = null;

        EObject lv_e_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:896:28: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_modality_1_0= ruleActionModality ) ) ( ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')' )? otherlv_5= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:897:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_modality_1_0= ruleActionModality ) ) ( ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')' )? otherlv_5= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:897:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_modality_1_0= ruleActionModality ) ) ( ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')' )? otherlv_5= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:897:2: ( (otherlv_0= RULE_ID ) ) ( (lv_modality_1_0= ruleActionModality ) ) ( ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')' )? otherlv_5= ';'
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:897:2: ( (otherlv_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:898:1: (otherlv_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:898:1: (otherlv_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:899:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getActionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAction1920); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getActionAccess().getChannelChannelCrossReference_0_0()); 
              	
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:910:2: ( (lv_modality_1_0= ruleActionModality ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:911:1: (lv_modality_1_0= ruleActionModality )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:911:1: (lv_modality_1_0= ruleActionModality )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:912:3: lv_modality_1_0= ruleActionModality
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getActionAccess().getModalityActionModalityEnumRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleActionModality_in_ruleAction1941);
            lv_modality_1_0=ruleActionModality();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getActionRule());
              	        }
                     		set(
                     			current, 
                     			"modality",
                      		lv_modality_1_0, 
                      		"ActionModality");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:928:2: ( ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==15) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:928:3: ( (lv_notSignal_2_0= '(' ) ) ( (lv_e_3_0= ruleExpression ) ) otherlv_4= ')'
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:928:3: ( (lv_notSignal_2_0= '(' ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:929:1: (lv_notSignal_2_0= '(' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:929:1: (lv_notSignal_2_0= '(' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:930:3: lv_notSignal_2_0= '('
                    {
                    lv_notSignal_2_0=(Token)match(input,15,FOLLOW_15_in_ruleAction1960); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_notSignal_2_0, grammarAccess.getActionAccess().getNotSignalLeftParenthesisKeyword_2_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getActionRule());
                      	        }
                             		setWithLastConsumed(current, "notSignal", true, "(");
                      	    
                    }

                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:943:2: ( (lv_e_3_0= ruleExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:944:1: (lv_e_3_0= ruleExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:944:1: (lv_e_3_0= ruleExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:945:3: lv_e_3_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getActionAccess().getEExpressionParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleAction1994);
                    lv_e_3_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getActionRule());
                      	        }
                             		set(
                             			current, 
                             			"e",
                              		lv_e_3_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_4=(Token)match(input,17,FOLLOW_17_in_ruleAction2006); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getActionAccess().getRightParenthesisKeyword_2_2());
                          
                    }

                    }
                    break;

            }

            otherlv_5=(Token)match(input,13,FOLLOW_13_in_ruleAction2020); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getActionAccess().getSemicolonKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleSwitch_Case"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:977:1: entryRuleSwitch_Case returns [EObject current=null] : iv_ruleSwitch_Case= ruleSwitch_Case EOF ;
    public final EObject entryRuleSwitch_Case() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSwitch_Case = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:978:2: (iv_ruleSwitch_Case= ruleSwitch_Case EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:979:2: iv_ruleSwitch_Case= ruleSwitch_Case EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSwitch_CaseRule()); 
            }
            pushFollow(FOLLOW_ruleSwitch_Case_in_entryRuleSwitch_Case2056);
            iv_ruleSwitch_Case=ruleSwitch_Case();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSwitch_Case; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSwitch_Case2066); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSwitch_Case"


    // $ANTLR start "ruleSwitch_Case"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:986:1: ruleSwitch_Case returns [EObject current=null] : (otherlv_0= 'match' otherlv_1= '(' ( (lv_value_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCase ) )* otherlv_6= 'otherwise' otherlv_7= ':' ( (lv_default_8_0= ruleStatement ) ) otherlv_9= '}' ) ;
    public final EObject ruleSwitch_Case() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_value_2_0 = null;

        EObject lv_cases_5_0 = null;

        EObject lv_default_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:989:28: ( (otherlv_0= 'match' otherlv_1= '(' ( (lv_value_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCase ) )* otherlv_6= 'otherwise' otherlv_7= ':' ( (lv_default_8_0= ruleStatement ) ) otherlv_9= '}' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:990:1: (otherlv_0= 'match' otherlv_1= '(' ( (lv_value_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCase ) )* otherlv_6= 'otherwise' otherlv_7= ':' ( (lv_default_8_0= ruleStatement ) ) otherlv_9= '}' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:990:1: (otherlv_0= 'match' otherlv_1= '(' ( (lv_value_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCase ) )* otherlv_6= 'otherwise' otherlv_7= ':' ( (lv_default_8_0= ruleStatement ) ) otherlv_9= '}' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:990:3: otherlv_0= 'match' otherlv_1= '(' ( (lv_value_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCase ) )* otherlv_6= 'otherwise' otherlv_7= ':' ( (lv_default_8_0= ruleStatement ) ) otherlv_9= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleSwitch_Case2103); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSwitch_CaseAccess().getMatchKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleSwitch_Case2115); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getSwitch_CaseAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:998:1: ( (lv_value_2_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:999:1: (lv_value_2_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:999:1: (lv_value_2_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1000:3: lv_value_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSwitch_CaseAccess().getValueExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleSwitch_Case2136);
            lv_value_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSwitch_CaseRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleSwitch_Case2148); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSwitch_CaseAccess().getRightParenthesisKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,25,FOLLOW_25_in_ruleSwitch_Case2160); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getSwitch_CaseAccess().getLeftCurlyBracketKeyword_4());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1024:1: ( (lv_cases_5_0= ruleCase ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==29) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1025:1: (lv_cases_5_0= ruleCase )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1025:1: (lv_cases_5_0= ruleCase )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1026:3: lv_cases_5_0= ruleCase
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSwitch_CaseAccess().getCasesCaseParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleCase_in_ruleSwitch_Case2181);
            	    lv_cases_5_0=ruleCase();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSwitch_CaseRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_5_0, 
            	              		"Case");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_6=(Token)match(input,28,FOLLOW_28_in_ruleSwitch_Case2194); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getSwitch_CaseAccess().getOtherwiseKeyword_6());
                  
            }
            otherlv_7=(Token)match(input,19,FOLLOW_19_in_ruleSwitch_Case2206); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getSwitch_CaseAccess().getColonKeyword_7());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1050:1: ( (lv_default_8_0= ruleStatement ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1051:1: (lv_default_8_0= ruleStatement )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1051:1: (lv_default_8_0= ruleStatement )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1052:3: lv_default_8_0= ruleStatement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSwitch_CaseAccess().getDefaultStatementParserRuleCall_8_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleStatement_in_ruleSwitch_Case2227);
            lv_default_8_0=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSwitch_CaseRule());
              	        }
                     		set(
                     			current, 
                     			"default",
                      		lv_default_8_0, 
                      		"Statement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_9=(Token)match(input,26,FOLLOW_26_in_ruleSwitch_Case2239); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_9, grammarAccess.getSwitch_CaseAccess().getRightCurlyBracketKeyword_9());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSwitch_Case"


    // $ANTLR start "entryRuleCase"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1080:1: entryRuleCase returns [EObject current=null] : iv_ruleCase= ruleCase EOF ;
    public final EObject entryRuleCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCase = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1081:2: (iv_ruleCase= ruleCase EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1082:2: iv_ruleCase= ruleCase EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseRule()); 
            }
            pushFollow(FOLLOW_ruleCase_in_entryRuleCase2275);
            iv_ruleCase=ruleCase();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCase; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCase2285); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCase"


    // $ANTLR start "ruleCase"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1089:1: ruleCase returns [EObject current=null] : (otherlv_0= 'with ' ( (lv_value_1_0= ruleExpression ) ) otherlv_2= ':' ( (lv_cmd_3_0= ruleStatement ) ) ) ;
    public final EObject ruleCase() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_value_1_0 = null;

        EObject lv_cmd_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1092:28: ( (otherlv_0= 'with ' ( (lv_value_1_0= ruleExpression ) ) otherlv_2= ':' ( (lv_cmd_3_0= ruleStatement ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1093:1: (otherlv_0= 'with ' ( (lv_value_1_0= ruleExpression ) ) otherlv_2= ':' ( (lv_cmd_3_0= ruleStatement ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1093:1: (otherlv_0= 'with ' ( (lv_value_1_0= ruleExpression ) ) otherlv_2= ':' ( (lv_cmd_3_0= ruleStatement ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1093:3: otherlv_0= 'with ' ( (lv_value_1_0= ruleExpression ) ) otherlv_2= ':' ( (lv_cmd_3_0= ruleStatement ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_29_in_ruleCase2322); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCaseAccess().getWithKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1097:1: ( (lv_value_1_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1098:1: (lv_value_1_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1098:1: (lv_value_1_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1099:3: lv_value_1_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseAccess().getValueExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleCase2343);
            lv_value_1_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleCase2355); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getCaseAccess().getColonKeyword_2());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1119:1: ( (lv_cmd_3_0= ruleStatement ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1120:1: (lv_cmd_3_0= ruleStatement )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1120:1: (lv_cmd_3_0= ruleStatement )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1121:3: lv_cmd_3_0= ruleStatement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getCaseAccess().getCmdStatementParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleStatement_in_ruleCase2376);
            lv_cmd_3_0=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getCaseRule());
              	        }
                     		set(
                     			current, 
                     			"cmd",
                      		lv_cmd_3_0, 
                      		"Statement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCase"


    // $ANTLR start "entryRuleIfThenElse"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1145:1: entryRuleIfThenElse returns [EObject current=null] : iv_ruleIfThenElse= ruleIfThenElse EOF ;
    public final EObject entryRuleIfThenElse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfThenElse = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1146:2: (iv_ruleIfThenElse= ruleIfThenElse EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1147:2: iv_ruleIfThenElse= ruleIfThenElse EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfThenElseRule()); 
            }
            pushFollow(FOLLOW_ruleIfThenElse_in_entryRuleIfThenElse2412);
            iv_ruleIfThenElse=ruleIfThenElse();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfThenElse; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfThenElse2422); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfThenElse"


    // $ANTLR start "ruleIfThenElse"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1154:1: ruleIfThenElse returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_thenBranch_4_0= ruleStatement ) ) ( ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) ) )? ) ;
    public final EObject ruleIfThenElse() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_withElse_5_0=null;
        EObject lv_expression_2_0 = null;

        EObject lv_thenBranch_4_0 = null;

        EObject lv_elseBranch_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1157:28: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_thenBranch_4_0= ruleStatement ) ) ( ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1158:1: (otherlv_0= 'if' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_thenBranch_4_0= ruleStatement ) ) ( ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1158:1: (otherlv_0= 'if' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_thenBranch_4_0= ruleStatement ) ) ( ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1158:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_expression_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_thenBranch_4_0= ruleStatement ) ) ( ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) ) )?
            {
            otherlv_0=(Token)match(input,30,FOLLOW_30_in_ruleIfThenElse2459); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIfThenElseAccess().getIfKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleIfThenElse2471); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getIfThenElseAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1166:1: ( (lv_expression_2_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1167:1: (lv_expression_2_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1167:1: (lv_expression_2_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1168:3: lv_expression_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIfThenElseAccess().getExpressionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleIfThenElse2492);
            lv_expression_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIfThenElseRule());
              	        }
                     		set(
                     			current, 
                     			"expression",
                      		lv_expression_2_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleIfThenElse2504); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIfThenElseAccess().getRightParenthesisKeyword_3());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1188:1: ( (lv_thenBranch_4_0= ruleStatement ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1189:1: (lv_thenBranch_4_0= ruleStatement )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1189:1: (lv_thenBranch_4_0= ruleStatement )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1190:3: lv_thenBranch_4_0= ruleStatement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIfThenElseAccess().getThenBranchStatementParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleStatement_in_ruleIfThenElse2525);
            lv_thenBranch_4_0=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIfThenElseRule());
              	        }
                     		set(
                     			current, 
                     			"thenBranch",
                      		lv_thenBranch_4_0, 
                      		"Statement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1206:2: ( ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==31) ) {
                int LA13_1 = input.LA(2);

                if ( (synpred1_InternalSecsi()) ) {
                    alt13=1;
                }
            }
            switch (alt13) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1206:3: ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) ) ( (lv_elseBranch_6_0= ruleStatement ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1206:3: ( ( ( 'else' ) )=> (lv_withElse_5_0= 'else' ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1206:4: ( ( 'else' ) )=> (lv_withElse_5_0= 'else' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1213:1: (lv_withElse_5_0= 'else' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1214:3: lv_withElse_5_0= 'else'
                    {
                    lv_withElse_5_0=(Token)match(input,31,FOLLOW_31_in_ruleIfThenElse2559); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_withElse_5_0, grammarAccess.getIfThenElseAccess().getWithElseElseKeyword_5_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getIfThenElseRule());
                      	        }
                             		setWithLastConsumed(current, "withElse", true, "else");
                      	    
                    }

                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1227:2: ( (lv_elseBranch_6_0= ruleStatement ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1228:1: (lv_elseBranch_6_0= ruleStatement )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1228:1: (lv_elseBranch_6_0= ruleStatement )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1229:3: lv_elseBranch_6_0= ruleStatement
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getIfThenElseAccess().getElseBranchStatementParserRuleCall_5_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleStatement_in_ruleIfThenElse2593);
                    lv_elseBranch_6_0=ruleStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getIfThenElseRule());
                      	        }
                             		set(
                             			current, 
                             			"elseBranch",
                              		lv_elseBranch_6_0, 
                              		"Statement");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfThenElse"


    // $ANTLR start "entryRuleWhile"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1253:1: entryRuleWhile returns [EObject current=null] : iv_ruleWhile= ruleWhile EOF ;
    public final EObject entryRuleWhile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhile = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1254:2: (iv_ruleWhile= ruleWhile EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1255:2: iv_ruleWhile= ruleWhile EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhileRule()); 
            }
            pushFollow(FOLLOW_ruleWhile_in_entryRuleWhile2631);
            iv_ruleWhile=ruleWhile();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhile; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhile2641); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhile"


    // $ANTLR start "ruleWhile"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1262:1: ruleWhile returns [EObject current=null] : (otherlv_0= 'while' otherlv_1= '(' ( (lv_expresion_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_body_4_0= ruleStatement ) ) ) ;
    public final EObject ruleWhile() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_expresion_2_0 = null;

        EObject lv_body_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1265:28: ( (otherlv_0= 'while' otherlv_1= '(' ( (lv_expresion_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_body_4_0= ruleStatement ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1266:1: (otherlv_0= 'while' otherlv_1= '(' ( (lv_expresion_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_body_4_0= ruleStatement ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1266:1: (otherlv_0= 'while' otherlv_1= '(' ( (lv_expresion_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_body_4_0= ruleStatement ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1266:3: otherlv_0= 'while' otherlv_1= '(' ( (lv_expresion_2_0= ruleExpression ) ) otherlv_3= ')' ( (lv_body_4_0= ruleStatement ) )
            {
            otherlv_0=(Token)match(input,32,FOLLOW_32_in_ruleWhile2678); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWhileAccess().getWhileKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleWhile2690); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getWhileAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1274:1: ( (lv_expresion_2_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1275:1: (lv_expresion_2_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1275:1: (lv_expresion_2_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1276:3: lv_expresion_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWhileAccess().getExpresionExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleWhile2711);
            lv_expresion_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWhileRule());
              	        }
                     		set(
                     			current, 
                     			"expresion",
                      		lv_expresion_2_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleWhile2723); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getWhileAccess().getRightParenthesisKeyword_3());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1296:1: ( (lv_body_4_0= ruleStatement ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1297:1: (lv_body_4_0= ruleStatement )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1297:1: (lv_body_4_0= ruleStatement )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1298:3: lv_body_4_0= ruleStatement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWhileAccess().getBodyStatementParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleStatement_in_ruleWhile2744);
            lv_body_4_0=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWhileRule());
              	        }
                     		set(
                     			current, 
                     			"body",
                      		lv_body_4_0, 
                      		"Statement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhile"


    // $ANTLR start "entryRuleSystem"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1322:1: entryRuleSystem returns [EObject current=null] : iv_ruleSystem= ruleSystem EOF ;
    public final EObject entryRuleSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSystem = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1323:2: (iv_ruleSystem= ruleSystem EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1324:2: iv_ruleSystem= ruleSystem EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSystemRule()); 
            }
            pushFollow(FOLLOW_ruleSystem_in_entryRuleSystem2780);
            iv_ruleSystem=ruleSystem();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSystem; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSystem2790); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSystem"


    // $ANTLR start "ruleSystem"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1331:1: ruleSystem returns [EObject current=null] : (otherlv_0= 'System' otherlv_1= '{' ( (lv_body_2_0= rulePar ) ) otherlv_3= '}' ) ;
    public final EObject ruleSystem() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_body_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1334:28: ( (otherlv_0= 'System' otherlv_1= '{' ( (lv_body_2_0= rulePar ) ) otherlv_3= '}' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1335:1: (otherlv_0= 'System' otherlv_1= '{' ( (lv_body_2_0= rulePar ) ) otherlv_3= '}' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1335:1: (otherlv_0= 'System' otherlv_1= '{' ( (lv_body_2_0= rulePar ) ) otherlv_3= '}' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1335:3: otherlv_0= 'System' otherlv_1= '{' ( (lv_body_2_0= rulePar ) ) otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_33_in_ruleSystem2827); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSystemAccess().getSystemKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleSystem2839); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getSystemAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1343:1: ( (lv_body_2_0= rulePar ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1344:1: (lv_body_2_0= rulePar )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1344:1: (lv_body_2_0= rulePar )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1345:3: lv_body_2_0= rulePar
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSystemAccess().getBodyParParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_rulePar_in_ruleSystem2860);
            lv_body_2_0=rulePar();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSystemRule());
              	        }
                     		set(
                     			current, 
                     			"body",
                      		lv_body_2_0, 
                      		"Par");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,26,FOLLOW_26_in_ruleSystem2872); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSystemAccess().getRightCurlyBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSystem"


    // $ANTLR start "entryRulePar"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1373:1: entryRulePar returns [EObject current=null] : iv_rulePar= rulePar EOF ;
    public final EObject entryRulePar() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePar = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1374:2: (iv_rulePar= rulePar EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1375:2: iv_rulePar= rulePar EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParRule()); 
            }
            pushFollow(FOLLOW_rulePar_in_entryRulePar2908);
            iv_rulePar=rulePar();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePar; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePar2918); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePar"


    // $ANTLR start "rulePar"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1382:1: rulePar returns [EObject current=null] : ( ( (lv_action_0_0= ruleSum ) ) ( () otherlv_2= '||' ( (lv_right_3_0= rulePar ) ) )? ) ;
    public final EObject rulePar() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_action_0_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1385:28: ( ( ( (lv_action_0_0= ruleSum ) ) ( () otherlv_2= '||' ( (lv_right_3_0= rulePar ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1386:1: ( ( (lv_action_0_0= ruleSum ) ) ( () otherlv_2= '||' ( (lv_right_3_0= rulePar ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1386:1: ( ( (lv_action_0_0= ruleSum ) ) ( () otherlv_2= '||' ( (lv_right_3_0= rulePar ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1386:2: ( (lv_action_0_0= ruleSum ) ) ( () otherlv_2= '||' ( (lv_right_3_0= rulePar ) ) )?
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1386:2: ( (lv_action_0_0= ruleSum ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1387:1: (lv_action_0_0= ruleSum )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1387:1: (lv_action_0_0= ruleSum )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1388:3: lv_action_0_0= ruleSum
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getParAccess().getActionSumParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleSum_in_rulePar2964);
            lv_action_0_0=ruleSum();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getParRule());
              	        }
                     		set(
                     			current, 
                     			"action",
                      		lv_action_0_0, 
                      		"Sum");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1404:2: ( () otherlv_2= '||' ( (lv_right_3_0= rulePar ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==34) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1404:3: () otherlv_2= '||' ( (lv_right_3_0= rulePar ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1404:3: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1405:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getParAccess().getParLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    otherlv_2=(Token)match(input,34,FOLLOW_34_in_rulePar2986); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getParAccess().getVerticalLineVerticalLineKeyword_1_1());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1414:1: ( (lv_right_3_0= rulePar ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1415:1: (lv_right_3_0= rulePar )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1415:1: (lv_right_3_0= rulePar )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1416:3: lv_right_3_0= rulePar
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getParAccess().getRightParParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePar_in_rulePar3007);
                    lv_right_3_0=rulePar();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getParRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"Par");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePar"


    // $ANTLR start "entryRuleSum"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1440:1: entryRuleSum returns [EObject current=null] : iv_ruleSum= ruleSum EOF ;
    public final EObject entryRuleSum() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSum = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1441:2: (iv_ruleSum= ruleSum EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1442:2: iv_ruleSum= ruleSum EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSumRule()); 
            }
            pushFollow(FOLLOW_ruleSum_in_entryRuleSum3045);
            iv_ruleSum=ruleSum();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSum; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSum3055); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSum"


    // $ANTLR start "ruleSum"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1449:1: ruleSum returns [EObject current=null] : ( ( (lv_action_0_0= ruleProc ) ) ( () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) ) )? ) ;
    public final EObject ruleSum() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_action_0_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1452:28: ( ( ( (lv_action_0_0= ruleProc ) ) ( () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1453:1: ( ( (lv_action_0_0= ruleProc ) ) ( () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1453:1: ( ( (lv_action_0_0= ruleProc ) ) ( () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1453:2: ( (lv_action_0_0= ruleProc ) ) ( () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) ) )?
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1453:2: ( (lv_action_0_0= ruleProc ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1454:1: (lv_action_0_0= ruleProc )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1454:1: (lv_action_0_0= ruleProc )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1455:3: lv_action_0_0= ruleProc
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSumAccess().getActionProcParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleProc_in_ruleSum3101);
            lv_action_0_0=ruleProc();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSumRule());
              	        }
                     		set(
                     			current, 
                     			"action",
                      		lv_action_0_0, 
                      		"Proc");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1471:2: ( () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==35) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1471:3: () otherlv_2= '+' ( (lv_right_3_0= ruleSum ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1471:3: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1472:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getSumAccess().getSumLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    otherlv_2=(Token)match(input,35,FOLLOW_35_in_ruleSum3123); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getSumAccess().getPlusSignKeyword_1_1());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1481:1: ( (lv_right_3_0= ruleSum ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1482:1: (lv_right_3_0= ruleSum )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1482:1: (lv_right_3_0= ruleSum )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1483:3: lv_right_3_0= ruleSum
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSumAccess().getRightSumParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSum_in_ruleSum3144);
                    lv_right_3_0=ruleSum();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSumRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"Sum");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSum"


    // $ANTLR start "entryRuleProc"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1507:1: entryRuleProc returns [EObject current=null] : iv_ruleProc= ruleProc EOF ;
    public final EObject entryRuleProc() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProc = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1508:2: (iv_ruleProc= ruleProc EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1509:2: iv_ruleProc= ruleProc EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProcRule()); 
            }
            pushFollow(FOLLOW_ruleProc_in_entryRuleProc3182);
            iv_ruleProc=ruleProc();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProc; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProc3192); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProc"


    // $ANTLR start "ruleProc"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1516:1: ruleProc returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) | ( ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) ) ) ) ;
    public final EObject ruleProc() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_re_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1519:28: ( ( ( (otherlv_0= RULE_ID ) ) | ( ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1520:1: ( ( (otherlv_0= RULE_ID ) ) | ( ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1520:1: ( ( (otherlv_0= RULE_ID ) ) | ( ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                int LA16_1 = input.LA(2);

                if ( (LA16_1==21) ) {
                    alt16=2;
                }
                else if ( (LA16_1==EOF||LA16_1==26||(LA16_1>=34 && LA16_1<=35)) ) {
                    alt16=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 16, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1520:2: ( (otherlv_0= RULE_ID ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1520:2: ( (otherlv_0= RULE_ID ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1521:1: (otherlv_0= RULE_ID )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1521:1: (otherlv_0= RULE_ID )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1522:3: otherlv_0= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getProcRule());
                      	        }
                              
                    }
                    otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProc3237); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_0, grammarAccess.getProcAccess().getNameAgentCrossReference_0_0()); 
                      	
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1534:6: ( ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1534:6: ( ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1534:7: ( (otherlv_1= RULE_ID ) ) ( (lv_re_2_0= ruleRen ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1534:7: ( (otherlv_1= RULE_ID ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1535:1: (otherlv_1= RULE_ID )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1535:1: (otherlv_1= RULE_ID )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1536:3: otherlv_1= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getProcRule());
                      	        }
                              
                    }
                    otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProc3264); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_1, grammarAccess.getProcAccess().getName1AgentCrossReference_1_0_0()); 
                      	
                    }

                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1547:2: ( (lv_re_2_0= ruleRen ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1548:1: (lv_re_2_0= ruleRen )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1548:1: (lv_re_2_0= ruleRen )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1549:3: lv_re_2_0= ruleRen
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getProcAccess().getReRenParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleRen_in_ruleProc3285);
                    lv_re_2_0=ruleRen();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getProcRule());
                      	        }
                             		set(
                             			current, 
                             			"re",
                              		lv_re_2_0, 
                              		"Ren");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProc"


    // $ANTLR start "entryRuleRen"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1573:1: entryRuleRen returns [EObject current=null] : iv_ruleRen= ruleRen EOF ;
    public final EObject entryRuleRen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRen = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1574:2: (iv_ruleRen= ruleRen EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1575:2: iv_ruleRen= ruleRen EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRenRule()); 
            }
            pushFollow(FOLLOW_ruleRen_in_entryRuleRen3322);
            iv_ruleRen=ruleRen();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRen; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRen3332); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRen"


    // $ANTLR start "ruleRen"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1582:1: ruleRen returns [EObject current=null] : (otherlv_0= '[' ( (otherlv_1= RULE_ID ) ) otherlv_2= '/' ( (lv_rightlist_3_0= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) ) )* otherlv_8= ']' ( (lv_r_9_0= ruleRes ) )? ) ;
    public final EObject ruleRen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_rightlist_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token lv_rightlist_7_0=null;
        Token otherlv_8=null;
        EObject lv_r_9_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1585:28: ( (otherlv_0= '[' ( (otherlv_1= RULE_ID ) ) otherlv_2= '/' ( (lv_rightlist_3_0= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) ) )* otherlv_8= ']' ( (lv_r_9_0= ruleRes ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1586:1: (otherlv_0= '[' ( (otherlv_1= RULE_ID ) ) otherlv_2= '/' ( (lv_rightlist_3_0= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) ) )* otherlv_8= ']' ( (lv_r_9_0= ruleRes ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1586:1: (otherlv_0= '[' ( (otherlv_1= RULE_ID ) ) otherlv_2= '/' ( (lv_rightlist_3_0= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) ) )* otherlv_8= ']' ( (lv_r_9_0= ruleRes ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1586:3: otherlv_0= '[' ( (otherlv_1= RULE_ID ) ) otherlv_2= '/' ( (lv_rightlist_3_0= RULE_ID ) ) (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) ) )* otherlv_8= ']' ( (lv_r_9_0= ruleRes ) )?
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleRen3369); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getRenAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1590:1: ( (otherlv_1= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1591:1: (otherlv_1= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1591:1: (otherlv_1= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1592:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRenRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRen3389); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getRenAccess().getLeflistsChannelCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,36,FOLLOW_36_in_ruleRen3401); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getRenAccess().getSolidusKeyword_2());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1607:1: ( (lv_rightlist_3_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1608:1: (lv_rightlist_3_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1608:1: (lv_rightlist_3_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1609:3: lv_rightlist_3_0= RULE_ID
            {
            lv_rightlist_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRen3418); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_rightlist_3_0, grammarAccess.getRenAccess().getRightlistIDTerminalRuleCall_3_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRenRule());
              	        }
                     		addWithLastConsumed(
                     			current, 
                     			"rightlist",
                      		lv_rightlist_3_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1625:2: (otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==16) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1625:4: otherlv_4= ',' ( (otherlv_5= RULE_ID ) ) otherlv_6= '/' ( (lv_rightlist_7_0= RULE_ID ) )
            	    {
            	    otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleRen3436); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_4, grammarAccess.getRenAccess().getCommaKeyword_4_0());
            	          
            	    }
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1629:1: ( (otherlv_5= RULE_ID ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1630:1: (otherlv_5= RULE_ID )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1630:1: (otherlv_5= RULE_ID )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1631:3: otherlv_5= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getRenRule());
            	      	        }
            	              
            	    }
            	    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRen3456); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_5, grammarAccess.getRenAccess().getLeflistsChannelCrossReference_4_1_0()); 
            	      	
            	    }

            	    }


            	    }

            	    otherlv_6=(Token)match(input,36,FOLLOW_36_in_ruleRen3468); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_6, grammarAccess.getRenAccess().getSolidusKeyword_4_2());
            	          
            	    }
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1646:1: ( (lv_rightlist_7_0= RULE_ID ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1647:1: (lv_rightlist_7_0= RULE_ID )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1647:1: (lv_rightlist_7_0= RULE_ID )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1648:3: lv_rightlist_7_0= RULE_ID
            	    {
            	    lv_rightlist_7_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRen3485); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			newLeafNode(lv_rightlist_7_0, grammarAccess.getRenAccess().getRightlistIDTerminalRuleCall_4_3_0()); 
            	      		
            	    }
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElement(grammarAccess.getRenRule());
            	      	        }
            	             		addWithLastConsumed(
            	             			current, 
            	             			"rightlist",
            	              		lv_rightlist_7_0, 
            	              		"ID");
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_8=(Token)match(input,23,FOLLOW_23_in_ruleRen3504); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getRenAccess().getRightSquareBracketKeyword_5());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1668:1: ( (lv_r_9_0= ruleRes ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==37) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1669:1: (lv_r_9_0= ruleRes )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1669:1: (lv_r_9_0= ruleRes )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1670:3: lv_r_9_0= ruleRes
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRenAccess().getRResParserRuleCall_6_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleRes_in_ruleRen3525);
                    lv_r_9_0=ruleRes();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRenRule());
                      	        }
                             		set(
                             			current, 
                             			"r",
                              		lv_r_9_0, 
                              		"Res");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRen"


    // $ANTLR start "entryRuleRes"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1694:1: entryRuleRes returns [EObject current=null] : iv_ruleRes= ruleRes EOF ;
    public final EObject entryRuleRes() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRes = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1695:2: (iv_ruleRes= ruleRes EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1696:2: iv_ruleRes= ruleRes EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getResRule()); 
            }
            pushFollow(FOLLOW_ruleRes_in_entryRuleRes3562);
            iv_ruleRes=ruleRes();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRes; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRes3572); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRes"


    // $ANTLR start "ruleRes"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1703:1: ruleRes returns [EObject current=null] : (otherlv_0= '\\\\' otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' ) ;
    public final EObject ruleRes() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1706:28: ( (otherlv_0= '\\\\' otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1707:1: (otherlv_0= '\\\\' otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1707:1: (otherlv_0= '\\\\' otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1707:3: otherlv_0= '\\\\' otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_37_in_ruleRes3609); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getResAccess().getReverseSolidusKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleRes3621); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getResAccess().getLeftCurlyBracketKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1715:1: ( (otherlv_2= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1716:1: (otherlv_2= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1716:1: (otherlv_2= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1717:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getResRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRes3641); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getResAccess().getLeftlistsChannelCrossReference_2_0()); 
              	
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1728:2: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==16) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1728:4: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
            	    {
            	    otherlv_3=(Token)match(input,16,FOLLOW_16_in_ruleRes3654); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_3, grammarAccess.getResAccess().getCommaKeyword_3_0());
            	          
            	    }
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1732:1: ( (otherlv_4= RULE_ID ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1733:1: (otherlv_4= RULE_ID )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1733:1: (otherlv_4= RULE_ID )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1734:3: otherlv_4= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getResRule());
            	      	        }
            	              
            	    }
            	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRes3674); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_4, grammarAccess.getResAccess().getLeflistsChannelCrossReference_3_1_0()); 
            	      	
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            otherlv_5=(Token)match(input,26,FOLLOW_26_in_ruleRes3688); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getResAccess().getRightCurlyBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRes"


    // $ANTLR start "entryRulePropList"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1757:1: entryRulePropList returns [EObject current=null] : iv_rulePropList= rulePropList EOF ;
    public final EObject entryRulePropList() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropList = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1758:2: (iv_rulePropList= rulePropList EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1759:2: iv_rulePropList= rulePropList EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPropListRule()); 
            }
            pushFollow(FOLLOW_rulePropList_in_entryRulePropList3724);
            iv_rulePropList=rulePropList();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePropList; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePropList3734); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropList"


    // $ANTLR start "rulePropList"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1766:1: rulePropList returns [EObject current=null] : ( () otherlv_1= 'Prop' otherlv_2= '{' ( (lv_element_3_0= ruleProp ) )* otherlv_4= '}' ) ;
    public final EObject rulePropList() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_element_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1769:28: ( ( () otherlv_1= 'Prop' otherlv_2= '{' ( (lv_element_3_0= ruleProp ) )* otherlv_4= '}' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1770:1: ( () otherlv_1= 'Prop' otherlv_2= '{' ( (lv_element_3_0= ruleProp ) )* otherlv_4= '}' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1770:1: ( () otherlv_1= 'Prop' otherlv_2= '{' ( (lv_element_3_0= ruleProp ) )* otherlv_4= '}' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1770:2: () otherlv_1= 'Prop' otherlv_2= '{' ( (lv_element_3_0= ruleProp ) )* otherlv_4= '}'
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1770:2: ()
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1771:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getPropListAccess().getPropListAction_0(),
                          current);
                  
            }

            }

            otherlv_1=(Token)match(input,38,FOLLOW_38_in_rulePropList3780); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getPropListAccess().getPropKeyword_1());
                  
            }
            otherlv_2=(Token)match(input,25,FOLLOW_25_in_rulePropList3792); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getPropListAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1784:1: ( (lv_element_3_0= ruleProp ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==15||LA20_0==21||LA20_0==40||(LA20_0>=42 && LA20_0<=44)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1785:1: (lv_element_3_0= ruleProp )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1785:1: (lv_element_3_0= ruleProp )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1786:3: lv_element_3_0= ruleProp
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getPropListAccess().getElementPropParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleProp_in_rulePropList3813);
            	    lv_element_3_0=ruleProp();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getPropListRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"element",
            	              		lv_element_3_0, 
            	              		"Prop");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

            otherlv_4=(Token)match(input,26,FOLLOW_26_in_rulePropList3826); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getPropListAccess().getRightCurlyBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropList"


    // $ANTLR start "entryRuleProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1814:1: entryRuleProp returns [EObject current=null] : iv_ruleProp= ruleProp EOF ;
    public final EObject entryRuleProp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProp = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1815:2: (iv_ruleProp= ruleProp EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1816:2: iv_ruleProp= ruleProp EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPropRule()); 
            }
            pushFollow(FOLLOW_ruleProp_in_entryRuleProp3862);
            iv_ruleProp=ruleProp();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProp; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProp3872); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProp"


    // $ANTLR start "ruleProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1823:1: ruleProp returns [EObject current=null] : (this_PropBody_0= rulePropBody otherlv_1= ';' ) ;
    public final EObject ruleProp() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_PropBody_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1826:28: ( (this_PropBody_0= rulePropBody otherlv_1= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1827:1: (this_PropBody_0= rulePropBody otherlv_1= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1827:1: (this_PropBody_0= rulePropBody otherlv_1= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1828:5: this_PropBody_0= rulePropBody otherlv_1= ';'
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getPropAccess().getPropBodyParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_rulePropBody_in_ruleProp3919);
            this_PropBody_0=rulePropBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_PropBody_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            otherlv_1=(Token)match(input,13,FOLLOW_13_in_ruleProp3930); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getPropAccess().getSemicolonKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProp"


    // $ANTLR start "entryRulePropBody"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1848:1: entryRulePropBody returns [EObject current=null] : iv_rulePropBody= rulePropBody EOF ;
    public final EObject entryRulePropBody() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropBody = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1849:2: (iv_rulePropBody= rulePropBody EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1850:2: iv_rulePropBody= rulePropBody EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPropBodyRule()); 
            }
            pushFollow(FOLLOW_rulePropBody_in_entryRulePropBody3966);
            iv_rulePropBody=rulePropBody();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePropBody; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePropBody3976); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropBody"


    // $ANTLR start "rulePropBody"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1857:1: rulePropBody returns [EObject current=null] : this_OrProp_0= ruleOrProp ;
    public final EObject rulePropBody() throws RecognitionException {
        EObject current = null;

        EObject this_OrProp_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1860:28: (this_OrProp_0= ruleOrProp )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1862:5: this_OrProp_0= ruleOrProp
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getPropBodyAccess().getOrPropParserRuleCall()); 
                  
            }
            pushFollow(FOLLOW_ruleOrProp_in_rulePropBody4022);
            this_OrProp_0=ruleOrProp();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_OrProp_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropBody"


    // $ANTLR start "entryRuleOrProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1878:1: entryRuleOrProp returns [EObject current=null] : iv_ruleOrProp= ruleOrProp EOF ;
    public final EObject entryRuleOrProp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrProp = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1879:2: (iv_ruleOrProp= ruleOrProp EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1880:2: iv_ruleOrProp= ruleOrProp EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrPropRule()); 
            }
            pushFollow(FOLLOW_ruleOrProp_in_entryRuleOrProp4056);
            iv_ruleOrProp=ruleOrProp();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrProp; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrProp4066); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrProp"


    // $ANTLR start "ruleOrProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1887:1: ruleOrProp returns [EObject current=null] : (this_ActProp_0= ruleActProp ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) ) )? ) ;
    public final EObject ruleOrProp() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_ActProp_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1890:28: ( (this_ActProp_0= ruleActProp ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1891:1: (this_ActProp_0= ruleActProp ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1891:1: (this_ActProp_0= ruleActProp ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1892:5: this_ActProp_0= ruleActProp ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getOrPropAccess().getActPropParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleActProp_in_ruleOrProp4113);
            this_ActProp_0=ruleActProp();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_ActProp_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1900:1: ( () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==34||LA22_0==39) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1900:2: () ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) ) ( (lv_right_3_0= ruleOrProp ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1900:2: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1901:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getOrPropAccess().getOrPropLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1906:2: ( ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1907:1: ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1907:1: ( (lv_op_2_1= '||' | lv_op_2_2= '&&' ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1908:1: (lv_op_2_1= '||' | lv_op_2_2= '&&' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1908:1: (lv_op_2_1= '||' | lv_op_2_2= '&&' )
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0==34) ) {
                        alt21=1;
                    }
                    else if ( (LA21_0==39) ) {
                        alt21=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 21, 0, input);

                        throw nvae;
                    }
                    switch (alt21) {
                        case 1 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1909:3: lv_op_2_1= '||'
                            {
                            lv_op_2_1=(Token)match(input,34,FOLLOW_34_in_ruleOrProp4142); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_1, grammarAccess.getOrPropAccess().getOpVerticalLineVerticalLineKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getOrPropRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1921:8: lv_op_2_2= '&&'
                            {
                            lv_op_2_2=(Token)match(input,39,FOLLOW_39_in_ruleOrProp4171); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_2, grammarAccess.getOrPropAccess().getOpAmpersandAmpersandKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getOrPropRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1936:2: ( (lv_right_3_0= ruleOrProp ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1937:1: (lv_right_3_0= ruleOrProp )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1937:1: (lv_right_3_0= ruleOrProp )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1938:3: lv_right_3_0= ruleOrProp
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getOrPropAccess().getRightOrPropParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOrProp_in_ruleOrProp4208);
                    lv_right_3_0=ruleOrProp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getOrPropRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"OrProp");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrProp"


    // $ANTLR start "entryRuleActProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1962:1: entryRuleActProp returns [EObject current=null] : iv_ruleActProp= ruleActProp EOF ;
    public final EObject entryRuleActProp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActProp = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1963:2: (iv_ruleActProp= ruleActProp EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1964:2: iv_ruleActProp= ruleActProp EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActPropRule()); 
            }
            pushFollow(FOLLOW_ruleActProp_in_entryRuleActProp4246);
            iv_ruleActProp=ruleActProp();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleActProp; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleActProp4256); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActProp"


    // $ANTLR start "ruleActProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1971:1: ruleActProp returns [EObject current=null] : ( (otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) ) ) | (otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) ) ) | ( (lv_arg_8_0= ruleTerm ) ) ) ;
    public final EObject ruleActProp() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_arg_3_0 = null;

        EObject lv_arg_7_0 = null;

        EObject lv_arg_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1974:28: ( ( (otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) ) ) | (otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) ) ) | ( (lv_arg_8_0= ruleTerm ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1975:1: ( (otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) ) ) | (otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) ) ) | ( (lv_arg_8_0= ruleTerm ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1975:1: ( (otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) ) ) | (otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) ) ) | ( (lv_arg_8_0= ruleTerm ) ) )
            int alt23=3;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt23=1;
                }
                break;
            case 21:
                {
                alt23=2;
                }
                break;
            case 15:
            case 42:
            case 43:
            case 44:
                {
                alt23=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1975:2: (otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1975:2: (otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1975:4: otherlv_0= '<' ( (otherlv_1= RULE_ID ) ) otherlv_2= '>' ( (lv_arg_3_0= ruleTerm ) )
                    {
                    otherlv_0=(Token)match(input,40,FOLLOW_40_in_ruleActProp4294); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_0, grammarAccess.getActPropAccess().getLessThanSignKeyword_0_0());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1979:1: ( (otherlv_1= RULE_ID ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1980:1: (otherlv_1= RULE_ID )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1980:1: (otherlv_1= RULE_ID )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1981:3: otherlv_1= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getActPropRule());
                      	        }
                              
                    }
                    otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleActProp4314); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_1, grammarAccess.getActPropAccess().getActionChannelCrossReference_0_1_0()); 
                      	
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,41,FOLLOW_41_in_ruleActProp4326); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getActPropAccess().getGreaterThanSignKeyword_0_2());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1996:1: ( (lv_arg_3_0= ruleTerm ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1997:1: (lv_arg_3_0= ruleTerm )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1997:1: (lv_arg_3_0= ruleTerm )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1998:3: lv_arg_3_0= ruleTerm
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getActPropAccess().getArgTermParserRuleCall_0_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleTerm_in_ruleActProp4347);
                    lv_arg_3_0=ruleTerm();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getActPropRule());
                      	        }
                             		set(
                             			current, 
                             			"arg",
                              		lv_arg_3_0, 
                              		"Term");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2015:6: (otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2015:6: (otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2015:8: otherlv_4= '[' ( (otherlv_5= RULE_ID ) ) otherlv_6= ']' ( (lv_arg_7_0= ruleTerm ) )
                    {
                    otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleActProp4367); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getActPropAccess().getLeftSquareBracketKeyword_1_0());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2019:1: ( (otherlv_5= RULE_ID ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2020:1: (otherlv_5= RULE_ID )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2020:1: (otherlv_5= RULE_ID )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2021:3: otherlv_5= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getActPropRule());
                      	        }
                              
                    }
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleActProp4387); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_5, grammarAccess.getActPropAccess().getActionChannelCrossReference_1_1_0()); 
                      	
                    }

                    }


                    }

                    otherlv_6=(Token)match(input,23,FOLLOW_23_in_ruleActProp4399); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getActPropAccess().getRightSquareBracketKeyword_1_2());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2036:1: ( (lv_arg_7_0= ruleTerm ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2037:1: (lv_arg_7_0= ruleTerm )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2037:1: (lv_arg_7_0= ruleTerm )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2038:3: lv_arg_7_0= ruleTerm
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getActPropAccess().getArgTermParserRuleCall_1_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleTerm_in_ruleActProp4420);
                    lv_arg_7_0=ruleTerm();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getActPropRule());
                      	        }
                             		set(
                             			current, 
                             			"arg",
                              		lv_arg_7_0, 
                              		"Term");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2055:6: ( (lv_arg_8_0= ruleTerm ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2055:6: ( (lv_arg_8_0= ruleTerm ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2056:1: (lv_arg_8_0= ruleTerm )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2056:1: (lv_arg_8_0= ruleTerm )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2057:3: lv_arg_8_0= ruleTerm
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getActPropAccess().getArgTermParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleTerm_in_ruleActProp4448);
                    lv_arg_8_0=ruleTerm();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getActPropRule());
                      	        }
                             		set(
                             			current, 
                             			"arg",
                              		lv_arg_8_0, 
                              		"Term");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActProp"


    // $ANTLR start "entryRuleTerm"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2081:1: entryRuleTerm returns [EObject current=null] : iv_ruleTerm= ruleTerm EOF ;
    public final EObject entryRuleTerm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTerm = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2082:2: (iv_ruleTerm= ruleTerm EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2083:2: iv_ruleTerm= ruleTerm EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTermRule()); 
            }
            pushFollow(FOLLOW_ruleTerm_in_entryRuleTerm4484);
            iv_ruleTerm=ruleTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTerm; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTerm4494); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTerm"


    // $ANTLR start "ruleTerm"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2090:1: ruleTerm returns [EObject current=null] : ( ( () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')' ) | this_NotProp_4= ruleNotProp | otherlv_5= 'true' | otherlv_6= 'false' ) ;
    public final EObject ruleTerm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_exp_2_0 = null;

        EObject this_NotProp_4 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2093:28: ( ( ( () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')' ) | this_NotProp_4= ruleNotProp | otherlv_5= 'true' | otherlv_6= 'false' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2094:1: ( ( () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')' ) | this_NotProp_4= ruleNotProp | otherlv_5= 'true' | otherlv_6= 'false' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2094:1: ( ( () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')' ) | this_NotProp_4= ruleNotProp | otherlv_5= 'true' | otherlv_6= 'false' )
            int alt24=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt24=1;
                }
                break;
            case 44:
                {
                alt24=2;
                }
                break;
            case 42:
                {
                alt24=3;
                }
                break;
            case 43:
                {
                alt24=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2094:2: ( () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2094:2: ( () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2094:3: () otherlv_1= '(' ( (lv_exp_2_0= rulePropBody ) ) otherlv_3= ')'
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2094:3: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2095:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getTermAccess().getPropBodyAction_0_0(),
                                  current);
                          
                    }

                    }

                    otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleTerm4541); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getTermAccess().getLeftParenthesisKeyword_0_1());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2104:1: ( (lv_exp_2_0= rulePropBody ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2105:1: (lv_exp_2_0= rulePropBody )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2105:1: (lv_exp_2_0= rulePropBody )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2106:3: lv_exp_2_0= rulePropBody
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getTermAccess().getExpPropBodyParserRuleCall_0_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePropBody_in_ruleTerm4562);
                    lv_exp_2_0=rulePropBody();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getTermRule());
                      	        }
                             		set(
                             			current, 
                             			"exp",
                              		lv_exp_2_0, 
                              		"PropBody");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleTerm4574); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getTermAccess().getRightParenthesisKeyword_0_3());
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2128:5: this_NotProp_4= ruleNotProp
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getTermAccess().getNotPropParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNotProp_in_ruleTerm4603);
                    this_NotProp_4=ruleNotProp();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NotProp_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2137:7: otherlv_5= 'true'
                    {
                    otherlv_5=(Token)match(input,42,FOLLOW_42_in_ruleTerm4620); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getTermAccess().getTrueKeyword_2());
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2142:7: otherlv_6= 'false'
                    {
                    otherlv_6=(Token)match(input,43,FOLLOW_43_in_ruleTerm4638); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getTermAccess().getFalseKeyword_3());
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTerm"


    // $ANTLR start "entryRuleNotProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2154:1: entryRuleNotProp returns [EObject current=null] : iv_ruleNotProp= ruleNotProp EOF ;
    public final EObject entryRuleNotProp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotProp = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2155:2: (iv_ruleNotProp= ruleNotProp EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2156:2: iv_ruleNotProp= ruleNotProp EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNotPropRule()); 
            }
            pushFollow(FOLLOW_ruleNotProp_in_entryRuleNotProp4674);
            iv_ruleNotProp=ruleNotProp();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNotProp; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNotProp4684); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotProp"


    // $ANTLR start "ruleNotProp"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2163:1: ruleNotProp returns [EObject current=null] : (otherlv_0= '!' ( (lv_arg_1_0= ruleTerm ) ) ) ;
    public final EObject ruleNotProp() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2166:28: ( (otherlv_0= '!' ( (lv_arg_1_0= ruleTerm ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2167:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleTerm ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2167:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleTerm ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2167:3: otherlv_0= '!' ( (lv_arg_1_0= ruleTerm ) )
            {
            otherlv_0=(Token)match(input,44,FOLLOW_44_in_ruleNotProp4721); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNotPropAccess().getExclamationMarkKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2171:1: ( (lv_arg_1_0= ruleTerm ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2172:1: (lv_arg_1_0= ruleTerm )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2172:1: (lv_arg_1_0= ruleTerm )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2173:3: lv_arg_1_0= ruleTerm
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNotPropAccess().getArgTermParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTerm_in_ruleNotProp4742);
            lv_arg_1_0=ruleTerm();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNotPropRule());
              	        }
                     		set(
                     			current, 
                     			"arg",
                      		lv_arg_1_0, 
                      		"Term");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotProp"


    // $ANTLR start "entryRuleExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2197:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2198:2: (iv_ruleExpression= ruleExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2199:2: iv_ruleExpression= ruleExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression4778);
            iv_ruleExpression=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression4788); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2206:1: ruleExpression returns [EObject current=null] : this_OrExpression_0= ruleOrExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_OrExpression_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2209:28: (this_OrExpression_0= ruleOrExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2211:5: this_OrExpression_0= ruleOrExpression
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getExpressionAccess().getOrExpressionParserRuleCall()); 
                  
            }
            pushFollow(FOLLOW_ruleOrExpression_in_ruleExpression4834);
            this_OrExpression_0=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_OrExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }

            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleOrExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2227:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2228:2: (iv_ruleOrExpression= ruleOrExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2229:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleOrExpression_in_entryRuleOrExpression4868);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrExpression4878); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2236:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) ) )? ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_AndExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2239:28: ( (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2240:1: (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2240:1: (this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2241:5: this_AndExpression_0= ruleAndExpression ( () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleAndExpression_in_ruleOrExpression4925);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AndExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2249:1: ( () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==34) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2249:2: () otherlv_2= '||' ( (lv_right_3_0= ruleOrExpression ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2249:2: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2250:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getOrExpressionAccess().getOrExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    otherlv_2=(Token)match(input,34,FOLLOW_34_in_ruleOrExpression4946); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getOrExpressionAccess().getVerticalLineVerticalLineKeyword_1_1());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2259:1: ( (lv_right_3_0= ruleOrExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2260:1: (lv_right_3_0= ruleOrExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2260:1: (lv_right_3_0= ruleOrExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2261:3: lv_right_3_0= ruleOrExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getOrExpressionAccess().getRightOrExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOrExpression_in_ruleOrExpression4967);
                    lv_right_3_0=ruleOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getOrExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"OrExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2285:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2286:2: (iv_ruleAndExpression= ruleAndExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2287:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAndExpression_in_entryRuleAndExpression5005);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAndExpression5015); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2294:1: ruleAndExpression returns [EObject current=null] : (this_Relation_0= ruleRelation ( () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) ) )? ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_Relation_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2297:28: ( (this_Relation_0= ruleRelation ( () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2298:1: (this_Relation_0= ruleRelation ( () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2298:1: (this_Relation_0= ruleRelation ( () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2299:5: this_Relation_0= ruleRelation ( () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAndExpressionAccess().getRelationParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleRelation_in_ruleAndExpression5062);
            this_Relation_0=ruleRelation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_Relation_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2307:1: ( () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==39) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2307:2: () otherlv_2= '&&' ( (lv_right_3_0= ruleAndExpression ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2307:2: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2308:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getAndExpressionAccess().getAndExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    otherlv_2=(Token)match(input,39,FOLLOW_39_in_ruleAndExpression5083); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getAndExpressionAccess().getAmpersandAmpersandKeyword_1_1());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2317:1: ( (lv_right_3_0= ruleAndExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2318:1: (lv_right_3_0= ruleAndExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2318:1: (lv_right_3_0= ruleAndExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2319:3: lv_right_3_0= ruleAndExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getAndExpressionAccess().getRightAndExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAndExpression_in_ruleAndExpression5104);
                    lv_right_3_0=ruleAndExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getAndExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"AndExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleRelation"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2343:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2344:2: (iv_ruleRelation= ruleRelation EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2345:2: iv_ruleRelation= ruleRelation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRelationRule()); 
            }
            pushFollow(FOLLOW_ruleRelation_in_entryRuleRelation5142);
            iv_ruleRelation=ruleRelation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRelation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRelation5152); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2352:1: ruleRelation returns [EObject current=null] : (this_SumExpression_0= ruleSumExpression ( () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        EObject this_SumExpression_0 = null;

        Enumerator lv_op_2_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2355:28: ( (this_SumExpression_0= ruleSumExpression ( () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2356:1: (this_SumExpression_0= ruleSumExpression ( () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2356:1: (this_SumExpression_0= ruleSumExpression ( () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2357:5: this_SumExpression_0= ruleSumExpression ( () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getRelationAccess().getSumExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleSumExpression_in_ruleRelation5199);
            this_SumExpression_0=ruleSumExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_SumExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2365:1: ( () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( ((LA27_0>=40 && LA27_0<=41)||(LA27_0>=51 && LA27_0<=54)) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2365:2: () ( (lv_op_2_0= ruleRelop ) ) ( (lv_right_3_0= ruleSumExpression ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2365:2: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2366:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getRelationAccess().getRelationLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2371:2: ( (lv_op_2_0= ruleRelop ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2372:1: (lv_op_2_0= ruleRelop )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2372:1: (lv_op_2_0= ruleRelop )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2373:3: lv_op_2_0= ruleRelop
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationAccess().getOpRelopEnumRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleRelop_in_ruleRelation5229);
                    lv_op_2_0=ruleRelop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationRule());
                      	        }
                             		set(
                             			current, 
                             			"op",
                              		lv_op_2_0, 
                              		"Relop");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2389:2: ( (lv_right_3_0= ruleSumExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2390:1: (lv_right_3_0= ruleSumExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2390:1: (lv_right_3_0= ruleSumExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2391:3: lv_right_3_0= ruleSumExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getRelationAccess().getRightSumExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSumExpression_in_ruleRelation5250);
                    lv_right_3_0=ruleSumExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getRelationRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"SumExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleSumExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2415:1: entryRuleSumExpression returns [EObject current=null] : iv_ruleSumExpression= ruleSumExpression EOF ;
    public final EObject entryRuleSumExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSumExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2416:2: (iv_ruleSumExpression= ruleSumExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2417:2: iv_ruleSumExpression= ruleSumExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSumExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleSumExpression_in_entryRuleSumExpression5288);
            iv_ruleSumExpression=ruleSumExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSumExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSumExpression5298); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSumExpression"


    // $ANTLR start "ruleSumExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2424:1: ruleSumExpression returns [EObject current=null] : (this_MulExpression_0= ruleMulExpression ( () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? ) ;
    public final EObject ruleSumExpression() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        Token lv_op_2_3=null;
        EObject this_MulExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2427:28: ( (this_MulExpression_0= ruleMulExpression ( () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2428:1: (this_MulExpression_0= ruleMulExpression ( () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2428:1: (this_MulExpression_0= ruleMulExpression ( () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2429:5: this_MulExpression_0= ruleMulExpression ( () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getSumExpressionAccess().getMulExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleMulExpression_in_ruleSumExpression5345);
            this_MulExpression_0=ruleMulExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_MulExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2437:1: ( () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==35||(LA29_0>=45 && LA29_0<=46)) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2437:2: () ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) ) ( (lv_right_3_0= ruleSumExpression ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2437:2: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2438:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getSumExpressionAccess().getSumExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2443:2: ( ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2444:1: ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2444:1: ( (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2445:1: (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2445:1: (lv_op_2_1= '+' | lv_op_2_2= '-' | lv_op_2_3= '%' )
                    int alt28=3;
                    switch ( input.LA(1) ) {
                    case 35:
                        {
                        alt28=1;
                        }
                        break;
                    case 45:
                        {
                        alt28=2;
                        }
                        break;
                    case 46:
                        {
                        alt28=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 28, 0, input);

                        throw nvae;
                    }

                    switch (alt28) {
                        case 1 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2446:3: lv_op_2_1= '+'
                            {
                            lv_op_2_1=(Token)match(input,35,FOLLOW_35_in_ruleSumExpression5374); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_1, grammarAccess.getSumExpressionAccess().getOpPlusSignKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getSumExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2458:8: lv_op_2_2= '-'
                            {
                            lv_op_2_2=(Token)match(input,45,FOLLOW_45_in_ruleSumExpression5403); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_2, grammarAccess.getSumExpressionAccess().getOpHyphenMinusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getSumExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_2, null);
                              	    
                            }

                            }
                            break;
                        case 3 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2470:8: lv_op_2_3= '%'
                            {
                            lv_op_2_3=(Token)match(input,46,FOLLOW_46_in_ruleSumExpression5432); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_3, grammarAccess.getSumExpressionAccess().getOpPercentSignKeyword_1_1_0_2());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getSumExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_3, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2485:2: ( (lv_right_3_0= ruleSumExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2486:1: (lv_right_3_0= ruleSumExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2486:1: (lv_right_3_0= ruleSumExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2487:3: lv_right_3_0= ruleSumExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSumExpressionAccess().getRightSumExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSumExpression_in_ruleSumExpression5469);
                    lv_right_3_0=ruleSumExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSumExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"SumExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSumExpression"


    // $ANTLR start "entryRuleMulExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2511:1: entryRuleMulExpression returns [EObject current=null] : iv_ruleMulExpression= ruleMulExpression EOF ;
    public final EObject entryRuleMulExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMulExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2512:2: (iv_ruleMulExpression= ruleMulExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2513:2: iv_ruleMulExpression= ruleMulExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMulExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleMulExpression_in_entryRuleMulExpression5507);
            iv_ruleMulExpression=ruleMulExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMulExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMulExpression5517); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMulExpression"


    // $ANTLR start "ruleMulExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2520:1: ruleMulExpression returns [EObject current=null] : (this_BaseExpression_0= ruleBaseExpression ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) ) )? ) ;
    public final EObject ruleMulExpression() throws RecognitionException {
        EObject current = null;

        Token lv_op_2_1=null;
        Token lv_op_2_2=null;
        EObject this_BaseExpression_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2523:28: ( (this_BaseExpression_0= ruleBaseExpression ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) ) )? ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2524:1: (this_BaseExpression_0= ruleBaseExpression ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) ) )? )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2524:1: (this_BaseExpression_0= ruleBaseExpression ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) ) )? )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2525:5: this_BaseExpression_0= ruleBaseExpression ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) ) )?
            {
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getMulExpressionAccess().getBaseExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_ruleMulExpression5564);
            this_BaseExpression_0=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_BaseExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2533:1: ( () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==36||LA31_0==47) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2533:2: () ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) ) ( (lv_right_3_0= ruleMulExpression ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2533:2: ()
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2534:5: 
                    {
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElementAndSet(
                                  grammarAccess.getMulExpressionAccess().getMulExpressionLeftAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2539:2: ( ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2540:1: ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2540:1: ( (lv_op_2_1= '*' | lv_op_2_2= '/' ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2541:1: (lv_op_2_1= '*' | lv_op_2_2= '/' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2541:1: (lv_op_2_1= '*' | lv_op_2_2= '/' )
                    int alt30=2;
                    int LA30_0 = input.LA(1);

                    if ( (LA30_0==47) ) {
                        alt30=1;
                    }
                    else if ( (LA30_0==36) ) {
                        alt30=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 30, 0, input);

                        throw nvae;
                    }
                    switch (alt30) {
                        case 1 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2542:3: lv_op_2_1= '*'
                            {
                            lv_op_2_1=(Token)match(input,47,FOLLOW_47_in_ruleMulExpression5593); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_1, grammarAccess.getMulExpressionAccess().getOpAsteriskKeyword_1_1_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMulExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2554:8: lv_op_2_2= '/'
                            {
                            lv_op_2_2=(Token)match(input,36,FOLLOW_36_in_ruleMulExpression5622); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_op_2_2, grammarAccess.getMulExpressionAccess().getOpSolidusKeyword_1_1_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getMulExpressionRule());
                              	        }
                                     		setWithLastConsumed(current, "op", lv_op_2_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2569:2: ( (lv_right_3_0= ruleMulExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2570:1: (lv_right_3_0= ruleMulExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2570:1: (lv_right_3_0= ruleMulExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2571:3: lv_right_3_0= ruleMulExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getMulExpressionAccess().getRightMulExpressionParserRuleCall_1_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleMulExpression_in_ruleMulExpression5659);
                    lv_right_3_0=ruleMulExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getMulExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"right",
                              		lv_right_3_0, 
                              		"MulExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMulExpression"


    // $ANTLR start "entryRuleBaseExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2595:1: entryRuleBaseExpression returns [EObject current=null] : iv_ruleBaseExpression= ruleBaseExpression EOF ;
    public final EObject entryRuleBaseExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBaseExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2596:2: (iv_ruleBaseExpression= ruleBaseExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2597:2: iv_ruleBaseExpression= ruleBaseExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBaseExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_entryRuleBaseExpression5697);
            iv_ruleBaseExpression=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBaseExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBaseExpression5707); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBaseExpression"


    // $ANTLR start "ruleBaseExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2604:1: ruleBaseExpression returns [EObject current=null] : (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_Negation_2= ruleNegation | (otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')' ) | this_ReferenceInExpression_6= ruleReferenceInExpression ) ;
    public final EObject ruleBaseExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject this_IntConstant_0 = null;

        EObject this_BoolConstant_1 = null;

        EObject this_Negation_2 = null;

        EObject this_Expression_4 = null;

        EObject this_ReferenceInExpression_6 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2607:28: ( (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_Negation_2= ruleNegation | (otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')' ) | this_ReferenceInExpression_6= ruleReferenceInExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2608:1: (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_Negation_2= ruleNegation | (otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')' ) | this_ReferenceInExpression_6= ruleReferenceInExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2608:1: (this_IntConstant_0= ruleIntConstant | this_BoolConstant_1= ruleBoolConstant | this_Negation_2= ruleNegation | (otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')' ) | this_ReferenceInExpression_6= ruleReferenceInExpression )
            int alt32=5;
            switch ( input.LA(1) ) {
            case RULE_INT:
                {
                alt32=1;
                }
                break;
            case 42:
            case 43:
                {
                alt32=2;
                }
                break;
            case 44:
                {
                alt32=3;
                }
                break;
            case 15:
                {
                alt32=4;
                }
                break;
            case RULE_ID:
                {
                alt32=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2609:5: this_IntConstant_0= ruleIntConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getIntConstantParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntConstant_in_ruleBaseExpression5754);
                    this_IntConstant_0=ruleIntConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntConstant_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2619:5: this_BoolConstant_1= ruleBoolConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getBoolConstantParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBoolConstant_in_ruleBaseExpression5781);
                    this_BoolConstant_1=ruleBoolConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BoolConstant_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2629:5: this_Negation_2= ruleNegation
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getNegationParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNegation_in_ruleBaseExpression5808);
                    this_Negation_2=ruleNegation();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Negation_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2638:6: (otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2638:6: (otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2638:8: otherlv_3= '(' this_Expression_4= ruleExpression otherlv_5= ')'
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_15_in_ruleBaseExpression5826); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getBaseExpressionAccess().getLeftParenthesisKeyword_3_0());
                          
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getExpressionParserRuleCall_3_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleBaseExpression5848);
                    this_Expression_4=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Expression_4; 
                              afterParserOrEnumRuleCall();
                          
                    }
                    otherlv_5=(Token)match(input,17,FOLLOW_17_in_ruleBaseExpression5859); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getBaseExpressionAccess().getRightParenthesisKeyword_3_2());
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2657:5: this_ReferenceInExpression_6= ruleReferenceInExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBaseExpressionAccess().getReferenceInExpressionParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleReferenceInExpression_in_ruleBaseExpression5888);
                    this_ReferenceInExpression_6=ruleReferenceInExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ReferenceInExpression_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBaseExpression"


    // $ANTLR start "entryRuleNegation"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2673:1: entryRuleNegation returns [EObject current=null] : iv_ruleNegation= ruleNegation EOF ;
    public final EObject entryRuleNegation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNegation = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2674:2: (iv_ruleNegation= ruleNegation EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2675:2: iv_ruleNegation= ruleNegation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNegationRule()); 
            }
            pushFollow(FOLLOW_ruleNegation_in_entryRuleNegation5923);
            iv_ruleNegation=ruleNegation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNegation; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNegation5933); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNegation"


    // $ANTLR start "ruleNegation"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2682:1: ruleNegation returns [EObject current=null] : (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseExpression ) ) ) ;
    public final EObject ruleNegation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_arg_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2685:28: ( (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseExpression ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2686:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseExpression ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2686:1: (otherlv_0= '!' ( (lv_arg_1_0= ruleBaseExpression ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2686:3: otherlv_0= '!' ( (lv_arg_1_0= ruleBaseExpression ) )
            {
            otherlv_0=(Token)match(input,44,FOLLOW_44_in_ruleNegation5970); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNegationAccess().getExclamationMarkKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2690:1: ( (lv_arg_1_0= ruleBaseExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2691:1: (lv_arg_1_0= ruleBaseExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2691:1: (lv_arg_1_0= ruleBaseExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2692:3: lv_arg_1_0= ruleBaseExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNegationAccess().getArgBaseExpressionParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBaseExpression_in_ruleNegation5991);
            lv_arg_1_0=ruleBaseExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNegationRule());
              	        }
                     		set(
                     			current, 
                     			"arg",
                      		lv_arg_1_0, 
                      		"BaseExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNegation"


    // $ANTLR start "entryRuleReferenceInExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2716:1: entryRuleReferenceInExpression returns [EObject current=null] : iv_ruleReferenceInExpression= ruleReferenceInExpression EOF ;
    public final EObject entryRuleReferenceInExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceInExpression = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2717:2: (iv_ruleReferenceInExpression= ruleReferenceInExpression EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2718:2: iv_ruleReferenceInExpression= ruleReferenceInExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReferenceInExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleReferenceInExpression_in_entryRuleReferenceInExpression6027);
            iv_ruleReferenceInExpression=ruleReferenceInExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReferenceInExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleReferenceInExpression6037); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceInExpression"


    // $ANTLR start "ruleReferenceInExpression"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2725:1: ruleReferenceInExpression returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleReferenceInExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2728:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2729:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2729:1: ( (otherlv_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2730:1: (otherlv_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2730:1: (otherlv_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2731:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getReferenceInExpressionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleReferenceInExpression6081); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getReferenceInExpressionAccess().getReferenceReferenceableElementCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceInExpression"


    // $ANTLR start "entryRuleTypeDef"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2752:1: entryRuleTypeDef returns [EObject current=null] : iv_ruleTypeDef= ruleTypeDef EOF ;
    public final EObject entryRuleTypeDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeDef = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2753:2: (iv_ruleTypeDef= ruleTypeDef EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2754:2: iv_ruleTypeDef= ruleTypeDef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeDefRule()); 
            }
            pushFollow(FOLLOW_ruleTypeDef_in_entryRuleTypeDef6118);
            iv_ruleTypeDef=ruleTypeDef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeDef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeDef6128); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeDef"


    // $ANTLR start "ruleTypeDef"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2761:1: ruleTypeDef returns [EObject current=null] : (otherlv_0= 'typedef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_elements_3_0= ruleTypeElement ) ) (otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) ) )* otherlv_6= ';' ) ;
    public final EObject ruleTypeDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_elements_3_0 = null;

        EObject lv_elements_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2764:28: ( (otherlv_0= 'typedef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_elements_3_0= ruleTypeElement ) ) (otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) ) )* otherlv_6= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2765:1: (otherlv_0= 'typedef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_elements_3_0= ruleTypeElement ) ) (otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) ) )* otherlv_6= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2765:1: (otherlv_0= 'typedef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_elements_3_0= ruleTypeElement ) ) (otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) ) )* otherlv_6= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2765:3: otherlv_0= 'typedef' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_elements_3_0= ruleTypeElement ) ) (otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) ) )* otherlv_6= ';'
            {
            otherlv_0=(Token)match(input,48,FOLLOW_48_in_ruleTypeDef6165); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTypeDefAccess().getTypedefKeyword_0());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2769:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2770:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2770:1: (lv_name_1_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2771:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTypeDef6182); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getTypeDefAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypeDefRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleTypeDef6199); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTypeDefAccess().getEqualsSignKeyword_2());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2791:1: ( (lv_elements_3_0= ruleTypeElement ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2792:1: (lv_elements_3_0= ruleTypeElement )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2792:1: (lv_elements_3_0= ruleTypeElement )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2793:3: lv_elements_3_0= ruleTypeElement
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTypeDefAccess().getElementsTypeElementParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTypeElement_in_ruleTypeDef6220);
            lv_elements_3_0=ruleTypeElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTypeDefRule());
              	        }
                     		add(
                     			current, 
                     			"elements",
                      		lv_elements_3_0, 
                      		"TypeElement");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2809:2: (otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) ) )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==49) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2809:4: otherlv_4= '|' ( (lv_elements_5_0= ruleTypeElement ) )
            	    {
            	    otherlv_4=(Token)match(input,49,FOLLOW_49_in_ruleTypeDef6233); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_4, grammarAccess.getTypeDefAccess().getVerticalLineKeyword_4_0());
            	          
            	    }
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2813:1: ( (lv_elements_5_0= ruleTypeElement ) )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2814:1: (lv_elements_5_0= ruleTypeElement )
            	    {
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2814:1: (lv_elements_5_0= ruleTypeElement )
            	    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2815:3: lv_elements_5_0= ruleTypeElement
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getTypeDefAccess().getElementsTypeElementParserRuleCall_4_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleTypeElement_in_ruleTypeDef6254);
            	    lv_elements_5_0=ruleTypeElement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getTypeDefRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"elements",
            	              		lv_elements_5_0, 
            	              		"TypeElement");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

            otherlv_6=(Token)match(input,13,FOLLOW_13_in_ruleTypeDef6268); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getTypeDefAccess().getSemicolonKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeDef"


    // $ANTLR start "entryRuleTypeElement"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2843:1: entryRuleTypeElement returns [EObject current=null] : iv_ruleTypeElement= ruleTypeElement EOF ;
    public final EObject entryRuleTypeElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeElement = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2844:2: (iv_ruleTypeElement= ruleTypeElement EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2845:2: iv_ruleTypeElement= ruleTypeElement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTypeElementRule()); 
            }
            pushFollow(FOLLOW_ruleTypeElement_in_entryRuleTypeElement6304);
            iv_ruleTypeElement=ruleTypeElement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTypeElement; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTypeElement6314); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeElement"


    // $ANTLR start "ruleTypeElement"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2852:1: ruleTypeElement returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleTypeElement() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2855:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2856:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2856:1: ( (lv_name_0_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2857:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2857:1: (lv_name_0_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2858:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTypeElement6355); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getTypeElementAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTypeElementRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeElement"


    // $ANTLR start "entryRuleVariable"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2882:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2883:2: (iv_ruleVariable= ruleVariable EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2884:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_ruleVariable_in_entryRuleVariable6395);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVariable6405); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2891:1: ruleVariable returns [EObject current=null] : ( ( (lv_type_0_0= ruleTypeExpression ) ) ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        EObject lv_type_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2894:28: ( ( ( (lv_type_0_0= ruleTypeExpression ) ) ( (lv_name_1_0= RULE_ID ) ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2895:1: ( ( (lv_type_0_0= ruleTypeExpression ) ) ( (lv_name_1_0= RULE_ID ) ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2895:1: ( ( (lv_type_0_0= ruleTypeExpression ) ) ( (lv_name_1_0= RULE_ID ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2895:2: ( (lv_type_0_0= ruleTypeExpression ) ) ( (lv_name_1_0= RULE_ID ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2895:2: ( (lv_type_0_0= ruleTypeExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2896:1: (lv_type_0_0= ruleTypeExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2896:1: (lv_type_0_0= ruleTypeExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2897:3: lv_type_0_0= ruleTypeExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVariableAccess().getTypeTypeExpressionParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleTypeExpression_in_ruleVariable6451);
            lv_type_0_0=ruleTypeExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVariableRule());
              	        }
                     		set(
                     			current, 
                     			"type",
                      		lv_type_0_0, 
                      		"TypeExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2913:2: ( (lv_name_1_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2914:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2914:1: (lv_name_1_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2915:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVariable6468); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getVariableRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleIntConstant"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2939:1: entryRuleIntConstant returns [EObject current=null] : iv_ruleIntConstant= ruleIntConstant EOF ;
    public final EObject entryRuleIntConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntConstant = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2940:2: (iv_ruleIntConstant= ruleIntConstant EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2941:2: iv_ruleIntConstant= ruleIntConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntConstantRule()); 
            }
            pushFollow(FOLLOW_ruleIntConstant_in_entryRuleIntConstant6509);
            iv_ruleIntConstant=ruleIntConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntConstant6519); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntConstant"


    // $ANTLR start "ruleIntConstant"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2948:1: ruleIntConstant returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleIntConstant() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2951:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2952:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2952:1: ( (lv_value_0_0= RULE_INT ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2953:1: (lv_value_0_0= RULE_INT )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2953:1: (lv_value_0_0= RULE_INT )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2954:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntConstant6560); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getIntConstantAccess().getValueINTTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntConstantRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"INT");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntConstant"


    // $ANTLR start "entryRuleBoolConstant"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2978:1: entryRuleBoolConstant returns [EObject current=null] : iv_ruleBoolConstant= ruleBoolConstant EOF ;
    public final EObject entryRuleBoolConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoolConstant = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2979:2: (iv_ruleBoolConstant= ruleBoolConstant EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2980:2: iv_ruleBoolConstant= ruleBoolConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBoolConstantRule()); 
            }
            pushFollow(FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant6600);
            iv_ruleBoolConstant=ruleBoolConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBoolConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBoolConstant6610); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolConstant"


    // $ANTLR start "ruleBoolConstant"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2987:1: ruleBoolConstant returns [EObject current=null] : ( (lv_value_0_0= ruleBoolValue ) ) ;
    public final EObject ruleBoolConstant() throws RecognitionException {
        EObject current = null;

        Enumerator lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2990:28: ( ( (lv_value_0_0= ruleBoolValue ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2991:1: ( (lv_value_0_0= ruleBoolValue ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2991:1: ( (lv_value_0_0= ruleBoolValue ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2992:1: (lv_value_0_0= ruleBoolValue )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2992:1: (lv_value_0_0= ruleBoolValue )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:2993:3: lv_value_0_0= ruleBoolValue
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getBoolConstantAccess().getValueBoolValueEnumRuleCall_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBoolValue_in_ruleBoolConstant6655);
            lv_value_0_0=ruleBoolValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getBoolConstantRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"BoolValue");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolConstant"


    // $ANTLR start "entryRuleVarDeclaration"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3017:1: entryRuleVarDeclaration returns [EObject current=null] : iv_ruleVarDeclaration= ruleVarDeclaration EOF ;
    public final EObject entryRuleVarDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVarDeclaration = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3018:2: (iv_ruleVarDeclaration= ruleVarDeclaration EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3019:2: iv_ruleVarDeclaration= ruleVarDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVarDeclarationRule()); 
            }
            pushFollow(FOLLOW_ruleVarDeclaration_in_entryRuleVarDeclaration6690);
            iv_ruleVarDeclaration=ruleVarDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVarDeclaration; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleVarDeclaration6700); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVarDeclaration"


    // $ANTLR start "ruleVarDeclaration"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3026:1: ruleVarDeclaration returns [EObject current=null] : ( ( (lv_variable_0_0= ruleVariable ) ) (otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) ) )? otherlv_3= ';' ) ;
    public final EObject ruleVarDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_variable_0_0 = null;

        EObject lv_init_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3029:28: ( ( ( (lv_variable_0_0= ruleVariable ) ) (otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) ) )? otherlv_3= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3030:1: ( ( (lv_variable_0_0= ruleVariable ) ) (otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) ) )? otherlv_3= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3030:1: ( ( (lv_variable_0_0= ruleVariable ) ) (otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) ) )? otherlv_3= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3030:2: ( (lv_variable_0_0= ruleVariable ) ) (otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) ) )? otherlv_3= ';'
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3030:2: ( (lv_variable_0_0= ruleVariable ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3031:1: (lv_variable_0_0= ruleVariable )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3031:1: (lv_variable_0_0= ruleVariable )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3032:3: lv_variable_0_0= ruleVariable
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getVarDeclarationAccess().getVariableVariableParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleVariable_in_ruleVarDeclaration6746);
            lv_variable_0_0=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getVarDeclarationRule());
              	        }
                     		set(
                     			current, 
                     			"variable",
                      		lv_variable_0_0, 
                      		"Variable");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3048:2: (otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==12) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3048:4: otherlv_1= '=' ( (lv_init_2_0= ruleExpression ) )
                    {
                    otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleVarDeclaration6759); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getVarDeclarationAccess().getEqualsSignKeyword_1_0());
                          
                    }
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3052:1: ( (lv_init_2_0= ruleExpression ) )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3053:1: (lv_init_2_0= ruleExpression )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3053:1: (lv_init_2_0= ruleExpression )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3054:3: lv_init_2_0= ruleExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getVarDeclarationAccess().getInitExpressionParserRuleCall_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleExpression_in_ruleVarDeclaration6780);
                    lv_init_2_0=ruleExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getVarDeclarationRule());
                      	        }
                             		set(
                             			current, 
                             			"init",
                              		lv_init_2_0, 
                              		"Expression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleVarDeclaration6794); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getVarDeclarationAccess().getSemicolonKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVarDeclaration"


    // $ANTLR start "entryRuleAssignment"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3082:1: entryRuleAssignment returns [EObject current=null] : iv_ruleAssignment= ruleAssignment EOF ;
    public final EObject entryRuleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment = null;


        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3083:2: (iv_ruleAssignment= ruleAssignment EOF )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3084:2: iv_ruleAssignment= ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FOLLOW_ruleAssignment_in_entryRuleAssignment6830);
            iv_ruleAssignment=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignment; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAssignment6840); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3091:1: ruleAssignment returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_in_2_0= ruleExpression ) ) otherlv_3= ';' ) ;
    public final EObject ruleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_in_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3094:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_in_2_0= ruleExpression ) ) otherlv_3= ';' ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3095:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_in_2_0= ruleExpression ) ) otherlv_3= ';' )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3095:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_in_2_0= ruleExpression ) ) otherlv_3= ';' )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3095:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=' ( (lv_in_2_0= ruleExpression ) ) otherlv_3= ';'
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3095:2: ( (otherlv_0= RULE_ID ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3096:1: (otherlv_0= RULE_ID )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3096:1: (otherlv_0= RULE_ID )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3097:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getAssignmentRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAssignment6885); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getAssignmentAccess().getIdVariableCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleAssignment6897); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAssignmentAccess().getEqualsSignKeyword_1());
                  
            }
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3112:1: ( (lv_in_2_0= ruleExpression ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3113:1: (lv_in_2_0= ruleExpression )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3113:1: (lv_in_2_0= ruleExpression )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3114:3: lv_in_2_0= ruleExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAssignmentAccess().getInExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleExpression_in_ruleAssignment6918);
            lv_in_2_0=ruleExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAssignmentRule());
              	        }
                     		set(
                     			current, 
                     			"in",
                      		lv_in_2_0, 
                      		"Expression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleAssignment6930); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getAssignmentAccess().getSemicolonKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "ruleActionModality"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3142:1: ruleActionModality returns [Enumerator current=null] : ( (enumLiteral_0= '?' ) | (enumLiteral_1= '!' ) ) ;
    public final Enumerator ruleActionModality() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3144:28: ( ( (enumLiteral_0= '?' ) | (enumLiteral_1= '!' ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3145:1: ( (enumLiteral_0= '?' ) | (enumLiteral_1= '!' ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3145:1: ( (enumLiteral_0= '?' ) | (enumLiteral_1= '!' ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==50) ) {
                alt35=1;
            }
            else if ( (LA35_0==44) ) {
                alt35=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3145:2: (enumLiteral_0= '?' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3145:2: (enumLiteral_0= '?' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3145:4: enumLiteral_0= '?'
                    {
                    enumLiteral_0=(Token)match(input,50,FOLLOW_50_in_ruleActionModality6980); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getActionModalityAccess().getINPUTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getActionModalityAccess().getINPUTEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3151:6: (enumLiteral_1= '!' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3151:6: (enumLiteral_1= '!' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3151:8: enumLiteral_1= '!'
                    {
                    enumLiteral_1=(Token)match(input,44,FOLLOW_44_in_ruleActionModality6997); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getActionModalityAccess().getOUTPUTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getActionModalityAccess().getOUTPUTEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionModality"


    // $ANTLR start "ruleBoolValue"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3161:1: ruleBoolValue returns [Enumerator current=null] : ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) ) ;
    public final Enumerator ruleBoolValue() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3163:28: ( ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3164:1: ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3164:1: ( (enumLiteral_0= 'true' ) | (enumLiteral_1= 'false' ) )
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==42) ) {
                alt36=1;
            }
            else if ( (LA36_0==43) ) {
                alt36=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }
            switch (alt36) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3164:2: (enumLiteral_0= 'true' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3164:2: (enumLiteral_0= 'true' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3164:4: enumLiteral_0= 'true'
                    {
                    enumLiteral_0=(Token)match(input,42,FOLLOW_42_in_ruleBoolValue7042); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getBoolValueAccess().getTrueEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getBoolValueAccess().getTrueEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3170:6: (enumLiteral_1= 'false' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3170:6: (enumLiteral_1= 'false' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3170:8: enumLiteral_1= 'false'
                    {
                    enumLiteral_1=(Token)match(input,43,FOLLOW_43_in_ruleBoolValue7059); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getBoolValueAccess().getFalseEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getBoolValueAccess().getFalseEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolValue"


    // $ANTLR start "ruleRelop"
    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3180:1: ruleRelop returns [Enumerator current=null] : ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) ) ;
    public final Enumerator ruleRelop() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;

         enterRule(); 
        try {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3182:28: ( ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) ) )
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3183:1: ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) )
            {
            // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3183:1: ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '<=' ) | (enumLiteral_3= '<' ) | (enumLiteral_4= '>=' ) | (enumLiteral_5= '>' ) )
            int alt37=6;
            switch ( input.LA(1) ) {
            case 51:
                {
                alt37=1;
                }
                break;
            case 52:
                {
                alt37=2;
                }
                break;
            case 53:
                {
                alt37=3;
                }
                break;
            case 40:
                {
                alt37=4;
                }
                break;
            case 54:
                {
                alt37=5;
                }
                break;
            case 41:
                {
                alt37=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }

            switch (alt37) {
                case 1 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3183:2: (enumLiteral_0= '==' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3183:2: (enumLiteral_0= '==' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3183:4: enumLiteral_0= '=='
                    {
                    enumLiteral_0=(Token)match(input,51,FOLLOW_51_in_ruleRelop7104); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelopAccess().getREL_EQEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getRelopAccess().getREL_EQEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3189:6: (enumLiteral_1= '!=' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3189:6: (enumLiteral_1= '!=' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3189:8: enumLiteral_1= '!='
                    {
                    enumLiteral_1=(Token)match(input,52,FOLLOW_52_in_ruleRelop7121); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelopAccess().getREL_NEQEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getRelopAccess().getREL_NEQEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3195:6: (enumLiteral_2= '<=' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3195:6: (enumLiteral_2= '<=' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3195:8: enumLiteral_2= '<='
                    {
                    enumLiteral_2=(Token)match(input,53,FOLLOW_53_in_ruleRelop7138); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelopAccess().getREL_LEQEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getRelopAccess().getREL_LEQEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3201:6: (enumLiteral_3= '<' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3201:6: (enumLiteral_3= '<' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3201:8: enumLiteral_3= '<'
                    {
                    enumLiteral_3=(Token)match(input,40,FOLLOW_40_in_ruleRelop7155); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelopAccess().getREL_LESEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_3, grammarAccess.getRelopAccess().getREL_LESEnumLiteralDeclaration_3()); 
                          
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3207:6: (enumLiteral_4= '>=' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3207:6: (enumLiteral_4= '>=' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3207:8: enumLiteral_4= '>='
                    {
                    enumLiteral_4=(Token)match(input,54,FOLLOW_54_in_ruleRelop7172); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelopAccess().getREL_GEQEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_4, grammarAccess.getRelopAccess().getREL_GEQEnumLiteralDeclaration_4()); 
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3213:6: (enumLiteral_5= '>' )
                    {
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3213:6: (enumLiteral_5= '>' )
                    // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:3213:8: enumLiteral_5= '>'
                    {
                    enumLiteral_5=(Token)match(input,41,FOLLOW_41_in_ruleRelop7189); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getRelopAccess().getREL_GTREnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_5, grammarAccess.getRelopAccess().getREL_GTREnumLiteralDeclaration_5()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelop"

    // $ANTLR start synpred1_InternalSecsi
    public final void synpred1_InternalSecsi_fragment() throws RecognitionException {   
        // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1206:4: ( ( 'else' ) )
        // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1207:1: ( 'else' )
        {
        // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1207:1: ( 'else' )
        // ../org.lic.secsi/src-gen/org/lic/parser/antlr/internal/InternalSecsi.g:1208:2: 'else'
        {
        match(input,31,FOLLOW_31_in_synpred1_InternalSecsi2541); if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred1_InternalSecsi

    // Delegated rules

    public final boolean synpred1_InternalSecsi() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalSecsi_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_ruleProgram_in_entryRuleProgram75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProgram85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChannelsDeclaration_in_ruleProgram132 = new BitSet(new long[]{0x0001004200044802L});
    public static final BitSet FOLLOW_ruleAgent_in_ruleProgram159 = new BitSet(new long[]{0x0001004200044802L});
    public static final BitSet FOLLOW_ruleTypeDef_in_ruleProgram186 = new BitSet(new long[]{0x0001004200044802L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleProgram213 = new BitSet(new long[]{0x0001004200044802L});
    public static final BitSet FOLLOW_ruleSystem_in_ruleProgram236 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_rulePropList_in_ruleProgram258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant295 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleConstant342 = new BitSet(new long[]{0x0000000001300010L});
    public static final BitSet FOLLOW_ruleTypeExpression_in_ruleConstant363 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleConstant380 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleConstant397 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleConstant418 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleConstant430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAgent_in_entryRuleAgent466 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAgent476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleAgent513 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAgent530 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleAgent547 = new BitSet(new long[]{0x0000000001320010L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleAgent569 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_16_in_ruleAgent582 = new BitSet(new long[]{0x0000000001300010L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleAgent603 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_17_in_ruleAgent619 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleChannelsDeclaration_in_ruleAgent640 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleAgent662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChannelsDeclaration_in_entryRuleChannelsDeclaration698 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChannelsDeclaration708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleChannelsDeclaration745 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleChannel_in_ruleChannelsDeclaration766 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_16_in_ruleChannelsDeclaration779 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleChannel_in_ruleChannelsDeclaration800 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_13_in_ruleChannelsDeclaration814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChannel_in_entryRuleChannel850 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChannel860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleChannel902 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleChannel919 = new BitSet(new long[]{0x0000000001300010L});
    public static final BitSet FOLLOW_ruleTypeExpression_in_ruleChannel940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeExpression_in_entryRuleTypeExpression978 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeExpression988 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleTypeExpression1026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeReference_in_ruleTypeExpression1064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerType_in_ruleTypeExpression1091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanType_in_ruleTypeExpression1118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerType_in_entryRuleIntegerType1153 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerType1163 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleIntegerType1200 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIntegerType1221 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleIntegerType1233 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIntegerType1254 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleIntegerType1266 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanType_in_entryRuleBooleanType1302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanType1312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleBooleanType1358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeReference_in_entryRuleTypeReference1394 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeReference1404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTypeReference1448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_entryRuleBlock1483 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlock1493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleBlock1548 = new BitSet(new long[]{0x000000014B340012L});
    public static final BitSet FOLLOW_ruleStatement_in_entryRuleStatement1585 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStatement1595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfThenElse_in_ruleStatement1642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhile_in_ruleStatement1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAction_in_ruleStatement1696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSwitch_Case_in_ruleStatement1723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignment_in_ruleStatement1750 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_ruleStatement1777 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleStatement1795 = new BitSet(new long[]{0x000000014F340010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleStatement1817 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleStatement1828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAction_in_entryRuleAction1865 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAction1875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAction1920 = new BitSet(new long[]{0x0004100000000000L});
    public static final BitSet FOLLOW_ruleActionModality_in_ruleAction1941 = new BitSet(new long[]{0x000000000000A000L});
    public static final BitSet FOLLOW_15_in_ruleAction1960 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleAction1994 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleAction2006 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleAction2020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSwitch_Case_in_entryRuleSwitch_Case2056 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSwitch_Case2066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleSwitch_Case2103 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleSwitch_Case2115 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleSwitch_Case2136 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleSwitch_Case2148 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleSwitch_Case2160 = new BitSet(new long[]{0x0000000030000000L});
    public static final BitSet FOLLOW_ruleCase_in_ruleSwitch_Case2181 = new BitSet(new long[]{0x0000000030000000L});
    public static final BitSet FOLLOW_28_in_ruleSwitch_Case2194 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleSwitch_Case2206 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleSwitch_Case2227 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleSwitch_Case2239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCase_in_entryRuleCase2275 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCase2285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleCase2322 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleCase2343 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleCase2355 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleCase2376 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfThenElse_in_entryRuleIfThenElse2412 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfThenElse2422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleIfThenElse2459 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleIfThenElse2471 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleIfThenElse2492 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleIfThenElse2504 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleIfThenElse2525 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_31_in_ruleIfThenElse2559 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleIfThenElse2593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhile_in_entryRuleWhile2631 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhile2641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleWhile2678 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleWhile2690 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleWhile2711 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleWhile2723 = new BitSet(new long[]{0x000000014B340010L});
    public static final BitSet FOLLOW_ruleStatement_in_ruleWhile2744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSystem_in_entryRuleSystem2780 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSystem2790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleSystem2827 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleSystem2839 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rulePar_in_ruleSystem2860 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleSystem2872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePar_in_entryRulePar2908 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePar2918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSum_in_rulePar2964 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_rulePar2986 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rulePar_in_rulePar3007 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSum_in_entryRuleSum3045 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSum3055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProc_in_ruleSum3101 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_35_in_ruleSum3123 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleSum_in_ruleSum3144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProc_in_entryRuleProc3182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProc3192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProc3237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProc3264 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_ruleRen_in_ruleProc3285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRen_in_entryRuleRen3322 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRen3332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleRen3369 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRen3389 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleRen3401 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRen3418 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_16_in_ruleRen3436 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRen3456 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleRen3468 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRen3485 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_23_in_ruleRen3504 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_ruleRes_in_ruleRen3525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRes_in_entryRuleRes3562 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRes3572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleRes3609 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleRes3621 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRes3641 = new BitSet(new long[]{0x0000000004010000L});
    public static final BitSet FOLLOW_16_in_ruleRes3654 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRes3674 = new BitSet(new long[]{0x0000000004010000L});
    public static final BitSet FOLLOW_26_in_ruleRes3688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePropList_in_entryRulePropList3724 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePropList3734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rulePropList3780 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_rulePropList3792 = new BitSet(new long[]{0x00001D0004208000L});
    public static final BitSet FOLLOW_ruleProp_in_rulePropList3813 = new BitSet(new long[]{0x00001D0004208000L});
    public static final BitSet FOLLOW_26_in_rulePropList3826 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProp_in_entryRuleProp3862 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProp3872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePropBody_in_ruleProp3919 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleProp3930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePropBody_in_entryRulePropBody3966 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePropBody3976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrProp_in_rulePropBody4022 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrProp_in_entryRuleOrProp4056 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrProp4066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActProp_in_ruleOrProp4113 = new BitSet(new long[]{0x0000008400000002L});
    public static final BitSet FOLLOW_34_in_ruleOrProp4142 = new BitSet(new long[]{0x00001D0000208000L});
    public static final BitSet FOLLOW_39_in_ruleOrProp4171 = new BitSet(new long[]{0x00001D0000208000L});
    public static final BitSet FOLLOW_ruleOrProp_in_ruleOrProp4208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActProp_in_entryRuleActProp4246 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleActProp4256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleActProp4294 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleActProp4314 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_ruleActProp4326 = new BitSet(new long[]{0x00001D0000208000L});
    public static final BitSet FOLLOW_ruleTerm_in_ruleActProp4347 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleActProp4367 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleActProp4387 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleActProp4399 = new BitSet(new long[]{0x00001D0000208000L});
    public static final BitSet FOLLOW_ruleTerm_in_ruleActProp4420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerm_in_ruleActProp4448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTerm_in_entryRuleTerm4484 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTerm4494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleTerm4541 = new BitSet(new long[]{0x00001D0000208000L});
    public static final BitSet FOLLOW_rulePropBody_in_ruleTerm4562 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleTerm4574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNotProp_in_ruleTerm4603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleTerm4620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleTerm4638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNotProp_in_entryRuleNotProp4674 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNotProp4684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleNotProp4721 = new BitSet(new long[]{0x00001D0000208000L});
    public static final BitSet FOLLOW_ruleTerm_in_ruleNotProp4742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression4778 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression4788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleExpression4834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrExpression_in_entryRuleOrExpression4868 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrExpression4878 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleOrExpression4925 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_ruleOrExpression4946 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleOrExpression4967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_entryRuleAndExpression5005 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAndExpression5015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelation_in_ruleAndExpression5062 = new BitSet(new long[]{0x0000008000000002L});
    public static final BitSet FOLLOW_39_in_ruleAndExpression5083 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleAndExpression5104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRelation_in_entryRuleRelation5142 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRelation5152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSumExpression_in_ruleRelation5199 = new BitSet(new long[]{0x0078030000000002L});
    public static final BitSet FOLLOW_ruleRelop_in_ruleRelation5229 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleSumExpression_in_ruleRelation5250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSumExpression_in_entryRuleSumExpression5288 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSumExpression5298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMulExpression_in_ruleSumExpression5345 = new BitSet(new long[]{0x0000600800000002L});
    public static final BitSet FOLLOW_35_in_ruleSumExpression5374 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_45_in_ruleSumExpression5403 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_46_in_ruleSumExpression5432 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleSumExpression_in_ruleSumExpression5469 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMulExpression_in_entryRuleMulExpression5507 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMulExpression5517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleMulExpression5564 = new BitSet(new long[]{0x0000801000000002L});
    public static final BitSet FOLLOW_47_in_ruleMulExpression5593 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_36_in_ruleMulExpression5622 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleMulExpression_in_ruleMulExpression5659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_entryRuleBaseExpression5697 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBaseExpression5707 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntConstant_in_ruleBaseExpression5754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolConstant_in_ruleBaseExpression5781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNegation_in_ruleBaseExpression5808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleBaseExpression5826 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleBaseExpression5848 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleBaseExpression5859 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceInExpression_in_ruleBaseExpression5888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNegation_in_entryRuleNegation5923 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNegation5933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleNegation5970 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleBaseExpression_in_ruleNegation5991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferenceInExpression_in_entryRuleReferenceInExpression6027 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReferenceInExpression6037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleReferenceInExpression6081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeDef_in_entryRuleTypeDef6118 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeDef6128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleTypeDef6165 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTypeDef6182 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleTypeDef6199 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleTypeElement_in_ruleTypeDef6220 = new BitSet(new long[]{0x0002000000002000L});
    public static final BitSet FOLLOW_49_in_ruleTypeDef6233 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleTypeElement_in_ruleTypeDef6254 = new BitSet(new long[]{0x0002000000002000L});
    public static final BitSet FOLLOW_13_in_ruleTypeDef6268 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeElement_in_entryRuleTypeElement6304 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTypeElement6314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTypeElement6355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_entryRuleVariable6395 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVariable6405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTypeExpression_in_ruleVariable6451 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVariable6468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntConstant_in_entryRuleIntConstant6509 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntConstant6519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntConstant6560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolConstant_in_entryRuleBoolConstant6600 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBoolConstant6610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBoolValue_in_ruleBoolConstant6655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVarDeclaration_in_entryRuleVarDeclaration6690 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVarDeclaration6700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVariable_in_ruleVarDeclaration6746 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_12_in_ruleVarDeclaration6759 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleVarDeclaration6780 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleVarDeclaration6794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAssignment_in_entryRuleAssignment6830 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAssignment6840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAssignment6885 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleAssignment6897 = new BitSet(new long[]{0x00001C0000008030L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleAssignment6918 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleAssignment6930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleActionModality6980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleActionModality6997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleBoolValue7042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleBoolValue7059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleRelop7104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruleRelop7121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleRelop7138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleRelop7155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_ruleRelop7172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleRelop7189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_synpred1_InternalSecsi2541 = new BitSet(new long[]{0x0000000000000002L});

}
package org.cmg.tapas.clts.extensions;

import com.google.common.base.Objects;
import java.util.List;
import org.cmg.tapas.views.TAPAsGraphElementContentProvider;
import org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody;
import org.cmg.tapas.xtext.clts.composedLts.LtsRule;
import org.cmg.tapas.xtext.clts.composedLts.LtsState;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class CLTSGraphElementContentProvider implements TAPAsGraphElementContentProvider {
  private LtsDeclarationBody ltsBody;
  
  @Override
  public Object[] getRelationships(final Object source, final Object dest) {
    EList<LtsRule> _rules = this.ltsBody.getRules();
    final Function1<LtsRule, Boolean> _function = (LtsRule r) -> {
      LtsState _src = r.getSrc();
      return Boolean.valueOf(Objects.equal(_src, source));
    };
    Iterable<LtsRule> _filter = IterableExtensions.<LtsRule>filter(_rules, _function);
    final Function1<LtsRule, Boolean> _function_1 = (LtsRule it) -> {
      LtsState _trg = it.getTrg();
      return Boolean.valueOf(Objects.equal(_trg, dest));
    };
    Iterable<LtsRule> _filter_1 = IterableExtensions.<LtsRule>filter(_filter, _function_1);
    List<LtsRule> _list = IterableExtensions.<LtsRule>toList(_filter_1);
    return _list.toArray();
  }
  
  @Override
  public Object[] getElements(final Object inputElement) {
    Object[] _xblockexpression = null;
    {
      if ((inputElement instanceof LtsDeclarationBody)) {
        this.ltsBody = ((LtsDeclarationBody) inputElement);
      }
      Object[] _elvis = null;
      EList<LtsState> _states = null;
      if (this.ltsBody!=null) {
        _states=this.ltsBody.getStates();
      }
      List<LtsState> _list = null;
      if (_states!=null) {
        _list=IterableExtensions.<LtsState>toList(_states);
      }
      Object[] _array = null;
      if (_list!=null) {
        _array=_list.toArray();
      }
      if (_array != null) {
        _elvis = _array;
      } else {
        Object[] _newArrayOfSize = new Object[0];
        _elvis = _newArrayOfSize;
      }
      _xblockexpression = _elvis;
    }
    return _xblockexpression;
  }
  
  @Override
  public void dispose() {
  }
  
  @Override
  public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput) {
  }
}

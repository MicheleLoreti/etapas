/*********************************************************************
  * FILE:  APProcess.java                                             *
  *                                                                   *
  * Copyright (C) 2006 Francesco Calzolai                             *
  *                                                                   *
  * This is free software; you can redistribute it and/or             *
  * modify it under the terms of the GNU General Public License       *
  * as published by the Free Software Foundation; either version 2    *
  * of the License, or (at your option) any later version.            *
  *                                                                   *
  * This software is distributed in the hope that it will be useful,  *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of    *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *
  * GNU General Public License for more details.                      *
  *                                                                   *
  * You should have received a copy of the GNU General Public License *
  * along with JAgent; if not, write to the Free Software             *
  * Foundation, Inc., 51 Franklin St, Fifth Floor,                    *
  * Boston, MA  02110-1301  USA                                       *
  *                                                                   *
  *********************************************************************/
package org.cmg.tapas.processAlgebra;


import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.graph.filter.Filter;

/** 
 * L'interfaccia � l'astrazione per un processo di una generica 
 * algebra di processo. Questa classe � parametrica rispetto al 
 * concetto di processo ed a quello di azione.
 *  
 * @param <P> La definizione di processo.
 * @param <R> La definizione di azione.
 *   
 * @author Francesco Calzolai, Michele Loreti
 * @version 1.0 - 16 Dic 2005
 */
public interface APProcess<P extends APProcess<P,R>, R extends ActionInterface> extends StateInterface {
	
	/** Restituisce tutte le possibili azioni e le relative destinazioni
	 * che � possibile compiere dal processo corrente. Le coppie
	 * sono del tipo:<br>
	 *   < azione, destinazione > 
	 * <br> 
	 *  
	 * @see TransitionCouple < R,P >.
	 * 
	 * @return Lista delle coppie del tipo < azione, destinazione >.
	 * 
	 */
	public Map<R,LinkedList<P>> getNext();
	
	/** Restituisce tutte le possibili destinazioni che � possibile
	 * raqgiungere dal processo corrente facendo l'azione <i>a</i>.
	 *
	 * @see TransitionCouple < R,P >.
	 * 
	 * @param a L'azione da compiere. 
	 * @return Lista delle coppie del tipo < azione, destinazione >.
	 *
	 */
	public LinkedList<P> getNext(R a);
	
	/** Restituisce tutte le possibili destinazioni che � possibile
	 * raqgiungere dal processo corrente e che verifichino il filtro
	 *  <i>f</i>.
	 * 
	 * @see TransitionCouple < R,P >
	 * @see Filter < T >.
	 *    
	 * @param f Filtro da verificare. 
	 * @return Lista delle coppie del tipo < azione, destinazione >.
	 */
	public LinkedList<P> getNext(Filter<R> f);
	
	
//	public R getSilent();
	
	public Vector<P> getComponents();

}

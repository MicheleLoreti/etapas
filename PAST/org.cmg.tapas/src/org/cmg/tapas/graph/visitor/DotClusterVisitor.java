package org.cmg.tapas.graph.visitor;


import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.StateInterface;
import org.cmg.tapas.graph.algorithms.bisimulation.DecoratedClazz;

public class DotClusterVisitor <P extends StateInterface,
								R extends ActionInterface> {

	private PrintStream out;
	private HashMap<P, DecoratedClazz<P>> partitionMap;
	
	public DotClusterVisitor(PrintStream out
			, HashMap<P, DecoratedClazz<P>> partitionMap ) {
		this.partitionMap = partitionMap;
		this.out = out;
	}
	
	public void visit() {
		out.println("digraph G { \n ranksep=.75;\n rankdir=LR;\n");
		Collection<DecoratedClazz<P>> v = partitionMap.values();
		int i = 0;
		for (DecoratedClazz<P> dc : v) {
			out.println("subgraph cluster"+i+" {");
			for (StateInterface s : dc) {
				out.println("\t\""+s+"\";");
			}
			out.println("}\n");
			i++;
		}
		out.println("}\n");
	}
}

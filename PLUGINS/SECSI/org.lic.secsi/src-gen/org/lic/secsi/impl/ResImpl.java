/**
 */
package org.lic.secsi.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.lic.secsi.Channel;
import org.lic.secsi.Res;
import org.lic.secsi.SecsiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Res</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.lic.secsi.impl.ResImpl#getLeftlists <em>Leftlists</em>}</li>
 *   <li>{@link org.lic.secsi.impl.ResImpl#getLeflists <em>Leflists</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ResImpl extends MinimalEObjectImpl.Container implements Res
{
  /**
   * The cached value of the '{@link #getLeftlists() <em>Leftlists</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeftlists()
   * @generated
   * @ordered
   */
  protected Channel leftlists;

  /**
   * The cached value of the '{@link #getLeflists() <em>Leflists</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeflists()
   * @generated
   * @ordered
   */
  protected EList<Channel> leflists;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ResImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.RES;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Channel getLeftlists()
  {
    if (leftlists != null && leftlists.eIsProxy())
    {
      InternalEObject oldLeftlists = (InternalEObject)leftlists;
      leftlists = (Channel)eResolveProxy(oldLeftlists);
      if (leftlists != oldLeftlists)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, SecsiPackage.RES__LEFTLISTS, oldLeftlists, leftlists));
      }
    }
    return leftlists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Channel basicGetLeftlists()
  {
    return leftlists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLeftlists(Channel newLeftlists)
  {
    Channel oldLeftlists = leftlists;
    leftlists = newLeftlists;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SecsiPackage.RES__LEFTLISTS, oldLeftlists, leftlists));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Channel> getLeflists()
  {
    if (leflists == null)
    {
      leflists = new EObjectResolvingEList<Channel>(Channel.class, this, SecsiPackage.RES__LEFLISTS);
    }
    return leflists;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SecsiPackage.RES__LEFTLISTS:
        if (resolve) return getLeftlists();
        return basicGetLeftlists();
      case SecsiPackage.RES__LEFLISTS:
        return getLeflists();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SecsiPackage.RES__LEFTLISTS:
        setLeftlists((Channel)newValue);
        return;
      case SecsiPackage.RES__LEFLISTS:
        getLeflists().clear();
        getLeflists().addAll((Collection<? extends Channel>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.RES__LEFTLISTS:
        setLeftlists((Channel)null);
        return;
      case SecsiPackage.RES__LEFLISTS:
        getLeflists().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SecsiPackage.RES__LEFTLISTS:
        return leftlists != null;
      case SecsiPackage.RES__LEFLISTS:
        return leflists != null && !leflists.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ResImpl

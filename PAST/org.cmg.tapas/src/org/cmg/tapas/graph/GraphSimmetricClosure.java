package org.cmg.tapas.graph;

import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class GraphSimmetricClosure<S extends StateInterface, A extends ActionInterface> extends GraphClosure<S, A> {

	public GraphSimmetricClosure(GraphInterface<S,A> graph , A action) {
		super(graph, action);
	}
	

	/**
	 * Chiude, in modo simmetrico, l'insieme specificato 
	 * rispetto all'azione specificata.
	 **/
	protected void computeClosure(GraphInterface<S, A> g , Set<S> clazz) {
		LinkedList<S> queue = new LinkedList<S>(clazz);
		while( !queue.isEmpty() ) {
			S s = queue.remove();
			Set<S> postset = g.getPostset(s, action );
			if( postset!=null ){
				for( S post: postset ){
					Map<A, ? extends Set<S>> map = g.getPostsetMap(post);
					if(map != null){
						Set<A> k = map.keySet();
						if(k.size()>1 || !k.contains(action))
							if(clazz.add(post))
								queue.add(post);
					}
				}
			}

			Map<A,? extends Set<S>> map = g.getPostsetMap(s);
			if(map != null){
				Set<A> k = map.keySet();
				if(k.size()>1 || !k.contains(action)){
					Set<S> preset = g.getPreset(s, action );
					if(preset!=null){
						for(S pre: preset){
							if(clazz.add(pre))
								queue.add(pre);

						}
					}
				}
			}
		}
	}
}

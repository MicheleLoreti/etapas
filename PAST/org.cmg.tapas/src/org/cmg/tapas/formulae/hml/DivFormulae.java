package org.cmg.tapas.formulae.hml;


import java.util.LinkedList;

import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;

/**
 * @author calzolai
 *
 */
public class DivFormulae<X extends APProcess<X, Y>,Y extends ActionInterface>
				extends HmlFormula<X, Y> {
	
	private String toString = "DIV";
	private HmlFormula<X, Y> div;
	private Y tauAct;
	
	public DivFormulae(Y tauAct){
		this.tauAct= tauAct;
		div = divFormula();
	}
	
	private HmlFormula<X, Y> divFormula(){
		// max X.(<tau> true & [tau] X)
		String name = "X";
		FixPoint<X, Y> fix = new FixPoint<X, Y>(true, name);

		LinkedList<HmlFormula<X, Y>> subList = new LinkedList<HmlFormula<X, Y>>();

		HmlFormula<X, Y> f = new DiamondFormula<X, Y>(tauAct, new True<X, Y>());
		subList.add(f);
		f = new BoxFormula<X, Y>(tauAct, fix.getReference());
		subList.add(f);

		AndFormula<X, Y> and = new AndFormula<X, Y>(subList);
		fix.setSubformula(and);
		return fix;
	}
	

	@Override
	protected String _toString() {
		return toString;
	}

	@Override
	protected HmlFormula<X, Y> doClone() {
		return div.doClone();
	}

	@Override
	protected void doVisit(FormulaVisitor<X, Y> visitor) {
		div.doVisit(visitor);		
	}

	@Override
	protected boolean _equals(Object o) {
		return div.equals(o);
	}

	@Override
	public Proof<X, Y> getProof(X s) {
		return div.getProof(s);
	}

	@Override
	public String getUnicode() {
		return toString;
	}

	@Override
	public String operator() {
		return "";
	}

	@Override
	public boolean satisfies(X s) {
		return div.satisfies(s);
	}
}

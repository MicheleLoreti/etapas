package org.cmg.tapas.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Properties;

public class TapasOptions extends Properties {
	
	private static final String PATH = "/org/cmg/tapas/util/resources/";
	private static final String FILENAME = "TAPAs.properties";
	private static final String FILE = PATH+FILENAME;

	private File optFile;
	private static TapasOptions istance;
	
	private TapasOptions() {
	super();
		if (!load()) {
			store();
		}
	}
	
	public static TapasOptions getIstance() {
		if (istance == null) {
			istance = new TapasOptions();
		}
		return istance;
	}
	
	private void store(){
		try {
			if(optFile == null || !optFile.isFile()){
				String str = getClass().getClassLoader().getResource(PATH).getFile();
				new File(str).mkdirs();
				new File(str+FILENAME).createNewFile();
				optFile = new File(str+FILENAME);
			}
			
			FileOutputStream fos = new FileOutputStream(optFile);
			super.store(fos, "");
			fos.close();
		} catch(Exception e){
			// XXX Doesn't matter if fail
//			e.printStackTrace();
//			JOptionPane.showMessageDialog(null
//					, "Unable to find the properties file\""+FILE+"\". "
//					, "alert"
//					, JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private boolean load(){
		try {
			URL url = getClass().getResource(FILE);
		    optFile = new File(url.getFile());
			super.load(new FileInputStream(optFile));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public Object setProperty(String key, String value){
		Object o = super.setProperty(key, value);
		store();
		return o;
	}
		
	public String getProperty(String key, String defaultValue){
		String res = super.getProperty(key, defaultValue);
		if(res == null){
			store();
		}
		return res;
	}	
}

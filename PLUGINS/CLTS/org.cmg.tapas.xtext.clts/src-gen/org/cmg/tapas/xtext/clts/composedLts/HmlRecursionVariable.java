/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Recursion Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable#getRefeerence <em>Refeerence</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlRecursionVariable()
 * @model
 * @generated
 */
public interface HmlRecursionVariable extends HmlFormula
{
  /**
   * Returns the value of the '<em><b>Refeerence</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Refeerence</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Refeerence</em>' reference.
   * @see #setRefeerence(HmlRecursionFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlRecursionVariable_Refeerence()
   * @model
   * @generated
   */
  HmlRecursionFormula getRefeerence();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable#getRefeerence <em>Refeerence</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Refeerence</em>' reference.
   * @see #getRefeerence()
   * @generated
   */
  void setRefeerence(HmlRecursionFormula value);

} // HmlRecursionVariable

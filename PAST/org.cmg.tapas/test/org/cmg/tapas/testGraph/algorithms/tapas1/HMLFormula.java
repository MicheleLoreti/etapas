package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

/**
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class HMLFormula {
	
	private Action head = null;	
	private boolean not = false;	
	private Vector<HMLFormula> queue = new Vector<HMLFormula>();
	
	private Vector<StatesSet> couplesSatisfied = new Vector<StatesSet>();

	public HMLFormula(Action a, boolean not) {
		this.head = a;
		this.not = not;
	}

	public Action getHead() {
		return head;	
	}

	public Vector<HMLFormula> getQueue() {
		return queue;	
	}
	
	public boolean isNot() {
		return not;	
	}	
	
	public void setHead(Action a) {
		this.head = a;
	}	

	public void setNot(boolean not) {
		this.not = not;
	}	
	
	public void addQueue(HMLFormula formula) {
		queue.addElement(formula);
	}		
	
	public int getCouplesSatisfiedDimension() {
		return couplesSatisfied.size();
	}	
	
	public StatesSet getCoupleSatisfiedAt(int index) {
		return couplesSatisfied.elementAt(index);
	}	
	
	public void addCoupleSatisfied(State s1, State s2) {
		StatesSet ss = new StatesSet();
		ss.addState(s1);
		ss.addState(s2);
		couplesSatisfied.addElement(ss);
	}

	public void addCoupleSatisfied(StatesSet couple) {
		if( couple.getDimension()==2 )
			couplesSatisfied.addElement(couple);
	}
	
	public boolean containCoupleSatisfied(State s1, State s2) {		
		StatesSet couple = null;
		
		for( int i=0; i<getCouplesSatisfiedDimension(); i++ ) {
			couple = getCoupleSatisfiedAt(i);
			if( couple.containState(s1) )
				if( couple.containState(s2) )
					return true;
		}
		
		return false;
	}
	
	public boolean containCoupleSatisfied(StatesSet couple) {
		for( int i=0; i<getCouplesSatisfiedDimension(); i++ )
			if( getCoupleSatisfiedAt(i).equals(couple) ) 
				return true;
				
		return false;
	}	
	

	public boolean satisfy(State s, Lts p) {
		StatesSet candidates = s.dP(getHead());
		
		boolean trovato = false;
		for( int i=0; i<candidates.getDimension(); i++ ) {
			trovato = true;
			
			for( int j=0; j<getQueue().size(); j++ ) {
				if( !getQueue().elementAt(j).satisfy(candidates.stateAt(i),p) ) {
					trovato = false;
					break;
				}
			}
			
			if( trovato ) 
				break;
		}
		
		return isNot() != trovato;
	}


}
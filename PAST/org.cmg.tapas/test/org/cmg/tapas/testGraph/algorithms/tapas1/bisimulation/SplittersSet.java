package org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation;

import java.util.Vector;


/**
 * Insieme di Splitter.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class SplittersSet {
	
	private Vector<Splitter> splittersSet = null;
	
	public SplittersSet() {
		splittersSet = new Vector<Splitter>();	
	}
	
	public SplittersSet(Splitter initialSplitter) {
		splittersSet = new Vector<Splitter>();
		splittersSet.addElement(initialSplitter);
	}
	
	public int getDimension() {
		return splittersSet.size();	
	}

	public void addSplitter(Splitter sp) {
		splittersSet.addElement(sp);	
	}
	
	public Splitter splitterAt(int index) {
		return splittersSet.elementAt(index);	
	}
	
	public Splitter removeFirstSplitter() {
		return splittersSet.remove(0);	
	}	

	public Splitter removeSplitterAt(int index) {
		return splittersSet.remove(index);	
	}
	
	public boolean isEmpty() {
		return splittersSet.isEmpty();	
	}	

}
package org.cmg.tapas.util;

public class Debug {
	
	private boolean debug;
	private static Debug instance = null;
	
	private Debug(){
		debug = false;
	}
	
	public static Debug getDebug(){
		if(instance == null)
			instance = new Debug();
		
		return instance;
	}
	
	public void setDebug(boolean debug){
		this.debug = debug;
	}
	
	public boolean isDebug(){
		return debug;
	}

	public void notDebug() {
		debug = !debug;
	}

}

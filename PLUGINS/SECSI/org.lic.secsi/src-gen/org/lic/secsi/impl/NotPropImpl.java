/**
 */
package org.lic.secsi.impl;

import org.eclipse.emf.ecore.EClass;

import org.lic.secsi.NotProp;
import org.lic.secsi.SecsiPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Not Prop</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NotPropImpl extends PropImpl implements NotProp
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NotPropImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.NOT_PROP;
  }

} //NotPropImpl

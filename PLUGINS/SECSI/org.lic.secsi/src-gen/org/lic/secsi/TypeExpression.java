/**
 */
package org.lic.secsi;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.lic.secsi.SecsiPackage#getTypeExpression()
 * @model
 * @generated
 */
public interface TypeExpression extends EObject
{
} // TypeExpression

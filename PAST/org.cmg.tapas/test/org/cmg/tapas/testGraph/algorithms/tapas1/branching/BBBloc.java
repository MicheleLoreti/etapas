package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;

/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBBloc {
	
	private BBStatesSetNotBottom notBottom = null;	
	private BBStatesSetBottom bottom = null;	
	private In in = null;	
	private BBPartition listOwner = null;	
	private boolean mark = false;
	
	public BBBloc()	{
		notBottom = new BBStatesSetNotBottom();
		bottom = new BBStatesSetBottom();
		in = new In();
	}	

	public void addBBStateBottom(BBState s) {		
		bottom.addBBState(s);
		s.setOwner(this);
	}

	public void addBBStateNotBottom(BBState s) {
		notBottom.addBBState(s);
		s.setOwner(this);
	}

	public int addNotInertTransition(NotInertTransition t) {
		return in.addNotInertTransition(t);
	}
	
	public BBStatesSetBottom getBBStatesSetBottom() {
		return bottom;	
	}

	public BBStatesSetNotBottom getBBStatesSetNotBottom() {
		return notBottom;	
	}

	public In getIn() {
		return in;	
	}
	
	public BBPartition getListOwner() {
		return listOwner;	
	}

	public boolean equals(BBBloc bb) {
		return this == bb;
	}

	public BBState getBBState(String name) {
		BBState s = null;
		
		s = getBBStatesSetBottom().getBBState(name);
		if( s==null ) 
			s = getBBStatesSetNotBottom().getBBState(name);
		
		return s;
	}

	public boolean containState(State s) {
		if( getBBStatesSetBottom().containState(s) ) 
			return true;
		
		if( getBBStatesSetNotBottom().containState(s) ) 
			return true;
		
		return false;
	}

	public boolean isMark() {
		return mark;	
	}

	public void setMark(boolean mark) {
		this.mark = mark;	
	}
	
	public void setListOwner(BBPartition owner) {
		listOwner = owner;	
	}

	public void print() {
		printAll(System.out);
	}


	public void printAll(java.io.PrintStream console) {
		console.append("  Bloc \n");
		
		console.append("    |-- Bottom: ");
		getBBStatesSetBottom().printAll(console);
		
		console.append("\n   |-- NotBottom: ");
		getBBStatesSetNotBottom().printAll(console);
		
		console.append("\n   |-- In: ");
		getIn().print(console);
	}  
}
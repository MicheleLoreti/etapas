package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.State;


/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class BBStatesSetNotBottom {
	
	private Vector<BBState> bbstates = null;
	
	public BBStatesSetNotBottom() {
		bbstates = new Vector<BBState>();
  	}
  	
  	public int getDimension() {
  		return bbstates.size();
  	}

	public boolean addBBState(BBState s) {	
		BBState current;	
		
		for( int i=0; i<getDimension(); i++ ) {
			current = bbstateAt(i);
			if( current==s ) {
				return false;
			}	
			if( current.getState().sEpsilon().containState(s.getState()) ) {
				bbstates.add(i,s);		
				return true;			
			}			
//			if( current.getInert().containInertTransition(s) ){
//				bbstates.add(i,s);		
//				return true;			
//			}			
		}
		
		bbstates.add(s);
		return true;
	}
	

	public BBState getBBState(String name) {
		BBState s;
		for( int i=0; i<getDimension(); i++ ) {
			s = bbstateAt(i);
			if( s.getState().getName().equals(name) ) 
				return s;
		}		
		return null;
	}

	public boolean containState(State s) {
		for( int i=0; i<getDimension(); i++ )	
			if( bbstateAt(i).getState()==s ) 
				return true;
				
		return false;
	}

	public BBState bbstateAt(int index) {
		try {
			return bbstates.elementAt(index);
		}	
		catch(ArrayIndexOutOfBoundsException unused) {
			return null;
		}
	}

	public boolean isEmpty() {
		return getDimension() == 0;
	}

	public void print() {
		printAll(System.out);
	}

	public void printAll(java.io.PrintStream console) {
		int i = 0;
		
		for( i=0; i<this.getDimension(); i++ ) {
			if(i>0) 
				console.append("\n                  ");
			bbstateAt(i).printAll(console);
		}
		
		if(i==0) 
			console.append("empty");
	}	
}
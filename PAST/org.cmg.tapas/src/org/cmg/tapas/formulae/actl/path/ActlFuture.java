package org.cmg.tapas.formulae.actl.path;


import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlPathFormulaVisitor;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.formulae.actl.action.ActlAllActions;
import org.cmg.tapas.formulae.actl.state.ActlTrue;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


public class ActlFuture<X extends APProcess<X ,Y>,Y extends ActionInterface> 
				extends PathFormula<X, Y>{
	
	private ActlUntil<X, Y> equivalent;	
	private ActlActionPredicate<X,Y> filter;
	
	public ActlFuture (ActlActionPredicate<X,Y> filter, ActlFormula<X, Y> formula){
		//Equivalent: F phi = true {true} U phi
		//Equivalent: F{filter} phi = true {filter} U phi
		if(filter == null)
			this.filter = new ActlAllActions<X, Y>();
		else
			this.filter = filter;
		ActlFormula<X, Y> formulaTrue = new ActlTrue<X, Y>();
		equivalent = new ActlUntil<X, Y>(formulaTrue, this.filter, formula);
	}

	@Override
	protected String _toString() {
		String tmp = " ";
		if (!(filter instanceof ActlAllActions)) 
			tmp =  " {"+filter.toString()+"} ";
		
		return this.operator()+tmp+equivalent.getFormula2().toString();
	}

	@Override
	protected PathFormula<X, Y> doClone() {
		return new ActlFuture<X, Y>(equivalent.getActionPredicate1(), equivalent.getFormula2().clone());
	}

	@Override
	public String getUnicode() {
		return this.operator()+" {"+filter.toString()+"} "
					+equivalent.getFormula2().getUnicode();
	}
	
	public ActlFormula<X, Y> getFormula() {
		return equivalent.getFormula2();
	}
	
	public ActlActionPredicate<X,Y> getFilter(){
		return filter;
	}

	@Override
	public String operator() {
		return "F";
	}

	@Override
	public boolean sat(X x, boolean forall) {
		return equivalent.sat(x, forall);
	}

	@Override
	protected void doVisit(ActlPathFormulaVisitor<X, Y> visitor) {
		visitor.visitFeaturesFormula(this);
	}
}

/**
 * 
 */
package org.cmg.tapas.formulae.actl;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface ActlActionPredicate<S extends APProcess<S, A>, A extends ActionInterface> {
	
	public Filter<A> getFilter();
	public void acceptVisitor( ActlActionPredicateVisitor<S,A> v );
	
}

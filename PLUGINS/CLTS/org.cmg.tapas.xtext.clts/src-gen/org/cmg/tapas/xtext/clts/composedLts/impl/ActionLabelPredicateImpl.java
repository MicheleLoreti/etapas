/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.Action;
import org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.ActionLabelPredicateImpl#getAct <em>Act</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionLabelPredicateImpl extends LabelPredicateImpl implements ActionLabelPredicate
{
  /**
   * The cached value of the '{@link #getAct() <em>Act</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAct()
   * @generated
   * @ordered
   */
  protected Action act;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ActionLabelPredicateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.ACTION_LABEL_PREDICATE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action getAct()
  {
    if (act != null && act.eIsProxy())
    {
      InternalEObject oldAct = (InternalEObject)act;
      act = (Action)eResolveProxy(oldAct);
      if (act != oldAct)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComposedLtsPackage.ACTION_LABEL_PREDICATE__ACT, oldAct, act));
      }
    }
    return act;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Action basicGetAct()
  {
    return act;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAct(Action newAct)
  {
    Action oldAct = act;
    act = newAct;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.ACTION_LABEL_PREDICATE__ACT, oldAct, act));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.ACTION_LABEL_PREDICATE__ACT:
        if (resolve) return getAct();
        return basicGetAct();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.ACTION_LABEL_PREDICATE__ACT:
        setAct((Action)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.ACTION_LABEL_PREDICATE__ACT:
        setAct((Action)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.ACTION_LABEL_PREDICATE__ACT:
        return act != null;
    }
    return super.eIsSet(featureID);
  }

} //ActionLabelPredicateImpl

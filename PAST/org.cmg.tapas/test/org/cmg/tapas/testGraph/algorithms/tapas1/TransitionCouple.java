package org.cmg.tapas.testGraph.algorithms.tapas1;

/**
 * La classe TransitionCouple rappresenta  
 * una coppia formata da un'azione e da un insieme di stati
 * raggiungibili con tale azione.
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class TransitionCouple {
	
	/**
	 * L'azione con cui sono etichettate le transizioni
	 **/	
	private Action action = null;

	/**
	 * L'insieme di stati destinazione delle transizioni
	 **/		
	private StatesSet destinationsSet = null;
	
	/**
	 * Costruisce una nuova coppia.
	 **/	
	public TransitionCouple(Action action) {
		this.action = action;
		destinationsSet = new StatesSet();	
	}

	/**
	 * Restituisce l'azione che etichetta tutte le transizioni.
	 **/	
	public Action getAction() {
		return action;	
	}

	/**
	 * Restituisce l'insieme di stati di destinazione delle transizioni.
	 **/		
	public StatesSet getDestinationsSet() {
		return destinationsSet;	
	}

	/**
	 * Aggiunge all'insieme degli stati di destinazione un nuovo oggetto.
	 * <p>
	 * Il metodo restituisce un valore booleano che � vero se l'insieme 
	 * degli stati di destinazione non contiene gi� uno stato 
	 * con lo stesso nome.
	 **/	
	public boolean addDestination(State s) {
		int initialDim = destinationsSet.getDimension();
		destinationsSet.addState(s);
		return destinationsSet.getDimension() != initialDim;
	}

	/**
	 * Rimuove dall'insieme degli stati di destinazione lo stato il cui
	 * nome coincide con la stringa passata come parametro.
	 **/	
	public boolean removeDestination(State s) {
		return this.destinationsSet.removeState(s.getName());
	}

}
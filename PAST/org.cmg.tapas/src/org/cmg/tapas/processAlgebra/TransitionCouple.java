/*********************************************************************
  * FILE:  TransitionCouple.java                                      *
  *                                                                   *
  * Copyright (C) 2006 Francesco Calzolai                             *
  *                                                                   *
  * This is free software; you can redistribute it and/or             *
  * modify it under the terms of the GNU General Public License       *
  * as published by the Free Software Foundation; either version 2    *
  * of the License, or (at your option) any later version.            *
  *                                                                   *
  * This software is distributed in the hope that it will be useful,  *
  * but WITHOUT ANY WARRANTY; without even the implied warranty of    *
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     *
  * GNU General Public License for more details.                      *
  *                                                                   *
  * You should have received a copy of the GNU General Public License *
  * along with JAgent; if not, write to the Free Software             *
  * Foundation, Inc., 51 Franklin St, Fifth Floor,                    *
  * Boston, MA  02110-1301  USA                                       *
  *                                                                   *
  *********************************************************************/
package org.cmg.tapas.processAlgebra;

import org.cmg.tapas.graph.ActionInterface;

/** La classe viene usata come contenitore dai metodi 
 * dell'interfaccia <i>APProcess</i> per restituire coppie del 
 * tipo:<br>
 * < azione, destinazione >
 * <br> 
 * Questa classe � parametrica ripsetto al concetto di processo ed
 *  a quello di azione.
 *  
 * @param <R> La definizione di azione.
 * @param <P> La definizione di processo.
 *  
 * @author Francesco Calzolai
 * @version 1.0 - 16 Dic 2005
 */

public class TransitionCouple<R extends ActionInterface, P extends APProcess> {
	
	
	//TODO: rendere i parametri read-only eliminando i metodi set
	private P dest;
	
	private R action;
	
	
	/** Dati un'azione <i>a</i>e un processo <i>d</i>, costruisce la
	 *  <i>TransitionCouple</i> corrispondente. 
	 * @param a L'azione.
	 * @param d Il processo
	 */
	public TransitionCouple(R a, P d){
		dest = d;
		action = a;
	}
		
	
	/**Restituisce l'azione.
	 * @return l'azione.
	 */
	public R getAction() {
		return action;
	}


	/**Setta l'azione con <i>action</i>.
	 * @param action L'azione.
	 */
	public void setAction(R action) {
		this.action = action;
	}


	/**Restituisce il processo destinazione.
	 * @return Il processo destinazione.
	 */
	public P getDest() {
		return dest;
	}


	/**Setta la destinazione a <i>dest</i>.
	 * @param dest Il processo destinazione.
	 */
	public void setDest(P dest) {
		this.dest = dest;
	}
}

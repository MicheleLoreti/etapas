/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.Label;
import org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ltl Atomic Proposition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAtomicPropositionImpl#getProp <em>Prop</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LtlAtomicPropositionImpl extends LtlFormulaImpl implements LtlAtomicProposition
{
  /**
   * The cached value of the '{@link #getProp() <em>Prop</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProp()
   * @generated
   * @ordered
   */
  protected Label prop;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LtlAtomicPropositionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.LTL_ATOMIC_PROPOSITION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Label getProp()
  {
    if (prop != null && prop.eIsProxy())
    {
      InternalEObject oldProp = (InternalEObject)prop;
      prop = (Label)eResolveProxy(oldProp);
      if (prop != oldProp)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComposedLtsPackage.LTL_ATOMIC_PROPOSITION__PROP, oldProp, prop));
      }
    }
    return prop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Label basicGetProp()
  {
    return prop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProp(Label newProp)
  {
    Label oldProp = prop;
    prop = newProp;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.LTL_ATOMIC_PROPOSITION__PROP, oldProp, prop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTL_ATOMIC_PROPOSITION__PROP:
        if (resolve) return getProp();
        return basicGetProp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTL_ATOMIC_PROPOSITION__PROP:
        setProp((Label)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTL_ATOMIC_PROPOSITION__PROP:
        setProp((Label)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTL_ATOMIC_PROPOSITION__PROP:
        return prop != null;
    }
    return super.eIsSet(featureID);
  }

} //LtlAtomicPropositionImpl

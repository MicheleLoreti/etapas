/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.HmlMinFixPoint;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hml Min Fix Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HmlMinFixPointImpl extends HmlRecursionFormulaImpl implements HmlMinFixPoint
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected HmlMinFixPointImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.HML_MIN_FIX_POINT;
  }

} //HmlMinFixPointImpl

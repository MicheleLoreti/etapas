/**
 */
package org.lic.secsi;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.lic.secsi.SecsiFactory
 * @model kind="package"
 * @generated
 */
public interface SecsiPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "secsi";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.lic.org/Secsi";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "secsi";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SecsiPackage eINSTANCE = org.lic.secsi.impl.SecsiPackageImpl.init();

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ProgramImpl <em>Program</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ProgramImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getProgram()
   * @generated
   */
  int PROGRAM = 0;

  /**
   * The feature id for the '<em><b>Globalchanels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__GLOBALCHANELS = 0;

  /**
   * The feature id for the '<em><b>Agents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__AGENTS = 1;

  /**
   * The feature id for the '<em><b>Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__TYPES = 2;

  /**
   * The feature id for the '<em><b>Consts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__CONSTS = 3;

  /**
   * The feature id for the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__SYSTEM = 4;

  /**
   * The feature id for the '<em><b>Properties</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM__PROPERTIES = 5;

  /**
   * The number of structural features of the '<em>Program</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROGRAM_FEATURE_COUNT = 6;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ReferenceableElementImpl <em>Referenceable Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ReferenceableElementImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getReferenceableElement()
   * @generated
   */
  int REFERENCEABLE_ELEMENT = 29;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCEABLE_ELEMENT__NAME = 0;

  /**
   * The number of structural features of the '<em>Referenceable Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCEABLE_ELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ConstantImpl <em>Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ConstantImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getConstant()
   * @generated
   */
  int CONSTANT = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__NAME = REFERENCEABLE_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__TYPE = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT__VALUE = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_FEATURE_COUNT = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.AgentImpl <em>Agent</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.AgentImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getAgent()
   * @generated
   */
  int AGENT = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AGENT__NAME = 0;

  /**
   * The feature id for the '<em><b>Params</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AGENT__PARAMS = 1;

  /**
   * The feature id for the '<em><b>Lc Decs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AGENT__LC_DECS = 2;

  /**
   * The feature id for the '<em><b>S</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AGENT__S = 3;

  /**
   * The number of structural features of the '<em>Agent</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AGENT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ChannelsDeclarationImpl <em>Channels Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ChannelsDeclarationImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getChannelsDeclaration()
   * @generated
   */
  int CHANNELS_DECLARATION = 3;

  /**
   * The feature id for the '<em><b>Declared</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNELS_DECLARATION__DECLARED = 0;

  /**
   * The number of structural features of the '<em>Channels Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNELS_DECLARATION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ChannelImpl <em>Channel</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ChannelImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getChannel()
   * @generated
   */
  int CHANNEL = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL__NAME = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL__TYPE = 1;

  /**
   * The number of structural features of the '<em>Channel</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHANNEL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.TypeImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getType()
   * @generated
   */
  int TYPE = 5;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.TypeExpressionImpl <em>Type Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.TypeExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeExpression()
   * @generated
   */
  int TYPE_EXPRESSION = 6;

  /**
   * The number of structural features of the '<em>Type Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.IntegerTypeImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getIntegerType()
   * @generated
   */
  int INTEGER_TYPE = 7;

  /**
   * The feature id for the '<em><b>Min</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE__MIN = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Max</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE__MAX = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Integer Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.BooleanTypeImpl <em>Boolean Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.BooleanTypeImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getBooleanType()
   * @generated
   */
  int BOOLEAN_TYPE = 8;

  /**
   * The number of structural features of the '<em>Boolean Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_TYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.TypeReferenceImpl <em>Type Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.TypeReferenceImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeReference()
   * @generated
   */
  int TYPE_REFERENCE = 9;

  /**
   * The feature id for the '<em><b>Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE__REFERENCE = TYPE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_REFERENCE_FEATURE_COUNT = TYPE_EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.StatementImpl <em>Statement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.StatementImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getStatement()
   * @generated
   */
  int STATEMENT = 11;

  /**
   * The number of structural features of the '<em>Statement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.BlockImpl <em>Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.BlockImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getBlock()
   * @generated
   */
  int BLOCK = 10;

  /**
   * The feature id for the '<em><b>Cmds</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLOCK__CMDS = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLOCK_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ActionImpl <em>Action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ActionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getAction()
   * @generated
   */
  int ACTION = 12;

  /**
   * The feature id for the '<em><b>Channel</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__CHANNEL = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Modality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__MODALITY = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Not Signal</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__NOT_SIGNAL = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>E</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__E = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.Switch_CaseImpl <em>Switch Case</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.Switch_CaseImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getSwitch_Case()
   * @generated
   */
  int SWITCH_CASE = 13;

  /**
   * The feature id for the '<em><b>Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_CASE__VALUE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Cases</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_CASE__CASES = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Default</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_CASE__DEFAULT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Cmd</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_CASE__CMD = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Switch Case</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_CASE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.IfThenElseImpl <em>If Then Else</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.IfThenElseImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getIfThenElse()
   * @generated
   */
  int IF_THEN_ELSE = 14;

  /**
   * The feature id for the '<em><b>Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_THEN_ELSE__EXPRESSION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Then Branch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_THEN_ELSE__THEN_BRANCH = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>With Else</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_THEN_ELSE__WITH_ELSE = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Else Branch</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_THEN_ELSE__ELSE_BRANCH = STATEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>If Then Else</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_THEN_ELSE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.WhileImpl <em>While</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.WhileImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getWhile()
   * @generated
   */
  int WHILE = 15;

  /**
   * The feature id for the '<em><b>Expresion</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE__EXPRESION = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE__BODY = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>While</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.SystemImpl <em>System</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.SystemImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getSystem()
   * @generated
   */
  int SYSTEM = 16;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYSTEM__BODY = 0;

  /**
   * The number of structural features of the '<em>System</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYSTEM_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ParImpl <em>Par</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ParImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getPar()
   * @generated
   */
  int PAR = 17;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PAR__ACTION = 0;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PAR__LEFT = 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PAR__RIGHT = 2;

  /**
   * The number of structural features of the '<em>Par</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PAR_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.SumImpl <em>Sum</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.SumImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getSum()
   * @generated
   */
  int SUM = 18;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM__ACTION = 0;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM__LEFT = 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM__RIGHT = 2;

  /**
   * The number of structural features of the '<em>Sum</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ProcImpl <em>Proc</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ProcImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getProc()
   * @generated
   */
  int PROC = 19;

  /**
   * The feature id for the '<em><b>Name</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__NAME = 0;

  /**
   * The feature id for the '<em><b>Name1</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__NAME1 = 1;

  /**
   * The feature id for the '<em><b>Re</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC__RE = 2;

  /**
   * The number of structural features of the '<em>Proc</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROC_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.RenImpl <em>Ren</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.RenImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getRen()
   * @generated
   */
  int REN = 20;

  /**
   * The feature id for the '<em><b>Leflists</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REN__LEFLISTS = 0;

  /**
   * The feature id for the '<em><b>Rightlist</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REN__RIGHTLIST = 1;

  /**
   * The feature id for the '<em><b>R</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REN__R = 2;

  /**
   * The number of structural features of the '<em>Ren</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REN_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ResImpl <em>Res</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ResImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getRes()
   * @generated
   */
  int RES = 21;

  /**
   * The feature id for the '<em><b>Leftlists</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RES__LEFTLISTS = 0;

  /**
   * The feature id for the '<em><b>Leflists</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RES__LEFLISTS = 1;

  /**
   * The number of structural features of the '<em>Res</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RES_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.PropListImpl <em>Prop List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.PropListImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getPropList()
   * @generated
   */
  int PROP_LIST = 22;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_LIST__ELEMENT = 0;

  /**
   * The number of structural features of the '<em>Prop List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.PropImpl <em>Prop</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.PropImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getProp()
   * @generated
   */
  int PROP = 23;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP__ARG = 0;

  /**
   * The number of structural features of the '<em>Prop</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.PropBodyImpl <em>Prop Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.PropBodyImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getPropBody()
   * @generated
   */
  int PROP_BODY = 24;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_BODY__ARG = PROP__ARG;

  /**
   * The feature id for the '<em><b>Action</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_BODY__ACTION = PROP_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_BODY__EXP = PROP_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Prop Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROP_BODY_FEATURE_COUNT = PROP_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.NotPropImpl <em>Not Prop</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.NotPropImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getNotProp()
   * @generated
   */
  int NOT_PROP = 25;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_PROP__ARG = PROP__ARG;

  /**
   * The number of structural features of the '<em>Not Prop</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_PROP_FEATURE_COUNT = PROP_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 26;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.NegationImpl <em>Negation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.NegationImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getNegation()
   * @generated
   */
  int NEGATION = 27;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEGATION__ARG = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Negation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NEGATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.ReferenceInExpressionImpl <em>Reference In Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.ReferenceInExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getReferenceInExpression()
   * @generated
   */
  int REFERENCE_IN_EXPRESSION = 28;

  /**
   * The feature id for the '<em><b>Reference</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_IN_EXPRESSION__REFERENCE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Reference In Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_IN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.TypeDefImpl <em>Type Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.TypeDefImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeDef()
   * @generated
   */
  int TYPE_DEF = 30;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF__NAME = TYPE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF__ELEMENTS = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Type Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_DEF_FEATURE_COUNT = TYPE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.TypeElementImpl <em>Type Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.TypeElementImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeElement()
   * @generated
   */
  int TYPE_ELEMENT = 31;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ELEMENT__NAME = REFERENCEABLE_ELEMENT__NAME;

  /**
   * The number of structural features of the '<em>Type Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_ELEMENT_FEATURE_COUNT = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.VariableImpl <em>Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.VariableImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getVariable()
   * @generated
   */
  int VARIABLE = 32;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__NAME = REFERENCEABLE_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE__TYPE = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_FEATURE_COUNT = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.IntConstantImpl <em>Int Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.IntConstantImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getIntConstant()
   * @generated
   */
  int INT_CONSTANT = 33;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_CONSTANT__VALUE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Int Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INT_CONSTANT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.BoolConstantImpl <em>Bool Constant</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.BoolConstantImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getBoolConstant()
   * @generated
   */
  int BOOL_CONSTANT = 34;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_CONSTANT__VALUE = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Bool Constant</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOL_CONSTANT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.VarDeclarationImpl <em>Var Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.VarDeclarationImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getVarDeclaration()
   * @generated
   */
  int VAR_DECLARATION = 35;

  /**
   * The feature id for the '<em><b>Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_DECLARATION__VARIABLE = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Init</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_DECLARATION__INIT = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Var Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VAR_DECLARATION_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.AssignmentImpl <em>Assignment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.AssignmentImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getAssignment()
   * @generated
   */
  int ASSIGNMENT = 36;

  /**
   * The feature id for the '<em><b>Id</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT__ID = STATEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>In</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT__IN = STATEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Assignment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ASSIGNMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.SignalImpl <em>Signal</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.SignalImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getSignal()
   * @generated
   */
  int SIGNAL = 37;

  /**
   * The number of structural features of the '<em>Signal</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIGNAL_FEATURE_COUNT = TYPE_EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.OrPropImpl <em>Or Prop</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.OrPropImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getOrProp()
   * @generated
   */
  int OR_PROP = 38;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP__ARG = PROP_BODY__ARG;

  /**
   * The feature id for the '<em><b>Action</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP__ACTION = PROP_BODY__ACTION;

  /**
   * The feature id for the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP__EXP = PROP_BODY__EXP;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP__LEFT = PROP_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP__OP = PROP_BODY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP__RIGHT = PROP_BODY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Or Prop</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_PROP_FEATURE_COUNT = PROP_BODY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.OrExpressionImpl <em>Or Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.OrExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getOrExpression()
   * @generated
   */
  int OR_EXPRESSION = 39;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Or Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OR_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.AndExpressionImpl <em>And Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.AndExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getAndExpression()
   * @generated
   */
  int AND_EXPRESSION = 40;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>And Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int AND_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.RelationImpl <em>Relation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.RelationImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getRelation()
   * @generated
   */
  int RELATION = 41;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__OP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Relation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.SumExpressionImpl <em>Sum Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.SumExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getSumExpression()
   * @generated
   */
  int SUM_EXPRESSION = 42;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Sum Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUM_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.impl.MulExpressionImpl <em>Mul Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.impl.MulExpressionImpl
   * @see org.lic.secsi.impl.SecsiPackageImpl#getMulExpression()
   * @generated
   */
  int MUL_EXPRESSION = 43;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Mul Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.lic.secsi.ActionModality <em>Action Modality</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.ActionModality
   * @see org.lic.secsi.impl.SecsiPackageImpl#getActionModality()
   * @generated
   */
  int ACTION_MODALITY = 44;

  /**
   * The meta object id for the '{@link org.lic.secsi.BoolValue <em>Bool Value</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.BoolValue
   * @see org.lic.secsi.impl.SecsiPackageImpl#getBoolValue()
   * @generated
   */
  int BOOL_VALUE = 45;

  /**
   * The meta object id for the '{@link org.lic.secsi.Relop <em>Relop</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.lic.secsi.Relop
   * @see org.lic.secsi.impl.SecsiPackageImpl#getRelop()
   * @generated
   */
  int RELOP = 46;


  /**
   * Returns the meta object for class '{@link org.lic.secsi.Program <em>Program</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Program</em>'.
   * @see org.lic.secsi.Program
   * @generated
   */
  EClass getProgram();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Program#getGlobalchanels <em>Globalchanels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Globalchanels</em>'.
   * @see org.lic.secsi.Program#getGlobalchanels()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Globalchanels();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Program#getAgents <em>Agents</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Agents</em>'.
   * @see org.lic.secsi.Program#getAgents()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Agents();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Program#getTypes <em>Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Types</em>'.
   * @see org.lic.secsi.Program#getTypes()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Types();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Program#getConsts <em>Consts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Consts</em>'.
   * @see org.lic.secsi.Program#getConsts()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Consts();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Program#getSystem <em>System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>System</em>'.
   * @see org.lic.secsi.Program#getSystem()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_System();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Program#getProperties <em>Properties</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Properties</em>'.
   * @see org.lic.secsi.Program#getProperties()
   * @see #getProgram()
   * @generated
   */
  EReference getProgram_Properties();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Constant <em>Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant</em>'.
   * @see org.lic.secsi.Constant
   * @generated
   */
  EClass getConstant();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Constant#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.lic.secsi.Constant#getType()
   * @see #getConstant()
   * @generated
   */
  EReference getConstant_Type();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Constant#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.lic.secsi.Constant#getValue()
   * @see #getConstant()
   * @generated
   */
  EReference getConstant_Value();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Agent <em>Agent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Agent</em>'.
   * @see org.lic.secsi.Agent
   * @generated
   */
  EClass getAgent();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.Agent#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.lic.secsi.Agent#getName()
   * @see #getAgent()
   * @generated
   */
  EAttribute getAgent_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Agent#getParams <em>Params</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Params</em>'.
   * @see org.lic.secsi.Agent#getParams()
   * @see #getAgent()
   * @generated
   */
  EReference getAgent_Params();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Agent#getLcDecs <em>Lc Decs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Lc Decs</em>'.
   * @see org.lic.secsi.Agent#getLcDecs()
   * @see #getAgent()
   * @generated
   */
  EReference getAgent_LcDecs();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Agent#getS <em>S</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>S</em>'.
   * @see org.lic.secsi.Agent#getS()
   * @see #getAgent()
   * @generated
   */
  EReference getAgent_S();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.ChannelsDeclaration <em>Channels Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Channels Declaration</em>'.
   * @see org.lic.secsi.ChannelsDeclaration
   * @generated
   */
  EClass getChannelsDeclaration();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.ChannelsDeclaration#getDeclared <em>Declared</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Declared</em>'.
   * @see org.lic.secsi.ChannelsDeclaration#getDeclared()
   * @see #getChannelsDeclaration()
   * @generated
   */
  EReference getChannelsDeclaration_Declared();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Channel <em>Channel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Channel</em>'.
   * @see org.lic.secsi.Channel
   * @generated
   */
  EClass getChannel();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.Channel#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.lic.secsi.Channel#getName()
   * @see #getChannel()
   * @generated
   */
  EAttribute getChannel_Name();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Channel#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.lic.secsi.Channel#getType()
   * @see #getChannel()
   * @generated
   */
  EReference getChannel_Type();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see org.lic.secsi.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.TypeExpression <em>Type Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Expression</em>'.
   * @see org.lic.secsi.TypeExpression
   * @generated
   */
  EClass getTypeExpression();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.IntegerType <em>Integer Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Type</em>'.
   * @see org.lic.secsi.IntegerType
   * @generated
   */
  EClass getIntegerType();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.IntegerType#getMin <em>Min</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Min</em>'.
   * @see org.lic.secsi.IntegerType#getMin()
   * @see #getIntegerType()
   * @generated
   */
  EReference getIntegerType_Min();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.IntegerType#getMax <em>Max</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Max</em>'.
   * @see org.lic.secsi.IntegerType#getMax()
   * @see #getIntegerType()
   * @generated
   */
  EReference getIntegerType_Max();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.BooleanType <em>Boolean Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Type</em>'.
   * @see org.lic.secsi.BooleanType
   * @generated
   */
  EClass getBooleanType();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.TypeReference <em>Type Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Reference</em>'.
   * @see org.lic.secsi.TypeReference
   * @generated
   */
  EClass getTypeReference();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.TypeReference#getReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Reference</em>'.
   * @see org.lic.secsi.TypeReference#getReference()
   * @see #getTypeReference()
   * @generated
   */
  EReference getTypeReference_Reference();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Block <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Block</em>'.
   * @see org.lic.secsi.Block
   * @generated
   */
  EClass getBlock();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Block#getCmds <em>Cmds</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Cmds</em>'.
   * @see org.lic.secsi.Block#getCmds()
   * @see #getBlock()
   * @generated
   */
  EReference getBlock_Cmds();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Statement <em>Statement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Statement</em>'.
   * @see org.lic.secsi.Statement
   * @generated
   */
  EClass getStatement();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action</em>'.
   * @see org.lic.secsi.Action
   * @generated
   */
  EClass getAction();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.Action#getChannel <em>Channel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Channel</em>'.
   * @see org.lic.secsi.Action#getChannel()
   * @see #getAction()
   * @generated
   */
  EReference getAction_Channel();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.Action#getModality <em>Modality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Modality</em>'.
   * @see org.lic.secsi.Action#getModality()
   * @see #getAction()
   * @generated
   */
  EAttribute getAction_Modality();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.Action#isNotSignal <em>Not Signal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Not Signal</em>'.
   * @see org.lic.secsi.Action#isNotSignal()
   * @see #getAction()
   * @generated
   */
  EAttribute getAction_NotSignal();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Action#getE <em>E</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>E</em>'.
   * @see org.lic.secsi.Action#getE()
   * @see #getAction()
   * @generated
   */
  EReference getAction_E();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Switch_Case <em>Switch Case</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Switch Case</em>'.
   * @see org.lic.secsi.Switch_Case
   * @generated
   */
  EClass getSwitch_Case();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Switch_Case#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Value</em>'.
   * @see org.lic.secsi.Switch_Case#getValue()
   * @see #getSwitch_Case()
   * @generated
   */
  EReference getSwitch_Case_Value();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.Switch_Case#getCases <em>Cases</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Cases</em>'.
   * @see org.lic.secsi.Switch_Case#getCases()
   * @see #getSwitch_Case()
   * @generated
   */
  EReference getSwitch_Case_Cases();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Switch_Case#getDefault <em>Default</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Default</em>'.
   * @see org.lic.secsi.Switch_Case#getDefault()
   * @see #getSwitch_Case()
   * @generated
   */
  EReference getSwitch_Case_Default();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Switch_Case#getCmd <em>Cmd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cmd</em>'.
   * @see org.lic.secsi.Switch_Case#getCmd()
   * @see #getSwitch_Case()
   * @generated
   */
  EReference getSwitch_Case_Cmd();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.IfThenElse <em>If Then Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Then Else</em>'.
   * @see org.lic.secsi.IfThenElse
   * @generated
   */
  EClass getIfThenElse();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.IfThenElse#getExpression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expression</em>'.
   * @see org.lic.secsi.IfThenElse#getExpression()
   * @see #getIfThenElse()
   * @generated
   */
  EReference getIfThenElse_Expression();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.IfThenElse#getThenBranch <em>Then Branch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Then Branch</em>'.
   * @see org.lic.secsi.IfThenElse#getThenBranch()
   * @see #getIfThenElse()
   * @generated
   */
  EReference getIfThenElse_ThenBranch();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.IfThenElse#isWithElse <em>With Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>With Else</em>'.
   * @see org.lic.secsi.IfThenElse#isWithElse()
   * @see #getIfThenElse()
   * @generated
   */
  EAttribute getIfThenElse_WithElse();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.IfThenElse#getElseBranch <em>Else Branch</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Else Branch</em>'.
   * @see org.lic.secsi.IfThenElse#getElseBranch()
   * @see #getIfThenElse()
   * @generated
   */
  EReference getIfThenElse_ElseBranch();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.While <em>While</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>While</em>'.
   * @see org.lic.secsi.While
   * @generated
   */
  EClass getWhile();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.While#getExpresion <em>Expresion</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Expresion</em>'.
   * @see org.lic.secsi.While#getExpresion()
   * @see #getWhile()
   * @generated
   */
  EReference getWhile_Expresion();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.While#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see org.lic.secsi.While#getBody()
   * @see #getWhile()
   * @generated
   */
  EReference getWhile_Body();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.System <em>System</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>System</em>'.
   * @see org.lic.secsi.System
   * @generated
   */
  EClass getSystem();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.System#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see org.lic.secsi.System#getBody()
   * @see #getSystem()
   * @generated
   */
  EReference getSystem_Body();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Par <em>Par</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Par</em>'.
   * @see org.lic.secsi.Par
   * @generated
   */
  EClass getPar();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Par#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Action</em>'.
   * @see org.lic.secsi.Par#getAction()
   * @see #getPar()
   * @generated
   */
  EReference getPar_Action();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Par#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.Par#getLeft()
   * @see #getPar()
   * @generated
   */
  EReference getPar_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Par#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.Par#getRight()
   * @see #getPar()
   * @generated
   */
  EReference getPar_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Sum <em>Sum</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sum</em>'.
   * @see org.lic.secsi.Sum
   * @generated
   */
  EClass getSum();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Sum#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Action</em>'.
   * @see org.lic.secsi.Sum#getAction()
   * @see #getSum()
   * @generated
   */
  EReference getSum_Action();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Sum#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.Sum#getLeft()
   * @see #getSum()
   * @generated
   */
  EReference getSum_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Sum#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.Sum#getRight()
   * @see #getSum()
   * @generated
   */
  EReference getSum_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Proc <em>Proc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Proc</em>'.
   * @see org.lic.secsi.Proc
   * @generated
   */
  EClass getProc();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.Proc#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name</em>'.
   * @see org.lic.secsi.Proc#getName()
   * @see #getProc()
   * @generated
   */
  EReference getProc_Name();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.Proc#getName1 <em>Name1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Name1</em>'.
   * @see org.lic.secsi.Proc#getName1()
   * @see #getProc()
   * @generated
   */
  EReference getProc_Name1();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Proc#getRe <em>Re</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Re</em>'.
   * @see org.lic.secsi.Proc#getRe()
   * @see #getProc()
   * @generated
   */
  EReference getProc_Re();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Ren <em>Ren</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ren</em>'.
   * @see org.lic.secsi.Ren
   * @generated
   */
  EClass getRen();

  /**
   * Returns the meta object for the reference list '{@link org.lic.secsi.Ren#getLeflists <em>Leflists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Leflists</em>'.
   * @see org.lic.secsi.Ren#getLeflists()
   * @see #getRen()
   * @generated
   */
  EReference getRen_Leflists();

  /**
   * Returns the meta object for the attribute list '{@link org.lic.secsi.Ren#getRightlist <em>Rightlist</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Rightlist</em>'.
   * @see org.lic.secsi.Ren#getRightlist()
   * @see #getRen()
   * @generated
   */
  EAttribute getRen_Rightlist();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Ren#getR <em>R</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>R</em>'.
   * @see org.lic.secsi.Ren#getR()
   * @see #getRen()
   * @generated
   */
  EReference getRen_R();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Res <em>Res</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Res</em>'.
   * @see org.lic.secsi.Res
   * @generated
   */
  EClass getRes();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.Res#getLeftlists <em>Leftlists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Leftlists</em>'.
   * @see org.lic.secsi.Res#getLeftlists()
   * @see #getRes()
   * @generated
   */
  EReference getRes_Leftlists();

  /**
   * Returns the meta object for the reference list '{@link org.lic.secsi.Res#getLeflists <em>Leflists</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Leflists</em>'.
   * @see org.lic.secsi.Res#getLeflists()
   * @see #getRes()
   * @generated
   */
  EReference getRes_Leflists();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.PropList <em>Prop List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Prop List</em>'.
   * @see org.lic.secsi.PropList
   * @generated
   */
  EClass getPropList();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.PropList#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Element</em>'.
   * @see org.lic.secsi.PropList#getElement()
   * @see #getPropList()
   * @generated
   */
  EReference getPropList_Element();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Prop <em>Prop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Prop</em>'.
   * @see org.lic.secsi.Prop
   * @generated
   */
  EClass getProp();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Prop#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.lic.secsi.Prop#getArg()
   * @see #getProp()
   * @generated
   */
  EReference getProp_Arg();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.PropBody <em>Prop Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Prop Body</em>'.
   * @see org.lic.secsi.PropBody
   * @generated
   */
  EClass getPropBody();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.PropBody#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Action</em>'.
   * @see org.lic.secsi.PropBody#getAction()
   * @see #getPropBody()
   * @generated
   */
  EReference getPropBody_Action();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.PropBody#getExp <em>Exp</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exp</em>'.
   * @see org.lic.secsi.PropBody#getExp()
   * @see #getPropBody()
   * @generated
   */
  EReference getPropBody_Exp();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.NotProp <em>Not Prop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not Prop</em>'.
   * @see org.lic.secsi.NotProp
   * @generated
   */
  EClass getNotProp();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see org.lic.secsi.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Negation <em>Negation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Negation</em>'.
   * @see org.lic.secsi.Negation
   * @generated
   */
  EClass getNegation();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Negation#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.lic.secsi.Negation#getArg()
   * @see #getNegation()
   * @generated
   */
  EReference getNegation_Arg();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.ReferenceInExpression <em>Reference In Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference In Expression</em>'.
   * @see org.lic.secsi.ReferenceInExpression
   * @generated
   */
  EClass getReferenceInExpression();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.ReferenceInExpression#getReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Reference</em>'.
   * @see org.lic.secsi.ReferenceInExpression#getReference()
   * @see #getReferenceInExpression()
   * @generated
   */
  EReference getReferenceInExpression_Reference();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.ReferenceableElement <em>Referenceable Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Referenceable Element</em>'.
   * @see org.lic.secsi.ReferenceableElement
   * @generated
   */
  EClass getReferenceableElement();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.ReferenceableElement#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.lic.secsi.ReferenceableElement#getName()
   * @see #getReferenceableElement()
   * @generated
   */
  EAttribute getReferenceableElement_Name();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.TypeDef <em>Type Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Def</em>'.
   * @see org.lic.secsi.TypeDef
   * @generated
   */
  EClass getTypeDef();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.TypeDef#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.lic.secsi.TypeDef#getName()
   * @see #getTypeDef()
   * @generated
   */
  EAttribute getTypeDef_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.lic.secsi.TypeDef#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.lic.secsi.TypeDef#getElements()
   * @see #getTypeDef()
   * @generated
   */
  EReference getTypeDef_Elements();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.TypeElement <em>Type Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type Element</em>'.
   * @see org.lic.secsi.TypeElement
   * @generated
   */
  EClass getTypeElement();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Variable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable</em>'.
   * @see org.lic.secsi.Variable
   * @generated
   */
  EClass getVariable();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Variable#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see org.lic.secsi.Variable#getType()
   * @see #getVariable()
   * @generated
   */
  EReference getVariable_Type();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.IntConstant <em>Int Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Int Constant</em>'.
   * @see org.lic.secsi.IntConstant
   * @generated
   */
  EClass getIntConstant();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.IntConstant#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.lic.secsi.IntConstant#getValue()
   * @see #getIntConstant()
   * @generated
   */
  EAttribute getIntConstant_Value();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.BoolConstant <em>Bool Constant</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Bool Constant</em>'.
   * @see org.lic.secsi.BoolConstant
   * @generated
   */
  EClass getBoolConstant();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.BoolConstant#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.lic.secsi.BoolConstant#getValue()
   * @see #getBoolConstant()
   * @generated
   */
  EAttribute getBoolConstant_Value();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.VarDeclaration <em>Var Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Var Declaration</em>'.
   * @see org.lic.secsi.VarDeclaration
   * @generated
   */
  EClass getVarDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.VarDeclaration#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Variable</em>'.
   * @see org.lic.secsi.VarDeclaration#getVariable()
   * @see #getVarDeclaration()
   * @generated
   */
  EReference getVarDeclaration_Variable();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.VarDeclaration#getInit <em>Init</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Init</em>'.
   * @see org.lic.secsi.VarDeclaration#getInit()
   * @see #getVarDeclaration()
   * @generated
   */
  EReference getVarDeclaration_Init();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Assignment <em>Assignment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Assignment</em>'.
   * @see org.lic.secsi.Assignment
   * @generated
   */
  EClass getAssignment();

  /**
   * Returns the meta object for the reference '{@link org.lic.secsi.Assignment#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Id</em>'.
   * @see org.lic.secsi.Assignment#getId()
   * @see #getAssignment()
   * @generated
   */
  EReference getAssignment_Id();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Assignment#getIn <em>In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>In</em>'.
   * @see org.lic.secsi.Assignment#getIn()
   * @see #getAssignment()
   * @generated
   */
  EReference getAssignment_In();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Signal <em>Signal</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Signal</em>'.
   * @see org.lic.secsi.Signal
   * @generated
   */
  EClass getSignal();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.OrProp <em>Or Prop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Or Prop</em>'.
   * @see org.lic.secsi.OrProp
   * @generated
   */
  EClass getOrProp();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.OrProp#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.OrProp#getLeft()
   * @see #getOrProp()
   * @generated
   */
  EReference getOrProp_Left();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.OrProp#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.lic.secsi.OrProp#getOp()
   * @see #getOrProp()
   * @generated
   */
  EAttribute getOrProp_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.OrProp#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.OrProp#getRight()
   * @see #getOrProp()
   * @generated
   */
  EReference getOrProp_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.OrExpression <em>Or Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Or Expression</em>'.
   * @see org.lic.secsi.OrExpression
   * @generated
   */
  EClass getOrExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.OrExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.OrExpression#getLeft()
   * @see #getOrExpression()
   * @generated
   */
  EReference getOrExpression_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.OrExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.OrExpression#getRight()
   * @see #getOrExpression()
   * @generated
   */
  EReference getOrExpression_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.AndExpression <em>And Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>And Expression</em>'.
   * @see org.lic.secsi.AndExpression
   * @generated
   */
  EClass getAndExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.AndExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.AndExpression#getLeft()
   * @see #getAndExpression()
   * @generated
   */
  EReference getAndExpression_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.AndExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.AndExpression#getRight()
   * @see #getAndExpression()
   * @generated
   */
  EReference getAndExpression_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.Relation <em>Relation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Relation</em>'.
   * @see org.lic.secsi.Relation
   * @generated
   */
  EClass getRelation();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Relation#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.Relation#getLeft()
   * @see #getRelation()
   * @generated
   */
  EReference getRelation_Left();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.Relation#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.lic.secsi.Relation#getOp()
   * @see #getRelation()
   * @generated
   */
  EAttribute getRelation_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.Relation#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.Relation#getRight()
   * @see #getRelation()
   * @generated
   */
  EReference getRelation_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.SumExpression <em>Sum Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sum Expression</em>'.
   * @see org.lic.secsi.SumExpression
   * @generated
   */
  EClass getSumExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.SumExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.SumExpression#getLeft()
   * @see #getSumExpression()
   * @generated
   */
  EReference getSumExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.SumExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.lic.secsi.SumExpression#getOp()
   * @see #getSumExpression()
   * @generated
   */
  EAttribute getSumExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.SumExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.SumExpression#getRight()
   * @see #getSumExpression()
   * @generated
   */
  EReference getSumExpression_Right();

  /**
   * Returns the meta object for class '{@link org.lic.secsi.MulExpression <em>Mul Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mul Expression</em>'.
   * @see org.lic.secsi.MulExpression
   * @generated
   */
  EClass getMulExpression();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.MulExpression#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.lic.secsi.MulExpression#getLeft()
   * @see #getMulExpression()
   * @generated
   */
  EReference getMulExpression_Left();

  /**
   * Returns the meta object for the attribute '{@link org.lic.secsi.MulExpression#getOp <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Op</em>'.
   * @see org.lic.secsi.MulExpression#getOp()
   * @see #getMulExpression()
   * @generated
   */
  EAttribute getMulExpression_Op();

  /**
   * Returns the meta object for the containment reference '{@link org.lic.secsi.MulExpression#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.lic.secsi.MulExpression#getRight()
   * @see #getMulExpression()
   * @generated
   */
  EReference getMulExpression_Right();

  /**
   * Returns the meta object for enum '{@link org.lic.secsi.ActionModality <em>Action Modality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Action Modality</em>'.
   * @see org.lic.secsi.ActionModality
   * @generated
   */
  EEnum getActionModality();

  /**
   * Returns the meta object for enum '{@link org.lic.secsi.BoolValue <em>Bool Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Bool Value</em>'.
   * @see org.lic.secsi.BoolValue
   * @generated
   */
  EEnum getBoolValue();

  /**
   * Returns the meta object for enum '{@link org.lic.secsi.Relop <em>Relop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Relop</em>'.
   * @see org.lic.secsi.Relop
   * @generated
   */
  EEnum getRelop();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SecsiFactory getSecsiFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ProgramImpl <em>Program</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ProgramImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getProgram()
     * @generated
     */
    EClass PROGRAM = eINSTANCE.getProgram();

    /**
     * The meta object literal for the '<em><b>Globalchanels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__GLOBALCHANELS = eINSTANCE.getProgram_Globalchanels();

    /**
     * The meta object literal for the '<em><b>Agents</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__AGENTS = eINSTANCE.getProgram_Agents();

    /**
     * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__TYPES = eINSTANCE.getProgram_Types();

    /**
     * The meta object literal for the '<em><b>Consts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__CONSTS = eINSTANCE.getProgram_Consts();

    /**
     * The meta object literal for the '<em><b>System</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__SYSTEM = eINSTANCE.getProgram_System();

    /**
     * The meta object literal for the '<em><b>Properties</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROGRAM__PROPERTIES = eINSTANCE.getProgram_Properties();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ConstantImpl <em>Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ConstantImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getConstant()
     * @generated
     */
    EClass CONSTANT = eINSTANCE.getConstant();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONSTANT__TYPE = eINSTANCE.getConstant_Type();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONSTANT__VALUE = eINSTANCE.getConstant_Value();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.AgentImpl <em>Agent</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.AgentImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getAgent()
     * @generated
     */
    EClass AGENT = eINSTANCE.getAgent();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute AGENT__NAME = eINSTANCE.getAgent_Name();

    /**
     * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AGENT__PARAMS = eINSTANCE.getAgent_Params();

    /**
     * The meta object literal for the '<em><b>Lc Decs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AGENT__LC_DECS = eINSTANCE.getAgent_LcDecs();

    /**
     * The meta object literal for the '<em><b>S</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AGENT__S = eINSTANCE.getAgent_S();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ChannelsDeclarationImpl <em>Channels Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ChannelsDeclarationImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getChannelsDeclaration()
     * @generated
     */
    EClass CHANNELS_DECLARATION = eINSTANCE.getChannelsDeclaration();

    /**
     * The meta object literal for the '<em><b>Declared</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHANNELS_DECLARATION__DECLARED = eINSTANCE.getChannelsDeclaration_Declared();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ChannelImpl <em>Channel</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ChannelImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getChannel()
     * @generated
     */
    EClass CHANNEL = eINSTANCE.getChannel();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHANNEL__NAME = eINSTANCE.getChannel_Name();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHANNEL__TYPE = eINSTANCE.getChannel_Type();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.TypeImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.TypeExpressionImpl <em>Type Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.TypeExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeExpression()
     * @generated
     */
    EClass TYPE_EXPRESSION = eINSTANCE.getTypeExpression();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.IntegerTypeImpl <em>Integer Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.IntegerTypeImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getIntegerType()
     * @generated
     */
    EClass INTEGER_TYPE = eINSTANCE.getIntegerType();

    /**
     * The meta object literal for the '<em><b>Min</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_TYPE__MIN = eINSTANCE.getIntegerType_Min();

    /**
     * The meta object literal for the '<em><b>Max</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTEGER_TYPE__MAX = eINSTANCE.getIntegerType_Max();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.BooleanTypeImpl <em>Boolean Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.BooleanTypeImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getBooleanType()
     * @generated
     */
    EClass BOOLEAN_TYPE = eINSTANCE.getBooleanType();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.TypeReferenceImpl <em>Type Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.TypeReferenceImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeReference()
     * @generated
     */
    EClass TYPE_REFERENCE = eINSTANCE.getTypeReference();

    /**
     * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE_REFERENCE__REFERENCE = eINSTANCE.getTypeReference_Reference();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.BlockImpl <em>Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.BlockImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getBlock()
     * @generated
     */
    EClass BLOCK = eINSTANCE.getBlock();

    /**
     * The meta object literal for the '<em><b>Cmds</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BLOCK__CMDS = eINSTANCE.getBlock_Cmds();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.StatementImpl <em>Statement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.StatementImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getStatement()
     * @generated
     */
    EClass STATEMENT = eINSTANCE.getStatement();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ActionImpl <em>Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ActionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getAction()
     * @generated
     */
    EClass ACTION = eINSTANCE.getAction();

    /**
     * The meta object literal for the '<em><b>Channel</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTION__CHANNEL = eINSTANCE.getAction_Channel();

    /**
     * The meta object literal for the '<em><b>Modality</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTION__MODALITY = eINSTANCE.getAction_Modality();

    /**
     * The meta object literal for the '<em><b>Not Signal</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTION__NOT_SIGNAL = eINSTANCE.getAction_NotSignal();

    /**
     * The meta object literal for the '<em><b>E</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTION__E = eINSTANCE.getAction_E();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.Switch_CaseImpl <em>Switch Case</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.Switch_CaseImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getSwitch_Case()
     * @generated
     */
    EClass SWITCH_CASE = eINSTANCE.getSwitch_Case();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_CASE__VALUE = eINSTANCE.getSwitch_Case_Value();

    /**
     * The meta object literal for the '<em><b>Cases</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_CASE__CASES = eINSTANCE.getSwitch_Case_Cases();

    /**
     * The meta object literal for the '<em><b>Default</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_CASE__DEFAULT = eINSTANCE.getSwitch_Case_Default();

    /**
     * The meta object literal for the '<em><b>Cmd</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_CASE__CMD = eINSTANCE.getSwitch_Case_Cmd();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.IfThenElseImpl <em>If Then Else</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.IfThenElseImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getIfThenElse()
     * @generated
     */
    EClass IF_THEN_ELSE = eINSTANCE.getIfThenElse();

    /**
     * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_THEN_ELSE__EXPRESSION = eINSTANCE.getIfThenElse_Expression();

    /**
     * The meta object literal for the '<em><b>Then Branch</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_THEN_ELSE__THEN_BRANCH = eINSTANCE.getIfThenElse_ThenBranch();

    /**
     * The meta object literal for the '<em><b>With Else</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IF_THEN_ELSE__WITH_ELSE = eINSTANCE.getIfThenElse_WithElse();

    /**
     * The meta object literal for the '<em><b>Else Branch</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_THEN_ELSE__ELSE_BRANCH = eINSTANCE.getIfThenElse_ElseBranch();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.WhileImpl <em>While</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.WhileImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getWhile()
     * @generated
     */
    EClass WHILE = eINSTANCE.getWhile();

    /**
     * The meta object literal for the '<em><b>Expresion</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE__EXPRESION = eINSTANCE.getWhile_Expresion();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE__BODY = eINSTANCE.getWhile_Body();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.SystemImpl <em>System</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.SystemImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getSystem()
     * @generated
     */
    EClass SYSTEM = eINSTANCE.getSystem();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYSTEM__BODY = eINSTANCE.getSystem_Body();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ParImpl <em>Par</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ParImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getPar()
     * @generated
     */
    EClass PAR = eINSTANCE.getPar();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PAR__ACTION = eINSTANCE.getPar_Action();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PAR__LEFT = eINSTANCE.getPar_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PAR__RIGHT = eINSTANCE.getPar_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.SumImpl <em>Sum</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.SumImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getSum()
     * @generated
     */
    EClass SUM = eINSTANCE.getSum();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM__ACTION = eINSTANCE.getSum_Action();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM__LEFT = eINSTANCE.getSum_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM__RIGHT = eINSTANCE.getSum_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ProcImpl <em>Proc</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ProcImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getProc()
     * @generated
     */
    EClass PROC = eINSTANCE.getProc();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROC__NAME = eINSTANCE.getProc_Name();

    /**
     * The meta object literal for the '<em><b>Name1</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROC__NAME1 = eINSTANCE.getProc_Name1();

    /**
     * The meta object literal for the '<em><b>Re</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROC__RE = eINSTANCE.getProc_Re();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.RenImpl <em>Ren</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.RenImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getRen()
     * @generated
     */
    EClass REN = eINSTANCE.getRen();

    /**
     * The meta object literal for the '<em><b>Leflists</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REN__LEFLISTS = eINSTANCE.getRen_Leflists();

    /**
     * The meta object literal for the '<em><b>Rightlist</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REN__RIGHTLIST = eINSTANCE.getRen_Rightlist();

    /**
     * The meta object literal for the '<em><b>R</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REN__R = eINSTANCE.getRen_R();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ResImpl <em>Res</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ResImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getRes()
     * @generated
     */
    EClass RES = eINSTANCE.getRes();

    /**
     * The meta object literal for the '<em><b>Leftlists</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RES__LEFTLISTS = eINSTANCE.getRes_Leftlists();

    /**
     * The meta object literal for the '<em><b>Leflists</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RES__LEFLISTS = eINSTANCE.getRes_Leflists();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.PropListImpl <em>Prop List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.PropListImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getPropList()
     * @generated
     */
    EClass PROP_LIST = eINSTANCE.getPropList();

    /**
     * The meta object literal for the '<em><b>Element</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROP_LIST__ELEMENT = eINSTANCE.getPropList_Element();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.PropImpl <em>Prop</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.PropImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getProp()
     * @generated
     */
    EClass PROP = eINSTANCE.getProp();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROP__ARG = eINSTANCE.getProp_Arg();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.PropBodyImpl <em>Prop Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.PropBodyImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getPropBody()
     * @generated
     */
    EClass PROP_BODY = eINSTANCE.getPropBody();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROP_BODY__ACTION = eINSTANCE.getPropBody_Action();

    /**
     * The meta object literal for the '<em><b>Exp</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROP_BODY__EXP = eINSTANCE.getPropBody_Exp();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.NotPropImpl <em>Not Prop</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.NotPropImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getNotProp()
     * @generated
     */
    EClass NOT_PROP = eINSTANCE.getNotProp();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.NegationImpl <em>Negation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.NegationImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getNegation()
     * @generated
     */
    EClass NEGATION = eINSTANCE.getNegation();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NEGATION__ARG = eINSTANCE.getNegation_Arg();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ReferenceInExpressionImpl <em>Reference In Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ReferenceInExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getReferenceInExpression()
     * @generated
     */
    EClass REFERENCE_IN_EXPRESSION = eINSTANCE.getReferenceInExpression();

    /**
     * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REFERENCE_IN_EXPRESSION__REFERENCE = eINSTANCE.getReferenceInExpression_Reference();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.ReferenceableElementImpl <em>Referenceable Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.ReferenceableElementImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getReferenceableElement()
     * @generated
     */
    EClass REFERENCEABLE_ELEMENT = eINSTANCE.getReferenceableElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REFERENCEABLE_ELEMENT__NAME = eINSTANCE.getReferenceableElement_Name();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.TypeDefImpl <em>Type Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.TypeDefImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeDef()
     * @generated
     */
    EClass TYPE_DEF = eINSTANCE.getTypeDef();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TYPE_DEF__NAME = eINSTANCE.getTypeDef_Name();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TYPE_DEF__ELEMENTS = eINSTANCE.getTypeDef_Elements();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.TypeElementImpl <em>Type Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.TypeElementImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getTypeElement()
     * @generated
     */
    EClass TYPE_ELEMENT = eINSTANCE.getTypeElement();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.VariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.VariableImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getVariable()
     * @generated
     */
    EClass VARIABLE = eINSTANCE.getVariable();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLE__TYPE = eINSTANCE.getVariable_Type();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.IntConstantImpl <em>Int Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.IntConstantImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getIntConstant()
     * @generated
     */
    EClass INT_CONSTANT = eINSTANCE.getIntConstant();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INT_CONSTANT__VALUE = eINSTANCE.getIntConstant_Value();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.BoolConstantImpl <em>Bool Constant</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.BoolConstantImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getBoolConstant()
     * @generated
     */
    EClass BOOL_CONSTANT = eINSTANCE.getBoolConstant();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOL_CONSTANT__VALUE = eINSTANCE.getBoolConstant_Value();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.VarDeclarationImpl <em>Var Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.VarDeclarationImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getVarDeclaration()
     * @generated
     */
    EClass VAR_DECLARATION = eINSTANCE.getVarDeclaration();

    /**
     * The meta object literal for the '<em><b>Variable</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_DECLARATION__VARIABLE = eINSTANCE.getVarDeclaration_Variable();

    /**
     * The meta object literal for the '<em><b>Init</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VAR_DECLARATION__INIT = eINSTANCE.getVarDeclaration_Init();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.AssignmentImpl <em>Assignment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.AssignmentImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getAssignment()
     * @generated
     */
    EClass ASSIGNMENT = eINSTANCE.getAssignment();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT__ID = eINSTANCE.getAssignment_Id();

    /**
     * The meta object literal for the '<em><b>In</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ASSIGNMENT__IN = eINSTANCE.getAssignment_In();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.SignalImpl <em>Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.SignalImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getSignal()
     * @generated
     */
    EClass SIGNAL = eINSTANCE.getSignal();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.OrPropImpl <em>Or Prop</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.OrPropImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getOrProp()
     * @generated
     */
    EClass OR_PROP = eINSTANCE.getOrProp();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_PROP__LEFT = eINSTANCE.getOrProp_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OR_PROP__OP = eINSTANCE.getOrProp_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_PROP__RIGHT = eINSTANCE.getOrProp_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.OrExpressionImpl <em>Or Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.OrExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getOrExpression()
     * @generated
     */
    EClass OR_EXPRESSION = eINSTANCE.getOrExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_EXPRESSION__LEFT = eINSTANCE.getOrExpression_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OR_EXPRESSION__RIGHT = eINSTANCE.getOrExpression_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.AndExpressionImpl <em>And Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.AndExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getAndExpression()
     * @generated
     */
    EClass AND_EXPRESSION = eINSTANCE.getAndExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_EXPRESSION__LEFT = eINSTANCE.getAndExpression_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference AND_EXPRESSION__RIGHT = eINSTANCE.getAndExpression_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.RelationImpl <em>Relation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.RelationImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getRelation()
     * @generated
     */
    EClass RELATION = eINSTANCE.getRelation();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__LEFT = eINSTANCE.getRelation_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RELATION__OP = eINSTANCE.getRelation_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATION__RIGHT = eINSTANCE.getRelation_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.SumExpressionImpl <em>Sum Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.SumExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getSumExpression()
     * @generated
     */
    EClass SUM_EXPRESSION = eINSTANCE.getSumExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM_EXPRESSION__LEFT = eINSTANCE.getSumExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUM_EXPRESSION__OP = eINSTANCE.getSumExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUM_EXPRESSION__RIGHT = eINSTANCE.getSumExpression_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.impl.MulExpressionImpl <em>Mul Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.impl.MulExpressionImpl
     * @see org.lic.secsi.impl.SecsiPackageImpl#getMulExpression()
     * @generated
     */
    EClass MUL_EXPRESSION = eINSTANCE.getMulExpression();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MUL_EXPRESSION__LEFT = eINSTANCE.getMulExpression_Left();

    /**
     * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MUL_EXPRESSION__OP = eINSTANCE.getMulExpression_Op();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MUL_EXPRESSION__RIGHT = eINSTANCE.getMulExpression_Right();

    /**
     * The meta object literal for the '{@link org.lic.secsi.ActionModality <em>Action Modality</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.ActionModality
     * @see org.lic.secsi.impl.SecsiPackageImpl#getActionModality()
     * @generated
     */
    EEnum ACTION_MODALITY = eINSTANCE.getActionModality();

    /**
     * The meta object literal for the '{@link org.lic.secsi.BoolValue <em>Bool Value</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.BoolValue
     * @see org.lic.secsi.impl.SecsiPackageImpl#getBoolValue()
     * @generated
     */
    EEnum BOOL_VALUE = eINSTANCE.getBoolValue();

    /**
     * The meta object literal for the '{@link org.lic.secsi.Relop <em>Relop</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.lic.secsi.Relop
     * @see org.lic.secsi.impl.SecsiPackageImpl#getRelop()
     * @generated
     */
    EEnum RELOP = eINSTANCE.getRelop();

  }

} //SecsiPackage

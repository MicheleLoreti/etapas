/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate#getArg <em>Arg</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getNotLabelPredicate()
 * @model
 * @generated
 */
public interface NotLabelPredicate extends LabelPredicate
{
  /**
   * Returns the value of the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' containment reference.
   * @see #setArg(LabelPredicate)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getNotLabelPredicate_Arg()
   * @model containment="true"
   * @generated
   */
  LabelPredicate getArg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate#getArg <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' containment reference.
   * @see #getArg()
   * @generated
   */
  void setArg(LabelPredicate value);

} // NotLabelPredicate

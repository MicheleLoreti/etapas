package org.cmg.tapas.graph.algorithms.counterExample;


import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import org.cmg.tapas.formulae.hml.HmlFormula;
import org.cmg.tapas.formulae.hml.NotFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.GraphData;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.processAlgebra.APProcess;


public class BranchingCounterExample	
		<S extends APProcess<S, A>, A extends ActionInterface>  extends
		BisimulationCounterExample<S, A> {

	private A tau;
	
	public BranchingCounterExample(GraphData<S, A> graph, A tauAct, Evaluator<S, ?> e) {
		super(graph, true, tauAct, e);
		tau = tauAct;
	}	
	
	@Override
	protected void actions(int i, Set<A> l1, int j, Set<A> l2){		
		//Azioni che possono essere fatte solamente dalla prima partizione
		Set<A> actSet = new TreeSet<A>(l1);
		actSet.removeAll(l2);
		actSet.remove(tau);
		if(actSet.size() > 0 && !l2.contains(tau)){
			HmlFormula<S,A> formula = Ffactory.createHMLUntil(actSet);		
			curr_formulae.addFormula(i, j, formula);
			HmlFormula<S,A> notFormula = new NotFormula<S, A>(formula);
			curr_formulae.addFormula(j, i, notFormula);
		}

		//Azioni che possono essere fatte solamente dalla seconda partizione
		actSet = new TreeSet<A>(l2);
		actSet.removeAll(l1);
		actSet.remove(tau);
		if(actSet.size() > 0 && !l1.contains(tau)){
			HmlFormula<S,A> formula = Ffactory.createHMLUntil(actSet);		
			curr_formulae.addFormula(j, i, formula);
			HmlFormula<S,A> notFormula = new NotFormula<S, A>(formula);
			curr_formulae.addFormula(i, j, notFormula);
		}
	}
	
	protected void subSet(A act, Object hs1, Set<Object> intersection
			, Object hs2, Set<Object> difference){		
		LinkedList<LinkedList<HmlFormula<S, A>>> sfList1 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
		LinkedList<LinkedList<HmlFormula<S, A>>> sfList2 = new LinkedList<LinkedList<HmlFormula<S,A>>>();

		for (Object subHS2 : difference) {
			for (Object subHS1 : intersection) {
				if(
					!(subHS1.equals(hs1) && subHS2.equals(hs2))
						||
					!(subHS1.equals(hs2) && subHS2.equals(hs1))
					){
					sfList1.add(old_formulae.getFormulae(subHS1, subHS2));
					sfList2.add(old_formulae.getFormulae(subHS2, subHS1));	
				}
			}
			LinkedList<HmlFormula<S,A>> formulas = Ffactory.createHmlUntil(act, sfList1);
			curr_formulae.addFormulae(hs1, hs2, formulas);
			
			formulas = Ffactory.createHmlUntil(act, sfList2);
			curr_formulae.addFormulae(hs2, hs1, formulas);
			
			sfList1 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
			sfList2 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
		}
	}
	

	
	protected void disjoinSet(Object hs1, Object hs2, A act, Set<Object> S1, Set<Object> S2){
		for (Object subHS1 : S1) {
			boolean ok1 = true;
			boolean ok2 = true;
			LinkedList<LinkedList<HmlFormula<S, A>>> fList1 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
			LinkedList<LinkedList<HmlFormula<S, A>>> fList2 = new LinkedList<LinkedList<HmlFormula<S,A>>>();
			for(Object subHS2 : S2) {
				if((!subHS1.equals(hs1) || !subHS2.equals(hs2))
						&&
						(!subHS1.equals(hs2) || !subHS2.equals(hs1))){
					// 1
					LinkedList<HmlFormula<S,A>> tmp1 = old_formulae.getFormulae(subHS1, subHS2);
					if(tmp1.size() > 0)
						fList1.add(tmp1);
					else 
						ok1 = false;
					
					// 2
					LinkedList<HmlFormula<S,A>> tmp2 = old_formulae.getFormulae(subHS2, subHS1);
					if(tmp2.size() > 0)
						fList2.add(tmp2);
					else 
						ok2 = false;
					
				}
			}
			// 1
			if(fList1.size() > 0 && ok1){
				LinkedList<HmlFormula<S, A>> res = Ffactory.createHmlUntil(act, fList1);
				curr_formulae.addFormulae(hs1, hs2, res);
			}
			
			// 2
			if(fList2.size() > 0 && ok2){
				LinkedList<HmlFormula<S, A>> res = Ffactory.createHmlUntil(act, fList2);
				curr_formulae.addFormulae(hs2, hs1, res);
			}			
		}
	}

}

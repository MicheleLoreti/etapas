/**
 * 
 */
package org.cmg.tapas.formulae.hml;


import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import org.cmg.tapas.formulae.lmc.Proof;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.graph.filter.HashSetActionFilter;
import org.cmg.tapas.graph.filter.SingleActionFilter;
import org.cmg.tapas.graph.filter.TrueFilter;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public final class BoxFormula<X extends APProcess<X, Y>,Y extends ActionInterface> 
						extends HmlFormula<X,Y> {

	private Filter<Y> filter;
	private HmlFormula<X,Y> arg;
	
	
	public BoxFormula( HmlFormula<X,Y> arg ) {
		this(new TrueFilter<Y>(),arg);
	}
		
	public BoxFormula(Filter<Y> actions , HmlFormula<X,Y> arg) {
		this.filter = actions;
		this.arg = arg;
	}
	
	public BoxFormula( Y action , HmlFormula<X, Y> arg) { 
		this( new SingleActionFilter<Y>(action) , arg );
	}
	
	public BoxFormula(Set<Y> action_set, HmlFormula<X, Y> arg) {
		this( new HashSetActionFilter<Y>(action_set) , arg );
	}

	
	/* (non-Javadoc)
	 * @see org.cmg.lmc.Formula#satisfies(graph.Graph)
	 */
	public boolean satisfies( X x ) {
		
		LinkedList<X> poset = x.getNext(filter);

			for (X s : poset ) {
				if (!arg.satisfies(s)) {
					return false;
				}
			}
		return true;

	}
	
	@Override
	public Proof<X, Y> getProof(X x) {
		LinkedList<X> poset = x.getNext( filter );
		Vector<Proof<X, Y>> proofs = new Vector<Proof<X, Y>>();
		Proof<X,Y> p;
			for (X s : poset) {
				p = arg.getProof(s);
				if (!p.isSuccess()) {
					return new Proof<X,Y>( x , this , new Object[] { p } , filter, false );
				}
				proofs.add(p);
			}
		return new Proof<X,Y>( x , this , proofs.toArray() , filter );
	}


	@Override
	protected HmlFormula<X, Y> doClone() {
		return new BoxFormula<X, Y>(filter,arg.clone());
	}

	@Override
	protected void doVisit(FormulaVisitor<X, Y> visitor) {
		visitor.visitBox(this);
	}

	@Override
	protected String _toString() {
		return "["+filter+"]"+arg;
	}

	@Override
	public String getUnicode() {
		return "["+filter+"]"+arg.getUnicode();
	}

	@Override
	public String operator() {
		return "BOX( "+filter+" )";
	}

	@Override
	protected boolean _equals(Object obj) {
		if(obj == null)
			return false;
		
		if (obj instanceof BoxFormula) {
			BoxFormula<X, Y> f = (BoxFormula<X, Y>) obj;
			
			if(!filter.equals(f.filter))
				return false;
			
			return arg.equals(f.arg);
			
		}
		
		return false;
	}
}

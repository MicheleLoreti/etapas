/**
 * 
 */
package org.cmg.tapas.formulae.hml;


import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface FormulaVisitor<X extends APProcess<X, Y>,Y extends ActionInterface> {
	
	void visitDiamond( DiamondFormula<X,Y> f);
	void visitWeakDiamond( WeakDiamondFormula<X,Y> f);
	void visitBox( BoxFormula<X, Y> name);
	void visitWeakBox( WeakBoxFormula<X, Y> name);
	void visitAnd( AndFormula<X, Y> f );
	void visitDiv(DivergenceFormula<X, Y> f );
	void visitOr( OrFormula<X,Y> f);
	void visitNot( NotFormula<X, Y> f );
	void visitTrue( True<X,Y> f);
	void visitFalse( False<X,Y> f);
	void visitFixPoint( FixPoint<X, Y> f );
	void visitReference( FixPoint<X,Y>.Reference ref );
	void visitUndefined( Undefined<X, Y> f);
	
}

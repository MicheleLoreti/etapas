package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

/**
 * La classe ActionsSet rappresenta un insieme di azioni
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class ActionsSet {
	
	/**
	 * Rappresenta l'insieme delle azioni.
	 **/	
	private Vector<Action> actions = null;		
	
	/**
	 * Costruisce una nuovo insieme di azioni vuoto. 
	 **/
	public ActionsSet() {
		actions = new Vector<Action>();
  	}
  	
	/**
	 * Restituisce il numero di elementi contenuti nell'insieme.
	 **/  	
  	public int getDimension() {
  		return actions.size();
  	}

	/**
	 * Aggiunge un elemento all'insieme un oggetto. 
	 * L'insieme viene mantenuto ordinato lessicograficamente rispetto 
	 * ai nomi delle azioni. Inoltre non � possibile 
	 * aggiungere azioni ripetute, 
	 * <p>
	 */
	public int addAction(Action action) {
		int i = 0;
		int compare = 1;
		for( i=0; i<getDimension(); i++ ) {
			compare = actionAt(i).getName().compareTo( action.getName() );
			if( compare>=0 )
				break;
		}
		
		if( compare != 0 ) 
			actions.add(i,action);
			
		return i;
	}


	/**
	 * Rimuove l'azione etichettata con la stringa passata come parametro.
	 **/
	public boolean removeAction(String name) {
		int i = getActionIndex(name);
		if( i != -1 ) {
			actions.removeElementAt(i);
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Permette di ricercare un'azione all'interno dell'insieme specificandone 
	 * il nome. Poich� l'insieme
	 * delle azioni � ordinato, viene effettuata una ricerca binaria.
	 **/	
	public Action getAction(String name) {
		int compare = 0;
		int low = 0;
		int high = getDimension() - 1;
		int mid = 0;
	
		while( low <= high ) {
			mid = (low + high) / 2;
			compare = actionAt(mid).getName().compareTo(name);
			if( compare < 0 ) 
				low = mid + 1;
			else if( compare > 0 ) 
				high = mid -1;
			else 
				return actionAt(mid);
		}
		
		return null;
	}	
	

	/**
	 * Controlla se l'insieme contiene un'azione con etichetta 
	 * uguale al parametro stringa in ingresso. 
	 **/		
	public int getActionIndex(String name) {
		int compare = 0;
		int low = 0;
		int high = getDimension() - 1;
		int mid = 0;
		
		while (low <= high) {
			mid = (low + high) / 2;
			compare = actionAt(mid).getName().compareTo(name);
			if( compare < 0 ) 
				low = mid + 1;
			else if( compare > 0 ) 
				high = mid -1;
			else 
				return mid;
		}
		
		return -1;
	}

	
	/**
	 * Ritorna l'oggetto di tipo <tt>Action</tt> contenuto nell'insieme 
	 * delle azioni alla posizione 
	 * specificata dal parametro intero in ingresso. 
	 **/ 
	public Action actionAt(int index) {
		try { 
			return actions.elementAt(index); 
		}	
		catch( ArrayIndexOutOfBoundsException e) { 
			return null; 
		}
	}

	/**
	 * Controlla se l'insieme delle azioni contiene un'azione con il 
	 * nome passato come parametro di ingresso. 
	 **/ 
	public boolean containAction(String name) {
		return getAction(name) != null;
	}

	/**
	 * Controlla se l'insieme delle azioni contiene tutte le azioni 
	 * dell'insieme <tt>ActionsSet</tt> passato in ingresso. 
	 **/
	public boolean containActionsSet(ActionsSet as) {
		for( int i=0; i<as.getDimension(); i++ )
			if( !containAction(as.actionAt(i).getName()) ) 
				return false;
				
		return true;
	}

	/**
	 * Verifica se questo insieme � uguale all'insieme 
	 * passato come parametro.
	 **/ 
	public boolean equals(ActionsSet as) {
		if( getDimension() != as.getDimension() ) 
			return false;

		for( int i=0; i<as.getDimension(); i++ )
			if( !containAction(as.actionAt(i).getName()) ) 
				return false;
				
		return true;
	}

	/**
	 * Controlla se l'insieme delle azioni � vuoto.
	 **/
	public boolean isEmpty() {
		return getDimension() == 0;
	}


	/**
	 * Stampa l'intero insieme di azioni
	 **/ 
	public void print() {
		print(System.out);
	}

	/**
	 * Stampa l'intero insieme di azioni
	 **/ 
	public void print(java.io.PrintStream console) {
		
		console.print("{");

		int i = 0;
		for( i=0; i<getDimension(); i++ ) {
			if(i>0) 
				console.print(", ");
			console.print( actions.elementAt(i).getName() );
		}
		if(i==0) 
			console.print("vuoto");
			
		console.print("}");
	}
}
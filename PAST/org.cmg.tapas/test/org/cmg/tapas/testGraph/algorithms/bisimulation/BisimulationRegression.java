package org.cmg.tapas.testGraph.algorithms.bisimulation;

import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.bisimulation.pt.PTChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.RandomGraphGenerator;
import org.cmg.tapas.testGraph.algorithms.regression.Regression;
import org.cmg.tapas.testGraph.algorithms.regression.Regression2;
import org.cmg.tapas.testGraph.algorithms.regression.Simulation;


/**
 * BisimulationRegression calcola la dipendenza (regressione)
 * tra la dimensione del grafo e il tempo per calcolare 
 * la bisimulazione.
 *
 * @author Guzman Tierno
 **/
public class BisimulationRegression {
	
	public static void main(String[] args) {
		Simulation sim = new BiSimulation();
		sim.simulate();

		System.out.println( sim.regression(Regression.POLYNOMIAL) + "\n" );		
		System.out.println( sim.regression(Regression.EXPONENTIAL) + "\n" );		
		System.out.println( sim.regression(Regression.LINEARITMIC) + "\n" );		
		System.out.println( sim.regression(Regression.LOGARITMIC) + "\n" );		
		System.out.println( sim.regression(Regression.QUADRATIC) + "\n" );		
		System.out.println( sim.regression(Regression.LINEAR) + "\n" );		
		System.out.println( sim.regression(Regression.LINEAR_LOG) + "\n" );		
		System.out.println( "Migliore:\n" + sim.bestRegression() );		
		System.out.println( sim.regression2(Regression2.QUADRATIC) + "\n" );
		
		double[] dims = sim.getDimensions();
		double[] means = sim.getMeanValues();		
		System.out.print( "plot( [" );
		for( int i=0; i<dims.length; ++i ) {
			System.out.print( dims[i] );		
			if( i<dims.length-1 )
				System.out.print( ", " );		
		}
		System.out.print( "], [" );
		for( int i=0; i<means.length; ++i ) {
			System.out.print( means[i] );		
			if( i<means.length-1 )
				System.out.print( ", " );		
		}
		System.out.println( "], 'r' );" );
	}	

}



class BiSimulation extends Simulation {

	public BiSimulation() {
		super(
			1,	 						// prove	
			50,							// dim base
			50,							// gap 
			12,							// gradi (numero di dimensioni)
			Simulation.SORT,
			Simulation.NULL_VECTOR
		);
	}

	private LtsAction[] actions = { 
		new LtsAction("0"),
		new LtsAction("1"),
		new LtsAction("2"),
		new LtsAction("3"),
		new LtsAction("4"),
		new LtsAction("5"),
		new LtsAction("6"),
		new LtsAction("7")
	};
	
	private double connectionProbability = 0.05;
	
	private RandomGraphGenerator<LtsState,LtsAction> generator = 
		new RandomGraphGenerator<LtsState,LtsAction>(
			new GraphItemFactory()
	);
	
	@Override 
	public double algorithm(int dim, int[] v) {
		double start;
		double time = 0;
		
		for( int i=0; i<1; ++i ) {
			if( i%2==0 )
				System.out.println( dim + ": " + i );
			PointedGraph<LtsState,LtsAction> pointedGraph = 
				generator.generateRandomGraph(
					dim, actions, connectionProbability
				);
			GraphInterface<LtsState,LtsAction> graph = pointedGraph.getGraph();
			
			// Tapas1
			//	start = System.currentTimeMillis();
			//	TBisimulationChecker<LtsState,LtsAction> checker = 
			//		new TBisimulationChecker<LtsState,LtsAction>();
			//	checker.setParameters(
			//		graph, graph, 
			//		pointedGraph.getState1(), pointedGraph.getState2(), 
			//		false
			//	); 
			//	checker.checkEquivalence();
			//	time += System.currentTimeMillis() - start;
		
		 	// KSO
			//	start = System.currentTimeMillis();
			//	KSOChecker<LtsState,LtsAction> ksoChecker = 
			//		new KSOChecker<LtsState,LtsAction>(graph);			
			//	HashMap<LtsState, ? extends Set<LtsState>> map1 = 
			//		ksoChecker.computeEquivalence(null);
			//	time += System.currentTimeMillis() - start;
		
		 	// PT
			start = System.currentTimeMillis();
			PTChecker<LtsState,LtsAction> ptChecker = 
				new PTChecker<LtsState,LtsAction>(graph);			
			ptChecker.computeEquivalence(null);
			time += System.currentTimeMillis() - start;

			// RankBased
			//	start = System.currentTimeMillis();
			//	RankBasedChecker<LtsState,LtsAction> rankChecker = 
			//		new RankBasedChecker<LtsState,LtsAction>(graph);			
			//	HashMap<LtsState, ? extends Set<LtsState>> map3 = 
			//		rankChecker.computeEquivalence(null);
			//	time += System.currentTimeMillis() - start;
		}
		
		System.gc();
		return time;
	}
	
	@Override 
	public double theoricValue(double n) {
		return n;
	}
}


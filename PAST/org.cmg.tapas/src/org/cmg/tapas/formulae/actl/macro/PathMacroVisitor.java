/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public interface PathMacroVisitor<S extends APProcess<S,A> , A extends ActionInterface> {

	public void visitNext( NextMacro<S, A> phi );
	public void visitUntil( UntilMacro<S, A> phi );
	public void visitFuture(FutureMacro<S, A> phi);
	
}

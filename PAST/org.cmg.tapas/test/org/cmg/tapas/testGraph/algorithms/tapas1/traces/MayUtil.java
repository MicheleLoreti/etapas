package org.cmg.tapas.testGraph.algorithms.tapas1.traces;

import java.util.Vector;

import org.cmg.tapas.testGraph.algorithms.tapas1.ProcessAG;
import org.cmg.tapas.testGraph.algorithms.tapas1.StatesSet;
import org.cmg.tapas.testGraph.algorithms.tapas1.Trace;
import org.cmg.tapas.testGraph.algorithms.tapas1.TransitionCouple;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfo;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogInfoEntry;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplitting;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTLogSplittingEntry;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTPartition;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTState;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.PTStatesSet;


/**
 * MayUtil.
 *
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class MayUtil {

	public static void whyNotEquivalent(
		PTLogSplitting log1,
		PTLogInfo log2,
	    PTState s1, 
	    PTState s2,
	    String nome1,
	    String nome2
	) {
		java.io.PrintStream console = System.out;

		PTLogSplittingEntry e1 = null;
		PTLogInfoEntry e2 = null;
		PTPartition splittedSet = null;
		PTStatesSet ss1 = null;
		PTStatesSet ss2 = null;
		PTStatesSet ss1d1 = null;
		PTStatesSet ss2d1 = null;
		boolean ss1not = false;
		boolean ss2not = false;
		Trace traccia = new Trace();
		Vector<PTLogInfoEntry> info = null;

		console.append("because there exists a sequence of actions \n S = ");
		console.flush();

		//Cerco la causa dello splitting in LogSplitting
		e1 = log1.getDeppestCause(s1,s2);
		//Recupero i blocchi che contengono s1 e s2, rispettivamente ss1 e ss2
		splittedSet = e1.getSplittedSet();

		if (splittedSet.ptstatesSetAt(0).containPTState(s1)) {
			ss1 = splittedSet.ptstatesSetAt(0);
			ss2 = splittedSet.ptstatesSetAt(1);
		} else {
			ss2 = splittedSet.ptstatesSetAt(0);
			ss1 = splittedSet.ptstatesSetAt(1);
		}

		while( true ) {
			//Cerco in LogInfo i blocchi rispetto ai quale sono stati splittati ss1 e ss2
			info = log2.getInfo(ss1,ss2,e1.getCause());
			e2 = info.elementAt(0);
			ss1d1 = e2.getFirstDestination();
			ss1not = e2.isNot();
			e2 = info.elementAt(1);
			ss2d1 = e2.getFirstDestination();
			ss2not = e2.isNot();
			//aggiungi cause alla soluzione
			traccia.append(e1.getCause());
			if( !ss1not && !ss2not ) {
				ss1 = ss1d1;
				ss2 = ss2d1;
				//Cerco la causa dello splitting in LogSplitting
				e1 = log1.getDeppestCause(ss1,ss2);			
			} else 
				break;
		}

		traccia.print(console);
		console.append("\nsuch that\n");
		if(ss1not) 
			console.append(" S is contained in Language("+nome2+")");
		else 
			console.append(" S is contained in Language("+nome1+")");
		console.append("\nwhereas\n");
		if(ss1not)
			console.append(" S is not contained in Language("+nome1+")");
		else 
			console.append(" S is not contained in Language("+nome2+")");
	}			
	
	
	public static PTPartition initPTPartition(
		ProcessAG processWAG
	) {
		PTPartition p = null;
		PTStatesSet ptstatesSet = new PTStatesSet();
		StatesSet statesSet = null;
		StatesSet destinations = null;
		Vector<TransitionCouple> transitionCouples = null;
		TransitionCouple tc = null;
		PTState ptstate = null;
		PTState ptstatePre = null;

		statesSet = processWAG.getStatesSet();
		
		for (int i=0; i<statesSet.getDimension(); i++) {
			ptstatesSet.addPTState(new PTState(statesSet.stateAt(i)));
		}
		
		for (int i=0; i<statesSet.getDimension(); i++) {
			transitionCouples = statesSet.stateAt(i).getTransitionCouples();
			ptstatePre = ptstatesSet.getPTState(statesSet.stateAt(i));
			for (int j=0; j<transitionCouples.size(); j++) {
				tc = transitionCouples.elementAt(j);
				destinations = tc.getDestinationsSet();
				for (int k=0; k<destinations.getDimension(); k++) {
					ptstate = ptstatesSet.getPTState(destinations.stateAt(k));
					ptstate.addPresetCouple(tc.getAction(), ptstatePre);
				}
			}
		}
		p = new PTPartition(ptstatesSet);

		return p;
	}


	
}
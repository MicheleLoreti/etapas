package org.cmg.tapas.formulae.actl.path;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import org.cmg.tapas.formulae.actl.ActlActionPredicate;
import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlPathFormulaVisitor;
import org.cmg.tapas.formulae.actl.PathFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.filter.Filter;
import org.cmg.tapas.processAlgebra.APProcess;


public class ActlUntil<X extends APProcess<X ,Y>,Y extends ActionInterface> 
				extends PathFormula<X, Y> {

	private ActlFormula<X, Y> formula1;
	private ActlActionPredicate<X,Y> filter1;
	
	private ActlActionPredicate<X,Y> filter2;
	private ActlFormula<X, Y> formula2;
	
	
	public ActlUntil(ActlFormula<X, Y> f1
			, ActlActionPredicate<X,Y> chi1
			, ActlFormula<X, Y> f2) {
		super();
		this.formula1 = f1;
		this.filter1 = chi1;
		this.filter2 = null;
		this.formula2 = f2;
	}	
	
	public ActlFormula<X, Y> getFormula1() {
		return formula1;
	}

	public Filter<Y> getFilter1() {
		return filter1.getFilter();
	}

	public Filter<Y> getFilter2() {
		return filter2.getFilter();
	}

	public ActlFormula<X, Y> getFormula2() {
		return formula2;
	}

	public ActlUntil(ActlFormula<X, Y> f1
			, ActlActionPredicate<X,Y> chi1
			, ActlActionPredicate<X,Y> chi2
			, ActlFormula<X, Y> f2) {
		super();
		this.formula1 = f1;
		this.filter1 = chi1;
		this.filter2 = chi2;
		this.formula2 = f2;
	}

	@Override
	protected String _toString() {
		String res = " ("+formula1+") {"+filter1
					 +"} "+this.operator()+" ";
		
		if(filter2 != null)
			res += "{"+filter2+"} ";
		
		res += "("+formula2+") ";
		
		return res;
	}

	@Override
	protected PathFormula<X,Y> doClone() {
		return new ActlUntil<X, Y>(formula1.clone()
				, filter1, filter2
				, formula2.clone());
	}

	@Override
	public String getUnicode() {		
		String res = " ("+formula1.getUnicode()+") {"+filter1
		 +"} "+this.operator()+" ";

		if(filter2 != null) {
			res += "{"+filter2+"} ";
		}

		res += "("+formula2.getUnicode()+") ";

		return res;
	}

	@Override
	public String operator() {
		return "U";
	}

	@Override
	public boolean sat(X x, boolean forall) {
		if (forall) {
			return checkForAll( x , new HashSet<X>(), new HashSet<X>(), new HashSet<X>());
		} else {
			return checkExists( x , new HashSet<X>() , new HashSet<X>(), new HashSet<X>());
		}
	}

	private boolean checkForAll(X x, HashSet<X> hashSet, HashSet<X> satSet, HashSet<X> unsatSet) {		
		if (satSet.contains(x)) {
			return true;
		}
		if (unsatSet.contains(x)) {
			return false;
		}
		if (hashSet.contains(x)) {
			return false;
		}
		if ((filter2 == null)&&(formula2.satisfies(x))) {
			return true;
		}
		if (!formula1.satisfies(x)) {
			return false;
		}
		hashSet.add(x);
		Map<Y,LinkedList<X>> next = x.getNext();
		boolean flag = false;
		for (Y a : next.keySet()) {
			LinkedList<X> nextList = next.get(a);
			if (!nextList.isEmpty()) {
				flag = true;
			}
			boolean sat1 = filter1.getFilter().check(a);
			boolean sat2 = (filter2!=null)&&(filter2.getFilter().check(a));
			if (!sat1 && !sat2 && (!next.isEmpty())) {
				unsatSet.add(x);
				hashSet.remove(x);
				return false;
			} 
			if (sat2 && !sat1) {
				for (X p : next.get(a)) {
					if (hashSet.contains(p)||!formula2.satisfies(p)) {
						unsatSet.add(x);
						hashSet.remove(x);
						return false;
					}
				}
			}
			if (sat1 && !sat2) {
				for (X p : nextList) {
					if (!checkForAll(p, hashSet,satSet,unsatSet)) {
						unsatSet.add(x);
						hashSet.remove(x);
						return false;
					}
				}
			}
			if (sat1 && sat2) {
				for (X p : next.get(a)) {
					if (hashSet.contains(p)||!(formula2.satisfies(p)||checkForAll(p, hashSet, satSet, unsatSet))) {
						unsatSet.add(x);
						hashSet.remove(x);
						return false;
					}
				}
			}
		}
		if (!flag) {
			unsatSet.add(x);
			hashSet.remove(x);
			return false;
		}
		hashSet.remove(x);
		satSet.add(x);
		return true;
	}

	private boolean checkExists(X x, HashSet<X> hashSet, HashSet<X> satSet, HashSet<X> unsatSet) {
		if (satSet.contains(x)) {
			return true;
		}
		if (unsatSet.contains(x)) {
			return false;
		}
		if (hashSet.contains(x)) {
			return false;
		}
		if ((filter2 == null)&&(formula2.satisfies(x))) {
			return true;
		}
		if (!formula1.satisfies(x)) {
			return false;
		}
		hashSet.add(x);
		Map<Y,LinkedList<X>> next = x.getNext();
		for (Y a : next.keySet()) {
			boolean sat1 = filter1.getFilter().check(a);
			boolean sat2 = (filter2!=null)&&(filter2.getFilter().check(a));
			if (!sat1 && !sat2) { //ERRORE
				unsatSet.add(x);
//				return false; //Per correggere l'errore
			} 
			if (sat2 && !sat1) {
				for (X p : next.get(a)) {
					if ((!hashSet.contains(p))&&formula2.satisfies(p)) {
						satSet.add(x);
						return true;
					}
				}
			}
			if (sat1 && !sat2) {
				for (X p : next.get(a)) {
					if (checkExists(p, hashSet,satSet,unsatSet)) {
						satSet.add(x);
						return true;
					}
				}
			}
			if (sat1 && sat2) {
				for (X p : next.get(a)) {
					if ((!hashSet.contains(p))&&(formula2.satisfies(p)||checkExists(p, hashSet,satSet,unsatSet))) {
						satSet.add(x);
						return true;
					}
				}
			}
		}
		hashSet.remove(x);
		unsatSet.add(x);
		return false;
	}

	public ActlActionPredicate<X, Y> getActionPredicate1() {
		return filter1;
	}

	public ActlActionPredicate<X, Y> getActionPredicate2() {
		return filter2;
	}

	@Override
	protected void doVisit(ActlPathFormulaVisitor<X, Y> visitor) {
		visitor.visitUntilFormula(this);
	}
}

/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hml Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getHmlFormula()
 * @model
 * @generated
 */
public interface HmlFormula extends EObject
{
} // HmlFormula

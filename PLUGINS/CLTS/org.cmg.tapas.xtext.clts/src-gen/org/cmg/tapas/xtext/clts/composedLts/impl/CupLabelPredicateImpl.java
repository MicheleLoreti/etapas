/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate;
import org.cmg.tapas.xtext.clts.composedLts.LabelPredicate;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cup Label Predicate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.CupLabelPredicateImpl#getLeft <em>Left</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.CupLabelPredicateImpl#getRigh <em>Righ</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CupLabelPredicateImpl extends LabelPredicateImpl implements CupLabelPredicate
{
  /**
   * The cached value of the '{@link #getLeft() <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeft()
   * @generated
   * @ordered
   */
  protected LabelPredicate left;

  /**
   * The cached value of the '{@link #getRigh() <em>Righ</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRigh()
   * @generated
   * @ordered
   */
  protected LabelPredicate righ;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CupLabelPredicateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.CUP_LABEL_PREDICATE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelPredicate getLeft()
  {
    return left;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLeft(LabelPredicate newLeft, NotificationChain msgs)
  {
    LabelPredicate oldLeft = left;
    left = newLeft;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT, oldLeft, newLeft);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLeft(LabelPredicate newLeft)
  {
    if (newLeft != left)
    {
      NotificationChain msgs = null;
      if (left != null)
        msgs = ((InternalEObject)left).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT, null, msgs);
      if (newLeft != null)
        msgs = ((InternalEObject)newLeft).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT, null, msgs);
      msgs = basicSetLeft(newLeft, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT, newLeft, newLeft));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LabelPredicate getRigh()
  {
    return righ;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRigh(LabelPredicate newRigh, NotificationChain msgs)
  {
    LabelPredicate oldRigh = righ;
    righ = newRigh;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH, oldRigh, newRigh);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRigh(LabelPredicate newRigh)
  {
    if (newRigh != righ)
    {
      NotificationChain msgs = null;
      if (righ != null)
        msgs = ((InternalEObject)righ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH, null, msgs);
      if (newRigh != null)
        msgs = ((InternalEObject)newRigh).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH, null, msgs);
      msgs = basicSetRigh(newRigh, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH, newRigh, newRigh));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT:
        return basicSetLeft(null, msgs);
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH:
        return basicSetRigh(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT:
        return getLeft();
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH:
        return getRigh();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT:
        setLeft((LabelPredicate)newValue);
        return;
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH:
        setRigh((LabelPredicate)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT:
        setLeft((LabelPredicate)null);
        return;
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH:
        setRigh((LabelPredicate)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__LEFT:
        return left != null;
      case ComposedLtsPackage.CUP_LABEL_PREDICATE__RIGH:
        return righ != null;
    }
    return super.eIsSet(featureID);
  }

} //CupLabelPredicateImpl

package org.cmg.tapas.graph.filter;

public abstract class AbstractFilter<T> implements Filter<T>{

	public abstract boolean equals(Object obj);
}

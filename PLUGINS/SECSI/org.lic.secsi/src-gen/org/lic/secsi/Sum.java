/**
 */
package org.lic.secsi;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.lic.secsi.Sum#getAction <em>Action</em>}</li>
 *   <li>{@link org.lic.secsi.Sum#getLeft <em>Left</em>}</li>
 *   <li>{@link org.lic.secsi.Sum#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.lic.secsi.SecsiPackage#getSum()
 * @model
 * @generated
 */
public interface Sum extends EObject
{
  /**
   * Returns the value of the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Action</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Action</em>' containment reference.
   * @see #setAction(Proc)
   * @see org.lic.secsi.SecsiPackage#getSum_Action()
   * @model containment="true"
   * @generated
   */
  Proc getAction();

  /**
   * Sets the value of the '{@link org.lic.secsi.Sum#getAction <em>Action</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Action</em>' containment reference.
   * @see #getAction()
   * @generated
   */
  void setAction(Proc value);

  /**
   * Returns the value of the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Left</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Left</em>' containment reference.
   * @see #setLeft(Sum)
   * @see org.lic.secsi.SecsiPackage#getSum_Left()
   * @model containment="true"
   * @generated
   */
  Sum getLeft();

  /**
   * Sets the value of the '{@link org.lic.secsi.Sum#getLeft <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Left</em>' containment reference.
   * @see #getLeft()
   * @generated
   */
  void setLeft(Sum value);

  /**
   * Returns the value of the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Right</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Right</em>' containment reference.
   * @see #setRight(Sum)
   * @see org.lic.secsi.SecsiPackage#getSum_Right()
   * @model containment="true"
   * @generated
   */
  Sum getRight();

  /**
   * Sets the value of the '{@link org.lic.secsi.Sum#getRight <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Right</em>' containment reference.
   * @see #getRight()
   * @generated
   */
  void setRight(Sum value);

} // Sum

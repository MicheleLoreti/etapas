/**
 */
package org.cmg.tapas.xtext.clts.composedLts.impl;

import java.util.Collection;

import org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage;
import org.cmg.tapas.xtext.clts.composedLts.LabelRule;
import org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody;
import org.cmg.tapas.xtext.clts.composedLts.LtsRule;
import org.cmg.tapas.xtext.clts.composedLts.LtsState;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lts Declaration Body</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl#getStates <em>States</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl#getInit <em>Init</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl#getRules <em>Rules</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl#getLabels <em>Labels</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LtsDeclarationBodyImpl extends LtsBodyImpl implements LtsDeclarationBody
{
  /**
   * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStates()
   * @generated
   * @ordered
   */
  protected EList<LtsState> states;

  /**
   * The cached value of the '{@link #getInit() <em>Init</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInit()
   * @generated
   * @ordered
   */
  protected EList<LtsState> init;

  /**
   * The cached value of the '{@link #getRules() <em>Rules</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRules()
   * @generated
   * @ordered
   */
  protected EList<LtsRule> rules;

  /**
   * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabels()
   * @generated
   * @ordered
   */
  protected EList<LabelRule> labels;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LtsDeclarationBodyImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ComposedLtsPackage.Literals.LTS_DECLARATION_BODY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LtsState> getStates()
  {
    if (states == null)
    {
      states = new EObjectContainmentEList<LtsState>(LtsState.class, this, ComposedLtsPackage.LTS_DECLARATION_BODY__STATES);
    }
    return states;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LtsState> getInit()
  {
    if (init == null)
    {
      init = new EObjectResolvingEList<LtsState>(LtsState.class, this, ComposedLtsPackage.LTS_DECLARATION_BODY__INIT);
    }
    return init;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LtsRule> getRules()
  {
    if (rules == null)
    {
      rules = new EObjectContainmentEList<LtsRule>(LtsRule.class, this, ComposedLtsPackage.LTS_DECLARATION_BODY__RULES);
    }
    return rules;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LabelRule> getLabels()
  {
    if (labels == null)
    {
      labels = new EObjectContainmentEList<LabelRule>(LabelRule.class, this, ComposedLtsPackage.LTS_DECLARATION_BODY__LABELS);
    }
    return labels;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_DECLARATION_BODY__STATES:
        return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
      case ComposedLtsPackage.LTS_DECLARATION_BODY__RULES:
        return ((InternalEList<?>)getRules()).basicRemove(otherEnd, msgs);
      case ComposedLtsPackage.LTS_DECLARATION_BODY__LABELS:
        return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_DECLARATION_BODY__STATES:
        return getStates();
      case ComposedLtsPackage.LTS_DECLARATION_BODY__INIT:
        return getInit();
      case ComposedLtsPackage.LTS_DECLARATION_BODY__RULES:
        return getRules();
      case ComposedLtsPackage.LTS_DECLARATION_BODY__LABELS:
        return getLabels();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_DECLARATION_BODY__STATES:
        getStates().clear();
        getStates().addAll((Collection<? extends LtsState>)newValue);
        return;
      case ComposedLtsPackage.LTS_DECLARATION_BODY__INIT:
        getInit().clear();
        getInit().addAll((Collection<? extends LtsState>)newValue);
        return;
      case ComposedLtsPackage.LTS_DECLARATION_BODY__RULES:
        getRules().clear();
        getRules().addAll((Collection<? extends LtsRule>)newValue);
        return;
      case ComposedLtsPackage.LTS_DECLARATION_BODY__LABELS:
        getLabels().clear();
        getLabels().addAll((Collection<? extends LabelRule>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_DECLARATION_BODY__STATES:
        getStates().clear();
        return;
      case ComposedLtsPackage.LTS_DECLARATION_BODY__INIT:
        getInit().clear();
        return;
      case ComposedLtsPackage.LTS_DECLARATION_BODY__RULES:
        getRules().clear();
        return;
      case ComposedLtsPackage.LTS_DECLARATION_BODY__LABELS:
        getLabels().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ComposedLtsPackage.LTS_DECLARATION_BODY__STATES:
        return states != null && !states.isEmpty();
      case ComposedLtsPackage.LTS_DECLARATION_BODY__INIT:
        return init != null && !init.isEmpty();
      case ComposedLtsPackage.LTS_DECLARATION_BODY__RULES:
        return rules != null && !rules.isEmpty();
      case ComposedLtsPackage.LTS_DECLARATION_BODY__LABELS:
        return labels != null && !labels.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //LtsDeclarationBodyImpl

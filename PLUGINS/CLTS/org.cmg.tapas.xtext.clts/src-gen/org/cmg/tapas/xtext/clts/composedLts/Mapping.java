/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.Mapping#getSrc <em>Src</em>}</li>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.Mapping#getTrg <em>Trg</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getMapping()
 * @model
 * @generated
 */
public interface Mapping extends EObject
{
  /**
   * Returns the value of the '<em><b>Src</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Src</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Src</em>' reference.
   * @see #setSrc(Action)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getMapping_Src()
   * @model
   * @generated
   */
  Action getSrc();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.Mapping#getSrc <em>Src</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Src</em>' reference.
   * @see #getSrc()
   * @generated
   */
  void setSrc(Action value);

  /**
   * Returns the value of the '<em><b>Trg</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trg</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trg</em>' reference.
   * @see #setTrg(Action)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getMapping_Trg()
   * @model
   * @generated
   */
  Action getTrg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.Mapping#getTrg <em>Trg</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trg</em>' reference.
   * @see #getTrg()
   * @generated
   */
  void setTrg(Action value);

} // Mapping

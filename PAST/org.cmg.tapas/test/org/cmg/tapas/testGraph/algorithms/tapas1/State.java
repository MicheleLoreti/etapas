package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;

/**
 * La classe State rappresenta uno stato di un grafo. 
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 */
public class State {
	
	/**
	 * Nome dello stato. 
	 **/	
	private String name = null;

	/**
	 * Rappresenta le transizioni uscenti da questo stato. 
	 **/	
	private Vector<TransitionCouple> transitionCouples = null;

	/**
	 * Costruisce un nuovo stato.
	 **/	
  	public State(String name) {
  		setName(name);
  		transitionCouples = new Vector<TransitionCouple>();
  	}

	/**
	 * Restituisce il nome dello stato.
	 */  	
  	public String getName() {
		return name;
  	}  	

	/**
	 * Setta il nome dello stato.
	 */
  	private void setName(String name) {
  		this.name = name;
  	}

	/**
	 * Restituisce un vettore contenente oggetti 
	 * di tipo <tt>TransitionCouple</tt>, ciascuno dei quali 
	 * rappresenta tutti gli stati raggiungibili dallo 
	 * stato corrente con una certa etichetta.
	 **/  	
	public Vector<TransitionCouple> getTransitionCouples() {
		return transitionCouples;	
	}

	/**
	 * Aggiunge una nuova transizione uscente a questo corrente. 
	 * Le transizioni uscenti sono organizzate
	 * in oggetti di tipo <tt>TransitionCouple</tt>. 
	 * Tali oggetti sono memorizzati nel vettore <tt>transitionCouples</tt>
	 * in maniera ordinata rispetto alle azioni. 
	 * Ciascun elemento del vettore rappresenta un'azione e tutti gli
	 * stati raggiungibili dallo stato corrente con tale azione. 
	 * L'inserimento di una nuova transizione 
	 * mantiene ordinata la struttura dati. 
	 * Inoltre non � possibile aggiungere transizioni duplicate, in tal caso
	 * viene restituito il valore booleano false.
	 **/ 
	public boolean addTransition(Action action, State destination) {
		TransitionCouple tc = null;
		Action a = null;
		int compare = 0;
		int i = 0;

		for (i=0; i<transitionCouples.size(); i++) {
			tc = transitionCouples.elementAt(i);
			a = tc.getAction();
			compare = a.compareTo(action);
			if( compare == 0 ) 
				return tc.addDestination(destination);
			else if (compare > 0) 
				break;
		}	
		tc = new TransitionCouple(action);
		transitionCouples.add(i,tc);
		return tc.addDestination(destination);
	}

	/**
	 * Rimuove una transizione uscente da questo stato.
	 **/ 
	public boolean removeTransition(Action action, State destination) {
		TransitionCouple tc = null;
		int compare = 0;
		int low = 0;
		int high = transitionCouples.size() - 1;
		int mid = 0;
		while (low <= high) {
			mid = (low + high) / 2;
			tc = transitionCouples.elementAt(mid);
			compare = tc.getAction().compareTo(action);
			if( compare < 0 ) 
				low = mid + 1;
			else if( compare > 0 ) 
				high = mid -1;
			else { 
				if( tc.removeDestination(destination) ) {
					if( tc.getDestinationsSet().isEmpty() ) 
						transitionCouples.remove(mid);
					return true;
				}

				return false;
			}
		}
		
		return false;
	}

	/**
 	 * Implementa la funzione CP. Tale funzione � cos� definita 
 	 * <p>
 	 * 		CP(s) = { a | esiste uno stato s' tale che s --a--> s' }
 	 * <p>
 	 **/
	public ActionsSet cP() {
		ActionsSet as = new	ActionsSet();
		TransitionCouple tc = null;
		
		for( int i=0; i<transitionCouples.size(); i++ ) {
			tc = transitionCouples.elementAt(i);
			as.addAction(tc.getAction());	
		}
		
		return as;
	}

	/**
 	 * Implementa la funzione DP(s,a). Tale funzione � cos� definita 
 	 * <p>
 	 * 		DP(s,a) = { s' | s --a--> s' }
 	 * <p>
 	 **/
	public StatesSet dP(Action action) {
		TransitionCouple tc = null;
		int compare = 0;
		int low = 0;
		int high = transitionCouples.size() - 1;
		int mid = 0;
		while (low <= high) {
			mid = (low + high) / 2;
			tc = transitionCouples.elementAt(mid);
			compare = tc.getAction().compareTo(action);
			if( compare < 0 ) 
				low = mid + 1;
			else if( compare > 0 ) 
				high = mid -1;
			else 
				return tc.getDestinationsSet();
		}
		
		return new StatesSet();
	}

	/**
 	 * Effettua la epsilon chiusura dello stato. Tale funzione � cos� definita 
 	 * <p>
 	 * 		sEpsilon(s) = { s' | s --tau-->^* s' }
 	 * <p>
 	 **/
	public StatesSet sEpsilon() {
		StatesSet sepsilon = new StatesSet();
		StatesSet temp = null;

		sepsilon.addState(this);
		temp = sepsilon.dP( Action.ACTION_TAU );
		if( !temp.isEmpty() )
			if( sepsilon.addStatesSet(temp) )
				return sepsilon.sEpsilon();		

		return sepsilon;
	}

	/**
 	 * Controlla se lo stato corrente � convergente
 	 * cio� se non esistono sequenze infinite di transizioni
 	 * etichettate con  <tt>tau</tt> che abbiano origine in tale stato.
 	 * (Il metodo � errato).
 	 **/ 
 	 // Totalmente errato, molti sistemi convergenti risultano
 	 // non convergenti. (Guzman)
 	 // Vedere le correzioni successive.
	public boolean stateConvergentOld() {
		StatesSet currentStates = null;
		StatesSet convergentStates = new StatesSet();
		StatesSet resultStates = new StatesSet();
		
		convergentStates.addState(this);
		resultStates.addState(this);
		int sentinella = 1;
		while (true) {
			currentStates = resultStates;
			resultStates = currentStates.dP( Action.ACTION_TAU );
			if (!(resultStates.isEmpty())) {
				convergentStates.addStatesSet(resultStates);
				if(	sentinella+resultStates.getDimension() != convergentStates.getDimension() ) 
					return false;
				sentinella = convergentStates.getDimension();
			}
			else 
				return true;
		}
	}
	
	/**
 	 * Controlla se lo stato corrente � convergente, 
 	 * cio� se non esistono sequenze infinite di transizioni
 	 * etichettate con <tt>tau</tt> che abbiano origine in tale stato.
 	 */	
 	 // Lento, ma corretto (Guzman).
 	 // Si veda il metodo precedente ed il commento successivo.
	public boolean stateConvergent() {
		HashMap<State, Integer> loopSizeMap = Components.computeComponents(this);
		StatesSet closure =	sEpsilon();
		for( int i=0; i<closure.getDimension(); ++i )
			if( loopSizeMap.get( closure.stateAt(i) )>0 )
				return false;
		
		return true;
	}
	
// _____________________________________________________________________________
// Quella che segue � una toppa ma corregge il calcolo della convergenza
// che era drasticamente errato (molti sistemi risultavano non convergenti
// pur essendo convergenti).
// Il calcolo delle componenti fortemente connesse � eseguito in modo 
// ricorsivo per cui sistemi con molti stati 
// possono dare problemi di memoria. Sarebbe possibile eliminare la ricorsione
// simulandola con uno stack ma richiederebbe sforzo e tempo.
// Sarebbe invece pi� importante, prima di apportare ottimizzazioni, eseguire
// il calcolo delle componenti fortemente connesse con un'unica passata
// per tutti gli stati invece che ridursi a calcolarle per i singoli stati
// ogni volta che c'� bisogno. Un tale approccio richiederebbe per� di 
// cambiare l'organizzazione degli algoritmi (per esempio, MustChecker,
// MustUtil, TestingUtil) e onestamente faccio fatica ad 
// avere la visione globale necessaria.
// (Guzman)

	public static class Components {
		// used for depth first search
		private static HashMap<State, Integer> dfsIndexMap;
		private static HashMap<State, Integer> loopSizeMap;
		private static int dfs;
		private static LinkedList<State> dfsStack;
	
		/**
		 * Calcola le componenti fortemente connesse 
		 * del sottografo associato allo stato
		 * specificato.
		 **/	
		public static HashMap<State, Integer> computeComponents(State state) {
			// init maps		
			dfsIndexMap = new HashMap<State, Integer>();
			loopSizeMap = new HashMap<State, Integer>();
			dfs = 0;
			dfsStack = new LinkedList<State>();
			
			// compute components of the subgraph associated to the given state
			dfsComponents(state);			
			
			return loopSizeMap;		
		}
	
		/** 
		 * Calcola le componenti del sottografo associato allo stato specificato. 
		 * Il metodo � ricorsivo.
		 * (Assume che il sistema non abbia pi� di 1000000 stati).
		 **/
		private static int dfsComponents(State state) {
			Integer rootI = dfsIndexMap.get(state);
			if( rootI!=null )
				return rootI;
				
			int myDfs = ++dfs;	
			dfsIndexMap.put( state, myDfs );
			int root = myDfs;		
			int stackSize = dfsStack.size();
			dfsStack.add( state );			
			
			StatesSet postset = state.dP( Action.ACTION_TAU );
			boolean selfLoops = false;
			if( postset!=null ) {
				for( int i=0; i<postset.getDimension(); ++i ) {
					State post = postset.stateAt(i);
					if( post==state )
						selfLoops = true;
					rootI = dfsComponents(post);
					if( rootI < root )
						root = rootI;
				}				
			}													
			
			if( root == myDfs ) {			
				int cycleCount = dfsStack.size() - stackSize;
				int loop = 
					cycleCount>1 ? cycleCount : 
					selfLoops ? 1 : 0;
				State item;
				for( int i=0; i<cycleCount; ++i ) {
					item = dfsStack.removeLast();
					// 1000000+myDfs plays the role of a flag since
					// it is greater than any other dfs.
					dfsIndexMap.put( item, 1000000 + myDfs	);				
					loopSizeMap.put( item, loop );
				} 
			}
			
			return root;
		}
	}
	
// _____________________________________________________________________________


	/**
 	 * Controlla se lo stato corrente � uno stato finale, 
 	 * cio� se lo stato corrente non ha alcuna transizione uscente.
 	 **/
	public boolean stateFinal() {
		return transitionCouples.size() == 0;
	}

	
	
	/**
	 * Esegue un confronto lessicografico tra il nome di 
	 * questo stato ed il nome dello stato passato come parametro.
	 **/
	public int compareTo(State s) {
		return name.compareTo(s.getName());
	}

	/**
	 * Stampa il nome dello stato sullo standard output. 
	 **/
  	public void print() {
  		System.out.print(name);
  	}

	/**
	 * Stampa tutte le transizioni uscenti dallo stato corrente 
	 * sullo standard output. 
	 **/ 
	public void printTransitions() {
		TransitionCouple tc = null;
		StatesSet destinations = null;
		
		for( int i=0; i<transitionCouples.size(); i++ ) {
			tc = transitionCouples.elementAt(i);
			destinations = tc.getDestinationsSet();
			for( int j=0; j<destinations.getDimension(); j++ ) {
				System.out.print("           ");
				this.print();
				System.out.print(" ---");
				tc.getAction().print();
				System.out.print("---> ");
				destinations.stateAt(j).print();
				System.out.println();
			}
		}
	}
}



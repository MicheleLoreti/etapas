/**
 */
package org.lic.secsi.impl;

import org.eclipse.emf.ecore.EClass;

import org.lic.secsi.SecsiPackage;
import org.lic.secsi.TypeElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TypeElementImpl extends ReferenceableElementImpl implements TypeElement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TypeElementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SecsiPackage.Literals.TYPE_ELEMENT;
  }

} //TypeElementImpl

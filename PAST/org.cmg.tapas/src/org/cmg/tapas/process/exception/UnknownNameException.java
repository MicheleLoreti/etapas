/**
 * 
 */
package org.cmg.tapas.process.exception;



/**
 * @author loreti
 *
 */
public class UnknownNameException extends ProcessException {

	public UnknownNameException( String name ) {
		super(name);
	}
	
}

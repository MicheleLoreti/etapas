package org.cmg.tapas.testGraph.algorithms;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.cmg.tapas.graph.ExpandedGraph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.LabelledState;
import org.cmg.tapas.graph.MappedGraph;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.algorithms.ActionLabel;
import org.cmg.tapas.graph.algorithms.Evaluator;
import org.cmg.tapas.graph.algorithms.GraphAnalyzer;
import org.cmg.tapas.graph.algorithms.GraphAnalyzer2;
import org.cmg.tapas.graph.algorithms.LoopInfo;
import org.cmg.tapas.graph.algorithms.bisimulation.BisimulationChecker;
import org.cmg.tapas.testGraph.GraphItemFactory;
import org.cmg.tapas.testGraph.LtsAction;
import org.cmg.tapas.testGraph.LtsState;
import org.cmg.tapas.testGraph.algorithms.tapas1.Lts;
import org.cmg.tapas.testGraph.algorithms.tapas1.bisimulation.TBisimulationChecker;

public class TestGraph extends TestCase{
	
	public void testCollapse() {		
		int repetitions = 1000;
		int actionCount = 1;		
		int statesNumber = 80;
		//double connectionProbability = 0.00005;		// 1
		//double connectionProbability = 0.01;			// 20..30
		double connectionProbability = 0.035;			// 70..80 - 1
		//double connectionProbability = 0.04;			// 75..80 - 1
		//double connectionProbability = 0.05;			// 79..80 - 1
		//double connectionProbability = 0.07;			// 80 - 1
		//double connectionProbability = 0.1;			// 1

		// _____________________________________________________________________

		GraphItemFactory graphFactory = new GraphItemFactory();
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>( graphFactory );
		GraphAnalyzer2<LtsState,LtsAction> analyzer; 

		GraphInterface<LtsState,LtsAction> graph;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		MappedGraph<LtsState,LtsAction,? extends Set<LtsState>> collapsed;
		GraphInterface<LtsState,LtsAction> newGraph;
		
		LtsAction[] actions = new LtsAction[actionCount];
		for( int i=0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );
		
		Evaluator<LtsState,? extends Set<LtsState>> map;
		HashSet<Set<LtsState>> values;
		for( int i=0; i<repetitions; ++i ) {							
			// generate random graph (according to settings)			
			pointedGraph = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph = pointedGraph.getGraph();
			analyzer = new GraphAnalyzer2<LtsState,LtsAction>( 
				graph, graphFactory
			);

			// bisim->collapse->bisim			
			BisimulationChecker<LtsState,LtsAction> bisimChecker = 
				new BisimulationChecker<LtsState,LtsAction>(graph);
			map = bisimChecker.computeEquivalence(null);
			values = new HashSet<Set<LtsState>>();						
			Iterator<LtsState> iter = graph.getStatesIterator();
			while( iter.hasNext()  ) {
				values.add( map.eval( iter.next() ) );				
			}
		
			collapsed = analyzer.collapse( map, analyzer.TRANSITIONS_ALL );
			newGraph = collapsed.getGraph();
			int stateCount = newGraph.numStates();
			assertEquals(values.size(), stateCount);
		}				
	}	
	
	public void testConvergence(){	
		int repetitions = 100;
		int statesNumber = 250;
		double connectionProbability = 0.005;

		// _____________________________________________________________________

		GraphInterface<LtsState,LtsAction> graph;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		GraphItemFactory graphFactory = new GraphItemFactory();
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>(graphFactory);
		GraphAnalyzer<LtsState,LtsAction> analyzer; 			
				
		LtsAction[] actions = {LtsAction.TAU};
		boolean tapas1Res=true, tapas2Res=true;
		for( int i=0; i<repetitions; ++i ) {							
			// generate random graphs (according to settings)			
			pointedGraph = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph = pointedGraph.getGraph();
			analyzer = new GraphAnalyzer<LtsState,LtsAction>(graph);
			LtsState initState = pointedGraph.getState1();
			
			// Tapas2
			LoopInfo<LtsState,LtsAction> info = analyzer.computeComponents(
				analyzer.TAU_FILTER, null
			);
			tapas2Res = info.isConvergent(initState);
			
			// Tapas1
			Lts lts = Lts.createProcess(graph, initState, null);
			tapas1Res = lts.getInitState().stateConvergent();

			assertEquals(tapas2Res, tapas1Res);
		}
	}
	
	public void testDelabel(){
		int repetitions = 100;
		int actionCount = 2;
		int statesNumber = 40;
		double connectionProbability = 0.2;		
		
		// Data
		PointedGraph<LtsState,LtsAction> pointedGraph;
		GraphInterface<LtsState,LtsAction> graph;
		LtsAction[] actions = new LtsAction[actionCount];
		for( int i=0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );

		for( int i=0; i<repetitions; ++i ) {
			RandomGraphGenerator<LtsState,LtsAction> generator = 
				new RandomGraphGenerator<LtsState,LtsAction>(
					new GraphItemFactory()
				);
			pointedGraph = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph = pointedGraph.getGraph();
	
			LtsAction soleAction = new LtsAction("_");
	
			GraphAnalyzer<LtsState,LtsAction> analyzer = 
				new GraphAnalyzer<LtsState,LtsAction>(graph);
			ExpandedGraph<
				LtsState, 
				LabelledState<ActionLabel<LtsAction>>, 
				LtsAction
			> expanded = analyzer.deLabel( soleAction );
			
			GraphInterface<LabelledState<ActionLabel<LtsAction>>, LtsAction>			
				newGraph = expanded.getGraph();
			Map<LtsState, LabelledState<ActionLabel<LtsAction>> > 
				map = expanded.getMap();
			
			boolean res = true;
			if( 
				newGraph.numStates()!=graph.numStates() + graph.numEdges() ||
				newGraph.numEdges()!=2*graph.numEdges() 
			) {
				res = false;
			}
			
			assertTrue(res);				
			
			Iterator<LtsState> iter = graph.getStatesIterator();
			while( res && iter.hasNext() ) {
				LtsState state = iter.next();
				for( LtsAction action: actions ) {
					ActionLabel<LtsAction> label = new ActionLabel<LtsAction>(
						action
					);
					if( graph.getPostset(state, action)!=null ) {
						Set<LabelledState<ActionLabel<LtsAction>>> image =
							newGraph.getPostset( map.get(state), soleAction );
						res = false;
						for( LabelledState<?> newState: image )
							res |= newState.getLabel().equals( label );	
						    assertTrue(res);
					}						
				}					
			}
			
			assertTrue(res);
		}
	}
	
	public void testFusion(){
		int repetitions = 3;
		int actionCount = 10;		
		int statesNumber = 800;
		double connectionProbability = 0.04;

		// _____________________________________________________________________

		GraphInterface<LtsState,LtsAction> graph1;
		GraphInterface<LtsState,LtsAction> graph2;
		GraphInterface<LtsState,LtsAction> graph;
		PointedGraph<LtsState,LtsAction> pointedGraph1;
		PointedGraph<LtsState,LtsAction> pointedGraph2;
		PointedGraph<LtsState,LtsAction> pointedGraph;
		GraphItemFactory graphFactory = new GraphItemFactory();
		RandomGraphGenerator<LtsState,LtsAction> generator = 
			new RandomGraphGenerator<LtsState,LtsAction>(graphFactory);
		
		LtsAction[] actions = new LtsAction[actionCount];
		for( int i=0; i<actionCount; ++i )
			actions[i] = new LtsAction( new Integer(i).toString() );
		
		boolean tapas1Res=true, bisimRes=true, fusionRes=true;
		double bisimTime=0, tapas1Time=0, fusionTime=0;
		double genTime=0;
		double start;
		Evaluator<LtsState,?> map;
		for( int i=0; i<repetitions; ++i ) {								
			// generate random graphs (according to settings)			
			start = System.currentTimeMillis();
			pointedGraph1 = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph1 = pointedGraph1.getGraph();
			LtsState initState1 = pointedGraph1.getState1();
			pointedGraph2 = generator.generateRandomGraph(
				statesNumber, actions, connectionProbability
			);
			graph2 = pointedGraph2.getGraph();
			LtsState initState2 = pointedGraph2.getState1();

			genTime += System.currentTimeMillis() - start;
			
			// Bisim(graph1, graph2)
			start = System.currentTimeMillis();
			BisimulationChecker<LtsState,LtsAction> bisimChecker = 
				new BisimulationChecker<LtsState,LtsAction>(graph1, graph2);
			map = bisimChecker.computeEquivalence(null);
			bisimRes = map.eval(initState1)==map.eval(initState2);
			bisimTime += System.currentTimeMillis() - start;
			
			// fusion(graph1, graph2)
			start = System.currentTimeMillis();
			pointedGraph = GraphAnalyzer2.fusion(
				graphFactory, graph1, graph2, initState1, initState2
			);			
			graph = pointedGraph.getGraph();
			bisimChecker = new BisimulationChecker<LtsState,LtsAction>( graph );				
			map = bisimChecker.computeEquivalence(null);
			fusionRes = 
				map.eval(pointedGraph.getState1()) == 
				map.eval(pointedGraph.getState2());
			fusionTime += System.currentTimeMillis() - start;
			
			// Tapas1
			start = System.currentTimeMillis();
			TBisimulationChecker<LtsState,LtsAction> 
				checker = new TBisimulationChecker<LtsState,LtsAction>();
	    	checker.setParameters(graph1,graph2, initState1,initState2, false); 
			tapas1Res = checker.checkEquivalence();
			tapas1Time += System.currentTimeMillis() - start;

			// check equality of results
			if( bisimRes != tapas1Res ) {
				System.out.println("Errore: bisim");
				fail();
			}
			if( fusionRes != tapas1Res ) {
				fail();
			}
		}
	}	
}

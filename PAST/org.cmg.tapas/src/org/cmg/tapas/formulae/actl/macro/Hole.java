/**
 * 
 */
package org.cmg.tapas.formulae.actl.macro;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public class Hole<S extends APProcess<S, A>, A extends ActionInterface> extends StateFormulaMacro< S, A >{

	@Override
	public ActlFormula<S, A> getFormula(ActlFormula<S, A>... f) {
		if (f.length != 1) {
			throw new IllegalArgumentException();
		}
		return f[0];
	}

	@Override
	public int parameters() {
		return 1;
	}

	@Override
	public void acceptVisitor(MacroVisitor<S, A> visitor) {
		visitor.visitHole(this);
	}

}

package org.cmg.tapas.testGraph.algorithms.tapas1;

import java.util.Vector;

/**
 * La classe Partition rappresenta un insieme di insiemi
 * di stati (non si assicura che esso sia veramente
 * una partizione).
 * <p>
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class Partition {
	/**
	 * Rappresenta la partizione come insieme di insiemi di stati.
	 **/
	private Vector<StatesSet> partition = null; 

	/**
	 * Costruisce una nuova partizione vuota. 
	 **/
	public Partition() {
		partition = new Vector<StatesSet>();
	}

	/**
	 * Costruisce una nuova partizione inizializzata 
	 * con il solo insieme di stati passato in ingresso. 
	 **/	
	public Partition(StatesSet stateSet) {
		partition = new Vector<StatesSet>();
		partition.addElement(stateSet);
	}

	/**
	 * Restituisce il numero di elementi contenuti nell'insieme.
	 **/   		
  	public int getDimension() {
  		return partition.size();
  	}

	/**
	 * Aggiunge un oggetto all'insieme.
	 **/
	public void addStatesSet(StatesSet ss) {
		partition.addElement(ss);	
	}

	/**
	 * Rimuove dall'insieme il primo insieme di stati. 
	 **/	
	public StatesSet removeFirstStatesSet() {
		return partition.remove(0);	
	}	

	/**
	 * Restituisce l'insieme di stati contenuto nella 
	 * posizione specificata come parametro di ingresso. 
	 **/	
	public StatesSet statesSetAt(int index) {
		return partition.elementAt(index);	
	}

	/**
	 * Controlla se la partizione � vuota. 
	 **/
	public boolean isEmpty() { 
		return partition.isEmpty();	
	}	

	/**
	 * Controlla se i due stati passati come parametri 
	 * appartengono allo stesso insieme all'interno
	 * della partizione. 
	 **/	
	public boolean checkSameClass(State s1, State s2) {
		StatesSet statesSet = null;
		int trovato = 0;
		for( int i=0; i<getDimension(); i++ ) {
			statesSet = statesSetAt(i);
			trovato = 0;
			if (statesSet.containState(s1)) 
				trovato++;
			if (statesSet.containState(s2)) 
				trovato++;
			if( trovato == 2 ) 
				return true;
			else if( trovato == 1 ) 
				return false;
		}
		
		return false;
	}

	/**
	 * Stampa l'intera partizione
	 **/ 
	public void print() {
		int i = 0;
		
		System.out.print("{");
		for (i=0; i<this.getDimension(); i++) {
			if (i>0) System.out.print(", ");
				partition.elementAt(i).print();
		}
		if (i==0) 
			System.out.print("empty");
			
		System.out.print("}");
	}  	

}
package org.cmg.tapas.testGraph.algorithms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.graph.Graph;
import org.cmg.tapas.graph.GraphInterface;
import org.cmg.tapas.graph.GraphItemFactoryI;
import org.cmg.tapas.graph.PointedGraph;
import org.cmg.tapas.graph.StateInterface;

/** 
 * RandomGraphGenerator � un generatore di grafi casuali.
 * <p>
 * @param <S> tipo degli stati
 * @param <A> tipo delle azioni
 * 
 * @author Guzman Tierno
 **/
public class RandomGraphGenerator<
	S extends StateInterface,
	A extends ActionInterface
> {
	// OPTION: pi� archi tra due stati.
	
	private Random random = new Random();
	
	/** Fabbrica di nodi e di azioni. **/
	protected GraphItemFactoryI<S,A> itemFactory;		
			
	/** Costruisce un RandomGraphGenerator. **/
	public RandomGraphGenerator(GraphItemFactoryI<S,A> itemFactory) {
		this.itemFactory = itemFactory;
	}

	/** Rende un nuovo nodo (stato). **/
	protected S newState(String name) {
		return itemFactory.newState(name);
	}

	/** Rende una nuova azione. **/
	protected A newAction(String name) {
		return itemFactory.newAction(name);
	}
	
	/** Setta la fabbrica di nodi e di azioni. **/
	public void setGraphItemFactory(GraphItemFactoryI<S,A> itemFactory) {
		this.itemFactory = itemFactory;
	}
	
	
	/** 
	 * Genera un grafo casuale con il numero di nodi 
	 * specificato e la probabilit� di connessione tra due nodi
	 * specificata.
	 * Rende una terna composta dal grafo generato e due suoi
	 * stati casuali non coincidenti.
	 **/
	public PointedGraph<S,A> generateRandomGraph(
		int nodes, 
		A[] actions,
		double connectionProbability
	) {
		GraphInterface<S,A> graph = new Graph<S,A>();
		if(nodes<=0)
			return new PointedGraph<S,A>(graph, null, null);			
		
		int actionCount = actions.length;
		int point1Index = random.nextInt(nodes);
		int point2Index = random.nextInt(nodes);
		if(nodes>1)
			while( point2Index==point1Index )
				point2Index = random.nextInt(nodes);
			
		S point1 = null;
		S point2 = null;
		S state;
		for( int i=0; i<nodes; ++i ) {
			state = newState( Integer.toString(i) );	
			graph.addState( state );
			if( i==point1Index )
				point1 = state;
			if( i==point2Index )
				point2 = state;				
		}

		Iterator<S> iterator1 = graph.getStatesIterator();
		Iterator<S> iterator2;
		S state1;
		S state2;
		while( iterator1.hasNext() ) {
			state1 = iterator1.next();			
			iterator2 = graph.getStatesIterator();
			while( iterator2.hasNext() ) {
				state2 = iterator2.next();	
				A action = actions[random.nextInt(actionCount)];
				if( random.nextDouble()<connectionProbability )
					graph.addEdge(state1, action, state2);
			}
		}
				
		return new PointedGraph<S,A>(graph, point1, point2);
	}

	/** 
	 * Genera un grafo casuale con il numero di nodi 
	 * specificato e la probabilit� di connessione tra due nodi
	 * specificata.
	 * Rende una terna composta dal grafo generato e due suoi
	 * stati casuali non coincidenti.
	 * Il metodo funziona bene ed � efficiente 
	 * se la probabilit� di connessione non � alta (<0.3).
	 **/
	public PointedGraph<S,A> generateRandomGraph2(
		int nodes, 
		A[] actions,
		double connectionProbability
	) {
		GraphInterface<S,A> graph = new Graph<S,A>();
		if(nodes<=0)
			return new PointedGraph<S,A>(graph, null, null);			
		
		int actionCount = actions.length;
		int point1Index = random.nextInt(nodes);
		int point2Index = random.nextInt(nodes);
		if(nodes>1)
			while( point2Index==point1Index )
				point2Index = random.nextInt(nodes);
			
		S point1 = null;
		S point2 = null;
		S state;
		HashMap<Integer, S> map = new HashMap<Integer, S>((int)(nodes/0.75) + 1);
		for( int i=0; i<nodes; ++i ) {
			state = newState( Integer.toString(i) );	
			graph.addState( state );
			map.put(i, state );
			if( i==point1Index )
				point1 = state;
			if( i==point2Index )
				point2 = state;				
		}

		int edges = (int)(connectionProbability*nodes*nodes);
		S state1;
		S state2;
		int i=0;
		while( i<edges ) {
			state1 = map.get( random.nextInt(nodes) );
			state2 = map.get( random.nextInt(nodes) );
			A action = actions[random.nextInt(actionCount)];
			if( graph.addEdge(state1, action, state2) )
				i++;
		}
				
		return new PointedGraph<S,A>(graph, point1, point2);
	}

	/** 
	 * Genera un grafo casuale con il numero di nodi 
	 * specificato, la probabilit� di connessione tra due nodi
	 * specificata e la percentuale di tau specificata.
	 * Si assume che tau sia la prima azione del vettore.
	 * Tutti gli altri archi sono etichettati con
	 * l'azione passata.
	 * Rende una terna composta dal grafo generato e due suoi
	 * stati casuali non coincidenti.
	 **/
	public PointedGraph<S,A> generateRandomGraph(
		int nodes, 
		A[] actions,
		double connectionProbability,
		double tauProbability
	) {
		GraphInterface<S,A> graph = new Graph<S,A>();
		if(nodes<=0)
			return new PointedGraph<S,A>(graph, null, null);			
		
		int point1Index = random.nextInt(nodes);
		int point2Index = random.nextInt(nodes);
		if(nodes>1)
			while( point2Index==point1Index )
				point2Index = random.nextInt(nodes);
			
		S point1 = null;
		S point2 = null;
		S state;
		for( int i=0; i<nodes; ++i ) {
			state = newState( Integer.toString(i) );	
			graph.addState( state );
			if( i==point1Index )
				point1 = state;
			if( i==point2Index )
				point2 = state;				
		}

		Iterator<S> iterator1 = graph.getStatesIterator();
		Iterator<S> iterator2;
		S state1;
		S state2;
		int i = 0;
		A action;
		A tauAction = actions[0];
		int others = actions.length-1;
		while( iterator1.hasNext() ) {
			state1 = iterator1.next();			
			iterator2 = graph.getStatesIterator();
			int j = 0;
			while( iterator2.hasNext() ) {
				state2 = iterator2.next();	
				if( random.nextDouble()<connectionProbability ) {
					if( random.nextDouble()<tauProbability )
						action = tauAction;
					else
						action = actions[random.nextInt(others)+1];					
					graph.addEdge(state1, action, state2);
				}
				j++;
			}
			i++;
		}
				
		return new PointedGraph<S,A>(graph, point1, point2);
	}


}
















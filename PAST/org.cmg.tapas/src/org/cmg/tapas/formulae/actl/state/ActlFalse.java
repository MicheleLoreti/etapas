/**
 * 
 */
package org.cmg.tapas.formulae.actl.state;


import org.cmg.tapas.formulae.actl.ActlFormula;
import org.cmg.tapas.formulae.actl.ActlFormulaVisitor;
import org.cmg.tapas.graph.ActionInterface;
import org.cmg.tapas.processAlgebra.APProcess;


/**
 * @author loreti
 *
 */
public final class ActlFalse<X extends APProcess<X ,Y>,Y extends ActionInterface> extends ActlFormula<X,Y> {

	@Override
	protected ActlFormula<X, Y> doClone() {
		return this;
	}

	@Override
	protected void doVisit(ActlFormulaVisitor<X, Y> visitor) {
		visitor.visitFalse(this);
	}

	@Override
	protected String _toString() {
		return "false";
	}

	@Override
	public String getUnicode() {
		return "false";
	}

	@Override
	public String operator() {
		return "";
	}

	@Override
	public boolean satisfies(X x) {
		return false;
	}
}

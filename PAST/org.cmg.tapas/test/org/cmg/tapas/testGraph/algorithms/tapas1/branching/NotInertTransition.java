package org.cmg.tapas.testGraph.algorithms.tapas1.branching;

import org.cmg.tapas.testGraph.algorithms.tapas1.Action;

/**
 * @author Gori Massimiliano
 * @author Guzman Tierno
 **/
public class NotInertTransition {
	
	private BBState p1 = null;
	private Action a = null;	
	private BBState p2 = null;	
  	
  	public NotInertTransition(BBState p1, Action a, BBState p2) {
  		this.p1 = p1;
  		this.a = a;
  		this.p2 = p2;  		  		
  	}
  	
  	public BBState getOrigin() {
  		return p1;
  	}

  	public BBState getDestination() {
  		return p2;
  	}
  	
  	public Action getAction() {
  		return a;
  	}

	public int compareTo(NotInertTransition t) {
		int compare = 0;
		
		if( t!=null ) {
			compare = a.compareTo( t.getAction() );
			if( compare == 0 ) {
				compare = p1.getState().compareTo( t.getOrigin().getState() );
				if( compare == 0 )
					return p2.getState().compareTo(t.getDestination().getState());					
				return compare;
			} 
            
			if( a.isTau() ) 
				return -1;
			if( t.getAction().isTau() ) 
				return 1;            
			return compare;
		}
		
		return -1;
	}

	public void print() {
		print(System.out);
	}

	public void print(java.io.PrintStream console) {
		p1.printAll(console);
		console.append(" ---");
		a.print(console);
		console.append("---> ");
		p2.printAll(console);
	}
}
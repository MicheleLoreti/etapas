/**
 */
package org.cmg.tapas.xtext.clts.composedLts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ltl Not Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula#getArg <em>Arg</em>}</li>
 * </ul>
 *
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtlNotFormula()
 * @model
 * @generated
 */
public interface LtlNotFormula extends LtlFormula
{
  /**
   * Returns the value of the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arg</em>' containment reference.
   * @see #setArg(LtlFormula)
   * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsPackage#getLtlNotFormula_Arg()
   * @model containment="true"
   * @generated
   */
  LtlFormula getArg();

  /**
   * Sets the value of the '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula#getArg <em>Arg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arg</em>' containment reference.
   * @see #getArg()
   * @generated
   */
  void setArg(LtlFormula value);

} // LtlNotFormula

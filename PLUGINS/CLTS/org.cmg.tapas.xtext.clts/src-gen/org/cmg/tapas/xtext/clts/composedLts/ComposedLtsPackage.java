/**
 */
package org.cmg.tapas.xtext.clts.composedLts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.cmg.tapas.xtext.clts.composedLts.ComposedLtsFactory
 * @model kind="package"
 * @generated
 */
public interface ComposedLtsPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "composedLts";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.cmg.org/tapas/xtext/clts/ComposedLts";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "composedLts";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ComposedLtsPackage eINSTANCE = org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl.init();

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ModelImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__NAME = 0;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__ELEMENTS = 1;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ElementImpl <em>Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ElementImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getElement()
   * @generated
   */
  int ELEMENT = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT__NAME = 0;

  /**
   * The number of structural features of the '<em>Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsImpl <em>Lts</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLts()
   * @generated
   */
  int LTS = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS__NAME = ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS__BODY = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Lts</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsBodyImpl <em>Lts Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsBodyImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsBody()
   * @generated
   */
  int LTS_BODY = 3;

  /**
   * The number of structural features of the '<em>Lts Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_BODY_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsCompositionImpl <em>Lts Composition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsCompositionImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsComposition()
   * @generated
   */
  int LTS_COMPOSITION = 4;

  /**
   * The feature id for the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_COMPOSITION__BODY = LTS_BODY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Lts Composition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_COMPOSITION_FEATURE_COUNT = LTS_BODY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.CompositionBodyImpl <em>Composition Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.CompositionBodyImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getCompositionBody()
   * @generated
   */
  int COMPOSITION_BODY = 5;

  /**
   * The number of structural features of the '<em>Composition Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPOSITION_BODY_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ReferenceImpl <em>Reference</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ReferenceImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getReference()
   * @generated
   */
  int REFERENCE = 6;

  /**
   * The feature id for the '<em><b>Lts</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE__LTS = COMPOSITION_BODY_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Reference</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCE_FEATURE_COUNT = COMPOSITION_BODY_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.MappingImpl <em>Mapping</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.MappingImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getMapping()
   * @generated
   */
  int MAPPING = 7;

  /**
   * The feature id for the '<em><b>Src</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__SRC = 0;

  /**
   * The feature id for the '<em><b>Trg</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING__TRG = 1;

  /**
   * The number of structural features of the '<em>Mapping</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAPPING_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl <em>Lts Declaration Body</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsDeclarationBody()
   * @generated
   */
  int LTS_DECLARATION_BODY = 8;

  /**
   * The feature id for the '<em><b>States</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_DECLARATION_BODY__STATES = LTS_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Init</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_DECLARATION_BODY__INIT = LTS_BODY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Rules</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_DECLARATION_BODY__RULES = LTS_BODY_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Labels</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_DECLARATION_BODY__LABELS = LTS_BODY_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Lts Declaration Body</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_DECLARATION_BODY_FEATURE_COUNT = LTS_BODY_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl <em>Lts Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsRule()
   * @generated
   */
  int LTS_RULE = 9;

  /**
   * The feature id for the '<em><b>Src</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_RULE__SRC = 0;

  /**
   * The feature id for the '<em><b>Act</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_RULE__ACT = 1;

  /**
   * The feature id for the '<em><b>Trg</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_RULE__TRG = 2;

  /**
   * The number of structural features of the '<em>Lts Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_RULE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsStateImpl <em>Lts State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsStateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsState()
   * @generated
   */
  int LTS_STATE = 10;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_STATE__NAME = 0;

  /**
   * The number of structural features of the '<em>Lts State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTS_STATE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LabelRuleImpl <em>Label Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LabelRuleImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLabelRule()
   * @generated
   */
  int LABEL_RULE = 11;

  /**
   * The feature id for the '<em><b>Label</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_RULE__LABEL = 0;

  /**
   * The feature id for the '<em><b>States</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_RULE__STATES = 1;

  /**
   * The number of structural features of the '<em>Label Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_RULE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.StateImpl <em>State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.StateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getState()
   * @generated
   */
  int STATE = 12;

  /**
   * The feature id for the '<em><b>Is Initial</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE__IS_INITIAL = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE__NAME = 1;

  /**
   * The feature id for the '<em><b>Rules</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE__RULES = 2;

  /**
   * The number of structural features of the '<em>State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STATE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.RuleImpl <em>Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.RuleImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getRule()
   * @generated
   */
  int RULE = 13;

  /**
   * The feature id for the '<em><b>Action</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RULE__ACTION = 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RULE__NEXT = 1;

  /**
   * The number of structural features of the '<em>Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RULE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LabelImpl <em>Label</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LabelImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLabel()
   * @generated
   */
  int LABEL = 14;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL__NAME = ELEMENT__NAME;

  /**
   * The number of structural features of the '<em>Label</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ActionImpl <em>Action</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ActionImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getAction()
   * @generated
   */
  int ACTION = 15;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION__NAME = ELEMENT__NAME;

  /**
   * The number of structural features of the '<em>Action</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.FormulaImpl <em>Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.FormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getFormula()
   * @generated
   */
  int FORMULA = 16;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA__NAME = ELEMENT__NAME;

  /**
   * The number of structural features of the '<em>Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaDeclarationImpl <em>Hml Formula Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaDeclarationImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlFormulaDeclaration()
   * @generated
   */
  int HML_FORMULA_DECLARATION = 17;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_FORMULA_DECLARATION__NAME = FORMULA__NAME;

  /**
   * The feature id for the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_FORMULA_DECLARATION__FORMULA = FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Hml Formula Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_FORMULA_DECLARATION_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaImpl <em>Hml Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlFormula()
   * @generated
   */
  int HML_FORMULA = 18;

  /**
   * The number of structural features of the '<em>Hml Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_FORMULA_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlPredicateImpl <em>Hml Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlPredicate()
   * @generated
   */
  int HML_PREDICATE = 19;

  /**
   * The feature id for the '<em><b>Label</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_PREDICATE__LABEL = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Hml Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_PREDICATE_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlTrueImpl <em>Hml True</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlTrueImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlTrue()
   * @generated
   */
  int HML_TRUE = 20;

  /**
   * The number of structural features of the '<em>Hml True</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_TRUE_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlFalseImpl <em>Hml False</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlFalseImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlFalse()
   * @generated
   */
  int HML_FALSE = 21;

  /**
   * The number of structural features of the '<em>Hml False</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_FALSE_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlNotFormulaImpl <em>Hml Not Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlNotFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlNotFormula()
   * @generated
   */
  int HML_NOT_FORMULA = 22;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_NOT_FORMULA__ARG = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Hml Not Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_NOT_FORMULA_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlDiamondFormulaImpl <em>Hml Diamond Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlDiamondFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlDiamondFormula()
   * @generated
   */
  int HML_DIAMOND_FORMULA = 23;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_DIAMOND_FORMULA__ACTION = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_DIAMOND_FORMULA__ARG = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Hml Diamond Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_DIAMOND_FORMULA_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlBoxFormulaImpl <em>Hml Box Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlBoxFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlBoxFormula()
   * @generated
   */
  int HML_BOX_FORMULA = 24;

  /**
   * The feature id for the '<em><b>Action</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_BOX_FORMULA__ACTION = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_BOX_FORMULA__ARG = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Hml Box Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_BOX_FORMULA_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionFormulaImpl <em>Hml Recursion Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlRecursionFormula()
   * @generated
   */
  int HML_RECURSION_FORMULA = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_RECURSION_FORMULA__NAME = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_RECURSION_FORMULA__ARG = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Hml Recursion Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_RECURSION_FORMULA_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlMinFixPointImpl <em>Hml Min Fix Point</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlMinFixPointImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlMinFixPoint()
   * @generated
   */
  int HML_MIN_FIX_POINT = 26;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_MIN_FIX_POINT__NAME = HML_RECURSION_FORMULA__NAME;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_MIN_FIX_POINT__ARG = HML_RECURSION_FORMULA__ARG;

  /**
   * The number of structural features of the '<em>Hml Min Fix Point</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_MIN_FIX_POINT_FEATURE_COUNT = HML_RECURSION_FORMULA_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlMaxFixPointImpl <em>Hml Max Fix Point</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlMaxFixPointImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlMaxFixPoint()
   * @generated
   */
  int HML_MAX_FIX_POINT = 27;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_MAX_FIX_POINT__NAME = HML_RECURSION_FORMULA__NAME;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_MAX_FIX_POINT__ARG = HML_RECURSION_FORMULA__ARG;

  /**
   * The number of structural features of the '<em>Hml Max Fix Point</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_MAX_FIX_POINT_FEATURE_COUNT = HML_RECURSION_FORMULA_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionVariableImpl <em>Hml Recursion Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionVariableImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlRecursionVariable()
   * @generated
   */
  int HML_RECURSION_VARIABLE = 28;

  /**
   * The feature id for the '<em><b>Refeerence</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_RECURSION_VARIABLE__REFEERENCE = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Hml Recursion Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_RECURSION_VARIABLE_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaDeclarationImpl <em>Ltl Formula Declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaDeclarationImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlFormulaDeclaration()
   * @generated
   */
  int LTL_FORMULA_DECLARATION = 29;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_DECLARATION__NAME = FORMULA__NAME;

  /**
   * The feature id for the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_DECLARATION__FORMULA = FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ltl Formula Declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_DECLARATION_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaImpl <em>Ltl Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlFormula()
   * @generated
   */
  int LTL_FORMULA = 30;

  /**
   * The number of structural features of the '<em>Ltl Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAtomicPropositionImpl <em>Ltl Atomic Proposition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlAtomicPropositionImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlAtomicProposition()
   * @generated
   */
  int LTL_ATOMIC_PROPOSITION = 31;

  /**
   * The feature id for the '<em><b>Prop</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ATOMIC_PROPOSITION__PROP = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ltl Atomic Proposition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ATOMIC_PROPOSITION_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlNextFormulaImpl <em>Ltl Next Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlNextFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlNextFormula()
   * @generated
   */
  int LTL_NEXT_FORMULA = 32;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_NEXT_FORMULA__ARG = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ltl Next Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_NEXT_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAlwaysFormulaImpl <em>Ltl Always Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlAlwaysFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlAlwaysFormula()
   * @generated
   */
  int LTL_ALWAYS_FORMULA = 33;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ALWAYS_FORMULA__ARG = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ltl Always Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_ALWAYS_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlEventuallyFormulaImpl <em>Ltl Eventually Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlEventuallyFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlEventuallyFormula()
   * @generated
   */
  int LTL_EVENTUALLY_FORMULA = 34;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_EVENTUALLY_FORMULA__ARG = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ltl Eventually Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_EVENTUALLY_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlNotFormulaImpl <em>Ltl Not Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlNotFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlNotFormula()
   * @generated
   */
  int LTL_NOT_FORMULA = 35;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_NOT_FORMULA__ARG = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ltl Not Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_NOT_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlTrueImpl <em>Ltl True</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlTrueImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlTrue()
   * @generated
   */
  int LTL_TRUE = 36;

  /**
   * The number of structural features of the '<em>Ltl True</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_TRUE_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlFalseImpl <em>Ltl False</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlFalseImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlFalse()
   * @generated
   */
  int LTL_FALSE = 37;

  /**
   * The number of structural features of the '<em>Ltl False</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FALSE_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LabelPredicateImpl <em>Label Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LabelPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLabelPredicate()
   * @generated
   */
  int LABEL_PREDICATE = 38;

  /**
   * The number of structural features of the '<em>Label Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LABEL_PREDICATE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.NotLabelPredicateImpl <em>Not Label Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.NotLabelPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getNotLabelPredicate()
   * @generated
   */
  int NOT_LABEL_PREDICATE = 39;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_LABEL_PREDICATE__ARG = LABEL_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Not Label Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOT_LABEL_PREDICATE_FEATURE_COUNT = LABEL_PREDICATE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ActionLabelPredicateImpl <em>Action Label Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ActionLabelPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getActionLabelPredicate()
   * @generated
   */
  int ACTION_LABEL_PREDICATE = 40;

  /**
   * The feature id for the '<em><b>Act</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_LABEL_PREDICATE__ACT = LABEL_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Action Label Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTION_LABEL_PREDICATE_FEATURE_COUNT = LABEL_PREDICATE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.AnyLabelPredicateImpl <em>Any Label Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.AnyLabelPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getAnyLabelPredicate()
   * @generated
   */
  int ANY_LABEL_PREDICATE = 41;

  /**
   * The number of structural features of the '<em>Any Label Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANY_LABEL_PREDICATE_FEATURE_COUNT = LABEL_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.InterleavingImpl <em>Interleaving</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.InterleavingImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getInterleaving()
   * @generated
   */
  int INTERLEAVING = 42;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVING__LEFT = COMPOSITION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVING__RIGHT = COMPOSITION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Interleaving</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERLEAVING_FEATURE_COUNT = COMPOSITION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.SynchronizationImpl <em>Synchronization</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.SynchronizationImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getSynchronization()
   * @generated
   */
  int SYNCHRONIZATION = 43;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNCHRONIZATION__LEFT = COMPOSITION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNCHRONIZATION__RIGHT = COMPOSITION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Synchronization</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNCHRONIZATION_FEATURE_COUNT = COMPOSITION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ControlledInteractionImpl <em>Controlled Interaction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ControlledInteractionImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getControlledInteraction()
   * @generated
   */
  int CONTROLLED_INTERACTION = 44;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROLLED_INTERACTION__LEFT = COMPOSITION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Action</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROLLED_INTERACTION__ACTION = COMPOSITION_BODY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROLLED_INTERACTION__RIGHT = COMPOSITION_BODY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Controlled Interaction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTROLLED_INTERACTION_FEATURE_COUNT = COMPOSITION_BODY_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.RenamingImpl <em>Renaming</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.RenamingImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getRenaming()
   * @generated
   */
  int RENAMING = 45;

  /**
   * The feature id for the '<em><b>Arg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RENAMING__ARG = COMPOSITION_BODY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Maps</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RENAMING__MAPS = COMPOSITION_BODY_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Renaming</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RENAMING_FEATURE_COUNT = COMPOSITION_BODY_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlOrFormulaImpl <em>Hml Or Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlOrFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlOrFormula()
   * @generated
   */
  int HML_OR_FORMULA = 46;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_OR_FORMULA__LEFT = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_OR_FORMULA__RIGHT = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Hml Or Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_OR_FORMULA_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlAndFormulaImpl <em>Hml And Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlAndFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlAndFormula()
   * @generated
   */
  int HML_AND_FORMULA = 47;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_AND_FORMULA__LEFT = HML_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_AND_FORMULA__RIGHT = HML_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Hml And Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int HML_AND_FORMULA_FEATURE_COUNT = HML_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlOrFormulaImpl <em>Ltl Or Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlOrFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlOrFormula()
   * @generated
   */
  int LTL_OR_FORMULA = 48;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_OR_FORMULA__LEFT = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_OR_FORMULA__RIGHT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Ltl Or Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_OR_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAndFormulaImpl <em>Ltl And Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlAndFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlAndFormula()
   * @generated
   */
  int LTL_AND_FORMULA = 49;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_AND_FORMULA__LEFT = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_AND_FORMULA__RIGHT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Ltl And Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_AND_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlUntilFormulaImpl <em>Ltl Until Formula</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlUntilFormulaImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlUntilFormula()
   * @generated
   */
  int LTL_UNTIL_FORMULA = 50;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNTIL_FORMULA__LEFT = LTL_FORMULA_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNTIL_FORMULA__RIGHT = LTL_FORMULA_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Ltl Until Formula</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_UNTIL_FORMULA_FEATURE_COUNT = LTL_FORMULA_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.CupLabelPredicateImpl <em>Cup Label Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.CupLabelPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getCupLabelPredicate()
   * @generated
   */
  int CUP_LABEL_PREDICATE = 51;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUP_LABEL_PREDICATE__LEFT = LABEL_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Righ</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUP_LABEL_PREDICATE__RIGH = LABEL_PREDICATE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Cup Label Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUP_LABEL_PREDICATE_FEATURE_COUNT = LABEL_PREDICATE_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.CapLabelPredicateImpl <em>Cap Label Predicate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.CapLabelPredicateImpl
   * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getCapLabelPredicate()
   * @generated
   */
  int CAP_LABEL_PREDICATE = 52;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAP_LABEL_PREDICATE__LEFT = LABEL_PREDICATE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAP_LABEL_PREDICATE__RIGHT = LABEL_PREDICATE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Cap Label Predicate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CAP_LABEL_PREDICATE_FEATURE_COUNT = LABEL_PREDICATE_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.tapas.xtext.clts.composedLts.Model#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Model#getName()
   * @see #getModel()
   * @generated
   */
  EAttribute getModel_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.tapas.xtext.clts.composedLts.Model#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Model#getElements()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Elements();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Element <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Element</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Element
   * @generated
   */
  EClass getElement();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.tapas.xtext.clts.composedLts.Element#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Element#getName()
   * @see #getElement()
   * @generated
   */
  EAttribute getElement_Name();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Lts <em>Lts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lts</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Lts
   * @generated
   */
  EClass getLts();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.Lts#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Lts#getBody()
   * @see #getLts()
   * @generated
   */
  EReference getLts_Body();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsBody <em>Lts Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lts Body</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsBody
   * @generated
   */
  EClass getLtsBody();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsComposition <em>Lts Composition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lts Composition</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsComposition
   * @generated
   */
  EClass getLtsComposition();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtsComposition#getBody <em>Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Body</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsComposition#getBody()
   * @see #getLtsComposition()
   * @generated
   */
  EReference getLtsComposition_Body();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.CompositionBody <em>Composition Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Composition Body</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CompositionBody
   * @generated
   */
  EClass getCompositionBody();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Reference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Reference</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Reference
   * @generated
   */
  EClass getReference();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.Reference#getLts <em>Lts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Lts</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Reference#getLts()
   * @see #getReference()
   * @generated
   */
  EReference getReference_Lts();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Mapping <em>Mapping</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mapping</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Mapping
   * @generated
   */
  EClass getMapping();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.Mapping#getSrc <em>Src</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Src</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Mapping#getSrc()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_Src();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.Mapping#getTrg <em>Trg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Trg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Mapping#getTrg()
   * @see #getMapping()
   * @generated
   */
  EReference getMapping_Trg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody <em>Lts Declaration Body</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lts Declaration Body</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody
   * @generated
   */
  EClass getLtsDeclarationBody();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getStates <em>States</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>States</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getStates()
   * @see #getLtsDeclarationBody()
   * @generated
   */
  EReference getLtsDeclarationBody_States();

  /**
   * Returns the meta object for the reference list '{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getInit <em>Init</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Init</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getInit()
   * @see #getLtsDeclarationBody()
   * @generated
   */
  EReference getLtsDeclarationBody_Init();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getRules <em>Rules</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rules</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getRules()
   * @see #getLtsDeclarationBody()
   * @generated
   */
  EReference getLtsDeclarationBody_Rules();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getLabels <em>Labels</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Labels</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsDeclarationBody#getLabels()
   * @see #getLtsDeclarationBody()
   * @generated
   */
  EReference getLtsDeclarationBody_Labels();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule <em>Lts Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lts Rule</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsRule
   * @generated
   */
  EClass getLtsRule();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getSrc <em>Src</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Src</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsRule#getSrc()
   * @see #getLtsRule()
   * @generated
   */
  EReference getLtsRule_Src();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getAct <em>Act</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Act</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsRule#getAct()
   * @see #getLtsRule()
   * @generated
   */
  EReference getLtsRule_Act();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtsRule#getTrg <em>Trg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Trg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsRule#getTrg()
   * @see #getLtsRule()
   * @generated
   */
  EReference getLtsRule_Trg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtsState <em>Lts State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lts State</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsState
   * @generated
   */
  EClass getLtsState();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.tapas.xtext.clts.composedLts.LtsState#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtsState#getName()
   * @see #getLtsState()
   * @generated
   */
  EAttribute getLtsState_Name();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule <em>Label Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Label Rule</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LabelRule
   * @generated
   */
  EClass getLabelRule();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Label</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LabelRule#getLabel()
   * @see #getLabelRule()
   * @generated
   */
  EReference getLabelRule_Label();

  /**
   * Returns the meta object for the reference list '{@link org.cmg.tapas.xtext.clts.composedLts.LabelRule#getStates <em>States</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>States</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LabelRule#getStates()
   * @see #getLabelRule()
   * @generated
   */
  EReference getLabelRule_States();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.State <em>State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>State</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.State
   * @generated
   */
  EClass getState();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.tapas.xtext.clts.composedLts.State#isIsInitial <em>Is Initial</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Is Initial</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.State#isIsInitial()
   * @see #getState()
   * @generated
   */
  EAttribute getState_IsInitial();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.tapas.xtext.clts.composedLts.State#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.State#getName()
   * @see #getState()
   * @generated
   */
  EAttribute getState_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.tapas.xtext.clts.composedLts.State#getRules <em>Rules</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Rules</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.State#getRules()
   * @see #getState()
   * @generated
   */
  EReference getState_Rules();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Rule <em>Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Rule</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Rule
   * @generated
   */
  EClass getRule();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.Rule#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Action</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Rule#getAction()
   * @see #getRule()
   * @generated
   */
  EReference getRule_Action();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.Rule#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Next</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Rule#getNext()
   * @see #getRule()
   * @generated
   */
  EReference getRule_Next();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Label <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Label</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Label
   * @generated
   */
  EClass getLabel();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Action <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Action
   * @generated
   */
  EClass getAction();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Formula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Formula
   * @generated
   */
  EClass getFormula();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration <em>Hml Formula Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Formula Declaration</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration
   * @generated
   */
  EClass getHmlFormulaDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration#getFormula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFormulaDeclaration#getFormula()
   * @see #getHmlFormulaDeclaration()
   * @generated
   */
  EReference getHmlFormulaDeclaration_Formula();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFormula <em>Hml Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFormula
   * @generated
   */
  EClass getHmlFormula();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlPredicate <em>Hml Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlPredicate
   * @generated
   */
  EClass getHmlPredicate();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlPredicate#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Label</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlPredicate#getLabel()
   * @see #getHmlPredicate()
   * @generated
   */
  EReference getHmlPredicate_Label();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlTrue <em>Hml True</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml True</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlTrue
   * @generated
   */
  EClass getHmlTrue();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlFalse <em>Hml False</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml False</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlFalse
   * @generated
   */
  EClass getHmlFalse();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula <em>Hml Not Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Not Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula
   * @generated
   */
  EClass getHmlNotFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlNotFormula#getArg()
   * @see #getHmlNotFormula()
   * @generated
   */
  EReference getHmlNotFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula <em>Hml Diamond Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Diamond Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula
   * @generated
   */
  EClass getHmlDiamondFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Action</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getAction()
   * @see #getHmlDiamondFormula()
   * @generated
   */
  EReference getHmlDiamondFormula_Action();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlDiamondFormula#getArg()
   * @see #getHmlDiamondFormula()
   * @generated
   */
  EReference getHmlDiamondFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula <em>Hml Box Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Box Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula
   * @generated
   */
  EClass getHmlBoxFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Action</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula#getAction()
   * @see #getHmlBoxFormula()
   * @generated
   */
  EReference getHmlBoxFormula_Action();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlBoxFormula#getArg()
   * @see #getHmlBoxFormula()
   * @generated
   */
  EReference getHmlBoxFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula <em>Hml Recursion Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Recursion Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula
   * @generated
   */
  EClass getHmlRecursionFormula();

  /**
   * Returns the meta object for the attribute '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getName()
   * @see #getHmlRecursionFormula()
   * @generated
   */
  EAttribute getHmlRecursionFormula_Name();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionFormula#getArg()
   * @see #getHmlRecursionFormula()
   * @generated
   */
  EReference getHmlRecursionFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlMinFixPoint <em>Hml Min Fix Point</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Min Fix Point</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlMinFixPoint
   * @generated
   */
  EClass getHmlMinFixPoint();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlMaxFixPoint <em>Hml Max Fix Point</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Max Fix Point</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlMaxFixPoint
   * @generated
   */
  EClass getHmlMaxFixPoint();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable <em>Hml Recursion Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Recursion Variable</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable
   * @generated
   */
  EClass getHmlRecursionVariable();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable#getRefeerence <em>Refeerence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Refeerence</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlRecursionVariable#getRefeerence()
   * @see #getHmlRecursionVariable()
   * @generated
   */
  EReference getHmlRecursionVariable_Refeerence();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration <em>Ltl Formula Declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Formula Declaration</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration
   * @generated
   */
  EClass getLtlFormulaDeclaration();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration#getFormula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFormulaDeclaration#getFormula()
   * @see #getLtlFormulaDeclaration()
   * @generated
   */
  EReference getLtlFormulaDeclaration_Formula();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFormula <em>Ltl Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFormula
   * @generated
   */
  EClass getLtlFormula();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition <em>Ltl Atomic Proposition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Atomic Proposition</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition
   * @generated
   */
  EClass getLtlAtomicProposition();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition#getProp <em>Prop</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Prop</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAtomicProposition#getProp()
   * @see #getLtlAtomicProposition()
   * @generated
   */
  EReference getLtlAtomicProposition_Prop();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula <em>Ltl Next Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Next Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula
   * @generated
   */
  EClass getLtlNextFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlNextFormula#getArg()
   * @see #getLtlNextFormula()
   * @generated
   */
  EReference getLtlNextFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula <em>Ltl Always Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Always Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula
   * @generated
   */
  EClass getLtlAlwaysFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAlwaysFormula#getArg()
   * @see #getLtlAlwaysFormula()
   * @generated
   */
  EReference getLtlAlwaysFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula <em>Ltl Eventually Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Eventually Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula
   * @generated
   */
  EClass getLtlEventuallyFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlEventuallyFormula#getArg()
   * @see #getLtlEventuallyFormula()
   * @generated
   */
  EReference getLtlEventuallyFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula <em>Ltl Not Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Not Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula
   * @generated
   */
  EClass getLtlNotFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlNotFormula#getArg()
   * @see #getLtlNotFormula()
   * @generated
   */
  EReference getLtlNotFormula_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlTrue <em>Ltl True</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl True</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlTrue
   * @generated
   */
  EClass getLtlTrue();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlFalse <em>Ltl False</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl False</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlFalse
   * @generated
   */
  EClass getLtlFalse();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LabelPredicate <em>Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Label Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LabelPredicate
   * @generated
   */
  EClass getLabelPredicate();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate <em>Not Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Not Label Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate
   * @generated
   */
  EClass getNotLabelPredicate();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.NotLabelPredicate#getArg()
   * @see #getNotLabelPredicate()
   * @generated
   */
  EReference getNotLabelPredicate_Arg();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate <em>Action Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Action Label Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate
   * @generated
   */
  EClass getActionLabelPredicate();

  /**
   * Returns the meta object for the reference '{@link org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate#getAct <em>Act</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Act</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.ActionLabelPredicate#getAct()
   * @see #getActionLabelPredicate()
   * @generated
   */
  EReference getActionLabelPredicate_Act();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.AnyLabelPredicate <em>Any Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Any Label Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.AnyLabelPredicate
   * @generated
   */
  EClass getAnyLabelPredicate();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Interleaving <em>Interleaving</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Interleaving</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Interleaving
   * @generated
   */
  EClass getInterleaving();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.Interleaving#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Interleaving#getLeft()
   * @see #getInterleaving()
   * @generated
   */
  EReference getInterleaving_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.Interleaving#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Interleaving#getRight()
   * @see #getInterleaving()
   * @generated
   */
  EReference getInterleaving_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Synchronization <em>Synchronization</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Synchronization</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Synchronization
   * @generated
   */
  EClass getSynchronization();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.Synchronization#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Synchronization#getLeft()
   * @see #getSynchronization()
   * @generated
   */
  EReference getSynchronization_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.Synchronization#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Synchronization#getRight()
   * @see #getSynchronization()
   * @generated
   */
  EReference getSynchronization_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction <em>Controlled Interaction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Controlled Interaction</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction
   * @generated
   */
  EClass getControlledInteraction();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction#getLeft()
   * @see #getControlledInteraction()
   * @generated
   */
  EReference getControlledInteraction_Left();

  /**
   * Returns the meta object for the reference list '{@link org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction#getAction <em>Action</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Action</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction#getAction()
   * @see #getControlledInteraction()
   * @generated
   */
  EReference getControlledInteraction_Action();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.ControlledInteraction#getRight()
   * @see #getControlledInteraction()
   * @generated
   */
  EReference getControlledInteraction_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.Renaming <em>Renaming</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Renaming</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Renaming
   * @generated
   */
  EClass getRenaming();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.Renaming#getArg <em>Arg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Arg</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Renaming#getArg()
   * @see #getRenaming()
   * @generated
   */
  EReference getRenaming_Arg();

  /**
   * Returns the meta object for the containment reference list '{@link org.cmg.tapas.xtext.clts.composedLts.Renaming#getMaps <em>Maps</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Maps</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.Renaming#getMaps()
   * @see #getRenaming()
   * @generated
   */
  EReference getRenaming_Maps();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula <em>Hml Or Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml Or Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula
   * @generated
   */
  EClass getHmlOrFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getLeft()
   * @see #getHmlOrFormula()
   * @generated
   */
  EReference getHmlOrFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlOrFormula#getRight()
   * @see #getHmlOrFormula()
   * @generated
   */
  EReference getHmlOrFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula <em>Hml And Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Hml And Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula
   * @generated
   */
  EClass getHmlAndFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula#getLeft()
   * @see #getHmlAndFormula()
   * @generated
   */
  EReference getHmlAndFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.HmlAndFormula#getRight()
   * @see #getHmlAndFormula()
   * @generated
   */
  EReference getHmlAndFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula <em>Ltl Or Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Or Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula
   * @generated
   */
  EClass getLtlOrFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula#getLeft()
   * @see #getLtlOrFormula()
   * @generated
   */
  EReference getLtlOrFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlOrFormula#getRight()
   * @see #getLtlOrFormula()
   * @generated
   */
  EReference getLtlOrFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula <em>Ltl And Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl And Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula
   * @generated
   */
  EClass getLtlAndFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula#getLeft()
   * @see #getLtlAndFormula()
   * @generated
   */
  EReference getLtlAndFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlAndFormula#getRight()
   * @see #getLtlAndFormula()
   * @generated
   */
  EReference getLtlAndFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula <em>Ltl Until Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ltl Until Formula</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula
   * @generated
   */
  EClass getLtlUntilFormula();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula#getLeft()
   * @see #getLtlUntilFormula()
   * @generated
   */
  EReference getLtlUntilFormula_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.LtlUntilFormula#getRight()
   * @see #getLtlUntilFormula()
   * @generated
   */
  EReference getLtlUntilFormula_Right();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate <em>Cup Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cup Label Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate
   * @generated
   */
  EClass getCupLabelPredicate();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getLeft()
   * @see #getCupLabelPredicate()
   * @generated
   */
  EReference getCupLabelPredicate_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getRigh <em>Righ</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Righ</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CupLabelPredicate#getRigh()
   * @see #getCupLabelPredicate()
   * @generated
   */
  EReference getCupLabelPredicate_Righ();

  /**
   * Returns the meta object for class '{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate <em>Cap Label Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cap Label Predicate</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate
   * @generated
   */
  EClass getCapLabelPredicate();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getLeft()
   * @see #getCapLabelPredicate()
   * @generated
   */
  EReference getCapLabelPredicate_Left();

  /**
   * Returns the meta object for the containment reference '{@link org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see org.cmg.tapas.xtext.clts.composedLts.CapLabelPredicate#getRight()
   * @see #getCapLabelPredicate()
   * @generated
   */
  EReference getCapLabelPredicate_Right();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ComposedLtsFactory getComposedLtsFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ModelImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODEL__NAME = eINSTANCE.getModel_Name();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__ELEMENTS = eINSTANCE.getModel_Elements();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ElementImpl <em>Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ElementImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getElement()
     * @generated
     */
    EClass ELEMENT = eINSTANCE.getElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsImpl <em>Lts</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLts()
     * @generated
     */
    EClass LTS = eINSTANCE.getLts();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS__BODY = eINSTANCE.getLts_Body();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsBodyImpl <em>Lts Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsBodyImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsBody()
     * @generated
     */
    EClass LTS_BODY = eINSTANCE.getLtsBody();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsCompositionImpl <em>Lts Composition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsCompositionImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsComposition()
     * @generated
     */
    EClass LTS_COMPOSITION = eINSTANCE.getLtsComposition();

    /**
     * The meta object literal for the '<em><b>Body</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_COMPOSITION__BODY = eINSTANCE.getLtsComposition_Body();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.CompositionBodyImpl <em>Composition Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.CompositionBodyImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getCompositionBody()
     * @generated
     */
    EClass COMPOSITION_BODY = eINSTANCE.getCompositionBody();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ReferenceImpl <em>Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ReferenceImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getReference()
     * @generated
     */
    EClass REFERENCE = eINSTANCE.getReference();

    /**
     * The meta object literal for the '<em><b>Lts</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REFERENCE__LTS = eINSTANCE.getReference_Lts();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.MappingImpl <em>Mapping</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.MappingImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getMapping()
     * @generated
     */
    EClass MAPPING = eINSTANCE.getMapping();

    /**
     * The meta object literal for the '<em><b>Src</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__SRC = eINSTANCE.getMapping_Src();

    /**
     * The meta object literal for the '<em><b>Trg</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAPPING__TRG = eINSTANCE.getMapping_Trg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl <em>Lts Declaration Body</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsDeclarationBodyImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsDeclarationBody()
     * @generated
     */
    EClass LTS_DECLARATION_BODY = eINSTANCE.getLtsDeclarationBody();

    /**
     * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_DECLARATION_BODY__STATES = eINSTANCE.getLtsDeclarationBody_States();

    /**
     * The meta object literal for the '<em><b>Init</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_DECLARATION_BODY__INIT = eINSTANCE.getLtsDeclarationBody_Init();

    /**
     * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_DECLARATION_BODY__RULES = eINSTANCE.getLtsDeclarationBody_Rules();

    /**
     * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_DECLARATION_BODY__LABELS = eINSTANCE.getLtsDeclarationBody_Labels();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl <em>Lts Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsRuleImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsRule()
     * @generated
     */
    EClass LTS_RULE = eINSTANCE.getLtsRule();

    /**
     * The meta object literal for the '<em><b>Src</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_RULE__SRC = eINSTANCE.getLtsRule_Src();

    /**
     * The meta object literal for the '<em><b>Act</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_RULE__ACT = eINSTANCE.getLtsRule_Act();

    /**
     * The meta object literal for the '<em><b>Trg</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTS_RULE__TRG = eINSTANCE.getLtsRule_Trg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtsStateImpl <em>Lts State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtsStateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtsState()
     * @generated
     */
    EClass LTS_STATE = eINSTANCE.getLtsState();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LTS_STATE__NAME = eINSTANCE.getLtsState_Name();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LabelRuleImpl <em>Label Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LabelRuleImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLabelRule()
     * @generated
     */
    EClass LABEL_RULE = eINSTANCE.getLabelRule();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LABEL_RULE__LABEL = eINSTANCE.getLabelRule_Label();

    /**
     * The meta object literal for the '<em><b>States</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LABEL_RULE__STATES = eINSTANCE.getLabelRule_States();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.StateImpl <em>State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.StateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getState()
     * @generated
     */
    EClass STATE = eINSTANCE.getState();

    /**
     * The meta object literal for the '<em><b>Is Initial</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STATE__IS_INITIAL = eINSTANCE.getState_IsInitial();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STATE__NAME = eINSTANCE.getState_Name();

    /**
     * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STATE__RULES = eINSTANCE.getState_Rules();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.RuleImpl <em>Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.RuleImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getRule()
     * @generated
     */
    EClass RULE = eINSTANCE.getRule();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RULE__ACTION = eINSTANCE.getRule_Action();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RULE__NEXT = eINSTANCE.getRule_Next();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LabelImpl <em>Label</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LabelImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLabel()
     * @generated
     */
    EClass LABEL = eINSTANCE.getLabel();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ActionImpl <em>Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ActionImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getAction()
     * @generated
     */
    EClass ACTION = eINSTANCE.getAction();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.FormulaImpl <em>Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.FormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getFormula()
     * @generated
     */
    EClass FORMULA = eINSTANCE.getFormula();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaDeclarationImpl <em>Hml Formula Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaDeclarationImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlFormulaDeclaration()
     * @generated
     */
    EClass HML_FORMULA_DECLARATION = eINSTANCE.getHmlFormulaDeclaration();

    /**
     * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_FORMULA_DECLARATION__FORMULA = eINSTANCE.getHmlFormulaDeclaration_Formula();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaImpl <em>Hml Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlFormula()
     * @generated
     */
    EClass HML_FORMULA = eINSTANCE.getHmlFormula();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlPredicateImpl <em>Hml Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlPredicate()
     * @generated
     */
    EClass HML_PREDICATE = eINSTANCE.getHmlPredicate();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_PREDICATE__LABEL = eINSTANCE.getHmlPredicate_Label();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlTrueImpl <em>Hml True</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlTrueImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlTrue()
     * @generated
     */
    EClass HML_TRUE = eINSTANCE.getHmlTrue();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlFalseImpl <em>Hml False</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlFalseImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlFalse()
     * @generated
     */
    EClass HML_FALSE = eINSTANCE.getHmlFalse();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlNotFormulaImpl <em>Hml Not Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlNotFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlNotFormula()
     * @generated
     */
    EClass HML_NOT_FORMULA = eINSTANCE.getHmlNotFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_NOT_FORMULA__ARG = eINSTANCE.getHmlNotFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlDiamondFormulaImpl <em>Hml Diamond Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlDiamondFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlDiamondFormula()
     * @generated
     */
    EClass HML_DIAMOND_FORMULA = eINSTANCE.getHmlDiamondFormula();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_DIAMOND_FORMULA__ACTION = eINSTANCE.getHmlDiamondFormula_Action();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_DIAMOND_FORMULA__ARG = eINSTANCE.getHmlDiamondFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlBoxFormulaImpl <em>Hml Box Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlBoxFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlBoxFormula()
     * @generated
     */
    EClass HML_BOX_FORMULA = eINSTANCE.getHmlBoxFormula();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_BOX_FORMULA__ACTION = eINSTANCE.getHmlBoxFormula_Action();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_BOX_FORMULA__ARG = eINSTANCE.getHmlBoxFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionFormulaImpl <em>Hml Recursion Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlRecursionFormula()
     * @generated
     */
    EClass HML_RECURSION_FORMULA = eINSTANCE.getHmlRecursionFormula();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute HML_RECURSION_FORMULA__NAME = eINSTANCE.getHmlRecursionFormula_Name();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_RECURSION_FORMULA__ARG = eINSTANCE.getHmlRecursionFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlMinFixPointImpl <em>Hml Min Fix Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlMinFixPointImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlMinFixPoint()
     * @generated
     */
    EClass HML_MIN_FIX_POINT = eINSTANCE.getHmlMinFixPoint();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlMaxFixPointImpl <em>Hml Max Fix Point</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlMaxFixPointImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlMaxFixPoint()
     * @generated
     */
    EClass HML_MAX_FIX_POINT = eINSTANCE.getHmlMaxFixPoint();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionVariableImpl <em>Hml Recursion Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlRecursionVariableImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlRecursionVariable()
     * @generated
     */
    EClass HML_RECURSION_VARIABLE = eINSTANCE.getHmlRecursionVariable();

    /**
     * The meta object literal for the '<em><b>Refeerence</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_RECURSION_VARIABLE__REFEERENCE = eINSTANCE.getHmlRecursionVariable_Refeerence();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaDeclarationImpl <em>Ltl Formula Declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaDeclarationImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlFormulaDeclaration()
     * @generated
     */
    EClass LTL_FORMULA_DECLARATION = eINSTANCE.getLtlFormulaDeclaration();

    /**
     * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_FORMULA_DECLARATION__FORMULA = eINSTANCE.getLtlFormulaDeclaration_Formula();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaImpl <em>Ltl Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlFormula()
     * @generated
     */
    EClass LTL_FORMULA = eINSTANCE.getLtlFormula();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAtomicPropositionImpl <em>Ltl Atomic Proposition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlAtomicPropositionImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlAtomicProposition()
     * @generated
     */
    EClass LTL_ATOMIC_PROPOSITION = eINSTANCE.getLtlAtomicProposition();

    /**
     * The meta object literal for the '<em><b>Prop</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_ATOMIC_PROPOSITION__PROP = eINSTANCE.getLtlAtomicProposition_Prop();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlNextFormulaImpl <em>Ltl Next Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlNextFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlNextFormula()
     * @generated
     */
    EClass LTL_NEXT_FORMULA = eINSTANCE.getLtlNextFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_NEXT_FORMULA__ARG = eINSTANCE.getLtlNextFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAlwaysFormulaImpl <em>Ltl Always Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlAlwaysFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlAlwaysFormula()
     * @generated
     */
    EClass LTL_ALWAYS_FORMULA = eINSTANCE.getLtlAlwaysFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_ALWAYS_FORMULA__ARG = eINSTANCE.getLtlAlwaysFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlEventuallyFormulaImpl <em>Ltl Eventually Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlEventuallyFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlEventuallyFormula()
     * @generated
     */
    EClass LTL_EVENTUALLY_FORMULA = eINSTANCE.getLtlEventuallyFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_EVENTUALLY_FORMULA__ARG = eINSTANCE.getLtlEventuallyFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlNotFormulaImpl <em>Ltl Not Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlNotFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlNotFormula()
     * @generated
     */
    EClass LTL_NOT_FORMULA = eINSTANCE.getLtlNotFormula();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_NOT_FORMULA__ARG = eINSTANCE.getLtlNotFormula_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlTrueImpl <em>Ltl True</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlTrueImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlTrue()
     * @generated
     */
    EClass LTL_TRUE = eINSTANCE.getLtlTrue();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlFalseImpl <em>Ltl False</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlFalseImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlFalse()
     * @generated
     */
    EClass LTL_FALSE = eINSTANCE.getLtlFalse();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LabelPredicateImpl <em>Label Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LabelPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLabelPredicate()
     * @generated
     */
    EClass LABEL_PREDICATE = eINSTANCE.getLabelPredicate();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.NotLabelPredicateImpl <em>Not Label Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.NotLabelPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getNotLabelPredicate()
     * @generated
     */
    EClass NOT_LABEL_PREDICATE = eINSTANCE.getNotLabelPredicate();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NOT_LABEL_PREDICATE__ARG = eINSTANCE.getNotLabelPredicate_Arg();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ActionLabelPredicateImpl <em>Action Label Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ActionLabelPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getActionLabelPredicate()
     * @generated
     */
    EClass ACTION_LABEL_PREDICATE = eINSTANCE.getActionLabelPredicate();

    /**
     * The meta object literal for the '<em><b>Act</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTION_LABEL_PREDICATE__ACT = eINSTANCE.getActionLabelPredicate_Act();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.AnyLabelPredicateImpl <em>Any Label Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.AnyLabelPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getAnyLabelPredicate()
     * @generated
     */
    EClass ANY_LABEL_PREDICATE = eINSTANCE.getAnyLabelPredicate();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.InterleavingImpl <em>Interleaving</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.InterleavingImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getInterleaving()
     * @generated
     */
    EClass INTERLEAVING = eINSTANCE.getInterleaving();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERLEAVING__LEFT = eINSTANCE.getInterleaving_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INTERLEAVING__RIGHT = eINSTANCE.getInterleaving_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.SynchronizationImpl <em>Synchronization</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.SynchronizationImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getSynchronization()
     * @generated
     */
    EClass SYNCHRONIZATION = eINSTANCE.getSynchronization();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYNCHRONIZATION__LEFT = eINSTANCE.getSynchronization_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYNCHRONIZATION__RIGHT = eINSTANCE.getSynchronization_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.ControlledInteractionImpl <em>Controlled Interaction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ControlledInteractionImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getControlledInteraction()
     * @generated
     */
    EClass CONTROLLED_INTERACTION = eINSTANCE.getControlledInteraction();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTROLLED_INTERACTION__LEFT = eINSTANCE.getControlledInteraction_Left();

    /**
     * The meta object literal for the '<em><b>Action</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTROLLED_INTERACTION__ACTION = eINSTANCE.getControlledInteraction_Action();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTROLLED_INTERACTION__RIGHT = eINSTANCE.getControlledInteraction_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.RenamingImpl <em>Renaming</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.RenamingImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getRenaming()
     * @generated
     */
    EClass RENAMING = eINSTANCE.getRenaming();

    /**
     * The meta object literal for the '<em><b>Arg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RENAMING__ARG = eINSTANCE.getRenaming_Arg();

    /**
     * The meta object literal for the '<em><b>Maps</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RENAMING__MAPS = eINSTANCE.getRenaming_Maps();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlOrFormulaImpl <em>Hml Or Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlOrFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlOrFormula()
     * @generated
     */
    EClass HML_OR_FORMULA = eINSTANCE.getHmlOrFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_OR_FORMULA__LEFT = eINSTANCE.getHmlOrFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_OR_FORMULA__RIGHT = eINSTANCE.getHmlOrFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.HmlAndFormulaImpl <em>Hml And Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.HmlAndFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getHmlAndFormula()
     * @generated
     */
    EClass HML_AND_FORMULA = eINSTANCE.getHmlAndFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_AND_FORMULA__LEFT = eINSTANCE.getHmlAndFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference HML_AND_FORMULA__RIGHT = eINSTANCE.getHmlAndFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlOrFormulaImpl <em>Ltl Or Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlOrFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlOrFormula()
     * @generated
     */
    EClass LTL_OR_FORMULA = eINSTANCE.getLtlOrFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_OR_FORMULA__LEFT = eINSTANCE.getLtlOrFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_OR_FORMULA__RIGHT = eINSTANCE.getLtlOrFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlAndFormulaImpl <em>Ltl And Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlAndFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlAndFormula()
     * @generated
     */
    EClass LTL_AND_FORMULA = eINSTANCE.getLtlAndFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_AND_FORMULA__LEFT = eINSTANCE.getLtlAndFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_AND_FORMULA__RIGHT = eINSTANCE.getLtlAndFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.LtlUntilFormulaImpl <em>Ltl Until Formula</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.LtlUntilFormulaImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getLtlUntilFormula()
     * @generated
     */
    EClass LTL_UNTIL_FORMULA = eINSTANCE.getLtlUntilFormula();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_UNTIL_FORMULA__LEFT = eINSTANCE.getLtlUntilFormula_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_UNTIL_FORMULA__RIGHT = eINSTANCE.getLtlUntilFormula_Right();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.CupLabelPredicateImpl <em>Cup Label Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.CupLabelPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getCupLabelPredicate()
     * @generated
     */
    EClass CUP_LABEL_PREDICATE = eINSTANCE.getCupLabelPredicate();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CUP_LABEL_PREDICATE__LEFT = eINSTANCE.getCupLabelPredicate_Left();

    /**
     * The meta object literal for the '<em><b>Righ</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CUP_LABEL_PREDICATE__RIGH = eINSTANCE.getCupLabelPredicate_Righ();

    /**
     * The meta object literal for the '{@link org.cmg.tapas.xtext.clts.composedLts.impl.CapLabelPredicateImpl <em>Cap Label Predicate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.CapLabelPredicateImpl
     * @see org.cmg.tapas.xtext.clts.composedLts.impl.ComposedLtsPackageImpl#getCapLabelPredicate()
     * @generated
     */
    EClass CAP_LABEL_PREDICATE = eINSTANCE.getCapLabelPredicate();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CAP_LABEL_PREDICATE__LEFT = eINSTANCE.getCapLabelPredicate_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CAP_LABEL_PREDICATE__RIGHT = eINSTANCE.getCapLabelPredicate_Right();

  }

} //ComposedLtsPackage

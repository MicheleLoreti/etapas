package org.cmg.tapas.util;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;

import javax.swing.AbstractAction;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

/**
 * @author Calzolai
 *
 */
public class GarbageCollectorFrameAdapter {
	
	private static GarbageCollectorFrameAdapter instance;
		
	public static GarbageCollectorFrameAdapter getInstance(){
		if(instance == null)
			instance = new GarbageCollectorFrameAdapter();
		return instance;
	}
	
	public static InternalFrameAdapter getIFA(){
		return new InternalFrameAdapter() {			
			 public void internalFrameClosed(InternalFrameEvent e) {
				 execute();
			 }
		};
	}
	
	public static AbstractAction getAA(){
		return new AbstractAction("Garbage Collector") {
			private static final long serialVersionUID = -1281633234181556692L;
			
			public void actionPerformed(ActionEvent e) {			
				execute();
			}
		};
	}
	
	public static WindowAdapter getWA(){
		return new WindowAdapter() {			
			public void windowClosed(WindowEvent e){
				execute();
			}
		};
	}
	
	public static void execute(){
//		System.out.print("Invoking garbage collector ..... ");
		Runtime r = Runtime.getRuntime();
		double start = r.freeMemory();
		r.gc();
		double free = (r.freeMemory()-start)/(1000000);
		DecimalFormat df = new DecimalFormat("#################.##") ;
//		System.out.println("free memory: "+df.format(free)+"MB");
	}
	
}
